var doReload=true;

function refresh_recursively(mail)	{
	mail.refresh();
	setTimeout( function() { refresh_recursively(mail);}, 300000);
	doReload=false;
}

Mail = function(d){
	
	var account = 0;
	var mailContent = null;
	var wasRowdblClick = false;
	var wasReplyClick  = false, replyAction='';
	var wasPrintClick  = false;
	var node = null;
	var isRefresh = 0, selectedRowId=0;
	var selectedRowUid=0;
	
	function renderByFlags(v,rec) {
		if(!rec.data.seen)	 v = '<b>'+v+'</b>'; 
		if(rec.data.deleted) v = '<span style="color:#808080;"><del>'+v+'</del></span>';
		return v;
	}
	
	var printMail = function (mailContent) {
		var win=window.open('', 'printMail', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
		win.document.write(
			'<body><div><table cellpadding=0 cellspacing=0 style="text-align:right;font-size:inherit;">'+
			'<tr><td>'+mailContent.from+'</td><td>&nbsp;<b>:מ</b></td></tr>'+
			'<tr><td>'+(Date.parseDate(mailContent.sent, 'Y-m-d H:i:s') ? Date.parseDate(mailContent.sent, 'Y-m-d H:i:s').format('d/m/Y H:i') : mailContent.sent)+'</td><td>&nbsp;<b>:נשלח ב</b></td></tr>'+
			'<tr><td>'+'&nbsp;&lt;'+(mailContent.to ? mailContent.to[0].mailto.toString() :'')+'&gt;'+(mailContent.to ? mailContent.to[0].name.toString() :'')+'</td><td>&nbsp;<b>:אל</b></td></tr>'
		);
		if (mailContent.cc) {
			win.document.write('<tr><td>');
			for (var i=0; i<mailContent.cc.length; i++) {
				win.document.write('&nbsp;;&lt;'+mailContent.cc[i].mailto.toString()+'&gt;'+mailContent.cc[i].name.toString()+'&nbsp;');
			}
			win.document.write('</td><td><b>:Cc</b></td></tr>');
		}
		win.document.write('<tr><td>'+mailContent.subject+'</td><td><b>:נושא</b></td></tr>');
		
		if (mailContent.attachments) {
			win.document.write('<tr><td>');
			for (var i=0; i<mailContent.attachments.length; i++) {
				win.document.write('&nbsp;;'+mailContent.attachments[i].name.toString());
			}
			win.document.write('</td><td>&nbsp;<b>:קבצים מצורפים</b></td></tr>');
		}
		
		win.document.write('</table></div><div><br><br>'+mailContent.html+'</div></body>');
		win.focus();
		win.print();
		wasPrintClick = false;
	};
	
	this.mailStore = new Ext.data.JsonStore({
		proxy:new Ext.data.HttpProxy(new Ext.data.Connection({timeout:180000, url:'?m=mail_new/mail_f&f=list_folder', method:'POST'})),
		totalProperty:'total', root:'data',
		fields:['uid','size','seen','flagged','answered','deleted','priority','subject','messageId','from','to','attachments',{name:'date', type:'date', dateFormat:'d/m/Y H:i'}],
		remoteSort:true
	});
	
	this.hlStr = hlStr;
	this.searchField = new GridSearchField({enableAutoSearch:false, name:'q',store:this.mailStore,id:'q_textfield', width:150, scope:this});
	this.statusFilter = new Ext.form.ComboBox({store:[['ALL','הצג הכל'],['UNSEEN','לא נקראו'],['FLAGGED','מסומנים'],['UNANSWERED','לא נענו']], 
								hiddenName:'statusFilter',editable:false, value:'ALL', width:100, listClass:'rtl', triggerAction:'all', forceSelection:true, 
		    					listeners:{
		    						select:function(f){
		    							this.mailStore.reload();
		    						},scope:this
		    					}});
	
	this.mailGrid = new Ext.grid.GridPanel({
		region:'center',height:270,split:true,border:false,
		enableHdMenu:false,enableColumnResize:false,
		loadMask:true,
		bodyStyle:'border-top:0',
		id:'bMailGrid',
		autoExpandColumn:'subject',
		store:this.mailStore,
		// selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		columns: [
			 {dataIndex:'attachments', header:'<img src="skin/icons/bmail/mail-attachment.gif">', width:20, align:'center', renderer:function(v,m,rec){if(v)return'<img src="skin/icons/bmail/mail-attachment.gif" title="'+v+'">';}}
			,{dataIndex:'size', header:'גודל', width:55, sortable:true, css:'text-align:right; direction:ltr;', renderer:function(v,m,rec){ v=readable_size(v); return renderByFlags(v,rec);}}
			,{dataIndex:'date', header:'התקבל', width:110, sortable:true, css:'text-align:right; direction:ltr;', renderer:function(v,m,rec){v=Ext.util.Format.date(v,'d/m/Y H:i'); return renderByFlags(v,rec);}}
			,{dataIndex:'subject', header:'נושא', id:'subject', sortable:true, scope:this, renderer:function(v,m,rec){return this.hlStr(renderByFlags(v,rec));}}
			,{dataIndex:'to',   header:'אל', width:170, sortable:true, hidden:true, renderer:this.hlStr,scope:this}
			,{dataIndex:'from', header:'מאת', width:170, sortable:true, scope:this, renderer:function(v,m,rec){ v=Ext.util.Format.htmlEncode(v[0]?v[0]:v[1]);return renderByFlags(this.hlStr(v),rec);}}
			,{header:'', width:22, renderer:function(v,m,rec){
				var icon;
				if (rec.data.answered) icon='mail-reply';
				else if (rec.data.seen) icon='mail-opened';
				else icon='mail-unopened';
				return '<img src="skin/icons/bmail/'+icon+'.gif">';
			}}
			,{header:'', width:11, renderer:function(v,m,rec){ 
					if(rec.data.priority && rec.data.priority<3) return '<img src="skin/icons/bmail/mail-important.gif">'; 
					if(rec.data.priority && rec.data.priority>3) return '<img src="skin/icons/bmail/importance-low.gif">'; 
				}
			}
		]
		,bbar: new Ext.PagingToolbar({store:this.mailStore, pageSize:25, displayInfo:true})
	});
	
	this.mailGrid.createMoveToMenu = function(node,account,records,isTab){
		
		var menuMoveTo = new Ext.menu.Menu({enableScrolling:true,scrollIncrement:2});
	//	var menuObject = {};	
		var that = this;
			
			function Recursive(root,base,node,records,accaunt,isTab,that) {
				var cmenu_array =[];
			
				if(!base){
					if(root.children){
						var menu_array =[];
						for(var i = 0; i < root.children.length; i++){
							menu_array.push(Recursive(root.children[i],false,node,records,account,isTab,that));
						}
						
						cmenu_array.push({text:root.folderName, folder:root.folder,iconCls:root.iconCls, scope:this,menu:menu_array,
									handler:function(v){
									moveToFolder(that,v.folder,records,node.attributes.folder,account);
										
										if(isTab){
											var tab = Ext.getCmp('mailTabPanel').getItem('big_read_p_'+account+''+records[0].data.uid);
											Ext.getCmp('mailTabPanel').remove(tab);
										}
									}
								});
						return cmenu_array;
					} 
					// this is last element
					return {text:root.folderName, folder:root.folder,iconCls:root.iconCls, scope:this,
								handler:function(v){
								moveToFolder(that,v.folder,records,node.attributes.folder,account);
									if(isTab){
										var tab = Ext.getCmp('mailTabPanel').getItem('big_read_p_'+account+''+records[0].data.uid);
										Ext.getCmp('mailTabPanel').remove(tab);
									}
								}
							};
					
				}
				root.eachChild(function(n) {
					// if(node.attributes.folder!=n.attributes.folder ) {
						if (!n.isLeaf()) {
							// make new branch here and re-run function with new root as argument
							var menu_item = Recursive(n.attributes,false,node,records,accaunt,isTab,this);
							cmenu_array.push(menu_item);
						} else {
							var itm = {text:n.attributes.folderName, folder:n.attributes.folder,iconCls:n.attributes.iconCls, scope:this,
								handler:function(v){
								moveToFolder(that,v.folder,records,node.attributes.folder,account);
									if(isTab){
										var tab = Ext.getCmp('mailTabPanel').getItem('big_read_p_'+account+''+records[0].data.uid);
										Ext.getCmp('mailTabPanel').remove(tab);
									}
								}
							};
							if(base) cmenu_array.push(itm);
							else return itm;// make menu entry here and go upper
						 }
					// }
				},this);
				return cmenu_array;
		}
		menuMoveTo.add(Recursive(getParentNode(node),true,node,records,account,isTab,that));
		return menuMoveTo;
	};

	
	this.mailGrid.showMailTabPanel = function(d,mailContent){
		var tab = null;
		tab = new Ext.Panel({
				layout: 'fit',closable:true,title:d.subject,autoScroll:true,
				id:'big_read_p_'+d.account+''+d.uid,
				records:this.getSelectionModel().getSelections(),
				node:node, account:account,
				tbar:[
					 '-' ,{ text:'מחק',  name:'del_msg', iconCls:'delete', scope:this,
							handler:function(){
								moveToFolder(this,getParentNode(tab.node).attributes.trashFolder,tab.records,tab.node.attributes.folder,tab.account);
								Ext.getCmp('mailTabPanel').remove(tab);
							}
						}
						 ,{ text:'השב', iconCls:'mail-reply', scope:this, handler:function(){
							 var win=new MailComposeWindow('mail-reply', tab.node.getOwnerTree(),{account:getParentNode(node).attributes, mailData:d, prevMail:mailContent, action:'reply'});
							 win.show();
							}
						 }
						 ,{ text:'השב לכולם', iconCls:'mail-reply', scope:this, handler:function(){
							 var win=new MailComposeWindow('mail-reply', tab.node.getOwnerTree(),{account:getParentNode(node).attributes, mailData:d, prevMail:mailContent, action:'replyall'});
							 win.show();
							}
						 }
						,{ text:'העבר', name:'forward', iconCls:'mail-forward', scope:this, handler:function(){
								var win=new MailComposeWindow('mail-forward',tab.node.getOwnerTree(),{account:getParentNode(node).attributes, mailData:d, prevMail:mailContent, action:'forward'});
								win.show();
							}}
						 ,{ text:'הדפס', iconCls:'print', scope:this, handler:function(){
								printMail(mailContent);
							}
						 }
					,'-' ,{text:'להעביר ל',iconCls:'mail-move', scope:this, menu:this.createMoveToMenu(node,account,this.getSelectionModel().getSelections(),1)}
					]
				});
			
		tab.on('afterrender', function(){
			d.type = 'full';
			showMsg(d,mailContent,tab);
		}, this);
	
		Ext.getCmp('mailTabPanel').add(tab);
		Ext.getCmp('mailTabPanel').activate(tab);
	};
	
	function treeNodeSeen(n){
		if (n.attributes.unseen > 0) n.setText(n.attributes.folderName+' <span dir=rtl><b>('+n.attributes.unseen+')</b></span>');
		else n.setText(n.attributes.folderName);
	}
	
	this.performMenuAction = function(d){
		if(wasRowdblClick){
			this.mailGrid.showMailTabPanel(d,mailContent);
			wasRowdblClick = false;
			this.mailGrid.getEl().unmask();
		}
		if(wasReplyClick && replyAction){
			var win=new MailComposeWindow('mail-reply',this.menuTree,{account:getParentNode(node).attributes,mailData:this.mailGrid.getSelectionModel().getSelected().data, prevMail:mailContent, action:replyAction});
			win.show();
			wasReplyClick = false;
			this.mailGrid.getEl().unmask();
		}
		if(wasPrintClick){
			printMail(mailContent);
			wasPrintClick = false;
		}
	};
	
	this.mailGrid.getSelectionModel().on('rowselect', function(sm) {
		this.mailList.getTopToolbar().find('name','reply')[0].setDisabled(sm.getCount() != 1);
		this.mailList.getTopToolbar().find('name','replyall')[0].setDisabled(sm.getCount() != 1);
		this.mailList.getTopToolbar().find('name','forward')[0].setDisabled(sm.getCount() != 1);
		this.mailList.getTopToolbar().find('name','print')[0].setDisabled(sm.getCount() != 1);

		if (sm.getCount() != 1) return;
		
		if(node.attributes.folderName.search(/^אשפה|Bin|Trash|(פריטים שנמחקו)$/i) == -1)
			this.mailList.getTopToolbar().find('name','del_msg')[0].setDisabled(!sm.getCount());
		else
			this.mailList.getTopToolbar().find('name','del_msg')[0].setDisabled(true);
		
		var d=sm.getSelected().data;
		this.mailPreview.q = Ext.getCmp('q_textfield').getValue();
		if (selectedRowUid!=sm.getSelected().data.uid) {
			mailContent = ''; 
			
			if(MailPreview.layout != 'Hide'){
				this.mailPreview.body.update('');
				this.mailPreview.getEl().mask('...טוען');
			}
			
			Ext.Ajax.request({
				url:'?m=mail_new/mail_f&f=load_msg',
				params:{account:account,folder:node.attributes.folder,uid:d.uid},
				timeout:180000,
				success: function(r,o){
					selectedRowUid=sm.getSelected().data.uid;
					mailContent = Ext.decode(r.responseText);
					d.type = 'prev';d.account = account;d.folder = node.attributes.folder;
					if (MailPreview.layout!='Hide') showMsg(d,mailContent,this.mailPreview);
					this.performMenuAction(d);
					
					if (!ajRqErrors(mailContent) && sm.getSelected() && sm.getSelected().data.seen == '') {
						//	var rowIndex=this.mailStore.indexOf(sm.getSelected());
							sm.getSelected().data.seen=1;
							sm.suspendEvents(false);
							this.mailGrid.view.refresh();
							sm.resumeEvents();
							this.menuTree.getSelectionModel().getSelectedNode().attributes.unseen--;
							treeNodeSeen(this.menuTree.getSelectionModel().getSelectedNode());
					}
					if (MailPreview.layout!='Hide') this.mailPreview.getEl().unmask();
				},
				failure: function(r){
					if (MailPreview.layout!='Hide') this.mailPreview.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); 
					if(wasRowdblClick){
						wasRowdblClick = false;
						this.mailGrid.getEl().unmask();
					}	
				},
				scope: this
			});
		}else this.performMenuAction(d);
		
		selectedRowUid=sm.getSelected().data.uid;
		selectedRowId=this.mailStore.indexOf(sm.getSelected());
	}, this);
	
	this.newFolderWin = function(acc,this_node,old_name){
		var parent_name = ''; 
		var win = null;
		if (!old_name) {
			parent_name = this_node.attributes.folder;
			} else parent_name = this_node;
	
		win=new Ext.Window({
			title:(old_name ? 'שינוי שם': 'הוספת תיקייה'), layout:'fit', modal:true, border:true, width:230, closeAction:'close',	buttonAlign:'center', plain:true,
			items:{xtype:'form', baseCls:'x-plain', labelWidth:65, items:{xtype:'textfield', fieldLabel:'שם תיקייה', width:140, name:'name', allowBlank:false}},
			buttons: [
				{text:(old_name ? 'בצע': 'הוסף'), scope:this, handler:function(){
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url:'?m=mail_new/mail_f&f='+(old_name ? 'rename_folder': 'create_folder'),
						params:{account:acc,name:win.items.items[0].items.items[0].getValue(),parent_name:parent_name, old_name:old_name},
						success: function(r,o){
							this.getEl().unmask();
							if (!ajRqErrors(Ext.decode(r.responseText))) {
								// this.refreshTree();
						    	if (old_name) {this_node.leaf = 0;};
								this.menuTree.getLoader().load(this.menuTree.getRootNode());
								win.close();
							}
						},
						failure: function(r,o){this.getEl().unmask(); win.close(); ajRqErrors(o.result); }
						,scope:this
					});
				}
				},{text:'בטל', handler: function(){win.close();}}
			]
		});
		win.show();
	};	
	
	var CustomTreeLoader = Ext.extend(Ext.tree.TreeLoader,{
		 requestData : function(node, callback, scope){
			 if(this.fireEvent("beforeload", this, node, callback) !== false){
				 if(this.directFn){
					 var args = this.getParams(node);
					 args.push(this.processDirectResponse.createDelegate(this, [{callback: callback, node: node, scope: scope}], true));
					 this.directFn.apply(window, args);
				 }else{
					 this.transId = Ext.Ajax.request({
						 method:this.requestMethod,
						 timeout:this.requestTimeout,
						 url: this.dataUrl||this.url,
						 success: this.handleResponse,
						 failure: this.handleFailure,
						 scope: this,
						 argument: {callback: callback, node: node, scope: scope},
						 params: this.getParams(node)
					 });
				 }
			 }else{
				 this.runCallback(callback, scope || node, []);
			 }
		 }
	});
	
	
	// folder tree
	this.menuTree = new Ext.tree.TreePanel({
		lines:false,
		loader:new CustomTreeLoader({
			requestTimeout:1800000,
			dataUrl: '?m=mail_new/mail_f&f=load_folders'
		}),
		bodyStyle:'direction:rtl',
		animate:true,
		border:false,
		root:{nodeType:'async'},
		rootVisible:false,
		listeners: {scope:this
			,click:function(n){
				if(n.parentNode.isRoot && n.attributes.root_id) return;//this is root
				if (node!=n || this.mailList.getTopToolbar().find('name','q')[0].getValue() ) {
					this.mailList.getTopToolbar().find('name','q')[0].setValue('');
					account=n.attributes.account; this.mailList.getTopToolbar().find('name','del_msg')[0].setDisabled(true);
					this.mailList.getTopToolbar().find('name','reply')[0].setDisabled(true);
					this.mailList.getTopToolbar().find('name','replyall')[0].setDisabled(true);
					this.mailList.getTopToolbar().find('name','forward')[0].setDisabled(true);
					this.mailList.getTopToolbar().find('name','print')[0].setDisabled(true);
					node = n; this.mailStore.reload({params:{from:'cache'}}); 
					if (n.attributes.folder=='OUTBOX') this.mailList.getTopToolbar().find('name','del_msg')[0].hide();
					else this.mailList.getTopToolbar().find('name','del_msg')[0].show();
					if(MailPreview.layout != 'Hide') this.mailPreview.body.update('');
					selectedRowId=0;
					Ext.getCmp('mailTabPanel').setActiveTab(0);
					var cm = this.mailGrid.getColumnModel();
					if (n.attributes.folderName.match(/^(Sent|Sent[- ]Mail)|OUTBOX|אשפה|(דואר יוצא)|(פריטים שנשלחו)$/i) ){
						cm.setHidden(5, true);	cm.setHidden(4, false);cm.setColumnHeader(2,'נשלח');
					} else {
						cm.setHidden(4, true);	cm.setHidden(5, false);cm.setColumnHeader(2,'התקבל');
					}
				}
			} 
			// ,load:function(n){ this.menuTree.getEl().unmask(); return; this.menuTree.selectPath('/xnode-10/xnode-72/xnode-73');  } //Dump(n.childNodes[0].getPath());  n.childNodes[0].select();
		}
		// root: new Ext.tree.AsyncTreeNode({
            // text:'<b>פרטי הדואר</b>',
			// icon:'skin/icons/1/mail-folders.gif',
			// expanded: true,
            // children: [
				// {text:'דואר נכנס', leaf:true, iconCls:'mail-inbox'}
				// ,{text:'דואר יוצא', leaf:true, iconCls:'mail-outbox'}
				// ,{text:'פריטים שנשלחו', leaf:true, iconCls:'mail-sent'}
				// ,{text:'Draft', leaf:true, iconCls:'mail-draft'}
			// ]
        // })
	});
	
	this.menuTree.getLoader().on('beforeload', function(treeLoader,node){	
		Ext.MessageBox.show({
          msg:'מתחבר לדוא"ל...',//msg:'<img src="skin/icons/1/trash48blue.gif" align=left><br>האם ברצונך לבטל את ההתקשרות הזאת ?'
          progressText:'טוען...', width:300, wait:true, waitConfig:{interval:200}, icon:'ext-mb-icon'
       });
		var setParams = {refresh:isRefresh};
		Ext.apply(treeLoader.baseParams, setParams);
	});
	
	this.menuTree.getLoader().on('load', function(o,n,r){
		Ext.MessageBox.hide();
		var res = Ext.decode(r.responseText);
		if(res.success != null && !res.success){
			Ext.MessageBox.show({
				width:400, title:'שגיאה', msg:res.msg, buttons:Ext.MessageBox.OK, icon:Ext.MessageBox.ERROR, scope:this});
		} else {
			if (res[0].children) {
				this.menuTree.selectPath(this.menuTree.getRootNode().childNodes[0].childNodes[0].getPath());
				account=res[0].children[0].account;
				node = this.menuTree.getRootNode().childNodes[0].childNodes[0]; 
				this.mailStore.reload({params:{from:'cache'}});
				var ttRootNode = null;
				for(var i = 1; i < this.menuTree.getRootNode().childNodes.length; i++){
					ttRootNode = this.menuTree.getRootNode().childNodes[i-1];
					//	Ext.QuickTips.init();
					new Ext.ToolTip({
							target: ttRootNode.ui.elNode,
							width: 200,
							mouseOffset:[-220,-20],
							autoLoad: {
								url: '?m=mail_new/mail_f&f=get_quota',
								params:{account:ttRootNode.attributes.root_id}
							},
							dismissDelay: 15000 // auto hide after 15 seconds
						});					
				}
				
			}
		}
	},this);
	
	// walk through nodes and add unseen count to text 
	this.menuTree.on('load', function(nod){
		nod.cascade(function(n){treeNodeSeen(n);});
		Ext.MessageBox.hide();
	});

	// right panel
	this.east=new Ext.Panel({
		region:'east', width:200, autoScroll:true, split:true, border:false,
		items:[
			this.menuTree
		]
		,tbar:new Ext.Toolbar({hideBorders:false,
				items: [
				    '<span style="font-weight:bold; color:#15428B;font-size:13px;">תיקיות הדואר</span>',
					{tooltip:'רענן תיקיות',iconCls:'update', scope:this,
						handler:function(){this.refreshTree();}
					},'-',
					{tooltip:'הגדרות דוא"ל', iconCls:'gear', scope:this,
						handler:function(){var win=new OpenAccountWin('edit',this);win.show();}
					}
			]})
	});	
	
	this.refreshTree = function(){isRefresh = 1; this.menuTree.getLoader().load(this.menuTree.getRootNode());};
	
	this.menuTree.on('contextmenu', function(thisnode, e){
		var menu_items=[];
		
		this.menuTree.selectPath(thisnode.getPath());
		account=thisnode.attributes.account; 
		this.mailList.getTopToolbar().find('name','del_msg')[0].setDisabled(true);
		this.mailList.getTopToolbar().find('name','reply')[0].setDisabled(true);
		node = thisnode; this.mailStore.reload(); Ext.getCmp('mailTabPanel').setActiveTab(0);

		// New Folder
		if (thisnode.attributes.folder != getParentNode(thisnode).attributes.trashFolder) menu_items.push({text:'תיקייה חדשה', iconCls:'folder_add', account:thisnode.attributes.root_id, handler:function(me){this.newFolderWin(thisnode.attributes.account || me.account,thisnode);}, scope:this});
		
		if (thisnode.attributes.folder && !thisnode.attributes.folderName.match(/^Inbox|(Sent|Sent[- ]Mail)|Spam|(Junk E-mail)|Drafts|Bin|Trash|אשפה|(דואר נכנס)|(דואר יוצא)|(פריטים שנשלחו)|(פריטים שנמחקו)$/i) ) 
			menu_items.push({text:'שינוי שם', iconCls:'mail-draft', node:thisnode.attributes, handler:function(me){this.newFolderWin(me.node.account,me.node.folder,me.node.folder);}, scope:this});
		
		if (thisnode.leaf) {
			if (!thisnode.attributes.folderName.match(/^Inbox|(Sent|Sent[- ]Mail)|Spam|(Junk E-mail)|Drafts|Bin|Trash|אשפה|(דואר נכנס)|(דואר יוצא)|(פריטים שנשלחו)|(פריטים שנמחקו)$/i) )
			// if (thisnode.attributes.folder && !thisnode.attributes.folder.match(/^(Inbox|(Sent|Sent[- ]Mail)|Spam|(Junk E-mail)|Drafts|Bin|Trash|&BdAF6QXkBdQ-|(דואר יוצא)|(&BdMF1QXQBeg- &BdkF1QXmBdA-)|(&BdMF1QXQBeg- &BdYF0QXc-)|(פריטים שנמחקו))$/i) ) 
			menu_items.push(
			//	{text:'שינוי שם', iconCls:'mail-draft', node:thisnode.attributes, handler:function(me){this.newFolderWin(me.node.account,me.node.folder)}, scope:this},
				{text:'מחק תיקייה', iconCls:'delete', node:thisnode.attributes, handler:function(me){
					
					Ext.MessageBox.show({
						width:260, title:'אזהרה!', msg:'האם ברצונך למחוק תיקייה ולהעביר את הפריטים בתיקייה זו לסל אשפה?', icon: Ext.MessageBox.WARNING, buttons:Ext.MessageBox.YESNO, scope:this,
						fn: function(btn) {
							if(btn=='yes') this.deleteFolder(me.node.folder,me.node.account);
						}
					});
				}, scope:this}
			);
			}
			
			if(thisnode.attributes.folder && (thisnode.attributes.folder == getParentNode(thisnode).attributes.trashFolder)){
				menu_items.push({text:'רוקן סל אשפה לצמיתות',iconCls:'delete', scope:this,
					handler:function(){
						Ext.MessageBox.show({
							width:345, title:'אזהרה!', msg:'האם ברצונך לרוקן סל אשפה?<br>לא ניתן להחזיר פריטים לאחר ביצוע פעולה זאת!', icon: Ext.MessageBox.WARNING, buttons:Ext.MessageBox.YESNO, scope:this,
							fn: function(btn) {
								if(btn=='yes') {
									Ext.Ajax.request({
										url:'?m=mail_new/mail_f&f=empty_folder',
										params:{account:thisnode.attributes.account,folder:thisnode.attributes.folder},
										timeout:180000,
										success: function(r,o){
											//var res = Ext.decode(r.responseText);
											if(thisnode == node){
												var mg = Ext.getCmp('mailTabPanel').get(0).get(0);
												mg.getStore().removeAll();
												mg.view.refresh();
											}
											thisnode.attributes.unseen = 0;
											treeNodeSeen(thisnode);
										},
										failure: function(r){Ext.Msg.alert('Error', 'Request failed.');},
										scope: this
									});
								} 
							}
						});
					}});
			} else {
				menu_items.push({text:'רוקן תיקייה', iconCls:'mail-trash', node:thisnode.attributes,
					handler:function(me){
						Ext.MessageBox.show({
							width:260, title:'אזהרה!', msg:'האם ברצונך להעביר את הפריטים בתיקייה זו לסל אשפה?', icon: Ext.MessageBox.WARNING, buttons:Ext.MessageBox.YESNO, scope:this,
							fn: function(btn) {
								if(btn=='yes') moveToFolder(this.mailGrid,getParentNode(thisnode).attributes.trashFolder,'*',me.node.folder,me.node.account);
							}
						});
					}, scope:this});
			} 
		
		e.stopEvent();
		if (menu_items.length>0) {
			this.menu = new Ext.menu.Menu({enableScrolling:true,autoScroll:true,autoWidth:true, items:menu_items});
			this.menu.showAt(e.getXY());
		}
		
	},this);
	
	this.mailPreview=new Ext.Panel({autoScroll:true,border:false});
	
	this.mailPreviewResize = function(width,height){
		if (MailPreview.width!=width || MailPreview.height!=height) {
			MailPreview.width=width;
			MailPreview.height=height;
			setObjView({a:'mail_cfg', module:'mail', object:'MailPreview', cfg:'{width:'+MailPreview.width+', height:'+MailPreview.height+', layout:"'+MailPreview.layout+'"}'});
		}
	};
	
	this.mailList = new Ext.Panel({
		closable:false,title:'ראשי', border:false,// baseCls:'x-plain', bodyStyle:'padding:4px 0px 4px 4px;' // border-right:1px solid #99BBE8;
		layout:'border', split:true,iconCls:'mailBox',
		items:[
			this.mailGrid,
			{id:'bottom-preview', layout:'fit', split:true, border:false, region:'south',
			items:(MailPreview.layout=='Bottom' ? this.mailPreview :null),
			height: MailPreview.height,
			hidden:MailPreview.layout!='Bottom',
			listeners:{
				resize: function(p,w,h) {this.mailPreviewResize(MailPreview.width, h);},
				scope: this
			}
            },{id:'right-preview', layout:'fit', border:false, split: true, region:'west',
			items:(MailPreview.layout=='Left' ? this.mailPreview :null),
			width: MailPreview.width,
			hidden:MailPreview.layout!='Left',
			listeners:{
				resize: function(p,w,h) {this.mailPreviewResize(w, MailPreview.height);},
				scope: this
			}
            }],
		tbar:[
	            this.searchField
		   ,{name:'searchPreferences',menu:new Ext.menu.Menu({width:80, cls:'rtl', items:[
                    {boxLabel:'נושא', name:'subject', style:'margin-right:3px;margin-left:8px;', xtype:'checkbox', checked:true,  scope:this},                              
                    {boxLabel:'ל', name:'to', style:'margin-right:3px;margin-left:8px;',    xtype:'checkbox', checked:true,  scope:this},                              
                    {boxLabel:'מאת', name:'from', style:'margin-right:3px;margin-left:8px;',  xtype:'checkbox', checked:true,  scope:this},                              
                    {boxLabel:'Cc', name:'cc', style:'margin-right:3px;margin-left:8px;',   xtype:'checkbox', checked:false, scope:this},                              
                    {boxLabel:'Bcc', name:'bcc', style:'margin-right:3px;margin-left:8px;',  xtype:'checkbox', checked:false, scope:this},                              
					{boxLabel:'מכתב', name:'text', style:'margin-right:3px;margin-left:8px;', xtype:'checkbox', checked:false, scope:this}                                
                ]}),iconCls:'filter', scope:this}
			 ,'-', this.statusFilter
			  ,'-',{ text:'חדש', name:'new_mail', iconCls:'mail_create', scope:this, handler:function(me){doReload=false; var win=new MailComposeWindow(me.iconCls,this.menuTree);win.show();}}
				 ,{ text:'בדוק דואר',iconCls:'check_email_box', scope:this, handler:function(){if (account && node) {this.mailList.getTopToolbar().find('name','q')[0].setValue(''); this.mailStore.reload({params:{from:'server'}}); }   }}
			 ,'-',{ text:'השב', name:'reply', disabled:true, iconCls:'mail-reply', scope:this,
						handler:function(me){
						if(!mailContent) {
							this.mailGrid.getEl().mask('...טוען');
							wasReplyClick = true; replyAction = 'reply';
						}else{
							var win=new MailComposeWindow(me.iconCls,this.menuTree,{account:getParentNode(node).attributes,mailData:this.mailGrid.getSelectionModel().getSelected().data, prevMail:mailContent, action:'reply'});
							win.show();
						}
					}}
			 ,{ text:'השב לכולם', name:'replyall', disabled:true, iconCls:'mail-reply', scope:this,
						handler:function(me){
						if(!mailContent) {
							this.mailGrid.getEl().mask('...טוען');
							wasReplyClick = true; replyAction = 'replyall';
						}else{
							var win=new MailComposeWindow(me.iconCls,this.menuTree,{account:getParentNode(node).attributes,mailData:this.mailGrid.getSelectionModel().getSelected().data, prevMail:mailContent, action:'replyall'});
							win.show();
						}
					}}
				,{ text:'העבר', name:'forward', disabled:true, iconCls:'mail-forward', scope:this,
						handler:function(me){
						if(!mailContent) {
							this.mailGrid.getEl().mask('...טוען'); 
							wasReplyClick = true; replyAction = 'forward';
						}else{
							var win=new MailComposeWindow(me.iconCls,this.menuTree,{account:getParentNode(node).attributes, mailData:this.mailGrid.getSelectionModel().getSelected().data, prevMail:mailContent, action:'forward'});
							win.show();
						}
					}}
				,{ text:'הדפס', name:'print', disabled:true, iconCls:'print', scope:this,
						handler:function(me){
						if(!mailContent) {this.mailGrid.getEl().mask('...טוען'); wasPrintClick = true;}
						else printMail(mailContent);
					}}
			     ,{ text:'מחק', name:'del_msg', disabled:true, iconCls:'trash', scope:this,
						handler:function(){
							moveToFolder(this.mailGrid,getParentNode(node).attributes.trashFolder,this.mailGrid.getSelectionModel().getSelections(),node.attributes.folder,account);
							this.mailList.getTopToolbar().find('name','del_msg')[0].setDisabled(true);}}
			,'-' 
		]
	});
	
	
	this.center = new Ext.TabPanel({
		autoScroll:true, activeTab:0,region:'center',id:'mailTabPanel',
		items:[this.mailList]
	});
	
	this.mailList.on('render',function(){
		Ext.Ajax.request({
			url:'?m=mail_new/mail_f&f=update_cache',
			scope: this
		});
	},this);
	// doubleClick
	this.mailGrid.on('rowdblclick', function(grid, row, e){
		var tab;
		var d = grid.getSelectionModel().getSelected().data;
		d.account = account; d.folder = node.attributes.folder;
		if(!(tab=Ext.getCmp('mailTabPanel').getItem('big_read_p_'+account+''+d.uid))){
			if(!mailContent) {this.mailGrid.getEl().mask('...טוען'); wasRowdblClick = true;}
			else this.mailGrid.showMailTabPanel(d,mailContent);
		}else{Ext.getCmp('mailTabPanel').activate(tab);}
	},this);
	
	function changeFlag(flag,obj){
		var selArray = obj.getSelectionModel().getSelections();
		var uids = [];
		for(var i = 0; i < selArray.length; i++){
			if(flag == 'SEEN'){
				if(!selArray[i].data.seen)
					node.attributes.unseen--;
				selArray[i].data.seen = 1;
			}else{
				if(selArray[i].data.seen)
					node.attributes.unseen++;
				selArray[i].data.seen = '';
			}
			uids.push(selArray[i].data.uid);
		}
		Ext.Ajax.request({
			url:'?m=mail_new/mail_f&f=change_flag&uids='+uids,
			params:{account:account,folder:node.attributes.folder,flag:flag},
			timeout:180000,
			success: function(r,o){
				var res = Ext.decode(r.responseText);
				if(!res.success) Ext.Msg.alert('Error', res.msg);
			},
			failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});

					treeNodeSeen(node);
					obj.getSelectionModel().suspendEvents(false);
					obj.getSelectionModel().clearSelections();
					obj.view.refresh();
					obj.getSelectionModel().resumeEvents();
	}
	
	// function getRootNode(node) {
		// var depth = 0;
		// var p = node;
		// while (p.parentNode) {
			// p = p.parentNode;
		// }
		
		// return p
	// }
	
	function getParentNode(node){   // Ruturns ROOT node
		var parentNode = node;
		while( typeof parentNode.leaf !== 'undefined'){
			parentNode = parentNode.parentNode;
		}
		return parentNode;
	}
	
	function moveToFolder(obj,toFolder,selArray,folder,faccount){
		obj = Ext.getCmp('bMailGrid');
		
		var uids = [];
		var unreaded = 0;
		var parseIntTemp = 0;
	
		if (selArray=='*'){
			uids='*';
			obj.store.removeAll();
		}else for(var i = 0; i < selArray.length; i++){
			uids.push(selArray[i].data.uid);
			
			if(!selArray[i].data.seen) unreaded++;
			
			//selectedRowId=obj.store.indexOf(selArray[i]);
			obj.store.remove(selArray[i]);
		}
		node.attributes.unseen = parseInt(node.attributes.unseen);
		
		Ext.Ajax.request({
			url:'?m=mail_new/mail_f&f=moveToFolder&uids='+uids,
			timeout:180000,
			params:{account:faccount,folder:folder,toFolder:toFolder},
			success: function(r,o){
				var res=Ext.decode(r.responseText);
				if(!res.success)Ext.Msg.alert('Error', res.msg);
				obj.getStore().reload();
				obj.loadMask.hide();
			},
			failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
		
					obj.getSelectionModel().suspendEvents(false);
		//obj.getStore().reload();
		parseIntTemp = parseInt(node.attributes.unseen) - unreaded;
		node.attributes.unseen = parseIntTemp;
					treeNodeSeen(node);
					
					var toNode = getParentNode(node).findChild('folder',toFolder);
					if(toNode){
			parseIntTemp = parseInt(toNode.attributes.unseen) + unreaded;
			toNode.attributes.unseen = parseIntTemp;
						treeNodeSeen(toNode);
					}
		obj.view.refresh();
					obj.getSelectionModel().resumeEvents();
	}

	this.deleteFolder = function (folder,faccount){
		this.mailGrid.getEl().mask('...טוען');
		Ext.Ajax.request({
			url:'?m=mail_new/mail_f&f=delete_folder',
			timeout:180000,
			params:{account:faccount,name:folder,trashFolder:getParentNode(node).attributes.trashFolder},
			success: function(r,o){
				var res=Ext.decode(r.responseText);
				if(res.success){
					this.mailGrid.getSelectionModel().suspendEvents(false);
					this.mailGrid.getStore().removeAll();

					var toNode = getParentNode(node).findChild('folder',getParentNode(node).attributes.trashFolder);
					if(toNode){
						toNode.attributes.unseen += parseInt(node.attributes.unseen);
						treeNodeSeen(toNode);
					}
					if (!node.nextSibiling && !node.previousSibiling) {node.parentNode.leaf = 1;}
					
					node.remove();
					this.mailGrid.getSelectionModel().clearSelections();
					this.mailGrid.view.refresh();
					this.mailGrid.getSelectionModel().resumeEvents();
				}else Ext.Msg.alert('Error', res.msg);
				this.mailGrid.getEl().unmask();
			},
			failure: function(r){this.mailGrid.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.mailGrid.on('rowcontextmenu', function(grid, row, e){
		// var row_data = this.store.getAt(row).data;
		Ext.each(this.getSelectionModel().getSelections(), function (r){if(r.data.seen==1) seen=1; if(r.data.seen==0) unseen=1;});
		
		this.getSelectionModel().suspendEvents(false);
		
		if(!this.getSelectionModel().getSelections().length) this.getSelectionModel().selectRow(row,false);

		this.getSelectionModel().resumeEvents();
		
		var menuItems = [];
		
		if(this.getSelectionModel().getSelections().length == 1) {
			this.getSelectionModel().suspendEvents(false);
			this.getSelectionModel().selectRow(row);
			this.getSelectionModel().resumeEvents();

			menuItems.push(
				{text:'פתח',iconCls:'mail-opened', scope:this, handler:function(){
					this.getSelectionModel().fireEvent('rowselect',this.getSelectionModel());
					this.fireEvent('rowdblclick',this);
				}},
				
				{text:'השב',iconCls:'mail-reply', scope:this, handler:function(){
					wasReplyClick = true; replyAction='reply';
					this.getSelectionModel().fireEvent('rowselect',this.getSelectionModel());
				}},
				
				{text:'השב לכולם',iconCls:'mail-reply', scope:this, handler:function(){
					wasReplyClick = true; replyAction='replyall';
					this.getSelectionModel().fireEvent('rowselect',this.getSelectionModel());
				}},
				
				{text:'העבר',iconCls:'mail-forward', scope:this, handler:function(){
					wasReplyClick = true; replyAction='forward';
					this.getSelectionModel().fireEvent('rowselect',this.getSelectionModel());
				}},
				
				
				{text:'הדפס',iconCls:'print', scope:this, handler:function(){
					wasPrintClick = true;
					this.getSelectionModel().fireEvent('rowselect',this.getSelectionModel());
				}}
			);
			
			if (this.getSelectionModel().getSelected().data.seen)  menuItems.push({text:'סמן כלא נקרא',iconCls:'mail-unopened', scope:this,handler:function(){changeFlag('UNSEEN',this);}});
			else  menuItems.push({text:'סמן כנקרא',iconCls:'mail-opened', scope:this, handler:function(){changeFlag('SEEN',this);}});
		}
		
		menuItems.push({text:'להעביר ל',iconCls:'mail-move', scope:this, menu:this.createMoveToMenu(node,account,this.getSelectionModel().getSelections(),0)});
		
		this.menu = new Ext.menu.Menu({width:125,items:menuItems});
		
		if(node.attributes.folderName.search(/^אשפה|Bin|Trash|(פריטים שנמחקו)$/i) == -1 && node.attributes.folder!='OUTBOX')
			this.menu.insert(8,{text:'מחק',iconCls:'trash', scope:this, handler:function(){moveToFolder(this,getParentNode(node).attributes.trashFolder,this.getSelectionModel().getSelections(),node.attributes.folder,account);}});
		else		
			this.menu.insert(8,{text:'מחק לצמיתות',iconCls:'delete', scope:this, handler:function(){
				Ext.MessageBox.show({width:380, title:'אזהרה!', msg:'האם ברצונך למחוק את ההודעות המסומנות לצמיתות?', icon: Ext.MessageBox.WARNING, buttons:Ext.MessageBox.YESNO, scope:this,
					fn: function(btn) {	if(btn=='yes') moveToFolder(this,'erase',this.getSelectionModel().getSelections(),node.attributes.folder,account);}
				});
			}});
		
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});
	
	this.mailStore.on('beforeload', function(store, options){
		
		var statusFilter = this.statusFilter.getValue();
		var searchValue = this.searchField.getValue().trim();
		var searchPreferences = '';
		doReload = searchValue.length >= this.searchField.minLength ? false : true;
		if(!doReload){
			var searchPrefArray = new Array();
			this.mailList.getTopToolbar().find('name','searchPreferences')[0].menu.items.each(function(itm){
				if(itm.getValue()) searchPrefArray.push(itm.getName());
			},this);
			searchPreferences = searchPrefArray.toString();
		}
		
		var setParams = {account:account, folder:node.attributes.folder, q:searchValue, searchFilter:searchPreferences, statusFilter:statusFilter};
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);

	
	this.mailStore.proxy.on('load', function(proxy, o, options){
		if(options.params.account != node.attributes.account || options.params.folder != node.attributes.folder){
			o.request.callback = function(){};
			this.mailGrid.loadMask.hide();
		}
	},this);
	
	
	this.mailStore.on('load', function(store,records,options){		
		node.attributes.unseen = this.mailStore.reader.jsonData.unseen;
		treeNodeSeen(node);
		
		var i = 0;
		this.mailStore.each(function(r) { 
			if (r.data.uid == selectedRowUid) {
				this.mailGrid.getSelectionModel().selectRow(i);
			} 
			i++;
		},this);
		if(options.params && options.params.from == 'cache') this.mailStore.reload({params:{from:'server'}});
	}, this);
	
	this.mailStore.proxy.on('exception', function(o, type, action, options, response, arg ){
		ajRqErrors(Ext.decode(response.responseText));
	},this);
	
	this.refresh = function(){
		if (doReload && account && node) {
			doReload = !doReload;
			var folder=node.attributes.folder;
			var start=this.mailStore.lastOptions.params.start;
			var statusFilter = this.mailStore.lastOptions.params.statusFilter;

Ext.Ajax.request({
				url:'?m=mail_new/mail_f&f=refresh_folders',
				params:{account:account,folder:folder,from:'server',start:start, statusFilter:statusFilter},
				timeout:180000,
				success: function(r,o){
					var searchValue = this.searchField.getValue().trim();
					// If currently in search do not load grid
					// or if current status filter diffrent from filter status sended to server
					if((searchValue.length < this.searchField.minLength) && (this.statusFilter.getValue() == statusFilter)){
					d = Ext.decode(r.responseText);
					Ext.each(d.treedata ,function(r){ 
						var n = this.menuTree.getRootNode().findChild('root_id',r.account_id).findChild('folder',r.folder);
						n.attributes.unseen=r.unseen;
						treeNodeSeen(n);
					},this);
					
					if (d.newMsg) {
						Crm.mailAlerts.msg('הודעת מערכת','התקבלו <b>'+d.newMsg+'</b> הודעות חדשות &nbsp; <img src="skin/icons/bmail/mail_new.gif" align="absmiddle">');
						selectedRowId=selectedRowId+d.newMsg;
					}
					if (!start && d.data && folder==node.attributes.folder) this.mailStore.loadData(d, false);
					}
					doReload=!doReload;
				},
				failure: function(){doReload=!doReload;},
				scope: this
			});
		}
	};
	
	Mail.superclass.constructor.call(this, {
		layout: 'border',
		items:[this.center,this.east/*,this.north*/]
    });
	
	this.on('afterrender',function(){firstRun();});
	// Start mail refreshing
	var mail=this;	setTimeout(function() { refresh_recursively(mail);}, 10000);
};
Ext.extend(Mail, Ext.Panel);

 function firstRun(){
	if (!primary_mail_account) {
		var check_win = null;
		check_win=new Ext.Window({
			title:'B-mail', layout:'fit',modal:true,width:350,closeAction:'close',buttonAlign:'center',bodyStyle:'padding:5px;',plain:true,
			items:new Ext.Panel({autoHeight:true, baseCls:'x-plain',border:false,
				items:{border:false,html:'<b>ברוכים הבאים לB-mail, מערכת הדואר האלקטרוני של באפי!</b><br>B-mail מאפשרת לכם לרכז את כל הדואר האלקטרוני שלכם מהתיבות השונות למקום אחד.<br> ניתן להגדיר מספר חשבונות דואר ממגוון ספקים:<br> Gmail, וואלה!, 013נטוויז\'ן, בזק בינלאומי ועוד.<br>באפשרותך להוסיף את חשבון הדואר האלקטרוני שלך.', baseCls: 'x-plain'}
			})
			,buttons:[
				 {text:'אישור', handler:function(){check_win.close(); var accwin = new OpenAccountWin('new',this); accwin.show();}}
				,{text:'ביטול', handler:function(){check_win.close();} }
			]
		});
		check_win.show();
		check_win.center();
	}
}

Ms.mail_new.run = function(cfg){
	var m=Ms.mail_new;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		win = Crm.desktop.createWindow({maximized:false, width:Crm.desktop.getWinWidth()-100,height:Crm.desktop.getWinHeight()-50, items: new Mail()}, m);
	}
	win.show();
};
