textField = Ext.extend(Ext.form.TextField, {allowBlank:false, listeners:{ show: function(el){el.focus(true,100);}}});

var Record = Ext.data.Record.create(['id','name']);

ExpensesgroupsGrid= function(d,cfg) {
	this.saveClientgroup = function(rec){
		Ext.Ajax.request({
			url:'?m=config/config&f=add_expensesgroup',
			params:rec.data,
			success: function(r,o){
				var d=Ext.decode(r.responseText);
				rec.set('id',d.id);
			},
			failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
		return true;
	}
	
	this.gridAfterEdit = function(e){
		if (this.saveClientgroup(e.record)) {
			if ((this.store.getCount()-this.store.indexOf(e.record))==1) {//If last record
				e.grid.store.insert(this.store.getCount(), new Record({VAT:100}));
				e.grid.startEditing(this.store.getCount()-1,0); 
			}
		}
	}

	this.store = new Ext.data.JsonStore({
		url:'?m=config/config&f=expenses_group_list',
		totalProperty:'total', root:'data',
		fields: ['id','name','VAT']
	});
	
	this.store.on('beforeload', function(store, options){this.store.removeAll();}, this);
	
	this.taxCombo = new Ext.form.ComboBox({
		store:[[0,'0%'],[66.66,'66.66%'],[100,'100%']], hiddenName:'VAT', valueField:'id', displayField:'val', width:70, triggerAction:'all', forceSelection:true, allowBlank:false, value:0
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show:	function(el){el.getEl().dom.parentNode.style.width='70px'; el.getEl().dom.style.height='18px';}
		}
	});

	
	ExpensesgroupsGrid.superclass.constructor.call(this, {
	    store:this.store, stripeRows:true, autoExpandColumn:'name', iconCls:'plus', enableColumnResize:false, clicksToEdit:1,
	    columns:[{header:'שם יחידה', dataIndex:'name', id:'name', editor:textField},{header:'מע"מ', dataIndex:'VAT', width:70, renderer:function(v){return '% '+v}, editor:this.taxCombo}]
		,tbar: [ 
			{text:'הוסף קבוצה', iconCls:'add', scope:this, handler:function(){
				this.store.insert(this.store.getCount(), new Record({VAT:100}));
				this.startEditing(this.store.getCount()-1,0);
			}}
			,{text:'מחק קבוצה', iconCls:'delete', disabled:true, scope:this, handler:function(){this.delRow();}}
		]
		,bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})
		,initComponent: function() {ExpensesgroupsGrid.superclass.initComponent.call(this);}
	});
	
	this.on('afteredit', this.gridAfterEdit);
	
	this.getSelectionModel().on('selectionchange', function(sm) {
		this.getTopToolbar().items.items[1].setDisabled(false);
	}, this);
	
	this.delRow = function(){
		if (this.selModel.selection.record.data.id) {
			Ext.MessageBox.show({
				width:250, title:'למחוק פריט?', msg:'האם ברצונך למחוק יחידה זאת ?', buttons:Ext.MessageBox.YESNO, scope:this,
				fn: function(btn) {if(btn=='yes') {
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=config/config&f=del_expensesgroup&id='+this.getSelectionModel().selection.record.data.id,
						success: function(r,o){
							r=Ext.decode(r.responseText);
							if (!ajRqErrors(r)) {
								// Dump(this.selModel.selection);
								var index = this.selModel.selection.cell[0];
								this.view.getRow(index).style.border='1px solid red';
								Ext.fly(this.view.getRow(index)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.selection.record);}, scope:this });
							}
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				}}
			});
		}
	};
    if (cfg) Ext.apply(this, cfg);
	this.store.load();
};

Ext.extend(ExpensesgroupsGrid, Ext.grid.EditorGridPanel);

if (Ms['config/expensesgroups']) 
Ms['config/expensesgroups'].run = function(cfg){
	var m=Ms['config/expensesgroups'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var  expensesgroupsGrid = new ExpensesgroupsGrid();
		win = Crm.desktop.createWindow({width:300, height:400
			,items:expensesgroupsGrid
		}, m);
	}
		
	win.on('show', function() {
		if (expensesgroupsGrid.store.getCount()>0) expensesgroupsGrid.startEditing(expensesgroupsGrid.store.getCount()-1,0);
	});	
	win.show();
};
