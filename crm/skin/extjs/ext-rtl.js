Ext.override(Ext.form.Field, {
	alignErrorIcon: function(){ this.errorIcon.alignTo(this.el, 'tr-tl', [-2, 0]); },
	focusClass:'fieldFocusCls'
});

Ext.override(Ext.util.JSON, {
	doDecode: function(json){
		try{ return eval("(" + json + ')'); }
		catch(e){ if (console && console.log) console.log('json decode error: '+json); }
	}
});

Ext.override(Ext.grid.EditorGridPanel, {
	enableHdMenu:false
});


Ext.override(Ext.Layer, {
	hideAction : function(){
		this.visible = false;
		if(this.useDisplay === true){
			this.setDisplayed(false);
		}else{
			this.setLeftTop(0,-10000); // negative x in firefox shows scrollbar in RTL
		}
	}
});


//alert( Ext.layout.AnchorLayout );
Ext.override( Ext.layout.FormLayout, {

	setContainer : function(ct){
		Ext.layout.FormLayout.superclass.setContainer.call(this, ct);

		if(ct.labelAlign) ct.addClass('x-form-label-'+ct.labelAlign);

		if(ct.hideLabels){
			this.labelStyle = "display:none";
			this.elementStyle = "padding-left:0;";
			this.labelAdjust = 0;
		}else{
			this.labelSeparator = ct.labelSeparator || this.labelSeparator;
			ct.labelWidth = ct.labelWidth || 100;
			if(typeof ct.labelWidth == 'number'){
				var pad = (typeof ct.labelPad == 'number' ? ct.labelPad : 5);
				if (Ext.isIE) pad=pad-3; // IE float fix
				this.labelAdjust = ct.labelWidth+pad;
				this.labelStyle = "width:"+ct.labelWidth+"px;";
				this.elementStyle = "padding-right:"+(ct.labelWidth+pad)+'px';
			}
			if(ct.labelAlign == 'top'){
				this.labelStyle = "width:auto;";
				this.labelAdjust = 0;
				this.elementStyle = "padding-left:0;";
			}
		}

		if(!this.fieldTpl){
			var t = new Ext.Template(
				'<div class="x-form-item {5}" tabIndex="-1">',
					'<label for="{0}" style="{2}" class="x-form-item-label">{1}{4}</label>',
					'<div class="x-form-element" id="x-form-el-{0}" style="{3}">',
					'</div><div class="{6}"></div>',
				'</div>'
			);
			t.disableFormats = true;
			t.compile();
			Ext.layout.FormLayout.prototype.fieldTpl = t;
		}
	}
});

// hide field labels
Ext.layout.FormLayout.prototype.trackLabels = true;


/*
// Adding a red asterisk to the required form fields
Ext.override(Ext.layout.FormLayout, {
	getTemplateArgs: function (field) {
		var noLabelSep = !field.fieldLabel || field.hideLabel;
		var labelSep = (typeof field.labelSeparator == 'undefined' ? this.labelSeparator : field.labelSeparator);
		if (field.required) labelSep += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
		return {
			id: field.id,
			label: field.fieldLabel,
			labelStyle: field.labelStyle || this.labelStyle || '',
			elementStyle: this.elementStyle || '',
			labelSeparator: noLabelSep ? '' : labelSep,
			itemCls: (field.itemCls || this.container.itemCls || '') + (field.hideLabel ? ' x-hide-label' : ''),
			clearCls: field.clearCls || 'x-form-clear-left'
		};
	}
});
*/