<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

//if ($SVARS['user_type']!=3 AND $SVARS['parent_id']) $group_id=$SVARS['parent_id']; else $group_id=$SVARS['user_id'];
//if (!$group_id) die('{success:false, msg:"group_id error"}');

define('RCMAIL_CHARSET', 'UTF-8');
require_once 'modules/mail_new/inc/main.inc.php';
require_once 'modules/mail_new/inc/utf7.inc.php';
require_once 'modules/mail_new/inc/utf8.class.php';

require_once 'modules/mail_new/inc/rcube_config.php';
require_once 'modules/mail_new/inc/rcube_shared.inc.php';

require_once 'modules/mail_new/inc/rcube_mime_struct.php';
require_once 'modules/mail_new/inc/rcube_message.php';
require_once 'modules/mail_new/inc/rcube_imap_generic.php';
require_once 'modules/mail_new/inc/rcube_imap.php';
require_once 'modules/mail_new/inc/rcmail.php';
require_once 'modules/mail_new/inc/rcube_result_index.php';
require_once 'modules/mail_new/inc/rcube_charset.php';

require_once 'modules/mail_new/inc/washtml.php';
require_once 'modules/mail_new/inc/tnef_decoder.php';
require_once 'modules/mail_new/inc/html.php';
require_once 'modules/mail_new/inc/func.inc.php';
require_once 'modules/mail_new/inc/rcube_string_replacer.php';

// set the PHP timelimit to 3 minutes
set_time_limit(180);

$qur = file_get_contents("../../datafetch.spx");
$mail_accounts = sql2array("SELECT *,AES_DECRYPT(password, $qur) AS password FROM mail_accounts WHERE user_id=".$SVARS['user']['id']."  ORDER BY `primary` DESC, name", 'id');

if (function_exists("f_{$f}")) call_user_func("f_{$f}");
else die('{success:false, msg:"function name error"}');

//————————————————————————————————————————————————————————————————————————————————————
function folder_lng_name($name) {

	$name=str_replace('[Gmail]/דואר יוצא', 'פריטים שנשלחו', $name);
	
	$name=str_replace('[Gmail]/', '', $name);
	
	if     (preg_match('/^Inbox$/i', $name))		return 'דואר נכנס';
	elseif (preg_match('/^(Sent|Sent[- ]Mail|Sent[- ]Items)$/i', $name)) return 'פריטים שנשלחו';
	elseif (preg_match('/^(Infected Items|Spam|Junk|Junk E-mail)$/i', $name)) return 'דואר זבל';
	elseif (preg_match('/^Drafts$/i', $name))		return 'טיוטות';
	elseif (preg_match('/^(Deleted Items|Bin|Trash)$/i', $name)) return 'אשפה';
	return $name;
}

//————————————————————————————————————————————————————————————————————————————————————
function folder_iconCls($name) {
	if ($name=="[Gmail]/דואר יוצא")	return 'mail-sent';
	
	$name=str_replace('[Gmail]/', '', $name);
	// $name=str_replace('INBOX'.$folder_delimiter, '', $name);
	if     (preg_match('/^(INBOX|דואר נכנס)$/i', $name))				return 'mail-inbox';
	elseif (preg_match('/^((Sent|Sent[- ]Mail|Sent[- ]Items)|(פריטים שנשלחו))$/i', $name))return 'mail-sent';
	elseif (preg_match('/^Outbox|(דואר יוצא)$/i', $name))			 return 'mail-outbox';
	elseif (preg_match('/^(Drafts|טיוטות)$/i', $name))	return 'mail-draft';
	elseif (preg_match('/^(Infected Items|Spam|Junk|Junk E-mail|דואר זבל)$/i', $name))	 return 'mail-spam';
	elseif (preg_match('/^((Deleted Items)|Bin|Trash|אשפה|(פריטים שנמחקו))$/i', $name))return 'mail-trash';
	else															 return 'folder';
	//return $name;
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_account(){
	global $mail_accounts, $SVARS, $IMAP;

	$id = intval($_POST['id']);
	
	if(empty($_POST['host']) AND empty($_POST['smtp_host']))
		die ("{success:false, msg:'נא להגיר פרטי שרת!'}");
	
	if(!$id AND empty($_POST['password']))
		die ("{success:false, msg:'נא להכניס סיסמה!'}");
	
	if(!$id AND $mail_accounts){
		foreach($mail_accounts as $account){
			if($account['email'] == $_POST['email']) 
				die ("{success:false, msg:'דוא\"ל ".$_POST['email']." כבר קיים ברשימה.'}");
		}
	}
	
	#handle primary account
	$primary = (isset($_POST['primary']) ? 1 : 0);
	if(!$mail_accounts) 
		$primary = 1;
	elseif($primary)
		runsql("UPDATE mail_accounts SET `primary`=0 WHERE user_id=".$SVARS['user']['id']);
	elseif(count($mail_accounts) > 1){
		$primaryExist = false;
		foreach($mail_accounts as $account){
			if($id AND $account['id'] == $id) continue;
			if($account['primary']) $primaryExist = true;
		}
		$primary = $primaryExist ? 0 : 1;
	}else $primary = 1; 
	
	$_POST['name'] = trim($_POST['name']);
	$qur = file_get_contents("../../datafetch.spx");

	
	if($_POST['host'] AND !empty($_POST['password'])){// Checking connection for income server
		$account = array('host'=>$_POST['host'],'username'=>$_POST['username'], 'password'=>$_POST['password'],'port'=>$_POST['port'],'security'=>($_POST['security'] ? $_POST['security'] : ''));
		imap_connect($account);
		
		//-----------------------------------------------------------------------------
		// --------- b-com server's configuration (folder delimiter) have to be fixed
		if ( preg_match('/b-com/',$account['host']) ) { $get_hierarchy_delimiter = '.'; 
			} else 	$get_hierarchy_delimiter = $IMAP->get_hierarchy_delimiter();
		// ---------
		//$get_hierarchy_delimiter = $IMAP->get_hierarchy_delimiter();
		//-----------------------------------------------------------------------------
		
	}
	
	$sql = ($id ? "UPDATE" : "INSERT INTO")." mail_accounts SET user_id=".$SVARS['user']['id']
		.",name=".quote(trim($_POST['name']))
		.",email=".quote(trim($_POST['email']))
		.",type='imap'"
		.",host=".($_POST['host'] ? quote(trim($_POST['host'])) : "''")
		.",port=".($_POST['port'] ? intval($_POST['port']) : 0)
		.",security=".quote($_POST['security'])
		.",smtp_host=".($_POST['smtp_host'] ? quote(trim($_POST['smtp_host'])) : "''")
		.",smtp_port=".($_POST['smtp_port'] ? intval($_POST['smtp_port']) : 0)
		.",smtp_sec=".quote($_POST['smtp_sec'])
		//.",smtp_auth=".($_POST['smtp_auth'] ? 1 : 0)
		.",listSubscribedOnly=".($_POST['listSubscribedOnly'] ? 1 : 0)
		.",username=".quote(trim($_POST['username']))
		.",last_update=".quote(date('Y-m-d h:m:s'))
		.",replyTo=".quote(trim($_POST['replyTo']))
		.",`primary`=$primary"
		.(!empty($_POST['password']) ? ",folder_delimiter=".quote($get_hierarchy_delimiter).",password=AES_ENCRYPT(".quote($_POST['password']).", $qur)" : '')
		.($id ? " WHERE id=$id" : "");	
	
	
	if(runsql($sql)){
		echo "{success:true}";
	}
	else
		echo "{success:false, msg:'Internal error occured. Try again.'}";
	return;

}

//————————————————————————————————————————————————————————————————————————————————————
function f_get_accounts(){
	global $mail_accounts;
	
	$m_accounts = array();
	// preventing send password to client
	$n = 0;
	if ($mail_accounts) foreach($mail_accounts as $account){
		unset($account['password']);
		$m_accounts[$n] = $account;
		$n++;
	}
	echo '{total:"'.count($mail_accounts).'", data:'.($mail_accounts ? array2json($m_accounts):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
/**
* Delete account and all related files.
*/
function f_delete_account(){
	global $mail_accounts;
	$id = (int)$_POST['id'];

	$sql_acc    = "DELETE FROM mail_accounts WHERE id=$id";
	$sql_folder = "DELETE FROM mail_folders WHERE account_id=$id";
	$sql_cache  = "DELETE FROM mail_cache WHERE account_id=$id";
	$sql_folder_cache  = "DELETE FROM mail_folders_cache WHERE account_id=$id";
	
	if($id AND runsql($sql_cache) AND runsql($sql_folder) AND runsql($sql_folder_cache) AND runsql($sql_acc)){
		unset($mail_accounts[$id]);
		deleteDir("../../temp/mail/$id");
		echo("{success:true}");
	}else echo("{success:false}");
}
/*
* Load list of folders from a server
* return: folder list
*/
function load_server_folders() {
 global $SVARS,$IMAP,$mail_accounts; 
	if (!$mail_accounts) return;
	$n=0;
	$reasignPrimary = false;

	foreach($mail_accounts as $k=>$account) {
		if(empty($account['host'])) continue;
		if(!imap_connect($account,true)){
			if($account['primary']) $reasignPrimary = true;
			unset($mail_accounts[$k]);
			continue;
		}
		
		if($reasignPrimary){ 
			$mail_accounts[$k]['primary'] = 1; 
			$reasignPrimary = false;
		}
		
		if($account['listSubscribedOnly']) $folders[$k] = $IMAP->list_mailboxes();
		else $folders[$k] = $IMAP->list_unsubscribed();
		
		foreach($folders[$k] as $folder){
			$folderList[$n] = array('account_id'=>$account['id'],'folder'=>$folder,'unseen'=>$IMAP->messagecount($folder,'UNSEEN'),'total'=>$IMAP->messagecount($folder, ($IMAP->threading ? 'THREADS':'ALL')));
			$n++;
		}
	}
	return $folderList;
}
//————————————————————————————————————————————————————————————————————————————————————
/*
* Empty folder forever
* Input: account id, folder to empty
*/
function f_empty_folder(){
	global $IMAP, $mail_accounts;
	
	$account_id=intval($_REQUEST['account']);
	$folder = $_REQUEST['folder'];

	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'Account not found.Try again.'}");
	$account = $mail_accounts[$account_id];
	
	imap_connect($account);

	if($IMAP->clear_mailbox($folder)){
		clear_cache('',$account_id,$folder,true);
		echo("{success:true}");
	}
	else
		echo("{success:false, msg:'Request failed.Try again.'}");

}
//————————————————————————————————————————————————————————————————————————————————————
function f_load_folders() {
 global $SVARS, $IMAP, $mail_accounts, $config;
	if (!$mail_accounts) return;
	
	foreach($mail_accounts as $account) $accountIds[] = $account['id'];
	
	if((int)$_REQUEST['refresh'])
		runsql("DELETE FROM mail_folders WHERE account_id IN(".implode(',',$accountIds).")");
	else
		$mail_folders  = sql2array("SELECT * FROM mail_folders WHERE account_id IN(".implode(',',$accountIds).") ORDER BY view");
	
	if(empty($mail_folders)){
		$mail_folders = load_server_folders();
		if($mail_folders)$firstLunch = true;
	}
	
	foreach($mail_accounts as $account) {
		$folder_delimiter = $account['folder_delimiter'];
		if(empty($account['host'])){
			$acc['folderName']	= "<b>".$account['name']."</b><span style='font-size:75%'>(רק שליחה)</span>";
			$acc['text']	= "<b>".$account['name']."</b><span style='font-size:75%'>(רק שליחה)</span>";
			$acc['email'] 	= $account['email'];
			$acc['root_id'] = $account['id'];
			$acc['primary'] = $account['primary'];
			$acc['sendReady'] = empty($account['smtp_host']) ? false : true;
			$acc['iconCls']	= 'mail-folders';
			$acc['leaf']= true;
			$accounts[]=$acc;
			unset($acc);
			continue;
		}

		// we know that gmail contains all folders that we need
		if(preg_match('/gmail/i',$account['host'])) $skipCheck = true; else $skipCheck = false;
		
		$tempfolders = array();
		$acc['folderName']	= "<b>".$account['name']."</b>";
		$acc['email'] = $account['email'];
		$acc['root_id'] = $account['id'];
		$acc['primary'] = $account['primary'];
		$acc['sendReady'] = empty($account['smtp_host']) ? false : true;
		$acc['iconCls']	= 'mail-folders';
		$acc['expanded']= true;
		$trash = false; $sent_mail = false;
		$sentFolderFound = false; $trashFolderFound = false; // Stop search for trash and sentItems folders if already founded
		$n=0;
		
		foreach($mail_folders as $folder) {
			if($folder['account_id'] == $account['id']){
				$wasHere =true; // protection from creating folders on account that not cached

				//$folder_str = utf8_to_windows1255(rcube_charset_convert($folder['folder'], 'UTF7-IMAP'));
				$folder_str = rcube_charset_convert($folder['folder'], 'UTF7-IMAP');
				$name = str_replace('[Gmail]/', '', $folder_str);
				$name = str_replace('INBOX'.$folder_delimiter, '', $name);
				$name = strip_tags($name);
				$name = preg_replace("/&#?[a-z0-9]{2,8};/i","",$name);
				
				if(!$sentFolderFound AND preg_match('/^((Sent|Sent[- ]Mail|Sent[- ]Items)|(פריטים שנשלחו))$/i', $name)) {
					$acc['sentFolder'] = $folder['folder'];
					$sent_mail = true;
					$sentFolderFound = true;
				} elseif(!$trashFolderFound AND preg_match('/^((Deleted Items)|Bin|Trash|אשפה|(פריטים שנמחקו))$/i', $name)){
					$acc['trashFolder'] = $folder['folder'];
					$trash = true;
					$trashFolderFound = true;
				}
				// using 'skipCheck' stupid, but sometimes script create folders when not needed. Should be fixed!
				if($skipCheck) {$sent_mail = true; $trash = true;} 
				
				if ($folder['folder'] != '[Gmail]') {
					$acc['children'][$n]['folderName']	= folder_lng_name($folder_str);
					$acc['children'][$n]['iconCls'] 	= folder_iconCls($folder_str);
					$acc['children'][$n]['unseen']	    = $folder['unseen'];
					$acc['children'][$n]['leaf']    	= true;
					$acc['children'][$n]['account'] 	= $folder['account_id'];
					$acc['children'][$n]['folder']  	= $folder['folder'];
					$csql.=($csql ? ',':'')."(".$folder['account_id'].",".quote($folder['folder']).",".$folder['unseen'].",".$folder['total'].",$n)";
					$n++;
				}
			}
		}
		
		if($wasHere){
			//checking if 'trash' and 'send' folders exists, if not create them 
			if(!$sent_mail){ array_push($tempfolders,'Sent'); $sent_mail = false;}
			if(!$trash)    { array_push($tempfolders,'Trash'); $trash = false; $acc['trashFolder'] = 'Trash';}

			if(!empty($tempfolders)){
				imap_connect($account);
				$imap->set_default_mailboxes($tempfolders);
				$imap->create_default_folders();

				foreach($tempfolders as $tf){
					$acc['children'][$n]['folderName']	= folder_lng_name($tf);
					$acc['children'][$n]['iconCls'] 	= folder_iconCls($tf);
					$acc['children'][$n]['unseen']	    = 0;
					$acc['children'][$n]['leaf']    	= true;
					$acc['children'][$n]['account'] 	= $account['id'];
					$acc['children'][$n]['folder']  	= $tf;
					$csql.=($csql ? ',':'')."($account[id],".quote($tf).",0,0,$n)";
				}
			}
			$wasHere = false;
			}
			
		if ($folder_delimiter) {//Build folder trees
			for ($i = count($acc['children'])-1; $i>=0; $i--) {
				$folder = $acc['children'][$i];
			//	$folder['folder'] = str_replace('[Gmail]/', '', $folder['folder']);
				
				$arr_nestedRealName = explode($folder_delimiter, $folder['folderName']);
				$arr_nestedName = explode($folder_delimiter, $folder['folder']);
				
				$offset = in_array('[Gmail]',$arr_nestedName) ? 1 : 0;
				$num_nested = count($arr_nestedName);
				
				$curr_folder_name = $arr_nestedRealName[$num_nested-$offset-1];
				unset($arr_nestedName[$num_nested-1]);
				
				$num_nested--;
				if ($num_nested-$offset) {
						$impl_out = implode($folder_delimiter,$arr_nestedName);
						$parent_index = 0;
						foreach ($acc['children'] as $k=>$fol) {
							if ($fol['folder'] == $impl_out) {
								$parent_index = $k;
							}
						}
						
						if (!$acc['children'][$parent_index]['children']) { $acc['children'][$parent_index]['children'] = array(); };
						$folder['folderName'] = $curr_folder_name;
						array_push($acc['children'][$parent_index]['children'],$folder);
						$acc['children'][$parent_index]['leaf'] = 0;
						unset($acc['children'][$i]);
					}
		}
	
		if (is_array($acc['children'])){
			foreach($acc['children'] as $key=>$row) $children[]=$row;
			$acc['children'] = $children;
			unset($children);
		 }else{
			$acc['leaf']= true;
			unset($acc['expanded']);
		 }
		
		$accounts[]=$acc;
		unset($acc);		
		}/*else{
			// call to function retriving new not cashed accounts
		}*/
	}
	$accounts[] = array(
		'folderName'=> "<b>דואר יוצא</b>",
		'text'	=> "<b>דוא\"ל יוצא</b>",
		'folder' => 'OUTBOX',
		// 'email' => '',
		// 'root_id' => 0,
		// 'primary' => 0,
		'iconCls' => 'mail-outbox',
		'leaf' => true
	);
	
	if($firstLunch){ 
		runsql("INSERT INTO mail_folders VALUES ".$csql);
	}
		// echo "/*<div dir=ltr align=left><pre>".print_r($accounts,1)."</pre></div>*/";
	echo $accounts ? mail_array2json($accounts) : '[]';
	
}

//
function f_get_quota(){
 global $IMAP, $mail_accounts, $config;
 
	$account_id = intval($_POST['account']);
	if($account_id){
		imap_connect($mail_accounts[$account_id]);
		$quotes = $IMAP->get_quota();
		if($quotes){
			$message = "<b>ניצול ".$quotes['percent']."%</b><br>"
						."נוצל <span dir='ltr'>".intval($quotes['used']/1024)." MB</span> מתוך <span dir='ltr'>".intval($quotes['total']/1024)." MB</span>";
		}else
			$message = "<b>אין מידע.</b>";
	}else
		$message = "<b>Uknown account.</b>";
	
	echo "<div>$message</div>";

}

//————————————————————————————————————————————————————————————————————————————————————
function f_refresh_folders(){
 global $IMAP, $mail_accounts, $config;
 
	if (!$mail_accounts) die("{success:false}");
	
	foreach($mail_accounts as $account) $accountIds[] = $account['id'];
	
	$mail_folders  = sql2array("SELECT * FROM mail_folders WHERE account_id IN(".implode(',',$accountIds).") ORDER BY view");
	
	if (!$mail_folders) die("{success:false}");
	
	foreach($mail_folders as $key=>$folder) {$mail_accounts[$folder['account_id']]['folders'][]=$folder;}
	
	$newMsg=0;
	
	foreach($mail_accounts as $accountId=>$account) {
		if ($account['folders']) {
			imap_connect($account);
			foreach($account['folders'] as $folderId=>$folder) {
				$count = $IMAP->messagecount($folder['folder'], ($IMAP->threading ? 'THREADS':'ALL'));
				$unseen = $IMAP->messagecount($folder['folder'], 'UNSEEN');
				if ($unseen != $mail_accounts[$accountId]['folders'][$folderId]['unseen']) {
					if ($unseen > $mail_accounts[$accountId]['folders'][$folderId]['unseen']) $newMsg+=($unseen - $mail_accounts[$accountId]['folders'][$folderId]['unseen']);
					$folder['unseen']  = $unseen;
					$updated_folders[] = $folder;
					runsql("UPDATE mail_folders SET unseen=$unseen, total=$count WHERE account_id=$folder[account_id] AND folder=".quote($folder['folder']));				}
			}
		}
		if ($accountId==$_REQUEST['account'] AND !$_REQUEST['start']) list($selCount,$ar,$selUnseen) = f_list_folder(1);
	}
	echo "{success:true, treedata:".($updated_folders ? array2json($updated_folders) : '[]').", total:".(int)$selCount.", newMsg:$newMsg, data:".($ar ? array2json($ar) : '[]').", unseen:".$selUnseen."}";
}

//————————————————————————————————————————————————————————————————————————————————————
/*
* Moving specified mails from source folder to destination folder
* Input: account_id, 
*		 from_mbox - source folder
*		 to_mbox   - destination folder
*		 uids      - messages uids array
*/

function f_moveToFolder(){
	global $SVARS, $IMAP, $mail_accounts;
	
	$account_id= intval($_REQUEST['account']);
	$to_mbox = $_REQUEST['toFolder'];
	$from_mbox = $_REQUEST['folder'];
	if ($to_mbox == $from_mbox) die("{success:true}");
	
	#Erase from OUTBOX with tempDir erasing
	if($to_mbox=='erase' AND $from_mbox=='OUTBOX') {
		$sql = "SELECT SQL_CALC_FOUND_ROWS timestart FROM mail_outbox WHERE ".($_REQUEST['uids']=='*'?'':"uid IN ($_REQUEST[uids]) AND ")."user_id=".$SVARS['user']['id'];
		$ar = sql2array($sql);
		if ($ar) foreach($ar as $key=>$row) if (is_dir("../../temp/mail_compose/".$SVARS['user']['id']."/$ar[timestart]")) deleteDir("../../temp/mail_compose/".$SVARS['user']['id']."/$ar[timestart]");
		runsql("DELETE FROM mail_outbox WHERE ".($_REQUEST['uids']=='*'?'':"uid IN ($_REQUEST[uids]) AND ")."user_id=".$SVARS['user']['id']);
		die ("{success:true}");
	}

	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'Account not found.Try again.'}");
	
	$account = $mail_accounts[$account_id];
	$uids =($_REQUEST['uids']=='*' ? '*' : explode(',',$_REQUEST['uids']));
	
	imap_connect($account);
	
	if($to_mbox!='erase' AND !($IMAP->mailbox_exists($to_mbox)))	die("{success:false, msg:'Folder not exist.Refresh your folder list.'}");
	
	if(($to_mbox=='erase' AND $IMAP->delete_message($uids, $from_mbox)) OR $IMAP->move_message($uids, $to_mbox, $from_mbox)){
		clear_cache ($_REQUEST['uids'], $account_id, $from_mbox, ($_REQUEST['uids']=='*'));
		echo("{success:true}");
		
		#Update CACHE from server
		// runsql("UPDATE mail_folders_cache SET folder=".quote($to_mbox)." WHERE ".($_REQUEST['uids']=='*'?'':"uid IN ($_REQUEST[uids]) AND ")."folder=".quote($from_mbox)." AND account_id=".(int)$account_id );
		runsql("DELETE FROM mail_folders_cache WHERE account_id=".(int)$account_id." AND folder=".quote($from_mbox).($_REQUEST['uids']=='*' ? '' : " AND uid IN ($_REQUEST[uids])"));
		
		$_REQUEST['from']='server';
		$_REQUEST['folder']=$to_mbox;
		
		f_list_folder(1);
	}
	else echo("{success:false, msg:'Request failed.Try again.'}");
}

/**
* Remove cached files from database and disk space
* on success return true otherwise false
*/
function clear_cache($uids,$account_id,$folder, $all=false, $move=false){

	if($account_id AND $folder){
		if($all){
			$uids = sql2array("SELECT uid FROM mail_cache WHERE account_id=$account_id AND"
			." cache_key=".quote(base64_encode($folder)),0,'uid',0);
			
			$sql = "DELETE FROM mail_cache WHERE account_id=$account_id AND"
			." cache_key=".quote(base64_encode($folder));
			
			$sql2 = "DELETE FROM mail_folders_cache WHERE account_id=$account_id AND"
			." folder=".quote(base64_encode($folder));

		}else{
			$sql = "DELETE FROM mail_cache WHERE account_id=$account_id AND cache_key=".quote(base64_encode($folder))
				." AND uid IN(".sql_escape($uids).")";
				
			$sql2 = "DELETE FROM mail_folders_cache WHERE account_id=$account_id AND folder=".quote(base64_encode($folder))
				." AND uid IN(".sql_escape($uids).")";
		}
			
		runsql($sql);runsql($sql2);
		
		// clear files
		if($uids){
			$uids = explode(',',$uids);
			foreach($uids as $uid){
				$path = "../../temp/mail/$account_id/".base64_encode($folder)."/_$uid";
				deleteDir($path);
			}
		}
		return true;
	}
	return false;
}

//————————————————————————————————————————————————————————————————————————————————————
function imap_connect($account,$not_die = false){
	global $config, $IMAP, $RCMAIL;
	
	if($IMAP AND $IMAP->conn->connected()){
		if($account['email'] == $IMAP->conn->user){
			return;
		}
		$IMAP->conn->closeConnection();
	}
	
	$IMAP = new rcube_imap();
	$IMAP->connect($account['host'], $account['username'], $account['password'], $account['port'], ($account['security']?$account['security']:null));
		//die("{success:false, msg:'Connection error! Cannot connect to ".$account['host'].". Validate account settings.'}");
	//, $imap_port, $imap_ssl
	if ($IMAP->conn->error){
		if($not_die) return false;
		die("{success:false, msg:'".jsEscape($IMAP->conn->error)."'}");
	}
	// Just old stuff, must be removed
	$RCMAIL = rcmail::get_instance();
	$RCMAIL->imap = $IMAP;
	$RCMAIL->set_imap_prop();
	
	return true;
}

//————————————————————————————————————————————————————————————————————————————————————
function f_create_folder() {
 global $IMAP, $mail_accounts;
	$account_id=(int)$_REQUEST['account'];
	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'Account not found.Try again.'}");
	$account=$mail_accounts[$account_id];

	$pn = $_REQUEST['parent_name'];
	$nn = $_REQUEST['name'];
	
	
	$folder_delimiter = $account['folder_delimiter'];
	 // echo "/* parent_name : $pn ; name : $nn ; folder_delim: $folder_delimiter*/";
	$name = mb_convert_encoding($_REQUEST['name'], "UTF7-IMAP", "UTF8");
	if ($_REQUEST['parent_name']) {
		$name = $_REQUEST['parent_name'].$folder_delimiter.$name;
	} else $name = $name;	
	
	if (!$name) die("{success:false, msg:'No folder name.'}");
	
	imap_connect($account);
	
	$result = $IMAP->create_mailbox($name, true);
	
	if ($result) {
		runsql("INSERT INTO mail_folders SET account_id=$account_id, view=99, folder=".quote($name));
		die("{success:true}"); 
	}	else die("{success:false, msg:'Creating folder error.'}");
}
//————————————————————————————————————————————————————————————————————————————————————
function f_rename_folder() {
 global $IMAP, $mail_accounts;
	$account_id=(int)$_REQUEST['account'];
	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'Account not found.Try again.'}");
	$account=$mail_accounts[$account_id];
	$folder_delimiter = $account['folder_delimiter'];

	if (!$_REQUEST['old_name'] || !$folder_delimiter || !$_REQUEST['name']) die("{succes:false, msg:'Folder rename failed!'");

	$old_name = $_REQUEST['old_name'];
	$old_name_arr = explode($folder_delimiter,$old_name);
	$new_name =  mb_convert_encoding($_REQUEST['name'], "UTF7-IMAP", "UTF8");
	$folder_level = count($old_name_arr) - 1;
	$old_name_arr[$folder_level] = $new_name;
	$new_name = implode($folder_delimiter, $old_name_arr);
	imap_connect($account);
	$result = $IMAP->rename_mailbox($old_name, $new_name);
	if ($result) {
		 runsql("UPDATE mail_folders SET folder=".quote($new_name)." WHERE account_id=$account_id AND folder=".quote($name_to_change));
	} else die("{success:false, msg:'Rename folder error.'}");
	
	die("{success:true}"); 
}
//————————————————————————————————————————————————————————————————————————————————————
function f_delete_folder() {
 global $IMAP, $mail_accounts;
	$account_id=(int)$_REQUEST['account'];
	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'Account not found.Try again.'}");
	$account=$mail_accounts[$account_id];
	
	$name=$_REQUEST['name'];
	if (!$name) die("{success:false, msg:'No folder name.'}");
	
	imap_connect($account);
	
	// $name = mb_convert_encoding($name, "UTF7-IMAP", "UTF8");

	if ($_REQUEST['trashFolder'] AND $IMAP->messagecount($name)) {
		if(!($IMAP->mailbox_exists($_REQUEST['trashFolder'])))	die("{success:false, msg:'trashFolder not exist.Refresh your folder list.'}");
		if(!($IMAP->move_message('*', $_REQUEST['trashFolder'], $name))) die("{success:false, msg:'Transfer to trashFolder failed.'}");
	}
	
	$result = $IMAP->delete_mailbox($name);
	clear_cache ('*', $account_id, $name, true);
	
	if ($result) {
		runsql("DELETE FROM mail_folders WHERE account_id=$account_id AND folder='$name'");
		die("{success:true}"); 
	}	else die("{success:false, msg:'Delete folder error.'}");
}
	
//————————————————————————————————————————————————————————————————————————————————————
function f_list_folder($return=0) {
 global $IMAP, $SVARS, $mail_accounts;
	
	$folder=$_REQUEST['folder'];
	if (!$folder) $_POST['folder']='INBOX';
	
	if (!$folder) $folder='INBOX';
	
	$_REQUEST['start'] = (int)$_REQUEST['start'] ? (int)$_REQUEST['start'] : 0 ;
	$_REQUEST['limit'] = (int)$_REQUEST['limit'] ? (int)$_REQUEST['limit'] : 25;
	
	if ($folder=='OUTBOX') {
		$sql = "SELECT SQL_CALC_FOUND_ROWS *, 1 AS seen FROM mail_outbox WHERE user_id=".$SVARS['user']['id']." ORDER BY timestart DESC LIMIT $_REQUEST[start],$_REQUEST[limit]";
		
		$ar = sql2array($sql);
		
		$count=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
		
		if ($ar) {
			foreach($ar as $k=>$r) {
				$ar[$k]['from'] = unserialize ($r['from']);
				$ar[$k]['attachments'] = unserialize ($r['attachments']);
				$ar[$k]['date'] = date('d/m/Y H:i',$r['timestart']);
			}
			// array_walk_recursive($ar, 'utfConvert');
		}
	
		echo '{"total":"'.$count.'","data":'.($ar ? array2json($ar) : '[]').',"unseen":"0"}';
		exit;		
	
	}
	
	$account_id=(int)$_REQUEST['account'];
	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'Account not found.Try again.'}");
	$account=$mail_accounts[$account_id];
	
	if ($_REQUEST['q'] OR (!empty($_POST['statusFilter']) AND $_POST['statusFilter'] != 'ALL')) {# Search from Server
		imap_connect($account);
		// include task specific functions
		if (is_file($incfile = 'modules/mail_new/actions/search.inc'))
		  include_once $incfile;

		$searchSet = $IMAP->get_search_set();
		$headers_ids = $searchSet[1];
		// Be always updated with count unseen messages
		$unseen = $IMAP->messagecount($folder, 'UNSEEN');
		
		if ($headers_ids) {
			$count = count($headers_ids);
			$headers_ids = array_slice($headers_ids, $_REQUEST['start'], $_REQUEST['limit']);
			foreach ($headers_ids as $k=>$header_id) {
					$v = $IMAP->get_headers($header_id, $folder,false,false);
					
					$msg['id']		= $v->id;
					$msg['uid']		= $v->uid;
					$msg['date']	= date('d/m/Y H:i', strtotime($v->date));
					$msg['size']	= $v->size;
					$msg['subject']	= ($v->subject ? $IMAP->decode_header($v->subject) : '(no subject)');

					$msg['messageId']= $v->messageId;
					if ($v->priority) $msg['priority']=$v->priority;
					
					// $msg['from'] = array($v->from,'');
					$v->from = $IMAP->decode_address_list($v->from);
					// print_ar($v->from,'from');exit;
					if ($v->from[1]) $msg['from'] = array($v->from[1]['name'], $v->from[1]['mailto']);
					// echo "/*search<div dir=ltr align=left><pre>".print_r($headers_ids,1)."</pre></div>*/";
					//$msg['to'] = $IMAP->decode_header($v->to);
					$to	= $IMAP->decode_address_list($v->to);
					$msg['to'] = $to[1]['name'] ? $to[1]['name'] : $to[1]['string'];
					// $msg['to'] = array($v->from[1]['name'], $v->from[1]['mailto']);
					// if (is_array($v->to))	foreach($v->to as $val) $msg['to'][] = array($val['name'], $val['mailto']);
					// if (is_array($v->cc))	foreach($v->cc as $val) $msg['cc'][] = array($val['name'], $val['mailto']);
					
					if (is_array($v->flags)) foreach($v->flags as $val) {
						if (strtolower($val)=='seen')		$msg['seen']=1;
						if (strtolower($val)=='flagged')	$msg['flagged']=1;
						if (strtolower($val)=='answered')	$msg['answered']=1;
						if (strtolower($val)=='deleted')	$msg['deleted']=1;
					}
					# attachments
					if ($v->body_structure->parts) foreach($v->body_structure->parts as $part) if ($part->disposition=='attachment' && $part->filename) $msg['attachments'][]=$part->filename;
					
					
					//array_walk_recursive($msg, 'utfConvert');
					$ar[$k]=$msg;
					unset($msg);					
			}
		}
	
	} elseif ($_REQUEST['from'] == 'server' OR $_REQUEST['start'] > 25) {# Download from Server
		imap_connect($account);
		$count = $IMAP->messagecount($folder, ($IMAP->threading ? 'THREADS':'ALL'));
		$IMAP->page_size=($_REQUEST['start']>25 ? 25 : 50);//$_REQUEST['limit'];
		
		$page = $_REQUEST['start'] ? (int)floor(1 + $_REQUEST['start']/$IMAP->page_size) : 1;

		if ($count){

			$sort_field = $_REQUEST['sort'] ? $_REQUEST['sort'] : (strstr($account['host'],'imap.gmail') ? 'DATE' : NULL);
			$sort_dir = $_REQUEST['dir'] ? $_REQUEST['dir'] : (strstr($account['host'],'imap.gmail') ? 'DESC' : NULL);
			
			$unseen = $IMAP->messagecount($folder, 'UNSEEN');
			
			$headers = $IMAP->list_headers($folder, $page, $sort_field, $sort_dir);
			
			foreach($headers as $k=>$v) {
				$msg['id']		= $v->id;
				$msg['uid']		= $v->uid;
				$msg['date']	= date('d/m/Y H:i', strtotime($v->date));
				$msg['size']	= $v->size;
				//$msg['subject']	= ($v->subject ? $v->subject : '(no subject)');
				$msg['subject']	= ($v->subject ? $IMAP->decode_header($v->subject) : '(no subject)');

				$msg['messageId']= $v->messageId;
				if ($v->priority) $msg['priority']=$v->priority;
				if ($v->from[1])	$msg['from'] = array($v->from[1]['name'], $v->from[1]['mailto']);
				
				$to	= $IMAP->decode_address_list($v->to);
				$msg['to'] = $to[1]['name'] ? $to[1]['name'] : $to[1]['string'];
				// if (is_array($v->to))	foreach($v->to as $val) $msg['to'][] = array($val['name'], $val['mailto']);
				// if (is_array($v->cc))	foreach($v->cc as $val) $msg['cc'][] = array($val['name'], $val['mailto']);
				
				if (is_array($v->flags)) foreach($v->flags as $val) {
					if (strtolower($val)=='seen')		$msg['seen']=1;
					if (strtolower($val)=='flagged')	$msg['flagged']=1;
					if (strtolower($val)=='answered')	$msg['answered']=1;
					if (strtolower($val)=='deleted')	$msg['deleted']=1;
				}
				# attachments
				if ($v->body_structure->parts) foreach($v->body_structure->parts as $part) if ($part->disposition=='attachment' && $part->filename) $msg['attachments'][]=$part->filename;
				
				$ins_ar[] = "($account_id,".quote($folder).",'$msg[uid]','$msg[size]','$msg[seen]','$msg[flagged]','$msg[answered]','$msg[deleted]','$msg[priority]',".quote($msg['subject']).",'$msg[messageId]',".quote(serialize($msg['from'])).",".quote($msg['to']).",".quote(serialize($msg['attachments'])).",".quote(str2time($msg['date'],'Y-m-d H:i:s')).")";
				
				$ar[$k]=$msg;
				unset($msg);
				
			}
			
			//if ($ar) array_walk_recursive($ar, 'utfConvert');
		}
		$unseen = $unseen ? $unseen : 0;
		
		if ($page==1)  {
			runsql("UPDATE mail_folders SET unseen=".$unseen.", total=".$count." WHERE account_id=".(int)$account_id." AND folder=".quote($folder));
		
		runsql("DELETE FROM mail_folders_cache WHERE account_id=".(int)$account_id." AND folder=".quote($folder));
		
			if ($ins_ar) runsql("INSERT INTO mail_folders_cache (account_id,folder,uid,size,seen,flagged,answered,deleted,priority,subject,messageId,`from`,`to`,attachments,date) VALUES ".implode(',',$ins_ar));
		}
	
		if ($ar) $ar=array_slice($ar, 0, 25);
	} else { #Load fron CACHE
	
		$sql = "SELECT SQL_CALC_FOUND_ROWS uid,size,seen,flagged,answered,deleted,priority,subject,messageId,`from`,`to`,attachments,date FROM mail_folders_cache WHERE account_id=".(int)$account_id." AND folder=".quote($folder)." ORDER BY date DESC LIMIT $_REQUEST[start],$_REQUEST[limit]";
		$ar = sql2array($sql);
		$counters = sql2array("SELECT unseen, total FROM mail_folders WHERE account_id=".(int)$account_id." AND folder=".quote($folder)." LIMIT 1",'','',1);
		
		if ($ar) {
			foreach($ar as $k=>$r) {
				$ar[$k]['from'] = unserialize ($r['from']);
				$ar[$k]['attachments'] = unserialize ($r['attachments']);
				$ar[$k]['date'] = str2time($r['date'],'d/m/Y H:i');
			}
		//	array_walk_recursive($ar, 'utfConvert');
		}else{// try load from server
			$_REQUEST['from'] = 'server';
			f_list_folder();
			return;;
		}
	
		if ($return) return array($counters['total'], $ar, $counters['unseen']);
		else echo '{"total":"'.$counters['total'].'","data":'.($ar ? array2json($ar) : '[]').',"unseen":"'.$counters['unseen'].'"}';
		exit;
	}

	if ($return) return array($count, $ar, $unseen);
	else echo '{"total":"'.$count.'","data":'.($ar ? array2json($ar) : '[]').',"unseen":"'.$unseen.'"}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_load_msg() {
 global $mail_accounts;
	
	$uid=(int)$_REQUEST['uid'];
	$account_id=(int)$_REQUEST['account'];
	
	if (!$_REQUEST['folder']) $_REQUEST['folder']='INBOX';
	
	$msg = load_msg($uid,$account_id,$_REQUEST['folder']);
	// unset($msg['html']);
	if($msg)
		echo array2json($msg);
	else
		die("{success:false, msg:'".jsEscape('Message not found.')."'}");
}
//————————————————————————————————————————————————————————————————————————————————————
function f_load_msg_html() {
 global $mail_accounts;
	
	$uid=(int)$_REQUEST['uid'];
	$account_id=(int)$_REQUEST['account'];
	
	if (!$_REQUEST['folder']) $_REQUEST['folder']='INBOX';
	$msg = load_msg($uid,$account_id,$_REQUEST['folder']);
	if($msg)
		echo '<body><div style="padding:10px">'.$msg['html'].'</div></body>';//array2json($msg);
	else
		die("{success:false, msg:'".jsEscape('Message not found.')."'}");

}
//————————————————————————————————————————————————————————————————————————————————————
function save_attachment($path,$uid,$account_id, $folder){
	global $IMAP, $mail_accounts;
	
	$account = $mail_accounts[$account_id];
	imap_connect($account);
	
	$IMAP->select_mailbox($folder);
	$MESSAGE = new rcube_message($uid, $account_id);
	
	if($MESSAGE->attachments){
		if (!is_dir($path)) {
			mkdir($path, 0777,true);			
		}
		foreach($MESSAGE->attachments as $att) {
			if (!is_file("$path/{$file}") OR filesize("$path/{$file}")!=$att->size) {
//				$file = utf8_to_windows1255($att->filename);
				$file = $att->filename;
				$fp=fopen("$path/{$file}", 'wb');
				$MESSAGE->get_part_content($att->mime_id, $fp);
				fclose($fp);
			}
		}
	}
}

//————————————————————————————————————————————————————————————————————————————————————
function load_msg($uid, $account_id, $folder, $dirty = false){
	global $IMAP, $SVARS, $mail_accounts;
	
	if ($folder=='OUTBOX') {
		$outboxMsg = sql2array("SELECT *, message AS html FROM mail_outbox WHERE uid=$uid LIMIT 1",'','',1);
		$outboxMsg['from'] = unserialize($outboxMsg['from']);
		$outboxMsg['to'] = array(array('name'=>'','mailto'=>$outboxMsg['to']));//"[{'name':'','mailto':'slava@b-com.co.il','string':'slava@b-com.co.il'}]";
		if ($outboxMsg['attachments']) {
			foreach(unserialize($outboxMsg['attachments']) as $k=>$f) $attachments[] = array(
				'pid'=>"$outboxMsg[timestart]/$f",
				'name'=>$f, 
				'size'=>filesize("../../temp/mail_compose/".$SVARS['user']['id']."/$outboxMsg[timestart]/".$f)
			);
		}
		$outboxMsg['attachments'] = $attachments;
		return $outboxMsg;
	}
	
	$cachedMsg = sql2array("SELECT * FROM mail_cache WHERE account_id=$account_id AND cache_key=".quote(base64_encode($folder))." AND uid=$uid LIMIT 1",'','',1);
	
	$account=$mail_accounts[$account_id];
	if($cachedMsg){
		$msg['subject']= $cachedMsg['subject'];
		$msg['to']     = unserialize($cachedMsg['to']);
		$msg['from']   = $cachedMsg['from'];
		$msg['cc']     = unserialize($cachedMsg['cc']);
		$msg['replyto']= unserialize($cachedMsg['replyto']);
		$msg['sent']   = $cachedMsg['sent'];
		$msg['html']   = $cachedMsg['data'];
		$attObj        = unserialize($cachedMsg['attachments']);
		if($attObj) foreach($attObj as $att) {
			$msg['attachments'][]=array('account'=>$account_id, 'pid'=>$att['pid'], 'name'=>$att['name'], 'size'=>$att['size']);
		}
		$dirtyMsg = $msg;
	//	array_walk_recursive($msg, 'utfConvert');
		
		$folder_cache = sql2array("SELECT account_id, folder, uid, seen FROM mail_folders_cache 
			WHERE account_id=$account_id AND folder=".quote($folder)." AND uid = $uid LIMIT 1",'','',1);
		if(!$folder_cache['seen']){
			imap_connect($account);
			$IMAP->select_mailbox($folder);
			$MESSAGE = new rcube_message($uid, $account_id);
			if ($MESSAGE->headers){// Set message flag to SEEN
			if($MESSAGE->headers->flags){
				foreach($MESSAGE->headers->flags as $flag){
					if($flag == 'Seen'){
						$isSeen=true;
						break;
					}
				}
			}
			if(!$isSeen)$IMAP->set_flag($uid,'SEEN',$folder);
			}// End set message flag to SEEN
			// Flag seen updated
			runsql("UPDATE mail_folders_cache SET seen=1 WHERE account_id=$account_id AND folder=".quote($folder)." AND uid = $uid LIMIT 1");
		}
	}else{
		imap_connect($account);
		$IMAP->select_mailbox($folder);

		$MESSAGE = new rcube_message($uid, $account_id);
		if (!$MESSAGE->headers) return 0;

		$msg['subject']	= $MESSAGE->subject;
		$msg['from'] = $MESSAGE->from;

		if ($MESSAGE->to) $msg['to'] = array_values($MESSAGE->to);
		if ($MESSAGE->cc) $msg['cc'] = array_values($MESSAGE->cc);
		if ($MESSAGE->replyto) $msg['replyto'] = array_values($MESSAGE->replyto);
		
		// if ($MESSAGE->headers->timestamp) $msg['date'] = date('d/m/Y H:i', $MESSAGE->headers->timestamp);
		// if ($MESSAGE->headers->internaldate) $msg['sent'] = date('d/m/Y H:i', strtotime($MESSAGE->headers->internaldate));
		if ($MESSAGE->headers->date) $msg['sent'] = date('d/m/Y H:i', strtotime($MESSAGE->headers->date));
		if ($MESSAGE->attachments) foreach($MESSAGE->attachments as $att) {
			$msg['attachments'][]=array('account'=>$account_id,'pid'=>$att->mime_id, 'name'=>$att->filename, 'size'=>round($att->size/1.37) );
		}
		// Enable unsafe content
		$MESSAGE->set_safe(false);
		/********************/
		$msg['html'] = rcmail_message_body($MESSAGE);
		
		// Temporary. Need to test rcmail_message_body function
		// if ($msg['html']=='<div id="rcmailMsgBody"></div>') $msg['html'] = $IMAP->get_body($uid);
		$dirtyMsg = $msg;
//		array_walk_recursive($msg, 'utfConvert');
		
		// Set message flag to SEEN
		if($MESSAGE->headers->flags){
			foreach($MESSAGE->headers->flags as $flag){
				if($flag == 'Seen'){
					$isSeen=true;
					break;
				}
			}
		}
		if(!$isSeen)$IMAP->set_flag($uid,'SEEN',$folder);
		// End set message flag to SEEN

		if($MESSAGE->inline_parts){
			$path="../../temp/mail/$account_id";
			if ($MESSAGE->inline_parts AND !is_dir($path)) {
				$path.= "/".base64_encode($folder)."/_".$uid;
				mkdir($path, 0777,true);			
			}else{
				$path.= "/".base64_encode($folder);
				if(!is_dir($path)){
					$path.="/_".$uid;
					mkdir($path, 0777,true);
				}else{
					$path.="/_".$uid; 
					if(!is_dir($path))
						mkdir($path, 0777);
				}
			}
			$path="../../temp/mail/$account_id/".base64_encode($folder)."/_".$uid;
			foreach($MESSAGE->inline_parts as $part) {
					$fp=fopen("$path/{$part->mime_id}.{$part->ctype_secondary}", 'wb');
					if($fp){
						$MESSAGE->get_part_content($part->mime_id, $fp);
						fclose($fp);
					}
				}
		}
		//temp cache message
		runsql("INSERT IGNORE INTO mail_cache SET uid=".$uid
				.",account_id=".$account_id
				.",cache_key=".quote(base64_encode($folder))
				.",subject=".quote($dirtyMsg['subject'])
				.",`from`=".quote($dirtyMsg['from'])
				.",replyTo=".quote(serialize($dirtyMsg['replyto']))
				.",`to`=".quote(serialize($dirtyMsg['to']))
				.",cc=".quote($dirtyMsg['cc'] ? serialize($dirtyMsg['cc']) : NULL)
				.",sent=".quote(str2time($dirtyMsg['sent'],'Y-m-d H:i'))
				.",attachments=".quote($dirtyMsg['attachments'] ? serialize($dirtyMsg['attachments']) : NULL)
				.",data=".quote($dirtyMsg['html'] ? $dirtyMsg['html'] : NULL));
	}
	if($dirty)
		return $dirtyMsg;
	return $msg;
}
//————————————————————————————————————————————————————————————————————————————————————
function f_change_flag(){
	global $IMAP, $mail_accounts;
	
	$uids = explode(',',$_REQUEST['uids']);
	$account_id=(int)$_REQUEST['account'];

	if($uids AND $account_id AND $_REQUEST['folder']){
		$account=$mail_accounts[$account_id];
		foreach($uids as $uid){
			imap_connect($account);
			if(!$IMAP->set_flag($uid,$_REQUEST['flag'],$_REQUEST['folder']))
				die("{success:false, msg:'Request failed.Try again.'}");
		}
		echo("{success:true}");
	}else
		echo "{success:false, msg:'Request failed. Try again.'}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_email_list() {
 global $SVARS;
 	
 	if($_POST['valuesqry'] == 'false'){
 		echo '{success:true, total:0, data:[]}';
 		return;
 	}
 	
 	$types = array(1=>'בית',2=>'עבודה',0=>'אחר');
 	$userTYpes = array('salesagent'=>'עובד', 'agent'=>'סוכן', 'clerk'=>'פקיד/ה', 'dispatcher'=>'נציג שרות', 'supplier'=>'ספק');
 		
 	$user_id = $SVARS['user']['id'];
	$start=(int)$_POST['start'];
 	$limit=($_POST['limit'] ? (int)$_POST['limit'] : 10);
 	
	if ($_POST['query']) $_POST['query']=trim(sql_escape($_POST['query']));

	if ($_POST['query']) {
		$where_agent_search = " AND ".make_where_search('fname,lname,email,email1,email2',$_POST['query']);
	}
	
	$union = " UNION "
		." SELECT fullname AS name, email, 'supplier','', '', '', ''"
		." FROM suppliers"
		." WHERE ".($_POST['query'] ? make_where_search('fullname,email',$_POST['query'])." AND " : '')." isDeleted = 0";
	
	if($SVARS['user']['isAdmin']){
		$where = '';
	}elseif($SVARS['user']['status'] == 'clerk' OR $SVARS['user']['status'] == 'agent'){
		$where = "(id = ".$SVARS['user']['agent_id']." OR agent = ".$SVARS['user']['agent_id'].") AND";
		$union = '';
	}else{
		$where = "(((attendant = $user_id OR supervisor = $user_id) AND status = 'agent') OR status = 'salesagent' OR status = 'dispatcher') AND";
	}
	
	$sql="SELECT TRIM(CONCAT(fname,' ',lname)) AS name,status,email,email_type1,email1,email_type2,email2"
		." FROM users"
		." WHERE ".$where
		." email <> '' $where_agent_search AND isDeleted = 0"
		.$union." LIMIT 0,".($start+$limit);
	
	$agents_ar=sql2array($sql);
	if($agents_ar) foreach($agents_ar as $k=>$r){
		if($r['email']  AND  (strstr($r['email'], $_POST['query']) OR strstr($r['name'], $_POST['query'])))
			$emails[]=array('name'=>'<font style="color:blue">,'.$userTYpes[$r['status']].': '.$r['name'].' (עיקרי) </font>','email'=>$r['email']);
		if($r['email1'] AND (strstr($r['email1'], $_POST['query']) OR strstr($r['name'], $_POST['query'])))
			 $emails[]=array('name'=>'<font style="color:blue">'.$userTYpes[$r['status']].': '.$r['name'].' ('.$types[$r['email_type1']].') </font>','email'=>$r['email1']);
		if($r['email2'] AND (strstr($r['email2'], $_POST['query']) OR strstr($r['name'], $_POST['query'])))
			 $emails[]=array('name'=>'<font style="color:blue">'.$userTYpes[$r['status']].': '.$r['name'].' ('.$types[$r['email_type2']].') </font>','email'=>$r['email2']);
	}
		
	$count=count($emails);
	
	if (is_array($emails)) {
		$emails=array_slice($emails, $start, $limit);
		echo '{success:true, total:'.$count.', data:'.array2json($emails).'}';
	}else echo '{success:true, total:0, data:[]}';
}
/*
* Get account in witch sended email will be saved
* Return: account
*/
function getMailSaveAccount($account){
	global $mail_accounts;
	
	if(empty($account['host'])){
		foreach($mail_accounts as $inner_account){
			if( $account['id'] != $inner_account['id'] AND $account['email'] == $inner_account['replyTo']){
				$save_account = $inner_account;
				break;
			}
		}
	}
	if(empty($save_account)) $save_account = $account;
	$mail_folders  = sql2array("SELECT folder FROM mail_folders WHERE account_id =".$save_account['id'],'','folder');
	//$mail_folders = load_server_folders();
	if(!empty($mail_folders)){
		foreach($mail_folders as $folder) {
//			$folder_str = utf8_to_windows1255(rcube_charset_convert($folder, 'UTF7-IMAP'));
			$folder_str = rcube_charset_convert($folder, 'UTF7-IMAP');
			$name=str_replace('[Gmail]/', '', $folder_str);
			$name=str_replace('INBOX.', '', $folder_str);
			if(preg_match('/^(Sent|Sent[- ]Mail)|(פריטים שנשלחו)$/i', $name)) {
				$save_account['sentFolder'] = $folder;
			}
		}
	}
	return $save_account;
}
//————————————————————————————————————————————————————————————————————————————————————
function f_send_mail(){
	global $mail_accounts, $SVARS, $CFG, $IMAP;
 	if (!(int)$_POST['timestart']) die('{success:false}');
	
 	require_once 'modules/mail_new/mail/swift_sendmail.php';
 	
	$acc_id = intval($_POST['account']);
	$account_id = intval($_POST['msg_account']);
	$senderName = $SVARS['user']['name'] ? $SVARS['user']['name'] : '';
	
	$to = $_POST['mailto'];
	$cfg['cc'] = $_POST['mailcc'];
	$cfg['bcc'] = $_POST['mailbcc'];
	$subject = $_POST['subject'];
	$cfg['importance'] = (int)$_POST['mail_importance'];
	
	$_POST['message'] = str_replace('ג€‹','',$_POST['message']);
	$_POST['message'] = str_replace('lang="HE"','',$_POST['message']);
	$message = trim($_POST['message']); // UTF-8
	
	if ($message) $message="<div dir=rtl>$message</div>";
	
	//Get primary account
	if ($mail_accounts AND !$acc_id){
		foreach($mail_accounts as $account){ 
			if ($account['primary']){ 
				$acc_id = $account['id'];
				break;
			}
		}
		if(empty($acc_id)){// take first
			$acc_id = $mail_accounts[0];
		}
	}
		
	if($acc_id){
		$account = $mail_accounts[$acc_id];
		$isNoSmtp_host = empty($account['smtp_host']) ? true : false;
	}else{
		$isNoSmtp_host = true;
		$acc_id = 0;
	}
	if($isNoSmtp_host){
		$cfg = get_system_mail_conf();
	}else{
		$cfg['from'] =  array($account['email'] => $senderName);
		$cfg['host'] = $account['smtp_host'];
		$cfg['security'] = $account['smtp_sec'];
		$cfg['port'] =  $account['smtp_port'];
		$cfg['user'] =  $account['username'];
		$cfg['pass'] =  $account['password'];
		$cfg['replyto'] = array(($account['replyTo'] ? $account['replyTo'] : $account['email']) => $senderName);
	}
	
	$size = strlen($message);
	
	// get Attachments
	foreach (glob("../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]/*") as $file) {
		$cfg['files'][] = array(0=>$file);
		$size+=filesize($file); 
		$attachments[]= base_name($file);
		$pid++;
		$attachment_data[$pid]='{"account":'.$acc_id.',"pid":'.$pid.',"name":"'.base_name($file).'","size":'.filesize($file).'}';
	}
	
	#Save message in OUTBOX
	runsql("INSERT INTO mail_outbox SET user_id=".$SVARS['user']['id'].", account=$acc_id, timestart=$_POST[timestart], `from`=".quote(serialize(array($SVARS['user']['name'],$account['email']))).", `to`=".quote($to)
		.", subject=".quote($subject)
		.", message=".quote($message)
		.", size=$size"
		.($attachments ? ", attachments=".quote(serialize($attachments)):''));
	
	if($MIME = swift_send_mail($to, $subject, $message, $cfg)){
		#Remove sent mail from OUTBOX
		runsql("DELETE FROM mail_outbox WHERE user_id=".$SVARS['user']['id']." AND account=$acc_id AND timestart=$_POST[timestart]");
		
		echo "/* f_change_flag: ";
		#Mark answered message
		if ($_POST['msg_account'] AND $_POST['uids']) {
			$_REQUEST['account']=$_POST['msg_account'];
			$_REQUEST['flag']='ANSWERED';
			f_change_flag();
		}
		echo "*/";

		if(!$isNoSmtp_host){
			//Gmail account save message automaticly
			if ($account['smtp_host'] != 'smtp.gmail.com' AND $_POST['sentFolder']){ //Save message in sentFolder
				$sentFolder = $_POST['sentFolder'];
				$saveIn = $account;
			}else{
				$saveIn = getMailSaveAccount($account);
				$sentFolder = $saveIn['sentFolder'];
			}
			if($sentFolder){
				imap_connect($saveIn);
				$IMAP->save_message($sentFolder, $MIME);
			}
		}
		
		deleteDir("../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]");
		echo("{success:true}");
	} else{
		$responce_error = $GLOBALS['send_mail_result']['message'];//." Message not delivered to:[".implode(';',$GLOBALS['send_mail_result']['failures'])."]";
		echo("{success:false, msg:".(empty($responce_error) ? "'Sending failed!'" : array2json($responce_error))."}");
	}
}

//————————————————————————————————————————————————————————————————————————————————————
function utf2entity($s) {

	if (function_exists('mb_convert_encoding')) return mb_convert_encoding($s, 'HTML-ENTITIES', 'UTF-8');

    $tgt = '';
    for($i=0,$len=strlen($s); $i<$len; $i++) {
        $c = $s[$i];
        $x = ord($c);
        if ($x < 0x80) { // 1-byte symbol
            $tgt .= $c;
            continue;
        } elseif (($x & 0xC0) == 0xC0) { // (n+1)-byte symbol
            $n = 1;
            while ( ($x & (0x40 >> $n)) > 0) $n++;
            $code = $x & (0x3F >> $n);
            for ($k=1; $k<=$n; $k++) {
                $y = ord($s[$i+$k]) & 0x3F;
                $code = ($code << 6) + $y;
            }
            $i += $n;
            $tgt .= '&#x'.dechex($code).';';
        } else {
            $tgt .= '?'; // takogo ne doljno bit'
        }
    }
    return $tgt;
}
//————————————————————————————————————————————————————————————————————————————————————
/**
* LIMIT cache per account
* Here you can specify how many mails cache per account.
* Change cacheLimit varible to desireble number of cached mails.
* Function also delete files from hard disk if found
*/
function f_update_cache(){
	global $mail_accounts;
	$cacheLimit = 100; // per account
	if(!$mail_accounts) return;
	foreach($mail_accounts as $account){
		$deleteQty = sql2array("SELECT IF(COUNT(account_id) > $cacheLimit, COUNT(account_id) - $cacheLimit,0) AS lmt FROM mail_cache WHERE account_id =".$account['id'],'','',1);
		if(!$deleteQty['lmt']) continue;
		$toDelete = sql2array("SELECT uid, account_id,cache_key FROM mail_cache WHERE account_id=".$account['id']." ORDER BY created LIMIT ".$deleteQty['lmt']);
		runsql("DELETE FROM mail_cache ORDER BY created LIMIT ".$deleteQty['lmt']);
		foreach($toDelete as $del){
			$path = "../../temp/mail/".$del['account_id']."/".$del['cache_key']."/_".$del['uid'];
			deleteDir($path);
		}
	}
}
//————————————————————————————————————————————————————————————————————————————————————
/*
* Copy files and non-empty directories
* src - source folder
* dst - destination folder
*/ 
function copy_cache($src, $dst, $deleteSource = false) {
  if (is_dir($dst)) deleteDir($dst);
  if (is_dir($src)) {
    mkdir($dst, 0777,true);
    $files = scandir($src);
    foreach ($files as $file)
    if ($file != "." && $file != "..") copy_cache("$src/$file", "$dst/$file");
  }
  else if (file_exists($src)) @copy($src, $dst);
  
  if($deleteSource) deleteDir($src);
}
//————————————————————————————————————————————————————————————————————————————————————
function f_get_attachments() {
 global $SVARS;
 
 	if (!(int)$_POST['timestart']) die('{success:false}');

 
	if ($_REQUEST['account'] AND $_REQUEST['uid']) {
		$path="../temp/mail/$_REQUEST[account]/".base64_encode($_REQUEST['folder'])."/_".$_REQUEST['uid']."/attachments";
		save_attachment($path,$_REQUEST['uid'],$_REQUEST['account'],$_REQUEST['folder']);
		
		copy_cache($path, "../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]");
	}
 
	foreach (glob("../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]/*") as $filename) {
		$size = filesize($filename);
		$sizeKb=($size > 1024000 ? number_format($size/1024) : round($size/1024,2) ).' kB';
		$files[] = array('file_name'=>str_replace("../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]/",'',$filename), 'size'=>$sizeKb);
	}
	
	echo "{success:true, data:".($files ? array2json($files):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_del_attachments() {
 global $SVARS;
 	if (!(int)$_POST['timestart']) die('{success:false}');
	deleteDir("../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]");
	echo "{success:true}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_del_attachment() {
 global  $SVARS;
 	if (!(int)$_POST['timestart']) die('{success:false}');
	delfile("../../temp/mail_compose/".$SVARS['user']['id']."/$_POST[timestart]/".$_REQUEST['file_name']);
	f_get_attachments();
}

//————————————————————————————————————————————————————————————————————————————————————
function f_image_to_signature(){
 global $SVARS;
 
	 if(!is_dir("../../crm-files/signature_files/users_digital/".substr($SVARS['user']['id'], -2)."/".$SVARS['user']['id']))
	 	mkdir("../../crm-files/signature_files/users_digital/".substr($SVARS['user']['id'], -2)."/".$SVARS['user']['id'], 0777, true);
	 
	 preg_match("/^(.+)\.([a-z0-9_#!@-]{1,5})$/i", $_FILES['file']['name'], $regs); # find file extention
	 $file_name = "sig_img_".time().".".strtolower($regs[2]);
	 
	 $uploadfile = "../../crm-files/signature_files/users_digital/".substr($SVARS['user']['id'], -2)."/".$SVARS['user']['id']."/".$file_name;

	 $file = "?m=common/get&digital_signature=1&file=".$file_name;
	 
	 if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)){
	 	echo "{success:true,file:'".$file."'}";
	 }else echo '{success:false, msg:"!בעיה בשמירת קובץ"}';
}
?>
