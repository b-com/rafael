<?
/**
 * 
 * Rebuild message body to include inline parts
 * Rely on B-mail recieving message processing
 * @author Eugene
 *
 */
class BuildMessage{
	private $message_body;
	private $filesPath;
	private $signaturePath;
	private $signatureSource;
	private $inlineStartString = '?m=mail_new/get';
	private $signatureStartString = '?m=common/get';
	private $imageSource;
	
	//$cfg['filesPath'] = '../temp/mail/346/SnVuaw==/_1';
	
	public function __construct($message,$message_body){
		global $SVARS;
		$this->message_body = $message_body;
		$this->signaturePath = "../../crm-files/signature_files/users_digital/".substr($SVARS['user']['id'], -2)."/".$SVARS['user']['id']."/*";
	
		$this->buildFilePath();
		$this->buildSignaturePath();
		
			$this->embedFiles($message);
		}
	
	/*
	 * Find image source and build file path to signature directory
	 */
	private function buildSignaturePath(){
		
		$subject = stristr($this->message_body,$this->signatureStartString);
		
		$p = strpos($subject, 'file');
		if($p === false) return false;
		
	    $signatureSource = substr($subject, 0, $p);
	    
	    if(empty($signatureSource)) return false;
	    
	    $this->signatureSource = $signatureSource;
		
	    return true;
	}
	
	/*
	 * Find image source and build file path to files directory
	 */
	private function buildFilePath(){
		global $group_id;
		
		$subject = stristr($this->message_body,$this->inlineStartString);
		
		$p = strpos($subject, 'pid');
		if($p === false) return false;
		
	    $imageSource = substr($subject, 0, $p);
	    
	    if(empty($imageSource)) return false;
	    
	    $this->imageSource = $imageSource;
	    // try explode first method
	    $parameters = explode('&amp;', $this->imageSource);
	    // try second method
	    if(empty($parameters)){
	    	$parameters = explode('&', $this->imageSource);
	    }
	    // unset empty last parameter
	    array_pop($parameters);
	    
	    // find file path paremeters
	    $fileParameters = array();
	    for($i = 1; $i < count($parameters); $i++){
    		list($key,$val) = explode('=',$parameters[$i]);
    		$fileParameters[trim($key)] = trim($val);
	    }
		
	    if(empty($fileParameters)) return false;
	    // create  filePath
	    if($fileParameters['crm'])
	    	$this->filesPath = "../../mail_data/$group_id/".substr($fileParameters['client_id'], -2)
	    	."/".$fileParameters['client_id']."/".$fileParameters['contact_id']."/*";
	    else 
	    	$this->filesPath = "../../temp/mail/".$fileParameters['account']
	    		."/".base64_encode(rawurldecode($fileParameters['folder']))."/_".$fileParameters['uid']."/*";
		
	    return true;
	}
	
	private function getImageSource(){
		return empty($this->imageSource) ? false : $this->imageSource;
	}
	
	private function getSignatureSource(){
		return empty($this->signatureSource) ? false : $this->signatureSource;
	}
	
	/*
	 * Embed found files to message body
	 */
	private function embedFiles($message){
		//src="?m=shared/mail/get&account=346&crm=0&folder=Junk&uid=1&pid=4"
		
		$newFilePath = array();
		$oldFilePath = array();
		
		// No files sourse found
		if($imageSource = $this->getImageSource()){
			
		$files = glob($this->filesPath);
		
		foreach ($files as $file){
			if(is_file($file)){
				$fileName = base_name($file);
				$fileName = preg_replace("/\.([^\.]{1,5})$/i", '', $fileName);
				$newFilePath[] = $message->embed(Swift_Image::fromPath($file));
				$oldFilePath[] = $imageSource."pid=$fileName";
			}	
		}
		}
		
		// embend signature image
		if($signatureSource = $this->getSignatureSource()){
			$files = glob($this->signaturePath);
			foreach ($files as $file){
				if(is_file($file)){
					$fileName = base_name($file);
					$newFilePath[] = $message->embed(Swift_Image::fromPath($file));
					$oldFilePath[] = $signatureSource."file=$fileName";
				}	
			}
		}
		
		// No replacements found
		if(empty($oldFilePath) OR empty($newFilePath)) return false;
		
		$this->message_body = str_replace($oldFilePath,$newFilePath,$this->message_body);
	}
	
	/*
	 * Return modified message body
	 */
	public function getMessage(){
		return $this->message_body;
	}
	
}
?>
