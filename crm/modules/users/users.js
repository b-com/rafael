
var user={};
var branches=[];
var departments=[];

UserEditWin = function(grid, d) {
	// if (d) this.id=d.id;
	// this.grid=grid;

	var p=1, e=1, a=1;
	var clientPhoto="skin/pict/man_icon.gif";
	
	var famStore = [[0,'-בחר-'],[1,'אבא'],[2,'אמא'],[3,'בעל'],[4,'אשה'],[5,'בן'],[6,'בת'],[7,'אחר']];
	var telStore = [[1,'בית'],[2,'עבודה'],[3,'נייד'],[4,'פקס בבית'],[5,'פקס בעבודה'],[0,'אחר']];
	var adrStore = [[1,'בית'],[2,'חברה/עסק'],[3,'עבודה'],[4,'למשלוח דואר'],[0,'אחר']];
	var emailStore=[[1,'בית'],[2,'עבודה'],[0,'אחר']];
	
	this.addPhone = function() {
		if (p>4) return;
		Ext.getCmp('phoneSet'+p).show();
		this.syncShadow();
		p++;
	}

	this.addEmail = function() {if (e>2) return; Ext.getCmp('emailSet'+e).show(); this.syncShadow(); e++;}

	this.addAddress = function() {if (a>2) return; Ext.getCmp('adrSet'+a).show(); this.syncShadow(); a++;}
	
	this.departmentsStore = new Ext.data.SimpleStore({fields:['id','name'], data:[]});
	
	this.groupsStore = new Ext.data.JsonStore({
		url:'?m=groups/groups&f=list', totalProperty:'total', root:'data',
		fields:['id','name','notes'],
		sortInfo:{field:'name', direction:'ASC'}
	});
	
	this.selectBranch = function(combo, record, index){
		var branch_id=record.data.field1;
		if (branch_id) {
			this.departmentsStore.loadData(departments[branch_id]);
			Ext.getCmp('dep0cb').clearValue();  Ext.getCmp('dep0cb').enable();
			Ext.getCmp('dep1cb').clearValue();  Ext.getCmp('dep1cb').enable();
			Ext.getCmp('dep2cb').clearValue();  Ext.getCmp('dep2cb').enable();
		}else {
			this.departmentsStore.removeAll();
			Ext.getCmp('dep0cb').disable();
			Ext.getCmp('dep1cb').disable();
			Ext.getCmp('dep2cb').disable();
		}
	}

	this.tabs = new Ext.TabPanel({
		baseCls:'x-plain', plain:true, activeTab:0, forceLayout:true,
		defaults: {xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'border:1px solid #8DB2E3; padding:0 5px'},
		items: [{
			title:'כללי', autoHeight:true,
            items: [{
				xtype:'panel', id:'private', layout:'form', baseCls:'x-plain',
				items:[{
					layout:'column', baseCls:'x-plain',
					defaults: {xtype:'panel', columnWidth:0.5, layout:'form', baseCls:'x-plain', bodyStyle:'padding:5px 0', border:false},
					items:[{items:[
						{fieldLabel:'שם פרטי',   name:'fname', id:'fname', xtype:'textfield', tabIndex:14, width:120, allowBlank:false}
						,{fieldLabel:'מספר ת.ז.', name:'passport', id:'passport', tabIndex:16, xtype:'textfield', width:120, allowBlank:false}
						,{fieldLabel:'תאריך לידה', name:'birthday', tabIndex:18, xtype:'datefield', width: 120, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:true, value: (d ? d.birthday : '')}
						,{fieldLabel:'קבוצת הרשאות', labelWidth:200, hiddenName:'group_id', tabIndex:21, xtype:'combo', width:120, store:this.groupsStore, mode:'local', valueField:'id', displayField:'name', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
					]},{items:[
						{fieldLabel:'שם משפחה', name:'lname', id:'lname', xtype:'textfield', tabIndex:15, width: 120}
						,{fieldLabel:'מין', xtype:'combo', hiddenName:'sex', tabIndex:19, width:120, value:0, store:[[0,'-בחר-'],[1,'זכר'],[2,'נקבה']], mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
						,{fieldLabel:'דת', xtype:'combo', hiddenName:'religion', tabIndex:20, width:120, value:0, store:[[0,'אחר'],[1,'יהודי'],[2,'נוצרי'],[3,'מוסלמי']], mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
						,{fieldLabel:'שם משתמש', name:'username', tabIndex:22, xtype:'textfield', width:120}
						,{fieldLabel:'סיסמא', name:'password', tabIndex:22, xtype:'textfield', inputType:'password', width:120}
						,{xtype:'checkbox', boxLabel:'פעיל', style:'margin-top:2px;', name:'active', checked:(d && d.json && d.json.active==1)}
					]}]
				}]
				},{layout:'column', baseCls:'x-plain', defaults:{xtype:'fieldset', columnWidth:0.5}, items:[
					{title: 'טלפון/פקס', style:'margin-left:5px; padding:2px;', cls: 'rtl', width: 150, autoHeight:true, items:[
						{layout:'table', layoutConfig: {columns:3}, baseCls: 'x-plain', id:'phoneSet0', items:[
							{xtype:'combo', hiddenName:'tel_type0', width: 90, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel0', xtype:'textfield', cls:'ltr', width: 90},
							{baseCls: 'x-plain', html:'<img src="skin/icons/add_call.png" onClick="Ext.getCmp(\'userEditWin\').addPhone()" ext:qtip="הוסף מספר טלפון">'}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet1', items:[
							{xtype:'combo', hiddenName:'tel_type1', width: 90, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel1', xtype:'textfield', cls:'ltr', width:90}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet2', items:[
							{xtype:'combo', hiddenName:'tel_type2', width: 90, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel2', xtype:'textfield', cls:'ltr', width:90}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet3', items:[
							{xtype:'combo', hiddenName:'tel_type3', width: 90, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel3', xtype:'textfield', cls:'ltr', width:90}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet4', items:[
								{xtype:'combo', hiddenName:'tel_type4', width: 90, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
								{name:'tel4', xtype:'textfield', cls:'ltr', width:90}
						]}
					]}
					,{title:'דוא"ל', style:'margin-right:5px; padding:2px', cls: 'rtl', autoHeight:true, items:[
						{layout:'table', layoutConfig: {columns:3}, baseCls: 'x-plain', id:'emailSet0', items:[
							{html:'עיקרי:', width: 60, baseCls: 'x-plain', cls: 'rtl'},
							{name:'email', xtype:'textfield', cls:'ltr', width: 123, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/},
							{baseCls:'x-plain', html:'<img src="skin/icons/add_email.png" onClick="Ext.getCmp(\'userEditWin\').addEmail()" ext:qtip=\'הוסף כתובת דוא"ל\'>'}
						]},{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'emailSet1', items:[
							{xtype:'combo', hiddenName:'email_type1', width: 60, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore},
							{name:'email1', xtype:'textfield', cls:'ltr', width:123, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						]},{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'emailSet2', items:[
							{xtype:'combo', hiddenName:'email_type2', width: 60, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore},
							{name:'email2', xtype:'textfield', cls:'ltr', width:123, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						]}
					]}
			]}
			,{xtype:'fieldset', title:'כתובת', cls:'rtl', style:'margin-top:5px; padding:2px', autoHeight:true,
				items:[
					{layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain', id:'adr0', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
						{html:'סוג:'},{html:'עיר:'},{html:'מיקוד:'},{html:'כתובת/ת.ד.:', colspan: 2},
						{xtype:'combo', hiddenName:'adr_type0', width: 100, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
						{name:'city0', xtype:'textfield', width: 80},
						{name:'zip0', xtype:'textfield', width: 50},
						{name:'adr0', xtype:'textfield', width:175},
						{baseCls: 'x-plain', html:'&nbsp;<img src="skin/icons/add_addr.png" onClick="Ext.getCmp(\'userEditWin\').addAddress()" ext:qtip=\'הוסף כתובת\'>'}
					]}
					,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet1', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
						{xtype:'combo', hiddenName:'adr_type1', width: 100, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
						{name:'city1', xtype:'textfield', width: 80},
						{name:'zip1', xtype:'textfield', width: 50},
						{name:'adr1', xtype:'textfield', width:175}
					]}
					,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet2', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
						{xtype:'combo', hiddenName:'adr_type2', width: 100, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
						{name:'city2', xtype:'textfield', width: 80},
						{name:'zip2', xtype:'textfield', width: 50},
						{name:'adr2', xtype:'textfield', width:175}
					]}
				]
			},{xtype:'fieldset', title:'הערות', labelAlign:'top', cls:'rtl', autoHeight:true, style:'margin-top:3px; padding:0 5px',
				items:{xtype:'textarea', name:'notes', width:'100%', height:29, grow:true, growMin:18, growMax:100, growAppend:''}
			}
			]}
			,{title:'פרטים נוספים', autoHeight:true, items:[
				{xtype:'panel', layout:'column', baseCls: 'x-plain', defaults:{xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'padding-top:5px', border:false},
					items:[{
						columnWidth:0.55, labelWidth:90,
						items:[
							{fieldLabel:'אתר אינטרנט', name:'url', xtype:'textfield', width:120, cls:'ltr' },
							{fieldLabel:'ICQ', name:'icq', xtype:'textfield', width:120, cls:'ltr'},
							{fieldLabel:'Skype', name:'skype', xtype:'textfield', width:120, cls:'ltr'},
							{fieldLabel:'Messenger', name:'msn', xtype:'textfield', width:120, cls:'ltr'},
							{fieldLabel:'FaceBook', name:'facebook', xtype:'textfield', width:120, cls:'ltr'}
						]},{
						columnWidth:0.45, labelWidth:32, hideMode:'visibility',
						items:[
							{xtype:'panel', baseCls:'x-plain', cls:'rtl', html:'<img id=clientPhoto src="'+clientPhoto+'" align="left">תמונת משתמש: ', listeners:{render:function(){window.setTimeout("$('clientPhoto').src='"+clientPhoto+"';",100);}}}
							// ,{xtype:'fileuploadfield', name:'photo', emptyText:'בחר קובץ...', fieldLabel:'קובץ',labelWidth: 60, width: 160}
						]
					}]
				}
				,{xtype:'fieldset', title:'עבודה', cls:'rtl', autoHeight:true, style:'padding:2px',
					items:{xtype:'panel', layout:'column', baseCls:'x-plain', defaults:{xtype:'panel', layout:'form', baseCls:'x-plain', border:false},
						items:[{columnWidth:0.5, labelWidth:50, items:[
							{fieldLabel:'סניף', hiddenName:'branch_id', xtype:'combo', store:branches, anchor:'96%', listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true
								,listeners:{select:this.selectBranch, scope:this}
							}
							,{fieldLabel:'תפקיד', name:'position', xtype:'textfield', anchor:'96%'}
							,{fieldLabel:'משכורת', name:'salary', xtype:'numberfield', width:80}
						]},{columnWidth:0.5, labelWidth:80, hideMode:'visibility', items:[
							{fieldLabel:'מחלקה ראשי', id:'dep0cb', hiddenName:'dep0', disabled:true, xtype:'combo', store:this.departmentsStore, valueField:'id', displayField:'name', mode:'local', width:130, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
							,{fieldLabel:'מחלקה 2', id:'dep1cb', hiddenName:'dep1', disabled:true, xtype:'combo', store:this.departmentsStore, valueField:'id', displayField:'name', mode:'local', width:130, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
							,{fieldLabel:'מחלקה 3', id:'dep2cb', hiddenName:'dep2', disabled:true, xtype:'combo', store:this.departmentsStore, valueField:'id', displayField:'name', mode:'local', width:130, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
						]}]
					}

				}
			]}
			,{title:'הרשאות', autoHeight:true,  forceLayout:true, items:[
				{xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'padding-top:5px', labelWidth:125, border:false,
					items:[
					{xtype:'checkbox', checked:false, fieldLabel:'שליחת SMS',  name:'sms_active'},
					{xtype:'checkbox', checked:false, fieldLabel:'שליחת פקסים',name:'fax_active'},
					{xtype:'fieldset', title:'קבלת פקסים', cls:'rtl', autoHeight:true, style:'padding:2px', items:fax_numbers
					},
					{xtype:'fieldset', title:'קבוצות לקוחות', cls:'rtl', labelWidth:125, autoHeight:true, style:'padding:2px', items:client_groups
					// [
						// {xtype:'checkbox', checked:false, fieldLabel:'0779030305', name:'g[1]'},
						// {xtype:'checkbox', checked:false,  fieldLabel:'0779030301', name:'g[2]'}
					// ]
					}
					]
				}
			]}
		]
	});
	
	this.tabs.on('tabchange', function(tabPanel, tab) {
		// if (Ext.getCmp('clType').getValue()==1) {Ext.getCmp('job').show(); Ext.getCmp('cont_pers').hide();}
		// else {Ext.getCmp('cont_pers').show(); Ext.getCmp('job').hide();}
		this.tabs.doLayout(); this.syncShadow();
	},this);
	
	this.form = new Ext.form.FormPanel({
		id:'clientForm', fileUpload:true, labelWidth:75, autoHeight:true, baseCls:'x-plain',
		items:[
			this.tabs

		]
	});
	
	UserEditWin.superclass.constructor.call(this, {
		title:(d ? 'עידכון פרטי משתמש' : 'הוספת משתמש חדש'),
		id:'userEditWin', layout:'fit', modal:true,
		width:500, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:0 5px;', plain:true, border:false,
		items:this.form,
		buttons:[{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('show', function(){
		Ext.getCmp('phoneSet1').hide(); Ext.getCmp('phoneSet2').hide(); Ext.getCmp('phoneSet3').hide(); Ext.getCmp('phoneSet4').hide();
		Ext.getCmp('emailSet1').hide(); Ext.getCmp('emailSet2').hide();
		Ext.getCmp('adrSet1').hide(); Ext.getCmp('adrSet2').hide();
		// Ext.getCmp('business').hide();
		
		// Dump(d);
		if (d.branch_id) {
			Ext.getCmp('dep0cb').enable(); Ext.getCmp('dep1cb').enable(); Ext.getCmp('dep2cb').enable();
			this.selectBranch('', {data:{field1:d.branch_id}});
		}
		if (d.id) this.formLoad();
		// else if(d) this.form.form.setValues(d);
		this.syncShadow();
	});
	
	this.formLoad=function() {
		// this.getEl().mask('...טוען');
		this.groupsStore.load();
		this.form.load({
			url:"?m=users/users&f=load_data&id="+d.id,
			reader: new Ext.data.JsonReader({root:'data', totalProperty:'total', successProperty:'success'}),
			success: function(f,a){
				var d=a.result.data;
				for (var n=1; n<5; n++) if (d['tel'+n]) Ext.getCmp('phoneSet'+n).show();
				for (var n=1; n<3; n++) if (d['email'+n]) Ext.getCmp('emailSet'+n).show();
				for (var n=1; n<3; n++) if (d['city'+n] || d['zip'+n]!=0 || d['adr'+n]) Ext.getCmp('adrSet'+n).show();
				if (d['photo']) clientPhoto=d['photo'];
				this.syncShadow();
				// this.getEl().unmask();
			},
			failure: function(){ this.getEl().unmask(); },
			scope:this
	   	});
	}

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		var p = {};
		
		if (this.tabs.items.items[2].items.items[0].items.items[0].getValue()) p['sms_active']=true;
		if (this.tabs.items.items[2].items.items[0].items.items[1].getValue()) p['fax_active']=true;
		
		if (fax_numbers) {
			var faxes = this.tabs.items.items[2].items.items[0].items.items[2].items.items;
			for (f in faxes) if (faxes[f].getValue && faxes[f].getValue()) {p[faxes[f].name]=faxes[f].getValue(); }
		}
		if (client_groups) {
			var groups = this.tabs.items.items[2].items.items[0].items.items[3].items.items;
			for (f in groups) if (groups[f].getValue && groups[f].getValue()) {p[groups[f].name]=groups[f].getValue(); }
		}
		
		this.form.form.submit({
			url: "?m=users/users&f=edit&id="+d.id,
			params:p,
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			scope:this,
			success: function(f,a){
				this.close();
				var data = Ext.decode(a.response.responseText);
				if (!d.id && grid.openEmployee) grid.openEmployee(data);
				else if (grid.store) grid.store.reload();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result);}
		});
	}
};
Ext.extend(UserEditWin, Ext.Window, {});


UserTab = function(d) {
	this.d=d;
	this.caller='client_info';
	
	this.userEditWin = function(){var win=new UserEditWin(this,this.d); win.show(); win.center();};
	
	this.onClientChange = function(d) {
		d.photo="skin/pict/man_icon.gif";
		if (d.sex==2) {d.title='שם'; d.icon='user-female'; d.job_title='תפקיד'; d.work_title='מקום עבודה'}
		else {d.title='שם'; d.icon='user'; d.job_title='תפקיד'; d.work_title='מקום עבודה'}
		// if (d.sex==3) {d.title='שם עסק'; d.icon='user_business'; d.job_title="מס' משתמשים"; d.work_title='תחום'};
		Ext.getCmp('clientData_'+d.id).tpl.overwrite(Ext.getCmp('clientData_'+d.id).scope.body, d);
	};
	
	this.contactsList= new ContactGrid({client_type:'user', client_id:this.d.id, clientCell:this.d.clientCell});
	this.contactsList.on('activate', function(tabPanel, tab) {this.contactsList.store.load();},this);

	this.filesList 	= new FilesGrid({client_id:this.d.id, client_type:'user'});//client_id,client_type,group_email
	this.filesList.on('activate', function(tabPanel, tab) {this.filesList.store.load();},this);

	this.userInfo = {
		xtype:'panel', //style:'border-bottom: 2px solid white; background-color:#f3f8fc;',
		title:'פרטי משתמש',
		id:'userInfo'+this.d.data.id
		,baseCls: 'x-plain'
		,tbar:[{text:'עדכן פרטי משתמש', iconCls:'edit', handler:this.companyEditWin, scope:this}]
		,tpl: new Ext.Template(
				'<table border=1 width=100% id=client_info dir=rtl><tr>',
				'<td nowrap><img src="skin/icons/1/{icon}.gif" align=absmiddle> <b>{title}:</b></td><td width=30%><b>{name}</b></td>',
				'<td nowrap><img src="skin/icons/1/zehut.gif" align=absmiddle> <b>ח.פ./ת.ז:</b></td><td width=10% nowrap>{passport}</td><td nowrap><b>רשיון נהיגה:</b> {license}</td>',
				'<td nowrap><img src="skin/icons/1/date.gif" align=absmiddle> <b>ת.לידה:</b></td><td width=15%>{birthday}</td>',
				'<td rowspan=4><img id=client_photo src="{photo}"></td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/telephone.gif" align=absmiddle> <b>טלפונים:</b></td><td width=30%>{tel_txt}</td>',
				'<td nowrap><img src="skin/icons/1/email.gif" align=absmiddle> <b>דוא"ל:</b></td><td width=30% colspan=2><a href=# onClick="sendEmail({id},\'{email}\');">{email}</a></td>',
				'<td nowrap width=15%><b>{job_title}:</b></td><td>{position}</td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/house.gif" align=absmiddle> <b>כתובת:</b></td><td width=30%>{adr}</td>',
				'<td nowrap><img src="skin/icons/1/money.png" border=0 align=absmiddle> <b>יתרת חוב:</b></td><td width=30% colspan=2><span style="color:red;" dir=ltr><b>{balance:ilMoney}</b></span></td>',
				'<td nowrap width=15%><b>{work_title}:</b></td><td>{work}</td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/note.gif" align=absmiddle><b>הערות:</b></td><td colspan=6>{notes}</td></tr></table>'
		)
		,listeners:{
			render:function(){this.tpl.overwrite(this.body, d.json)}
		}
	};
	// this.tabs = new Ext.TabPanel({
			// activeTab:0, baseCls:'x-plain', style:'border-bottom: 1px solid #8DB2E3;', region:'center',
			// defaults: {xtype:'panel', layout:'form', baseCls: 'x-plain', style:'padding-top:10px'},
			// items:[this.contactsList/*,this.filesList*/]
		// });

	// UserTab.superclass.constructor.call(this, {
		// id: 'tab_'+this.d.id,
		// iconCls: (this.d.sex==2 ? 'client-female' :(this.d.sex==3 ? 'client-business' :'client')),
		// title: this.d.name,
		// closable:true, autoScroll:true, layout:'border', 
		// items: [this.clientInfo,this.tabs]
		// ,tbar: [ 
			// {text:'עדכן פרטי משתמש', iconCls:'edit', handler:this.userEditWin, scope:this}
			// ,'-' ,{text:'SMS שלח', iconCls:'send_sms', handler:function(){this.contactsList.smsWindow();}, scope:this}
		// ]

    // });
	
	UserTab.superclass.constructor.call(this, {
		id:'userTab_'+this.d.id,
		iconCls:'client-business', title:d.data.name, closable:true, autoScroll:true, activeTab:0,
		items:[this.userInfo,this.contactsList,this.filesList]
    });

	
	// this.on('render',function(){this.onClientChange(this.d);},this);
};
Ext.extend(UserTab, Ext.TabPanel);

UsersGrid = function(cfg) {
	Ext.apply(this, cfg);

	this.store = new Ext.data.JsonStore({
		url:'?m=users/users&f=list'+(cfg.company_id ? '&company_id='+cfg.company_id:''), totalProperty:'total', root:'data',
		fields:['id','branch_id','name','passport','tel','tel_txt','fax','adr','email','birthday','notes','c_num','sex'],
		sortInfo:{field:'name', direction:'ASC'},
		remoteSort:true
	});

	this.userEditWin = function(d){var win=new UserEditWin(this,d); win.show(); win.center();}

	this.searchField = new GridSearchField({store:this.store, width:150});

	this.store.on('load', function(){	// check for errors
			var callback=function() { this.store.load(); };
			ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		}, this);
		
	this.hlStr = hlStr;
	
	this.nameRenderer = function(str, p, record){
		return "<span style='display:block;height:16px;padding-right:18px;background:url(skin/icons/user"+(record.json.sex==2?'-female':(record.json.sex==3?'_business':''))+".gif) no-repeat right top;'>" +this.hlStr(str)+ "</span>";
	}	
	
    this.columns = [
		{dataIndex:'adr', id:'adr', header:'כתובת', sortable:true, renderer:this.hlStr.createDelegate(this) },
		{dataIndex:'passport', header:'ת.ז./ח.פ.', width:70, sortable:true, renderer:this.hlStr.createDelegate(this) },
		{dataIndex:'fax', header:'פקס', width:80, sortable:true, renderer:this.hlStr.createDelegate(this) }, 
		{dataIndex:'tel', header:'טלפונים', width:140, renderer:this.hlStr.createDelegate(this) }, 
		{dataIndex:'name', header:'שם', width:200, sortable:true, renderer:this.nameRenderer.createDelegate(this) }
		// ,this.expander
	];
	
	UsersGrid.superclass.constructor.call(this, {
		title:'רשימת משתמשים',iconCls:'grid',
		enableColLock:false,loadMask:true,stripeRows:true,enableHdMenu: false,
		// plugins: this.expander,
		autoExpandColumn: 'adr',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'חיפוש:', this.searchField
			,'-' , 'סניף:', {xtype:'combo', store:[['','כל הסניפים']].concat(branches), id:'branchFilter', width:120, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, value:'', listeners:{select:function(f){this.store.load()},scope:this} }
			,'-' , new Ext.form.ComboBox({store:[[1,'פעילים'],[0,'מבוטלים']], id:'activeFilter', value:1, width:80, listClass:'rtl', triggerAction:'all', forceSelection:true, 
				listeners:{
					select:function(f){
						this.store.reload();
						if (f.getValue()==1) {
							this.getTopToolbar().items.get('user_del').hide();
							this.getTopToolbar().items.get('user_restore').show();
						}else {
							this.getTopToolbar().items.get('user_del').show();
							this.getTopToolbar().items.get('user_restore').hide();
						}
					}, 
					change:function(f){if(f.getRawValue()==''){f.setValue(0);this.store.reload();} },
					scope:this
				}
			})
			,'-' ,{ text:'משתמש חדש', iconCls:'user_add', handler:function(){this.userEditWin({})}, scope:this }
			,'-' ,{ text:'עדכן משתמש', id:'user_edit', iconCls:'edit', handler:function(){this.userEditWin(this.getSelectionModel().getSelected().data);}, disabled:true, scope:this }
			,'-' ,{ text:'מחק משתמש', id:'user_del', iconCls:'delete', handler:function(){this.delClientPromt();}, disabled:true, scope:this }
				 ,{ text:'שחזר משתמש', id:'user_restore', iconCls:'restore', handler:function(){this.restoreClientPromt();}, disabled:true, hidden:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
    });

	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			q:this.searchField.getRawValue().trim()
			,deleted:Ext.getCmp('activeFilter').getValue()
			,branch_id:Ext.getCmp('branchFilter').getValue()
		}
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);
	
	// this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('user_edit').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('user_del').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('user_restore').setDisabled(!sm.getCount());
	}, this);
	
	// open client in new tab
	this.openEmployee = function(d){
		var tab;
		if(!(tab=this.tabs.getItem('userTab_'+d.id))) {tab=new UserTab(d); this.tabs.add(tab); }
		this.tabs.setActiveTab(tab);
	};

	// doubleClick
	this.on('rowdblclick', function(grid, row, e){
		if (cfg.company_id) return;
		if (e.getTarget().tagName.toLowerCase()=='button') return;
		var data = grid.getSelectionModel().getSelected();
		this.openEmployee(data);
	});
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row).data;
		
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'פתח משתמש', iconCls:'user_open', scope:this, handler:function(){this.openEmployee(this.srData);} }
				,{text:'עדכן משתמש', iconCls:'user_edit', scope:this, handler:function(){this.userEditWin(this.srData);} }
				// ,( (Ext.getCmp('search_team_id').getValue()!='deleted') ? {text:'פתח גישה לתיק משתמש באינטרנט', iconCls:'key', scope:this, handler:function(){set_password(this.srData)}} : '' )
				// ,( (Ext.getCmp('search_team_id').getValue()=='deleted') ? {text:'שחזר משתמש', iconCls:'user_restore', scope:this, handler:this.restoreClientPromt} : {text:'מחק משתמש', iconCls:'user_del', scope:this, handler:this.delClientPromt} )
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.delClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:250, title:'למחוק משתמש?', msg:'<img src="skin/icons/1/user_del48.gif" align=left><br>האם ברצונך לבטל את המשתמש<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delClient(records); }
		});
	}
	
	this.delClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '/?m=company/employees_f&f=delete&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('משתמש בוטל','משתמש <b>'+d.data.name+'</b> בוטל בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};

	this.restoreClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'לשחזר משתמש?', msg:'האם ברצונך לשחזר את המשתמש<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.restoreClient(records); }
		});
	}
	
	this.restoreClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '/?m=company/employees_f&f=restore&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid green';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('משתמש שוחזר','משתמש <b>'+d.data.name+'</b> שוחזר בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	this.on('render', function(){ this.store.load() });
};
Ext.extend(UsersGrid, Ext.grid.GridPanel);

Ms.users.run = function(cfg){
	var m=Ms.users;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var usersTabs = new Ext.TabPanel({activeTab:0, border:false, defaults:{autoScroll:true}, enableTabScroll:true});
		usersTabs.add(new UsersGrid({tabs:usersTabs}));
		win = Crm.desktop.createWindow({width:900, height:600, items:usersTabs}, m);
	}
	win.show();
};