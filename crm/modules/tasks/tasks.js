var taskStatus = [[1,'טרם בוצעה'],[2,'בוצעה'],[3,'הודעה בלבד'],[0,'כולן'],[-1,'מחוקות']];
var clientCell='';
var userCombo = {xtype:'combo', store:users, hiddenName:'user_id', value:0, width:100, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true};
var today = new Date();

Ext.override(Ext.form.BasicForm,{
    clearDirty : function(nodeToRecurse){
        nodeToRecurse = nodeToRecurse || this;
        nodeToRecurse.items.each(function(f){if(f.items) this.clearDirty(f); else f.originalValue = f.getValue();});
    }
});

ActionWindow = function(cfg) {
	this.form = new Ext.form.FormPanel({labelAlign:'top',baseCls:'x-plain',	items: {height:45, anchor:'100%', name:'notes', fieldLabel:'תאור הפעולה', xtype:'textarea', cls:'rtl', allowBlank:false}});
	
	ActionWindow.superclass.constructor.call(this, {
		title:'פעולה חדשה',
		modal:true,
		resizable:false,
		width:460,
		buttonAlign:'center',
		bodyStyle:'padding:5px;',
		plain:true,
		items:this.form,
		buttons: [{text:'שמור', handler:function(){cfg.callBack(this.form.items.items[0].getValue()); this.close();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
};
Ext.extend(ActionWindow, Ext.Window, {});

//Task Add-Edit Window
TaskWindow = function(grid, d) {

	this.task_id=d.id;
	var Action = Ext.data.Record.create([
		{name: 'date', mapping: 'date', type: 'date', dateFormat: 'm/d/Y H:i'},
		{name: 'user_id'},
		{name: 'notes', type: 'string'}
	]);

	var fm = Ext.form;
	
	this.client_id=0;
	
	this.clientCombo = new ClientCombo({width:200});
	
	this.saveAction = function(t) {
		if (!t || !this.task_id) return;
		else if (t=='phone') txt='בוצעה שיחה';
		else if (t=='mail') txt='נשלח דוא"ל';
		else if (t=='sms') txt='נשלח SMS';
		else txt=t;

		Ext.Ajax.request({
			url: '?m=tasks/tasks&f=add_action&task_id='+this.task_id,
			params:{notes:txt, client_id:this.clientCombo.getValue()},
			success: function(r,o){
				r=Ext.decode(r.responseText);
				if (!ajRqErrors(r)) {
					this.taskStore.reload();
					var p = new Action({date: (new Date()).format('d/m/Y H:i'), user_id:user.user_id, client:this.clientCombo.getRawValue(), notes:txt});
					this.taskStore.insert(0, p);
				}
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.');},
			scope: this
		});
	}
	
	this.clientCombo.beforeBlur=function () {
		var val = this.getRawValue(), rec = this.findRecord(this.displayField, val);
		if (!rec && this.forceSelection) {
			if (val.length > 0 && val != this.emptyText) {
				this.el.dom.value = Ext.isDefined(this.lastSelectionText) ? this.lastSelectionText : "";
				this.applyEmptyText();
			} else {this.clearValue();}
		} else {
			if (rec) {val = rec.get(this.valueField || this.displayField); this.setValue(val);} else {this.setValue(null);this.setRawValue(val);}
		}
	};
	var wins=[];
	this.openClientNewWin = function(){
		var client_id = this.clientCombo.getValue();
		if (!client_id) return;
		var url='http://'+location.host+'/?m=crm/ccard&init=1&a=res_table&client_type=client&client_id='+client_id;
		if (!wins[client_id] || wins[client_id].closed) wins[client_id]=top.window.open(url, 'client'+client_id, 'width=800,height=550,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,renew=0,titlebar=0');
		if (wins[client_id]) wins[client_id].focus();
	};
	
	this.taskStore = new Ext.data.JsonStore({
		url:'?m=tasks/tasks&f=actions_list&task_id='+d.id,
		//id: 'id',
		totalProperty:'total',	root:'data',
		fields: ['id',{name:'date',type:'date', dateFormat:'d/m/Y H:i'},'user_id','name','notes'],
		sortInfo: {field:'date', direction:'DESC'},
		remoteSort: false
	});

	this.form = new Ext.form.FormPanel({
		id:'newTaskForm', labelWidth:75, autoHeight:true, baseCls: 'x-plain',labelAlign:'top',
		style:'padding:0 6px',
		items:[{
			layout:'table', layoutConfig:{columns:6}, baseCls:'x-plain', cls:'rtl', defaults: {layout:'form', baseCls: 'x-plain'},
			items:[
				{xtype:'label', text:'סטטוס:'},{xtype:'combo', store:[[1,'טרם בוצעה'],[2,'בוצעה'],[3,'הודעה בלבד']], allowBlank:false, hiddenName:'status', value:(d.status?d.status:1), width:85, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true},
				{xtype:'label', text:'כותרת:', style:'padding-right:10px'},{xtype:'textfield', colspan:3, width:'100%', name:'name', allowBlank:false, value:(d.name?d.name:'')},
				{xtype:'label', text:'עדיפות:'},{xtype:'combo', store:[[-1,'נמוכה'],[0,'רגילה'],[1,'חשובה']], hiddenName:'importance', value:(d.importance ? d.importance :0), width:85, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true},
				{xtype:'label', text:'לטיפול ע"י:', style:'padding-right:10px'},{xtype:'combo', store:users, hiddenName:'user_id', value:((d.user_id!=null && users_obj[d.user_id]) ? d.user_id :user.user_id), width:120, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, allowBlank:false},
				{xtype:'label', text:'יעד סיום:', style:'padding-right:10px;'},{name:'end', xtype:'xdatetime', dateConfig:{allowBlank:false, altFormats:Ext.form.DateField.prototype.altFormats}, value:(d.end ? d.end:'')},
				{xtype:'label', text:"שליחת SMS על שינוי סטאטוס למס':", colspan:3},{xtype:'textfield',name:'sms', value:(d.sms ? d.sms:'')},
				{xtype:'label', text:'תזכורת לביצוע:', style:'padding-right:10px'},{name:'remind', xtype:'xdatetime', value:(d.remind ? d.remind:'')}
			]
		},
		// {xtype:'label', text:'תאור המשימה:', style:'padding-right:10px'},
		{height:45, anchor:'100%', name:'notes', fieldLabel:'תאור המשימה', xtype:'textarea', cls:'rtl', value:(d.notes ? d.notes:'')}
		,{xtype:'hidden', name:'id', value:(d ? d.id:'')}
		,{xtype:'hidden', name:'client_id', value:(d ? d.client_id:'')}
		,{xtype:'hidden', name:'client_type', value:(d ? d.client_type:'')}
		,{xtype:'hidden', name:'link'  ,  value:(d ? d.link:'') }

		]
	});
	
	this.contactWindow = function(grid,type,d){
		var win=new ContactWindow({grid:grid,type:type, callBack:function(){this.saveAction(type)}.createDelegate(this)},d);
		win.show();
	};
	
	this.smsWindow = function(grid,d){
		if (smsActive) {
			var win=new SmsWindow(grid,d);
			win.show();
		}else Ext.Msg.alert('שרות SMS', 'לקבלת שרות SMS התקשרו: 072-2514900');
	}

	this.actionWindow = function(cfg){
		var win=new ActionWindow(cfg);
		win.show();
	}
	
	this.actionAdd = {
		text:'הוסף פעולה', iconCls:'add', 
		menu: {
			items: [
				{text:'שיחה', iconCls:'contact_add', handler:function(){
					if(!this.form.form.isValid()) {Ext.Msg.alert('', 'נא להזין שם המשימה ויעד סיום!'); return;}
					else if (!this.clientCombo.getValue()) {Ext.Msg.alert('שיחה', 'נא לבחור שם לקוח'); return;}
					else if (!this.task_id) this.submitForm();
					if (this.clientCombo.getValue()) this.contactWindow({client_id:this.clientCombo.getValue(),client_type:'client', callBack:function(){this.saveAction('phone');}.createDelegate(this) },'phone');
					}, scope:this},
				{text:'שלח דוא"ל', iconCls:'mail_add', handler:function(){
					if(!this.form.form.isValid()) {Ext.Msg.alert('', 'נא להזין שם המשימה ויעד סיום!'); return;}
					else if (!this.clientCombo.getValue()) {Ext.Msg.alert('דוא"ל', 'נא לבחור שם לקוח'); return;}
					else if (!this.task_id) this.submitForm();
					if (this.clientCombo.getValue()) this.contactWindow({client_id:this.clientCombo.getValue(),client_type:'client', callBack:function(){this.saveAction('mail');}.createDelegate(this) },'mail');
					}, scope:this},
				{text:'שלח SMS', iconCls:'send_sms', handler:function(){
					if(!this.form.form.isValid()) Ext.Msg.alert('', 'נא להזין שם המשימה ויעד סיום!'); 
					else if (!this.clientCombo.getValue()) Ext.Msg.alert('SMS', 'נא לבחור שם לקוח');
					else if (!this.task_id) this.submitForm();
					if (this.clientCombo.getValue()) this.smsWindow({client_id:this.clientCombo.getValue(),client_type:'client', callBack:function(){this.saveAction('mail');}.createDelegate(this) },'mail');
					}, scope:this},
				{text:'אחר', iconCls:'add', handler:function(){
					if(!this.form.form.isValid()) {Ext.Msg.alert('אחר', 'נא להזין שם המשימה ויעד סיום!'); return;}
					else if (!this.task_id) this.submitForm();
					this.actionWindow({callBack:function(t){this.saveAction(t);}.createDelegate(this) });
					}, scope:this
				},
				{text:'פתח לקוח בחלון חדש', iconCls:'user_open', handler:function(){this.openClientNewWin()}, scope:this}
			]
		}
	};
	
	this.grid = new Ext.grid.GridPanel({
		store:this.taskStore,
		height:160,	style:'padding-right:10px; padding-left:10px;',	cls:'rtl', autoScroll:true, border:false,
		// hidden:(d.client_id),
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		autoExpandColumn:'notes',
		columns: [
			{header:'תאריך ושעה', dataIndex:'date', width:100, css:'direction:ltr;', renderer:Ext.util.Format.dateRenderer('d/m/Y H:i')}
			,{header:'משתמש', dataIndex:'user_id', renderer:userRenderer},{header:'לקוח', dataIndex:'name'}
			,{id:'notes', header:'תאור פעולה', dataIndex:'notes', 
				renderer:function(val,meta,record,rowIndex,colIndex,store){meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(val)+'"'; return Ext.util.Format.ellipsis(Ext.util.Format.stripTags(val),100); } 
			
			}],
		tbar: (!d.client_id ? ['בחר לקוח: ',this.clientCombo, this.actionAdd] : ['מעקב:'])
	});
	
	TaskWindow.superclass.constructor.call(this, {
		title:(d.id?'עידכון משימה':'משימה חדשה'),
		iconCls:'task',	layout:'fit', id:'TaskWindow', modal:true,
		width:600, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[this.form,this.grid],
		buttons:[{text:(d.id?'עדכן':'הוסף'), handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.submitForm = function() {
		var p ={user_name:this.form.find('hiddenName','user_id')[0].getRawValue()};
		if(!this.form.form.isValid()) {Ext.Msg.alert('', 'נא להזין שם המשימה ויעד סיום!'); return;}
		if(!this.form.form.isDirty()) {this.close(); return;}
		else {
			this.form.form.items.each(function(f){
				if (f.originalValue.toString()!=f.getValue().toString()) 
					p['_'+(f.name||f.hiddenName)] = (f.xtype=='datefield' && f.getValue() ? f.getValue().format('d/m/Y H:i') : f.getValue());
			});
		}

		this.form.form.submit({
			url: "?m=tasks/tasks&f=add_task",
			params: p,
			waitTitle:'אנא המתן...', waitMsg:'טוען...',
			success: function(f,a){
				grid.store.reload();
				this.task_id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.task_id);
				this.form.form.items.each(function(f){f.originalValue=f.getValue();});
				
				if (top.alertsStore) {	// update alerts
					
					//Dump(this.form.find('name','remind')[0].getValue());
					var remind = this.form.find('name','remind')[0].getValue();
					if (remind) remind=remind.format('Y-m-d H:i');
					var alertRec = top.alertsStore.getById(this.task_id);
					// Dump('alertRec='+alertRec);
					//Dump(Ext.apply({},remind));
					// top.alertsStore.afterEdit=function(){};
					// top.alertsStore.onUpdate=function(){};
					// alert(top.alertsStore.afterEdit);
					if (!remind || this.form.find('hiddenName','user_id')[0].getValue()!=user.user_id) {
						 if (alertRec && top.alertsStore.getById(this.task_id)) top.alertsStore.remove(top.alertsStore.getById(this.task_id));	// remove
					}else if ( remind < new Date(new Date().getTime()+3600000).format('Y-m-d H:i') ) {
						if (!alertRec) {	// insert new record
							top.alertsStore.add(new top.alertsStore.recordType({
								id:		this.task_id,
								type:	'task',
								remind: remind,
								end:	this.form.find('name','end')[0].getValue().format('d/m/Y H:i'),
								name:	this.form.find('name','name')[0].getValue(),
								notes:	this.form.find('name','notes')[0].getValue()
							}));
						}else {		// update record
							alertRec.beginEdit();
							alertRec.set('remind', remind);
							alertRec.set('end',	  this.form.find('name','end')[0].getValue().format('d/m/Y H:i'));
							alertRec.set('name',  this.form.find('name','name')[0].getValue());
							alertRec.set('notes', this.form.find('name','notes')[0].getValue());
							alertRec.endEdit();
							alertRec.commit();
						}
					}
					top.checkAlerts();
				}
				this.close();
			},
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	
	if (d.id) this.taskStore.load();
};
Ext.extend(TaskWindow, Ext.Window, {});

TasksGrid = function() {
	this.store = new Ext.data.JsonStore({
		url:'?m=tasks/tasks&f=tasks_list',
		totalProperty: 'total',
		root: 'data',
		fields: ['id','status','name','owner_id',{name:'start',type:'date', dateFormat:'d/m/Y H:i'},'user_id',{name:'end',type:'date', dateFormat:'d/m/Y H:i'},{name:'remind',type:'date', dateFormat:'d/m/Y H:i'},'importance','sms','notes','client_name','first_name','last_name'],
		sortInfo: {field:'end', direction:'ASC'},
		remoteSort: true
	});

	this.taskWindow = function(d){
		var win=new TaskWindow(this,d);
		win.show();
		win.center();
	}

	this.searchField = new GridSearchField({store:this.store, width:250});
	
	this.hlStr = hlStr;
	
	this.nameRenderer = function(str, meta, r){
		meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(r.data.notes)+'" ext:qtitle="'+Ext.util.Format.htmlEncode(str)+'"'; 
		str=this.hlStr(str);
		if (r.data.status==2) str='<font color=gray>'+str+'</font>';
		return str;
	};
	
	this.statusRenderer = function(str, p, record){
		if (str==1) {
			if (today.format('YmdHi')>record.data.end.format('YmdHi')) return "<img src='skin/icons/flag-red.gif' ext:qtip='בפיגור'>";
			else return "<img src='skin/icons/flag-lightred.gif' ext:qtip='לא בוצעה'>";
		}
		else if (str==2) return "<img src='skin/icons/check-green.gif' ext:qtip='בוצעה'>";
		else if (str==3) return "<img src='skin/icons/info.gif' ext:qtip='הודעה בלבד'>";
	};
	
	this.importanceRenderer = function(str){
		if (str==-1) return "<img src='skin/icons/importance-low.gif' ext:qtip='עדיפות נמוכה'>";
		else if (str==1) return "<img src='skin/icons/important.gif' ext:qtip='חשוב'>";
		else if (str==2) return "<img src='skin/icons/important.gif' ext:qtip='דחוף'>";
		else return '';
	};
	
	this.endRenderer = function(d,p,r){
		var date=Ext.util.Format.date(d,'d/m/Y H:i');
		if (r.data.status<2 && today.format('YmdHi')>d.format('YmdHi')) date='<font color=#e71d12>'+date+'</font>';
		else if (r.data.status==2) date='<font color=gray>'+date+'</font>';
		return date;
	};
	
	this.remindRenderer = function(d){ if (d) return "<img src='skin/icons/bell.gif' ext:qtip='תזכורת ב-"+d.format('H:i d/m/Y')+"'>"; };
	
	this.userRenderer = function(v, m, r){
		if (v==0) return 'כל משתמשים';
		if (users_obj[v]) v=users_obj[v].fullName;
		if (r.data.status==2) v='<font color=gray>'+v+'</font>';
		return v;
	};
	
    this.columns = [
		{dataIndex:'status', header:'&nbsp;', width:16, sortable:true, renderer:this.statusRenderer},
		{dataIndex:'importance', header:'&nbsp;', width:11, sortable:true, renderer:this.importanceRenderer},
		{dataIndex:'remind', header:'&nbsp;', width:13, sortable:true, renderer:this.remindRenderer},
		{dataIndex:'name', header:'כותרת', sortable:true, width:250, renderer:this.nameRenderer, scope:this},
		{dataIndex:'end', header:'יעד סיום', width:100, sortable:true, css:'direction:ltr;text-align:right;', renderer:this.endRenderer}, 
		{dataIndex:'user_id', header:'לטיפול ע"י', sortable:true, renderer:this.userRenderer}, 
		{dataIndex:'notes', id:'notes', header:'תיאור משימה', renderer:function(val,meta,record){meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(val)+'" ext:qtitle="'+Ext.util.Format.htmlEncode(record.data.name)+'"'; val=Ext.util.Format.ellipsis(val,100); if (record.data.status==2) val='<font color=gray>'+val+'</font>'; return val; }}, 
		{dataIndex:'client_name', header:'נפתחה בתיק לקוח', sortable:true},
		{dataIndex:'owner_id', header:'נפתחה ע"י', sortable:true, renderer:this.userRenderer}
	];
	
	TasksGrid.superclass.constructor.call(this, {
		enableColLock:false, loadMask:true, border:false, stripeRows:true,	enableHdMenu:false, plugins:this.expander,	autoExpandColumn:'notes',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		tbar: [
			'-' , 'חיפוש:', this.searchField
			, 'סטטוס משימה:',new Ext.form.ComboBox({value:1, id:'task_status', hoddenName:'task_status', valueField:'id',  displayField:'val', width:85, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:taskStatus, 
				listeners:{select:function(e){
							this.store.reload();
							if (e.getValue()==-1) {this.getTopToolbar().items.get('del_task').hide(); this.getTopToolbar().items.get('rest_task').show();
							}else {this.getTopToolbar().items.get('del_task').show();this.getTopToolbar().items.get('rest_task').hide();}
						}, scope:this}})
			, '-' ,{ text:'משימה חדשה', iconCls:'task', handler:function(){this.taskWindow({})}, scope:this }
			, '-' ,{ text:'עדכן משימה', id:'edit_task', iconCls:'edit', handler:function(){this.taskWindow(this.getSelectionModel().getSelected().data)}, disabled:true, scope:this }
			, '-' ,{ text:'מחק משימה',  id:'del_task', iconCls:'delete', handler:function(){this.delTaskPromt();}, disabled:true, scope:this }
				  ,{ text:'שחזר משימה', id:'rest_task', iconCls:'restore', handler:function(){this.restoreTaskPromt();}, disabled:true, hidden:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})
    });
	
	this.store.on('beforeload', function(store, options){
			this.store.removeAll();
			var setParams = {
				q: this.searchField.getRawValue().trim(),
				status: Ext.getCmp('task_status').getValue()
			}
			Ext.apply(store.baseParams, setParams);
			Ext.apply(options.params, setParams);
		}, this);

	this.store.on('load', function(){	// check for errors
			var callback=function() { this.store.load(); };
			ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		}, this);
	this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('edit_task').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('del_task').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('rest_task').setDisabled(!sm.getCount());
	}, this);
	
	// doubleClick
	this.on('rowdblclick', function(grid, row, e){this.taskWindow(grid.getSelectionModel().getSelected().data)});
	
	this.on('rowcontextmenu', function(grid, row, e){
		if (this.selModel.getCount()<2) this.selModel.selectRow(row);
		this.srData=this.store.getAt(row).data;
		this.menu = new Ext.menu.Menu({
			items: ((Ext.getCmp('task_status').getValue()==-1) ? {text:'שחזר משימה', iconCls:'restore', scope:this, handler:this.restoreTaskPromt} : [{text:'עדכן משימה', iconCls:'edit', handler:function(){this.taskWindow(this.srData)}, scope:this },{text:'מחק משימה', iconCls:'delete', scope:this, handler:this.delTaskPromt}] )
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.delTaskPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var rec = this.getSelectionModel().getSelected();
		if (rec.data.owner_id!=user.user_id) {Ext.Msg.alert('אין הרשאות', 'רק בעל משימה יכול למחוק.'); return;}
		Ext.MessageBox.show({
			width:250, title:'למחוק משימה?', msg:'האם ברצונך למחוק את משימה זאת?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delTask(rec); }
		});
	}
	
	this.delTask = function(rec){
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=tasks/tasks&f=del_task&id='+rec.id,
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					row=this.store.indexOf(rec);
					this.view.getRow(row).style.border='1px solid red';
					Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){
						if (top.alertsStore && top.alertsStore.getById(rec.id)) top.alertsStore.remove(top.alertsStore.getById(rec.id));
						this.store.remove(rec);
						this.view.refresh();
					}, scope:this });
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.restoreTaskPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		var rec = this.getSelectionModel().getSelected();
		if (rec.data.owner_id!=user.user_id) {Ext.Msg.alert('אין הרשאות', 'רק בעל משימה יכול לשחזר.'); return;}
		
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'לשחזר משימה?', msg:'האם ברצונך לשחזר את המשימה הזאת?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.restoreTask(rec); }
		});
	}
	
	this.restoreTask = function(rec){
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=tasks/tasks&f=rest_task&id='+rec.id,
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					row=this.store.indexOf(rec);
					this.selModel.selectRow(row);
					this.view.getRow(row).style.border='1px solid green';
					Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){
						this.store.remove(rec);// this.store.remove(this.selModel.getSelected());
						this.view.refresh();
						// topMsg.msg('המשימה שוחזרה');
					}, scope:this });
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};	
};
Ext.extend(TasksGrid, Ext.grid.GridPanel);

Ms.tasks.run = function(cfg){
	var m=Ms.tasks;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		win = Crm.desktop.createWindow({ width:900,	height:600,	items: new TasksGrid()}, m);
	}
	win.show();
};