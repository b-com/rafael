var filesGrid; // for store reload from scan window
var filesFolders=['כללי','הזמנות','מכירות'];


SearchFilterField = Ext.extend(Ext.form.TwinTriggerField, {
	initComponent: function(){
		SearchFilterField.superclass.initComponent.call(this);
		this.on('keyup', this.keySearch, this);
		this.keyUpTask = new Ext.util.DelayedTask(this.onTrigger2Click, this);
	},
	emptyText:'חיפוש',
	enableKeyEvents:true,
	validationEvent:false,
	validateOnBlur:false,
	trigger1Class:'x-form-clear-trigger',
	trigger2Class:'x-form-search-trigger',
	hideTrigger1:true,
	width:180,

	keySearch: function(f,e){
		if (e.keyCode<48 && e.keyCode!=8) return;	// not word char or backspace
		else if (e.keyCode==13) { this.keyUpTask.cancel(); this.onTrigger2Click(); return; } // enter
		this.keyUpTask.delay(10);
	},
	
	onTrigger1Click: function(){
		this.store.filterBy(function(){return true;});
		this.triggers[0].hide();
	},

	onTrigger2Click: function(){
		var v=this.getRawValue().trim();
		if (v.length<1) { this.onTrigger1Click(); return; }
		//if (v.length<2) return;
		this.store.filterBy(function(record){
			if (record.data.name.indexOf(v)>-1) return true;
			else if (typeof(record.data.notes)!='undefined' && record.data.notes.indexOf && record.data.notes.indexOf(v)>-1) return true;
			else if (record.data.linkText && record.data.linkText.indexOf(v)>-1) return true;
			return false;
		});
		this.triggers[0].show();
	}
});

UploadWin = function(grid) {
	this.grid=grid;
	this.fileExt='';
	
	// if(this.grid.client_type=='contact' || this.grid.order_id) var showLinks=false; esle var showLinks=true;

	//add custom folders
	var folders=this.grid.store.collect('folder');
	for(var i=0; i<folders.length; i++){ if (!filesFolders.inArray(folders[i])) filesFolders.push(folders[i]); }
	
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',
		fileUpload:true,
		labelWidth:60,
		defaultType:'textfield',
		items: [
			{xtype:'fileuploadfield', name:'file', id:'file', emptyText:'בחר קובץ...', fieldLabel:'שם קובץ', anchor:'95%', allowBlank:false,
				regex:/^[^\/\\\?:*<>"|]+$/, invalidText:'שם קובץ אינו יכול להכיל את התווים הבאים:<br><b>\\ / * ? " &lt; &gt; |</b>',
				listeners: { scope:this,
					fileselected:function(fld,v){ 
						v = v.replace(/\//g, '\\');
						v = v.replace(/^([^\\]+\\)*/g, '');	// remove path
						var regs = /^(.+)\.(.{2,4})$/.exec(v);
						if (regs && regs[2]) { fld.el.dom.value=regs[1]; this.fileExt=regs[2]; }
						else fld.el.dom.value=v;
						fld.validate();
						fld.el.dom.setAttribute('name','file_name');
						fld.el.dom.removeAttribute('readOnly');
						fld.el.dom.style.color='black';
					}
				}
			 }
			,{xtype:'combo', hiddenName:'folder', fieldLabel:'תיקייה', store:filesFolders, value:'כללי', anchor:'95%', listClass:'rtl', triggerAction:'all', allowBlank:false }
			// ,this.linkedGrid
		]
	});
	
	if (this.grid.show_links) {
		var Record = Ext.data.Record.create(['id','type','name']);
		
		this.linksCombo = new Ext.ux.IconCombo({
			store:this.grid.linksStore, client_id:this.grid.client_id, client_type:this.grid.client_type, value:'', emptyText:'-בחר-', forceSelection:false, hiddenName:'link', valueField:'id', width:317
			,itemCls:'rtl',	displayField:'name', iconClsField:'icon', triggerAction:'all', mode:'remote'
		}); 
		
		this.linksCombo.on('select',function(combo, record, index){
			this.linkedGrid.store.insert(0, new Record({id:record.data.id,type:record.data.type,name:record.data.name})); 
		},this);
			
		this.linkedGrid={
			xtype:'grid',stripeRows:true,height:100,fieldLabel:'שייך ל-',cls:'ltr',enableColumnResize:false,enableHdMenu:false,anchor:'95%',
			store:new Ext.data.JsonStore({root:'data', fields:['id','type','name']}),
			border: false,
			hideHeaders:true,
			selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
			view: new Ext.grid.GridView(),
			columns: [
				{width:17, renderer:function(v,m,r){return '<button class="b-action delete" ext:qtip="בטל שיוך"></button>';}}
				,{dataIndex:'name',width:277}
			]
			,tbar:{items:[this.linksCombo]}
			,listeners:{
				rowclick:function(grid,row,e){
					var el = e.getTarget();
					if (el.tagName.toLowerCase()=='button') {
						var action=el.className.replace(/b-action /, '');
						if (action=='delete') {this.removelink(row);};
					}
				},scope:this
			}
		};
		
		this.removelink = function(row){
			this.linkedGrid.selModel.selectRow(row);
			this.linkedGrid.view.getRow(row).style.border='1px solid red';
			Ext.fly(this.linkedGrid.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.linkedGrid.store.remove(this.linkedGrid.selModel.getSelected());}, scope:this });
		};
	
		this.form.add(this.linkedGrid);
	}
	
	this.form.add({xtype:'textfield', name:'notes', fieldLabel:'הערות', anchor:'95%'});

	UploadWin.superclass.constructor.call(this, {
		title: 'העלאת מסמך', iconCls:'upload',	width:430,height:150,	modal:true,	resizable:false, layout:'fit',	plain:true,	bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		var folder = this.form.form.findField('folder').getRawValue().trim();
		this.form.form.findField('folder').setValue(folder);
		
		var fileName = this.form.form.findField('file_name').getRawValue().trim();
		if (this.fileExt) fileName=fileName+'.'+this.fileExt;
		
		// if (this.form.form.findField('link').getRawValue().trim()=='') this.form.form.findField('link').setValue(''); // clear link value
		
		// check if file already exists (in folder)
		this.overwriteRow=0;
		if (overwrite) this.overwriteRow = this.grid.fileExists(folder+'/'+fileName);
		else if (!overwrite && this.grid.fileExists(folder+'/'+fileName)) {
			var fileExistWin = new Ext.Window({
				title:'קובץ כבר קיים', width:300, plain:true, modal:true, border:false, closable:true, buttonAlign:'center', bodyStyle:{direction:'rtl',padding:'10px'},
				html:'קובץ בשם <b>'+fileName+'</b> כבר קיים בתקייה <b>'+folder+'</b>',
				buttons:[
					 {text:'החלף', handler:function(){fileExistWin.close(); this.submitForm(true);}, scope:this}
					,{text:'ביטול', handler:function(){fileExistWin.close();}}
				]				
			});
			fileExistWin.show();
			return;
		}
		
		var linked_data=[];
		if (this.grid.show_links) {
			if(this.grid.order_id) linked_data.push({id:this.grid.order_id,type:'order'});
			else this.linkedGrid.store.each(function(r){linked_data.push({id:r.data.id,type:r.data.type})});
		}
		this.form.form.submit({
			url: '?m=files/files_f&f=upload_files&client_id='+this.grid.client_id+(this.overwriteRow ? '&overwriteId='+this.overwriteRow.data.id :''),
			params:{client_type:this.grid.client_type,linked_data:Ext.encode(linked_data)},
			waitTitle: 'אנא המתן...',
			waitMsg: 'טוען את הקובץ...',
			scope:this,
			success: function(form, o){
				
				if (this.overwriteRow) this.grid.store.remove(this.overwriteRow);
				var rec = new this.grid.store.recordType({newRecord:true, folder:folder, name:fileName, id:o.result.id, user:o.result.user, size:o.result.size, date:Date.parseDate(o.result.date,'d/m/y H:i'), /*link:form.findField('link').getValue(), linkText:form.findField('link').getRawValue(),*/ notes:form.findField('notes').getRawValue()});
				rec.commit();
				this.grid.store.addSorted(rec);
				this.close();
				// this.grid.load_policiesList=true;
				Ext.fly(this.grid.view.getRow(this.grid.store.indexOf(rec))).highlight("#b6ff6c", {attr:'background-color', easing:'easeIn', duration:3 });
			},
			failure: function(r,o){
				this.close();
				ajRqErrors(o.result);
			}
		});
	}	
};
Ext.extend(UploadWin, Ext.Window, {});

EditWin = function(grid) {
	this.grid=grid;
	this.fileExt='';
	this.record=grid.getSelectionModel().getSelections()[0];
	
	var regs = /^(.+)\.(.{2,4})$/.exec(this.record.data.name);
	if (regs && regs[2]) { this.file_name=regs[1]; this.fileExt=regs[2]; }
	else this.file_name=this.record.data.name;

	//add custom folders
	var folders=this.grid.store.collect('folder');
	for(var i=0; i<folders.length; i++){ if (!filesFolders.inArray(folders[i])) filesFolders.push(folders[i]); }
	
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',
		labelWidth:55,
		defaultType:'textfield',
		items: [
			{xtype:'textfield', name:'file_name', fieldLabel:'שם קובץ', value:this.file_name, anchor:'95%', regex:/^[^\/\\\?:*<>"|]+$/, invalidText:'שם קובץ אינו יכול להכיל את התווים הבאים:<br><b>\\ / * ? " &lt; &gt; |</b>'}
			,{xtype:'combo', hiddenName:'folder', fieldLabel:'תיקייה', store:filesFolders, value:this.record.data.folder, anchor:'95%', listClass:'rtl', triggerAction:'all', allowBlank:false }
			// ,this.linksCombo
			// ,this.linkedGrid
			// ,{xtype:'textfield', name:'notes', fieldLabel:'הערות', value:this.record.data.notes, anchor:'95%'}
			,{xtype:'hidden', name:'id', value:this.record.data.id }
			,{xtype:'hidden', name:'file_ext', value:this.fileExt }
		]
	});
	
	if (this.grid.show_links) {
		var Record = Ext.data.Record.create(['id','type','name']);

		this.linksCombo = new LinksCombo({
			store:this.grid.linksStore, client_id:this.grid.client_id, client_type:this.grid.client_type, value:'',emptyText:'-בחר-', forceSelection:false, hiddenName:'link', valueField:'id', width:317, fieldLabel:'הוסף שיוך'
			,itemCls:'rtl',	displayField:'name', iconClsField:'icon', triggerAction:'all', mode:'remote'
		}); 
		
		this.linksCombo.on('select',function(combo, record, index){
			this.linkedStore.insert(0, new Record({id:record.data.id,type:record.data.type,name:record.data.name})); 
		},this);
		
		this.linkedStore =  new Ext.data.JsonStore({
			url:'?m=crm/crm_f&f=linked_short_list_load&id='+this.record.data.id+'&type=file',
			root:'data', fields:['id','type','name']
		});
		
		this.linkedGrid={
			xtype:'grid',stripeRows:true,height:100,fieldLabel:'שייך ל-',cls:'ltr',enableColumnResize:false,enableHdMenu:false,anchor:'95%',
			store:this.linkedStore,
			border: false, 
			hideHeaders:true,
			selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
			view: new Ext.grid.GridView(),
			columns: [
				{width:17, renderer:function(v,m,r){return '<button class="b-action delete" ext:qtip="בטל שיוך"></button>';}} 
				,{dataIndex:'name',width:280}
			]
			,tbar:{items:[this.linksCombo]}
			,listeners:{
				rowclick:function(grid,row,e){
					var el = e.getTarget();
					if (el.tagName.toLowerCase()=='button') {
						var action=el.className.replace(/b-action /, '');
						if (action=='delete') {this.removelink(row);};
					}
				},scope:this
			}
		};

		this.linkedGrid.store.load();
		
		this.removelink = function(row){
			this.linkedGrid.selModel.selectRow(row);
			this.linkedGrid.view.getRow(row).style.border='1px solid red';
			Ext.fly(this.linkedGrid.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.linkedGrid.store.remove(this.linkedGrid.selModel.getSelected());}, scope:this });
		};
		this.form.add(this.linkedGrid);
	}
	
	this.form.add({xtype:'textfield', name:'notes', fieldLabel:'הערות', value:this.record.data.notes, anchor:'95%'});
	
	
	EditWin.superclass.constructor.call(this, {
		title: 'עריכת מסמך',
		iconCls:'edit_file',
		width:430,
		height:150,
		modal:true,
		resizable:false,
		layout:'fit',
		plain:true,
		bodyStyle:'padding:5px;',
		buttonAlign:'center',
		items: this.form,
		buttons: [{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		var folder = this.form.form.findField('folder').getRawValue().trim();
		this.form.form.findField('folder').setValue(folder);
		
		var fileName = this.form.form.findField('file_name').getRawValue().trim();
		if (this.fileExt) fileName=fileName+'.'+this.fileExt;
		
		// if (this.form.form.findField('link').getRawValue().trim()=='') this.form.form.findField('link').setValue(''); // clear link value
		
		// check if file already exists (in folder)
		var fileExists=false;
		this.grid.store.each(function(r){
			if (folder==r.data.folder && fileName==r.data.name && this.record.data.id!=r.data.id) { fileExists=r; return; }
		}, this);
		if (fileExists) {
			Ext.Msg.show({title:'קובץ כבר קיים', msg:'קובץ בשם <b>'+fileName+'</b> כבר קיים בתקייה <b>'+folder+'</b><br>בחר שם אחר.', minWidth:400, modal:true, icon:Ext.Msg.INFO, buttons:Ext.Msg.OK});
			return;
		}
		
		var linked_data=[];
		if (this.grid.show_links) {
			if(this.grid.order_id) linked_data.push({id:this.grid.order_id,type:'order'});
			else this.linkedStore.each(function(r){linked_data.push({id:r.data.id,type:r.data.type})});
		}
		
		this.form.form.submit({
			url: '?m=files/files_f&f=update_file&client_id='+this.grid.client_id,
			params:{client_type:this.grid.client_type,linked_data:Ext.encode(linked_data)},
			waitTitle: 'אנא המתן...',
			waitMsg: 'טוען...',
			scope:this,
			success: function(form, o){
				if (folder==this.record.data.folder) { // same folder
					this.record.beginEdit();
					this.record.set('name', fileName);
					this.record.set('notes', this.form.form.findField('notes').getValue());
					// this.record.set('link', this.form.form.findField('link').getValue());
					// this.record.set('linkText', this.form.form.findField('link').getRawValue());
					this.record.endEdit();
					this.record.commit();
					this.grid.getSelectionModel().deselectRow(this.grid.store.indexOf(this.record));
				}else { // different folder
					rCopy=this.record.copy();
					this.grid.store.remove(this.record);
					rCopy.set('folder', folder);
					this.grid.store.addSorted(rCopy);
					this.record=rCopy;
				}
				this.close();
				// this.grid.load_policiesList=true;
				Ext.fly(this.grid.view.getRow(this.grid.store.indexOf(this.record))).highlight("#b6ff6c", {attr:'background-color', easing:'easeIn', duration:2 });
			},
			failure: function(r,o){this.close();ajRqErrors(o.result);}
		});
	}	
};
Ext.extend(EditWin, Ext.Window, {});

EditFolderWin = function(folderName, grid) {
	var folders=grid.store.collect('folder');
	for(var i=0; i<folders.length; i++){ if (!filesFolders.inArray(folders[i])) filesFolders.push(folders[i]); }
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',
		labelWidth:55,
		defaultType:'textfield',
		items:[
			{xtype:'textfield', name:'newName', fieldLabel:'תיקייה', value:folderName, anchor:'95%'}
			,{xtype:'hidden', name:'oldName', value:folderName}
		]
	});
	EditFolderWin.superclass.constructor.call(this, {
		title:'שינוי שם תקייה', iconCls:'edit_file', width:430, modal:true, resizable:false, plain:true, layout:'fit', bodyStyle:'padding:5px;', buttonAlign:'center', 
		items:this.form,
		buttons:[{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		var newName = this.form.form.findField('newName').getValue().trim();
		this.form.form.findField('newName').setValue(newName);
		if (newName==folderName) { this.close(); return; } // no change
		for(var i=0; i<filesFolders.length; i++) if(newName==filesFolders[i]){ Ext.Msg.alert('שגיאה', 'תקייה בשם <b>'+newName+'</b> כבר קיימת<br>אנא בחר שם אחר.'); return; }
		this.form.form.submit({
			url: '?m=files/files_f&f=update_folder_name&client_id='+grid.client_id,
			waitTitle:'אנא המתן...', waitMsg:'טוען...', scope:this,
			success:function(){ this.close(); grid.store.reload(); },
			failure:function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}
};
Ext.extend(EditFolderWin, Ext.Window, {});

MoveToClientWin = function(grid) {
	this.clientType=grid.client_type;
	
	this.clientsStore = new Ext.data.JsonStore({url:'?m=crm/fax_f&f=select_client&clientType='+this.clientType, totalProperty:'total', root:'data', fields:['id','name']});
	this.clientsCombo = new Ext.form.ComboBox({
		fieldLabel:(this.clientType=='client' ? 'לקוח' : 'מעסיק'), 
		hiddenName:'toClient', store:this.clientsStore, valueField:'id', displayField:'name', minChars:2, forceSelection:true,
		selectOnFocus:true, width:280, loadingText:'טוען...', emptyText:'חיפוש...', listClass:'rtl', //triggerAction:'all', allowBlank:false,
		listeners:{
			select: function(combo, record, index){
				if (this.clientsCombo.getValue()) {
					this.loadClientData(this.clientsCombo.getValue());
					Ext.getCmp('movefile_btn').enable();
				}else Ext.getCmp('movefile_btn').disable();
			},scope:this
		}
	});
	
	this.loadClientData=function(client_id){
		Ext.Ajax.request({scope:this
			,url:'?m=crm/fax_f&f=select_police&client_id='+client_id+'&clientType='+this.clientType
			,success: function(r,o){
				r=Ext.decode(r.responseText);
				
				if (r&&r.folders&&r.folders.length){
					var folders=[];
					Ext.apply(folders, filesFolders);
					for(var i=0; i<r.folders.length; i++) if (!folders.inArray(r.folders[i])) folders.push(r.folders[i]);
					this.folderCombo.store.loadData(folders);
					this.folderCombo.setValue('כללי');
				}
			},failure:function(r){}
		});
	};
	
	this.folderCombo = new Ext.form.ComboBox({fieldLabel:'תיקייה', hiddenName:'file_cat', store:filesFolders, value:'כללי', triggerAction:'all', width:280, mode:'local', listClass:'rtl', allowBlank:false});
		
	var records = grid.getSelectionModel().getSelections();
	var ids=[];
	Ext.each(records, function(r){ids.push(r.data.id);});

	this.form = new Ext.form.FormPanel({
		baseCls: 'x-plain',
		labelWidth:80,
		defaultType: 'textfield',
		items: [
			this.clientsCombo, this.folderCombo
			// ,{xtype:'checkbox', boxLabel:'השאר עותק אצל הלקוח הנוכחי', name:'copy', checked:false}
			,{xtype:'hidden', name:'ids', value:ids.join(',')}
		]
	});

	MoveToClientWin.superclass.constructor.call(this, {
		title: 'העברת קובץ ללקוח אחר', iconCls:(this.clientType=='client'?'user-link':'business_link'),
		width:400, modal:true, resizable:false, plain:true, bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{id:'movefile_btn', text:'<b>העבר</b>', disabled:true, handler:function(){this.submitForm()}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function() {
		if(!this.form.form.isValid()) return;
		this.form.form.submit({scope:this, waitTitle:'אנא המתן...', waitMsg:'טוען...', 
			url:'?m=files/files_f&f=change_client&client_id='+grid.client_id+'&clientType='+this.clientType,
			success:function(){ this.close(); grid.store.reload(); /*grid.load_policiesList=true;*/},
			failure:function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}
};
Ext.extend(MoveToClientWin, Ext.Window, {});

FilesGrid = function(client_id,client_type,group_email,short_list) {
	this.client_id=client_id;
	this.short_list=(short_list?true:false);
	this.order_id='';
	this.show_links=true;
	this.additionalMenu = false;
	if(client_type) this.client_type=client_type; else this.client_type='client';
	
	if (this.client_type=='contact') this.show_links=false;
	
	// this.load_policiesList=false;
	
	// if(this.client_type=='group') clientEmail=group_email;
	
	this.linksStore =  new Ext.data.JsonStore({
		url:'?m=crm/crm_f&f=links_short_list_load&client_id='+this.client_id+'&client_type='+this.client_type+'&show_orders=1',
		root:'data', fields:['id','type', 'name', 'icon']
	});
	
	this.store = new Ext.data.GroupingStore({
		proxy: new Ext.data.HttpProxy({
			url: '?m=files/files_f&f=files_tree&client_id='+this.client_id+'&client_type='+this.client_type,
			method:'GET'}),
		reader: new Ext.data.JsonReader({root:'data', totalProperty:'total'}, ['id','client_passport','client_name','folder','name','link','linkText',{name:'date',type:'date', dateFormat:'d/m/y H:i'},'size','user','notes','contact_email']),
		remoteSort:true,
		sortInfo:{field:'date', direction:'DESC'},
		groupField:'folder'
	});

	this.searchFilter = new SearchFilterField({store:this.store, width:100/*, hidden:(this.short_list)*/});

	this.store.on('beforeload', function(){
		this.store.removeAll();
	}, this);

	this.store.on('load', function(){	// check for errors
		var callback=function() { this.store.load({ params:{start:0, limit:500} }); };
		if (this.store.getCount()<50) this.view.expandAllGroups();
		ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
	}, this);

	this.fileExists = function(fullName){
		var exists=false;
		this.store.each(function(r){
			if (fullName==r.data.folder+'/'+r.data.name) { exists=r; return; }
		});
		return exists;
	}

	this.previewWinHide = function(){
		var win = Ext.getCmp('previewWin'+this.client_id);
		if (win && win.isVisible()) win.hide();
	}
	this.delFilePromt = function(){
		this.previewWinHide();
		if (!this.getSelectionModel().getCount()) return;
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.folder+'/<b>'+r.data.name+'</b>');});
		Ext.MessageBox.show({
			width:400, title:'למחוק קבצים?', msg:'האם ברצונך למחוק את קבצים:<br>'+names.join(',<br>')+' ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delFile(records); }
		});
	};

	this.delFile = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=files/files_f&f=del_file&client_id='+this.client_id+'&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
						// topMsg.msg('פקס נמחק','פקס <b>'+d.data.from+'</b> <span dir=ltr>'+d.data.date+'</span>' +' נמחק בהצלחה');
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};

	this.ShowUploadWin = function(files_list){
		var files_list=(files_list?files_list:this);
		this.previewWinHide();
		var uploadWin=new UploadWin(files_list);
		uploadWin.show();
	};
	
	this.ShowEditWin = function(){
		this.previewWinHide();
		var editWin=new EditWin(this);
		editWin.show();
	};	
	
	this.showEditFolderWin = function(folder){
		this.previewWinHide();
		var editFolderWin=new EditFolderWin(folder, this);
		editFolderWin.show();
	};

	this.emailWindow = function(d){
		this.previewWinHide();
		var sel=this.getSelectionModel().getSelections();
		if (!sel[0].data) return;
		
		var win;
		if (this.client_type=='contact') {
			win=new MailWindow((sel[0].data.contact_email?sel[0].data.contact_email:''),sel);
			win.show();
		}
		else{
			var timestart=Math.round(new Date().getTime()/1000);
			var files_ids=[];
			var files_names=[];
			Ext.each(sel, function(r){
				files_ids.push(r.data.id);
				files_names.push(r.data.name);
			});
			Ext.Ajax.request({
				url: '?m=crm/contacts_f&f=copy_files_to_mail_compose&copy_from_folder=files&client_id='+this.client_id,
				params:{timestart:timestart,attachments:files_ids.toString()},
				success: function(r){
					rt=Ext.decode(r.responseText);
					if (rt.success) {
						var data={json:{subject:(d.client_name?'לקוח '+d.client_name:'')+(d.client_passport?', '+d.client_passport:'')+' קבצים: '+files_names.toString(),getMail:true}};
						win=new MailComposeWindow({client_id:this.client_id,link:this.link ,client_type:this.client_type,timestart:timestart,type:'mail'},'mail_create',{},'',data); win.show();
						win.show();
					};
				},
				failure: function(r){Ext.Msg.alert('שגיאה','בעית תקשורת');},
				scope: this
			});	
		}
	}
	
	this.sendFaxWin = function(){
		this.previewWinHide();
		if (!CFG.user.fax_active) { Ext.Msg.alert('שרות פקס', 'לקבלת שרות פקס התקשרו: 074-7144333'); return; }
		var sel=this.getSelectionModel().getSelections();
		if (!sel[0].data) return;
		
		var win;
		if (this.client_type=='contact') win=new SendFax({grid:this, file:sel[0].data, link_to:'adr_book'});
		else win=new SendFax({grid:this, file:sel[0].data});
		win.show();
	};
	
	this.tbar = [
		this.searchFilter
		, (this.short_list?'':'-') , { text:(this.short_list?'':'העלאת מסמך'), tooltip:'העלאת מסמך', iconCls:'upload', scope:this, handler:function(){this.ShowUploadWin()}}
		, '-' , { text:(this.short_list?'':'סריקת מסמך'), tooltip:'סריקת מסמך', iconCls:'scan', scope:this, /*disabled:(!Ext.isIE && !Ext.isGecko),*/ handler:function(){
			this.previewWinHide();
			filesGrid=this;
			var win=window.open('http://'+location.host+'/crm/?m=files/scan&client_id='+this.client_id+'&client_type='+this.client_type+(this.order_id?'&order_id='+this.order_id:''), 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
			win.focus();
		} }
		, (this.short_list?'':'-') ,{ text:'ערוך', id:'edit_file', hidden:(this.short_list), iconCls:'edit_file', handler:this.ShowEditWin, disabled:true, scope:this}
		, (this.short_list?'':'-') ,{ text:'הורד למחשב', id:'dl_file', hidden:(this.short_list), iconCls:'download', handler:function(){var d=this.getSelectionModel().getSelections()[0].data; location.href='http://'+location.host+'/crmdownload/'+d.id+'/'+encodeURIComponent(d.name);}, disabled:true, scope:this }
		, (this.short_list?'':'-') ,{ text:'שלח בדוא"ל', id:'send_mail', hidden:(this.short_list),iconCls:'mail_add', handler:function(){var d=this.getSelectionModel().getSelections()[0].data; this.emailWindow(d);}, disabled:true, scope:this }
		, (this.short_list?'':'-') ,{ text:'שלח בפקס', id:'send_fax', hidden:(this.short_list), iconCls:'fax-send', handler:this.sendFaxWin,  disabled:true, scope:this }
		, (this.short_list?'':'-') ,{ text:'מחק', id:'del_file', hidden:(this.short_list), iconCls:'delete', handler:this.delFilePromt, disabled:true, scope:this }
	];
	
	if(!this.short_list) this.bbar = new Ext.PagingToolbar({store:this.store, pageSize:500, displayInfo:true});
	
	this.notesRenderer = function(str, meta, record){
		meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(record.data.notes)+'" ext:qtitle="'+Ext.util.Format.htmlEncode(record.data.name)+'"';
		return record.data.notes;
	};

	FilesGrid.superclass.constructor.call(this, {
		border:false,
		minColumnWidth:18,
		enableColLock: false,
		loadMask: true,
		stripeRows:true,
		enableHdMenu:false,
		title: 'מסמכים',
		hidden:(this.short_list),
		cls:'ltr',
		autoExpandColumn:'name',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		view: new Ext.grid.GroupingView({
			groupTextTpl: '<span dir=rtl style="height:16px;line-height:16px; padding-right:18px; background:url(/crm/skin/icons/folder.gif) no-repeat top right; zoom:1; display:block;"><b>{group}</b></span>'
			,startCollapsed:true 
			,enableRowBody: true
			,forceFit:this.short_list
			,scrollOffset:(this.short_list?0:'undefined')
		}),

		columns:[
			{id:'folder', dataIndex:'folder', hidden:true }
			,{dataIndex:'id', width:41, hidden:(this.short_list),renderer:function(v,m,r){return '<button class="b-action new_win" ext:qtip="פתח בחלון חדש"></button>'+((/\.(pdf|jpg|jpeg|gif|png)$/).test(r.data.name)?'&nbsp;<button class="b-action preview" ext:qtip="הצג"></button>':'');} } // <button class="b-action download" ext:qtip="הורד למחשב"></button>&nbsp;
			,{header:'נוסף ב', id:'date', width:85, dataIndex:'date', sortable:true, css:'direction:ltr;', align:'right', renderer:Ext.util.Format.dateRenderer('d/m/y H:i') }
			,{header:'נוצר ע"י', id:'user', hidden:(this.short_list),width:75, dataIndex:'user', sortable:false}
			,{header:'גודל', id:'size', hidden:(this.short_list), width:55, dataIndex:'size', sortable:true, css:'direction:ltr;padding-left:5px;' }
			,{header:'הערות', id:'notes', width:220, hidden:(this.short_list), dataIndex:'notes', sortable:true, renderer:this.notesRenderer.createDelegate(this) }
			// ,{header:'שייך ל', id:'linkText', hidden:(this.short_list || this.client_type=='group' || this.client_type=='contact'), width:240, dataIndex:'linkText', sortable:true}
			,{header:'שם הקובץ', id:'name', dataIndex:'name', sortable:true, scope:this, 
				renderer:function(v,m,r){
					var regs=/^(.+)(\.[^\.]{2,4})$/.exec(v);
					return '<img unselectable=on src="/crm/skin/0.gif" class="file_icon '+(r.data.linkText?'link_add':'')+'" '+(r.data.linkText?'ext:qtip="'+r.data.linkText+'"':'')+'/> '+ '<img unselectable=on src="/crm/skin/0.gif" class="file_icon '+this.file_cls(v)+'"/> '+ (regs ? '<span dir=rtl>'+regs[1]+'</span><font color=silver>'+regs[2]+'</font>' : v);
				}.createDelegate(this)
			}
			,{dataIndex:'', width:30 }
		]
		
	});
	
	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('del_file').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('dl_file').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('edit_file').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('send_mail').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('send_fax').setDisabled(sm.getCount()!=1);
	}, this);
	
	this.preview = function(d) {
		var previewWin = Ext.getCmp('previewWin'+this.client_id);
		
		if (!previewWin) previewWin = new Ext.Window({
			id:'previewWin'+this.client_id, html:'<iframe id=prevIframe src="about:blank" style="width:100%; height:100%;" frameBorder=0></iframe>',
			x:5, y:50, width:(Ext.getBody().getWidth()-270), height:(Ext.getBody().getHeight()-55), plain:true, border:false, layout:'fit', closable:false,
			listeners: { hide:function(){Ext.get('prevIframe').dom.src='about:blank';} }
		});
		
		// i

		if ((/\.(pdf|jpg|jpeg|gif|png)$/i).test(d.name)) {
			this.prev_d=d;
			previewWin.setTitle('<div class="x-tool x-tool-close" style="float:right" onClick="Ext.getCmp(\'previewWin'+this.client_id+'\').close();"></div> <span class="span_icon '+this.file_cls(d.name)+'">'+d.name+'</span>');
			previewWin.show();
			var iframe = Ext.get('prevIframe');
			if (iframe.dom.src!='about:blank') iframe.dom.src='about:blank';
			iframe.dom.src='http://'+location.host+'/crm/?m=common/get&file_id='+d.id+'&file_name='+encodeURIComponent(d.name)+'&NOHD=1';
		}
	}
	
	this.on('rowclick', function(grid, row, e){
		var d=this.getSelectionModel().getSelected().data;

		var el = e.getTarget();
		if (el.tagName.toLowerCase()=='button') {
			var action=el.className.replace(/b-action /, '');
			if (action=='new_win') {
				var win=top.window.open('http://'+location.host+'/crm/?m=common/get&file_id='+d.id+'&file_name='+encodeURIComponent(d.name)+'&NOHD=1', 'file'+d.id, 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
				win.focus();
			}else if (action=='download') location.href='http://'+location.host+'/crm/?m=common/get&file_id='+d.id+'&file_name='+encodeURIComponent(d.name)+'&NOHD=1&download=1';
			else if (action=='preview') this.preview(d);
		}
		
		var previewWin = Ext.getCmp('previewWin'+this.client_id);
		if (previewWin && previewWin.isVisible()) {
			if ((/\.(pdf|jpg|jpeg|gif|png)$/i).test(d.name)) { if (this.prev_d!=d) this.preview(d); }
			else previewWin.close();
		}
	});
	
	this.on('rowdblclick', function(grid, row, e){
		var d=this.getSelectionModel().getSelected().data;
		if ((/\.(pdf|jpg|jpeg|gif|png)$/i).test(d.name)) this.preview(d);
		else {
			// var win=top.window.open('http://'+location.host+'/crm/crmview/'+d.id+'/'+encodeURIComponent(d.name), 'file'+d.id, 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0'); 
			var win=top.window.open('http://'+location.host+'/crm/?m=common/get&file_id='+d.id+'&file_name='+encodeURIComponent(d.name)+'&NOHD=1', 'file'+d.id, 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0'); 
			win.focus(); 
		}
	});

	
	this.append=function(id){
		var win=window.open('http://'+location.host+'/scan/?m=files/scan&client_id='+this.client_id+'&client_type='+this.client_type+'&append='+id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
		win.focus();
	}
	
	this.joinPdfs=function(sel){
		var ids=[];
		Ext.each(sel, function(r){ids.push(r.data.id);});

		Ext.Ajax.request({
			url: '?m=files/files_f&f=join_pdfs&client_id='+this.client_id+'&client_type='+this.client_type+'&ids='+ids.join(','),
			success: function(r){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					var rec = new this.store.recordType({newRecord:true, folder:d.folder, name:d.name, id:d.id, size:d.size, date:d.date, link:'', linkText:'', notes:''});
					rec.commit();
					this.store.addSorted(rec);
					Ext.fly(this.view.getRow(this.store.indexOf(rec))).highlight("#b6ff6c", {attr:'background-color', easing:'easeIn', duration:3 });
				}
			},
			failure: function(r){ Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	}
	
	this.moveToFolder = function(b){
		var previewWin = Ext.getCmp('previewWin'+this.client_id);

		if (previewWin && previewWin.isVisible()) previewWin.close();
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});
		Ext.Ajax.request({
			url: '?m=files/files_f&f=change_folder&newFolder='+encodeURIComponent(b.text)+'&client_id='+this.client_id+'&client_type='+this.client_type+'&ids='+ids.join(','),
			success: function(r){ if (!ajRqErrors(Ext.decode(r.responseText))) this.store.reload(); },
			failure: function(r){ Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	}	
	
	this.moveToClientWin = function(){
		var previewWin = Ext.getCmp('previewWin'+this.client_id);

		if (previewWin && previewWin.isVisible()) previewWin.close();
		if (!this.getSelectionModel().getCount()) return;

		var win=new MoveToClientWin(this);
		win.show();
	};

	this.on('rowcontextmenu', function(grid, row, e){
		var d=this.store.getAt(row).data;
		var sm=this.selModel;

		if (sm.getCount()>1) {
			this.menu = new Ext.menu.Menu({
				items: [{text:'שלח בדוא"ל', iconCls:'mail_add', scope:this, handler:function(){this.emailWindow(d)}}]
			});
			// check if all selected files are PDF and add button
			var allPdf=true;
			var sel=sm.getSelections();
			Ext.each(sel, function(r){ if (!(/\.pdf$/i).test(r.data.name)) allPdf=false; });
			if (allPdf) this.menu.add({text:'חבר לקובץ אחד', iconCls:'page_add', handler:function(){this.joinPdfs(sel);}, scope:this });

		}else {
			this.selModel.selectRow(row);
			
			this.menu = new Ext.menu.Menu({
				items: [
					 {text:'ערוך', iconCls:'edit_file', scope:this, handler:function(){ this.ShowEditWin(); } }
					,{text:'פתח בחלון חדש', iconCls:'new_win', scope:this, handler:function(){ var win=top.window.open('http://'+location.host+'/crm/?m=common/get&file_id='+d.id+'&file_name='+encodeURIComponent(d.name)+'&NOHD=1', 'file'+d.id, 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');  win.focus(); } }
					,{text:'הורד למחשב', iconCls:'download', scope:this, handler:function(){location.href='http://'+location.host+'/crm/?m=common/get&file_id='+d.id+'&file_name='+encodeURIComponent(d.name)+'&NOHD=1&download=1';} }
				]
			});
			this.menu.add({text:'שלח בדוא"ל', iconCls:'mail_add', scope:this, handler:function(){this.emailWindow(d)}});
			if ((/\.pdf$/i).test(d.name)) this.menu.add({text:'שלח בפקס', iconCls:'fax-send', handler:this.sendFaxWin, scope:this });			
			if ((/\.pdf$/i).test(d.name)) this.menu.add({text:'הוסף/הסר עמודים', iconCls:'page_add', handler:function(){this.append(d.id);}, scope:this });
			this.menu.add({text:'מחק', iconCls:'delete', scope:this, handler:function(){this.delFilePromt()} });
		}
		
		if (typeof(this.additionalMenu)=='function') this.menu.add(this.additionalMenu(this.store.getAt(row)));
		this.menu.addSeparator();
		
		var foldersMenu=[];
		var folders=this.store.collect('folder');
		for(var i=0; i<folders.length; i++) foldersMenu.push({text:folders[i], iconCls:'folder', handler:this.moveToFolder, scope:this});
		foldersMenu.push({
			xtype:'panel', layout:'table', baseCls:'x-plain', style:'direction:rtl',
			items: [{xtype:'label', text:'חדש:'},{xtype:'textfield', id:'newFolderName', width:90}
				,{xtype:'button', text:'OK', handler:function(){this.menu.hide(); this.moveToFolder({text:Ext.getCmp('newFolderName').getValue()});}, scope:this}
			]
		});
		this.menu.add({text:'&nbsp; &nbsp; &nbsp; העבר לתקייה', iconCls:'folder', scope:this, menu:{items:foldersMenu}});
		
		if (this.client_type!='contact') this.menu.add({text:'העבר ללקוח אחר', iconCls:'user-link', scope:this, handler:this.moveToClientWin});

		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	// folder context menu
	this.on('render', function(grid, row, e){
		this.view.mainBody.on('contextmenu', function(e){
			e.preventDefault();
			var hd = e.getTarget('.x-grid-group-hd', this.view.mainBody);
			if (hd){
				var folder = hd.parentNode.id.match(/folder-(.+)/)[1];
				this.groupHeaderMenu = new Ext.menu.Menu({
					items:[{text:'שנה שם תקייה', iconCls:'folder', scope:this, handler:function(){this.showEditFolderWin(folder)}}]
				});
				this.groupHeaderMenu.showAt(e.getXY());
			}
		}, this);
	});
	
	this.file_cls = function(file) {
		var regs = /^(.+)\.(.{3,4})$/.exec(file);
		if (!regs || !regs[2]) return 'file_other';
		
		var ext = regs[2].toLowerCase();
		if (ext=='doc' || ext=='docx' || ext=='wri') return 'file_doc';
		else if (ext=='xls' || ext=='xlsx' || ext=='csv') return 'file_xls';
		else if (ext=='pdf') return 'file_pdf';
		else if (ext=='ppt' || ext=='pps') return 'file_ppt';
		else if (ext=='zip' || ext=='arj') return 'file_zip';
		else if (ext=='rar') return 'file_rar';
		else if (ext=='jpg' || ext=='jpeg') return 'file_jpg';
		else if (ext=='gif') return 'file_gif';
		else if (ext=='png') return 'file_png';
		else if (ext=='tif' || ext=='tiff') return 'file_tif';
		else if (ext=='exe') return 'file_exe';
		else if (ext=='msg') return 'file_msg';
		else return 'file_other';
	}		
};
Ext.extend(FilesGrid, Ext.grid.GridPanel);

Ms.files.run = function(cfg){
	var m=Ms.files;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
	
		var filesList = new FilesGrid(-1,'archive');//-1 Fictive client_id
		
		filesList.expenseWindow = function(rec){var win=new ExpenseWindow({
			file_id:rec.data.id, callBack:function(){rec.set('linkText','שיוך בוצע')}.createDelegate(this) 
		}); win.show();}

		filesList.show_links = false;
		
		filesList.additionalMenu = function(rec){return [
			{text:'מסמך ה"ח חדש', iconCls:'accounting', scope:filesList, 
				menu:{
					items:{text:'הוצאה חדשה', iconCls:'police_add', scope:filesList, handler:function(){
						if (rec.data.linkText) Ext.MessageBox.show({
							width:320, title:'אזהרה!', msg:'המסמך שוייך. האם ברצונך לבצע שיוך נוסף?', icon: Ext.MessageBox.WARNING, buttons:Ext.MessageBox.YESNO, scope:this,
							fn: function(btn) {	if(btn=='yes') filesList.expenseWindow(rec)	}
						});
						else filesList.expenseWindow(rec)
						}}
					}
				},
			]
		};
		
		filesList.store.load();
		
	
		win = Crm.desktop.createWindow({ width:900,	height:600,	items:filesList}, m);
	}
	win.show();
};