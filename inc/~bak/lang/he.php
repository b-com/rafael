<?
$LNG['ln']=$ln;	# current language

# Common
$LNG['Home']	= '����';
$LNG['Add']		= '����';
$LNG['AddNew']	= '���� ���';
$LNG['Edit']	= '����';
$LNG['Delete']	= '���';
$LNG['Save']	= '����';
$LNG['Cancel']	= '�����';
$LNG['Title']	= '�����';
$LNG['Date']	= '�����';
$LNG['Added']	= '����';
$LNG['Language']= '���';
$LNG['Weight']	= '����';
$LNG['Align']	= '�����';
$LNG['File']	= '����';
$LNG['Active']	= '����';
$LNG['ActiveF']	= '����';
$LNG['Content']	= '����';
$LNG['Category']= '�������';
$LNG['Preview']	= '���� ������';
$LNG['View']	= '���';
$LNG['All']		= '���';
$LNG['Select']	= '���';
$LNG['List']	= '�����';
$LNG['Are_you_sure']='?��� ����';
$LNG['read_more']	= '���� &gt;&gt;&gt;';
$LNG['Short_Text']	= '���� ���';
$LNG['Full_Text']	= '���� ���';
$LNG['Description']	= '�����';


$LNG['all_news']		= '&lt;&lt;&lt; �� ������';
$LNG['all_announces']	= '&lt;&lt;&lt; �� �������';


# Block names
$LNG['Pages']	= '������';
$LNG['Block']	= '����';
$LNG['News']	= '�����';
$LNG['Announces']='������';


# Users
$LNG['Username']	= '�� �����';
$LNG['Password']	= '�����';
$LNG['Password_check']	= '����� ����';
$LNG['Login']		= '�����';
$LNG['Logout']		= '�����';
$LNG['Active']		= '����';
$LNG['FullName']	= '�� ���';
$LNG['FirstName']	= '�� ����';
$LNG['LastName']	= '�� �����';
$LNG['Email'] 		= '���"�';
$LNG['Company'] 	= '����';
$LNG['Tel1'] 		= '����� 1';
$LNG['Tel2'] 		= '����� 2';
$LNG['Fax'] 		= '���';

# Login errors
$LNG['Missing_username_or_password']	= '��� ��� �� �� ����� ������';
$LNG['Username_or_password_not_valid']	= '�� ����� �� ����� �� ������';
$LNG['You_are_logged_in_as']			= '����';


# Pages
$LNG['Filter']		= '�����';
$LNG['New_page']	= '���� ���';
$LNG['Pages_List']	= '����� ������';
$LNG['Edit_Page']	= '���� ����';
$LNG['Type']		= '���';
$LNG['Image']		= '�����';
$LNG['Image_align']	= '��� �����';
$LNG['Preview_only_warning'] = '���� ����, ������� �� �����';


# Announces
$LNG['Edit_announce']	= '���� �����';
$LNG['Add_announce']	= '���� �����';


# Search
$LNG['Search']			= '�����';
$LNG['Search_on_site']	= '����� ����';
$LNG['Found_in_pages']	= '����� �������';
$LNG['Found_in_news']	= '����� ������';
$LNG['Nothing_found']	= '�� ����� ������';

# Blocks
$LNG['Select_block_note']	= '(Block type - if selected, other fields will be ignored)';

# Backup
$LNG['Backup_Database']	= 'Backup Database';


$LNG['']	= '';
$LNG['']	= '';
$LNG['']	= '';

?>