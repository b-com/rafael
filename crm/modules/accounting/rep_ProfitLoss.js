ProfitLoss_rep = function(cfg) {


this.typeRenderer = function (str) {var store_types = {part:'חלופים',material:'חומרים',service:'עבודה',fserv:'עבודת חוץ',other:'אחר'}; return store_types[str];}

	this.repPrint = function() {
		var d_from=this.form.find('name','d_from')[0].getValue().format('Y-m-d');
		var d_to=this.form.find('name','d_to')[0].getValue().format('Y-m-d');
		// var pname=(this.form.find('name','pname')[0].checked ? 1 : 0);
		// var status=this.form.find('hiddenName','status')[0].getValue();
		//var q=encodeURIComponent(this.searchField.getRawValue().trim());
		//var SortState = this.loader.getLoader();
		window.open('?m=accounting/reports&f=ProfitLossRep&print=1&d_from='+d_from+'&d_to='+d_to);
	};
	
	this.exportWindow = function(){
		 
		var d_from=this.form.find('name','d_from')[0].getValue().format('Y-m-d');
		var d_to=this.form.find('name','d_to')[0].getValue().format('Y-m-d');
		 //var pname=(this.form.find('name','pname')[0].checked ? 1 : 0);
		 //var status=this.form.find('hiddenName','status')[0].getValue();
		//var q=encodeURIComponent(this.searchField.getRawValue().trim());
		//var SortState = this.store.getSortState();
		window.open('?m=accounting/reports&f=ProfitLossRep&export=1&d_from='+d_from+'&d_to='+d_to);
	};
	
	this.form = new Ext.form.FormPanel({
		labelWidth:80, height:36, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
		items: [{layout:'table', layoutConfig: {columns:9}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
					items:[
						{xtype:'label', text:'תאריך דו"ח בין:', style:'padding-right:10px;'},
						{xtype:'datefield', name:'d_from', width: 85, format:'d/m/Y', value:(from ? from : month_first()), cls:'ltr', allowBlank:false},//, vtype:'daterange'
						{xtype:'label', text:'ל:'},
						{xtype:'datefield', name:'d_to',width: 85, format:'d/m/Y', value:(to ? to : month_last()), cls:'ltr', allowBlank:false},//, vtype:'daterange'
						
						{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.loader.load(this.tree.root);}, scope:this, width: 90},
						{xtype:'button', cls:'btnRight', id:'invoice_prn_btn', style:'margin-right:8px;', iconCls:'print', text:'הדפס דו"ח', disabled: false, width: 90, handler:function(){this.repPrint()}, scope: this},
						{xtype:'button', cls:'btnRight', id:'invoice_exl_btn', style:'margin-right:8px;', iconCls:'excel', text:'Excel-ייצוא ל', disabled: false, width: 90, handler:function(){this.exportWindow()}, scope: this}
				
					]
				}
		]
	});
	
	this.loader = new Ext.tree.TreeLoader({
		dataUrl: '?m=accounting/reports&f=ProfitLossRep',
		
		preloadChildren:true,
		uiProviders:{'col': Ext.tree.ColumnNodeUI}
	});
	

	this.loader.on("beforeload", function(treeLoader, node) {
		this.loader.baseParams.group_id = node.attributes.group_id;
		this.loader.baseParams.type = node.attributes.type;
		this.loader.baseParams.group_name = node.attributes.group_name;
		this.loader.baseParams.d_from=this.form.find('name','d_from')[0].getValue();
		this.loader.baseParams.d_to=this.form.find('name','d_to')[0].getValue();
	}, this);	
	
	this.tree = new Ext.ux.tree.TreeGrid({
        enableDD:true,
		width:'100%',
		region:'center',
		singleExpand:true,
		columnResize:false,
		enableSort:false,
		enableHdMenu:false,
		loader: this.loader,
		cls:'rtl',
		selModel: new Ext.tree.MultiSelectionModel({
			listeners: {
				selectionchange: function(sm,node){if(node[0] && node[0].leaf!==true) sm.unselect(node[0]);}
			}
		}),
        columns:[
			{header:'תאור', dataIndex:'type_name', width:130, sortType:'asFloat', align:'right'},
			{ dataIndex:'type', width:0, sortType:'asFloat', align:'right'},
			{header:"קבוצה", dataIndex:'group_name', width:90, align:'right'}
			//{header:"תאריך", dataIndex:'sup_doc_date', width:90, sortType:'asFloat', align:'right'}
			,{header:"שם פריט", dataIndex:'prod_name', width:150, align:'right'}
			//,{header:"מס' חשבונית", dataIndex:'sup_doc_num', width:90, cls:'invoice', align:'right'}
			,{header:"סכום", dataIndex:'Sum', width:90, sortType:'asFloat', renderer:Ext.util.Format.ilMoney, align:'right'}
			//,{header:'מע"מ', dataIndex:'VAT_sum', width:90, sortType:'asFloat', renderer:Ext.util.Format.ilMoney, align:'right'}
			//,{header:"", dataIndex:'file_id', width:18, renderer:function(v){if (v) return '<img src="skin/icons/preview.gif">';}}
		]
    });
	this.tree1 = new Ext.ux.tree.TreeGrid({
        enableDD:true,
		width:'100%',
		region:'center',
		singleExpand:true,
		columnResize:false,
		enableSort:false,
		enableHdMenu:false,
		loader: this.loader,
		cls:'rtl',
		selModel: new Ext.tree.MultiSelectionModel({
			listeners: {
				selectionchange: function(sm,node){if(node[0] && node[0].leaf!==true) sm.unselect(node[0]);}
			}
		}),
        columns:[
			{header:'תאור', dataIndex:'type_name', width:130, sortType:'asFloat', align:'right'},
			{ dataIndex:'type', width:0, sortType:'asFloat', align:'right'},
			{header:"קבוצה", dataIndex:'group_name', width:90, align:'right'}
			//{header:"תאריך", dataIndex:'sup_doc_date', width:90, sortType:'asFloat', align:'right'}
			,{header:"שם פריט", dataIndex:'prod_name', width:150, align:'right'}
			//,{header:"מס' חשבונית", dataIndex:'sup_doc_num', width:90, cls:'invoice', align:'right'}
			,{header:"סכום", dataIndex:'Sum', width:90, sortType:'asFloat', renderer:Ext.util.Format.ilMoney, align:'right'}
			//,{header:'מע"מ', dataIndex:'VAT_sum', width:90, sortType:'asFloat', renderer:Ext.util.Format.ilMoney, align:'right'}
			//,{header:"", dataIndex:'file_id', width:18, renderer:function(v){if (v) return '<img src="skin/icons/preview.gif">';}}
		]
    });
	
	this.tree.on('contextmenu', function(node, e){
		if(node.leaf!==true) {e.stopEvent(); return;}
		var sm = this.tree.getSelectionModel();
		sm.select(node,true,true);
		if(sm.getSelectedNodes().length!=1 || sm.getSelectedNodes()[0].attributes.file_id) {e.stopEvent(); return;}
		
		this.menu = new Ext.menu.Menu({
			width:120,
			items: [
			{text:'סריקת מסמך', iconCls:'scan', handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/scan&client_id='+sm.getSelectedNodes()[0].attributes.client_id+'&acc_doc='+sm.getSelectedNodes()[0].attributes.id+'&client_type=client&folder='+encodeURIComponent('הוצאות')+'&file_name='+encodeURIComponent("חשבונית ספק מס' "+sm.getSelectedNodes()[0].attributes.sup_doc_num), 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.scanNode = sm.getSelectedNodes()[0];
					win.callBack = function() {this.scanNode.parentNode.reload();};
					win.focus();
				}, scope:this}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);

	ProfitLoss_rep.superclass.constructor.call(this, {border:false, layout:'border', region:'center', items:[this.form,this.tree]})

	this.preview = function(d) {
		var previewWin = Ext.getCmp('previewWin');
		if (!previewWin) previewWin = new Ext.Window({
			id:'previewWin', html:'<iframe id=prevIframe src="about:blank" style="width:100%; height:100%;" frameBorder=0></iframe>',
			x:5, y:50, width:(Ext.getBody().getWidth()-270), height:(Ext.getBody().getHeight()-55), plain:true, border:false, layout:'fit', closable:false,
			listeners: { hide:function(){Ext.get('prevIframe').dom.src='about:blank';} }
		});
		this.prev_d=d;
		previewWin.setTitle('<div class="x-tool x-tool-close" style="float:right" onClick="Ext.getCmp(\'previewWin\').close();"></div> <span class="span_icon file_pdf">'+"חשבונית ספק מס' "+d.sup_doc_num+'</span>');
		previewWin.show();
		var iframe = Ext.get('prevIframe');
		if (iframe.dom.src!='about:blank') iframe.dom.src='about:blank';
		iframe.dom.src='http://'+location.host+'/crmview/'+d.file_id+'/'+encodeURIComponent("חשבונית ספק מס' "+d.sup_doc_num);
	}
	
	this.tree.on('dblclick', function(node, e){
		var d=node.attributes; if (d.file_id) this.preview(d);
	},this);

};
Ext.extend(ProfitLoss_rep, Ext.Panel);