<?
$LNG['ln']=$ln;	# current language

# Common
$LNG['Home']	= 'Home';
$LNG['Add']		= 'Add';
$LNG['AddNew']	= 'Add New';
$LNG['Edit']	= 'Edit';
$LNG['Delete']	= 'Delete';
$LNG['Save']	= 'Save';
$LNG['Cancel']	= 'Cancel';
$LNG['Title']	= 'Title';
$LNG['Date']	= 'Date';
$LNG['Added']	= 'Added';
$LNG['Language']= 'Language';
$LNG['Weight']	= 'Weight';
$LNG['Align']	= 'Align';
$LNG['File']	= 'File';
$LNG['Active']	= 'Active';
$LNG['ActiveF']	= 'Active';
$LNG['Content']	= 'Content';
$LNG['Category']= 'Category';
$LNG['Preview']	= 'Preview';
$LNG['All']		= 'All';
$LNG['Select']	= 'Select';
$LNG['Are_you_sure']='Are you sure?';
$LNG['read_more']	= 'read more &gt;&gt;&gt;';
$LNG['Short_Text']	= 'Short Text';
$LNG['Full_Text']	= 'Full Text';


$LNG['all_news']		= 'all news &gt;&gt;&gt;';
$LNG['all_announces']	= 'all announces &gt;&gt;&gt;';


# Block names
$LNG['Pages']	= 'Pages';
$LNG['Block']	= 'Block';
$LNG['News']	= 'News';
$LNG['Announces']='Announces';


# Login
$LNG['Username']	= 'Username';
$LNG['Password']	= 'Password';
$LNG['Login']		= 'Login';
$LNG['Logout']		= 'Logout';
# Login errors
$LNG['Missing_username_or_password']	= 'Please enter username and password.';
$LNG['Username_or_password_not_valid']	= 'Username or password wrong,<br>try again.';
$LNG['You_are_logged_in_as_admin']		= 'You are logged in as admin';


# Pages
$LNG['Filter']		= 'Filter';
$LNG['New_page']	= 'New page';
$LNG['Pages_List']	= 'Pages List';
$LNG['Edit_Page']	= 'Edit Page';
$LNG['Type']		= 'Type';
$LNG['Image']		= 'Image';
$LNG['Image_align']	= 'Image align';
$LNG['Preview_only_warning'] = 'Perview only, changes not saved!';


# Announces
$LNG['Edit_announce']	= 'Edit announce';
$LNG['Add_announce']	= 'Add announce';


# Search
$LNG['Search']			= 'Search';
$LNG['Search_on_site']	= 'Search on site';
$LNG['Found_in_pages']	= 'Found in pages';
$LNG['Found_in_news']	= 'Found in News';
$LNG['Nothing_found']	= 'Nothing found.';

# Blocks
$LNG['Select_block_note']	= '(Block type.<br>other fields will be ignored)';

# Backup
$LNG['Backup_Database']	= 'Backup Database';


$LNG['']	= '';
$LNG['']	= '';
$LNG['']	= '';

?>