IndexBarGrid = Ext.extend(Ext.grid.GridPanel, {

	init_toolbar: function() {

		this.store = new Ext.data.JsonStore({
			url:'/?m=config/cards_class&class='+this.php_class+'&method=show_list',
			totalProperty:'total',	
			root:'data',
			fields: this.fields,
			remoteSort: true
		});
	
	//-------delete function--------------------
		this.delItem = function(){
		
			if (this.sRow!==null) this.selModel.selectRow(this.sRow); // content menu
			if (!this.getSelectionModel().getCount()) return;
			var records = this.getSelectionModel().getSelections();
			

			Ext.MessageBox.show({
				width:250, 
				title:'מחיקת תוכן', 
				msg:'האם ברצונך למחוק את השור'+(records.length===1?'ה':'ות המסומנות')+'?', 
				buttons:Ext.MessageBox.YESNO, scope:this,
				fn: function(btn) { 
					if(btn==='yes') {

						var ids=[];
						Ext.each(records, function(r){ids.push(r.data.id);});
						this.getEl().mask('...טוען');
						
						Ext.Ajax.request({
							url: '/?m=config/cards_class&class='+this.php_class+'&method=delete&ids='+ids.join(','),
							success: function(r,o){
								d=Ext.decode(r.responseText);
								if (!ajRqErrors(d)) {
									Ext.each(records, function(d){
										row=this.store.indexOf(d);
										this.selModel.selectRow(row);
										this.view.getRow(row).style.border='1px solid red';
										Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
									},this);

								}
								
								this.getEl().unmask();
								
								
							},
							failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed'); },
							scope: this
						});
						if (this.php_class=='ACC_class') {
										this.cardsGrid.store.reload()
										this.cardsGrid.class_id_store.reload()
									}
					}
				}
			});
		};
		
	//-------end delete function--------------------

	//-------print function--------------------
		this.printList = function() {
			this.printExportList('print_all')
		};
		
		this.exportExcel = function() {
			this.printExportList('export_excel')
		};	
		
		this.printExportList = function(method){
		url='/?m=config/cards_class&class='+this.php_class+'&method='+method;
		window.open(url, '', 'toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
		};	
	//-------end print function--------------------
	
	//-------add new button function--------------------

	this.onAdd = function (btn, ev) {

        var u = new this.store.recordType({
		
        });
        this.plugins.stopEditing();
        this.store.insert(0, u);
        this.plugins.startEditing(0);
    };
	//------- end add new button function--------------------
	
	this.plugins = new Ext.ux.grid.RowEditor({
		errorSummary:false,
		parent:this,
		listeners:{
					canceledit:function(o, a){
						try{
							var store = this.parent.getStore();
							var rec = store.getAt(0);
							if(!rec.data.id){
								store.remove(rec);
								this.parent.getView().refresh();
								} 
							}
						catch(e){}
					},
				 	beforeedit:function(o, rowIndex){
						var g = this.parent, view = g.getView(),
							row = view.getRow(rowIndex),
							record = g.store.getAt(rowIndex);
						
						if (record.data.group_id==0){
							g.getColumnModel().getCellEditor(0,rowIndex ).field.disable();
							if (this.parent.php_class=='ACC_card')
								g.getColumnModel().getCellEditor(2,rowIndex ).field.disable();

						}else {
							g.getColumnModel().getCellEditor(0,rowIndex ).field.enable();
							if (this.parent.php_class=='ACC_card')
								g.getColumnModel().getCellEditor(2,rowIndex ).field.enable();
						}
					}, 
					afteredit:function(o, d, rec, i){
					
						if (rec.data.id) {var method='update';} else {var method='add';}
						
					//	console.log(rec.data);
						Ext.Ajax.request({
							url: '/?m=config/cards_class&class='+this.parent.php_class+'&method='+method,
							params:rec.data,
							success: function(r,o){
								r = Ext.decode(r.responseText);
								if(ajRqErrors(r)){ 
									//Error need alert and rollback changes
									Ext.Msg.alert('קרתה תקלה, אנא נסה שנית');
									if (rec.data.id) {
										this.parent.getStore().reload();
									}
									else {	
										this.parent.getStore().removeAt(i);
										this.parent.getView().refresh();
									}
								} 
								else{
								
									if (rec.data.id) {
									rec.commit();
									//	this.parent.getStore().reload(); 
									//	this.parent.getView().refresh();
									}
									else{ //it is new
										this.parent.getStore().reload(); 
										this.parent.getView().refresh();
										
									}

									if (this.parent.php_class=='ACC_class') {
										this.parent.cardsGrid.store.reload()
										this.parent.cardsGrid.class_id_store.reload()
									}
								}
								},
								failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
								scope: this
						});
					}
		},scope:this
		
	}),
	
	
	this.selModel = new Ext.grid.RowSelectionModel({singleSelect:true,
					listeners:{
						rowselect: function(sm, rowIndex, rec){
							this.getTopToolbar().find('iconCls','delete')[0].setDisabled(rec.data.group_id==0);
						},
						scope:this
					}
				}),
				
	this.tbar = [
				 { text:'הוספת חדש', iconCls:'add', handler:this.onAdd, scope:this }
				,{ text:'מחיקה',  iconCls:'delete', handler:this.delItem, disabled:true, scope:this}
				,{ text:'הדפסת רשימה',  name:'print_list', iconCls:'print',handler:this.printList, scope:this }
				,{ text:'ייצוא לאקסל',  name:'export_list', iconCls:'excel',handler:this.exportExcel, scope:this }
	];
	 
	this.bbar = new Ext.PagingToolbar({store:this.store,pageSize:50, displayInfo:true});
	}
});
