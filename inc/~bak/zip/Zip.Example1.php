<?php
// Example. Zip all .html files in the current directory and send the file for Download.
$fileDir = './';

include_once("Zip.php");
$fileTime = date("D, d M Y H:i:s T");

$zip = new Zip();
$zip->setComment("Example Zip file.\nCreated on " . date('l jS \of F Y h:i:s A'));
$zip->addFile("Hello World!", "hello.txt");

if ($handle = opendir($fileDir)) {
	/* This is the correct way to loop over the directory. */
	while (false !== ($file = readdir($handle))) {
		if (strpos($file, ".html") !== false) {
			$pathData = pathinfo($fileDir . $file);
			$fileName = $pathData['filename'];

			$zip->addFile(file_get_contents($fileDir . $file), $file, filectime($fileDir . $file));
		}
	}
}

if (ini_get('zlib.output_compression')) {
	ini_set('zlib.output_compression', 'Off');
}

$zip->finalize(); // Not scrictly necessary, the next line will call it anyway.
$zipData = $zip->getZipData();
$length = strlen($zipData);

header('Pragma: public');
header("Last-Modified: " . $fileTime);
header("Expires: 0");
header("Accept-Ranges: bytes");
header("Connection: close");
header("Content-Type: application/zip");
header('Content-Disposition: attachment; filename="ZipExample.zip";' );
header("Content-Transfer-Encoding: binary");
header("Content-Length: ". $length);
echo $zipData;
?>