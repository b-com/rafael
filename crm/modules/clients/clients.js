ClientWindow = function(grid, d, cat) {
	if (!cat) cat='clients';
	if (d) this.client_id=(d.client_id || d.id);

	var phone_cnt=1, email_cnt=1, adr_cnt=1;
	var clientPhoto="skin/pict/man_icon.gif";
	
	var famStore = [[0,'-בחר-'],[1,'אבא'],[2,'אמא'],[3,'בעל'],[4,'אשה'],[5,'בן'],[6,'בת'],[7,'אחר']];
	var telStore = [[1,'בית'],[2,'עבודה'],[3,'נייד'],[4,'פקס בבית'],[5,'פקס בעבודה'],[0,'אחר']];
	var adrStore = [[1,'בית'],[2,'חברה/עסק'],[3,'עבודה'],[4,'למשלוח דואר'],[0,'אחר']];
	var emailStore=[[1,'בית'],[2,'עבודה'],[0,'אחר']];
	this.grid=grid;
	
	this.addPhone = function() {if (phone_cnt>7) return; Ext.getCmp('phoneSet'+phone_cnt).show(); this.syncShadow(); phone_cnt++;}
	this.addEmail = function() {if (email_cnt>2) return; Ext.getCmp('emailSet'+email_cnt).show(); this.syncShadow(); email_cnt++;}
	this.addAddress = function() {if (adr_cnt>2) return; Ext.getCmp('adrSet'+adr_cnt).show(); this.syncShadow(); adr_cnt++;}

	this.tabs = new Ext.TabPanel({
		region: 'center', activeTab:0,
		baseCls: 'x-plain', style:'margin-top:5px',
		plain: true,
		defaults: {xtype:'panel', layout:'form', baseCls: 'x-plain', bodyStyle:'border: 1px solid #8DB2E3; padding: 5px', autoHeight:true},
		items: [{
			title:'כללי', autoHeight:true,
            items: [
				{xtype:'fieldset', id:'private', title:'פרטי לקוח', cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:[
					{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
						{html:'שם מלא:'},{html:'מספר ת.ז:'},{html:'תאריך לידה:'},{html:'מין:'}
						,{name:'name', xtype:'textfield', tabIndex:5, width:335, allowBlank:false}
						,{name:'passport', xtype:'textfield', tabIndex:7, width:130, allowBlank:true}
						,{name:'birthday', xtype:'datefield', tabIndex:8, width:130, format:'d/m/Y', cls:'rtl', vtype:'daterange', value:(d ? d.birthday : '')}
						,{hiddenName:'sex', xtype:'combo', tabIndex:9, width:117, value:0, store:[[0,'-בחר-'],[1,'זכר'],[2,'נקבה']], mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
					]}

				]}
	
				,{xtype:'fieldset', id:'business', title:'פרטי עסק', cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:[
					{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
						{html:'שם עסק:'},{html:'תחום העסק:'},{html:'ח.פ./ע.מ.:'},{html:"מס' עובדים:"}
						,{name:'bname', id:'bname', xtype:'textfield', tabIndex:10, width:335, allowBlank:false}
						,{name:'bdefine', xtype:'textfield', tabIndex:11, width:140}
						,{name:'hp', xtype:'textfield', tabIndex:12, width:130, allowBlank:true}
						,{name:'emp_num', xtype:'textfield', tabIndex:13, width:107, cls:'rtl'}
					]}
					,{layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
						{html:'איש קשר:'},{html:'מנכ"ל:'},{html:'בעלים:'}
						,{name:'cperson', xtype:'textfield', tabIndex:14, width:237}
						,{name:'director', xtype:'textfield', tabIndex:15, width:238}
						,{name:'owner', xtype:'textfield', tabIndex:16, width:237}
					]}
				]}
				
				,{
					layout:'column', baseCls: 'x-plain', defaults: {xtype:'fieldset'},
					items:[{
							title: 'טלפון/פקס', style:'margin-left:5px; padding:2px;', cls: 'rtl', width: 150, autoHeight: true ,columnWidth:0.5,
							items: [
							{
								layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'phoneSet0',
								items:[
									{xtype:'label', text:"סוג:"}
									,{xtype:'label', text:"מספר:"}
									,{xtype:'label', text:"הערות:", colspan:2}
									,{xtype:'combo', hiddenName:'tel_type0', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel0', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes0', xtype:'textfield', cls:'rtl', width:125}
									,{baseCls: 'x-plain', html:'<img src="skin/icons/add_call.png" onClick="Ext.getCmp(\'clientWindow\').addPhone()" ext:qtip="הוסף מספר טלפון">'}

								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet1',
								items:[
									{xtype:'combo', hiddenName:'tel_type1', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel1', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes1', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet2',
								items:[
									{xtype:'combo', hiddenName:'tel_type2', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield' ,name:'tel2',  cls:'ltr', width:100}
									,{name:'tel_notes2', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet3',
								items:[
									{xtype:'combo', hiddenName:'tel_type3', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel3', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes3', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet4',
								items:[
									{xtype:'combo', hiddenName:'tel_type4', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel4', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes4', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet5',
								items:[
									{xtype:'combo', hiddenName:'tel_type5', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield', name:'tel5', cls:'ltr', width:100}
									,{name:'tel_notes5', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet6',
								items:[
									{xtype:'combo', hiddenName:'tel_type6', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield', name:'tel6', cls:'ltr', width:100}
									,{name:'tel_notes6', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet7',
								items:[
									{xtype:'combo', hiddenName:'tel_type7', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield', name:'tel7', cls:'ltr', width:100}
									,{name:'tel_notes7', xtype:'textfield', cls:'rtl', width:125}
								]
							}]
						},{
						title:'דוא"ל', style:'margin-right:5px; padding:2px', cls: 'rtl', autoHeight:true, columnWidth:0.5,
						items: [{
							layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'emailSet0',
							items:[
								{xtype:'label', text:"סוג:"}
								,{xtype:'label', text:"כתובת:"}
								,{xtype:'label', text:"הערות:", colspan:2}
								,{xtype:'combo', width:65, listClass:'rtl', value:1, store:[[1,'עיקרי']], readOnly:true}
								,{xtype:'textfield', name:'email', cls:'ltr', width: 140, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
								,{xtype:'textfield', name:'email_notes', cls:'rtl', width:130}
								,{baseCls: 'x-plain', html:'<img src="skin/icons/add_email.png" onClick="Ext.getCmp(\'clientWindow\').addEmail()" ext:qtip=\'הוסף כתובת דוא"ל\'>'}						
							]
						},{
							layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'emailSet1',
							items:[
								{xtype:'combo', hiddenName:'email_type1', width:65, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore}
								,{xtype:'textfield', name:'email1', cls:'ltr', width:140, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
								,{name:'email_notes1', xtype:'textfield', cls:'rtl', width:130}
							]
						},{
							layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'emailSet2',
							items:[
								{xtype:'combo', hiddenName:'email_type2', width:65, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore}
								,{xtype:'textfield', name:'email2', cls:'ltr', width:140, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
								,{name:'email_notes2', xtype:'textfield', cls:'rtl', width:130}
							]
						}]
						}
					]
				}
				,{
					xtype:'fieldset', title:'כתובת', cls:'rtl', style: 'margin-top:3px; padding:2px', autoHeight: true,
					items: [
						{
							layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain',id:'adrSet0', defaults: {baseCls: 'x-plain', cls: 'rtl'},
							items:[
								{html:'סוג:'},{html:'עיר:'},{html:'מיקוד:'},{html:'כתובת/ת.ד.:', colspan: 2},
								{xtype:'combo', hiddenName:'adr_type0', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
								{name:'city0', xtype:'textfield', width: 100},
								{name:'zip0', xtype:'textfield', width: 125},
								{name:'adr0', xtype:'textfield', width:372},
								{baseCls: 'x-plain', html:'&nbsp;<img src="skin/icons/add_addr.png" onClick="Ext.getCmp(\'clientWindow\').addAddress()" ext:qtip=\'הוסף כתובת\'>'}
							]
						},{
							layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet1', defaults: {baseCls: 'x-plain', cls: 'rtl'},
							items:[
								{xtype:'combo', hiddenName:'adr_type1', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
								{name:'city1', xtype:'textfield', width: 100},
								{name:'zip1', xtype:'textfield', width: 125},
								{name:'adr1', xtype:'textfield', width:372}
							]
						},{
							layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet2', defaults: {baseCls: 'x-plain', cls: 'rtl'},
							items:[
								{xtype:'combo', hiddenName:'adr_type2', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
								{name:'city2', xtype:'textfield', width: 100},
								{name:'zip2', xtype:'textfield', width: 125},
								{name:'adr2', xtype:'textfield', width:372}
							]
						}
					]
				}
			]
		},{
            title: 'הערות', layout: 'anchor', items: {fieldLabel:'הערות', height:220, name:'notes', xtype:'textarea', cls:'rtl',  width: '100%'}
		}
	]});
	
	if (user.tax_module && cat=='clients') this.tabs.insert(3, new ClientTaxTab(d)); 
	
	this.tabs.on('tabchange', function(tabPanel, tab) {
		// if (Ext.getCmp('clType').getValue()==1) {Ext.getCmp('job').show();}
		// else {Ext.getCmp('job').hide();}
		this.tabs.doLayout(); this.syncShadow();
	},this);
	
	this.form = new Ext.form.FormPanel({
		id:'clientForm', fileUpload:(cat=='clients'), labelWidth:75, autoHeight:true, baseCls: 'x-plain',
		items:[{
			layout:'table', layoutConfig:{columns:8}, baseCls:'x-plain', cls:'rtl', defaults: {layout:'form', baseCls: 'x-plain'},
			items:[
				{xtype:'label', text:'סוג:'},
				{xtype:'combo', tabIndex:10, id:'clType', name:'c_type', hiddenName:'c_type', valueField:'id', displayField:'val', width:90, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:false, store:[[1,'פרטי'],[2,'עסקי'],[3,'ספק']],
					listeners:{
						select:function(f){ 
							if (f.getValue()==1){//פרטי
								this.tabs.unhideTabStripItem(1);
								this.tabs.hideTabStripItem(2);
								// Ext.getCmp('job').show();
								Ext.getCmp('private').show();
								Ext.getCmp('business').hide();
							}else {//חברה
							 	this.tabs.hideTabStripItem(1);
							 	this.tabs.unhideTabStripItem(2);
								// Ext.getCmp('job').hide();
								Ext.getCmp('private').hide();
								Ext.getCmp('business').show();
							}
							this.syncShadow();
						},scope:this}
				}
				// ,
				// {xtype:'label', text:'תקייה:', style:'padding-right:15px', hidden:(cat=='clients'), hideParent:true },
				// {xtype:'combo', tabIndex:11, store:((typeof addressBookCats!='undefined')?addressBookCats:[]), hiddenName:'parent_id', hidden:(cat=='clients'), hideParent:true, value:cat, width:150, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true},
				// {xtype:'label', text:'איש מכירות:', style:'padding-right:15px', hidden:(cat!='clients'), hideParent:true },
				// {xtype:'combo', tabIndex:11, store:crm_agents, hiddenName:'agent_id', hidden:(cat!='clients'), disabled:(this.grid.caller=='claim_info'), hideParent:true, value:0, width:100, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true},
				,{xtype:'label', text:"מס' מפתח:", style:'padding-right:15px', hidden:(cat!='clients'), hideParent:true }
				,{name:'acc_card', tabIndex:12, xtype:'textfield', width:90, hidden:(cat!='clients'), hideParent:true }
				//,{name:'cat', xtype:'hidden', value:cat}
			]
		}, this.tabs]
	});
	
	ClientWindow.superclass.constructor.call(this, {
		title:( cat=='clients' ? (this.client_id?'עידכון פרטי לקוח':'הוספת לקוח חדש') : (this.client_id?'עידכון פרטי איש קשר':'הוספת איש קשר חדש') ),
		layout:'fit', id:'clientWindow', modal:true,
		width:780, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons:[{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('show', function(){
		Ext.getCmp('phoneSet1').hide(); Ext.getCmp('phoneSet2').hide(); Ext.getCmp('phoneSet3').hide(); Ext.getCmp('phoneSet4').hide(); Ext.getCmp('phoneSet5').hide(); Ext.getCmp('phoneSet6').hide(); Ext.getCmp('phoneSet7').hide();
		Ext.getCmp('emailSet1').hide(); Ext.getCmp('emailSet2').hide();
		Ext.getCmp('adrSet1').hide(); Ext.getCmp('adrSet2').hide();
		Ext.getCmp('business').hide();

		if (cat!='clients') this.tabs.hideTabStripItem(1);
		this.tabs.hideTabStripItem(2);
		
		if (this.client_id) this.formLoad();
		else if (d) {//Load Client data from d
			this.form.form.setValues(d);
			if (d.c_type!=1) {//Change Client window tabs
				// this.tabs.hideTabStripItem(2);
				// this.tabs.unhideTabStripItem(1);
				// Ext.getCmp('job').hide();
				Ext.getCmp('private').hide();
				Ext.getCmp('business').show();
			}
		}
		
		this.tabs.setActiveTab( d&&d.activeTab ? d.activeTab:0 );
		this.syncShadow();
	});
	
	this.formLoad=function() {
		// this.getEl().mask('...טוען');
		this.form.load({
			url: (cat=='clients' ? "?m=clients/clients&f=load_client_data&client_id="+this.client_id : "/?m=organizer/contacts_f&f=load_data&id="+this.client_id),
			params:{load_tax_folder:(user.tax_module && cat=='clients')},
			reader: new Ext.data.JsonReader({
				root: 'data',
				successProperty: 'success',
				totalProperty: 'total'
			}),
			success: function(f,a){
				var d=a.result.data;
				for (var n=1; n<8; n++) if (d['tel'+n]) {Ext.getCmp('phoneSet'+n).show();phone_cnt=n+1;}
				for (var n=1; n<3; n++) if (d['email'+n]) {Ext.getCmp('emailSet'+n).show();email_cnt=n+1;}
				for (var n=1; n<3; n++) if (d['city'+n] || d['zip'+n]!=0 || d['adr'+n]) {Ext.getCmp('adrSet'+n).show();adr_cnt=n+1};
				if (d['client_photo']) clientPhoto=d['client_photo'];
				if (d['c_type']==2 || d['c_type']==3) {
					// this.tabs.hideTabStripItem(2);
					// this.tabs.unhideTabStripItem(1);
					// Ext.getCmp('job').hide();
					Ext.getCmp('private').hide();
					Ext.getCmp('business').show();
				}
				else{
					// this.tabs.hideTabStripItem(1);
					// this.tabs.unhideTabStripItem(2);
				}
				this.syncShadow();
				this.getEl().unmask();
			},
			failure: function(){ this.getEl().unmask(); },
			scope:this
	   	});
	}

	this.submitForm = function(overwrite) {
		clType = Ext.getCmp('clType').getValue();
		// if (clType==1) {//פרטי
			// first_name = Ext.getCmp('first_name'); 
			// last_name = Ext.getCmp('last_name'); 
			// if (!first_name.getValue() && !last_name.getValue()) {last_name.markInvalid('חובה למלא שדה זה'); return;}
			// passport = Ext.getCmp('passport'); if (!passport.getValue() && cat=='clients') {passport.markInvalid('חובה למלא שדה זה'); return;}
		// }else {//חברה
			// bname = Ext.getCmp('bname'); if (!bname.getValue()) {bname.markInvalid('חובה למלא שדה זה'); return;}
			// hp = Ext.getCmp('hp'); if (!hp.getValue() && cat=='clients') {hp.markInvalid('חובה למלא שדה זה'); return;}
		// }
		// if(!this.form.form.isValid()) return;
		
		this.form.find('name','name')[0].allowBlank = (clType!=1);
		// this.form.find('name','passport')[0].allowBlank = (clType!=1);
		
		this.form.find('name','bname')[0].allowBlank = (clType==1);
		// this.form.find('name','hp')[0].allowBlank = (clType==1);
		if(!this.form.form.isValid()) return;
		this.form.form.submit({
			url:"?m=clients/clients&f=add_client&id="+this.client_id,
			// params:{client_group:this.form.find('name','client_group')[0].getValue()},
			waitTitle:'אנא המתן...', waitMsg:'טוען...',	scope:this,
			success: function(f,a){
				this.close();
				var data = Ext.decode(a.response.responseText);
				// Dump(data);
				if (this.grid.callBack) this.grid.callBack(data);
			},
			failure: function(f,a){
			   this.close(); ajRqErrors(a.result);}
		});
	}
};
Ext.extend(ClientWindow, Ext.Window, {});
