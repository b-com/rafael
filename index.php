<?
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", "On");

include_once "inc/common.php";
include_once "inc/cfg.php";

$a = $_REQUEST['a'];

send_headers();
if ($a=='new_user') add_user_form();
elseif ($a=='terms') user_terms();
elseif ($a=='capcha_gen') capcha_gen();
else user_login_form();

//————————————————————————————————————————————————————————————————————————————————————
function capcha_gen() {
	include_once "inc/capcha_gen.php";

	$width = isset($_GET['width']) ? $_GET['width'] : '250';
	$height = isset($_GET['height']) ? $_GET['height'] : '90';
	$characters = isset($_GET['characters']) && $_GET['characters'] > 1 ? $_GET['characters'] : '6';
	$captcha = new CaptchaSecurityImages($width,$height,$characters);
}
//————————————————————————————————————————————————————————————————————————————————————
function user_login_form() {
	// echo $_SERVER['REMOTE_ADDR'];
	?>
	<html>
	<head>
	<title>Rafael - חשבונאות חדשנית</title>
	<link rel="stylesheet" type="text/css" href="crm/skin/style_ext.css?<?=filemtime('crm/skin/style_ext.css');?>" />
	<script type="text/javascript" src="crm/skin/extjs/adapter/ext/ext-base.js"></script>
	<script type="text/javascript" src="crm/skin/extjs/ext-all.js?<?=filemtime('crm/skin/extjs/ext-all.js')?>"></script>
	</head>
	<body dir=rtl><br><br><br>
	<table border="0" cellspacing="0" align=center width='300' class=tf cellpadding="0" style='border:1px solid #8c8c8c;' bgcolor="#FFFFFF"><tr><td align=center style="padding:7px">
	<img border=0 src='crm/skin/rafael_logo.PNG'><br>
	<?	if ($login_message) echo "<font class=sm14 color=#CC3300>$login_message</font><br><br>"; ?>
	<div style='background-color: #f8f8f8; padding:6px; text-align:right; padding-right:32px;'>
	<font style='font-size:14px; font-weight:bold; color: #696969'>כניסת משתמש לתוכנה </font><br><br>
	<form name=login_form onSubmit='return openCRM(this)' method=post>
	<div id=loginMsg style='color:red'></div>
	<table border="0" cellpadding="0" cellspacing="0" style='font-size:11px; color:'>
	<tr><td width=70>חברה</td><td><input type=text name=company class=text style='border:1px solid #c9c9c9; width:130px; font-size:12px; color:#646464; background: white'></td></tr>
	<tr><td width=70>שם משתמש</td><td><input type=text name=username class=text style='border:1px solid #c9c9c9; width:130px; font-size:12px; color:#646464; background: white'></td></tr>
	<tr><td>סיסמא</td><td><input type=password name=password id=password class=text style='border:1px solid #c9c9c9; width:130px; font-size:12px; background: white'></td></tr>
	<tr><td align=right><a href='http://rafael-ac.com/?m=contact' style='font-size:10px !important; color:gray !important; font-weight:normal !important; padding:0 !important;'>שכחתי סיסמא</a></td>
	<td align=left><input type=image tabindex=3 src="crm/skin/b_login.png" border="0"></td></tr>
	</table>
	</form></div>
	</td></tr>
	</table>
	
	
	<SCRIPT>
	document.login_form.company.focus();

	openCRM = function(f) {
		Ext.Ajax.request({
			url: 'crm/?m=users/users&f=login',
			params:{
				company:f.company.value,
				// company:encodeURIComponent(f.company.value),
				username:encodeURIComponent(f.username.value),
				password:encodeURIComponent(f.password.value)
			},
			success: function(r,o){
				var d=Ext.decode(r.responseText);
				if (d.success==true) window.location='/crm/'; 
				else if (d.msg) document.getElementById('loginMsg').innerHTML=d.msg;
			},
			failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
		return false;
	}
	</SCRIPT>	
	</body>
	</html>
	<?
}
//————————————————————————————————————————————————————————————————————————————————————
function add_user_form() {
 global $CFG, $LNG, $a, $m, $f, $languages, $cats_ar;

	include_once ("inc/forms2.php");
	include_once ("inc/mail/sendmail.php");
?>

<style>
body {margin: 0; background: none repeat scroll 0 0 #EAEAEA; font-family: Arial}
a, a.on, a.visited {text-decoration:none; border:0}
.tf    { border-collapse:collapse; border:0; }
.tf td { padding:0; margin:0 }
.tf fieldset  { font-size:12px; border:1px solid #dfe1ec; padding:0 5px 8px 5px; margin-bottom:10px;}
.tf legend { color:#325BAF; font-weight:bold; }
.tf .text, .tf textarea {border:1px solid #c9c9c9; font-size:12px; color:#646464;}
.tf .text, .tf select {border:1px solid #c9c9c9; font-size:12px; color:#646464; height:18px; font-weight:bold; line-height:14px;}
.tf label { color:#466CB7; font-size:12px; }
.tf label.required { background:url(../inc/forms/required.gif) no-repeat -5px top; padding-left:7px; zoom:1; display: -moz-inline-box }
.tf input.error, .tf textarea.error { border:1px solid red; background:#FFFCE2 url(../inc/forms/warning.gif) no-repeat left 2px !important; padding-left:16px; }
.tf textarea.error, .tf select.error option  { border:1px solid red; background:#FFFCE2 url(../inc/forms/warning.gif) no-repeat left top !important; padding-left:16px; }
.tf label.error { color:red; }
.tf .radios { vertical-align:middle; padding:1px; border:1px solid #B7CFE3; zoom:1 }
.tf .radios label { margin: 1 0 1 3; zoom:1 }
.tf input.readonly, .tf textarea.readonly, .tf td.readonly { background:#EEEEEE; border:1px solid #B5B8C8; color:#404040; }
.tf .btn {width: 110px; height:36px; color:#f70105; border:1px solid #c7c7c7; font-size:14pt; background:#fff url(../skin/portal/icons/hand.png) no-repeat 120px 1px; background-position:right top; cursor:hand; cursor:pointer}
</style>

<?
	$FORM=array(
	 'table'	=> "$CFG[prefix]users",
	 'id'		=> 'new_user_form',
	 'primary'	=> 'user_id',
	 'title'	=> 'פרטי בעל העסק',
	 'action'	=> "/?m=$m&a=$a",
	 'js_check'	=> 2,
	 'fields'	=> array(
		'date_added'=>array('db'=>1, 'value'=>date('Y-m-d H:i:s') ),
		'type'		=>array('db'=>1, 'value'=>3 ),
		'active'	=>array('db'=>1, 'value'=>-1 ),
		array('type'=>'header', 'html'=>'פרטי העסק' ),
		'login' 	=>array('text', 'שם משתמש לחברה','width'=>130, 'db'=>1, 'required'=>1 ),
		'name' 	=>array('text', 'שם העסק','width'=>320, 'db'=>1, 'required'=>1 ),
		'cnumber' 	=>array('text', 'ח.פ./ע.מ.','width'=>130, 'db'=>1, 'required'=>1 ),
		'city0' =>array('text', 'עיר', 'width'=>130, 'db'=>1, 'required'=>0),
		'adr0' 	=>array('text', 'כתובת', 'width'=>318, 'db'=>1, 'required'=>0 ),
		'zip0'	=>array('text', 'מיקוד', 'width'=>130, 'db'=>1, 'add'=>'dir=ltr', 'required'=>0, 'regexp'=>'/^[0-9]{5}$/' ),
		'tel0' 	=>array('text', 'טלפון 1', 'width'=>130, 'db'=>1, 'add'=>'dir=ltr', 'required'=>0 ),
		'tel1' 	=>array('text', 'טלפון 2', 'width'=>130, 'db'=>1, 'add'=>'dir=ltr' ),
		'fax'	 	=>array('text', 'פקס', 'width'=>130, 'db'=>1, 'add'=>'dir=ltr', 'required'=>0 ),
		'email'	=>array('text', 'דוא"ל', 'width'=>130, 'db'=>1, 'add'=>'dir=ltr', 'ercheck'=>'email', 'required'=>1 ),
		'username'	=>array('text', 'שם משתמש', 'width'=>130, 'db'=>1,'regexp'=>'/^[a-zA-Z0-9!@#$%^&*()_-]{3,30}$/', 'required'=>1, 'add'=>'dir=ltr' ),
		'password'	=>array('password', 'סיסמא', 'width'=>130, 'db'=>1, 'notes'=>'<span class=sm10>(שלפחות 6 אותיות)</span>', 'regexp'=>'/^[a-zA-Z0-9!@#$%^&*()_-]{6,30}$/', 'required'=>1, 'add'=>'dir=ltr' ),
		'password2'	=>array('password', 'סיסמא שנית', 'width'=>130, 'required'=>1, 'add'=>'equalto=password dir=ltr' ),
		'fname'		=>array('text', 'שם פרטי', 'width'=>130, 'db'=>1, 'required'=>1 ),
		'lname'		=>array('text', 'שם משפחה', 'width'=>130, 'db'=>1, 'required'=>1 ),
		'u_tel'		=>array('text', 'טלפון', 'width'=>130, 'db'=>1, 'add'=>'dir=ltr', 'required'=>1 ),
		'u_email'	=>array('text', 'דוא"ל','width'=>130, 'db'=>1, 'add'=>'dir=ltr', 'ercheck'=>'email', 'required'=>0 ),
		'agreement'	=>array('checkbox', 'תנאי שימוש','required'=>1),
		'capcha'	=>array('text', 'capcha','required'=>1),
		'sbmt'	=>array('hidden', 'value'=>1)
		)
	);
	
	if ( $_POST['sbmt'] AND $a=='new_user'){
		

		if ($_POST['password']!=$_POST['password2']) {
			$FORM['error_text'].="Passwords not equal<br>";
			$FORM['fields']['password']['error'] = $FORM['fields']['password2']['error'] = 1;
		}

		if ($_POST['password']) $FORM['fields']['password']['value']=md5($_POST['password']);

		if (!isset($FORM['error_text'])) { # If no errors add/update to Database
			$company_arr = sql2array("SELECT id,name FROM companies",'id','name','','Rafael');
			
			if ($company_arr AND in_array($_POST['name'], $company_arr)){
				echo '{success:false, msg:company name already exsits!}';
				return;
			}
			
			// Add a new company to companies table
			$sql="companies SET name=".quote($_POST['name'])
				.", tel=".quote($_POST['tel0'])
				.", current_vat_period=".quote(Date("Y-m"))."-1"
				.", vat_period='1'"
				.", tel2=".quote($_POST['tel1'])
				.", fax=".quote($_POST['fax'])
				.", email=".quote($_POST['email'])
				.", url=".quote($_POST['url'])
				.", city=".quote($_POST['city0'])
				.", street=".quote($_POST['adr0'])
				.", zip=".quote($_POST['zip0'])
				.", date_added=".quote(Date("Y-m-d"))
				.", active='1'"
			;
			print_ar($_POST['mod']);
			if ($_POST['mod']) $sql.=", modules=".quote(implode(',',array_keys($_POST['mod']))); elseif (!$id) $sql.=", modules='10,49'";
			
			$sql = "INSERT INTO $sql";
			
			if (runsql($sql,'Rafael')) {
				if (!$id) {#Create new user
					$id = mysql_insert_id();

					$sql = "INSERT INTO users SET company_id = ".quote($id)
					.", username = ".quote($_POST['username'])
					.", password = ".quote($FORM['fields']['password']['value'])
					.", fname = ".quote($_POST['fname'])
					.", lname = ".quote($_POST['lname'])
					.", date_added = ".quote(Date("Y-m-d"))
					.", active = '1'";
					
					runsql($sql,'Rafael');
					
				}
				
				?>
				<table border="0" cellspacing="0" align=center width='300' class=tf cellpadding="0" style='border:1px solid #8c8c8c;' bgcolor="#FFFFFF"><tr><td align=center style="padding:7px">
				<img border=0 src='http://cnf.co.il/skin/01crm.jpg'><br>
				<?	if ($login_message) echo "<font class=sm14 color=#CC3300>$login_message</font><br><br>"; ?>
				<div style='background-color: #f8f8f8; height:105px; padding:6px; text-align:right'>
				<font style='direction:rtl; font-size:14px; font-weight:bold; color: #696969'>הרשמה בוצעה בהצלחה.<br>שם המשתמש והסיסמה נשלחו<br>לכתובת הדוא"ל שהוזנה בעת ההרשמה.<br>תודה ויום טוב!</font><br><br>
				</div>
				</td></tr>
				</table>
				<script>setTimeout("document.location='/'",3000);</script>
				<?
				$user_details = "<div style='text-align:right; direction:rtl;'>ברוכים הבאים למערכת הנהלת החשבונות המתקדמת 'רפאל'"
								."<br>"
								."פרטי כניסה למערכת:<br>"
								."שם חברה: ".$_POST['name']."<br>"
								."שם משתמש: ".$_POST['username']."<br>"
								."סיסמה: ".$_POST['password']
								."</div>";
				if(send_mail($_POST['email'],"פרטי התחברות למערכת הנהלת החשבונות 'רפאל'",$user_details))
					return "OK!";

			}else echo '{success:false}';
		}
	}else {	# set default values
		$FORM['fields']['type']['value']=1;
	}
	
	list($f, $l)=frm_parse($FORM);
?>
<?='<font color=red>'.$FORM['error_text'].'</font>'?>
<?=$FORM['form_tag']?>
<?=$f['id'];?>

<div dir=rtl style="width:100%; height:100%; overflow:auto; zoom:1; background-image: url('crm/skin/bg.gif'); background-repeat: repeat-x; background-position-y: top;">
<div style='width:970px;' align=right><a href='http://rafael-ac.com' border=0><img src ='crm/skin/rafael_logo_small.png' border=0></a></div>
<table border="0" cellspacing="0" align=center width=80% class=tf cellpadding="0" style='border:1px solid #8c8c8c;' bgcolor="#FFFFFF"><tr><td align=center style="padding:7px">
	<fieldset><legend>פרטי העסק</legend>
		<table width='100%'><tr>
		<td style='width:190px;'><?=$l['login'].'<br>'.$f['login']?></td>
		<td style='width:380px;'><?=$l['name'].'<br>'.$f['name']?></td>
		<td><?=$l['cnumber'].'<br>'.$f['cnumber']?></td>
		</tr></table>
	</fieldset>
	
	<fieldset><legend>כתובת</legend>
		<table align=right><tr>
		<td style='width:190px;'><?=$l['city0'].'<br>'.$f['city0']?></td>
		<td style='width:375px;'><?=$l['adr0'].'<br>'.$f['adr0']?></td>
		<td style='width:150px;'><?=$l['zip0'].'<br>'.$f['zip0']?></td>
		</tr></table>
	</fieldset>
	
	<fieldset><legend>פרטי יצירת קשר</legend>
		<table width='100%'><tr>
		<td><?=$l['tel0'].'<br>'.$f['tel0']?></td>
		<td><?=$l['tel1'].'<br>'.$f['tel1']?></td>
		<td><?=$l['fax'].'<br>'.$f['fax']?></td>
		<td><?=$l['email'].'<br>'.$f['email']?></td>
		</tr></table>
	</fieldset>
		
	<fieldset><legend>פרטי משתמש</legend>
		<table width='100%'><tr>
		<td><?=$l['username'].'<br>'.$f['username']?></td>
		<td><?=$l['password'].'<br>'.$f['password']?></td>
		<td><?=$l['password2'].'<br>'.$f['password2']?></td>
		<td> &nbsp; </td>
		</tr><tr>
		<td><?=$l['fname'].'<br>'.$f['fname']?></td>
		<td><?=$l['lname'].'<br>'.$f['lname']?></td>
		<td><?=$l['u_tel'].'<br>'.$f['u_tel']?></td>
		<td><?=$l['u_email'].'<br>'.$f['u_email'].$f['sbmt']?></td>
		</tr>
		<tr><td colspan=4 align=center><br><img src="?a=capcha_gen"></td></tr>
		<tr><td colspan=4 align=center><?=$l['capcha'].'<br>'.$f['capcha']?></td></tr>
		<tr><td colspan=4 align=center><br><?=$f['agreement']?> אני מסכים ל<a href='http://rafael-ac.com/terms.htm' target='_blank'><b>תנאי שימוש</b></a></td></tr>
		</table>
		
	</fieldset>
	<div align=center><a href='javascript:void(0)' onClick="frm=$('new_user_form'); if (validateCompleteForm(frm)) frm.submit();"><img border=0 src='../crm/skin/send.gif'></a></div>
</td></tr></table><br>

	<table width=100%>
	<tr>
		<td align="center" height="31" style="background-image: url('http://rafael-ac.com/skin/menu_bg.gif'); background-repeat: repeat-x; background-position-y: top;">
		&nbsp;
		</td>
	</tr></table></div></div>


<?}
//————————————————————————————————————————————————————————————————————————————————————
function user_terms() {
?>
<div class=tl><div class=tr style='padding:10px; height:35px; width:100%;'></div></div>
<div class=p style='padding:10px; width:100%; border-left: solid 1px #CACDE0; border-right: solid 1px #CACDE0;'>
<font SIZE="3"><b>
<p ALIGN="CENTER">תנאי שימוש בתוכנה</p>
</b></font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
<p ALIGN="RIGHT">&nbsp;</p>
</font><font SIZE="2">
<p ALIGN="RIGHT">השימוש בתוכנת</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">באפי</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">( להלן: <b>&quot;התוכנה&quot;</b></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
) </font><font SIZE="2">לרבות כניסה לתוכנה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">ושימוש בתכניה ובשירותים המוצעים בה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">מהווים הסכמה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">של המשתמש לכל התנאים המפורטים כדלהלן :</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">הגדרות</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">&quot;משתמש&quot;</font></b><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
– </font><font SIZE="2">אדם, לרבות תאגיד משפטי, אשר בוחר להיכנס לתוכנה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">ו/או לעשות שימוש בתכניה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">&quot;תכני התוכנה&quot;</b></font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">– הצעות, נתונים, ניתוחים, הערכות, סקירות, פרסומים, תמונות, 
הודעות (לרבות דואר אלקטרוני והודעות </font><font FACE="Arial" SIZE="2">SMS</font><font SIZE="2"> 
), מחשבונים ( לרבות קישורים למחשבונים של גופים שלישיים</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
) </font><font SIZE="2">, וכן כל מצג, פרסום ו/או מידע העומד ו/או שיועמד בעתיד 
לרשות המשתמש בתוכנה.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">&quot;מידע משתמש&quot;</b> – כל מידע הנמסר על ידי המשתמש ו/או מי מטעמו, 
לרבות פרטיו האישיים.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">&quot;חברת ביטוח&quot;</b> – כהגדרתה בחוק הפיקוח על שירותים פיננסיים 
(ביטוח), תשמ&quot;א-1981</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">כללי</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">בי-קום&nbsp; פתרונות תוכנה בע&quot;ם </font>
<font FACE="Times New Roman (Hebrew)" SIZE="2">( </font><font SIZE="2">להלן: <b>
&quot;החברה&quot;</b></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </font>
<font SIZE="2">) הינה הבעלים והמפעילה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">של התוכנה אשר משמשת כזירת ביטוח אינטרנטית בה נבחנות 
סימולטאנית מספר הצעות ביטוח מחברות ביטוח שונות, ככל האפשר, בזמן אמת.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">אחריות</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font></b><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">השימוש בתוכנה ובתכנים המופיעים בה מוצעים למשתמש כמות שהם ( </font>
<font FACE="Arial" SIZE="2">as is</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font FACE="Arial" SIZE="2">(</font><font SIZE="2"> ולמשתמש לא תהא כל 
תביעה, דרישה או טענה בגין השימוש בתכניה ו/או הסתמכותו על תכניה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">למשתמש ידוע כי עלולים להיגרם בתוכנה תקלות ו/או שיבושים ו/או 
שגיאות ו/או אי דיוקים. החברה לא תישא באחריות בגין כל נזק אשר יגרם כתוצאה מתקלות 
ו/או שיבושים ו/או שגיאות ו/או אי דיוקים כאמור</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">החברה אינה אחראית לאתרים לגביהם קיים קישור ( להלן: </font><b>
<font FACE="Times New Roman (Hebrew)" SIZE="2">&quot;</font><font SIZE="2">לינק</font><font FACE="Times New Roman (Hebrew)" SIZE="2">&quot;</font></b><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
), </font><font SIZE="2">אין לה כל שליטה על תוכנם של אתרים אלה, היא לא תהא 
אחראית בגין כל מידע המקושר לתוכנה באמצעות לינק</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">ו/או בכל דרך אחרת והיא לא תחוב באחריות כלשהי בגין הסתמכות 
המשתמש</font><font FACE="Arial" SIZE="2"> </font><font SIZE="2">על המידע המובא 
באתרים אלה</font><font FACE="Times New Roman (Hebrew)" SIZE="2" COLOR="#2e2e2e"> 
.</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </p>
</font><font SIZE="2">
<p ALIGN="RIGHT">בנוסף, ידוע למשתמש כי אין בהופעת קישור לאתר שלישי משום המלצה 
לאתר או לבעליו.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">התכנים בתוכנה אינם באים להחליף יעוץ ביטוחי ו/או יעוץ פנסיוני ו/או 
יעוץ מקצועי אחר, ואין לראות בתכניה כמתן יעוץ ו/או מצג כלשהו</font><font FACE="Times New Roman (Hebrew)" SIZE="2">,</font><font SIZE="2"> 
ובכל מקרה יש לפנות לייעוץ של איש מקצוע מוסמך.</font><font FACE="Arial" SIZE="2"><br>
</font><font SIZE="2">החברה לא תחוב בכל אחריות בגין החלטות ו/או פעולות של המשתמש 
הנובעות מהסתמכות על תכני התוכנה.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">הרשאות שימוש</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font></b><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">המשתמש מסכים לכך ומאשר כי במידת הצורך ימסור לידי החברה את 
הרשאות השימוש שלו בחברות הביטוח השונות</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
(</font><font FACE="Arial" SIZE="2">username </font>
<font FACE="Times New Roman (Hebrew)" SIZE="2">+</font><font FACE="Arial" SIZE="2"> 
password</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </font>
<font FACE="Arial" SIZE="2">(</font><font SIZE="2"> זאת על מנת שהחברה תוכל לעדכן 
ולהתאים את תכני התוכנה לצרכיו של המשתמש</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">המשתמש מסכים לכך ומאשר כי יהיה האחראי הבלעדי בגין כל נזק ו/או 
הפסד /ואו אבדן רווח שיגרם לחברה כתוצאה מכל טענה ו/או דרישה ו/או תביעה שתהיה 
לחברת ביטוח כנגד החברה ו/או מי מטעמה בגין מתן הרשאות השימוש כאמור לעיל.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">המשתמש מסכים לכך ומאשר כי אם יוחלט על ידי ערכאה שיפוטית או מעין 
שיפוטית כלשהי כי החברה ו/או מי מטעמה אחראים בגין מסירת ההרשאות כאמור לעיל, ישפה 
המשתמש את החברה בגין כל נזק שייגרם לה ו/או חוב פסוק שייפסק נגדה. </p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><u><font SIZE="2">
<p ALIGN="RIGHT">אבטחה ופרטיות</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font></u><font FACE="Times New Roman (Hebrew)" SIZE="2">:</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">החברה נוקטת באמצעי אבטחה סבירים כמקובל בתחום</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">האינטרנט</font><font SIZE="2" COLOR="#323232"> ואף דואגת 
לעדכן את מערכת האבטחה של התוכנה מעת לעת</font><font FACE="Times New Roman (Hebrew)" SIZE="2" COLOR="#323232">.
</font><font SIZE="2" COLOR="#323232">למרות זאת</font><font FACE="Times New Roman (Hebrew)" SIZE="2" COLOR="#323232">,
</font><font SIZE="2">קיים סיכון של חדירה למאגרי הנתונים של החברה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.
</font><font SIZE="2">על כן, החברה לא תהא אחראית לנזק שייגרם בעקבות חדירה למאגר 
נתוניה ו/או בגין שימוש שיעשה במידע הלקוח במקרה של חדירה למאגר הנתונים כאמור.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">המשתמש מסכים לכך ומאשר כי כל מידע שנמסר באמצעות התוכנה נמסר 
מרצונו החופשי.</p>
<p ALIGN="RIGHT">המשתמש מסכים</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">לכך שהחברה תוכל לעשות שימוש במידע אודותיו, לרבות שימוש 
במידע למטרות סטטיסטיות, העברת מידע פרסומי אליו 
, זאת בכפוף להוראות חוק הגנת הפרטיות, התשמ&quot;א - 1981.</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT"><font style='color:red;'><b>מבלי לגרוע באמור לעיל, החברה מתחייבת לשמור בסודיות מלאה את כל פרטי לקוחות הסוכן וכל מסמך ו/או מידע מזהה ביחס אליהם ולא להעביר פרטים ומידע אלה לכל סוכן אחר או לכל חברה אחרת לצורך שיווק פוליסות ביטוח ו/או מוצרים פיננסיים אחרים ו/או בכלל.</b></font></p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">המשתמש מסכים לכך שהחברה תהא רשאית לשמור את המידע שיימסר על ידו 
במאגרי המידע שלה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</font><font SIZE="2"><br>
באם המשתמש אינו מעוניין לעשות שימוש במידע הנמסר על ידו, יודיע המשתמש לחברה על כך 
והחברה</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </font>
<font SIZE="2">תדאג לא לעשות שימוש במידע כאמור מיום קבלת ההודעה ואילך</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">זכויות והגבלת שימוש</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font></b><font FACE="Times New Roman">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">למען הסר כל ספק מובהר בזאת כי זכויות היוצרים ושאר הזכויות 
הקנייניות בתוכנה ובתכניה, שייכות אך ורק לחברה</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
</font><font SIZE="2">
<p ALIGN="RIGHT">מובהר ומוסכם כי המשתמש ו/או מי מטעמו לא יהיה רשאי למכור ו/או 
למסור ו/או להשכיר ו/או לחקות ו/או להרשות שימוש ו/או לעשות שינוי בתוכנה ו/או 
להעביר באיזו דרך שהיא כל מידע או חלק ממנו שמקורו בתוכנה זו, בכולו או במקצתו, בין 
בתמורה ובין ללא תמורה, ללא קבלת הסכמתה של החברה בכתב ומראש.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font>
<font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">שיפוי</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
<p ALIGN="RIGHT"></p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">המשתמש ישפה את החברה ו/או מי מטעמה בגין נזק, ו/או הפסד ו/או 
אבדן רווח ו/או תשלום או הוצאה שייגרמו להם, לרבות הוצאות משפט ושכ&quot;ט עורך דין,</font><font SIZE="2" COLOR="#323232"> 
עקב הפרת תנאי שימוש אלה על ידי המשתמש ו/או על ידי מי מטעמו. </font>
<font FACE="Times New Roman (Hebrew)" SIZE="2"></p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">שונות </u></font>
<font FACE="Times New Roman (Hebrew)" SIZE="2">:</p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">הסכם זה מנוסח בלשון זכר מטעמי נוחות בלבד, אך הינו מתייחס גם 
לנשים.</p>
<p ALIGN="RIGHT">הכותרות ו/או ההדגשות בתנאי שימוש אלו משמשות לשם הנוחיות בלבד 
ואין לייחס להן משמעות פרשנית כלשהי</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.
</font><font FACE="Arial" SIZE="2"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">על הסכם זה יחולו</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">חוקי מדינת ישראל. המשתמש מסכים כי בכל מחלוקת הנובעת</font><font FACE="Arial" SIZE="2">
</font><font SIZE="2">מהשימוש בתוכנה או הקשורה בה תחול סמכות השיפוט המקומית של 
בתי המשפט</font><font FACE="Arial" SIZE="2"> </font><font SIZE="2">במחוז חיפה 
בלבד.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">מובהר ומוסכם כי כל הסכם לרבות נספחים, שיערך בין הצדדים,</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">יהיה כפוף לתנאי השימוש בתוכנה. במידה ותנאי השימוש סותרים 
ו/או אינם מתיישבים בקנה אחד עם התנאים בהסכם אחר יגברו תנאי השימוש בתוכנה, והצדדים 
יפעלו על פיהם.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">החברה שומרת לעצמה את הזכות לשנות את תנאי השימוש בתוכנה מעת לעת, 
זאת ללא מתן הודעה מראש ועל הצדדים יחולו תנאי השימוש המתוקנים מיום פרסומם ואילך.</p>
</b></font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">תנאי לשימוש בתוכנה הוא כי המשתמש קרא בקפידה את תנאי השימוש בתוכנה, 
הבין את משמעותם והסכים לקבלם כפי שהם.</p>
</font>
</div><div style='background-image:url(skin/portal/bg1.png); background-repeat:no-repeat; zoom:1; background-position:right bottom; height:8px; padding-right:10px;'>
<div style='background-image:url(skin/portal/bg1.png); background-repeat:no-repeat; zoom:1; background-position:left bottom; height:8px;'></div></div>
<?}
?>