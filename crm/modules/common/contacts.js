var today = new Date();

MailSuperCombo = function(config) {
	MailSuperCombo.superclass.constructor.call(this, {
		store: new Ext.data.Store({
			proxy: new Ext.data.HttpProxy({url: '?m=common/contacts&f=email_list'}),
			reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['name','email'])
		}),
		fieldLabel:'אל (דוא"ל)', allowBlank:false, msgTarget:'under', allowAddNewData:true,
		itemDelimiterKey:{'13':true,/*'32':true,*/'59':true,'188':true},
		xtype:'superboxselect', emptyText:'הקלד דוא"ל', resizable:true, valueField:'email',
		regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/,
		displayField: 'email', minChars: 2, anchor:'-7', pageSize:10,
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">&lt;{email}&gt;<i>{name}</i></div></tpl>'),
		itemSelector: 'div.search-item',
		displayFieldTpl: '{email}',
		forceSelection:true, extraItemCls:'x-tag',
		listeners: {
			newitem: function(bs,v){
				v = v.toLowerCase();
				if (/^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/.test(v)){
					var newObj = {email: v,name: v};
					bs.addNewItem(newObj);
				}
			}
		}
	});
	Ext.apply(this, config);
};
Ext.extend(MailSuperCombo, Ext.ux.form.SuperBoxSelect);

PostWindow = function(grid,d) {
	this.grid=grid;
	
	this.clientCombo = new ClientCombo({fieldLabel:'הנמען', anchor:'100%', value:(d ? d.receiver: '') }),
	this.clientCombo.on('select', function(e,r){var fld=this.form.find('name','adr')[0]; if (!fld.getValue()) fld.setValue(r.json.address)}, this);
	
	this.form = new Ext.form.FormPanel({
		labelWidth:40, 
		baseCls: 'x-plain',
		items: [
			{layout:'table', style:'margin-bottom:3px', layoutConfig:{columns:6}, baseCls:'x-plain', cls:'rtl', defaults: {layout:'form', baseCls: 'x-plain'},
				items:[
					{xtype:'label', text:'תאריך:', style:'display:block; width:40px'},
					{xtype:'xdatetime', name:'date',  width: 140, format:'d/m/Y', cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false, value:((d && d.date) ? d.date: today)},
					{xtype:'label', text:'שולח:', style:'display:block; width:35px;'},
					{xtype:'combo', store:users, hiddenName:'user_id', value:0, width:120, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, value:((d && d.user_id) ? d.user_id:user.user_id)},
					{xtype:'label', text: 'סוג משלוח:', style:'display:block; width:65px;'},
					{xtype:'combo', emptyText:'- בחר -', store:[['ragil','רגיל'],['rashum','רשום'],['havila','חבילה'],['express','אקספרס']], allowBlank:false, hiddenName:'type', width:87, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, value:((d && d.type)? d.type:'ragil')}
				]
			},
			(this.grid.client_id ? {xtype:'hidden', name:'client_id', value:this.grid.client_id} : this.clientCombo),
			{xtype:'textfield', name:'adr', id:'adr', fieldLabel:'כתובת', anchor:'100%',   cls:'rtl', value:((d && d.adr) ? d.adr : '')},
			{xtype:'textfield', name:'subject', fieldLabel:'תוכן', anchor:'100%',   cls:'rtl', value:((d && d.subject) ? d.subject : '')},
			{xtype:'textarea', name:'notes', height:45, anchor:'100%',  fieldLabel:'הערות', cls:'rtl', value:((d && d.notes) ? d.notes : '')},
			{xtype:'hidden', value:((d && d.contact_id) ? d.contact_id  : '')}
		]
	});
	
	PostWindow.superclass.constructor.call(this, {
		title: (d ? 'עריכת דואר' :'הוספת דואר חדש'),
		layout: 'fit', modal:true, width: 526, closeAction:'close',	buttonAlign:'center', bodyStyle:'padding:5px;',	plain: true,
		items: this.form,
		buttons: [
			{text:(d ? 'עדכן' : 'הוסף'), handler:function(){this.submitForm();}, scope:this},
			{text:'ביטול', handler:function(){this.close();}, scope:this}
		]
	});

	
	this.submitForm = function(overwrite) {
	
		if(!this.form.form.isValid()) return;
		
		this.form.form.submit({
			url: "?m=common/contacts&f=add_post",
			params:{receiver:this.clientCombo.getRawValue(), contact_id:((d && d.contact_id) ? d.contact_id : '')},
			waitTitle: 'אנא המתן...', waitMsg: 'טוען...', scope:this,
			success: function(f,a){
				this.close();
				grid.store.reload();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	};
	
	this.on('render', 
		function(){
			if (this.grid.client_id) Ext.Ajax.request({
				url:'?m=common/contacts&f=get_adr&client_id='+this.grid.client_id,
				success: function(r){r=Ext.decode(r.responseText); Ext.getCmp('adr').setValue(r.adr);},
				scope: this
			});
			}, 
		this
	);
	
};
Ext.extend(PostWindow, Ext.Window, {});

ContactWindow = function(cfg,d) {//grid,type,d,email
	this.grid=cfg.grid;
	var attach_flag=false;
	//new mail - get attachments
	if (cfg.attach) {
		attach_names=[];  attach_ids=[];
		Ext.each(cfg.attach, function(r){attach_ids.push(r.data.id); attach_names.push(r.data.name); });
		attach_names = attach_names.join(',');
		attach_ids = attach_ids.join(',');	
		attach_flag=true;
	}	
	
	//edit mail - get attachments (from db)
	else if(d){
		if (d.json.attach_ids) {
			var attach_ids=d.json.attach_ids; 
			var attach_names=d.json.attach_names;
			attach_flag=true;
		}
	}
	this.mailSuperCombo = new MailSuperCombo({value:(cfg.type=='mail' ? cfg.email:''), allowBlank:(cfg.type!='mail'),hidden:(cfg.type!='mail')});

	this.form = new Ext.form.FormPanel({
		id:'contactForm', labelWidth:80, baseCls:'x-plain',
		items: [
			this.mailSuperCombo
			,{
				layout:'column', baseCls:'x-plain',	defaults: {xtype:'panel', layout:'form', baseCls:'x-plain', border:false},
				items:[{
					bodyStyle:'padding:0', columnWidth:0.45,
					items: [
						{xtype:'combo', fieldLabel:'חשיבות', name:'importance', hiddenName:'importance', valueField:'id',  displayField:'val', anchor:'100%', value: (d ? d.json.importance : 0), mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:false, store:[[0,'רגיל'],[1,'חשוב'],[2,'דחוף']]}
						,{fieldLabel:'איש מכירות',  name:'agent', hiddenName:'agent_id', xtype:'combo', valueField:'id', displayField:'val', anchor:'100%', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:true, store:agents, value: ((d && d.json.agent_id) ? d.json.agent_id : 0), hidden:(this.grid.module=='claims' || this.grid.module=='garage'?true:false)}
					]
				},{
					bodyStyle:'padding-right:78px' ,columnWidth:0.55, items: [
						{fieldLabel:'תאריך', name:'date', xtype:'xdatetime', hiddenFormat:'d/m/Y H:i', value:(d && cfg.type!='mail' ? d.json.date : new Date().format('d/m/Y H:i'))},
						{fieldLabel:'תאריך תזכורת', name:'remind_date', xtype:'xdatetime', hiddenFormat:'d/m/Y H:i', value:(d ? d.json.remind_date : '')}
					]
				}]
			}
			,{fieldLabel:'נושא', name:'subject', xtype:'textfield', anchor:'-7', allowBlank:false }
			
			,{xtype:'panel', baseCls:'x-plain', anchor:'-7', items:[
				{xtype:(cfg.type=='mail'?'htmleditor':'textarea'),
					id:'html_notes', name:'notes', width:560, height:150, 
					listeners: { initialize:function(){this.getEditorBody().style.direction='rtl';} },
					value:(d ? d.json.notes:''+(cfg.type=='mail'?this.grid.signature:''))
				}]
			}		
			,{
				xtype:'panel', layout:'table', hidden:!attach_flag, layoutConfig:{columns:2}, baseCls:'x-plain', bodyStyle:'direction:rtl;',
				items: [
					 {xtype:'label', text:'קבצים מצורפים:', cls:'label', width:92, style:'display:block; text-align:left; background:url(skin/icons/attach.gif) no-repeat right 1px;' }
					,{html:(attach_flag ? attach_names :''), style:'padding-right:5px;color:darkblue;font-weight:bold;', baseCls:'x-plain' }
				]
			}
			,{name:'attach', xtype:'hidden', value:(attach_flag ? attach_ids :'')}
			,{name:'type', xtype:'hidden', value:cfg.type}
		]
	});
	
	if (d) this.form.find('name','subject')[0].setValue(d.json.subject);
	
	ContactWindow.superclass.constructor.call(this, {
		title: (cfg.type=='mail' ? 'שליחת דוא"ל <span style="font-weight:normal;font-size:10px">(יוצר התקשרות חדשה)</span>' : (d ? 'עריכת התקשרות' :'הוספת התקשרות') ),
		modal:true, width:590, closeAction:'close',	buttonAlign:'center', bodyStyle:'padding:5px;',	plain:true,
		items: this.form,
		buttons: [
		{text:(cfg.type=='mail' ? (d?'שלח שוב':'שלח') : 'שמור'), disabled:(d && d.data &&(/* d.data.type=='mail' || */ d.data.type=='sms')), handler:function(){this.submitForm();}, scope:this}
		,{text:'הדפס', handler:function(){this.contactPrint();}, scope:this, hidden:!(d&&d.data&&d.data.type=='mail')}
		,{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.contactPrint = function(){
		window.open('?m=common/contacts&f=contact_print&client_id='+this.grid.client_id+'&client_type='+this.grid.client_type+'&contact_id='+d.data.contact_id, '', 'toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	};
	

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		this.form.form.submit({
			url: '?m=common/contacts&f=add_contact',
			params:{client_type:this.grid.client_type, client_id:this.grid.client_id, link:this.grid.link, id:(d ? d.data.contact_id : 0), emails:this.mailSuperCombo.getValue()},
			waitTitle:'אנא המתן...', waitMsg:'טוען...', scope:this,
			success: function(f,a){
				this.close();
				var json = Ext.decode(a.response.responseText);
				if (cfg.callBack) cfg.callBack();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}

	if (!d && cfg.type=='mail')	Ext.Ajax.request({
		url:'?m=common/contacts&f=signature&a=load',
		success: function(r){
			r=Ext.decode(r.responseText); 
			if (Ext.getCmp('html_notes').getValue()!='undefined') Ext.getCmp('html_notes').setValue(Ext.getCmp('html_notes').getValue()+'<br/><br/><br/><br/><br/>'+r.data.signature);
			else Ext.getCmp('html_notes').setValue('<br/><br/><br/><br/><br/>'+r.data.signature);
		},
		scope: this
	});
};
Ext.extend(ContactWindow, Ext.Window, {});

SmsWindow = function(cfg,d) {
	this.grid=cfg.grid;
	Dump(this.grid.client_type);

	if (d&&d.data) this.data=d.data;

	this.addTel=function(){
		Ext.getDom('sms_tels').options.add(new Option(Ext.getCmp('sms_tel').getValue()));
		Ext.getCmp('sms_tel').setValue('');
	}
	
	this.checkTel=function(t){
		t=t.replace(/[^0-9 -]+/g, '');
		Ext.getCmp('sms_tel').setRawValue(t);
		var t1=t.replace(/[^0-9]+/g, '');
		var isValid=(/^05[024][0-9]{7}$/).test(t1);
		if (isValid) Ext.getCmp('sms_tel_add').enable();
		else Ext.getCmp('sms_tel_add').disable();
		return ((t=='' && Ext.getDom('sms_tels').length) ? true : isValid);
	}
	
	this.checkOriginator=function(t){
		if (t=='') return true;
		t=t.replace(/[^0-9 -]+/g, '');
		Ext.getCmp('originator').setRawValue(t);
		var t1=t.replace(/[^0-9]+/g, '');
		//return (/^05[024][0-9]{7}$/).test(t1);
		return (/^0[0-9]{8,9}$/).test(t1);
	}
	
	this.removeTel=function(){
		var sel=Ext.getDom('sms_tels');
		sel.remove(sel.selectedIndex);
	}
	
	this.charCount=function(fld){
		var v=fld.getValue();
		var msg='באורך: <b>'+v.length+'</b> תווים';
		if (v.length>70) msg+=' (<b>'+Math.ceil(v.length/67)+'</b>)';// <img src="skin/icons/help.gif" width=16 height=16 ext:qtip="">
		Ext.get('char_count').update(msg);
	}
	
	this.patternLoad = function(id) {
		// this.getEl().mask('...טוען');		
		Ext.Ajax.request({
			url:'?m=common/contacts&f=pattern&a=load&sms=1&id='+id,
			success: function(r,o){r=Ext.decode(r.responseText);if (!ajRqErrors(r)) { this.form.form.setValues({notes:r.data.text}); this.charCount(this.form.find('name','notes')[0])};},// this.getEl().unmask();
			failure:function(){this.getEl().unmask()},
			scope:this
		});
	};

	this.form = new Ext.form.FormPanel({
		id:'contactForm',
		labelWidth:80,
		baseCls:'x-plain',
		items: [
			{layout:'table', layoutConfig: {columns:5}, baseCls: 'x-plain', cls:'rtl', 
				items:[
					{xtype:'label', text:'חשיבות:', cls:'label', style:'display:block; text-align:left'}
					,{xtype:'combo', name:'importance', hiddenName:'importance', valueField:'id',  displayField:'val', width: 139, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:false, store:[[0,'רגיל'],[1,'חשוב'],[2,'דחוף']], value:(this.data&&this.data.importance ? this.data.importance : 0)}
					,{xtype:'label', text:'תאריך:', cls:'label', style:'display:block; text-align:left; padding-right:32px'}
					,{xtype:'xdatetime', name:'date', colspan:2, hiddenFormat:'d/m/Y H:i', value:(this.data&&this.data.date ? this.data.date : new Date().format('d/m/Y H:i'))}
					
					,{xtype:'label', text:'סוכן:', cls:'label', style:'display:block; text-align:left; padding-right:32px'}
					,{xtype:'combo', name:'agent', hiddenName:'agent_id', valueField:'id', displayField:'val', width:139, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:true, emptyText:'- בחר -', store:agents, value:(this.data&&this.data.agent ? this.data.agent : null)}

					,{xtype:'label', text:'מ-:', cls:'label', style:'display:block; text-align:left'}
					,{xtype:'textfield', name:'originator', id:'originator', cls:'ltr', colspan:2, value:(sms_originator?sms_originator:''), width:138, enableKeyEvents:true, validator:this.checkOriginator}
		
					,{xtype:'label', text:'תבנית:', cls:'label', style:'display:block; text-align:left'}
					,{xtype:'combo', store:smsPatterns,  name:'pattern_name', id:'pattern_name', value:'ללא תבנית', colspan:4, width:139, listClass:'rtl', triggerAction:'all', forceSelection:true, listeners:{scope:this, select:function(e){this.patternLoad(e.getValue())}}}
					
					,{xtype:'label', text:'הודעה ', cls:'label', style:'display:block; text-align:left'}
					,{id:'char_count', html:'באורך:', baseCls:'x-plain', border:false, style:'line-height:18px'}
					,{xtype:'label', text:'טל:', cls:'label', style:'display:block; text-align:left'}
					,{xtype:'textfield', id:'sms_tel', cls:'ltr', width:138, enableKeyEvents:true, validator:this.checkTel }
					,{xtype:'button', text:'הוסף', id:'sms_tel_add', disabled:true, scope:this, handler:this.addTel}
					
					,{xtype:'textarea', name:'notes', colspan:3, height:72, width:224, allowBlank:false, enableKeyEvents:true, listeners:{scope:this, keyup:this.charCount, change:this.charCount},value:(this.data&&this.data.notes ? this.data.notes : '') }
					,{bodyStyle:'padding-top:2px', html:"<select size=6 id=sms_tels dir=ltr style='width:138px; height:72px; border:0px solid #b5b8c8;'></select>"}
					,{xtype:'button', text:'הסר', scope:this, handler:this.removeTel, style:'margin-top:5px;'} 
				]
			}		
			,{xtype:'xdatetime',fieldLabel:'תאריך תזכורת', name:'back_date',  hiddenFormat:'d/m/Y H:i',value:(this.data&&this.data.back_date ? this.data.back_date : '')}
			,{xtype:'hidden', name:'tels'} 
		]
	});
	
	if (cfg.number) Ext.getCmp('sms_tel').setValue(cfg.number);
	// if (sms_msg_tpl) this.form.find('name','notes')[0].setValue(sms_msg_tpl);
	
	SmsWindow.superclass.constructor.call(this, {
		title:'הודעת SMS',	iconCls:'send_sms',	modal:true,	resizable:false, width:490,	buttonAlign:'center', bodyStyle:'padding:5px;',	plain:true,
		items:this.form,
		buttons: [{text:'שלח', handler:function(){this.submitForm();}, scope:this, iconCls:'send_sms'},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		// if (this.form.find('name','notes')[0].getValue().trim()==sms_msg_tpl.trim()) { this.form.find('name','notes')[0].markInvalid(); return; }
		
		// set tels value from select & text
		var ops=Ext.getDom('sms_tels').options;
		var tels=[];
		if (ops.length) for (i=0; i<ops.length; i++) tels.push(ops[i].text);
		if (Ext.getCmp('sms_tel').getValue()) tels.push(Ext.getCmp('sms_tel').getValue());		
		this.form.find('name','tels')[0].setValue(tels.join(','));
		
		if (tels.length==0) { Ext.getCmp('sms_tel').markInvalid(); return; }
		
		
		this.form.form.submit({
			url: '?m=common/contacts&f=add_contact',
			params:{client_type:this.grid.client_type, client_id:this.grid.client_id, link:this.grid.link, id:(d ? d.data.contact_id : 0)},
			waitTitle: 'אנא המתן...', waitMsg: 'טוען...', scope:this,
			success: function(f,a){
				this.close();
				var json = Ext.decode(a.response.responseText);
				if (cfg.callBack) cfg.callBack();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}	
};
Ext.extend(SmsWindow, Ext.Window, {});

SendFax = function(cfg) {
	if (cfg) Ext.apply(this, cfg);
	
	var d = cfg.d || '';
	
	var sender_name='', sender_fax='', sender_tel='';
	
	this.validaterecipient_fax=function(t){
		t=t.replace(/[^0-9]+/g, '');
		Ext.getCmp('recipient_fax').setRawValue(t);
		return (/^(0[0-9]{8,9})|(1[0-9]{9,11})$/).test(t);
		// return (/^0[0-9]{8,9}$/).test(t);
	}

	this.form = new Ext.form.FormPanel({
		labelWidth:70, baseCls:'x-plain', autoHeight:true,
		items:[
			 {hidden:(cfg.file?false:true), html:'שלח בפקס <b>'+cfg.file.name+'</b>', style:'padding:5px', baseCls:'x-plain' }
			,{fieldLabel:'מספר פקס', name:'recipient_fax', id:'recipient_fax', xtype:'textfield', validator:this.validaterecipient_fax, /* cls:'ltr', */ anchor:'50%', value:(d && d.json.recipient_fax ? d.json.recipient_fax:'')}
			,{hidden:(cfg.file?false:true), layout:'table', baseCls:'x-plain', style:'margin-bottom:5px', defaults:{baseCls:'x-plain'}, items:{xtype:'checkbox', boxLabel:'<b>עמוד מקדים</b>', name:'cover_page', id:'cover_page', checked:false, scope:this, handler:function(e){if(e.checked){Ext.getCmp('coverPageFields').show(); Ext.getCmp('preview_btn').show();} else {Ext.getCmp('coverPageFields').hide();Ext.getCmp('preview_btn').hide();}  this.syncShadow();}}}

			,{layout:'column', id:'coverPageFields', baseCls:'x-plain', defaults:{baseCls:'x-plain', border:false}, items:[
				{columnWidth:0.5, layout:'form', items:[
					{xtype:'textfield', fieldLabel:'לכבוד', name:'recipient_name', anchor:'100%', value:(d && d.json.recipient_name ? d.json.recipient_name:'')}
					,{xtype:'textfield', fieldLabel:'מאת', name:'sender_name', anchor:'100%', value:user.name}
					,{xtype:'textfield', fieldLabel:'פקס', name:'sender_fax', anchor:'100%', value:user.fax}
					,{xtype:'textfield', fieldLabel:'טלפון', name:'sender_tel', anchor:'100%', value:user.tel}
				]},{columnWidth:0.5, labelWidth:20, style:'padding-right:20px', items:[
					{xtype:'checkbox', boxLabel:'לוגו', name:'logo', checked:true}
				]}
				,{columnWidth:1, labelAlign:'top', items:[
					{xtype:'label', text:'הנדון:', style:'display:block'}
					,{fieldLabel:'הנדון', name:'subject', xtype:'textfield', width:523, style:'clear:both',value:(d && d.json.subject ? d.json.subject:'')}
					,{xtype:'label', text:'טקסט:', style:'display:block; margin-top:3px'}
					,{xtype:'htmleditor', name:'text', width:523, height:140, style:'clear:both'
						,listeners:{initialize:function(){this.getEditorBody().style.direction='rtl'}}
						,enableColors:false, enableFont:false, enableLinks:false, enableSourceEdit:false
						,value:(d && d.json.notes? d.json.notes:'')
					}
				]}
			]}
			// ,{name:'link_to', xtype:'hidden', value:(cfg.link_to?cfg.link_to:'clients') }
			// ,{name:'link_id', xtype:'hidden', value:(cfg.grid?cfg.grid.client_id:'') }
			,{name:'attach_id', xtype:'hidden', value:(cfg.file?cfg.file.id:'') }
			// ,{name:'link', xtype:'hidden', value:(cfg.grid && cfg.grid.link?cfg.grid.link:'')}
		]
	});
	
	SendFax.superclass.constructor.call(this, {
		title:'שליחת פקס', items:this.form, iconCls:'fax', layout:'fit', modal:true, width:550, autoHeight:true, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		buttons: [
			{text:'<b>שלח פקס</b>', iconCls:'fax', disabled:(d ? true : false), handler:function(){this.submitForm();}, scope:this}
			,{text:'תצוגה מקדימה', id:'preview_btn', iconCls:'preview', hidden:true, handler:function(){
				this.form.standardSubmit=true;
				this.form.form.el.dom.action='?m=common/fax&f=send_fax&a=preview';
				this.form.form.el.dom.target='coverpage';
				this.form.form.el.dom.submit();
			}, scope:this}
			,{text:'ביטול', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.on('show', function(){
		if (cfg.file) {
			Ext.getCmp('coverPageFields').hide();
			this.syncShadow();
		}else {
			Ext.getCmp('preview_btn').show();
			Ext.getCmp('cover_page').setValue(true);
		}
	});
	
	this.submitForm = function() {
		if(!this.form.form.isValid()) return; 
		// this.form.form.getEl().dom.target='_blank';  this.form.form.submit();  return; // open in new window

		this.el.mask('...טוען', 'x-mask-loading');
		this.form.form.submit({
			url:'?m=common/fax&f=send_fax',
			params:{client_type:this.grid.client_type, client_id:this.grid.client_id, link:this.grid.link},

			scope:this,
			success:function(f,a){
				this.close();
				if (cfg.grid && cfg.grid.title=='טיפול שוטף') cfg.grid.store.reload();
				Ext.Msg.alert('פקס נשלח', 'פקס נשלח');
			},
			failure:function(f,a){ this.close(); ajRqErrors(a.result); }
		});
	}	
};
Ext.extend(SendFax, Ext.Window, {});

// create the Grid {module:'client', id:this.d.id, cellNum:this.d.clientCell}
ContactGrid = function(cfg) {//client_id,client_type,link,clientCell,clientEmail,module,columns

	this.client_type= cfg.client_type;
	this.client_id	= cfg.client_id;
	this.link		= cfg.link ? cfg.link : '';
	this.clientCell	= cfg.clientCell ? cfg.clientCell : '';
	this.clientEmail= cfg.clientEmail ? cfg.clientEmail : '';

	this.store = new Ext.data.JsonStore({
		url:'?m=common/contacts&f=list_load&client_type='+this.client_type+'&client_id='+this.client_id+(this.link?'&link='+this.link:''),
		totalProperty:'total', delCount:'deleted', root:'data',
		fields: ['contact_id','user_id',{name:'date', type:'date', dateFormat:'d/m/Y H:i'},'importance',{name:'end',type:'date', dateFormat:'d/m/Y H:i'},{name:'remind_date',type:'date', dateFormat:'d/m/Y H:i'},'agent_id','firm','subject','type','treated','remind_text','notes','attach_ids','deleted', 'recipient_name', 'recipient_fax'],
		sortInfo: {field:'date', direction:'DESC'},	
		remoteSort:true
	});
	
	this.store.on('beforeload', function(store, options){
			this.store.removeAll();
			var setParams = {type_select: typeSelect.getValue()};
			Ext.apply(store.baseParams, setParams);
			Ext.apply(options.params, setParams);
		}, this);
	
	this.store.on('load', function(){	// check for errors
			var callback=function() { this.store.load(); };
			ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		}, this);

	// this.userRenderer = function(val) {return (users_obj[val] ? users_obj[val].fullName : '');}
	
	this.treatedRenderer = function(str, p, r){
		if(r.json.type!='task' && r.json.type!='fax_out'){
			if (r.data.treated==1) return "<img src='skin/icons/enable.gif' align=absmiddle>";
			else if (r.data.treated==-1) {
				if (today.format('YmdHi')>r.data.remind_date.format('YmdHi')) return "<img src='skin/icons/waiting.gif' align=absmiddle title='[בפיגור]'>";
				else return "<img src='skin/icons/waiting_green.gif' align=absmiddle title='[לא טופל]'>";
			}
		}
		else if(r.json.type=='fax_out'){ return "";	}
		else{
			if (r.data.treated==1 && r.json.type!='fax_out') {
				if (today.format('YmdHi')>r.data.end.format('YmdHi')) return "<img src='skin/icons/waiting.gif' align=absmiddle ext:qtip='בפיגור'>";
				else return "<img src='skin/icons/waiting_green.gif' align=absmiddle ext:qtip='טרם בוצעה'>";
			}
			else if (r.data.treated==2) return "<img src='skin/icons/enable.gif' ext:qtip='בוצעה' align=absmiddle>";
			else if (r.data.treated==3) return "<img src='skin/icons/info.gif' ext:qtip='הודעה בלבד'>";
		}
	};
	
	this.subjectRenderer = function(str, meta, record){
		meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(record.data.notes)+'" ext:qtitle="'+Ext.util.Format.htmlEncode(record.data.subject)+'"';
		var add='', pict='';
		if (record.json.importance==2) add = " <sup style='color:red'>דחוף</sup>";
		if (record.json.importance==1) add = " <sup style='color:#FF6600'>חשוב</sup>";
		if (record.json.importance==-1) add = " <sup style='color:green'>עדיפות נמוכה</sup>";
		
		if (record.json.type=='mail') (record.json.subject.indexOf('מכתב חידוש') != -1 ? pict='mail_yellow.gif' : pict='email.gif' );
		else if (record.json.type=='sms') pict='sms.gif';
		else if (record.json.type=='ragil' || record.json.type=='rashum' || record.json.type=='havila' || record.json.type=='express') pict='mail.gif';
		else if (record.json.type=='meeting') pict='handshake.png';
		else if (record.json.type=='task') pict='tasks.gif';
		else if (record.json.type=='fax_out') {if (record.json.treated==1) pict='fax-wait.gif'; else if (record.json.treated==2) pict='fax-out.gif'; else pict='fax-failed.gif';}
		else pict='call.png'
		return "<img src='skin/icons/"+pict+"' align=absmiddle> "+record.json.subject+add;
	};
	
	var typeSelect =    new Ext.form.ComboBox({store:[['all','הכל'],['phone','שיחות'],['meeting','סיכומי פגישה'],['mail','דוא"ל'],['sms','SMS'],['post','דואר יוצא'],['faxes','פקסים יוצאים'],['tasks','משימות'],['deleted','מבוטל']], value:'all', width:90, listClass:'rtl', triggerAction:'all', forceSelection:true, editable:false
		,listeners:{
			select:function(f){
				// Dump(this.getTopToolbar().find('text','הוסף'));
				if (f.getValue()=='deleted') {
					this.getTopToolbar().find('text','הוסף')[0].hide();
					this.getTopToolbar().items.get('contact_edit').hide();
					this.getTopToolbar().items.get('contact_del').hide();
					this.getTopToolbar().items.get('contact_rest').show();
					this.getTopToolbar().items.get('contacts_erase').show();
				} else {
					this.getTopToolbar().find('text','הוסף')[0].show();
					this.getTopToolbar().items.get('contact_edit').show();
					this.getTopToolbar().items.get('contact_del').show();
					this.getTopToolbar().items.get('contact_rest').hide();
					this.getTopToolbar().items.get('contacts_erase').hide();
				}
				this.store.reload();
			},
			scope:this
		}
	});
	// var typeSelect = new Ext.form.ComboBox({store:[[1,'פעילים'],[0,'מבוטלים']], value:1, width:80, listClass:'rtl', triggerAction:'all', forceSelection:true, 
		// listeners:{
			// select:function(f){	this.store.reload();}, 
			// change:function(f){if(f.getRawValue()==''){f.setValue(0);this.store.reload();} },
			// scope:this
		// }
	// })
	this.contactWindow = function(grid,type,d){
		var win;
		if (type=='task') {
			var data={client_id:this.client_id, client_type:this.client_type, link:this.link}; 
			if (d && d.data) {
				data.id=d.data.contact_id;
				data.start=d.data.date;
				data.importance=d.data.importance;
				data.name=d.data.subject;
				data.status=d.data.treated;
				data.notes=d.data.notes;
				data.user_id=d.data.user_id;
				data.end=d.data.end;
				data.remind=d.data.remind_date;
			}
			win = new TaskWindow(grid,data);
			win.show();
		}
		else if (type=='fax_out') {
			if(!d) {
				if (!faxActive) { Ext.Msg.alert('שרות פקס', 'לקבלת שרות פקס התקשרו: 074-7144333'); return; }
				var win=new SendFax({grid:this, file:'', d:''});
				win.show();
			}
			else {
				if (!faxActive) { Ext.Msg.alert('שרות פקס', 'לקבלת שרות פקס התקשרו: 074-7144333'); return; }
				// var win=new SendFax({grid:this, file:'', d:d});
				// win.show();			
	
				// var win=top.window.open('http://bafi.co.il/?m=crm/fax_get&type=out&fax_id=32', '', 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');  
				var win=top.window.open('http://'+location.host+'/crm/?m=common/fax_get&type=out'+'&fax_id='+d.data.contact_id, '', 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');  
				win.focus(); 
			}
		}
		
		else if (type=='phone' || type=='meeting' || type=='mail') {
			var cfg={grid:grid, type:type, email:this.clientEmail, callBack:function(){this.store.reload()}.createDelegate(this)};
			var win = new ContactWindow(cfg,d); win.show();}
		else if (type=='sms') {
			if (smsActive) {
			var cfg={grid:grid, number:this.clientCell, callBack:function(){this.store.reload()}.createDelegate(this) };
			var win=new SmsWindow(cfg,d);
			win.show();
			}else {
				Ext.Msg.alert('שרות SMS', 'לקבלת שרות SMS התקשרו: 074-7144333');
			}
		}
		else {win = new PostWindow(grid,((d && d.data) ? d.data : {})); win.show();}
	}
	
	
	// הגדרת עמודות: cols==default columns cfg, columns==database columns cfg, this.cols==shown columns
	this.cols = [];
	
	var cols = {
		user:		{header:'משתמש', 	dataIndex:'user_id',	id:'user', 		initColId:0, width:90, 	hidden:false, sortable:true, renderer:userRenderer}
		,notes:		{header:'הערות', 	dataIndex:'notes', 		id:'notes', 	initColId:1, 			hidden:false, renderer:function(val,meta,record,rowIndex,colIndex,store){meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(val)+'" ext:qtitle="'+Ext.util.Format.htmlEncode(record.data.subject)+'"'; return Ext.util.Format.ellipsis(Ext.util.Format.stripTags(val),100); }}
		,subject:	{header:'נושא', 	dataIndex:'subject', 	id:'subject',	initColId:2, width:180, hidden:false, sortable:true, renderer:this.subjectRenderer.createDelegate(this)}
		,agent:		{header:'איש מכירות',dataIndex:'agent_id', 	id:'agent',		initColId:4, width:100, hidden:false, sortable:true, renderer:userRenderer}
		,remind_date:{header:'תזכורת', 	dataIndex:'remind_date',id:'remind_date',initColId:5, width:100,hidden:false, sortable:true, css:'direction:ltr;', renderer:Ext.util.Format.dateRenderer('d/m/Y H:i')}
		,end:		{header:'יעד סיום', dataIndex:'end', 		id:'end',  		initColId:6, width:100, hidden:false, sortable:true, css:'direction:ltr;', renderer:Ext.util.Format.dateRenderer('d/m/Y H:i')}
		,date:		{header:'תאריך', 	dataIndex:'date', 		id:'date',  	initColId:7, width:100, hidden:false, sortable:true, css:'direction:ltr;', renderer:Ext.util.Format.dateRenderer('d/m/Y H:i')}
		,treated:	{header:'טופל', 	dataIndex:'treated',	id:'treated', 	initColId:8, width:30,  hidden:false, sortable:true, renderer: this.treatedRenderer.createDelegate(this)}
	};
	
	if (cfg.columns) {
		for (var c=0; c<cfg.columns.length; c++) {		
			ci=cfg.columns[c].dataIndex;
			cols[ci].hidden=(cols[ci].hidden || cfg.columns[c].hidden);
			cols[ci].width=(cfg.columns[c].width || cols[ci].width);
			this.cols.push(cols[ci]);
		}
	}else for (var c in cols) this.cols.push(cols[c]);
	
	this.onItemCheck = function (item, checked){
		var cm = this.getColumnModel();
		if (item.text=='משתמש')			cm.setHidden(cm.getIndexById('user'), !checked); 
		else if (item.text=='חברה')		cm.setHidden(cm.getIndexById('firm'), !checked); 
		else if (item.text=='איש מכירות') cm.setHidden(cm.getIndexById('agent'), !checked); 
		else if (item.text=='תזכורת')	cm.setHidden(cm.getIndexById('remind_date'), !checked); 
		else if (item.text=="יעד סיום")	cm.setHidden(cm.getIndexById('end'), !checked); 
		else if (item.text=="תאריך")	cm.setHidden(cm.getIndexById('date'), !checked);
    };
	
	this.resetColumns = function (){
		var cm = this.getColumnModel();
		for(var c in cols) {	
			cm.setHidden(cm.getIndexById(c), cols[c].hidden);
			cm.moveColumn(cm.getIndexById(c),cols[c].initColId);
		}
    };

	this.tbar=[
		'קטגוריה :', typeSelect
		,'-' ,{ text:'שחזר התקשרות', id:'contact_rest', iconCls:'restore', hidden: true, handler:function(){this.restContact();}, disabled:true, scope:this }
		,{text:'הוסף', iconCls:'add', //x-menu-item-icon 
			menu: {items: [
				{ text:'שיחה', iconCls:'contact_add', handler:function(){this.contactWindow(this,'phone');}, scope:this }
				,{ text:'דוא"ל', iconCls:'mail_add', handler:function(){this.contactWindow(this,'mail');}, scope:this }
				,{ text:'סיכום פגישה', iconCls:'handshake', handler:function(){this.contactWindow(this,'meeting');}, scope:this }
				,{ text:'פקס', iconCls:'fax_add', handler:function(){this.contactWindow(this,'fax_out');}, scope:this }
				,{ text:'משימה', iconCls:'task', handler:function(){this.contactWindow(this,'task',{});}, scope:this }
				,{ text:'דואר יוצא', iconCls:'post_add', handler:function(){this.contactWindow(this,'post');}, scope:this }
				,{ text:'SMS', iconCls:'send_sms', handler:function(){this.contactWindow(this,'sms');}, scope:this }
			]}
		}
		,'-' ,{ text:'עדכן', id:'contact_edit', iconCls:'edit', handler:function(){this.contactWindow(this, this.getSelectionModel().getSelected().data.type, this.getSelectionModel().getSelected());}, disabled:true, scope:this }
		,'-' ,{ text:'רוקן סל התקשרויות מבוטלות',  id:'contacts_erase', iconCls:'delete', hidden:true, handler:function(){this.eraseContacts();}, scope:this }
		,{ text:'מחק',  id:'contact_del', iconCls:'trash', handler:function(){this.delContact();}, disabled:true, scope:this }
		,{ text:'הדפס',  id:'contacts_print', iconCls:'print', handler:function(){this.contactsPrint(this);}, scope:this }
		,{text:'הגדרת עמודות', iconCls:'cols-icon', //x-menu-item-icon 
			menu: {items: [
				{text:'תאריך', checked:!cols['date'].hidden, checkHandler:this.onItemCheck, scope:this},
				{text:"יעד סיום", checked:!cols['end'].hidden, checkHandler:this.onItemCheck, scope:this},
				{text:"תזכורת", checked:!cols['remind_date'].hidden, checkHandler:this.onItemCheck, scope:this},
				{text:"איש מכירות", checked:!cols['agent'].hidden, checkHandler:this.onItemCheck, scope:this},
				{text:'משתמש', checked:!cols['user'].hidden, checkHandler:this.onItemCheck, scope:this}
			]}
		}
	];
	
	this.colModel= new Ext.grid.ColumnModel({
        columns: this.cols,
		listeners:{
			columnmoved:function() 	{parent.contact_grid_columns=this.columns; contact_grid_columns=this.columns; setObjView({a:'set_columns', module:'clients', object:'ContactGrid', columns:Ext.encode(this.columns)})}
			,hiddenchange:function(){parent.contact_grid_columns=this.columns; contact_grid_columns=this.columns; setObjView({a:'set_columns', module:'clients', object:'ContactGrid', columns:Ext.encode(this.columns)})}
			// ,widthchange:function() {alert(555);setObjView({a:'set_columns', module:'clients', object:'ContactGrid', columns:Ext.encode(this.columns)})}
		}
    });

	this.bbar = new Ext.PagingToolbar({store:this.store, pageSize:25, displayInfo:true});

	ContactGrid.superclass.constructor.call(this, {
		stripeRows: true, autoExpandColumn:'notes',	cls:'ltr', border:false, enableHdMenu:false, width:'100%', title:'טיפול שוטף',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false})
	});

	this.store.load();
	
	this.on('rowdblclick', function(grid, row, e){var d=this.store.getAt(row); this.contactWindow(this ,d.data.type, d);});

	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row).data;
		var d=this.store.getAt(row);
		
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				 {text:(d.data.type!='task' && d.data.treated==1 ? 'סמן כלא טופל':'סמן כטופל'), hidden:(d.data.type=='task' || d.data.type=='fax_out'), disabled:(d.data.type!='task' && d.data.treated=='0'), iconCls:(d.data.treated==1 ? 'not_treated':'treated'), scope:this, handler:function(){this.changeStatus(d);}}
				,{text:'עדכן התקשרות', iconCls:'edit',scope:this, handler:function(){this.contactWindow(this ,d.data.type, d);} }
				,{text:'מחק התקשרות',  iconCls:'delete', scope:this, handler:function(){this.selModel.selectRow(row); this.delContact();}}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('contact_edit').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('contact_rest').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('contact_del').setDisabled(!sm.getCount());
	}, this);
	
	this.changeStatus = function(record){
		Ext.MessageBox.show({
			width:250, title:'לשנות סטטוס התקשרות?', msg:'האם ברצונך לשנות סטטוס של ההתקשרות הזאת ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) {
				if(btn=='yes') {
					// this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=common/contacts&f=change_status&client_type='+this.client_type+'&client_id='+this.client_id+'&contact_id='+record.data.contact_id+'&treated='+record.data.treated,
						success: function(r,o){
							r=Ext.decode(r.responseText);
							if (!ajRqErrors(r) && r.total>0) record.set('treated', -record.data.treated );
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				}
			}
		});
	};

	this.contactsPrint = function(grid){
		var sort = grid.store.sortInfo.field;
		var dir = grid.store.sortInfo.direction;
		typeSelected = typeSelect.getValue();
		url='&f=list_load&link='+this.link+'&client_id='+this.client_id+'&client_type='+this.client_type+'&sort='+sort+'&dir='+dir+'&type_select='+typeSelected+'&print_rep=true';
		window.open('?m=crm/contacts_f' + url, '', 'toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	};

	this.delContact = function(){
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.agency);});
		
		Ext.MessageBox.show({
			width:250, title:'למחוק התקשרות?', msg:'האם ברצונך לבטל את ההתקשרות הזאת ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') {
					var contact_ids=[], task_ids=[], fax_out_ids=[];
					
					Ext.each(records, function(r){if (r.data.type=='task') task_ids.push(r.data.contact_id); else if (r.data.type=='fax_out') fax_out_ids.push(r.data.contact_id); else contact_ids.push(r.data.contact_id);});
					
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=common/contacts&f=del_contact&client_type='+this.client_type+'&client_id='+this.client_id+'&contact_ids='+contact_ids.join(',')+'&task_ids='+task_ids.join(',')+'&fax_out_ids='+fax_out_ids.join(','),
						success: function(r,o){
							r=Ext.decode(r.responseText);
							if (!ajRqErrors(r)) {
								Ext.each(records, function(r){
									row=this.store.indexOf(r);
									this.store.getAt(row).data.deleted=1;
									this.selModel.selectRow(row);
									this.view.getRow(row).style.border='1px solid red';
									Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:true, scope:this, callback:function(){this.store.filter('deleted','0'); this.view.refresh();} });
								},this);
							}
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});				
				}
			}
		});
	};
	
	this.restContact = function(){
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.agency);});
		
		Ext.MessageBox.show({
			width:300, title:'לשחזר התקשרות?', msg:'האם ברצונך לשחזר את ההתקשרות הזאת ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') {
					var contact_ids=[];
					var task_ids=[];
					var fax_out_ids=[];
					Ext.each(records, function(r){if (r.data.type=='task') task_ids.push(r.data.contact_id); else if (r.data.type=='fax_out') fax_out_ids.push(r.data.contact_id); else contact_ids.push(r.data.contact_id); });
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=common/contacts&f=rest_contact&client_id='+this.client_id+'&contact_ids='+contact_ids.join(',')+'&task_ids='+task_ids.join(',')+'&fax_out_ids='+fax_out_ids.join(','),
						success: function(r,o){
							r=Ext.decode(r.responseText);
							if (!ajRqErrors(r)) {
								Ext.each(records, function(r){
									row=this.store.indexOf(r);
									this.store.getAt(row).data.deleted=0;
									this.selModel.selectRow(row);
									this.view.getRow(row).style.border='1px solid red';
									Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:true, scope:this, callback:function(){this.store.filter('deleted','1'); this.view.refresh();} });
								},this);
							}
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				}
			}
		});
	};
	
	this.eraseContacts = function(){
		Ext.MessageBox.show({
			width:250, title:'לרוקן סל?', msg:'האם ברצונך לרוקן סל התקשרויות מבוטלות ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') {
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=common/contacts&f=erase_contact&client_id='+this.client_id,
						success: function(r,o){
							r=Ext.decode(r.responseText);
							if (!ajRqErrors(r)) this.store.load();
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				}
			}
		});
	};
};
Ext.extend(ContactGrid, Ext.grid.GridPanel);
