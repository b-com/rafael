<?php
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

$client_id=(int)$_REQUEST['client_id'];
$client_type=$_REQUEST['client_type'];
if (!$_GET['init']) $NO_SKIN=1;

if (function_exists("f_{$f}")) call_user_func("f_{$f}");
else die('{success:false, msg:"function name error"}');

//———————————————————————————————————————————————————————————————————————————————————— 
function f_clients_shortList() {
 global $SVARS, $group_id;

	$start=(int)$_REQUEST['start'];
 	$limit=(int)$_REQUEST['limit'];
	
	$where="deleted=".(int)$_REQUEST['deleted'];
	 
	if ($_REQUEST['q']) {
		$make_where_search = make_where_search('name',$_REQUEST['q']);
		$where.=" AND ".$make_where_search;//search in user data
	}

	$sql="SELECT SQL_CALC_FOUND_ROWS id, "
		."tel_type0, tel0, tel_type1, tel1,tel_type2, tel2,tel_type3, tel3,tel_type4, tel4, fax,cnumber as passport ,"
		."TRIM(CONCAT(name,' ',lname,' ',fname)) AS client ,"
		."adr_type0,city0,adr0,zip0,adr_type1,city1,adr1,zip1,adr_type2,city2,adr2,zip2,email "
		
		//."cnumber, DATE_FORMAT(birthday,'%d/%m/%Y') AS birthday "
		."FROM clients WHERE company_id = '$SVARS[cid]' AND $where ORDER BY client "
	;
	
	$rows=sql2array($sql);
	$rows_json = array2json($rows);
	//print_ar($rows_json,'$rows_json');
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if (is_array($rows)) {
		foreach ($rows as $key=>$row) {
		//	$adr=$row['adr0'];
			//if ($row['city0']) $adr.=($adr?', ':'').$row['city0'];
			//if ($row['zip0']) $adr.=($adr?', ':'').$row['zip0'];
			$responce.= ($responce?',':'')
			."{
				id:'$row[id]',
				client:'".jsEscape($row['client'])."',
				passport:'$row[passport]',
				city0:'$row[city0]',
				fax:'$row[fax]',
				fam_status:'$row[fam_status]',
				adr0:'$row[adr0]',
				phones:'".jsEscape($row['tel0'])."',
				email:'$row[email]',
				birthday:'$row[birthday]'
			}";
		}
		$responce = $_REQUEST['callback'].'({total:'.$rowsCount.',data:['.$responce.']})';
		echo $responce;
	} else echo $_REQUEST['callback'].'({total:0,data:[]})';
}

//********************************************************************************************************************
function client_phones($r, $separator=', ',$tel_type='') {
	$telStore = array(1=>'בית',2=>'עבודה',3=>'נייד',4=>'פקס בבית',5=>'פקס בעבודה',0=>'אחר');
 
	if ($tel_type=='phone') 	
		{for ($i=0; $i<8; $i++) if ($r["tel$i"] AND $r["tel_type$i"]<4) $tel.=($tel ? $separator : '').$telStore[(int)$r["tel_type$i"]].': '.$r["tel$i"];}
	elseif($tel_type=='fax')
		{for ($i=0; $i<8; $i++) if ($r["tel$i"] AND $r["tel_type$i"]>=4) $tel.=($tel ? $separator : '').$telStore[(int)$r["tel_type$i"]].': '.$r["tel$i"];}
	else
		{for ($i=0; $i<8; $i++) if ($r["tel$i"]) $tel.=($tel ? $separator : '').$telStore[(int)$r["tel_type$i"]].': '.$r["tel$i"];}
	return htmlspecialchars($tel);
}
//********************************************************************************************************************
function client_emails($r, $separator=', ') {
	$emailStore= array(1=>'בית',2=>'עבודה',0=>'אחר');
	for ($i=1; $i<3; $i++) if ($r["email$i"]) $email.=($email ? $separator : '').$emailStore[(int)$r["email_type$i"]].': '.$r["email$i"];
	return htmlspecialchars($email);
}
//********************************************************************************************************************
function load_client_data() {
	global $SVARS;

	$id = (int) $_REQUEST['client_id'];
	$sql = "SELECT * FROM clients WHERE id = $id";
}
//********************************************************************************************************************
function client_addresses($r, $separator='; <br>', $amount=3) {
	$adrStore = array(1=>'בית',2=>'חברה/עסק',3=>'עבודה',4=>'למשלוח דואר',0=>'אחר');

	for ($i=0; $i<$amount; $i++) {
		if (trim($r["city$i"]) OR $r["zip$i"] OR trim($r["adr$i"])) {
			$a=$r["city$i"];
			if ($r["adr$i"])  $a.=($a?', ':'').$r["adr$i"];
			if ($r["zip$i"])  $a.=($a?', ':'').$r["zip$i"];
		$adr.= ($adr ? $separator : '').($a ? $adrStore[$r["adr_type$i"]].': ':'').htmlspecialchars($a);
		}
	}
	return ($adr?$adr:'');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_client() {
 global $SVARS;
	$id=(int)$_REQUEST['id'];

	$sql="clients SET name=".quote($_REQUEST['name']?$_REQUEST['name']:$_REQUEST['client'])
		.", company_id=".(int)$SVARS['cid']
		.", cnumber=".quote($_REQUEST['cnumber']?$_REQUEST['cnumber']:$_REQUEST['passport'])
		.", tel0=".quote($_REQUEST['phones'])
		.", tel1=".quote($_REQUEST['tel1'])
		.", fax=".quote($_REQUEST['fax'])
		.", email=".quote($_REQUEST['email'])
		.", url=".quote($_REQUEST['url'])
		.", notes=".quote($_REQUEST['notes'])
		.(isset($_REQUEST["active"])	? ($_REQUEST["active"]=='true' ? ", active=1":", active=0"):'')
		.(isset($_REQUEST["date_end"])	? ", date_end='".str2time($_REQUEST['date_end'],'Y-m-d')."'":'')
		;
	
	if ($_REQUEST['mod']) $sql.=", modules=".quote(implode(',',array_keys($_REQUEST['mod'])));
	
	$n=0;
	for ($i=0; $i<3; $i++) if ($_REQUEST["city$i"] OR $_REQUEST["zip$i"] OR $_REQUEST["adr$i"]) {$sql.=", adr_type$n=".(int)$_REQUEST["adr_type$i"].", city$n=".quote($_REQUEST["city$i"]).", zip$n=".quote($_REQUEST["zip$i"]).", adr$n=".quote($_REQUEST["adr$i"]); $n++;}
	for ($i=$n; $i<3; $i++) $sql.=", adr_type$i='', city$i='', zip$i='', adr$i=''";

	$n=0;
	for ($i=0; $i<5; $i++) if ($_REQUEST["cperson$i"] OR $_REQUEST["cp_tel$i"] OR $_REQUEST["cp_email$i"]) {$sql.=", cperson$n=".quote($_REQUEST["cperson$i"]).", cp_tel$n=".quote($_REQUEST["cp_tel$i"]).", cp_email$n=".quote($_REQUEST["cp_email$i"]); $n++;}
	for ($i=$n; $i<5; $i++) $sql.=", cperson$i='', cp_tel$i='', cp_email$i=''";

	$sql = ($id ? "UPDATE $sql WHERE id=$id" : "INSERT INTO $sql");
	//print_ar($sql);
	if (runsql($sql)) {
		if (!$id) $id = mysql_insert_id();
		
		if ($_POST['fax_numbers']) {
			runsql("DELETE FROM fax_numbers WHERE company_id=$id");
			$fax_numbers=explode(',',$_POST['fax_numbers']);
			if ($fax_numbers) foreach($fax_numbers as $k=>$number) runsql("INSERT INTO fax_numbers SET company_id=$id, fax_number=".quote($number));
		}
		
		$name = jsEscape(trim($_POST['name']));
		echo "{id:$id, name:'$name', success:true}"; 
	}else echo '{success:false}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_del_client() {
 global $SVARS, $group_id;

		//if (!preg_match('/\A[0-9]+(,[0-9]+)*$/', $_REQUEST['passport'])) die('{success:false, msg:"wrong id"}');

		$sql = "UPDATE clients SET deleted=1 WHERE id=".$_REQUEST['ids']; 
		
		if (runsql($sql) AND mysql_affected_rows()) echo "{success:true}"; 
			else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}?>