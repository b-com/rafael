<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }
#Get file from tmp OUTBOX folder
if ($_REQUEST['folder']=='OUTBOX') get_part_from_cache("../../tmp_files/mail_compose/".$SVARS['user']['id']."/$_REQUEST[pid]");

define('RCMAIL_CHARSET', 'UTF-8');
require_once 'modules/mail_new/inc/main.inc.php';
require_once 'modules/mail_new/inc/utf7.inc.php';
require_once 'modules/mail_new/inc/tnef_decoder.php';
// @include_once('lib/utf8.class.php');

require_once 'modules/mail_new/inc/rcube_config.php';
require_once 'modules/mail_new/inc/rcube_shared.inc.php';

require_once 'modules/mail_new/inc/rcube_mime_struct.php';
require_once 'modules/mail_new/inc/rcube_message.php';
require_once 'modules/mail_new/inc/rcube_imap_generic.php';
require_once 'modules/mail_new/inc/rcube_imap.php';
require_once 'modules/mail_new/inc/rcmail.php';

require_once 'modules/mail_new/inc/washtml.php';
// require_once 'modules/mail_new/inc/html.php';
require_once 'modules/mail_new/inc/func.inc.php';

$qur = file_get_contents("../../datafetch.spx");
$mail_accounts = sql2array("SELECT *,AES_DECRYPT(password, $qur) AS password FROM mail_accounts WHERE user_id=".$SVARS['user']['id']." ORDER BY `primary` DESC, name", 'id');

get_part();

//http://bafi.loc/?m=mail/get&account=2&folder=INBOX&uid=14&pid=2.2

//————————————————————————————————————————————————————————————————————————————————————
function mime_by_ext($filename){$idx = strtolower(end(explode('.',$filename))); $mimet=array('ai'=>'application/postscript','aif'=>'audio/x-aiff','aifc'=>'audio/x-aiff','aiff'=>'audio/x-aiff','asc'=>'text/plain','atom'=>'application/atom+xml','avi'=>'video/x-msvideo','bcpio'=>'application/x-bcpio','bmp'=>'image/bmp','cdf'=>'application/x-netcdf','cgm'=>'image/cgm','cpio'=>'application/x-cpio','cpt'=>'application/mac-compactpro','crl'=>'application/x-pkcs7-crl','crt'=>'application/x-x509-ca-cert','csh'=>'application/x-csh','css'=>'text/css','dcr'=>'application/x-director','dir'=>'application/x-director','djv'=>'image/vnd.djvu','djvu'=>'image/vnd.djvu','doc'=>'application/msword','dtd'=>'application/xml-dtd','dvi'=>'application/x-dvi','dxr'=>'application/x-director','eps'=>'application/postscript','etx'=>'text/x-setext','ez'=>'application/andrew-inset','gif'=>'image/gif','gram'=>'application/srgs','grxml'=>'application/srgs+xml','gtar'=>'application/x-gtar','hdf'=>'application/x-hdf','hqx'=>'application/mac-binhex40','html'=>'text/html','html'=>'text/html','ice'=>'x-conference/x-cooltalk','ico'=>'image/x-icon','ics'=>'text/calendar','ief'=>'image/ief','ifb'=>'text/calendar','iges'=>'model/iges','igs'=>'model/iges','jpe'=>'image/jpeg','jpeg'=>'image/jpeg','jpg'=>'image/jpeg','js'=>'application/x-javascript','kar'=>'audio/midi','latex'=>'application/x-latex','m3u'=>'audio/x-mpegurl','man'=>'application/x-troff-man','mathml'=>'application/mathml+xml','me'=>'application/x-troff-me','mesh'=>'model/mesh','mid'=>'audio/midi','midi'=>'audio/midi','mif'=>'application/vnd.mif','mov'=>'video/quicktime','movie'=>'video/x-sgi-movie','mp2'=>'audio/mpeg','mp3'=>'audio/mpeg','mpe'=>'video/mpeg','mpeg'=>'video/mpeg','mpg'=>'video/mpeg','mpga'=>'audio/mpeg','ms'=>'application/x-troff-ms','msh'=>'model/mesh','mxu m4u'=>'video/vnd.mpegurl','nc'=>'application/x-netcdf','oda'=>'application/oda','ogg'=>'application/ogg','pbm'=>'image/x-portable-bitmap','pdb'=>'chemical/x-pdb','pdf'=>'application/pdf','pgm'=>'image/x-portable-graymap','pgn'=>'application/x-chess-pgn','php'=>'application/x-httpd-php','php4'=>'application/x-httpd-php','php3'=>'application/x-httpd-php','phtml'=>'application/x-httpd-php','phps'=>'application/x-httpd-php-source','png'=>'image/png','pnm'=>'image/x-portable-anymap','ppm'=>'image/x-portable-pixmap','ppt'=>'application/vnd.ms-powerpoint','ps'=>'application/postscript','qt'=>'video/quicktime','ra'=>'audio/x-pn-realaudio','ram'=>'audio/x-pn-realaudio','ras'=>'image/x-cmu-raster','rdf'=>'application/rdf+xml','rgb'=>'image/x-rgb','rm'=>'application/vnd.rn-realmedia','roff'=>'application/x-troff','rtf'=>'text/rtf','rtx'=>'text/richtext','sgm'=>'text/sgml','sgml'=>'text/sgml','sh'=>'application/x-sh','shar'=>'application/x-shar','shtml'=>'text/html','silo'=>'model/mesh','sit'=>'application/x-stuffit','skd'=>'application/x-koan','skm'=>'application/x-koan','skp'=>'application/x-koan','skt'=>'application/x-koan','smi'=>'application/smil','smil'=>'application/smil','snd'=>'audio/basic','spl'=>'application/x-futuresplash','src'=>'application/x-wais-source','sv4cpio'=>'application/x-sv4cpio','sv4crc'=>'application/x-sv4crc','svg'=>'image/svg+xml','swf'=>'application/x-shockwave-flash','t'=>'application/x-troff','tar'=>'application/x-tar','tcl'=>'application/x-tcl','tex'=>'application/x-tex','texi'=>'application/x-texinfo','texinfo'=>'application/x-texinfo','tgz'=>'application/x-tar','tif'=>'image/tiff','tiff'=>'image/tiff','tr'=>'application/x-troff','tsv'=>'text/tab-separated-values','txt'=>'text/plain','ustar'=>'application/x-ustar','vcd'=>'application/x-cdlink','vrml'=>'model/vrml','vxml'=>'application/voicexml+xml','wav'=>'audio/x-wav','wbmp'=>'image/vnd.wap.wbmp','wbxml'=>'application/vnd.wap.wbxml','wml'=>'text/vnd.wap.wml','wmlc'=>'application/vnd.wap.wmlc','wmlc'=>'application/vnd.wap.wmlc','wmls'=>'text/vnd.wap.wmlscript','wmlsc'=>'application/vnd.wap.wmlscriptc','wmlsc'=>'application/vnd.wap.wmlscriptc','wrl'=>'model/vrml','xbm'=>'image/x-xbitmap','xht'=>'application/xhtml+xml','xhtml'=>'application/xhtml+xml','xls'=>'application/vnd.ms-excel','xml xsl'=>'application/xml','xpm'=>'image/x-xpixmap','xslt'=>'application/xslt+xml','xul'=>'application/vnd.mozilla.xul+xml','xwd'=>'image/x-xwindowdump','xyz'=>'chemical/x-xyz','zip'=>'application/zip');
	if (isset($mimet[$idx])) return $mimet[$idx];  else return 'application/octet-stream';
}

//————————————————————————————————————————————————————————————————————————————————————
/*
* Be carefull. Changing files path affect on mail sending
*/
function get_part() {
 global $CFG, $group_id, $RCMAIL, $mail_accounts;
	$uid=(int)$_REQUEST['uid'];
	$pid=$_REQUEST['pid'];
	$account_id=(int)$_REQUEST['account'];
	
	if (!$uid) die("Error: no uid");
	// if (!preg_match('/^[0-9]+[0-9\.]*$/',$pid)) die("Error: no pid");
	// if (!$pid) die("Error: no pid");
	if (!isset($pid)) die("Error: no pid");

	if((int)$_REQUEST['crm']){
		$client_id=(int)$_REQUEST['client_id'];
		$contact_id=(int)$_REQUEST['contact_id'];
		// $files = glob("../../mail_data/$account_id/$client_id/$contact_id.*");
		$files = glob("../../mail_data/$group_id/".substr($client_id, -2)."/$client_id/$contact_id/$pid.*");
	}else{
		if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'wrong account'}");
	
		$folder=rawurldecode($_REQUEST['folder']);
		if (!$folder) $folder = base64_encode('INBOX');

		$files = glob("../../temp/mail/$account_id/".base64_encode($folder)."/_$uid/$pid.*");
	}
	if ($files[0] AND is_file($files[0])) get_part_from_cache($files[0]); # for cached inline images
	else get_part_from_server();
}

//————————————————————————————————————————————————————————————————————————————————————
function get_part_from_cache($file) {
 global $CFG, $m, $a, $group_id, $RCMAIL, $mail_accounts;
	$mimetype = mime_by_ext($file);
	header("Content-Type: $mimetype");
	header("Content-Transfer-Encoding: binary");
	header('Content-Length: '.filesize($file));
	readfile($file);
	exit;	
}

//————————————————————————————————————————————————————————————————————————————————————
function get_part_from_server() {
 global $CFG, $m, $a, $group_id, $RCMAIL, $mail_accounts;
	$uid=(int)$_REQUEST['uid'];
	$pid=$_REQUEST['pid'];
	
	if (!$uid) die("Error: no uid");
	// if (!preg_match('/^[0-9]+[0-9\.]*$/',$pid)) die("Error: no pid");
	if (!isset($pid)) die("Error: no pid");
	
	$folder=$_REQUEST['folder'];
	if (!$folder) $folder='INBOX';
	
	$account_id=(int)$_REQUEST['account'];
	if (!$account_id OR !$mail_accounts[$account_id]) die("{success:false, msg:'wrong account'}");
	$account=$mail_accounts[$account_id];
	
	$host = ($account['security']?$account['security'].'://':''). $account['host'].':'.$account['port'];

	$RCMAIL = rcmail::get_instance();
	
	$RCMAIL->login($account['username'], $account['password'], $host);
	
	if ($RCMAIL->imap->conn->error) die("{success:false, msg:'".jsEscape($RCMAIL->imap->conn->error)."'}");

	$RCMAIL->imap->select_mailbox($folder);
	
	$MESSAGE = new rcube_message($uid, $account_id);
	if (!$MESSAGE->headers) die("Error: Message not found.");
	
	if (!($part=$MESSAGE->mime_parts[$pid])) die("Error: Part not found.");

	$ctype_primary = strtolower($part->ctype_primary);
	$ctype_secondary = strtolower($part->ctype_secondary);
	$mimetype = sprintf('%s/%s', $ctype_primary, $ctype_secondary);

	$browser = new rcube_browser();
	
	if ($_GET['download']) {	// send download headers
		header("Content-Type: application/octet-stream");
		if ($browser->ie) header("Content-Type: application/force-download");
	}else if ($ctype_primary=='text') {
		header("Content-Type: text/$ctype_secondary; charset=" . ($part->charset ? $part->charset : RCMAIL_CHARSET));
	}else {
		$mimetype = rcmail_fix_mimetype($mimetype);
		if ($mimetype=='application/octet-stream' AND $part->filename) $mimetype = mime_by_ext($part->filename);
		header("Content-Type: $mimetype");
		header("Content-Transfer-Encoding: binary");
	}
	// deliver part content
	if ($ctype_primary=='text' && $ctype_secondary=='html' && !$_GET['download']) {
		// get part body if not available
		if (!$part->body) $part->body = $MESSAGE->get_part_content($part->mime_id);

		// $OUTPUT = new rcube_html_page();
		// $OUTPUT->write(rcmail_print_body($part, array('safe' => $MESSAGE->is_safe, 'inline_html' => false)));
		$html = rcmail_print_body($part, array('safe' => $MESSAGE->is_safe, 'inline_html' => false));
		echo $html;
	
	}else {
		@set_time_limit(1800); #(30 min) don't kill the connection if download takes more than 30 sec.

		$filename = $part->filename ? $part->filename : ($MESSAGE->subject ? $MESSAGE->subject : 'roundcube') . '.'.$ctype_secondary;
		$filename = preg_replace('[\r\n]', '', $filename);

		if ($browser->ie && $browser->ver < 7)	$filename = rawurlencode(abbreviate_string($filename, 55));
		else if ($browser->ie)					$filename = rawurlencode($filename);
		else									$filename = addcslashes($filename, '"');
		
		$disposition = $_GET['download'] ? 'attachment' : 'inline';
		header("Content-Disposition: $disposition; filename=\"$filename\"");
		
		// turn off output buffering and print part content
		if ($part->body) echo $part->body;
		else if ($part->size) $RCMAIL->imap->get_message_part($MESSAGE->uid, $part->mime_id, $part, true);
	}

	exit;
}

