
CompanyEditWin = function(cfg) {
	var d=cfg.data;
	var p=1, a=1;
	
	var adrStore = [[2,'חברה/עסק'],[4,'למשלוח דואר'],[0,'אחר']];
	
	this.addCp = function() {if (p>4) return;
		Ext.getCmp('cpSet'+p).show(); this.syncShadow(); p++;
	}

	this.addAddress = function() {if (a>2) return; Ext.getCmp('adrSet'+a).show(); this.syncShadow(); a++;}

	this.addTel=function(){
		Ext.getDom('fax_tels').options.add(new Option(Ext.getCmp('fax_tel').getValue()));
		Ext.getCmp('fax_tel').setValue('');
	}
	this.removeTel=function(){
		var sel=Ext.getDom('fax_tels');
		sel.remove(sel.selectedIndex);
	}
	this.checkTel=function(t){
		t=t.replace(/[^0-9 -]+/g, '');
		Ext.getCmp('fax_tel').setRawValue(t);
		var t1=t.replace(/[^0-9]+/g, '');
		var isValid=(/^0[0-9]{8,9}$/).test(t1);
		if (isValid) Ext.getCmp('fax_tel_add').enable();
		else Ext.getCmp('fax_tel_add').disable();
		// return ((t=='' && Ext.getDom('fax_tels') && Ext.getDom('fax_tels').length) ? true : isValid);
		return true;
	}

	this.tabs = new Ext.TabPanel({
		baseCls:'x-plain', plain:true, activeTab:0, //deferredRender:false,
		defaults: {xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'border:1px solid #8DB2E3; padding:0 5px'},
		items: [
			{title:'כללי', autoHeight:true,
            items: [
				{layout:'table', layoutConfig:{columns:6}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl', style:'margin-top:5px'}, items:[
					// {xtype:'label', text:'קבןצת לקוחות:'}
					// ,{xtype:'combo', name:'group_id', width:280, colspan:5, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:client_groups}
					{xtype:'label', text:'שם עסק:'},{name:'name', xtype:'textfield', tabIndex:1, width:280, colspan:3, allowBlank:false}
					,{xtype:'label', text:'מספר ח.פ.:', style:'padding-right:7px'},{name:'cnumber', xtype:'textfield', tabIndex:2, width:100}
					,{xtype:'label', text:'טל 1:'},{name:'tel0', xtype:'textfield', tabIndex:3, width:100}
					,{xtype:'label', text:'טל 2:', style:'padding-right:7px'},{name:'tel1', xtype:'textfield', tabIndex:4, width:100}
					,{xtype:'label', text:'פקס:', style:'padding-right:7px'},{name:'fax', xtype:'textfield', tabIndex:5, width:100}
					,{xtype:'label', text:'דוא"ל:'},{name:'email', xtype:'textfield', tabIndex:6, width:100, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
					,{xtype:'label', text:'אתר אינטרנט:', style:'padding-right:7px'},{name:'url', xtype:'textfield', tabIndex:7, width:'100%', colspan:3}
				]}
				,{xtype:'fieldset', title:'כתובת', cls:'rtl', style:'margin-top:5px; padding:2px', autoHeight:true,
					items:[
						{layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain', id:'adr0', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{html:'סוג:'},{html:'עיר:'},{html:'מיקוד:'},{html:'כתובת/ת.ד.:', colspan: 2},
							{xtype:'combo', hiddenName:'adr_type0', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city0', xtype:'textfield', width:90},
							{name:'zip0', xtype:'textfield', width:60},
							{name:'adr0', xtype:'textfield', width:215},
							{baseCls: 'x-plain', html:'&nbsp;<img src="skin/icons/add_addr.png" onClick="Ext.getCmp(\'companyEditWin\').addAddress()" ext:qtip=\'הוסף כתובת\'>'}
						]}
						,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet1', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'combo', hiddenName:'adr_type1', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city1', xtype:'textfield', width:90},
							{name:'zip1', xtype:'textfield', width:60},
							{name:'adr1', xtype:'textfield', width:215}
						]}
						,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet2', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'combo', hiddenName:'adr_type2', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city2', xtype:'textfield', width:90},
							{name:'zip2', xtype:'textfield', width:60},
							{name:'adr2', xtype:'textfield', width:215}
						]}
					]
				}
				,{xtype:'fieldset', title:'אנשי קשר', cls:'rtl', style:'margin-top:5px; padding:2px', autoHeight:true,
					items:[
						{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'cpSet0', items:[
							{xtype:'label', text:'שם/תפקיד:'},
							{xtype:'label', text:"מס' טלפון:"},
							{xtype:'label', text:'דוא"ל:', colspan:2},
							{xtype:'textfield', name:'cperson0', width:150},
							{xtype:'textfield', name:'cp_tel0', cls:'ltr', width:120},
							{xtype:'textfield', name:'cp_email0', cls:'ltr', width:196, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/},
							{baseCls: 'x-plain', html:'<img src="skin/icons/add_call.png" style="padding-right:3px;" onClick="Ext.getCmp(\'companyEditWin\').addCp()" ext:qtip="הוסף מספר טלפון">'}
						]}
						,{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'cpSet1', items:[
							{xtype:'textfield', name:'cperson1', width:150},
							{xtype:'textfield', name:'cp_tel1', cls:'ltr', width:120},
							{xtype:'textfield', name:'cp_email1', cls:'ltr', width:196, colspan:2, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						]}
						,{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'cpSet2', items:[
							{xtype:'textfield', name:'cperson2', width:150},
							{xtype:'textfield', name:'cp_tel2', cls:'ltr', width:120},
							{xtype:'textfield', name:'cp_email2', cls:'ltr', width:196, colspan:2, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						]}
						,{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'cpSet3', items:[
							{xtype:'textfield', name:'cperson3', width:150},
							{xtype:'textfield', name:'cp_tel3', cls:'ltr', width:120},
							{xtype:'textfield', name:'cp_email3', cls:'ltr', width:196, colspan:2}
						]}
						,{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'cpSet4', items:[
							{xtype:'textfield', name:'cperson4', width:150},
							{xtype:'textfield', name:'cp_tel4', cls:'ltr', width:120},
							{xtype:'textfield', name:'cp_email4', cls:'ltr', width:196, colspan:2}
						]}
					]
				}
				,{xtype:'fieldset', title:'שימוש במערכת', labelAlign:'top', cls:'rtl', autoHeight:true, style:'margin-top:3px; padding:5px',
					items:[
						{layout:'table', layoutConfig: {columns:5}, baseCls:'x-plain', items:[
							{xtype:'label', text:"שם עסק:", style:'padding-right:3px'}
							,{xtype:'textfield', name:'login', cls:'ltr',allowBlank:false}
							,{xtype:'checkbox', style:'margin: 2px 10px 2px 0;', name:'active_box', checked:(d && d.json && d.json.active==1)}
							,{xtype:'label', text:"פעיל עד:", style:'padding-right:3px'}
							,{xtype:'datefield', name:'date_end', format:'d/m/Y', cls:'ltr', vtype:'daterange'}
						]}
					]
				}
				,{xtype:'fieldset', title:'הערות', labelAlign:'top', cls:'rtl', autoHeight:true, style:'margin-top:3px; padding:0 5px',
					items:{xtype:'textarea', name:'notes', width:'100%', height:29, grow:true, growMin:18, growMax:100, growAppend:''}
				}
				,{xtype:'hidden', name:'id'}
			]}
			,{title:'קבלת פקסים', height:360,// autoHeight:true, 
				items: [
					{layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', items:[
						{xtype:'label', text:"מס' פקס:"}
						,{xtype:'textfield', id:'fax_tel', cls:'ltr', width:138, enableKeyEvents:true, validator:this.checkTel }
						,{xtype:'button', text:'הוסף',id:'fax_tel_add', disabled:true, scope:this, handler:this.addTel}
						,{bodyStyle:'padding-top:2px',colspan:2,  html:"<select size=8 id=fax_tels dir=ltr style='width:138px; height:72px; border:0px solid #b5b8c8;'></select>"}
						,{xtype:'button', text:'הסר', scope:this, handler:this.removeTel, style:'margin-top:5px;'}
					]}
				],
				listeners:{
					show:function(f){
						if (!d.id) return;
						this.getEl().mask('...טוען');
						Ext.Ajax.request({
							url: '?m=companies/companies&f=faxes_list&id='+d.id,
							success: function(r,o){
								d=Ext.decode(r.responseText);
								if (!ajRqErrors(d)) {
									if (d.fax_numbers) for (i=0; i<d.fax_numbers.length; i++) {
										Ext.getDom('fax_tels').options.add(new Option(d.fax_numbers[i].fax_number));
									}
								}
								this.getEl().unmask();
							},
							failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
							scope: this
						});

					}, 
					scope:this
				}
			}
			,{title:'מודולים', height:360,// autoHeight:true, 
				items: [
					{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl', style:'margin:14px'}, items:Modules}
				]
			}
		]
	});
	
	this.tabs.on('tabchange', function(tabPanel, tab) {this.tabs.doLayout(); this.syncShadow();},this);
	
	this.form = new Ext.form.FormPanel({
		id:'clientForm', fileUpload:true, labelWidth:75, autoHeight:true, baseCls:'x-plain',
		items:[
			this.tabs
		]
	});
	
	CompanyEditWin.superclass.constructor.call(this, {
		title:(d ? 'עידכון פרטי עסק' : 'הוספת עסק חדש'),
		id:'companyEditWin',
		layout:'fit', modal:true,
		width:640, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:0 5px;', plain:true, border:false,
		items:this.form,
		buttons:[{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('show', function(){
		Ext.getCmp('cpSet1').hide(); Ext.getCmp('cpSet2').hide(); Ext.getCmp('cpSet3').hide(); Ext.getCmp('cpSet4').hide();
		Ext.getCmp('adrSet1').hide(); Ext.getCmp('adrSet2').hide();

		if (d.id) {
			this.getEl().mask('...טוען');
			d.data=d.json;
			this.form.form.loadRecord(d);
			for (var n=1; n<5; n++) if (d.data['cperson'+n] || d.data['cp_tel'+n] || d.data['cp_email'+n]) Ext.getCmp('cpSet'+n).show();
			for (var n=1; n<3; n++) if (d.data['city'+n] || d.data['zip'+n]!=0 || d.data['adr'+n]) Ext.getCmp('adrSet'+n).show();
			for(m in d.json.modules) {
				if (typeof(d.json.modules[m])=='number') if (this.form.find('name','mod['+d.json.modules[m]+']')[0]) this.form.find('name','mod['+d.json.modules[m]+']')[0].setValue(true);
			} 
			this.getEl().unmask();
		}
		this.syncShadow();
	}, this);
	
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		if (Ext.getDom('fax_tels')) {
			var ops=Ext.getDom('fax_tels').options;
			var fax_numbers=[];
			if (ops.length) for (i=0; i<ops.length; i++) fax_numbers.push(ops[i].text);
			if (Ext.getCmp('fax_tel').getValue()) fax_numbers.push(Ext.getCmp('fax_tel').getValue());		
		}

		this.form.form.submit({
			url: "?m=companies/companies&f=edit&id="+d.id,
			params:{active:this.form.find('name','active_box')[0].getValue(),fax_numbers:fax_numbers},
			waitTitle:'אנא המתן...', waitMsg:'טוען...',
			scope:this,
			success: function(f,a){if (cfg.callBack && typeof(cfg.callBack)=='function') cfg.callBack(); this.close();},
			failure: function(r,o){this.close(); ajRqErrors(o.result);}
		});
	}
};
Ext.extend(CompanyEditWin, Ext.Window, {});

CompanyTab = function(d) {
	this.d=d;
	this.caller='client_info';
	
	this.companyEditWin = function(){var win=new CompanyEditWin({data:this.d, callBack:function(d){this.onClientChange();}.createDelegate(this)}); win.show(); win.center();};
	
	this.onClientChange = function(d) {
		this.companyInfo.tpl.overwrite(this.companyInfo.body, d.json);
	};
	
	this.contactsList= new ContactGrid({client_type:'client', client_id:this.d.id, clientCell:this.d.clientCell});
	this.contactsList.on('activate', function(tabPanel, tab) {this.contactsList.store.load();},this);

	this.filesList 	= new FilesGrid({client_id:this.d.id, client_type:'client'});//client_id,client_type,group_email
	this.filesList.on('activate', function(tabPanel, tab) {this.filesList.store.load();},this);

	this.usersGrid 	= new UsersGrid({company_id:this.d.id});
	this.usersGrid.on('activate', function(tabPanel, tab) {this.usersGrid.store.load();},this);
	
	this.companyInfo = {
		xtype:'panel', //style:'border-bottom: 2px solid white; background-color:#f3f8fc;',
		title:'פרטי העסק',
		id:'companyInfo'+this.d.data.id
		,baseCls: 'x-plain'
		,tbar:[{text:'עדכן פרטי עסק', iconCls:'edit', handler:this.companyEditWin, scope:this}]
		,tpl: new Ext.Template(
				'<table border=1 width=100% id=client_info dir=rtl><tr>',
				'<td nowrap><img src="skin/icons/1/{icon}.gif" align=absmiddle> <b>{title}:</b></td><td width=30%><b>{name}</b></td>',
				'<td nowrap><img src="skin/icons/1/zehut.gif" align=absmiddle> <b>ח.פ./ת.ז:</b></td><td width=10% nowrap>{passport}</td><td nowrap><b>רשיון נהיגה:</b> {license}</td>',
				'<td nowrap><img src="skin/icons/1/date.gif" align=absmiddle> <b>ת.לידה:</b></td><td width=15%>{birthday}</td>',
				'<td rowspan=4><img id=client_photo src="{photo}"></td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/telephone.gif" align=absmiddle> <b>טלפונים:</b></td><td width=30%>{tel_txt}</td>',
				'<td nowrap><img src="skin/icons/1/email.gif" align=absmiddle> <b>דוא"ל:</b></td><td width=30% colspan=2><a href=# onClick="sendEmail({id},\'{email}\');">{email}</a></td>',
				'<td nowrap width=15%><b>{job_title}:</b></td><td>{position}</td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/house.gif" align=absmiddle> <b>כתובת:</b></td><td width=30%>{adr}</td>',
				'<td nowrap><img src="skin/icons/1/money.png" border=0 align=absmiddle> <b>יתרת חוב:</b></td><td width=30% colspan=2><span style="color:red;" dir=ltr><b>{balance:ilMoney}</b></span></td>',
				'<td nowrap width=15%><b>{work_title}:</b></td><td>{work}</td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/note.gif" align=absmiddle><b>הערות:</b></td><td colspan=6>{notes}</td></tr></table>'
		)
		,listeners:{
			render:function(){this.tpl.overwrite(this.body, d.json)}
		}
	};
	
	CompanyTab.superclass.constructor.call(this, {
		id:'companyTab_'+this.d.id,
		iconCls:'client-business', title:d.data.name, closable:true, autoScroll:true, activeTab:4,
		items:[this.companyInfo,this.contactsList,this.filesList,this.usersGrid,this.clientAccount]
    });
	
	// this.on('render',function(){this.onClientChange(this.d);},this);
};
Ext.extend(CompanyTab, Ext.TabPanel);

CompaniesGrid = function(cfg) {
	Ext.apply(this, cfg);

	this.store = new Ext.data.JsonStore({
		url:'?m=companies/companies&f=list', totalProperty:'total', root:'data',
		fields:['id','group_id','name','date_end','cperson0','tel','fax','email','notes'],
		sortInfo:{field:'name', direction:'ASC'},
		remoteSort:true
	});

	this.companyEditWin = function(d){var win=new CompanyEditWin({data:d, callBack:function(d){this.store.reload();}.createDelegate(this)}); win.show(); win.center();}

	this.searchField = new GridSearchField({store:this.store, width:150});

	// this.store.on('load', function(){	// check for errors
			// var callback=function() { this.store.load(); };
			// ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		// }, this);
		
	this.hlStr = hlStr;
	
    this.columns = [
		// {dataIndex:'notes', id:'notes', header:'הערות', sortable:true, renderer:this.hlStr.createDelegate(this) },
		{dataIndex:'email', id:'email', header:'דוא"ל', width:70, renderer:this.hlStr.createDelegate(this) },
		{dataIndex:'fax', header:'פקס', width:80, renderer:this.hlStr.createDelegate(this) }, 
		{dataIndex:'tel', header:'טלפונים', width:140, renderer:function(v,p,r){return this.hlStr(r.json.tel0+(r.json.tel0 && r.json.tel1 ? ', ':'' )+r.json.tel1)}, scope:this }, 
		{dataIndex:'cperson0', header:'איש קשר', width:170, sortable:true},
		{dataIndex:'date_end', header:'פעיל עד', width:70, sortable:true},
		{dataIndex:'name', header:'שם', width:170, sortable:true/*, renderer:function(v){return "<span style='display:block;height:16px;padding-right:18px;background:url(skin/icons/user_business.gif) no-repeat right top;'>" +this.hlStr(v)+ "</span>";},*/, scope:this}
	];
	
	CompaniesGrid.superclass.constructor.call(this, {
		title:'רשימת עסקים',iconCls:'grid',
		enableColLock:false,loadMask:true,stripeRows:true,enableHdMenu: false,
		autoExpandColumn: 'email',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'חיפוש:', this.searchField
			,'-' , 'סטאטוס:'
			, new Ext.form.ComboBox({store:[[1,'פעילים'],[0,'מבוטלים']], id:'activeFilter', value:1, width:80, listClass:'rtl', triggerAction:'all', forceSelection:true, 
				listeners:{
					select:function(f){	this.store.reload();}, 
					change:function(f){if(f.getRawValue()==''){f.setValue(0);this.store.reload();} },
					scope:this
				}
			})
			,'-' ,{ text:'עסק חדש', iconCls:'add', handler:function(){this.companyEditWin({})}, scope:this }
			,'-' ,{ text:'עדכן עסק',id:'company_edit', iconCls:'edit', handler:function(){this.companyEditWin(this.getSelectionModel().getSelected());}, disabled:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
    });

	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			q:this.searchField.getRawValue().trim()
			,active:Ext.getCmp('activeFilter').getValue()
		}
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);
	
	// this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('company_edit').setDisabled(sm.getCount()!=1);
	}, this);
	
	// open client in new tab
	this.openCompany = function(d){
		var tab;
		if(!(tab=this.tabs.getItem('companyTab_'+d.id))) {tab=new CompanyTab(d); this.tabs.add(tab); }
		this.tabs.setActiveTab(tab);
	};

	// doubleClick
	this.on('rowdblclick', function(grid, row, e){
		var data = grid.getSelectionModel().getSelected();
		this.openCompany(data);
	});
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row);
		
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'פתח כרטיס עסק', iconCls:'user_open', scope:this, handler:function(){this.openCompany(this.srData);} }
				,{text:'עדכן פרטי העסק', iconCls:'edit', scope:this, handler:function(){this.companyEditWin(this.srData);} }
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.delClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:250, title:'למחוק משתמש?', msg:'<img src="skin/icons/1/user_del48.gif" align=left><br>האם ברצונך לבטל את המשתמש<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delClient(records); }
		});
	}
	
	this.delClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '/?m=company/employees_f&f=delete&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('משתמש בוטל','משתמש <b>'+d.data.name+'</b> בוטל בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};

	this.restoreClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'לשחזר משתמש?', msg:'האם ברצונך לשחזר את המשתמש<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.restoreClient(records); }
		});
	}
	
	this.restoreClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '/?m=company/employees_f&f=restore&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid green';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('משתמש שוחזר','משתמש <b>'+d.data.name+'</b> שוחזר בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.on('render', function(){ this.store.load() });
};
Ext.extend(CompaniesGrid, Ext.grid.GridPanel);

Ms.companies.run = function(cfg){
	var m=Ms.companies;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var companiesTabs = new Ext.TabPanel({activeTab:0, border:false, defaults:{autoScroll:true}, enableTabScroll:true});
		companiesTabs.add(new CompaniesGrid({tabs:companiesTabs}));
		win = Crm.desktop.createWindow({ width:900,	height:600,	items: companiesTabs}, m);
	}
	win.show();
};