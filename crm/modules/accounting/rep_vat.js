VatRep = function(cfg) {
	this.repWindow = function(type) {
		var rep_period=(this.form.find('name','rep_period')[1].checked ? 2:1);
		var month=this.form.find('hiddenName','month')[0].getValue();
		var year=this.form.find('name','year')[0].getValue();
		var SortState = this.store.getSortState();
		window.open('?m=accounting/reports&f=vat&'+type+'=1&rep_period='+rep_period+'&month='+month+'&year='+year+'&sort='+SortState.field+'&dir='+SortState.direction,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	};
	
	var rep_periods1=[[1,'ינואר'],[2,'פברואר'],[3,'מרץ'],[4,'אפריל'],[5,'מאי'],[6,'יוני'],[7,'יולי'],[8,'אוגוסט'],[9,'ספטמבר'],[10,'אוקטובר'],[11,'נובמבר'],[12,'דצמבר']];
	var rep_periods2=[[1,'ינואר-פברואר'],[3,'מרץ-אפריל'],[5,'מאי-יוני'],[7,'יולי-אוגוסט'],[9,'ספטמבר-אוקטובר'],[11,'נובמבר-דצמבר']];
	
	this.form = new Ext.form.FormPanel({
		labelWidth:80, height:32, region:'north', border:false, style:'padding:5px 5px 0 0;', cls:'x-panel-mc',
		items: [{layout:'table', layoutConfig: {columns:10}, baseCls:'x-plain', cls:'rtl',items:[
				{xtype:'label', text:'תקופת דיווח:', style:'padding-right:10px;'},
				{xtype:'radio', name:'rep_period', boxLabel:'חודשי', 	checked:(cfg.rep_period!=2), style:'margin:0 7px 1px 0;', labelStyle:'padding-bottom:7px;', handler:function(e){if (!e.checked) return; this.form.find('hiddenName','month')[0].store.loadData(rep_periods1); this.form.find('hiddenName','month')[0].setValue(this.form.find('hiddenName','month')[0].getValue()); this.store.load();}, scope:this},
				{xtype:'radio', name:'rep_period', boxLabel:'דו-חודשי', checked:(cfg.rep_period==2), style:'margin:0 7px 1px 0;', labelStyle:'padding-bottom:7px;', handler:function(e){if (!e.checked) return; this.form.find('hiddenName','month')[0].store.loadData(rep_periods2); this.form.find('hiddenName','month')[0].setValue(this.form.find('hiddenName','month')[0].getValue()); this.store.load();}, scope:this},
				{xtype:'combo', store:(cfg.rep_period!=2?rep_periods1:rep_periods2), hiddenName:'month', value:cfg.month+1, style:'margin-right:8px;', width:125, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false, listeners:{select: function(el){this.store.reload();},scope:this}},
				{xtype:'label', text:'שנה:', style:'display:block; padding-right:12px;'},			
				{xtype:'combo', value:cfg.year, name:'year', width:50, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:false, store:years, listeners:{select:function(){this.store.reload();},scope:this}},
				
				// {xtype:'button', text:'הצג דו"ח',  style:'margin-right:8px;', iconCls:'other', handler:function(){ this.store.reload();}, scope:this, width:90},
				{xtype:'button', id:'vat_prn_pay', style:'margin-right:8px;', iconCls:'money', text:'תשלום מע"מ ומס הכנסה', disabled:true, width:90, handler:function(){this.repWindow('payment')}, scope: this},
				{xtype:'button', id:'vat_prn_btn', style:'margin-right:8px;', iconCls:'print', text:'הדפס דו"ח', disabled:true, width:90, handler:function(){this.repWindow('print')}, scope: this},
				{xtype:'button', id:'vat_exl_btn', style:'margin-right:8px;', iconCls:'excel', text:'Excel-ייצוא ל', disabled:true, width:90, handler:function(){this.repWindow('export')}, scope: this},
				{xtype:'button', id:'vat_close_btn', style:'margin-right:25px;', iconCls:'close_period', text:'נעילת תקופת דיווח', width:90, handler:function(){Ms.accounting.closeVATPeriod();}, scope: this}
			]}
		]
	});
		
	this.store = new Ext.data.JsonStore({
		url:'?m=accounting/reports&f=vat', totalProperty:'total', root:'data',
		fields: ['id','client_id','client_name',{name:'date',type:'date', dateFormat:'Y-m-d'},'credit_with_vat','credit_no_vat','debt','num','type','ex_vat2','ex_vat1','in_vat','username'],
		sortInfo: {field:'client_name', direction:'ASC'},
		timeout:1000, remoteSort:true
	});
	
	this.hlStr = hlStr;
	this.searchField = new GridSearchField({store:this.store, width:150});
	
	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			rep_period:(this.form.find('name','rep_period')[1].checked ? 2:1),
			month: this.form.find('hiddenName','month')[0].getValue(),
			year: this.form.find('name','year')[0].getValue()
		}
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
		this.getEl().mask('...טוען');
	}, this);

	this.store.on('load', function(store, options){
		Ext.getCmp('vat_prn_btn').setDisabled(!store.getCount());
		Ext.getCmp('vat_prn_pay').setDisabled(!store.getCount());
		Ext.getCmp('vat_exl_btn').setDisabled(!store.getCount());
		var Rec = this.store.recordType;
		var p = new Rec(this.store.reader.jsonData.summary);
		this.store.insert(this.store.getCount(), p);
		var p = new Rec({client_name:'לתשלום:',ex_vat2:this.store.reader.jsonData.summary.in_vat-this.store.reader.jsonData.summary.ex_vat1-this.store.reader.jsonData.summary.ex_vat2});
		this.store.insert(this.store.getCount(), p);
		this.grid.view.addRowClass(this.store.getCount()-2,'x-grid3-row-last');
		
		var panel_html='<p class=p_tax>תשלום מקדמות מס-הכנסה עפ"י המחזור העסקי</p>';
		panel_html = panel_html + '<table class=t_tax align=center cellspacing=1 cellpadding=2 border=0">';
		panel_html = panel_html + '<tr><th>מחזור עסקי:</th><td>'+Ext.util.Format.ilMoney((this.store.reader.jsonData.summary.credit_no_vat+this.store.reader.jsonData.summary.credit_with_vat-this.store.reader.jsonData.summary.in_vat))+'</td><tr>';
		panel_html = panel_html + '<tr><th>מקדמה עפ"י % מהמחזור העסקי:</th><td>'+Ext.util.Format.ilMoney((this.store.reader.jsonData.summary.credit_no_vat+this.store.reader.jsonData.summary.credit_with_vat-this.store.reader.jsonData.summary.in_vat)*this.store.reader.jsonData.income_tax/100)+'</td><tr>';
		panel_html = panel_html + '<tr><th>ניכויים:</th><td>'+Ext.util.Format.ilMoney(this.store.reader.jsonData.clear_tax)+'</td><tr>';
		panel_html = panel_html + '<tr style="color:green;"><th>לתשלום:</th><td>'+Ext.util.Format.ilMoney(((this.store.reader.jsonData.summary.credit_no_vat+this.store.reader.jsonData.summary.credit_with_vat-this.store.reader.jsonData.summary.in_vat)*this.store.reader.jsonData.income_tax/100-this.store.reader.jsonData.clear_tax))+'</td><tr>';
		panel_html = panel_html + '</table>';
		panel_html=panel_html.replace(/'/g, '&#39;');

		this.panel.body.update(panel_html);
		
		this.getEl().unmask();
	}, this);
	
	this.colModel = new Ext.grid.ColumnModel([
		{header:'תאריך הפקה<br>&nbsp;', dataIndex:'date', width:70, sortable:true, css:'direction:ltr;', align:'center', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
		//,{header:'אסמכתא<br>&nbsp;', width:120, dataIndex:'type', renderer:docTypeRender, sortable:true}
		,{header:'אסמכתא<br>&nbsp;', width:60, dataIndex:'num', sortable:true}
		,{header:'שם לקוח<br>&nbsp;', width:100, dataIndex:'client_name', id:'client_name', renderer:clientRenderer, scope:this}
		,{header:'הכנסות<br>פטורות מע"מ', width:90, dataIndex:'credit_no_vat', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
		,{header:'מע"מ<br>&nbsp;', width:80, dataIndex:'in_vat', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
		,{header:'הכנסות<br>כולל מע"מ', width:90, dataIndex:'credit_with_vat', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
		,{header:'הוצאות<br>כולל מע"מ', width:90, dataIndex:'debt', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
		,{header:'מע"מ<br>תשומות אחרות', width:90, dataIndex:'ex_vat1', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
		,{header:'מע"מ<br>תשומות וציוד', width:95, dataIndex:'ex_vat2', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
	]);
	

	this.grid = new Ext.grid.GridPanel({
		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',autoExpandColumn:'client_name', border:false, enableHdMenu:false,
		store:this.store,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		colModel: this.colModel,
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50/*, displayInfo:true*/})
	});


	// this.grid.on('rowdblclick', function(grid){
		// var data = grid.getSelectionModel().getSelected().data;
		// var win=window.open('http://'+location.host+'?m=accounting/print_doc&id='+data.type, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	// });
	
	// this.grid.on('rowcontextmenu', function(grid, row, e){
		// var r=this.store.getAt(row);
		// this.menu = new Ext.menu.Menu({
			// items: [
				// {text:'הדפס', iconCls:'print', scope:this, handler: function(){
					// var win=window.open('http://'+location.host+'?m=accounting/print_doc&type='+r.data.type+'&id='+r.data.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					// win.focus();
					// }
				// }
				// ,{text:'פתח לקוח בחלון חדש', iconCls:'user_open', scope:this, handler: function(){openClientNewWin(r.data);} }
			// ]
		// });
		// e.stopEvent();
		// this.menu.showAt(e.getXY());
	// },this);
	
	
	this.grid.on('render', function(){this.store.load();}, this);
		
	this.panel = new Ext.Panel ({html:'test',region:'south',cls:'rtl',height:90});
	
	VatRep.superclass.constructor.call(this, {border:false, layout:'border', region:'center', title:'דוח מע"מ', items:[this.form,this.grid,this.panel]})
};
Ext.extend(VatRep, Ext.Panel);