<?

//—————————————————————————————————————————————————————————————————————————————————————————————
#get all cats from $table to array, if $disabled true include disabled too
function load_cats($table, $not_active=0) {
 global $ln;
	$sql="SELECT id, parent_id, page_name, type, language, title, img_ext, img_pos, LEFT(short_text,1) AS short_text, LEFT(full_text,1) AS full_text, counter, sort, active, DATE_FORMAT(date_added,'%d/%m/%y') AS date_added, DATE_FORMAT(date_updated,'%d/%m/%y') AS date_updated FROM $table WHERE (language='' OR language='$ln')".($not_active?'':" AND active>0")." ORDER BY sort, title";
	$cats_ar=sql2array($sql, 'id');
	if (!is_array($cats_ar)) return;
 return $cats_ar;
}

//—————————————————————————————————————————————————————————————————————————————————————————————
function load_page($page) {
 global $CFG;
	if (!$page=trim($page)) return;
	$sql="SELECT full_text FROM $CFG[prefix]pages WHERE ".(is_numeric($page)?"id='$page'":"page_name='".addslashes($page)."'").($SVARS['is_admin']?'':' AND active>0');
 return sql2array($sql,'','full_text',1);
}

//—————————————————————————————————————————————————————————————————————————————————————————————
function page_has_sub($cats_ar, $id) {
	if (!$cats_ar OR !$id) return;
	if ($cats_ar[$id]['sub']) return true;

	foreach ($cats_ar as $key=>$row) {
		if ($id==$row['parent_id']) {
			$cats_ar[$id]['sub']=true;
			return true;
		}		
	}
}

//—————————————————————————————————————————————————————————————————————————————————————————————
function get_page_id($page, $pages_cats='') {
 if (!$pages_cats) global $pages_cats;
	if (!$pages_cats OR !$page) return;

	if (is_numeric($page) AND $pages_cats[$page]) return $page;
	else {	# Search for page id
		foreach ($pages_cats as $id=>$row) if ($page==$row['page_name']) return $id;
	}
}

//—————————————————————————————————————————————————————————————————————————————————————————————
# returns array with pointers to all parent categories
function page_get_pointers_ar($cats_ar, $id, $pointers=false) {
	$pointers=page_get_pointers($cats_ar, $id);
	if (!$pointers) return;
	$pointers_ar=explode(" ", $pointers);
 return $pointers_ar;
}

//—————————————————————————————————————————————————————————————————————————————————————————————
# returns string with pointers to all parent categories
function page_get_pointers($cats_ar, $id, $pointers='') {
	if (!$cats_ar OR !$id) return;
	if (!$cats_ar[$id]['parent_id']) return $pointers;
	if ($cats_ar[$id]['parent_id']) {
		$pointers=( $pointers ? "{$cats_ar[$id]['parent_id']} $pointers" : $cats_ar[$id]['parent_id'] );
		if ($id==$cats_ar[$id]['parent_id']) return;	# points to itself!
		$pointers=page_get_pointers($cats_ar, $cats_ar[$id]['parent_id'], $pointers);
		return $pointers;
	}
}

//—————————————————————————————————————————————————————————————————————————————————————————————
# get array of sub categories
function get_subcats_list($cats_ar, $id, $levels=0, $subcats='') {
	if (!$cats_ar OR !$id) return;

	if (page_has_sub($cats_ar, $id)) {
		foreach ($cats_ar as $cat=>$cat_ar) {
			if ($cats_ar[$cat]['parent_id']==$id) {
				$subcats[]=$cat;
				if ($levels AND page_has_sub($cats_ar, $cat) AND $id!=$cats_ar[$id]['parent_id']) get_subcats_list($cats_ar, $cat, $levels-1, &$subcats);
			}
		}
		return $subcats;
	}
}


//—————————————————————————————————————————————————————————————————————————————————————————————
# str cat_full_name(int id, array cats_ar, link{1/str})
# get - $id category name, $cats_ar - array of all categories, $links - if true-shows links
# return string with full cat name (Main\name1\name2)
function cat_full_name($id, $cats_ar, $link='') {
 global $LNG, $ln;
	if (!$cats_ar[$id]) return;
	$split=' -&gt; ';//"<IMG src='skins/default/bs_cat.gif' width=12 height=6 align=middle border=0>";
	$pointers_ar=page_get_pointers_ar($cats_ar, $id);
	
	if (is_array($pointers_ar)) {
		//$pointers_ar=explode(" ", $cats_ar[$id]['pointers']);
		foreach ($pointers_ar as $cat) {
			if ($link) $cat_name = "<A HREF='{$link}$cat'>{$cats_ar[$cat][title]}</A>";
			else $cat_name = $cats_ar[$cat]['title'];
			
			if ($ln=='he') $cat_name="<span dir=rtl>$cat_name</span>";
			$cat_full_name.=($cat_full_name?$split:''). $cat_name;
		}
	}

	# add this category
	$cat_name = $cats_ar[$id]['title'];
	if ($ln=='he') $cat_name="<span dir=rtl>$cat_name</span>";
	$cat_full_name.=($cat_full_name?$split:''). $cat_name;
	
 return "<a dir=rtl href='/'>$LNG[Home]</a>{$split}$cat_full_name";
}
//————————————————————————————————————————————————————————————————————————————————————
function rewrite_links($str) {
 global $CFG;
	if (!$CFG['rewrite']) return $str;
	$find[]='|/?\?m=user(?:&a=([\w]+))?(&?)$|e';		$repl[]="'/user'.('$1'?'/$1':'').('$2'?'?':'')";
	//$find[]='|/?\?m=pages&id=([a-z0-9_-]+)|';		$repl[]='/pages/$1';
	$find[]='|/?\?m=pages&id=([^ %#&=?\'"]+)|';		$repl[]='/$1.htm';
	$find[]='|/?\?m=contact(&?)|e';					$repl[]="'/contact'.('$1'?'?':'')";
	$find[]='|/?\?m=search(&?)|e';					$repl[]="'/search'.('$1'?'?':'')";
	$find[]='|/?\?m=cart(&?)|e';					$repl[]="'/cart'.('$1'?'?':'')";
	$find[]='|/?\?m=checkout(&?)|e';				$repl[]="'/checkout'.('$1'?'?':'')";
	//$find[]='|/?\?m=friends(&?)|e';					$repl[]="'/friends'.('$1'?'?':'')";
	$find[]='|/?\?m=catalog&cat_id=(\d+)(&id=(\d+))?|';		$repl[]='/catalog/$1/$3';
	//$find[]='|/?\?m=catalog(?:&cat_id=([0-9-]+))?(?:&id=(\d+))?(&?)|';		$repl[]="'/catalog'.('$1'?'/$1':'').('$2'?'/$2':'').('$3'?'?':'')";
	$find[]='|/?\?m=auctions(?:&cat_id=([0-9-]+))?(?:&id=(\d+))?(?:&a=([a-z_-]+))?|e';	$repl[]="'/auctions'.('$1'?'/$1':'').('$2'?'/$2':'').('$3'?'/$3':'')";
	$str=preg_replace($find, $repl, $str);
 return $str;
}
?>