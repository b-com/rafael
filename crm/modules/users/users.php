<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();
//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;

	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end 
		FROM users 
		WHERE company_id = $SVARS[cid]
		ORDER BY fullName",'id');
	
	// echo "/*<div dir=ltr align=left><pre>".print_r($user_fax_in_ar,1)."</pre></div>*/";
	
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;
	}
	
	#Load contacts patterns
	$patterns_ar=sql2array("SELECT id, type, name FROM patterns ORDER BY name",'id');
	if ($patterns_ar) foreach($patterns_ar as $id=>$pattern) {
		if ($pattern['type']=='sms') $smsPatterns.=($smsPatterns ? ',':'')."[$id,'".jsEscape($pattern['name'])."']";
	}
	
	$fax_numbers=sql2array("SELECT fax_number FROM fax_numbers WHERE company_id=$SVARS[cid]",0,'fax_number',0,'rafael');
	if ($fax_numbers) foreach($fax_numbers as $k=>$number) $fax_numbers[$k]=array('xtype'=>'checkbox', 'checked'=>false, 'fieldLabel'=>$number, 'name'=>"fax_in[$number]");

	$client_groups=sql2array("SELECT * FROM client_groups",0,'name');
	if ($client_groups) foreach($client_groups as $k=>$client_group) $client_groups[$k]=array('xtype'=>'checkbox', 'checked'=>false, 'fieldLabel'=>$client_group, 'name'=>"groups[$k]");
	?>
	
	var smsActive=<?=($SVARS['user']['sms_active'] ? 'true' : 'false')?>;
	var faxActive=<?=($SVARS['user']['fax_active'] ? 'true' : 'false')?>;
	var user={user_id:<?=$SVARS['user']['id']?>}; 
	var users=[<?=$users;?>];
	var agents=[<?=$agents;?>];
	var users_obj=<?=array2json($users_ar);?>;
	var fax_numbers=<?=array2json($fax_numbers);?>;
	var client_groups=<?=array2json($client_groups);?>;
	
	var	sms_originator='';
	var smsPatterns=[<?=$smsPatterns;?>];

	Ext.ux.Loader.load([
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			'skin/extjs/ux/superboxselect/superboxselect.css?<?=filemtime('skin/extjs/ux/superboxselect/superboxselect.css');?>',
			'skin/extjs/ux/superboxselect/SuperBoxSelect.js?<?=filemtime('skin/extjs/ux/superboxselect/SuperBoxSelect.js');?>',
			'modules/common/contacts.js?<?=filemtime('modules/common/contacts.js');?>',
			'modules/common/files.js?<?=filemtime('modules/common/files.js');?>',
			'modules/users/users.js?<?=filemtime('modules/users/users.js');?>'
		], function(){Ms.users.run();}
	);
	
	<?
}

//————————————————————————————————————————————————————————————————————————————————————
function f_list() {
 global $CFG, $SVARS, $m, $a, $group_id, $deleted;
	$rowsOnPage=30;
	
	$telStore=array(1=>'בית',2=>'עבודה',3=>'נייד',4=>'פקס בבית',5=>'פקס בעבודה',0=>'אחר');
	$emailStore=array(1=>'בית',2=>'עבודה',0=>'אחר');
	$adrStore = array(1=>'בית',2=>'חברה/עסק',3=>'עבודה',4=>'למשלוח דואר',0=>'אחר');
	
	if ((int)$_REQUEST['company_id'] AND $SVARS['cid']==1) $company_id=(int)$_REQUEST['company_id']; else $company_id=$SVARS['cid'];

	// $permissions = sql2array("SELECT * FROM crm_permissions WHERE user_id=$SVARS[user_id]",'group_id');
	// if ($permissions) foreach ($permissions as $permission) {if ($permission['pview']==1) $crm_permissions['view'].=($crm_permissions['view']?',':'').$permission['group_id'];}
	
	// if ((int)$_GET['groupID'] AND $permissions[(int)$_GET['groupID']]['pview']==1) $groups=(int)$_GET['groupID']; #Selected group
	// else $groups=$group_id.($crm_permissions['view']?',':'').$crm_permissions['view']; # All allowed groups
	$groups=$group_id;

	// $where="group_id IN($groups)";
	
	// if ((int)$_POST['team_id']) $where.=" AND employees.team_id=".(int)$_POST['team_id'];
	// if ((int)$_POST['agent_id']) $where.=" AND employees.agent_id=".(int)$_POST['agent_id'];

	if ($_POST['q']) $_POST['q'] = trim( sql_escape(iconv("UTF-8", "windows-1255", $_POST['q'])) );
	
	if ($_POST['q']) {
		$make_where_search=make_where_search('fname,lname,passport,city0,adr0,city1,adr1,city2,adr2,tel0,tel1,tel2,tel3,tel4,email,email1,email2', $_POST['q']);
		if ($make_where_search) $where.=" AND ($make_where_search)";
	}
	
	$where=" active=".(int)$_POST['deleted'];
	
	if ($_POST['sort']=='tz')  $order_by='passport';
	elseif ($_POST['sort']=='adr') $order_by='city';
	elseif ($_POST['sort']=='tel') $order_by='work_phone';
	elseif ($_POST['sort']=='fax') $order_by='fax';
	else $order_by='full_name';
	
	#get data to array
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$sql="SELECT SQL_CALC_FOUND_ROWS *, TRIM(CONCAT(users.lname,' ',users.fname)) AS full_name, DATE_FORMAT(users.birthday,'%d/%m/%Y') AS birthday "
		."FROM users".($where ? " WHERE $where ":'')
		."ORDER BY $order_by".($_POST['dir']=='DESC'?' DESC':'')
		." LIMIT $_POST[start],$_POST[limit]";
	
	echo "/*crm_$company_id $sql*/\r\n";
	
	$rows=sql2array($sql,0,0,0, "crm_".$company_id);
	
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if ($rows) {	# load number of contacts and policies
		foreach($rows as $row) {
			$clients[]=$row['id'];
			if ($row['agent_id']) $agents_ids[$row['agent_id']]=true;
			if ($row['team_id']) $teams_ids[$row['team_id']]=true;
		}
	}
	
	if ($rows) foreach ($rows as $k=>$r) {
		$C[$k]['id']		= $r['id'];
		$C[$k]['branch_id']	= $r['branch_id'];
		$C[$k]['name']		= ($r['c_type']==2 ? $r['fname'] : $r['full_name']);
		$C[$k]['passport']	= $r['passport'];
		$C[$k]['birthday']	= $r['birthday']!='00/00/0000' ? $r['birthday'] : '';
		$C[$k]['email']		= $r['email'];
		$C[$k]['notes']		= $r['notes'];
		$C[$k]['fax']		= $r['fax'];
		$C[$k]['sex']		= $r['sex'];
		
		# phones
		$C[$k]['tel']=$C[$k]['fax']=$C[$k]['tel_txt']='';
		for ($i=0; $i<8; $i++) {
  				if ($r["tel$i"]) {
					if ($r["tel_type$i"]==4 OR $r["tel_type$i"]==5) $C[$k]['fax'].=($C[$k]['fax']?', ':'').$r["tel$i"];
					else $C[$k]['tel'].=($C[$k]['tel']?', ':'').$r["tel$i"];
				  	$C[$k]['tel_txt'].=($C[$k]['tel_txt']?', ':'').$telStore[$r["tel_type$i"]].': '.$r["tel$i"];
				}
  		}
		  		
		# address
		unset($C[$k]['adr'],$adr);
		for ($i=0; $i<3; $i++) {
  			if ($r["city$i"] OR $r["zip$i"] OR trim($r["adr$i"])) {
  				$adr=$r["city$i"];
  				if ($r["adr$i"]) $adr.=($adr?', ':'').$r["adr$i"];
  				if ($r["zip$i"]) $adr.=($adr?', ':'').$r["zip$i"];
			  $C[$k]['adr'].= ($C[$k]['adr']?'<br>':'').($adr ? $adrStore[$r["adr_type$i"]].': ':'*').$adr;
			}
  		}
	}
	
	echo '{total:"'.$rowsCount.'", data:'.array2json($C).'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_edit() {
 global $SVARS, $CFG, $client_id, $group_id;
	$id=(int)$_REQUEST['id'];
	// foreach ($_POST as $key=>$val) $_POST[$key] = iconv('UTF-8', 'windows-1255', $val);	# if not upload
	
	$sql="users SET passport=".quote(sprintf("%09s", $_POST['passport']))
		.", group_id=".(int)$_POST['group_id']
		.", username=".quote($_POST['username'])
		.", active=".(($_POST['active'] AND $_POST['username']) ? 1 :0)
		.($_POST['password'] ? ", password=".quote(md5($_POST['password'])):'')
		.", fname=".quote($_POST['fname'])
		.", lname=".quote($_POST['lname'])
		.", birthday='".str2time($_POST['birthday'],'Y-m-d')."'"
		.", sex=".(int)$_POST['sex']
		.", religion=".(int)$_POST['religion']
		.", fam_status=".(int)$_POST['fam_status']
		.", notes=".quote($_POST['notes']);
	
	$n=0;
	for ($i=0; $i<5; $i++) if ($_POST["tel$i"]) {$sql.=", tel_type$n=".(int)$_POST["tel_type$i"].", tel$n=".quote($_POST["tel$i"]); $n++;}
	for ($i=$n; $i<5; $i++) $sql.=", tel_type$i='', tel$i=''";
	
	$sql.=", email=".quote($_POST['email']);
	$n=1;
	for ($i=1; $i<3; $i++) if ($_POST["email$i"]) {$sql.=", email_type$n=".(int)$_POST["email_type$i"].", email$n=".quote($_POST["email$i"]); $n++;}
	for ($i=$n; $i<3; $i++) $sql.=", email_type$i='', email$i=''";
	
	$n=0;
	for ($i=0; $i<3; $i++) if ($_POST["city$i"] OR $_POST["zip$i"] OR $_POST["adr$i"]) {$sql.=", adr_type$n=".(int)$_POST["adr_type$i"].", city$n=".quote($_POST["city$i"]).", zip$n=".quote($_POST["zip$i"]).", adr$n=".quote($_POST["adr$i"]); $n++;}
	for ($i=$n; $i<3; $i++) $sql.=", adr_type$i='', city$i='', zip$i='', adr$i=''";

	# clear departnemts if branche not selected
	if (isset($_POST["branch_id"]) AND (int)$_POST["branch_id"]==0) $_POST["dep0"]=$_POST["dep1"]=$_POST["dep2"]=0;
	
	echo "/*<div dir=ltr align=left><pre>".print_r($_POST['fax_in'],1)."</pre></div>*/";
	
	$sql.=(isset($_POST["dep0"])	? ", dep0=".(int)$_POST['dep0']:'')
		.(isset($_POST["dep1"])		? ", dep1=".(int)$_POST['dep1']:'')
		.(isset($_POST["dep2"])		? ", dep2=".(int)$_POST['dep2']:'')
		.(isset($_POST["branch_id"])? ", branch_id=".(int)$_POST['branch_id']:'')
		.(isset($_POST["position"]) ? ", position=".quote($_POST['position']):'')
		.(isset($_POST["salary"])	? ", salary=".(int)$_POST['salary']:'')
		.(isset($_POST["url"])		? ", url=".quote($_POST['url']):'')
		.(isset($_POST["icq"])		? ", icq=".quote($_POST['icq']):'')
		.(isset($_POST["skype"])	? ", skype=".quote($_POST['skype']):'')
		.(isset($_POST["msn"])		? ", msn=".quote($_POST['msn']):'')
		.(isset($_POST["facebook"])	? ", facebook=".quote($_POST['facebook']):'')
		.(isset($_POST["sms_active"])? ", sms_active=1":", sms_active=0")
		.(isset($_POST["fax_active"])? ", fax_active=1":", fax_active=0")
		.(isset($_POST['fax_in'])	? ", fax_in=".quote(implode(',',array_keys($_POST['fax_in']))):", fax_in=''")
		.(isset($_POST['groups'])	? ", groups=".quote(implode(',',array_keys($_POST['groups']))):", groups=''")
		;
	
	$sql = ($id ? "UPDATE $sql WHERE id=$id" : "INSERT INTO $sql");
	// echo "/*\n$sql\n*/";
	
	if (runsql($sql)) {
		if (!$id) $id = mysql_insert_id();
		mem_unset("SV$SVARS[cid]/$id");
		if (is_array($_FILES) AND !$_FILES['photo']['error'] AND is_file($_FILES['photo']['tmp_name'])) upload_photo($id);
		
		$name = jsEscape(trim($_POST['lname'].' '.$_POST['fname']));
		echo "{id:$id, name:'$name', success:true, group_id:'$group_id'}"; 
	}else echo '{success:false}';		
	
}

//————————————————————————————————————————————————————————————————————————————————————
function upload_photo($id) {
 global $CFG, $group_id;
	if (!$id) { error("upload_photo() - no id"); return; }
	if (!is_array($_FILES) OR $_FILES['photo']['error']) return;
	
	include_once ("$CFG[ROOT]/inc/forms.php");
	
	list($width, $height, $ext) = getimagesize($_FILES['photo']['tmp_name']);
	$img_types=array('', 'gif', 'jpg', 'png');
	$ext=$img_types[$ext];
	if ($ext) {
		if (!is_dir("files/employee_photos/$group_id")) mkdir("files/employee_photos/$group_id", 0777);
		$dst=realpath("files/employee_photos/$group_id")."/$id.$ext";
		foreach (glob("files/employee_photos/$group_id/$id.*") as $file) @unlink($file); # remove old photos
		if ($width>70 OR $height>70) {	# resize
			if (!resize_img($_FILES['photo']['tmp_name'], $dst, 70, 70) ) return 0;
		}else if (!move_uploaded_file($_FILES['photo']['tmp_name'], $dst)) return 0;
	}else return 1;
}


//————————————————————————————————————————————————————————————————————————————————————
function f_delete() {
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	if (runsql("UPDATE employees SET deleted=1 WHERE id IN($_GET[ids])")) echo '{success:true}';
	else echo '{success:false, msg:"delete error"}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_restore() {
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	if (runsql("UPDATE employees SET deleted=0 WHERE id IN($_GET[ids])")) echo '{success:true}';
	else echo '{success:false, msg:"restore error"}';
}


//————————————————————————————————————————————————————————————————————————————————————
function f_load_data() {
 global $SVARS, $group_id;
	// echo "aaaaaaaaa";
	$id=(int)$_REQUEST['id'];
	if (!$id) {echo '{success:false}'; return;}
	
	$groups=$group_id;
	
	$sql="SELECT * FROM users WHERE id=$id";
	$data=sql2array($sql,'','',1);
	if ($data['birthday']=='0000-00-00') $data['birthday']='';
	if ($data['fam_status']==0)	$data['fam_status']=1;
	if ($data['salary']==0)		$data['salary']='';
	if ($data['branch_id']==0)	$data['branch_id']='';
	if ($data['dep0']==0)		$data['dep0']='';
	if ($data['dep1']==0)		$data['dep1']='';
	if ($data['dep2']==0)		$data['dep2']='';
	
	if ($data['fax_in']) foreach(explode(',',$data['fax_in']) as $f) $data["fax_in[$f]"] = true;
	if ($data['groups']) foreach(explode(',',$data['groups']) as $f) $data["groups[$f]"] = true;
	
	# Set user_logo
	$photo = glob("files/employee_photos/$group_id/$id.*");
	$photo = $photo[0];
	if ($photo) $data['photo']="$photo?".filemtime($photo);
	unset($data['password']);
	echo '{success:true, total:1 ,data:'.array2json($data).'}';
}

//————————————————————————————————————————————————————————————————————
function error_inc() {  # increase error counter (in xCache memory)
	$ipKey="ipEr$_SERVER[REMOTE_ADDR]";
	if (!mem_isset($ipKey)) mem_set($ipKey, 1, 600);
	else mem_inc($ipKey, 1, 600);
	
	$username = trim($_REQUEST['username']);
	if ($username) {
		$usrKey="usrEr$username";
		if (!mem_isset($usrKey)) mem_set($usrKey, 1, 600);
		else mem_inc($usrKey, 1, 600);
	}
}

//————————————————————————————————————————————————————————————————————
function f_login() {
	global $SVARS;

	f_user_logout();
	$msg=user_login();
	
	if ($msg=='ok') {
		 echo "{success:true}";
	}
	else echo "{success:false, msg:'".jsEscape($msg)."'}";
}
//————————————————————————————————————————————————————————————————————
function user_login() {
 global $CFG, $SVARS, $LNG;

	$company  = trim($_REQUEST['company']);
	$username = trim($_REQUEST['username']);
	$password = trim($_REQUEST['password']);
	

	if (!$company OR !$username OR !$password) return 'נא למלא את כל השדות.';
	
	# check ip errors
	// if (mem_get("ipEr$_SERVER[REMOTE_ADDR]")>15) return 'IP חסום זמנית';
	if (mem_get("usrEr$company/$username")>5) return 'משתמש חסום זמנית';
	
	$company=sql2array("SELECT * FROM companies WHERE name=".quote($company), '', '', 1, 'Rafael');

	if (!$company) { error_inc(); return 'שם משתמש או סיסמא לא נכונים'; }
	
	if ($company['active']<1) return "עסק לא פעיל";
	
	if ($company['date_end'] AND $company['date_end']!='0000-00-00' AND $company['date_end']<date('Y-m-d')) return "תקופת השימוש בתוכנה הסתיימה,<br>להמשך השימוש בתוכנה, נא ליצור קשר עם המשרד.";
	
	$SVARS['cid']=$company['id'];
		
	$SVARS['company'] = array(
		'date_added'=>$company['date_added'],
		'date_end'=>$company['date_end']);
		
	
	$user=sql2array("SELECT * FROM users WHERE username=".quote($username), '', '', 1, 'Rafael');

	if (!$user) { error_inc(); return 'שם משתמש או סיסמא לא נכונים'; }
	
	if ($user['active']<1) return "משתמש לא פעיל";
	
	if ($user['date_end'] AND $user['date_end']!='0000-00-00' AND $user['date_end']<date('Y-m-d')) return "תקופת השימוש בתוכנה הסתיימה,<br>להמשך השימוש בתוכנה, נא ליצור קשר עם המשרד.";
	
	if ($user['password'] != md5($password)) { error_inc(); return 'שם משתמש או סיסמא לא נכונים'; }
	// print_ar ($user); exit;
	# Login Success
	
	$SES = base_convert($company['id'],10,36) .'|'. base_convert($user['id'],10,36) .'|'. base_convert(ip2long($_SERVER['REMOTE_ADDR']),10,36) .'|'. base_convert(time(),10,36) . base_convert(mt_rand(100000,999999),10,36);
	setcookie("SES", $SES, 0, '/');
	$_COOKIE['SES']=$SES;
	
	runsql("UPDATE users SET ip='$_SERVER[REMOTE_ADDR]', cookie='$SES', last_enter='".date('Y-m-d H:i:s')."' WHERE id=$user[id]");
	
	# log
	runsql("INSERT LOW_PRIORITY INTO user_login_log SET user_id='$user[id]', time='".date('Y-m-d- H:i:s')."', ip='$_SERVER[REMOTE_ADDR]', proxy=".quote(getProxy()));
	# delete old user_login_log
	if (rand(1,30)==1) runsql("DELETE LOW_PRIORITY FROM user_login_log WHERE time<'".date('Y-m-d H:i:s', strtotime('-90 days'))."'");

	// mem_unset("SV$user[id]");
 return 'ok';
}


//————————————————————————————————————————————————————————————————————
function f_user_logout() {
 global $SVARS, $f;
	if (!$_COOKIE['SES']) return;
	$ses=explode('|', $_COOKIE['SES']);
	$company_id	= (int)base_convert($ses[0],36,10);	
	$user_id	= (int)base_convert($ses[1],36,10);
	if (!$company_id OR !$user_id) return;
	
	mem_unset("SV$company_id/$user_id");
	if ($f!='login') {runsql("UPDATE users SET cookie='' WHERE id=$user_id", "crm_$company_id"); header("Location: /");}
	$SVARS=null;
}

//————————————————————————————————————————————————————————————————————————————————————
function getProxy() {	// return proxy address string
	if ($_SERVER['HTTP_CLIENT_IP'])			$ips['HTTP_CLIENT_IP']		= $_SERVER['HTTP_CLIENT_IP'];
	if ($_SERVER['HTTP_FORWARDED'])			$ips['HTTP_FORWARDED']		= $_SERVER['HTTP_FORWARDED'];
	if ($_SERVER['HTTP_FORWARDED_FOR'])		$ips['HTTP_FORWARDED_FOR']	= $_SERVER['HTTP_FORWARDED_FOR'];
	if ($_SERVER['HTTP_X_FORWARDED'])		$ips['HTTP_X_FORWARDED']	= $_SERVER['HTTP_X_FORWARDED'];
	if ($_SERVER['HTTP_X_FORWARDED_FOR'])	$ips['HTTP_X_FORWARDED_FOR']= $_SERVER['HTTP_X_FORWARDED_FOR'];
	if ($_SERVER['HTTP_SP_HOST'])			$ips['HTTP_SP_HOST']		= $_SERVER['HTTP_SP_HOST'];
	if ($_SERVER['HTTP_VIA'])				$ips['HTTP_VIA']			= $_SERVER['HTTP_VIA'];
	if ($_SERVER['HTTP_X_REAL_IP'])			$ips['HTTP_X_REAL_IP']		= $_SERVER['HTTP_X_REAL_IP'];
	if ($_SERVER['CLIENT_IP'])				$ips['CLIENT_IP']			= $_SERVER['CLIENT_IP'];
	if ($_SERVER['X_CLIENT_IP'])			$ips['X_CLIENT_IP']			= $_SERVER['X_CLIENT_IP'];
	if ($_SERVER['HTTP_X_COMING_FROM'])		$ips['HTTP_X_COMING_FROM']	= $_SERVER['HTTP_X_COMING_FROM'];
	if ($_SERVER['HTTP_COMING_FROM'])		$ips['HTTP_COMING_FROM']	= $_SERVER['HTTP_COMING_FROM'];
	if ($_SERVER['HTTP_FROM'])				$ips['HTTP_FROM']			= $_SERVER['HTTP_FROM'];
	if ($_SERVER['HTTP_X_LOCKING'])			$ips['HTTP_X_LOCKING']		= $_SERVER['HTTP_X_LOCKING'];
	if ($_SERVER['HTTP_CACHE_INFO'])		$ips['HTTP_CACHE_INFO']		= $_SERVER['HTTP_CACHE_INFO'];
	if ($_SERVER['HTTP_CACHE_CONTROL'])		$ips['HTTP_CACHE_CONTROL']	= $_SERVER['HTTP_CACHE_CONTROL'];
	if ($_SERVER['HTTP_X_LOOKING'])			$ips['HTTP_X_LOOKING']		= $_SERVER['HTTP_X_LOOKING'];
	if ($_SERVER['HTTP_XONNECTION'])		$ips['HTTP_XONNECTION']		= $_SERVER['HTTP_XONNECTION'];
	if ($_SERVER['HTTP_PROXY_CONNECTION'])	$ips['HTTP_PROXY_CONNECTION']=$_SERVER['HTTP_PROXY_CONNECTION'];
	if ($_SERVER['HTTP_XROXY_CONNECTION'])	$ips['HTTP_XROXY_CONNECTION']=$_SERVER['HTTP_XROXY_CONNECTION'];
	if ($_SERVER['HTTP_PROXY_AUTHORIZATION'])$ips['HTTP_PROXY_AUTHORIZATION'] = $_SERVER['HTTP_PROXY_AUTHORIZATION'];		
  if (is_array($ips)) return implode(", ", array_unique($ips));
}

?>