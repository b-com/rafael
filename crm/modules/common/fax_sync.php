<?
$NO_SKIN=1;

$GLOBALS['fax_last_date'] = 'fax_last_date';
if (!file_exists($GLOBALS['fax_last_date']) OR time()-filemtime($GLOBALS['fax_last_date'])>180 ) {
	sync_faxes();
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function sync_faxes() {
 global $CFG, $m, $a, $fax_last_date;
	
	if (xcache_get("crm/sync_faxes")) { sleep(2); return; }
	
	xcache_set("crm/sync_faxes", 1, 300);
	
	$start_date = file_get_contents($fax_last_date);
	
	$fp=fopen($fax_last_date, 'wb');
	if (!$fp) return;
	if (flock($fp, LOCK_EX)) {	# do an exclusive lock
		$last_date = sync_fax_db($start_date);
		# save last_date
		if ($last_date) fwrite($fp, $last_date);
		else fwrite($fp, $start_date);
		
		flock($fp, LOCK_UN);	# release the lock
	}else return;	# file already open
	fclose($fp);
	
	xcache_unset("crm/sync_faxes");
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function sync_fax_db($start_date) {
 global $CFG, $m, $a;
	set_time_limit(30);
	ini_set('memory_limit', '100M');
	ignore_user_abort(true);
	
	//$start_id = (int)$start_id + 1;
	//$url="http://bafi.loc/faxserver/html/faxes.php?a=fax_list&start_id=$start_id";

	$url="https://bafi.openvoice.co.il/faxes.php?a=fax_list&start_date=".urlencode($start_date);
	
	$response_ar = unserialize( curl_get($url) );
	//print_ar($url,'$url');
	//print_ar($response_ar,'$response_ar');
	 //return;
	if (!$response_ar) return;
	// echo "<div dir=ltr align=left><pre>".print_r($response_ar,1)."</pre></div>"; 
	
	$fax_numbers = sql2array("SELECT fax_number, company_id FROM fax_numbers",'fax_number','company_id',0,'rafael');
	
	
	$fields = array('destiny_fax', 'company_fax', 'company_name', 'type', 'date', 'faxpath', 'errormsg');
  	
	foreach($response_ar as $row) {
		$db=$fax_numbers[$row['destiny_fax']];
		// echo $row['destiny_fax'].'->'.$fax_numbers[$row['destiny_fax']]."<br>";
		if ($db) {
			$values[$db][] = array($row['destiny_fax'], $row['company_fax'], $row['company_name'], $row['type'], $row['date'], $row['faxpath'], ($row['errormsg']?1:0));
			$last_date=$row['date'];
		}
	}
	
	// echo "<div dir=ltr align=left><pre>".print_r($values,1)."</pre></div>"; //return;
	
	if ($values) foreach($values as $db=>$rows) sql_multi_insert('fax_in', $db, $fields, $rows);
	
 return $last_date;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function curl_get($url) {
	if (!$url) return;
 	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 
	$response = curl_exec($ch); 
	$error = curl_error($ch); 
	if(!empty($error)) { /*echo $error;*/ return; }

	curl_close($ch);
 return $response;
}



//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function sql_multi_insert($table, $db, &$fields, &$rows, $max_query_length=100000) {
	$n=0;
	foreach ($rows as $row) {
		foreach ($row as $k=>$r) $row[$k]="'".sql_escape($r)."'";
		$inserts[$n].= ($inserts[$n]?",\n":'') . '('.implode(',',$row).')';
		if (strlen($inserts[$n]) > $max_query_length ) $n++;
	}
	foreach ($inserts as $insert) {
		$sql="INSERT IGNORE INTO $table (".implode(',',$fields).") VALUES\n".$insert;
		//echo "<div dir=ltr align=left><pre>".print_r($sql,1)."</pre></div>";
		runsql($sql);
	}
}

/*
//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function load_fax_file($fax) {
 global $group_id;
	$url="https://bafi.openvoice.co.il/faxes/$fax[faxpath]/fax.pdf";
	//$filename="Fax #$fax[id] From $fax[company_fax] ($fax[company_name]) ".str2time($fax['date'],'d.m.y H;i').'.pdf';

	set_time_limit(180);
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 120);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	$content = curl_exec($ch);
	
	curl_close($ch);

 return $content;
	# save file
	// if (!is_dir("../crm-files/$group_id")) mkdir("../crm-files/$group_id", 0777);
	// if (!is_dir("../crm-files/$group_id/fax_in")) mkdir("../crm-files/$group_id/fax_in", 0777);
	// if (write2file("../crm-files/$group_id/fax_in/$fax[id]", $content)) {
		
	// }
}
*/


// http://bafi.openvoice.co.il/faxes/recvd/2009/7/21/Unknown/000000011/fax.pdf

/*
function readfile_chunked($filename, $retbytes=true) {
	set_time_limit(0);
	$chunksize = 1*(1024*1024); // how many bytes per chunk
	$buffer = '';
	$cnt =0;
	// $handle = fopen($filename, 'rb');
	$handle = fopen($filename, 'rb');
	if ($handle === false) return false;

	while (!feof($handle)) {
		$buffer = fread($handle, $chunksize);
		echo $buffer;
		ob_flush();
		flush();
		if ($retbytes) $cnt += strlen($buffer);
	}
	$status = fclose($handle);
	if ($retbytes && $status) return $cnt; // return num. bytes delivered like readfile() does.
 return $status;
}
*/


?>