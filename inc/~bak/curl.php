<?

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function ereg_get($pattern, $string, $num=1) { if (ereg($pattern, $string, $regs)) return $regs[$num]; }


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function load_page($page_name, $url, $post_vars='', $html=1) {
 global $session, $quiet;
	# make cookies string
	if ($session['cookies']) foreach($session['cookies'] as $key=>$val) {
		$cookies.= ($cookies?'; ':'')."$key=$val";
		$cookies_html.=($cookies_html?'; ':'')."<font color=green>$key</font>=<font color=fuchsia>$val</font>";
	}

	if (!$quiet) {	# print request
		if ($post_vars) {
			$post_vars_ar=explode('&', $post_vars);
			$post_vars_html='';
			foreach ($post_vars_ar as $row) {
				list($name,$val) = explode('=', $row, 2);
				$post_vars_html.="<font color=#006635>".urldecode($name)."</font>=<font color=#FF33FF>".urldecode($val)."</font><font color=blue>&</font>";
			}
		}
		if (!$quiet) echo "<hr><div align=left dir=ltr style='word-wrap:break-word'><h5>$page_name</h5>"
			."<b>Cookies:</b> $cookies_html<br><b>"
			.($post_vars?'POST':'GET').":</b> <font color=blue>$url</font><br>"
			.($post_vars?"<b>post_vars:</b> $post_vars_html<br>":'')
			.'</div><div style="overflow:auto; width:100%; height:300px; border:1px solid blue; padding:5px;">';
	}elseif (!$quiet) echo "$page_name\r\n".($post_vars?'POST':'GET').": $url";
	
	$response = curl_get($url, ($cookies?"Cookie: $cookies":''), $post_vars);
	if ($response['cookies']) $session['cookies'] = is_array($session['cookies']) ? array_merge($session['cookies'], $response['cookies']) : $response['cookies'];

	if (!$quiet) { # print responce
		$headers=eregi_replace("(Set-Cookie:)([^=]+)=([^;]*)([^\n]+)", "<font color=red>\\1</font><font color=green>\\2</font>=<font color=fuchsia>\\3</font>\\4", $response['header']);
		$headers=eregi_replace("(Location:)(.+)", "<font color=#CC0000><b>\\1</b></font><font color=blue>\\2</font>", $headers);
		echo "<div align=left><font color=#9900FF>$headers</font></div><br>";
		if ($html) echo "<div dir=ltr align=left>".htmlspecialchars($response['body'])."</div>";
		//else echo eregi_replace('src="', "\\0http://140.4.88.103/dspsoct/", $response['body']);
		echo "</div>\r\n";
	}
	flush();
 return $response;
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function curl_get($url, $headers='', $post_vars='') {
	$ch = curl_init($url);
	//curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');	# user fidder proxy (for testing)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 300);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	
	# add headers
	if ($headers && !is_array($headers)) $headers=explode("\r\n", trim($headers));	# split string headers
	$headers[]="User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";	# add User-Agent header
	$headers[]="Expect:";
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	# add post_vars
	if ($post_vars) { curl_setopt($ch, CURLOPT_POST, 1); curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars); }

	$curl_response = curl_exec($ch);
	//echo "<pre>\$curl_response="; print_r($curl_response); echo "</pre>";
	
	if ($curl_response===false) {
		$response['info']=curl_getinfo($ch);	# add request info array
		echo "<font color=red>Fetch Error!</font><br>Request Info: ".print_r($response['info'],1); exit;
	}
	curl_close ($ch); 

	// Split response into header and body sections
	list($response['header'], $response['body']) = explode("\r\n\r\n", $curl_response, 2);
	if (eregi("^HTTP/", $response['body'])) {	# continue headers
		list($header2, $response['body']) = explode("\r\n\r\n", $response['body'], 2);
		$response['header'].="\r\n$header2";
	}
	$response_header_lines = explode("\r\n", $response['header']);
	
	// First line of headers is the HTTP response code
	$http_response_line = array_shift($response_header_lines);
	if(preg_match('@^HTTP/[0-9]\.[0-9] ([0-9]{3})@',$http_response_line, $matches)) { $response['code']=$matches[1]; }
	
	// put the rest of the headers in an array
	foreach($response_header_lines as $header_line) {
		list($header,$value) = explode(': ', $header_line, 2);
		if (strtolower($header)=='set-cookie') {
			if (eregi("([^=]+)=([^;]*)", $value, $regs)) $response['cookies'][$regs[1]]=$regs[2];
		}
	}
	//echo "<pre>"; print_r($response); echo "</pre>";

 return $response;
}

?>