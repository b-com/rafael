TreeSearchField = Ext.extend(Ext.form.TwinTriggerField, {
	initComponent: function(){
		if(!this.loader.baseParams) this.loader.baseParams = {};
		GridSearchField.superclass.initComponent.call(this);
		this.on('keyup', this.keySearch, this);
		this.keyUpTask = new Ext.util.DelayedTask(this.onTrigger2Click, this);
	},

	paramName:'q', enableKeyEvents:true, validationEvent:false, validateOnBlur:false, hideTrigger1:true, width:150, hasSearch:false,
	trigger1Class:'x-form-clear-trigger', trigger2Class:'x-form-search-trigger', terms:false,

	keySearch: function(f,e){
		if (e.keyCode<48 && e.keyCode!=8 && e.keyCode!=46) return;	// not word char or backspace/delete
		else if (e.keyCode==13) { this.keyUpTask.cancel(); this.onTrigger2Click(); return; } // enter
		this.keyUpTask.delay(700);
	},
	
	onTrigger1Click: function(){
		if(this.hasSearch){
			this.loader.baseParams[this.paramName] = '';
			// this.store.removeAll();
			this.el.dom.value = '';
			this.triggers[0].hide();
			this.hasSearch = false;
			this.terms=false;
			this.loader.load(this.node);
			this.focus();
		}
	},

	onTrigger2Click: function(){
		var v=this.getRawValue().trim();
		if(v.length<1) { this.onTrigger1Click(); return; }
		if(v.length<2) return;
		this.terms=v.replace(/[\.\+\?,;:\\()/]+/g, '\\$1').split(' ');
		// this.store.removeAll();
		this.loader.baseParams[this.paramName] = v;
		this.loader.load(this.node);
		this.hasSearch = true;
		this.triggers[0].show();
		this.focus();
	}
});

CashRep= function() {

	this.deposit = function(account) {
		var sm = this.tree.getSelectionModel(), ids=[];
		
		for (var i=0; i<sm.getSelectedNodes().length; i++) {
			ids.push(sm.getSelectedNodes()[i].attributes.row_id);
			sm.getSelectedNodes()[i].attributes.deposit=account;
			
		}
		window.open('?m=common/accounting&f=deposit&account='+account+'&payments='+Ext.encode(ids),'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	}
	
	this.exportWindow = function(){
		var client_name=encodeURIComponent(this.getTopToolbar().find('paramName','client_name')[0].getValue()),
			deposit = this.getTopToolbar().find('hiddenName','deposit')[0].getValue();
		window.open('?m=accounting/reports&f=cheques&export=1&client_name='+client_name+'&deposit='+deposit,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	};

	this.loader = new Ext.tree.TreeLoader({
		dataUrl: '?m=accounting/reports&f=cheques',
		// baseParams:{client_id:c.client_id, client_type:c.client_type, police_type:this.policeType.getValue()},
		preloadChildren:true,
		
		uiProviders:{'col': Ext.tree.ColumnNodeUI}		
	});
	


	this.loader.on("beforeload", function(treeLoader, node) {
		this.loader.baseParams.year = node.attributes.year;
		this.loader.baseParams.month = node.attributes.month;
		this.loader.baseParams.monthName = node.attributes.monthName;
		this.loader.baseParams.day = node.attributes.day;
		this.loader.baseParams.accept_id = node.attributes.accept_id;
		this.loader.baseParams.invoice_id = node.attributes.invoice_id;
		this.loader.baseParams.row_id = node.attributes.row_id;
		
	}, this);

	

    this.tree = new Ext.ux.tree.TreeGrid({
		enableDD:true,
		width:'100%',
		region:'center',
		singleExpand:true,
		columnResize:false,
		enableSort:false,
		enableHdMenu:false,
		lines:true,
		loader: this.loader,
		 listeners: {
				selectionchange: function(sm,node){if(node[0] && node[0].leaf!==true) sm.unselect(node[0]);}
			},
		selModel: new Ext.tree.MultiSelectionModel({
			
		}),
        columns:[
			{header:'שנה', dataIndex:'year', width:130, align:'right', sortType:'asFloat'}
			,{header:'חודש', dataIndex:'month', width:130, align:'right', sortType:'asFloat'}
			,{ dataIndex:'monthNum'}
			,{ dataIndex:'accept_id'}
			,{ dataIndex:'row_id'}
			,{header:'יום', dataIndex:'day', width:130, align:'right', sortType:'asFloat' }
			,{header:"כניסה", cls:'income_column', dataIndex:'income_sum', width:120, renderer:Ext.util.Format.ilMoney, align:'right', sortType:'asFloat'}
			,{header:"יציאה", cls:'expenseColumn', dataIndex:'expSum', width:120, align:'right', renderer:Ext.util.Format.ilMoney, align:'right', sortType:'asFloat'}
			,{header:"תנועה", dataIndex:'dailyBalance', align:'right', width:120, cls:'invoice',renderer:Ext.util.Format.ilMoney, align:'right', sortType:'asFloat'}
			,{header:"לתשלום", id:'pay',dataIndex:'unpayed',cls:'unpayed', align:'right', width:120,renderer:Ext.util.Format.ilMoney, align:'right', sortType:'asFloat'}
			,{header:"הערות", dataIndex:'notes', width:220, align:'right'}
		]

    });
	
	this.tree.on('contextmenu', function(node, e){
		if(node.leaf!==true) {e.stopEvent(); return;}
		var sm = this.tree.getSelectionModel();
		sm.select(node,true,true);
		this.menu = new Ext.menu.Menu({
			width:120,
			items: [
				{text:'הפקדה', iconCls:'safebox', menu:{defaults:{
					handler:function(e){
						this.deposit(e.accountId)}, scope:this}, items:bankAccounts, scope:this}, scope:this}
				,{hidden:(sm.getSelectedNodes().length!=1), text:'פתח חשבונית',iconCls:'other_police',
				handler:function(){
					
					var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type=invoice_accept&ids='+sm.getSelectedNodes()[0].attributes.invoice_id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this}
				,{hidden:(sm.getSelectedNodes().length!=1), text:'פתח קבלה',iconCls:'other_police',
				handler:function(){
					
					var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type=accept&ids='+sm.getSelectedNodes()[0].attributes.accept_id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this}
				,{hidden:(sm.getSelectedNodes().length!=1), text:'פרטי לקוח',iconCls:'client',handler:function(){
					var win=new ClientWindow(
						{callBack:function(data){}.createDelegate(this)}
						,{bname:name, type:'לקוח', c_type:1, client_id:sm.getSelectedNodes()[0].attributes.client_id}
					);
					win.show(); win.center();
			}, scope:this}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);
	
	
	this.tree.on('dblclick', function(node, e){
		var el=e.getTarget();
		if (el.className=='x-treegrid-text') el=el.offsetParent;
		if (el.className=='x-treegrid-col invoice') {
			var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type=invoice_accept&ids='+node.attributes.invoice_id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
			win.focus();
		} else if (el.className=='x-treegrid-col accept') {
			var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type=accept&ids='+node.attributes.accept_id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
			win.focus();
		} else if (el.className=='x-treegrid-col client_col') {
			if (!node.attributes.client_id) return;
			this.getTopToolbar().find('paramName','client_name')[0].setValue(node.attributes.client_name);
			this.searchField.hasSearch = true;
			this.searchField.triggers[0].show();

			this.loader.baseParams.client_name = node.attributes.client_name;
			this.loader.load(this.tree.root);
		} else if (el.className=='x-treegrid-col deposit') {
			this.getTopToolbar().find('hiddenName','deposit')[0].setValue(node.attributes.deposit);
			this.loader.baseParams.deposit = node.attributes.deposit;
			this.loader.load(this.tree.root);
		}
	},this);
	
	this.searchField = new TreeSearchField({loader:this.loader, node:this.tree.root, paramName:'client_name', width:148});
	
	this.tbar = {items:[
		{xtype:'button', cls:'btnRight', iconCls:'excel', text:'Excel - יצוא ל', width:90, handler:this.exportWindow, scope:this}
		,{text:':לקוח', width:130, style:'display:block; text-align:left;'}, this.searchField
		,{text:':בנק', width:90, style:'display:block; text-align:left;'}
		, new Ext.form.ComboBox({store:bankAccountsAr, hiddenName:'deposit', value:-1, width:118, listClass:'rtl', triggerAction:'all', forceSelection:true, 
			listeners:{
				select:function(f){this.loader.baseParams.deposit = f.getValue(); this.loader.load(this.tree.root);}, 
				scope:this
			}
		})
	]};

	CashRep.superclass.constructor.call(this, {border:false, layout:'border', region:'center', items:[this.tree], tbar:this.tbar})
};
Ext.extend(CashRep, Ext.Panel);

//CashBalanceRep= function() {
//	
//	this.form = new Ext.form.FormPanel({
//		labelWidth:80, height:36, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
//		items: [{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
//				items:[
//				{xtype:'label', text:'תאריך:', style:'padding-right:10px;'},
//				{xtype:'datefield', name:'date', width: 85, format:'d/m/Y', value:today, cls:'ltr', allowBlank:false},//, vtype:'daterange'
//				{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.store.reload();}, scope:this, width: 90}
//				// ,{xtype:'button', cls:'btnRight', id:'cash_prn_btn', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:this.repPrint, scope: this},//repPrint
//				// {xtype:'button', cls:'btnRight', id:'cash_exl_btn', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:this.exportWindow, scope: this}
//			]}
//		]
//	});
//	
//	this.store = new Ext.data.JsonStore({
//		url:'/?m=accounting/reports&f=cash_balance', totalProperty:'total', root:'data',
//		fields: ['type','balance'],
//		sortInfo: {field:'date', direction:'ASC'},
//		timeout:1000, remoteSort:true
//	});
//	
//	this.store.on('beforeload', function(store, options){
//		this.store.removeAll();
//		var setParams=this.form.form.getValues();
//		Ext.apply(store.baseParams, setParams);
//		Ext.apply(options.params, setParams);
//		this.getEl().mask('...טוען');
//	}, this);
//
//	this.store.on('load', function(store, options){
//		Ext.getCmp('cash_prn_btn').setDisabled(!store.getCount());
//		Ext.getCmp('cash_exl_btn').setDisabled(!store.getCount());
//		var Rec = this.store.recordType;
//		var p = new Rec(this.store.reader.jsonData.summary);
//		this.store.insert(this.store.getCount(), p);
//		this.getEl().unmask();
//	}, this);
//	
//	this.colModel = new Ext.grid.ColumnModel([
//			{header:'יתרה', width:100, dataIndex:'balance', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
//			,{header:'סוג מסמך', id:'cash_type', width:120, dataIndex:'type'}
//	]);
//	
//	this.grid = new Ext.grid.GridPanel({
//		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',	autoExpandColumn:'cash_type', border:false, enableHdMenu:false,
//		store: this.store,
//		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
//		colModel: this.colModel
//		//, bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
//	});
//
//	
//	CashBalanceRep.superclass.constructor.call(this, {border:false, layout:'border', title:'דוח יתרות קופה', items:[this.form,this.grid]})
//};
//Ext.extend(CashBalanceRep, Ext.Panel);
/*
CashRep = function() {

	this.repPrint = function() {
		var params=this.form.form.getValues();
		window.open('?m=accounting/reports&f=cash&print=1&d_from='+params.d_from+'&d_to='+params.d_to+'&type='+params.type,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	};
	
	this.exportWindow = function(){
		var params=this.form.form.getValues();
		window.open('?m=accounting/reports&f=cash&export=1&d_from='+params.d_from+'&d_to='+params.d_to+'&type='+params.type);
	};
	
	this.form = new Ext.form.FormPanel({
		labelWidth:80, height:66, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
		items: [{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
				items:[
				{xtype:'label', text:'תקופה בין:', style:'padding-right:10px;'},
				{xtype:'datefield', name:'d_from', width: 85, format:'d/m/Y', value:month_first(), cls:'ltr', allowBlank:false},//, vtype:'daterange'
				{xtype:'label', text:'ל:'},
				{xtype:'datefield', name:'d_to',width: 85, format:'d/m/Y', value:month_last(), cls:'ltr', allowBlank:false},//, vtype:'daterange'
				{xtype:'label', text:'סיווג:', style:'padding-right:10px;'},{xtype:'combo', hiddenName:'type', width: 150, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:[[0,'כל פעילות'],[1001,'מזומן'],[1002,"שיקים"],[3,'אשראי']]}
			]},{layout:'table', layoutConfig: {columns:8}, baseCls: 'x-plain', cls:'rtl', style:'padding:5px 120px',
			items:[
				{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.store.reload();}, scope:this, width: 90},
				{xtype:'button', cls:'btnRight', id:'cash_prn_btn', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:this.repPrint, scope: this},//repPrint
				{xtype:'button', cls:'btnRight', id:'cash_exl_btn', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:this.exportWindow, scope: this}
			]}
		]
	});
		
	this.store = new Ext.data.JsonStore({
		url:'/?m=accounting/reports&f=cash', totalProperty:'total', root:'data',
		fields: ['doc_id','client_id','client_name',{name:'date',type:'date', dateFormat:'Y-m-d'},'notes','num','type','credit','debt','balance','username'],
		sortInfo: {field:'date', direction:'ASC'},
		timeout:1000, remoteSort:true
	});
	
	// this.hlStr = hlStr;
	// this.searchField = new GridSearchField({store:this.store, width:150});
	
	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams=this.form.form.getValues();
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
		this.getEl().mask('...טוען');
	}, this);

	this.store.on('load', function(store, options){
		Ext.getCmp('cash_prn_btn').setDisabled(!store.getCount());
		Ext.getCmp('cash_exl_btn').setDisabled(!store.getCount());
		var Rec = this.store.recordType;
		var p = new Rec(this.store.reader.jsonData.summary);
		this.store.insert(this.store.getCount(), p);
		this.getEl().unmask();
	}, this);
	
	this.colModel = new Ext.grid.ColumnModel([
			{header:'משתמש', dataIndex:'username', width:75, sortable:false, hidden:true}
			,{header:'הערות', id: 'notes', dataIndex:'notes', sortable:false}
			,{header:'יתרה', width:100, dataIndex:'balance', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'זכות', width:100, dataIndex:'debt', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'חובה', width:100, dataIndex:'credit', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'שם לקוח/ספק', width:150, dataIndex:'client_name'}
			,{header:'מספר', width:120, dataIndex:'num'}
			,{header:'סוג מסמך', width:120, dataIndex:'type', renderer:docTypeRender}
			,{header:'תאריך הפקה', dataIndex:'date', width:85, css:'direction:ltr;', align:'center', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
	]);
	
	// this.onItemCheck = function (item, checked){
		// var cm = this.grid.getColumnModel();
		// if (item.text=='משתמש')	cm.setHidden(0, !checked);// else
    // };

	this.grid = new Ext.grid.GridPanel({
		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',	autoExpandColumn:'notes', border:false, enableHdMenu:false,
		store: this.store,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		// tbar: [
			// 'חיפוש:', this.searchField
			// ,'-' ,{ text:'פתח לקוח', id:'user_open', iconCls:'user_open', handler:function(){openClientNewWin(this.grid.getSelectionModel().getSelected().data)}, disabled:true, scope:this }
			// ,'-' ,{text:'הגדרת עמודות', iconCls:'cols-icon', //x-menu-item-icon 
				// menu: {items: [
					// {text: 'משתמש', checked: false, checkHandler:this.onItemCheck, scope:this}
				// ]}} 
		// ],
		colModel: this.colModel,
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
	});
	
	// this.grid.getSelectionModel().on('selectionchange', function(sm, row, r) {
		// this.grid.getTopToolbar().items.get('user_open').setDisabled(sm.getCount()!=1);
	// }, this);

	this.grid.on('rowdblclick', function(grid){
		var data = grid.getSelectionModel().getSelected().data;
		var win=window.open('http://'+location.host+'/?m=accounting/print_doc&type='+data.type+'&id='+data.doc_id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	});
	
	this.grid.on('rowcontextmenu', function(grid, row, e){
		var r=this.store.getAt(row);
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'פתח לקוח בחלון חדש', iconCls:'user_open', scope:this, handler: function(){openClientNewWin(r.data);} }
				,{text:'למסומנים SMS', iconCls:'send_sms', scope:this, 
					handler:function(){
						this.grid.getSelectionModel().selectRow(row,true);
						var records = this.grid.getSelectionModel().getSelections();
						var ids=[]; var lids=[];
						Ext.each(records, function(r){
							if (r.data.link) lids.push(r.data.id);
							else ids.push(r.data.client_id);
						});
						this.sendSmsWindow({ids:ids.join(','), lids:lids.join(',')});
					}
				}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);
	
	CashRep.superclass.constructor.call(this, {border:false, layout:'border', title:'דוח תנועות קופה', items:[this.form,this.grid]})
};
Ext.extend(CashRep, Ext.Panel);
*/
//ChequesCashRep= function() {
//	this.exportWindow = function(){
//		var SortState = this.store.getSortState();
//		window.open('?m=accounting/reports&f=cheques&export=1&payment_status=0,3&sort='+SortState.field+'&dir='+SortState.direction);
//	};
//
//	this.repPrint = function() {
//		var SortState = this.store.getSortState();
//		window.open('?m=accounting/reports&f=cheques&print=1&payment_status=0,3&sort='+SortState.field+'&dir='+SortState.direction,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
//	};
//	
//	this.form = new Ext.form.FormPanel({
//		labelWidth:80, height:36, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
//		items: [{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
//				items:[
//				{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.store.reload();}, scope:this, width: 90}
//				,{xtype:'button', cls:'btnRight', id:'cheques_prn_btn', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:this.repPrint, scope: this}
//				,{xtype:'button', cls:'btnRight', id:'cheques_exl_btn', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:this.exportWindow, scope: this}
//			]}]
//		});
//	
//	this.store = new Ext.data.JsonStore({
//		url:'?m=accounting/reports&f=cheques',
//		totalProperty:'total', root:'data',
//		fields: ['accept_id','num','payment_status','client_name',{name:'accept_date',type:'date', dateFormat:'Y-m-d'},{name:'date',type:'date', dateFormat:'Y-m-d'},'dept','bank','account','number','sum'],
//		sortInfo: {field:'date', direction:'ASC'},
//		timeout:1000, remoteSort:true
//	});
//	
//	this.store.on('beforeload', function(store, options){
//		this.store.removeAll();
//		var setParams={payment_status:'0,3'};
//		Ext.apply(store.baseParams, setParams);
//		Ext.apply(options.params, setParams);
//		// this.getEl().mask('...טוען');
//	}, this);
//
//	this.store.on('load', function(store, options){
//		Ext.getCmp('cheques_prn_btn').setDisabled(!store.getCount());
//		Ext.getCmp('cheques_exl_btn').setDisabled(!store.getCount());
//		var Rec = this.store.recordType;
//		var p = new Rec(this.store.reader.jsonData.summary);
//		this.store.insert(this.store.getCount(), p);
//		this.getEl().unmask();
//	}, this);
//	
//	this.colModel = new Ext.grid.ColumnModel([
//		{header:'סכום', dataIndex:'sum', width:90, summaryType:'sum', renderer:Ext.util.Format.ilMoney}
//		,{header:"מס'  שיק", dataIndex:'number'}
//		,{header:'חשבון', dataIndex:'account', width:90}
//		,{header:"בנק", dataIndex:'bank', width:90}
//		,{header:"סניף", dataIndex:'dept', width:50}
//		,{header:'שם לקוח', dataIndex:'client_name', sortable:true, id:'client_name'}
//		,{header:'תאריך פרעון', dataIndex:'date', width:85, sortable:true, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
//		,{header:'מספר קבלה', dataIndex:'num', width:60}
//		,{header:'תאריך קבלה', dataIndex:'accept_date', width:85, sortable:true, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
//		,{header:'סטאטוס', dataIndex:'payment_status', width:60, sortable:true, renderer:function(v) {return (v==3 ?'חזר':'')}}
//	]);
//	
//	this.grid = new Ext.grid.GridPanel({
//		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',	autoExpandColumn:'client_name', border:false, enableHdMenu:false,
//		store: this.store,
//		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
//		colModel: this.colModel
//		//, bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
//	});
//
//	this.grid.on('rowdblclick', function(grid){
//		var data = grid.getSelectionModel().getSelected().data;
//		var win=window.open('http://'+location.host+'/?m=accounting/print_doc&type=accept&id='+data.accept_id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
//	});
//	
//	ChequesCashRep.superclass.constructor.call(this, {border:false, layout:'border', region:'center', title:'רשימת שיקים בקופה', items:[this.form,this.grid]})
//
//	this.store.load();
//
//};
//Ext.extend(ChequesCashRep, Ext.Panel);
//
//CreditsCashRep= function() {
//	this.exportWindow = function(){
//		var SortState = this.store.getSortState();
//		window.open('?m=accounting/reports&f=credit_payments&export=1&payment_status=0,3&sort='+SortState.field+'&dir='+SortState.direction);
//	};
//
//	this.repPrint = function() {
//		var SortState = this.store.getSortState();
//		window.open('?m=accounting/reports&f=credit_payments&print=1&payment_status=0,3&sort='+SortState.field+'&dir='+SortState.direction,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
//	};
//	
//	this.form = new Ext.form.FormPanel({
//		labelWidth:80, height:36, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
//		items: [{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
//				items:[
//				{xtype:'label', text:'חברת אשראי:', style:'padding-right:10px;'},{xtype:'combo', hiddenName:'bank', width: 150, value:'כולם', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:['כולם','וויזה','ישראכרט/מאסטרכארד','לאומי קארד','דיינרס','אמריקן אקספרס']}
//				,{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.store.reload();}, scope:this, width: 90}
//				,{xtype:'button', cls:'btnRight', id:'credits_prn_btn', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:this.repPrint, scope: this}
//				,{xtype:'button', cls:'btnRight', id:'credits_exl_btn', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:this.exportWindow, scope: this}
//			]}]
//		});
//	
//	this.store = new Ext.data.JsonStore({
//		url:'?m=accounting/reports&f=credit_payments',
//		totalProperty:'total', root:'data',
//		fields: ['accept_id','num','payment_status','client_name',{name:'accept_date',type:'date', dateFormat:'Y-m-d'},{name:'date',type:'date', dateFormat:'Y-m-d'},'bank','number','payment','sum'],
//		sortInfo: {field:'date', direction:'ASC'},
//		timeout:1000, remoteSort:true
//	});
//	
//	this.store.on('beforeload', function(store, options){
//		this.store.removeAll();
//		var setParams=this.form.form.getValues();
//		setParams.status=0;
//		Ext.apply(store.baseParams, setParams);
//		Ext.apply(options.params, setParams);
//		// this.getEl().mask('...טוען');
//	}, this);
//
//	this.store.on('load', function(store, options){
//		Ext.getCmp('credits_prn_btn').setDisabled(!store.getCount());
//		Ext.getCmp('credits_exl_btn').setDisabled(!store.getCount());
//		var Rec = this.store.recordType;
//		var p = new Rec(this.store.reader.jsonData.summary);
//		this.store.insert(this.store.getCount(), p);
//		this.getEl().unmask();
//	}, this);
//	
//	this.colModel = new Ext.grid.ColumnModel([
//		{header:'סכום', dataIndex:'sum', width:90, summaryType:'sum', renderer:Ext.util.Format.ilMoney}
//		,{header:"תשלום", dataIndex:'payment', width:70}
//		,{header:"מס'  כרטיס", dataIndex:'number'}
//		,{header:"סוג אשראי", dataIndex:'bank', width:90}
//		,{header:'שם לקוח', dataIndex:'client_name', sortable:true, id:'client_name'}
//		,{header:'תאריך פרעון', dataIndex:'date', width:85, sortable:true, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
//		,{header:'מספר קבלה', dataIndex:'num', width:60}
//		,{header:'תאריך קבלה', dataIndex:'accept_date', width:85, sortable:true, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
//	]);
//	
//	this.grid = new Ext.grid.GridPanel({
//		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',	autoExpandColumn:'client_name', border:false, enableHdMenu:false,
//		store: this.store,
//		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
//		colModel: this.colModel
//		//, bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
//	});
//
//	this.grid.on('rowdblclick', function(grid){
//		var data = grid.getSelectionModel().getSelected().data;
//		var win=window.open('http://'+location.host+'/?m=accounting/print_doc&type=accept&id='+data.accept_id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
//	});
//
//	CreditsCashRep.superclass.constructor.call(this, {border:false, layout:'border', region:'center', title:'רשימת תשלומי אשראי', items:[this.form,this.grid]})
//	
//	this.store.load();
//};
//Ext.extend(CreditsCashRep, Ext.Panel);
//
//PaimentsRep = function() {
//
//	this.repPrint = function() {
//		var params=this.form.form.getValues();
//		window.open('?m=accounting/reports&f=paiments&print=1&d_from='+params.d_from+'&d_to='+params.d_to,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
//	};
//	
//	this.exportWindow = function(){
//		var params=this.form.form.getValues();
//		window.open('?m=accounting/reports&f=paiments&export=1&d_from='+params.d_from+'&d_to='+params.d_to);
//	};
//	
//	this.form = new Ext.form.FormPanel({
//		labelWidth:80, height:66, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
//		items: [{layout:'table', layoutConfig: {columns:7}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
//				items:[
//				{xtype:'label', text:'תקופה בין:', style:'padding-right:10px;'},
//				{xtype:'datefield', name:'d_from', width: 85, format:'d/m/Y', value:month_first(), cls:'ltr', allowBlank:false},//, vtype:'daterange'
//				{xtype:'label', text:'ל:'},
//				{xtype:'datefield', name:'d_to',width: 85, format:'d/m/Y', value:month_last(), cls:'ltr', allowBlank:false},//, vtype:'daterange'
//				// {xtype:'label', text:'סיווג:', style:'padding-right:10px;'},{xtype:'combo', hiddenName:'type', width: 150, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:[[0,'כל פעילות'],[1001,'מזומן'],[1002,"שיקים"],[3,'אשראי']]}
//			// ]},{layout:'table', layoutConfig: {columns:8}, baseCls: 'x-plain', cls:'rtl', style:'padding:5px 120px',
//			// items:[
//				{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.store.reload();}, scope:this, width: 90},
//				{xtype:'button', cls:'btnRight', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:this.repPrint, scope: this},//repPrint
//				{xtype:'button', cls:'btnRight', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:this.exportWindow, scope: this}
//			]}
//		]
//	});
//		
//	this.store = new Ext.data.JsonStore({
//		url:'/?m=accounting/reports&f=paiments', totalProperty:'total', root:'data',
//		fields: ['client_id','client_name','credit','debt','balance'],
//		sortInfo: {field:'date', direction:'ASC'},
//		timeout:1000, remoteSort:true
//	});
//	
//	// this.hlStr = hlStr;
//	// this.searchField = new GridSearchField({store:this.store, width:150});
//	
//	this.store.on('beforeload', function(store, options){
//		this.store.removeAll();
//		var setParams=this.form.form.getValues();
//		Ext.apply(store.baseParams, setParams);
//		Ext.apply(options.params, setParams);
//		this.getEl().mask('...טוען');
//	}, this);
//
//	this.store.on('load', function(store, options){
//		this.form.find('iconCls','print')[0].setDisabled(!store.getCount());
//		this.form.find('iconCls','excel')[0].setDisabled(!store.getCount());
//		// Ext.getCmp('cash_prn_btn').setDisabled(!store.getCount());
//		// Ext.getCmp('cash_exl_btn').setDisabled(!store.getCount());
//		var Rec = this.store.recordType;
//		var p = new Rec(this.store.reader.jsonData.summary);
//		this.store.insert(this.store.getCount(), p);
//		this.getEl().unmask();
//	}, this);
//	
//	this.colModel = new Ext.grid.ColumnModel([
//			// {header:'משתמש', dataIndex:'username', width:75, sortable:false, hidden:true}
//			// ,{header:'הערות', id: 'notes', dataIndex:'notes', sortable:false}
//			// ,
//			{header:'הפרש', width:100, dataIndex:'balance', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
//			,{header:'תשלומים', width:100, dataIndex:'debt', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
//			,{header:'תקבולים', width:100, dataIndex:'credit', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
//			,{header:'שם לקוח/ספק', dataIndex:'client_name', id:'client_name'}
//			// ,{header:'מספר', width:120, dataIndex:'num'}
//			// ,{header:'סוג מסמך', width:120, dataIndex:'type', renderer:docTypeRender}
//			// ,{header:'תאריך הפקה', dataIndex:'date', width:85, css:'direction:ltr;', align:'center', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
//	]);
//	
//	// this.onItemCheck = function (item, checked){
//		// var cm = this.grid.getColumnModel();
//		// if (item.text=='משתמש')	cm.setHidden(0, !checked);// else
//    // };
//
//	this.grid = new Ext.grid.GridPanel({
//		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',	autoExpandColumn:'client_name', border:false, enableHdMenu:false,
//		store: this.store,
//		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
//		// tbar: [
//			// 'חיפוש:', this.searchField
//			// ,'-' ,{ text:'פתח לקוח', id:'user_open', iconCls:'user_open', handler:function(){openClientNewWin(this.grid.getSelectionModel().getSelected().data)}, disabled:true, scope:this }
//			// ,'-' ,{text:'הגדרת עמודות', iconCls:'cols-icon', //x-menu-item-icon 
//				// menu: {items: [
//					// {text: 'משתמש', checked: false, checkHandler:this.onItemCheck, scope:this}
//				// ]}} 
//		// ],
//		colModel: this.colModel,
//		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
//	});
//	
//	// this.grid.getSelectionModel().on('selectionchange', function(sm, row, r) {
//		// this.grid.getTopToolbar().items.get('user_open').setDisabled(sm.getCount()!=1);
//	// }, this);
//
//	// this.grid.on('rowdblclick', function(grid){
//		// var data = grid.getSelectionModel().getSelected().data;
//		// var win=window.open('http://'+location.host+'/?m=accounting/print_doc&type='+data.type+'&id='+data.doc_id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
//	// });
//	
//	this.grid.on('rowcontextmenu', function(grid, row, e){
//		var r=this.store.getAt(row);
//		this.menu = new Ext.menu.Menu({
//			items: [
//				{text:'פתח לקוח בחלון חדש', iconCls:'user_open', scope:this, handler: function(){openClientNewWin(r.data);} }
//				,{text:'למסומנים SMS', iconCls:'send_sms', scope:this, 
//					handler:function(){
//						this.grid.getSelectionModel().selectRow(row,true);
//						var records = this.grid.getSelectionModel().getSelections();
//						var ids=[]; var lids=[];
//						Ext.each(records, function(r){
//							if (r.data.link) lids.push(r.data.id);
//							else ids.push(r.data.client_id);
//						});
//						this.sendSmsWindow({ids:ids.join(','), lids:lids.join(',')});
//					}
//				}
//			]
//		});
//		e.stopEvent();
//		this.menu.showAt(e.getXY());
//	},this);
//	
//	PaimentsRep.superclass.constructor.call(this, {border:false, layout:'border', title:'דוח תקבולים ותשלומים', items:[this.form,this.grid]})
//};
//Ext.extend(PaimentsRep, Ext.Panel);
//
//CashInvoiceRep= function() {
//	this.repPrint = function() {
//		window.open('?m=accounting/reports&f=accept_invoice&print=1','','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
//	};
//	
//	this.exportWindow = function(){
//		window.open('?m=accounting/reports&f=accept_invoice&export=1');
//	};
//
//	this.invoiceWindow = function(t,d){
//		if (d.json && d.json.invoice) {Ext.Msg.alert('הזהרה!', 'חשבונית כבר הופקה!'); return;}
//		var grid=this.grid;
//		grid.client_id=d.client_id;
//		var win=new InvoiceWindow(grid,t,{client_id:this.client_id, doc_type:'order', orders_ids:d.orders_ids, notes:"קבלה מס' "+d.accept_num, payments_ids:d.id, isum:d.sum}); win.show();
//	}
//
//	this.form = new Ext.form.FormPanel({
//		labelWidth:80, height:66, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
//		items: [{layout:'table', layoutConfig: {columns:7}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
//				items:[
//				{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){this.store.reload();}, scope:this, width: 90},
//				{xtype:'button', cls:'btnRight', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:this.repPrint, scope: this},//repPrint
//				{xtype:'button', cls:'btnRight', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:this.exportWindow, scope: this}
//			]}
//		]
//	});
//	
//	this.store = new Ext.data.JsonStore({
//		url:'/?m=accounting/reports&f=accept_invoice', totalProperty:'total', root:'data',
//		fields: ['id','client_id','client_name','accept_num',{name:'accept_date',type:'date', dateFormat:'Y-m-d'},'ptype',{name:'date',type:'date', dateFormat:'Y-m-d'},'orders_ids','sum'],
//		sortInfo: {field:'date', direction:'ASC'},
//		timeout:1000, remoteSort:true
//	});
//	
//	this.store.on('load', function(store, options){
//		this.form.find('iconCls','print')[0].setDisabled(!store.getCount());
//		this.form.find('iconCls','excel')[0].setDisabled(!store.getCount());
//		// this.getEl().unmask();
//	}, this);
//
//	this.colModel = new Ext.grid.ColumnModel([
//			{header:'סכום', width:100, dataIndex:'sum', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
//			,{header:'תאריל פרעון', width:100, dataIndex:'date', width:85, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
//			,{header:'סוג תשלום', width:100, dataIndex:'ptype'}
//			,{header:"תאריך קבלה", dataIndex:'accept_date', width:85, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
//			,{header:"מס' קבלב", dataIndex:'accept_num'}
//			,{header:'שם לקוח/ספק', dataIndex:'client_name', id:'client_name'}
//	]);
//	
//	this.grid = new Ext.grid.GridPanel({
//		stripeRows:true, layout:'fit', region:'center',	autoExpandColumn:'client_name', border:false, enableHdMenu:false,
//		store: this.store,
//		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
//		// tbar: [
//			// 'חיפוש:', this.searchField
//			// ,'-' ,{ text:'פתח לקוח', id:'user_open', iconCls:'user_open', handler:function(){openClientNewWin(this.grid.getSelectionModel().getSelected().data)}, disabled:true, scope:this }
//			// ,'-' ,{text:'הגדרת עמודות', iconCls:'cols-icon', //x-menu-item-icon 
//				// menu: {items: [
//					// {text: 'משתמש', checked: false, checkHandler:this.onItemCheck, scope:this}
//				// ]}} 
//		// ],
//		colModel: this.colModel,
//		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
//	});
//	
//	this.grid.on('rowcontextmenu', function(grid, row, e){
//		var r=this.store.getAt(row);
//		this.menu = new Ext.menu.Menu({
//			items: [
//				{text:'פתח לקוח בחלון חדש', iconCls:'user_open', scope:this, handler: function(){openClientNewWin(r.data);} }
//				,{text:'הפק חשבונית', iconCls:'police_add', scope:this, handler: function(){this.invoiceWindow('invoice_credit',r.data);} }
//
//			]
//		});
//		e.stopEvent();
//		this.menu.showAt(e.getXY());
//	},this);
//
//	CashInvoiceRep.superclass.constructor.call(this, {border:false, layout:'border', title:'רשימת חשבוניות להפקה', items:[this.form,this.grid]})
//
//	this.store.load();
//};
//Ext.extend(CashInvoiceRep, Ext.Panel);