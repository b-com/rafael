
IndexBarGrid = Ext.extend(Ext.grid.EditorGridPanel, {

	init_toolbar: function() {
		
		this.store = new Ext.data.JsonStore({
			url:'?m=common/clients&f=clients_shortList',
			params:{deleted:'0'},
			totalProperty:'total',	
			root:'data',
			fields: this.fields,
			remoteSort: true
		});
	
	//-------delete function--------------------
		this.delItem = function(){
		
			if (this.sRow!==null) this.selModel.selectRow(this.sRow); // content menu
			if (!this.getSelectionModel().getCount()) return;
			var records = this.getSelectionModel().getSelections();
			

			Ext.MessageBox.show({
				width:250, 
				title:'מחיקת תוכן', 
				msg:'האם ברצונך למחוק את השור'+(records.length===1?'ה':'ות המסומנות')+'?', 
				buttons:Ext.MessageBox.YESNO, scope:this,
				fn: function(btn) {
					 
					if(btn==='yes') {

						var ids=[];
						var passport;
						Ext.each(records, function(r){
							ids.push(r.data.id);
							passport=r.data.passport;
						});
						this.getEl().mask('...טוען');
						
						Ext.Ajax.request({
							url:'?m=common/clients&f=del_client',
							params:{passport:passport, ids:ids},
							success: function(r,o){
								d=Ext.decode(r.responseText);
								if (!ajRqErrors(d)) {
									Ext.each(records, function(d){
										row=this.store.indexOf(d);
										this.selModel.selectRow(row);
										this.view.getRow(row).style.border='1px solid red';
										Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
									},this);

								}else
									Ext.Msg.confirm('לקוח נמחק בהצלחה!');
								
								this.getEl().unmask();
								
								
							},
							failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed'); },
							scope: this
						});
						if (this.php_class=='ACC_class') {
										this.cardsGrid.store.reload()
										this.cardsGrid.class_id_store.reload()
									}
					}
				}
			});
		};
		
		//-------end delete function--------------------

		//-------end edit function--------------------		
		
		this.editItem = function(){
						if (this.sRow!==null) this.selModel.selectRow(this.sRow); // content menu
						if (!this.getSelectionModel().getCount()) return;
						// var records = this.getSelectionModel().getSelections();
					
						var record = this.selModel.getSelected();
						this.plugins.startEditing(record);
					}
					
					
		//-------end edit function--------------------		
	
	
	//-------print function-------------------------
		this.printList = function() {
			this.printExportList('print_all')
		};
		
		this.exportExcel = function() {
			this.printExportList('export_excel')
		};	
		
		this.printExportList = function(method){
		url='/?m=config/cards_class&class='+this.php_class+'&method='+method;
		window.open(url, '', 'toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
		};	
	//-------end print function--------------------
	
	//-------add new button function--------------------

	this.onAdd = function (btn, ev) {
        var u = new this.store.recordType({});
        this.plugins.stopEditing();
        this.store.insert(0, u);
        this.plugins.startEditing(0);
    };
	//------- end add new button function--------------------
	
	this.plugins = new Ext.ux.grid.RowEditor({
		errorSummary:false,
		cls:'rtl',
		parent:this,
		listeners:{

					canceledit:function(o, a){
						try{
							var store = this.parent.getStore();
							var rec = store.getAt(0);
							if(!rec.data.id){
								store.remove(rec);
								this.parent.getView().refresh();
								} 
							}
						catch(e){}
					},
				 	beforeedit:function(o, rowIndex){
						var g = this.parent, view = g.getView(),
							row = view.getRow(rowIndex),
							record = g.store.getAt(rowIndex);
						
						if (record.data.group_id==0){
							g.getColumnModel().getCellEditor(0,rowIndex ).field.disable();
							if (this.parent.php_class=='ACC_card')
								g.getColumnModel().getCellEditor(2,rowIndex ).field.disable();

						}else {
							g.getColumnModel().getCellEditor(0,rowIndex ).field.enable();
							if (this.parent.php_class=='ACC_card')
								g.getColumnModel().getCellEditor(2,rowIndex ).field.enable();
						}
					}, 
					afteredit:function(o, d, rec, i){
					
						if (rec.data.id) {var method='update';} else {var method='add';}
						Ext.Ajax.request({
							url: '?m=common/clients&f=add_client',
							params:rec.data,
							success: function(r,o){
							r = Ext.decode(r.responseText);
							if(ajRqErrors(r)){ 
									//Error need alert and rollback changes
									Ext.Msg.alert('קרתה תקלה, אנא נסה שנית');
									if (rec.data.id) {
										this.parent.getStore().reload();
									}
									else {	
										this.parent.getStore().removeAt(i);
										this.parent.getView().refresh();
									}
								} 
								else{
									if (rec.data.id) {
									rec.commit();
									//	this.parent.getStore().reload(); 
									//	this.parent.getView().refresh();
									}
									else{ //it is new
										this.parent.getStore().reload(); 
										this.parent.getView().refresh();
										
									}
									if (this.parent.php_class=='ACC_class') {
										this.parent.cardsGrid.store.reload()
										this.parent.cardsGrid.class_id_store.reload()
									}
								}
							},
							failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
							scope: this
						});
					}
		},scope:this
		
	}),
	this.hlStr = hlStr;
	this.searchField = new GridSearchField({store:this.store, width:120});
	this.clientWindow = function(d){ var win=new ClientWindow(this, d); win.show(); win.center();}
	this.selModel = new Ext.grid.RowSelectionModel({singleSelect:true,
					listeners:{
						rowselect: function(sm, rowIndex, rec){
							this.getTopToolbar().find('iconCls','delete')[0].setDisabled(rec.data.group_id==0);
							this.getTopToolbar().find('iconCls','edit')[0].setDisabled(rec.data.group_id==0);
							// this.getTopToolbar().find('iconCls','user_edit')[0].setDisabled(rec.data.group_id==0);
						},
						scope:this
					}
				}),
				
	this.tbar = [
				 { text:'חיפוש', scope:this }, this.searchField,
				 { text:'לקוח חדש', iconCls:'add', handler:this.onAdd, scope:this }
				,{ text:'מחיקת לקוח',  iconCls:'delete', handler:this.delItem, disabled:true, scope:this}
				,{ text:'עריכת לקוח',  iconCls:'edit', handler:this.editItem, disabled:true, scope:this}
				// ,{ text:'תצוגה מורחבת',  name:'complete_view', iconCls:'print',handler:this.printList, scope:this }
				// ,{ text:'עדכן', iconCls:'user_edit', disabled:true,
				// 	handler:function(){
				// 		var selectedRow = this.selModel.getSelected();
				// 		console.log(selectedRow);
				// 		// var rowId = this.selModel.getSelected().store.indexOf(this.selModel.getSelection()[0]);
				// 		// console.log(rowID);
				// 		// console.log(this.selModel.getSelected());
				// 		// Dump(this.plugins);
				// 		// var id = this.selModel.getSelected().json;
   	// 					// this.clientWindow(id);
   						
 			// 		},
				// scope:this 
				// }
	];
	 
	this.bbar = new Ext.PagingToolbar({store:this.store,pageSize:50, displayInfo:true});
	}
});


textField = Ext.extend(Ext.form.TextField, {allowBlank:false, listeners:{ show: function(el){el.focus(true,100);}}});

var Record = Ext.data.Record.create(['id','client']);

ClientsGrid= function(d,cfg) {
	this.fields = ['id','client','passport','email','adr0','city0','fax','phones','birthday'];
	// this.php_class = 'ACC_card';
	
	
	this.init_toolbar();
		
	this.store.load();
	
	
	
	ClientsGrid.superclass.constructor.call(this, {
		// title:'כרטיסיות',
		// php_class: 'ACC_card',
		combo:cfg.combo,
		store:this.store, 
		autoScroll:true,
		width:600,
		height:400, 
		layout:'fit',
		cls:'ltr',
		// region:'west',
		frame:true, 
		border:false, 
		autoExpandColumn:'phones',
		selModel:this.selModel,
		colModel: new Ext.grid.ColumnModel({
						defaults: {sortable:false, menuDisabled:true},
						columns: [
								// {header:'תאריך לידה', width:90, dataIndex:'birthday',id:'birthday', editor: new Ext.form.TextField({allowBlank:false})},
								{header:'טלפון', width:90, dataIndex:'phones', id:'phones', editor: new Ext.form.TextField({})},
								{header:'פקס', width:80, dataIndex:'fax', id:'fax', editor: new Ext.form.TextField({})},
								{header:'עיר', width:75, dataIndex:'city0', editor: new Ext.form.TextField({})},
								{header:'כתובת', width:80, dataIndex:'adr0', editor: new Ext.form.TextField({})},
								{header:'דוא"ל', width:110, dataIndex:'email', editor: new Ext.form.TextField({
								validator: function(newEmailAddress)
											{
												var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
												if(!reg.test(newEmailAddress) ) {
												return 'כתובת המייל אינה תקינה';
												} else {
														return true;
												}
											}
								
								})},
								{header:'ת.זהות/ח"פ', width:75, dataIndex:'passport', editor: new Ext.form.TextField({
								validator: function(newPassport)
											{
												var reg = /^([0-9\.])/;
												if(reg.test(newPassport)){
													return true;
												} else {
														return 'יש להזין 9 ספרות';
												}
												
											}
								
								})},
								{header:'שם לקוח', width:80, dataIndex:'client',renderer:this.hlStr.createDelegate(this), editor: new Ext.form.TextField({allowBlank:false})}
								// ,{header:'קוד קבוצה', width:75, dataIndex:'class_id', editor: new Ext.form.ComboBox({store:this.class_id_store, displayField:'class_id',allowBlank:false, valueField:'userclass_row_id', autoScroll:'true', mode:'remote', listClass:'rtl', triggerAction:'all', forceSelection:true }), align: 'center'}
						]
						})
	});
			
	//for calling outside this
	this.on('activate',function(){
		this.store.load();  
		
	});
	this.on('rowcontextmenu',function(thisGrid, rowIndex, evtObj){

	evtObj.stopEvent(); //Hide default browser context menu

	thisGrid.getSelectionModel().selectRow(rowIndex);
		if(!thisGrid.rowCtxMenu){ //create static instance of Ext menu
			thisGrid.rowCtxMenu = new Ext.menu.Menu({
				items : {
					text : 'ערוך לקוח',
					handler : function(){
						var record = thisGrid.selModel.getSelected();
						thisGrid.plugins.startEditing(record);
					}
				}
			})
		}
		thisGrid.rowCtxMenu.showAt(evtObj.getXY());
	});
};
Ext.extend(ClientsGrid, IndexBarGrid);

ClientWindow = function(grid, d, cat) {
	// if(m=='garage/clients') cat='garage';
	// console.log(d);
	
	if (!cat) cat='clients';
	if (d) this.client_id=(d.client_id || d.id);

	var phone_cnt=1, email_cnt=1, adr_cnt=1, group_cnt=1;
	var clientPhoto="skin/pict/man_icon.gif";
	
	// var famStore = [[0,'-בחר-'],[1,'אבא'],[2,'אמא'],[3,'בעל'],[4,'אשה'],[5,'בן'],[6,'בת'],[8,'אח'],[9,'אחות'],[7,'אחר']];
	
	var telStore = [[1,'בית'],[2,'עבודה'],[3,'נייד'],[4,'פקס בבית'],[5,'פקס בעבודה'],[0,'אחר']];
	var adrStore = [[1,'בית'],/*[2,'חברה/עסק'],*/[3,'עבודה'],[4,'למשלוח דואר'],[0,'אחר']];
	var emailStore=[[1,'בית'],[2,'עבודה'],[0,'אחר']];
	this.grid=grid;
	
	
	this.addPhone = function() {if (phone_cnt>7) return; Ext.getCmp('phoneSet'+phone_cnt).show(); this.syncShadow(); phone_cnt++;}
	this.addEmail = function() {if (email_cnt>2) return; Ext.getCmp('emailSet'+email_cnt).show(); this.syncShadow(); email_cnt++;}
	this.addAddress = function() {if (adr_cnt>2) return; Ext.getCmp('adrSet'+adr_cnt).show(); this.syncShadow(); adr_cnt++;}
	this.addgroups = function() {if (group_cnt>2) return; Ext.getCmp('groupSet'+group_cnt).show(); this.syncShadow(); group_cnt++;}

	// var employersList=[[0,'-בחר-']];
	
	// var client_groups_add_combo = [[0,'<font style="font-size:24px; line-height:6px;" color=#C0C0C0 face=Arial>&#9632;</font> &nbsp;ללא קבוצה']].concat(CFG.client_groups);

	// this.profStore = new Ext.data.JsonStore({url:'/?m=crm/clients_f&f=select_profession',totalProperty:'total', root:'data', fields:['name']});
	// this.profCombo = new Ext.form.ComboBox({
	// 	hiddenName:'profession', store:this.profStore, valueField:'name', displayField:'name', minChars:2, forceSelection:true,
	// 	selectOnFocus:true, width:280, loadingText:'טוען...', emptyText:'חיפוש...', listClass:'rtl'//, //triggerAction:'all', allowBlank:false,
		
	// });

	
	
	this.tabs = new Ext.TabPanel({
		region: 'center', activeTab:0,
		baseCls: 'x-plain', style:'margin-top:5px',
		//deferredRender: false,
		plain: true,
		defaults: {xtype:'panel', layout:'form', baseCls: 'x-plain', bodyStyle:'border: 1px solid #8DB2E3; padding: 5px', autoHeight:true},
		items: [{
			title:'כללי', autoHeight:true,
            items: [{
				xtype:'panel', id:'private', layout:'form', baseCls:'x-plain',
				items:[{
					layout:'column', baseCls: 'x-plain',
					defaults: {xtype:'panel', columnWidth:0.33, layout:'form', baseCls:'x-plain', bodyStyle:'padding:5px 0', border:false},
					items:[{
						items: [
							{fieldLabel:'שם פרטי',   name:'first_name', id:'first_name', xtype:'textfield', tabIndex:14, width:120}
							// ,{fieldLabel:'מעסיק', xtype:'combo', tabIndex:17, /*store:employersList.concat(CFG.employers), hiddenName:'team_id',*/ hidden:(cat!='clients'), value:(this.grid.team_id ? this.grid.team_id : 0), listWidth:200, listAlign :'tr-br?', disabled:(this.grid.team_id ? true : false), width:120, listClass:'rtl', triggerAction:'all', forceSelection:true}
						]
					 },{
						items: [
							{fieldLabel:'שם משפחה', name:'last_name', id:'last_name', xtype:'textfield', tabIndex:15, width: 120},
							// {fieldLabel:'תאריך לידה', name:'birthday', tabIndex:18, xtype:'datefield', width: 120, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:true, value: (d ? d.birthday : '')}

							{layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', cls:'rtl', defaults: {layout:'form', baseCls: 'x-plain'}, style:'margin-top:5px',scope:this,
								items:[
									{xtype:'label', text:'תאריך לידה:', style:'padding-left:10px;'}
									,{fieldLabel:'תאריך לידה: ', name:'birthday', tabIndex:18, xtype:'datefield', width: 120, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:true, value: (d && d.birthday? d.birthday : '')
										,listeners:{change:function(v){
											// Ext.getCmp('client_age').setText('גיל: '+(v.value?(getAge(v.value)).toString()+'':''));
											// if(v.value)	Ext.getCmp('client_age').show();
											// else Ext.getCmp('client_age').hide();
										}}
										,scope:this}
									// ,{xtype:'label',name:'age',id:'client_age', style:'padding-left:5px;', text:'גיל: '+(d && d.birthday?(getAge(d.birthday.toString())).toString():'')+'',style:'margin-bottom:5px;',hidden:(!(d && d.birthday))}
								
								]
							}
						]
					 },{
						items: [
							{fieldLabel:'מספר ת.ז', name:'client_id', id:'client_id', tabIndex:16, xtype:'textfield', width:120,
								listeners:{
									change:function(f){
									check_passport(f.getValue());
									},scope:this
								}
							}
						]
					}
					]
				}]
			}
			,{
					layout:'column', baseCls: 'x-plain', defaults: {xtype:'fieldset'},
					items:[{
							title: 'טלפון/פקס', style:'margin-left:5px; padding:2px;', cls: 'rtl', width: 150, autoHeight: true ,columnWidth:0.5,
							items: [
							{
								layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'phoneSet0',
								items:[
									{xtype:'label', text:"סוג:"}
									,{xtype:'label', text:"מספר:"}
									,{xtype:'label', text:"הערות:", colspan:2}
									,{xtype:'combo', hiddenName:'tel_type0', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel0', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes0', xtype:'textfield', cls:'rtl', width:125}
									// ,{baseCls: 'x-plain', html:'<img src="skin/icons/add_call.png" onClick="Ext.getCmp(\'clientWindow\').addPhone()" 			ext:qtip="הוסף מספר טלפון">'}
									,{baseCls: 'x-plain', html:'&nbsp; <a href="javascript:void(0)" onClick="Ext.getCmp(\'clientWindow\').addPhone()"   		 ext:qtip=\'הוסף מספר טלפון\'> <img src="skin/icons/add_call.png"></a>'}						

								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet1',
								items:[
									{xtype:'combo', hiddenName:'tel_type1', width: 110, value: 3, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel1', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes1', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet2',
								items:[
									{xtype:'combo', hiddenName:'tel_type2', width: 110, value: 4, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield' ,name:'tel2',  cls:'ltr', width:100}
									,{name:'tel_notes2', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet3',
								items:[
									{xtype:'combo', hiddenName:'tel_type3', width: 110, value: 3, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel3', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes3', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet4',
								items:[
									{xtype:'combo', hiddenName:'tel_type4', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{name:'tel4', xtype:'textfield', cls:'ltr', width:100}
									,{name:'tel_notes4', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet5',
								items:[
									{xtype:'combo', hiddenName:'tel_type5', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield', name:'tel5', cls:'ltr', width:100}
									,{name:'tel_notes5', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet6',
								items:[
									{xtype:'combo', hiddenName:'tel_type6', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield', name:'tel6', cls:'ltr', width:100}
									,{name:'tel_notes6', xtype:'textfield', cls:'rtl', width:125}
								]
							},{
								layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'phoneSet7',
								items:[
									{xtype:'combo', hiddenName:'tel_type7', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore}
									,{xtype:'textfield', name:'tel7', cls:'ltr', width:100}
									,{name:'tel_notes7', xtype:'textfield', cls:'rtl', width:125}
								]
							}]
						},{
						title:'דוא"ל', style:'margin-right:5px; padding:2px', cls: 'rtl', autoHeight:true, columnWidth:0.5,
						items: [{
							layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', id:'emailSet0',
							items:[
								{xtype:'label', text:"סוג:"}
								,{xtype:'label', text:"כתובת:"}
								,{xtype:'label', text:"הערות:", colspan:2}
								,{xtype:'combo', width:65, listClass:'rtl', value:1, store:[[1,'עיקרי']], readOnly:true}
								,{xtype:'textfield', name:'email', cls:'ltr', width: 140, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
								,{xtype:'textfield', name:'email_notes', cls:'rtl', width:130}
								,{baseCls: 'x-plain', html:'&nbsp; <a href="javascript:void(0)" onClick="Ext.getCmp(\'clientWindow\').addEmail()"   		 ext:qtip=\'הוסף כתובת דוא"ל\'> <img src="skin/icons/add_email.png"></a>'}						
							]
						},{
							layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'emailSet1',
							items:[
								{xtype:'combo', hiddenName:'email_type1', width:65, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore}
								,{xtype:'textfield', name:'email1', cls:'ltr', width:140, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
								,{name:'email_notes1', xtype:'textfield', cls:'rtl', width:130}
							]
						},{
							layout:'table', layoutConfig:{columns:3}, baseCls:'x-plain', id:'emailSet2',
							items:[
								{xtype:'combo', hiddenName:'email_type2', width:65, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore}
								,{xtype:'textfield', name:'email2', cls:'ltr', width:140, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
								,{name:'email_notes2', xtype:'textfield', cls:'rtl', width:130}
							]
						}]
						}
					]
				}
				,{
					xtype:'fieldset', title:'כתובת', cls:'rtl', style: 'margin-top:3px; padding:2px', autoHeight: true,
					items: [
						{
							layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain',
							id:'adrSet0', defaults: {baseCls: 'x-plain', cls: 'rtl'},
							items:[
								{html:'סוג:'},{html:'כתובת/ת.ד.:'},{html:'עיר:'},{html:'מיקוד:', colspan: 2},
								{xtype:'combo', hiddenName:'adr_type0', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
								{name:'adr0', xtype:'textfield', width:372},
								{name:'city0', xtype:'textfield', width: 100},
								{name:'zip0', xtype:'textfield', width: 125},
								{baseCls: 'x-plain', html:'&nbsp;<a href="javascript:void(0)" onClick="Ext.getCmp(\'clientWindow\').addAddress()" ext:qtip=\'הוסף כתובת\'><img src="skin/icons/add_addr.png"></a>'}
							]
						},{
							layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet1', defaults: {baseCls: 'x-plain', cls: 'rtl'},
							items:[
								{xtype:'combo', hiddenName:'adr_type1', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
								{name:'adr1', xtype:'textfield', width:372},
								{name:'city1', xtype:'textfield', width: 100},
								{name:'zip1', xtype:'textfield', width: 125}
							]
						},{
							layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet2', defaults: {baseCls: 'x-plain', cls: 'rtl'},
							items:[
								{xtype:'combo', hiddenName:'adr_type2', width: 110, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
								{name:'adr2', xtype:'textfield', width:372},
								{name:'city2', xtype:'textfield', width: 100},
								{name:'zip2', xtype:'textfield', width: 125}
							]
						}
					]
				}
			]
		}
		
		
	]});
	
	// if (/*CFG.user.tax_module &&*/ cat=='clients') this.tabs.insert(3, new ClientTaxTab(d)); 
	
	this.draw_family_fields =function(number_of_familyMember,d){
		
		this.getKesherHafokhValue=function (client_details){
			if(isEmpty(client_details)) return '';
			if(client_details.link_type && client_details.client_name)	return 	client_details.link_type+' של '+client_details.client_name;
			else return '';
		};
	
		var family_Fields_arr=[];
		for(var i=0;i<number_of_familyMember;i++){
			var clientCombo = new ClientCombo({width:95,hiddenName:'f_name'+i,listWidth:200,  index:i ,listAlign :'tr-br?',forceSelection:false,pageSize:8,value:(d && d?d['f_name'+i]:'')});

			clientCombo.on('select', function(combo,record){
				this.tabs.find('name','f_linked_client_id'+combo.index)[0].setValue(record.data.id);
				this.tabs.find('name','f_tz'+combo.index)[0].setValue(record.data.passport);
				this.tabs.find('name','f_bdate'+combo.index)[0].setValue(record.data.birthday);
				this.tabs.find('name','f_tel'+combo.index)[0].setValue(record.data.tel0);
				this.tabs.find('name','f_fax'+combo.index)[0].setValue(record.data.fax);
				this.tabs.find('name','f_email'+combo.index)[0].setValue(record.data.email);
				this.tabs.find('name','f_license_num'+combo.index)[0].setValue(record.data.license_num);
				this.tabs.find('name','f_license_date'+combo.index)[0].setValue(record.data.license_date);
				this.tabs.find('name','f_sex'+combo.index)[0].setValue(record.json.sex);
						
				var kesher_hafukh='';
				var link_type=this.form.find('hiddenName','f_type'+combo.index)[0].getValue();

				if(link_type && this.form.find('hiddenName','f_type'+combo.index)[0].getStore().query('name', link_type,false).items[0]){
					var client_sex=(d.sex && d.sex=='2'?'female':'male');
					var linked_client_sex=record.json.sex;
					var  kesher_hafukh = this.form.find('hiddenName','f_type'+combo.index)[0].getStore().query('name', link_type,false).items[0].data['kesher_'+linked_client_sex+'_to_'+client_sex];
				}
				if(!kesher_hafukh ){									
					var client_name= d.first_name +' '+ d.last_name;

					kesher_hafukh = this.getKesherHafokhValue({link_type:link_type,client_name:client_name})
					
					};
				this.form.find('name','f_kesher_hafokh'+combo.index)[0].setValue(kesher_hafukh);
			
			}, this);
			
			clientCombo.on('change', function(combo){
				if(combo.getValue()==combo.getRawValue()){
					this.form.find('name','f_linked_client_id'+combo.index)[0].setValue(0);
					this.form.find('name','f_kesher_hafokh'+combo.index)[0].setValue('');
				}
			}, this);

		
			family_Fields_arr.push(
				{layoutConfig: {columns:10}, id:'famSet'+i,
					items:[
						// {name:'f_type'+i, xtype:'combo', width:55, value: 0, mode:'local', listClass:'rtl', editable:true, triggerAction:'all', forceSelection:false, store:famStore},
						{xtype:'combo', hiddenName:'f_type'+i, width:55, index:i , listAlign:'tr-br?',emptyText:'-בחר-',value:(d && d?d['f_type'+i]:''),
							mode:'local', listClass:'rtl', editable:true, triggerAction:'all', forceSelection:false,
							displayField:'name', valueField:'name', 
							tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right;">{name}</div></tpl>'),
							itemSelector: 'div.search-item',
							displayFieldTpl:'<div class="search-item" style="padding-right:5px; text-align:right;">{name}</div>',
							store:new Ext.data.JsonStore({
								data:famStore,
								fields:['id','name','kesher_male_to_male','kesher_male_to_female','kesher_female_to_female','kesher_female_to_male'],
								sortInfo:{field:'id', direction:"ASC"}
							}),
							listeners:{
							select:function(combo,record){
								var client_sex=(d.sex && d.sex=='2'?'female':'male');
								var linked_client_sex=this.form.find('name','f_sex'+combo.index)[0].getValue();
								var kesher_hafukh='';

								kesher_hafukh = record.data['kesher_'+linked_client_sex+'_to_'+client_sex];
								if(!kesher_hafukh ) {
									var client_name= d.first_name +' '+ d.last_name;
									kesher_hafukh = this.getKesherHafokhValue({link_type:combo.getValue(),client_name: client_name});
								}
								this.form.find('name','f_kesher_hafokh'+combo.index)[0].setValue(kesher_hafukh);
							},
							change:function(combo){
								if(!this.form.find('hiddenName','f_type'+combo.index)[0].getStore().query('name', link_type,false).items[0]){
									var client_name= d.first_name +' '+ d.last_name;
									var kesher_hafukh = this.getKesherHafokhValue({link_type:combo.getValue(),client_name: client_name});
									this.form.find('name','f_kesher_hafokh'+combo.index)[0].setValue(kesher_hafukh);
								}

							},
							scope:this}
						},
						clientCombo,
						{name:'f_tz'+i, xtype:'textfield', width: 60,value:(d && d?d['f_tz'+i]:'')},
						{name:'f_bdate'+i,xtype:'datefield', width: 78, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:true,value:(d && d?d['f_bdate'+i]:'')},
						{name:'f_tel'+i,  xtype:'textfield', width: 70,value:(d && d?d['f_tel'+i]:'')},
						{name:'f_fax'+i,  xtype:'textfield', width: 70,value:(d && d?d['f_fax'+i]:'')},
						{name:'f_email'+i,  xtype:'textfield', width: 115,value:(d && d?d['f_email'+i]:'')},
						{name:'f_license_num'+i, xtype:'textfield', width: 70,value:(d && d?d['f_license_num'+i]:'')},
						{name:'f_license_date'+i, xtype:'datefield', width: 78, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:true,value:(d && d?d['f_license_date'+i]:'')},
						{name:'f_notes'+i, xtype:'textfield', width: 70,value:(d && d?d['f_notes'+i]:'')},
						{name:'f_linked_client_id'+i, xtype:'hidden',value:(d && d?d['f_linked_client_id'+i]:'')},
						{name:'f_kesher_hafokh'+i, xtype:'hidden',value:(d && d?d['f_kesher_hafokh'+i]:'')},
						{name:'f_sex'+i, xtype:'hidden',value:(d && d?d['f_sex'+i]:'')}
					]
				}
			);
		}
		family_Fields_arr.push({name:'number_of_familyMember', xtype:'hidden',value:number_of_familyMember});
		
		// this.tabs.find('name','familyfields')[0].add(family_Fields_arr);
		
		// for(var i=0;i<number_of_familyMember;i++){

		// 	var kesher_hafukh='';
		// 	// var link_type=this.form.find('hiddenName','f_type'+i)[0].getValue();
	
		// 	if(link_type && this.form.find('hiddenName','f_type'+i)[0].getStore().query('name', link_type,false).items[0]){
		// 		var client_sex=(d.sex && d.sex=='2'?'female':'male');
		// 		var linked_client_sex=this.form.find('name','f_sex'+i)[0].getValue();
		// 		var  kesher_hafukh = this.form.find('hiddenName','f_type'+i)[0].getStore().query('name', link_type,false).items[0].data['kesher_'+linked_client_sex+'_to_'+client_sex];
		// 	}
		// 	if(!kesher_hafukh )  kesher_hafukh = this.getKesherHafokhValue({link_type:link_type,client_name: d['f_name'+i]});

		// 	this.form.find('name','f_kesher_hafokh'+i)[0].setValue(kesher_hafukh);
			
		// }
	};


	
	this.tabs.on('tabchange', function(tabPanel, tab) {
		// if (Ext.getCmp('clType').getValue()==1) {Ext.getCmp('job').show();}
		// else {Ext.getCmp('job').hide();}
		this.tabs.doLayout(); this.syncShadow();
	},this);
	
	this.form = new Ext.form.FormPanel({
		id:'clientForm', fileUpload:true, labelWidth:75, autoHeight:true, baseCls: 'x-plain', valid:true,
		items:[{
			layout:'table', layoutConfig:{columns:8}, baseCls:'x-plain', cls:'rtl', defaults: {layout:'form', baseCls: 'x-plain'},
	
		}, this.tabs]
	 });
	
	ClientWindow.superclass.constructor.call(this, {
		title:'עידכון פרטי לקוח',
		layout:'fit', id:'clientWindow', modal:true,
		width:860, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons:[{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('show', function(){
		// Ext.getCmp('phoneSet1').hide(); Ext.getCmp('phoneSet2').hide(); 
		Ext.getCmp('phoneSet3').hide(); Ext.getCmp('phoneSet4').hide(); Ext.getCmp('phoneSet5').hide(); Ext.getCmp('phoneSet6').hide(); Ext.getCmp('phoneSet7').hide();
		Ext.getCmp('emailSet1').hide(); Ext.getCmp('emailSet2').hide();
		Ext.getCmp('adrSet1').hide(); Ext.getCmp('adrSet2').hide();
		// Ext.getCmp('groupSet1').hide(); Ext.getCmp('groupSet2').hide();
		// Ext.getCmp('business').hide();
		
		if(!this.client_id){
			this.tabs.unhideTabStripItem(1);
			this.tabs.hideTabStripItem(2);
		}
		// if (cat!='clients') {
			// this.tabs.hideTabStripItem(1);		
		// }
	
		if (this.client_id) this.formLoad(this);
		// else if (d) {this.draw_family_fields(7,'');	 this.form.form.setValues(d);}
		
		this.tabs.setActiveTab(d.aTab ? d.aTab:0 );
		Ext.getCmp('clType').focus(true,100);
		this.syncShadow();
	});

	this.formLoad=function(d) {
		// this.getEl().mask('...טוען');
		console.log(d);
		Ext.getCmp('first_name').setValue(d.client);
		Ext.getCmp('client_id').setValue(d.passport);
		// this.form.load({
		// 	url: "/?m=crm/crm_f&f=load_client_data&client_id="+this.client_id,
		// 	params:{load_tax_folder:(/*CFG.user.tax_module && */cat=='clients'),/*load_mortgage:(m=='mortgage/clients' && cat=='clients')*/},
		// 	reader: new Ext.data.JsonReader({
		// 		root: 'data',
		// 		successProperty: 'success',
		// 		totalProperty: 'total'
		// 	}),
		// 	success: function(f,a){
		// 		var d=a.result.data;
				
		// 		var number_of_familyMembers=(d && d.number_of_family && d.number_of_family>7?d.number_of_family:7);
		// 		this.draw_family_fields(number_of_familyMembers,d);
		
		// 		for (var n=1; n<8; n++) if (d['tel'+n]) {Ext.getCmp('phoneSet'+n).show();phone_cnt=n+1;}
		// 		for (var n=1; n<3; n++) if (d['email'+n]) {Ext.getCmp('emailSet'+n).show();email_cnt=n+1;}
		// 		for (var n=1; n<3; n++) if (d['city'+n] || d['zip'+n]!=0 || d['adr'+n]) {Ext.getCmp('adrSet'+n).show();adr_cnt=n+1;}
		// 		for (var n=1; n<3; n++) if (d['client_group'+n]) {Ext.getCmp('groupSet'+n).show();group_cnt=n+1;}
		// 		if (d['client_photo']) clientPhoto=d['client_photo'];
		// 		// if (d['c_type']==2) {
		// 		// 	this.tabs.hideTabStripItem(1);
		// 		// 	// Ext.getCmp('job').hide();
		// 		// 	// Ext.getCmp('license').hide();
		// 		// 	// Ext.getCmp('private').hide();
		// 		// 	// Ext.getCmp('business').show();
		// 		// }
		// 		// else{
		// 		// 	this.tabs.hideTabStripItem(2);

		// 		// }
		// 		this.syncShadow();
		// 		this.getEl().unmask();

		// 	},
		// 	failure: function(){ this.getEl().unmask(); },
		// 	scope:this
	 //   	});
	}

	this.submitForm = function(overwrite) {
		clType = Ext.getCmp('clType').getValue();
		if (clType==1) {//פרטי
			first_name = Ext.getCmp('first_name'); 
			last_name = Ext.getCmp('last_name'); 
			if (!first_name.getValue() && !last_name.getValue()) {
				last_name.markInvalid('חובה למלא שדה זה'); return;
			}
			passport = Ext.getCmp('client_id'); 
			console.log(passport);
			console.log(first_name);
			console.log(last_name);
			if (!passport.getValue() && cat=='clients') {
				passport.markInvalid('חובה למלא שדה זה'); return;
			}
		
		}else {//חברה
			bname = Ext.getCmp('bname'); if (!bname.getValue()) {bname.markInvalid('חובה למלא שדה זה'); return;}
			hp = Ext.getCmp('hp'); if (!hp.getValue() && cat=='clients') {hp.markInvalid('חובה למלא שדה זה'); return;}
		}
		
		
		
		/*this.form.valid used in check_passport */
		if(!this.form.form.isValid() || !this.form.valid ) return;
		if(this.grid.team_id) this.form.find('hiddenName','team_id')[0].setDisabled(false);
		
		var number_of_familyMember=7;

		if(this.form.find('name','c_type')[0].getValue()==1){
			number_of_familyMember= this.form.find('name','number_of_familyMember')[0].getValue();
		
		// if(number_of_familyMember) for(var i=0; i<number_of_familyMember; i++){
			// params['f_name'+i] = this.form.find('hiddenName','f_name'+i)[0].getRawValue();
		}
		
		// params['number_of_familyMember'] =number_of_familyMember;
		// }

		// params['client_group'] = this.form.find('name','client_group')[0].getValue();
		// params['client_group1'] =this.form.find('name','client_group1')[0].getValue();
		// params['client_group2'] =this.form.find('name','client_group2')[0].getValue();
		// Dump(params);
		this.form.form.submit({
			url:"/?m=crm/clients_f&f=add_client&id="+this.client_id,
			params:
			{
				// params,
				// f_name0:this.form.find('hiddenName','f_name0')[0].getRawValue(),
				// f_name1:this.form.find('hiddenName','f_name1')[0].getRawValue(),
				// f_name2:this.form.find('hiddenName','f_name2')[0].getRawValue(),
				// f_name3:this.form.find('hiddenName','f_name3')[0].getRawValue(),
				// f_name4:this.form.find('hiddenName','f_name4')[0].getRawValue(),
				// f_name5:this.form.find('hiddenName','f_name5')[0].getRawValue(),
				// f_name6:this.form.find('hiddenName','f_name6')[0].getRawValue(),
				number_of_familyMember:number_of_familyMember,
				client_group:this.form.find('name','client_group')[0].getValue(),
				client_group1:this.form.find('name','client_group1')[0].getValue(),
				client_group2:this.form.find('name','client_group2')[0].getValue()
			},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			scope:this,
			success: function(f,a){
				this.close();
				var data = Ext.decode(a.response.responseText);
				
				if (d.module=='collection') d.callback(data);
				else if (!this.client_id && this.grid.openClient) {grid.store.reload();this.grid.openClient(data);}
				else if  (this.grid.caller=='claim_info' || this.grid.caller=='client_info') this.grid.load();
				else if (grid.store) grid.store.reload();
				else window.location.reload();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result);}
		});
	}
};
Ext.extend(ClientWindow, Ext.Window, {});


ClientTaxTab = function(d) {
	ClientTaxTab.superclass.constructor.call(this, {
		title:'תיק מיסוי', labelAlign:'top',
		items:[{
			layout:'table', layoutConfig:{columns:7}, baseCls:'x-plain', defaults: {baseCls: 'x-plain', cls: 'rtl'}, style:'margin-bottom:7px;',
			items:[
				{xtype:'label', text:'תאריך הגעה:', style:'text-align:right;'},
				{name:'taxing_start', xtype:'datefield', width: 120, format:'d/m/Y', cls:'ltr', vtype:'daterange', value: (d ? d.taxing_start : '')},
				{xtype:'label', text:'מקור הגעה:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'tax_client_sourse', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_client_sourse_store
				},
				{xtype:'label', text:'סטאטוס:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'tax_client_status', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, colspan:2, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_client_statuses_store
					, listeners:{
						change: function(){this.find('name','date_status_update')[0].setValue(new Date())},
						scope:this
					}, scope:this
				},
				{xtype:'label', text:'פקיד שומה:', style:'text-align:right;'},
				{xtype:'combo', hiddenName:'tax_assessor', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_assessors_store
				},
				{xtype:'label', text:'פקיד מטפל במס הכנסה:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'tax_clerck', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_clerks_store
				},
				{xtype:'label', text:'סוכן מיסוי:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'tax_agent', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, colspan:2, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_agents_store
				},
				{xtype:'label', text:'תוכנית:', style:'text-align:right;'},
				{xtype:'textfield', name:'tax_programm'},
				{xtype:'label', text:'מתווך מיסוי:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'tax_broker', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_brokers_store
				},
				{xtype:'label', text:'סניף:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'tax_branch', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, colspan:2, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_branches_store
				},
				{xtype:'label', text:'סוג תיק:', style:'text-align:right;'},
				{xtype:'combo', hiddenName:'tax_folder_type', emptyText:'- בחר -', valueField:'id', displayField:'name', mode:'local', width:120, listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true
					, store:tax_folder_types_store
				},
				{xtype:'label', text:'מיקום תיק:', style:'text-align:right; padding-right:5px;'},
				{xtype:'textfield', name:'tax_folder_place'},
				{xtype:'label', text:'עמלה:', style:'text-align:right; padding-right:5px;'},
				{xtype:'numberfield', name:'tax_commission', width:103},{xtype:'label', text:'%', style:'text-align:right;'},
				{xtype:'label', text:'ועדה רפואית:', style:'text-align:right;'},
				{xtype:'combo', hiddenName:'medical_commission', emptyText:'- בחר -', width:120, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:['לא','בתהליך','גמורה'],value:(d ? d.medical_commission : '')},
				{xtype:'label', text:'תאריך הגשת דוח אחרון:', style:'text-align:right; padding-right:5px;'},
				{name:'date_last_report', xtype:'datefield', width:120, format:'d/m/Y', cls:'ltr', vtype:'daterange', value: (d ? d.date_last_report : '')},
				{xtype:'label', text:'כולל מע"מ:', style:'text-align:right; padding-right:5px;'},
				{xtype:'combo', hiddenName:'vat_included', emptyText:'- בחר -', valueField:'id', displayField:'val', width:120, colspan:2, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:[[0,'לא'],[1,'כן']]},
				{xtype:'label', text:'מיוצג:', style:'text-align:right;'},
				{xtype:'combo', hiddenName:'represented', emptyText:'- בחר -', value:'', width:120, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:['כן','לא','בוטל','דני']},
				{xtype:'label', text:'תאריך רישום סטאטוס אחרון:', style:'text-align:right; padding-right:5px;'},
				{name:'date_status_update', xtype:'datefield', width:120, colspan:3, format:'d/m/Y', cls:'ltr', vtype:'daterange', value: (d ? d.date_status_update : '')}
			]}
			,{fieldLabel:'הערות מיסוי', xtype:'textarea', name:'tax_notes', width:'100%'}
			,{xtype:'hidden', name:'tax_client_status_old'}
		]
	});
}
Ext.extend(ClientTaxTab, Ext.Panel, {});

if (Ms['clients']) 
	Ms['clients'].run = function(cfg){
	var m=Ms['clients'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var  clientsGrid = new ClientsGrid();
		win = Crm.desktop.createWindow({width:600, height:700
			,items:clientsGrid
		}, m);
	}
		
	win.on('show', function() {
		if (clientsGrid.store.getCount()>0) clientsGrid.startEditing(clientsGrid.store.getCount()-1,0);
	});	
	win.show();
};
