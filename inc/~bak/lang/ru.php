<?
$LNG['ln']=$ln;	# current language

# Common
$LNG['Home']	= '�������';
$LNG['Add']		= '��������';
$LNG['AddNew']	= '��������';
$LNG['Edit']	= '�������������';
$LNG['Delete']	= '�������';
$LNG['Save']	= '���������';
$LNG['Cancel']	= '������';
$LNG['Title']	= '���������';
$LNG['Date']	= '����';
$LNG['Added']	= '���������';
$LNG['Language']= '����';
$LNG['Weight']	= '���';
$LNG['Align']	= '�������';
$LNG['File']	= '����';
$LNG['Active']	= '��������';
$LNG['ActiveF']	= '��������';
$LNG['Content']	= '�������';
$LNG['Category']= '������';
$LNG['Preview']	= '������������';
$LNG['All']		= '���';
$LNG['Select']	= '�������';
$LNG['Are_you_sure']='�� �������?';
$LNG['read_more']	= '�������� &gt;&gt;&gt;';
$LNG['Short_Text']	= '�������� �����';
$LNG['Full_Text']	= '������ �����';


$LNG['all_news']		= '��� ������� &gt;&gt;&gt;';
$LNG['all_announces']	= '��� ������ &gt;&gt;&gt;';


# Block names
$LNG['Pages']	= '��������';
$LNG['Block']	= '����';
$LNG['News']	= '�������';
$LNG['Announces']='���� ������';


# Login
$LNG['Username']	= '��� ������������';
$LNG['Password']	= '������';
$LNG['Login']		= '����';
$LNG['Logout']		= '�����';
# Login errors
$LNG['Missing_username_or_password']	= '���������� ������� ��� ������������ � ������.';
$LNG['Username_or_password_not_valid']	= '��� ������������ ��� ������ �� �����,<br>���������� ��� ���.';
$LNG['You_are_logged_in_as_admin']		= '�� ����� ��� �������������';


# Pages
$LNG['Filter']		= '������';
$LNG['New_page']	= '����� ��������';
$LNG['Pages_List']	= '������ �������';
$LNG['New_Page']	= '����� ��������';
$LNG['Edit_Page']	= '������������� ��������';
$LNG['Type']		= '���';
$LNG['Image']		= '��������';
$LNG['Image_align']	= '������� ��������';
$LNG['Preview_only_warning'] = '��������������� ��������, ��������� �� ���������!';


# Announces
$LNG['Edit_announce']	= '������������� �������';
$LNG['Add_announce']	= '�������� �������';


# Search
$LNG['Search']			= '������';
$LNG['Search_on_site']	= '����� �� �����';
$LNG['Found_in_pages']	= '������� � ������� ��������';
$LNG['Found_in_news']	= '������� � ������� �������';
$LNG['Nothing_found']	= '�� ���������� �������� ������ �� �������.';

# Blocks
$LNG['Select_block_note']	= '(�������� ����������� ���� �� ������.<br>��� ��������� ���� ����� ��������������)';

# Backup
$LNG['Backup_Database']	= '��������� ���� ������';


$LNG['']	= '';
$LNG['']	= '';
$LNG['']	= '';

?>