<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
 
	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end FROM users ORDER BY fullName",'id');
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;
	}
	
	#Load Banks & Branches
	$banks=sql2array("SELECT bank_code, bank_name FROM acc_banks GROUP BY bank_code", 'bank_code',0,0,'rafael');
	// if ($banks) foreach ($banks as $bank) $banks_ar[]="['$bank[bank_code]','".jsEscape("$bank[bank_code] - $bank[bank_name]")."']";
	if ($banks) foreach ($banks as $bank) $banks_names[]="'".jsEscape("$bank[bank_code] - $bank[bank_name]")."'";
	
	#Load Client Bank Accounts
	$Accounts=sql2array("SELECT id, bank, branch, number, bank_name FROM acc_bank_accounts LEFT JOIN acc_banks ON (acc_banks.bank_code=bank  AND acc_banks.branch_code=branch)", 'id');
	if ($Accounts) foreach ($Accounts as $account) {
		$bankAccounts[]="{accountId:$account[id], iconCls:'safebox', text:'".jsEscape("$account[bank_name]: $account[bank]-$account[branch]-$account[number]")."'}";
		$bankAccountsAr[]="[$account[id], '".jsEscape("$account[bank_name]: $account[number]")."']";
	}
	#Load Units & WareTypes
	$wareunits=sql2array("SELECT * FROM wareunits GROUP BY name", 'id','name');
	if ($wareunits) foreach ($wareunits as $id=>$name) $wareunits_ar[$id]="['$id','".jsEscape($name)."']"; else $wareunits_ar=array();

	$waretypes=sql2array("SELECT * FROM waretypes GROUP BY name", 'id','name');
	if ($waretypes) foreach ($waretypes as $id=>$name) $waretypes_ar[$id]="['$id','".jsEscape($name)."']"; else $waretypes_ar=array();
	
	for ($i=date('Y'); $i>=date('Y')-10; $i--) $years .= ($years?',':'')."$i";

	?>
	var user={
		user_id:'<?=$SVARS['user_id']?>'
		,parent_id:'<?=$SVARS['parent_id']?>'
		,type:'<?=$SVARS['user_type']?>'
		,username:'<?=jsEscape($SVARS['username'])?>'
		,name:'<?=jsEscape($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])?>'
		,tel:'<?=jsEscape($SVARS['tel'])?>'
		,fax:'<?=jsEscape($SVARS['fax'])?>'
		,fax_active:'<?=(int)$SVARS['fax_active']?>'
		,permissions:'<?=$SVARS['user_permissions']?>'
	};	
	
	var vat_period=<?=$SVARS['company']['vat_period'];?>;
	var date_parts = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/.exec('<?=$SVARS['company']['current_vat_period'];?>');
	var current_vat_period=new Date(date_parts[1],date_parts[2]-1,date_parts[3]);
	
	var wareunits = [<?=implode(',',$wareunits_ar);?>];
	var waretypes = [<?=implode(',',$waretypes_ar);?>];
	var wareunits_obj = <?=array2json($wareunits);?>;
	var waretypes_obj = <?=array2json($waretypes);?>;
	
	var users=[<?=$users;?>];
	var agents=[<?=$agents;?>];
	var users_obj=<?=array2json($users_ar);?>;
	/*var banks = [<?//=implode(',',$banks_ar)?>];*/
	var banks_names = [<?=implode(',',$banks_names)?>];
	var bankAccounts = [<?=($bankAccounts ? implode(',',$bankAccounts):'')?>];
	var bankAccountsAr = [[0,'לא הופקד'],[-1,'הכל']<?=($bankAccountsAr ? ','.implode(',',$bankAccountsAr):'')?>];
	
	var years = [<?=$years;?>];

	Ext.ux.Loader.load([
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/clients/clients.js?<?=filemtime('modules/clients/clients.js');?>',
			'modules/common/accounting.js?<?=filemtime('modules/common/accounting.js');?>',
			'modules/config/expensesgroups.js?<?=filemtime('modules/config/expensesgroups.js');?>',
			'modules/config/incomegroups.js?<?=filemtime('modules/config/incomegroups.js');?>',
			'skin/extjs/ux/treegrid/treegrid.css?<?=filemtime('skin/extjs/ux/treegrid/treegrid.css');?>',
			'modules/accounting/rep_invoices.js?<?=filemtime('modules/accounting/rep_invoices.js');?>',
			'modules/accounting/rep_expenses.js?<?=filemtime('modules/accounting/rep_expenses.js');?>',
			'modules/accounting/rep_cash.js?<?=filemtime('modules/accounting/rep_cash.js');?>',
			'modules/accounting/rep_vat.js?<?=filemtime('modules/accounting/rep_vat.js');?>',
			'modules/accounting/rep_ProfitLoss.js?<?=filemtime('modules/accounting/rep_ProfitLoss.js');?>',
			'modules/accounting/rep_real.js?<?=filemtime('modules/accounting/rep_real.js');?>',
			'modules/accounting/accounting.js?<?=filemtime('modules/accounting/accounting.js');?>'
			
		], function(){Ms.accounting.run();}
	);
	<?
	/*
			'skin/extjs/ux/treegrid/TreeGridSorter.js?<?=filemtime('skin/extjs/ux/treegrid/TreeGridSorter.js');?>',
			'skin/extjs/ux/treegrid/TreeGridColumnResizer.js?<?=filemtime('skin/extjs/ux/treegrid/TreeGridColumnResizer.js');?>',
			'skin/extjs/ux/treegrid/TreeGridNodeUI.js?<?=filemtime('skin/extjs/ux/treegrid/TreeGridNodeUI.js');?>',
			'skin/extjs/ux/treegrid/TreeGridLoader.js?<?=filemtime('skin/extjs/ux/treegrid/TreeGridLoader.js');?>',
			'skin/extjs/ux/treegrid/TreeGridColumns.js?<?=filemtime('skin/extjs/ux/treegrid/TreeGridColumns.js');?>',
			'skin/extjs/ux/treegrid/TreeGridSorter.js?<?=filemtime('skin/extjs/ux/treegrid/TreeGridSorter.js');?>',

	*/
}
//————————————————————————————————————————————————————————————————————————————————————
function f_invoice_data() {
 global $CFG, $SVARS;
	$sql = "SELECT num, VAT FROM acc_docs WHERE type LIKE 'invoice%' ORDER by id DESC LIMIT 1;";
	$last_invoice = sql2array($sql,0,0,1);
	echo "{success:true, data:".array2json($last_invoice)."}";
}
//————————————————————————————————————————————————————————————————————————————————————
/*
function f_load_data() {
 global $CFG, $SVARS;
	
	if (!(int)$_REQUEST['period']) $_REQUEST['period']=1;
	$month = date('m');
	$income = sql2array("SELECT SUM(sum) AS sum, DATE_FORMAT(date, '%m') AS month FROM acc_docs WHERE type LIKE 'invoice%' AND date>'".(date("Y")-1)."-21-31' GROUP BY month",'month','sum');
	$cash	= sql2array("SELECT SUM(IF (credit_class=100, -sum, sum)) as balance, IF (credit_class=100, credit_card, debt_card) as card FROM acc_operations WHERE (credit_class=100 OR debt_class=100) AND date<='".date('Y-m-d')."' GROUP BY card",'card','balance');
	
	if ($income) {
		foreach($income as $m=>$sum) {
			$yearIncome+=$sum;
			if ($m>($month-$_REQUEST['period'])) $lastIncome+=$sum;
			if ($m>($month-$_REQUEST['period']*2)) $prevIncome+=$sum;
		}
	}
	$prevIncome = $prevIncome - $lastIncome;
	
	if ($ptype=='מזומן') $cashCard=1001;
	elseif ($ptype=='שיק') $cashCard=1002;
	elseif ($ptype=='אשראי' AND $bank=='וויזה') $cashCard=1004;
	elseif ($ptype=='אשראי' AND $bank=='ישראכרט/מאסטרכארד') $cashCard=1005;
	elseif ($ptype=='אשראי' AND $bank=='דיינרס') $cashCard=1006;
	elseif ($ptype=='אשראי' AND $bank=='אמריקן אקספרס') $cashCard=1007;
	elseif ($ptype=='אשראי' AND $bank=='לאומי קארד') $cashCard=1008;
	else $cashCard=1000;
	
	$cashCash=($cash[1000]+$cash[1001]);
	$cashCheque=$cash[1002];
	$cashCredit=($cash[1004]+$cash[1005]+$cash[1006]+$cash[1007]+$cash[1008]);
	$cashTotal=$cashCash+$cashCheque+$cashCredit;
	echo "{success:true, data:{incomeLast:'$lastIncome',incomePrev:'$prevIncome',incomeYear:'$yearIncome',cashCash:'$cashCash',cashCheque:'$cashCheque',cashCredit:'$cashCredit',cashTotal:'$cashTotal'}}";
}
*/
//————————————————————————————————————————————————————————————————————————————————————
function f_load_data() {
 global $SVARS;
	if (!(int)$_REQUEST['vat_period']) 		$_REQUEST['vat_period']=($SVARS['company']['vat_period'] ?  $SVARS['company']['vat_period'] : 1);
	if (!$_REQUEST['current_vat_period']) 	$_REQUEST['current_vat_period']=($SVARS['company']['current_vat_period'] ?  $SVARS['company']['current_vat_period'] : '0000-00-00');
	
	if($_REQUEST['current_vat_period']=='0000-00-00'){
		echo "{success:true, data:{"
			."incomeLast:'0',incomePrev:'0',incomeYear:'0',incomeAverage:'0'"
			.",expenseCurr:'0',expensePrev:'0',expenseYear:'0',expenseAver:'0',expensePercent:'0%'"
			.",profitLast:'0',profitPrev:'0',profitYear:'0',profitAverage:'0',profitPercent:'0%'"
			.",profitLastColor:'000000',profitPrevColor:'000000',profitYearColor:'000000',profitAverageColor:'000000',profitPercentColor:'000000'"
			.",currentCash:'0',nextCash:'0',futureCash:'0',totalCash:'0'"
			.",new_invoices:'0'"
			.",current_vat_period:'0000-00-00'"
			.",current_vat_period_str:'-'"
			.",prev_vat_period_str:'-'"
		."}}";
	}
	else{
		$month = str2time($_REQUEST['current_vat_period'],'n'); if ($_REQUEST['vat_period']==2 AND ($month % 2)) $month++;
		$year = str2time($SVARS['company']['current_vat_period'],'Y');
		
		// echo "/* $_REQUEST[vat_period]: $_REQUEST[current_vat_period]: $month/$year */";

		# הכנסות
		$income  = sql2array(
			"SELECT DATE_FORMAT(date, '%m') AS month, 
			(SUM(IF(TYPE IN ('invoice_credit','invoice_accept'), sum, 0))-SUM(IF(TYPE IN ('invoice_debt'), sum, 0))-SUM(IF(TYPE IN ('invoice_credit','invoice_accept'), vat_sum, 0))) 
			AS sum
			FROM acc_docs
			WHERE acc_docs.type IN ('invoice_debt','invoice_credit','invoice_accept') AND date > '".($year-1)."-12-31'
			GROUP BY month",'month','sum');
		
		
		
				
		$lastIncome=$prevIncome=$yearIncome=$averageIncome=0;
		if ($income) {
			foreach($income as $m=>$sum) {
				$yearIncome+=$sum;
				if ($m>($month-$_REQUEST['vat_period'])) $lastIncome+=$sum;
				if ($m>($month-$_REQUEST['vat_period']*2)) $prevIncome+=$sum;
			}
		}
		$prevIncome = $prevIncome - $lastIncome;
		if ($m) {
			$averageIncome=round($yearIncome/$m,2);
		}


		# הוצאות
		$year = str2time($SVARS['company']['current_vat_period'],'y');

		$expense = sql2array(
			"SELECT rep_period,SUM(sum) as sum 
			FROM acc_docs
		LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id 
			WHERE acc_docs.type = 'expense' AND acc_expense_ext.expense_type!=2 AND rep_period > '".($year-1)."12' AND rep_period < '".($year+1)."01'
			GROUP BY rep_period",'rep_period','sum');
		
		$expenseCurr=$expensePrev=$expenseYear=$expenseAver=0;
		if ($expense) {
			foreach($expense as $rep_period=>$sum) {
				$expenseYear+=$sum;
				
				$rep_period_month=substr($rep_period,2,3);
					
				if($_REQUEST['vat_period']==1){
					if((int)str2time($_REQUEST['current_vat_period'],'n')-(int)$rep_period_month==0) $expenseCurr+=$sum;
					if((int)str2time($_REQUEST['current_vat_period'],'n')-(int)$rep_period_month==1) $expensePrev+=$sum;
				}
				if($_REQUEST['vat_period']==2){
					if((int)str2time($_REQUEST['current_vat_period'],'n')-(int)$rep_period_month==0 OR (int)str2time($_REQUEST['current_vat_period'],'n')-(int)$rep_period_month==-1) $expenseCurr+=$sum;
					if((int)str2time($_REQUEST['current_vat_period'],'n')-(int)$rep_period_month==1 OR (int)str2time($_REQUEST['current_vat_period'],'n')-(int)$rep_period_month==2) $expensePrev+=$sum;
				}
			}
		}
		
		if($_REQUEST['vat_period']==1) $expenseAver=round($expenseYear/(int)str2time($_REQUEST['current_vat_period'],'n'),2);
		if($_REQUEST['vat_period']==2) $expenseAver=round($expenseYear/((int)str2time($_REQUEST['current_vat_period'],'n')+1),2);
		
		# תנועות קופה
		$month = date('Ym');
		
		$payments = sql2array("SELECT ptype, SUM(acc_accept_rows.sum) AS sum, DATE_FORMAT(acc_accept_rows.date, '%Y%m') AS month "
		."FROM acc_accept_rows "
		."INNER JOIN acc_docs ON (acc_docs.type IN ('accept','invoice_accept') AND acc_docs.id=acc_accept_rows.accept_id ) "
		."WHERE acc_accept_rows.ptype IN ('מזומן','שיק','אשראי') AND payment_status IN (0,3) GROUP BY ptype, month");
		
		echo "/*<div dir=ltr align=left><pre>". print_r($payments,1)."</pre></div>*/";
		
		if ($payments) {
			echo"/*$payment[sum]*/";
			echo "/*$payments[month]*/";
			echo"/*$month*/";
			foreach($payments as $payment) {
				$totalCash+=$payment['sum'];
				if ($payment['month']<=$month) $currentCash+=$payment['sum'];
				elseif ($payment['month']-$month<=$_REQUEST['vat_period']) $nextCash+=$payment['sum'];
				else $futureCash+=$payment['sum'];
			}
		}
		
		$new_invoices = sql2array("SELECT COUNT(*) AS cnt FROM acc_docs WHERE type='invoice_accept' AND printed=0",0,'cnt',1);

		$current_vat_period_str=str2time($_REQUEST['current_vat_period'],'d/m/Y').'-'.date("d/m/Y",mktime(0,0,0,str2time($_REQUEST['current_vat_period'],'n')+$_REQUEST['vat_period'],str2time($_REQUEST['current_vat_period'],'d')-1,str2time($_REQUEST['current_vat_period'],'Y')));
		$prev_vat_period_str=date("d/m/Y",strtotime("-$_REQUEST[vat_period] month",str2time($_REQUEST['current_vat_period']))).'-'.date("d/m/Y",strtotime('-1 day',str2time($_REQUEST['current_vat_period'])));
		
		echo "{success:true, data:{"
			."incomeLast:'".round($lastIncome,2)."',incomePrev:'".round($prevIncome,2)."',incomeYear:'".round($yearIncome,2)."',incomeAverage:'".round($averageIncome,2)."'"
			.",expenseCurr:'".round($expenseCurr,2)."',expensePrev:'".round($expensePrev,2)."',expenseYear:'".round($expenseYear,2)."',expenseAver:'".round($expenseAver,2)."',expensePercent:'".($yearIncome ? round($expenseYear/$yearIncome*100,2):0)."%'"
			.",profitLast:'".($lastIncome>$expenseCurr?'+ ':'').round($lastIncome-$expenseCurr,2)."',profitPrev:'".($prevIncome>$expensePrev?'+ ':'').round($prevIncome-$expensePrev,2)."',profitYear:'".($yearIncome>$expenseYear?'+ ':'').round($yearIncome-$expenseYear,2)."',profitAverage:'".($averageIncome>$expenseAver?'+ ':'').round($averageIncome-$expenseAver,2)."',profitPercent:'".($yearIncome ? round(100-$expenseYear/$yearIncome*100,2):0)."%'"
			.",profitLastColor:'".($lastIncome>$expenseCurr?'ffab00':'fe1a00')."',profitPrevColor:'".($prevIncome>$expensePrev?'ffab00':'fe1a00')."',profitYearColor:'".($yearIncome>$expenseYear?'ffab00':'fe1a00')."',profitAverageColor:'".($averageIncome>$expenseAver?'ffab00':'fe1a00')."',profitPercentColor:'".($yearIncome>$expenseYear?'ffab00':'fe1a00')."'"
			.",currentCash:'$currentCash',nextCash:'$nextCash',futureCash:'$futureCash',totalCash:'$totalCash'"
			.",new_invoices:'$new_invoices'"
			.",current_vat_period:'$_REQUEST[current_vat_period]'"
			.",current_vat_period_str:'$current_vat_period_str'"
			.",prev_vat_period:'$prev_vat_period_str'"
		."}}";
			
		#do invoices from accepts
		f_do_invoices();
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_do_invoices() {
 global $CFG,$SVARS;
 	$num=sql2array("SELECT invoice_accept FROM acc_counters",'','invoice_accept',1);
$vat_rate =sql2array("SELECT t1.vat_rate as vat_rate FROM VAT_Rates t1 , VAT_Rates t2, acc_docs
				     where t1.from_date<(select acc_docs.date as date from acc_docs order by id desc limit 1) 
					 and (select acc_docs.date as date from acc_docs order by id desc limit 1)<t2.from_date 
				     and t1.id = t2.id-1 limit 1",0,'vat_rate',1) ;
	$sql = "SELECT acc_accept_rows.*, acc_docs.id AS doc_id, acc_docs.client_id, acc_docs.tax_clear "
		."FROM acc_accept_rows "
		."LEFT JOIN acc_docs ON acc_docs.id = acc_accept_rows.accept_id "
		."WHERE acc_accept_rows.date<='".date('Y-m-d')."' AND invoice_id=0 ";
	$rows=sql2array($sql);
	
	if ($rows) {
		$credit_class=800; $credit_card=($tax?8001:8003); $debt_class=200; $debt_card=$clientID;

		foreach ($rows as $k=>$r) {
			$num++;
			
			$row=&$rows[$k];
			
			
			$clientID = (int)$row['client_id'];
			if (!$clientID) die('{success:false, msg:"clientID error"}');
			
			$sum=round($row['sum'],2);
			if ((int)$row['tax_clear']) $sum = round(100*round($sum,2)/(100-(int)$row['tax_clear']),2);

			$vat_sum=round(($sum/(100+$CFG['VAT'])*$CFG['VAT']),2);

			
			$sql="INSERT INTO acc_docs SET  num=$num, client_id=$clientID"
				." , parent_doc_id=".$row['doc_id']
				." , date='".$row['date']
				."', type='invoice_accept"
				."', sum='".$sum
				."', VAT=$vat_rate"
				." , vat_sum=".quote($vat_sum)
				." , notes='".sql_escape($_REQUEST['notes'])
				."', user_id=".$SVARS['user']['id'];
			
			if (runsql($sql)) {
				$docID = mysql_insert_id();
				
				if ($row['ptype']=='מזומן') {$cashCard=1001; $payment_id=0;}
				elseif ($row['ptype']=='שיק') {$cashCard=1002; $payment_id=(int)$row->id;}
				elseif ($row['ptype']=='אשראי') {
					if ($row['bank']=='וויזה') $cashCard=1004;
					elseif ($row['bank']=='ישראכרט/מאסטרכארד') $cashCard=1005;
					elseif ($row['bank']=='דיינרס') $cashCard=1006;
					elseif ($row['bank']=='אמריקן אקספרס') $cashCard=1007;
					elseif ($row['bank']=='לאומי קארד') $cashCard=1008;
				} else $cashCard=1000;

				#Update accept row
				runsql("UPDATE acc_accept_rows SET payment_status=1, invoice_id=$docID WHERE id=$row[id]");
				#Insert invoice rows
				runsql("INSERT INTO acc_invoice_rows (invoice_id,name) SELECT $docID,name FROM acc_invoice_rows WHERE invoice_id=$row[doc_id]");

				runsql(
					"INSERT INTO acc_operations (date,doc_id,credit_class,credit_card,debt_class,debt_card,sum,user_id) VALUES "
					#Insert invoice sum
					."('".$row['date']."',$docID,800,8001,200,$clientID,'".($sum-$vat_sum)."',{$SVARS['user']['id']})"
					#Insert VAT sum
					.",('".$row['date']."',$docID,550,5502,200,$clientID,'$vat_sum',{$SVARS['user']['id']})"
					#Insert deposite sum
					.",('".$row['date']."',$docID,100,$cashCard,300,1,'".$sum."',{$SVARS['user']['id']})"
				);
			}
		}
		#Update document counter
		runsql("UPDATE acc_counters SET invoice_accept=$num");
		if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET invoice_accept=$num");
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_close_vat_period() {
 global $SVARS;
	if (runsql("UPDATE clients SET current_vat_period=DATE_ADD(current_vat_period,INTERVAL {$SVARS['company']['vat_period']} MONTH) WHERE id=$SVARS[cid]",'rafael')) echo "{success:true}";
	else echo "{success:false}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_init_vat_period() {
 global $SVARS;
	$current_vat_period=quote($_POST['current_vat_year']).'-'.quote($_POST['current_vat_month']).'-01'; 
	
	if(runsql("UPDATE clients SET vat_period=".quote($_POST['vat_period']).", current_vat_period=".quote($current_vat_period).", income_tax=".quote($_POST['income_tax'])." WHERE id=$SVARS[cid]",'rafael')) 
		 echo "{success:true}"; 
	else echo '{success:false}';
}
?>