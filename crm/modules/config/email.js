// Add new account
OpenAccountWin = function() {
	
	this.accountsStore = new Ext.data.JsonStore({
			url:'?m=mail/mail_f&f=get_accounts',
			totalProperty:'total',
			root:'data',
			fields:['id','email','smtp_host','smtp_port','smtp_sec','smtp_auth','username']
		});
	
	this.form = new Ext.form.FormPanel({
		baseCls: 'x-plain',autoHeight:true,
		labelWidth:80,
		items:[
			{xtype:'fieldset',title:'פרטי שרת', cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:
				[ 
					{layout:'table', layoutConfig: {columns:5}, baseCls:'x-plain', items:[
						 {xtype:'label', text:'דוא"ל :'},{colspan:4,name:'name', xtype:'textfield', tabIndex:3, width:200, allowBlank:false}
						,{xtype:'label', text:'שרת דואר יוצא (SMTP) :'},{name:'smtp_host',  xtype:'textfield', tabIndex:7, width:200,allowBlank:false}
						,{xtype:'label', text:'פורט :'},{name:'smtp_port',  xtype:'numberfield', tabIndex:8, width:50,allowBlank:false}
						,{name :'smtp_sec',  xtype:'checkbox',boxLabel:'SSL', tabIndex:9}
						,{name :'smtp_auth', colspan:5,  xtype:'checkbox',boxLabel:'שרת הדואר היוצא שלי דורש אימות', tabIndex:10}
						,{xtype:'hidden', name:'id'}
					]}
				]
			 }
			  ,{xtype:'fieldset',title:'פרטי כניסה', iconCls:'key',cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:
				[ 
					{layout:'table', layoutConfig: {columns:2}, baseCls: 'x-plain', items:[
						{xtype:'label', text:'שם משתמש :'},{name:'username', xtype:'textfield', tabIndex:10, width:200, allowBlank:false}
					   ,{xtype:'label', text:'סיסמה :'},{name:'password',inputType:'password', xtype:'textfield', tabIndex:11, width:200}
					]}
				]
			 }											
			]
	});
	
	OpenAccountWin.superclass.constructor.call(this, {
		layout:'fit',width:520,height:300,iconCls:'emailconf',
		plain:true,id:'config_mail',title:'הגדרת דוא"ל',
		bodyStyle:'padding:5px;',
		buttonAlign:'left',
		items: this.form,
		buttons:[
			 {text:'עדכן',handler:function(){this.onSubmit();}, scope:this}
			,{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('render',function(d){
		//this.getEl().mask();
		this.form.load({
			url: "?m=config/email&f=load_mailProperties",
			reader: new Ext.data.JsonReader({
				root: 'data',
				successProperty: 'success'
			}),
			success: function(f,a){
				d=a.result.data;
				this.form.form.setValues(d);
				this.form.find('name','smtp_sec')[0].setValue((d.smtp_sec == 'ssl') ? true : false);
				this.form.find('name','smtp_auth')[0].setValue(d.smtp_auth);
			},
			failure: function(o,r){ajRqErrors(r.result);},
			scope:this
	   	});
	},this);
	
		
	this.onSubmit = function() {
		if(this.form.form.isValid()){
			this.el.mask('...טוען', 'x-mask-loading');
			this.form.form.submit({
				url:'?m=config/email&f=add_account',
				scope:this,
				success: function(r,o){
					Ext.MessageBox.show({width:250, title:'עדכון חדוא\"ל', msg:'דוא\"ל עודכן בהצלחה!', buttons:Ext.MessageBox.OK,icon:Ext.MessageBox.INFO});
					this.el.unmask();
					this.syncShadow();
					this.close();
				},
				failure: function(r,o){
					this.el.unmask();
					this.syncShadow();
					Ext.MessageBox.show({width:250, title:'שגיאה', msg:'תקלה! נסה שוב.', buttons:Ext.MessageBox.OK,icon:Ext.MessageBox.ERROR});
				}
			});
		}
	};
};
Ext.extend(OpenAccountWin, Ext.Window, {});

Ms['config/email'].run = function(cfg){
	var win = Ext.getCmp('config_mail');
	if(!win){win = new OpenAccountWin();}
	win.show();win.center();

};