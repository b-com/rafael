<?
$NO_SKIN=1;


$clientType=$_GET['clientType'];

// if (file_exists("$_GET[comp].php")) include_once ("$_GET[comp].php");
if (function_exists("f_{$f}")) call_user_func("f_{$f}");
else die('{success:false, msg:"function name error"}');


//————————————————————————————————————————————————————————————————————————————————————
function f_select_client() {
 global $CFG, $m, $a, $group_id, $clientType;

	if($clientType=='client'){
		$where="clients.deleted=".(int)($_POST['team_id']=='deleted');
	
		if ($_POST['query']) $where.=" AND ".make_where_search('name,fname,lname', trim($_POST['query']));
		
		#get data to array
		//$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
		//$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
		$sql="SELECT id, TRIM(CONCAT(name,' ',lname,' ',fname,' (ת.ז: ',cnumber,')')) AS name "
			."FROM clients "
			."WHERE $where "
			."ORDER BY name "
			."LIMIT 50"
			//."LIMIT $_POST[start],$_POST[limit]"
			;
	}elseif($clientType=='user'){
		
		if ($_POST['query']) $where=" WHERE ".make_where_search('username,fname,lname', trim($_POST['query']));

		$sql="SELECT id, TRIM(CONCAT(lname,' ',fname,' (',username,')')) AS name "
			."FROM users $where "
			."ORDER BY name "
			."LIMIT 50";
			
		echo "/*$sql*/";
	}
	
	$rows=sql2array($sql);
	//$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	echo '{total:"'.$rowsCount.'", data:'.array2json($rows).'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_set_metapel() {
 global $group_id;
 
	$id=(int)$_REQUEST['fax_id'];
	if(!$id) {echo '{success:false, msg:"metapel error"}'; return;}
 
	if (runsql("UPDATE ".($_REQUEST['type']=='out' ? 'fax_out' : 'fax_in')." SET metapel=$_REQUEST[user_id] WHERE id=$id"))
		echo '{success:true}';
	else echo '{success:false, msg:"metapel error"}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_change_tupal() {
 global $SVARS;
	$id=(int)$_REQUEST['id'];
	if(!$id) {echo '{success:false, msg:"change tupal error"}'; return;}
	
	$new_status=($_REQUEST['tupal']?0:1);
	
	if (runsql("UPDATE ".($_REQUEST['type']=='out' ? 'fax_out' : 'fax_in')." SET tupal=$new_status WHERE id=$id"))
		echo '{success:true}';
	else echo '{success:false, msg:"change tupal error"}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_change_name() {
 global $SVARS;
	$id=(int)$_REQUEST['id'];
	if(!$id) {echo '{success:false, msg:"change name error"}'; return;}
	
	if (runsql("UPDATE ".($_REQUEST['type']=='out' ? 'fax_out' : 'fax_in')." SET name=".quote($_REQUEST['name'])." WHERE id=$id"))
		echo '{success:true}';
	else echo '{success:false, msg:"change name error"}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_link_fax() {
 global $CFG, $SVARS, $m, $a, $group_id, $clientType;
	
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_POST['ids'])) die('{success:false, msg:"wrong ids"}');
	$client_id=(int)$_POST['client_id'];

	if (!$client_id) die('{success:false, msg:"wrong client_id"}');
	
	$client_type=$clientType;
	
	$_POST['notes']		= trim($_POST['notes']);
	$_POST['file_cat']	= trim($_POST['file_cat']);
	$_POST['file_name']	= trim($_POST['file_name']);
	
	if (!$_POST['file_cat']) $_POST['file_cat']='כללי';
	
	$faxes=sql2array("SELECT * FROM ".($_REQUEST['type']=='out' ? 'fax_out' : 'fax_in')." WHERE id IN($_POST[ids])");
	if (!$faxes) die('{success:false, msg:"wrong ids"}');

	if ($faxes) foreach($faxes as $fax) {
		
		if ($_REQUEST['type']=='in') {
			if ($fax['new']) get_from_server($fax);
			$dbFileName = $_POST['file_name'] ? $_POST['file_name'].'.pdf' : "פקס מ-".fax_in_short_name($fax)." #$fax[id].pdf";
			$faxFile="../../crm-fax/$SVARS[cid]/$fax[destiny_fax]/$fax[id].pdf";
		
		}elseif ($_REQUEST['type']=='out') {
			if ($fax['link_to']!='clients') {	#! if not already linked (sent from client)
				$faxFile=$fax['file'];
				$dbFileName = $_POST['file_name'] ? $_POST['file_name'].'.pdf' : "פקס ל-$fax[fax] #$fax[id].pdf";
			}	
		}
		
		if ($faxFile AND !is_file($faxFile)) die('{success:false, msg:"Fax file not found!"}');
		elseif ($faxFile) {
			$sql="INSERT INTO files SET "
				."user_id={$SVARS['user']['id']}, "
				."client_id=$client_id, "
				."client_type='$client_type', "
				."file_name='".sql_escape($dbFileName)."', "
				."file_type='fax', "
				."file_ext='pdf', "
				."file_size='".filesize($faxFile)."', "
				."file_cat=".quote($_POST['file_cat']).", "
				."link=".quote($link).", "
				."notes=".quote($_POST['notes']).", "
				."date_added='$fax[date]'";
			if (!runsql($sql)) { echo '{success:false}'; return; }
			$last_id=mysql_insert_id();
			
			# prepare targetDir and targetFile var
			$targetDir="../../crm-files/$SVARS[cid]/".substr($last_id, -2);
			if (!is_dir($targetDir)) mkdir($targetDir, 0777, true);
			$targetFile="$targetDir/$last_id.pdf";
			
			# move files
			if ($_REQUEST['type']=='in') { # move file to client folder
				if (rename($faxFile, $targetFile)) runsql("DELETE FROM fax_in WHERE id=$fax[id]");
				else { runsql("DELETE FROM files WHERE id=$last_id"); echo '{success:false}'; return; } # error renaming file
			
			}elseif ($_REQUEST['type']=='out') {	# should run if($fax['link_to']!='clients')
				if (rename($faxFile, $targetFile)) runsql("UPDATE fax_out SET file=".quote($targetFile).", link_to='clients', link_id=$client_id, attach_id=$last_id WHERE id=$fax[id]");
				else { runsql("DELETE FROM files WHERE id=$last_id"); echo '{success:false}'; return; } # error renaming file
				
			}
		}//elseif (count($faxes)==1) die('{success:false, msg:"Fax file not found!"}');
	}
	
	echo '{success:true}';
}
//————————————————————————————————————————————————————————————————————————————————————
function get_from_server($fax) {
 global $SVARS;
	
	$url="https://bafi.openvoice.co.il/faxes/$fax[faxpath]/fax.pdf";

	$filename="Fax [$fax[id]] From $fax[company_fax] ($fax[company_name]) ".str2time($fax['date'],'d.m.y H-i').'.pdf';
	
	set_time_limit(180);
	ini_set('memory_limit', '200M');
	ignore_user_abort(true);

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 120);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	$content = curl_exec($ch);
	
	curl_close($ch);
	
	if (!$content) { $NO_HEADERS=1; echo "<div dir=rtl>שרות לא זמין כעת<br>נסה שנית מאוחר יותר.</div>";  return false; }
	
	if ($_GET['download']) { 
		header('Content-Type: application/octet-stream');
		header("Content-disposition: attachment; filename=\"$filename\"" );
	}else header('Content-Type: application/pdf');
	header('Content-Length: '.strlen($content) );
	header('Connection: close');
	ob_end_clean();

	echo $content;	# send file content to user
	
	flush();
	
	# save file
	if (!is_dir("../../crm-fax/$SVARS[cid]/$fax[destiny_fax]")) mkdir("../../crm-fax/$SVARS[cid]/$fax[destiny_fax]", 0777, true);
	if (write2file("../../crm-fax/$SVARS[cid]/$fax[destiny_fax]/$fax[id].pdf", $content)) {
		$sql="UPDATE fax_in SET new=0, readed=1 WHERE id=$fax[id]";
		runsql($sql);
		
		if (!eregi('crm\.loc', $_SERVER['HTTP_HOST'])) {
			# delete fax from server
			$ch = curl_init("https://bafi.openvoice.co.il/faxes.php?a=del&faxpath=".urlencode($fax['faxpath']));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			$content = curl_exec($ch);
			curl_close($ch);
		}
	}
}

//————————————————————————————————————————————————————————————————————————————————————
function f_send_mail() {
 global $SVARS, $CFG, $client_id, $group_id;
	$id=(int)$_POST['id'];
	if (!$id) die('{success:false, msg:"Wrong id"}');

	include_once "../inc/mail/sendmail.php";
	$CFG['debug']=0; $GLOBALS['CFG']['show_errors']=0;
	$from=sql2array("SELECT CONCAT(fname,' ',lname) as fullname, email, o_email, c_email FROM users WHERE user_id=$SVARS[user_id]",'','',1);
	
	if ($from['email']) $replyto=$from['email'];
	elseif ($from['o_email']) $replyto=$from['o_email'];
	elseif ($from['c_email']) $replyto=$from['c_email'];
	else $replyto='support@bafi.co.il';
	
	if ($_POST['type']=='out') {
		include_once "../inc/fax.php";
		$fax = sql2array("SELECT * FROM fax_out WHERE id=$id", 0,0,1);
		if ($fax['file'] AND !is_file($fax['file'])) die('{success:false, msg:"File not found"}');
		elseif ($fax['file']) $file=array($fax['file'], 'Fax.pdf');
		
		$mail_html="<div dir=rtl align=right style='min-width:600px; font-family:Tahoma,Arial,Helvetica;'>"
			.text2html($_POST['message'])
			.($fax['text'] ? "<br><br><hr>Fax Content:<hr>".fax_cover_html($fax) : '')
			."</div>";
		
	}else {
		$fax = sql2array("SELECT * FROM fax_in WHERE id=$id", 0,0,1);
		$filepath=realpath("../../crm-fax/$SVARS[cid]/$fax[destiny_fax]/$fax[id].pdf");
		if (!file_exists($filepath)) die('{success:false, msg:"File not found."}');
		$file=array($filepath, 'Fax.pdf');
		
		$mail_html="<div dir=rtl align=right style='font-family:Tahoma,Arial,Helvetica;'>".text2html($_POST['message'])."</div>";
	}
	
	if (send_mail($_POST['email'], $_POST['subject'], $mail_html, array('from'=>"$from[fullname] ($replyto) <support@bafi.co.il>", 'replyto'=>"$from[fullname] <$replyto>", 'file'=>$file)) ) {
		echo '{success:true}'; return;
	}else die('{success:false, msg:"Error sending email"}');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_load_fax_data() {
 global $SVARS, $CFG, $a, $group_id;
	$id=(int)$_GET['id'];
	if (!$id) {echo '{success:false}'; return;}
	
	$data = sql2array("SELECT sender_name, sender_fax, sender_tel, subject, logo, text FROM fax_out WHERE id=$id",0,0,1);
	if (!$data) $data=array();
	if ($data['subject'].$data['text']) $data['cover_page']=1;
	echo '{success:true, total:1, data:'.array2json($data).'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_send_fax() {
 global $SVARS, $CFG, $a, $group_id;
	// die('{success:false, msg:"זמנית לא ניתן לשלוח פקסים, אנא נסו שוב מאוחר יותר. <br>עמכם הסליחה."}');
	
	// $id=(int)$_POST['id'];
	if (isset($_POST['link_id']))	$_POST['link_id']	= (int)$_POST['link_id'];	# item id in linked table
	if (isset($_POST['attach_id']))	$_POST['attach_id']	= (int)$_POST['attach_id'];	# attached file id (in crm_files or other)
	// if (!$id) die('{success:false, msg:"Wrong id"}');

	$recipient_fax=ereg_replace('[^0-9]','',$_POST['recipient_fax']);
	if ($a!='preview' AND strlen($recipient_fax)<3) die('{success:false, msg:"Bad fax number."}');
	
	include_once "../inc/fax.php";
	
	if ($_POST['cover_page']) $pdf = html2pdf(fax_cover_html($_POST), $_POST['logo']);

	if ($a=='preview') { send_headers(true);  $pdf->Output('coverpage.pdf', 'I');  exit; } # show PDF
	
	if (get_fax_credits()<2) die('{success:false, msg:"חבילת הפקסים שרכשתם נסתיימה, נא לצור קשר עם 074-7144333."}');

	
	# get file from crm_files
	if ($_POST['client_type']=='client' AND $_POST['client_id'] AND $_POST['attach_id']) {
		$file=sql2array("SELECT * FROM files WHERE id=$_POST[attach_id]", 0, 0, 1); 
		$filepath="../../crm-files/$SVARS[cid]/".substr($file['id'], -2)."/$file[id]".($file['file_ext']?".$file[file_ext]":'');
		// echo "/* ".print_r($file,1)." */";
		// echo "/* $filepath */";
		if (!$file OR !is_file($filepath)) die('{success:false, msg:"קובץ לא קיים"}');
	
	# get file from faxes
	}elseif ($_POST['link_to']=='faxes' AND $_POST['link_id']) {
		$fax = sql2array("SELECT * FROM fax_in WHERE id=$_POST[link_id]", 0,0,1);
		$filepath="../../crm-fax/$SVARS[cid]/$fax[destiny_fax]/$fax[id].pdf";
		if (!$fax OR !is_file($filepath)) die('{success:false, msg:"קובץ לא קיים"}');
	
	# get file from fax_out
	}elseif ($_POST['link_to']=='fax_out' AND $_POST['link_id']) {
		$fax = sql2array("SELECT * FROM fax_out WHERE id=$_POST[link_id]", 0,0,1);
		if ($fax['file']) {
			$filepath=$fax['file'];
			if (!$fax OR !$filepath OR !is_file($filepath)) die('{success:false, msg:"קובץ לא קיים"}');
		}elseif ($fax['text']) {}
	
	# get file receipt
	}elseif ($_POST['link_to']=='receipt' AND $_POST['attach_id']) {
		include_once "modules/crm/crm_receipts_f.php";
		$_POST['receipt_id']=$_POST['attach_id'];
		$GLOBALS['prms_groups']=$group_id;
		$GLOBALS['receipt_id']=$_POST['attach_id'];
		$GLOBALS['client_id']=$_POST['link_id'];
		f_receipt_to_pdf();
		$filepath="../tmp_files/r$_POST[attach_id]"."c$_POST[link_id].pdf";
	}
	
	# generate coverpage
	if ($_POST['cover_page'] AND $pdf) $coverpage = $pdf->Output('coverpage.pdf', 'S'); # get coverpage PDF content
	
	if (!$_POST['cover_page']) $_POST['recipient_name']=$_POST['sender_name']=$_POST['sender_fax']=$_POST['sender_tel']=$_POST['subject']=$_POST['text']=$_POST['logo']='';
	
	if (!$filepath AND !$coverpage) die('{success:false, msg:"Nothing to send"}');
	
	// echo "/*".print_r($_POST,1)."*/\n";
	
	# add to db
	$sql="INSERT INTO fax_out SET status=1"
		.", user_id={$SVARS['user']['id']}"
		// .", jobid=$jobid"
		.", date='".date('Y-m-d H:i:s')."'"
		.", recipient_fax=".quote($recipient_fax)
		.", recipient_name=".quote($_POST['recipient_name'])
		.", sender_name=".quote($_POST['sender_name'])
		.", sender_fax=".quote($_POST['sender_fax'])
		.", sender_tel=".quote($_POST['sender_tel'])
		.", subject=".quote($_POST['subject'])
		.", text=".quote($_POST['text'])
		.", logo=".(int)$_POST['logo']
		.($_POST['client_type'] ? ", link_to=".quote($_POST['client_type']) :'')# enum('fax_in','fax_out','clients')
		.($_POST['client_id'] 	? ", link_id=$_POST[client_id]" :'')			# client_id / fax id 
		// .($_POST['link'] 	? ", link=".quote($_POST['link']) :'')			# claim_id
		.($_POST['attach_id'] 	? ", attach_id=$_POST[attach_id]" :'')			# file_id
		.($filepath ? ", file=".quote($filepath) :'');
	// echo "/* $sql */\n";
	runsql($sql);
	$fax_id=mysql_insert_id();
	if (!$fax_id) die('{success:false, msg:"Error.."}');
	
	# send to server
	// $send_fax='OK#1';
	
	$send_fax=send_fax($fax_id, $recipient_fax, $filepath, $coverpage);
	// echo "/*$send_fax*/";
	if (ereg("OK#([0-9]+)", $send_fax, $regs)) {
		runsql("UPDATE fax_out SET jobid=$regs[1] WHERE id=$fax_id");
		echo '{success:true}';
	}else { 
		runsql("DELETE FROM fax_out WHERE id=$fax_id");
		echo '{success:false, msg:"שגיאה בשלחת פקס<br>נסה שנית מאוחר יותר.'.($SVARS['is_admin'] ? '<br>'.jsEscape($send_fax) :'').'"}';
	}
	
	if ($_POST['link_to']=='receipt' AND is_file("../tmp_files/r$_POST[attach_id]"."c$_POST[link_id].pdf")) unlink("../tmp_files/r$_POST[attach_id]"."c$_POST[link_id].pdf");
}

//————————————————————————————————————————————————————————————————————————————————————
function f_fax_not_readed() {
 global $CFG, $m, $a, $group_id;
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	if (runsql("UPDATE fax_in SET readed=0 WHERE id IN ($_GET[ids])")) echo '{success:true}';
	else echo '{success:false, msg:"Error"}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_fax_del() {
 global $CFG, $m, $a, $group_id;
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	//echo '{success:true}'; return;
	
	if (runsql("UPDATE ".($_REQUEST['type']=='out' ? 'fax_out' : 'fax_in')." SET deleted='".date('Y-m-d')."' WHERE id IN ($_GET[ids])"))
		echo '{success:true}';
	else echo '{success:false, msg:"delete error"}';
	// flush();

	# delete old faxes
	if (rand(1,10)==1) {
		ignore_user_abort(true);
		
		# del old fax out
		// $old_out_deleted=sql2array("SELECT * FROM fax_out WHERE deleted>'0000-00-00' AND deleted<'".date('Y-m-d', strtotime('-1 month'))."'");
		// if ($old_out_deleted) foreach($old_out_deleted as $fax) {# remove file
			// unlink("../crm-fax/out/$fax[group_id]/$fax[id].pdf");
		// }

		# del old fax in
		$old_in_deleted=sql2array("SELECT * FROM fax_in WHERE deleted>'0000-00-00' AND deleted<'".date('Y-m-d', strtotime('-1 month'))."'");
		if ($old_in_deleted) {
			foreach($old_in_deleted as $fax) {
				if ($fax['new'] AND !eregi('bafi\.loc', $_SERVER['HTTP_HOST'])) { # remove from server
					set_time_limit(60);
					$ch = curl_init("https://bafi.openvoice.co.il/faxes.php?a=del&faxpath=".urlencode($fax['faxpath']));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					$content = curl_exec($ch);
					curl_close($ch);
				}else {	# remove local file
					$file="../crm-fax/$fax[destiny_fax]/in/$fax[id].pdf";
					unlink($file);
				}
			}
			# delete from DB
			runsql("DELETE FROM fax_in WHERE deleted>'0000-00-00' AND deleted<'".date('Y-m-d', strtotime('-1 month'))."'");
		}
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_fax_restore() {
 global $CFG, $m, $a, $group_id;
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	
	if (runsql("UPDATE ".($_REQUEST['type']=='out' ? 'fax_out' : 'fax_in')." SET deleted='0000-00-00' WHERE id IN($_GET[ids])"))
		echo '{success:true}';
	else echo '{success:false, msg:"restore error"}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_fax_list() {
 global $CFG, $SVARS, $m, $a, $group_id;
 
	if ($_GET['type']!='in' AND $_GET['type']!='out' AND $_GET['type']!='deleted') $_GET['type']='in';
	
	if ($_GET['type']=='in') f_fax_in_list();
	elseif ($_GET['type']=='out') f_fax_out_list();
	elseif ($_GET['type']=='deleted') f_fax_in_list();
}
//————————————————————————————————————————————————————————————————————————————————————
function fax_in_short_name($r) {
	$fax='';
	$r['company_name']= ereg_replace('^972','0',$r['company_name']);
	$r['company_fax'] = ereg_replace('^972','0',$r['company_fax']);
	
	if ($r['company_name'] AND !eregi('^(Anonymous)|(X+)|(0+)$',$r['company_name'])) $fax.= $r['company_name'];
	if ($r['company_fax'] AND $r['company_fax']!=$r['company_name'] AND !eregi('^(Anonymous)|(X+)|(0+)$',$r['company_fax']))  
		$fax= ($fax ? "($fax) $r[company_fax]" : $r['company_fax']);
	
	if ($fax==='') { if ($r['company_name']) $fax=$r['company_name']; else $fax='Anonymous'; }
 return $fax;
}
//————————————————————————————————————————————————————————————————————————————————————
function f_fax_in_list() {
 global $CFG, $SVARS, $m, $a, $group_id;
	
	include 'modules/common/fax_sync.php';
	
	if (!$SVARS['user']['fax_in']) { echo '{total:0,data:[]}'; return; }

	$fax_nums_ar = explode(',',$SVARS['user']['fax_in']);
	
	array_walk($fax_nums_ar, 'trim');

	$fax_numbers = "'".implode("','", $fax_nums_ar)."'";
	
	if ($_GET['type']!='in' AND $_GET['type']!='out' AND $_GET['type']!='deleted') $_GET['type']='in';
	
	if ($_GET['type']=='deleted') $where="deleted>0"; 
	else $where="type='$_GET[type]' AND deleted='0000-00-00'"; 
	
	$where.=" AND destiny_fax IN($fax_numbers)";
	if($_GET['metapel']) $where.=" AND metapel=$_GET[metapel] ";
	
	if	   ($_GET['sort']=='to') 	$order_by='destiny_fax';
	elseif ($_GET['sort']=='name')	$order_by='company_name';
	elseif ($_GET['sort']=='date')	$order_by='date';
	else							$order_by='id';
	
	#get data to array
	$_GET['limit'] = (int)$_GET['limit'] ? (int)$_GET['limit'] : 100;
	$_GET['start'] = (int)$_GET['start'] ? (int)$_GET['start'] : 0 ;
	
	$sql="SELECT SQL_CALC_FOUND_ROWS id, type, destiny_fax, company_fax, company_name, `date`, errormsg, readed, tupal, metapel, name "
		."FROM fax_in "
		.($where ? "WHERE $where " :'');
	
	if ($_GET['type']=='deleted') $sql.="UNION (SELECT id, 'out' AS type, recipient_fax AS destiny_fax, '' AS company_fax, '' AS company_name, `date`, '' AS errormsg, 1 AS readed, metapel, tupal, name "
				."FROM fax_out WHERE deleted>0) ";
	$sql.="ORDER BY $order_by".($_GET['dir']=='DESC'?' DESC':'')." "
		."LIMIT $_GET[start],$_GET[limit]";
	// echo "/* $sql */\r\n";
	$rows=sql2array($sql);
	$rowsCount=count($rows);
	if ($rowsCount==$_GET['limit']) $rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	
	if ($rows) foreach ($rows as $k=>$r) {
		if     ($r['type']=='in')	$r['fax']=fax_in_short_name($r);
		elseif ($r['type']=='out')  $r['fax']=$r['destiny_fax'];
		
		$C[$k]['id']	= $r['id'];
		$C[$k]['to']	= $r['destiny_fax'];
		$C[$k]['fax']	= ($r['name']?$r['name']:$r['fax']);

		$C[$k]['type']	= $r['type'];
		$C[$k]['date']	= str2time($r['date'], 'd/m/y H:i');
		$C[$k]['readed']= $r['readed'];
		$C[$k]['metapel']= $r['metapel'];
		$C[$k]['tupal']= $r['tupal'];
		if ($r['errormsg']) $C[$k]['error']=1;
		
		// if ($r['new']) $new_count++;
	}else $C=array();

	if ($_GET['type']!='deleted') $del_count=(int)sql2array("SELECT COUNT(*) AS count FROM fax_in WHERE destiny_fax IN($fax_numbers) AND deleted>0", false, 'count', true);
	
	echo "{total:'$rowsCount', deleted:'$del_count', data:".array2json($C)."}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_fax_out_list() {
	include_once "../inc/fax.php";
	
	$where = "(link_to='fax_in' OR link_to='fax_out')";
	if($_GET['metapel']) $where.=" AND metapel=$_GET[metapel] ";
	$where.= $_GET['type']=='deleted' ? " AND deleted>0" : " AND deleted='0000-00-00'";
	
	if ($_GET['sort']=='fax')		$order_by='recipient_fax';
	elseif ($_GET['sort']=='date')	$order_by='date';
	else							$order_by='id';
	
	$_GET['limit'] = (int)$_GET['limit'] ? (int)$_GET['limit'] : 100;
	$_GET['start'] = (int)$_GET['start'] ? (int)$_GET['start'] : 0 ;
	
	#get data to array
	$sql="SELECT SQL_CALC_FOUND_ROWS * "
		."FROM fax_out "
		.($where ? "WHERE $where " :'')
		."ORDER BY $order_by".($_GET['dir']=='DESC'?' DESC':'')." "
		."LIMIT $_GET[start],$_GET[limit]";
	echo "/*\$sql=$sql\r\n;*/";
	$rows=sql2array($sql);
	$rowsCount=count($rows);
	if ($rowsCount==$_GET['limit']) $rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if ($rows) foreach ($rows as $k=>$r) {
		$C[$k]['id']	= $r['id'];
		$C[$k]['fax']	= ($r['name'] ? $r['name'] : $r['recipient_fax']);
		$C[$k]['type']	= 'out';
		$C[$k]['date']	= str2time($r['date'], 'd/m/y H:i');
		$C[$k]['status']= $r['status'];
		$C[$k]['metapel']= $r['metapel'];
		$C[$k]['tupal']= $r['tupal'];
		if ($r['status']==3) $C[$k]['error']=1;
	}else $C=array();
	
	echo "{total:'$rowsCount', data:".array2json($C)."}";
}
?>