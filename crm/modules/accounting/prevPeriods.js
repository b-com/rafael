prevPeriods = function() {


this.typeRenderer = function (str) {var store_types = {part:'חלופים',material:'חומרים',service:'עבודה',fserv:'עבודת חוץ',other:'אחר'}; return store_types[str];}
var rep_periods = [[1,'ינואר'],[2,'ינואר-פברואר'],[3,'פברואר'],[4,'פברואר-מרץ'],[5,'מרץ'],[6,'מרץ-אפריל'],[7,'אפריל'],[8,'אפריל-מאי'],[8,'מאי'],[9,'מאי-יוני'],[10,'יוני'],[11,'יוני-יולי'],[12,'יולי'],[13,'יולי-אוגוסט'],[14,'אוגוסט'],[15,'-אוגוסט-ספטמבר'],[16,'ספטמבר'],[17,'ספטמבר-אוקטובר'],[18,'אוקטובר'],[19,'אוקטובר-נובמבר'],[20,'נובמבר'],[21,'נובמבר-דצמבר'],[22,'דצמבר']]

this.submitFunc = function(){
	var isum = Ext.getCmp('Form').getForm().findField("taxAbleDls").getValue()+Ext.getCmp('Form').getForm().findField("taxFreeDls").getValue();
	var eTax1 = Ext.getCmp('Form').getForm().findField("realTax").getValue();
	var eTax2 = Ext.getCmp('Form').getForm().findField("ExpTax").getValue();
	var year = Ext.getCmp('Year').getRawValue();
	var month = Ext.getCmp('Form').getForm().findField("Month").getRawValue();
	//alert(month);
	Ext.getCmp('Form').form.submit({
			url: "?m=common/accounting&f=prevPeriods",
			params:{month:month, year:year,
				isum:Ext.encode(isum),eTax1:Ext.encode(eTax1),eTax2:Ext.encode(eTax2),formType:1},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
			Ext.MessageBox.show({
				width:280, 
				title:'אישור!', 
				msg:'פעולה בוצעה בהצלחה', 
				buttons:Ext.MessageBox.OK, 
				icon: Ext.MessageBox.QUESTION, 
				scope:this
			
			});
			// this.grid.tbar.hide();
			// if (this.type=='invoice_credit' || this.type=='invoice_debt' || this.type=='invoice_accept') this.grid.tbar.dom.style.display='none';
			if (this.type=='accept' || this.type=='accept_tmp' || this.type=='invoice_accept') this.prevPeriodsStore.tbar.dom.style.display='none';
			// this.syncSize();
			//if (cfg.callBack) cfg.callBack();
			this.id = Ext.decode(a.response.responseText).id;
			//this.form.find('name','id')[0].setValue(this.id);
			this.id = Ext.decode(a.response.responseText).id;
			//this.form.find('name','id')[0].setValue(this.id);
			this.buttons[0].disable();
			this.buttons[1].enable();
			this.buttons[2].setText('סגור');
			this.syncShadow();
		}
				        })
}


//this.submitFunc = function(){
//	alert(Ext.getCmp('Month').getRawValue())
//}

this.form = new Ext.form.FormPanel({
	id:'Form',x:0,y:0,border:true, height:570,width:897,layout:'absolute', bodyStyle: 'background:transparent;',cls:'test',
	items:[
		{ x:790,y:87,id:'Month',xtype:'combo', store:rep_periods, hiddenName:'month', value:'ינואר', style:'margin-right:78px;height:37;', width:90, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false
		, renderer:function(v){var obj={0:'ינואר',1:'נובמבר-דצמבר',2:'נובמבר',3:'אוקטובר-נובמבר',4:'אוקטובר',5:'ספטמבר-אוקטובר',6:'ספטמבר',7:'אוגוסט-ספטמבר',8:'אוגוסט',9:'יולי-אוגוסט',10:'יולי',11:'יוני-יולי',12:'יוני',13:'מאי-יוני',14:'מאי',15:'אפריל-מאי',16:'אפריל',17:'מרץ-אפריל',18:'מרץ',19:'פברואר-מרץ',20:'פברואר',21:'ינואר-פברואר',22:'דצמבר'}; return obj[v];}},
		{ x:720, y:87, id:'Year',xtype:'combo', store:years,value:'2003', hiddenName:'year', style:'margin-right:90px;height:37;', width:70, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false},
		{x:15,y:145,id:'taxAbleDls',dataIndex:'taxableDeals',emptyText:'נא להזין סכום',xtype:'numberfield',width:240,height:40,style:'margin-right:354px;',allowBlank:false},
		{x:295,y:145,id:'taxFreeDls',dataIndex:'taxFreeDeals',emptyText:'נא להזין סכום',xtype:'numberfield',width:240,height:45,style:'margin-right:41px;',allowBlank:false},
		{x:595, y:210,id:'realTax',dataIndex:'permRealTax',emptyText:'נא להזין סכום',xtype:'numberfield',width:245,height:39,style:'margin-right:50px;',allowBlank:false},
		{x:595, y:267,id:'ExpTax',dataIndex:'otherExpTax',emptyText:'נא להזין סכום',xtype:'numberfield',width:245,height:39,style:'margin-right:50px;',allowBlank:false},
		{x:22, y:420,xtype:'button', cls:'subminBtn', text:'שמור נתונים',  height:70,width: 210,style:'margin-right:653px;', 
			handler:this.submitFunc
		}		
	]
});



	prevPeriods.superclass.constructor.call(this, 
	{width:900,height:600, resizable:false,bodyStyle: 'background:transparent;', border:true,layout:'absolute', 
		items:[this.form],
		buttons:[{
		text:'בטל', 
		handler:function(){
			//Dump(this);
			this.destroy();
		}, 
		scope:this
	}]
		})



};
Ext.extend(prevPeriods, Ext.Window);