﻿<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
 
 	$ln='he';
	
	$modules=sql2array("SELECT * FROM modules WHERE active=1 AND parent_id=0 ORDER BY id",'','',0,'rafael');
	
	if ($modules)  foreach($modules as $m) {
		$Ms[]=array('defaults'=>"{baseCls:'x-plain', cls:'rtl'}",
			'items'=>array(
				array(
					'html'=>"<div class='".($m['shortcutIconCls']? $m['shortcutIconCls']: 'demo-grid-shortcut')."' style='width:100%; height:48px; background-position:center top; background-repeat:no-repeat;' onClick='Ext.getCmp(\"mod$m[id]\").setValue(!Ext.getCmp(\"mod$m[id]\").getValue());'></div>",
					'style'=>'text-align:center;','baseCls'=>'x-plain','cls'=>'rtl'
				),
				array('xtype'=>'checkbox', 'checked'=>false, 'boxLabel'=>$m["title_$ln"],	'name'=>"mod[$m[id]]")
			)
		);
	}
	
	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end FROM users ORDER BY fullName",'id');
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;
	}

	#Load contacts patterns
	$patterns_ar=sql2array("SELECT id, type, name FROM patterns ORDER BY name",'id');
	if ($patterns_ar) foreach($patterns_ar as $id=>$pattern) {
		if ($pattern['type']=='sms') $smsPatterns.=($smsPatterns ? ',':'')."[$id,'".jsEscape($pattern['name'])."']";
	}
	
	$client_groups_ar=sql2array("SELECT * FROM client_groups",'id','name');
	
	if ($client_groups_ar) foreach($client_groups_ar as $id=>$client_group) $client_groups.=($client_groups ? ',':'')."[$id,'".jsEscape($client_group)."']";

	?>
	
	var Modules=<?=array2json($Ms)?>;
	
	var smsActive=<?=($SVARS['user']['sms_active'] ? 'true' : 'false')?>;
	var faxActive=<?=($SVARS['user']['fax_active'] ? 'true' : 'false')?>;
	var fax_numbers=[];

	var users=[<?=$users;?>];
	var agents=[<?=$agents;?>];
	var users_obj=<?=array2json($users_ar);?>;
	var	sms_originator='';
	var smsPatterns=[<?=$smsPatterns;?>];
	var client_groups=[<?=$client_groups;?>];
	
	Ext.ux.Loader.load([
			'skin/extjs/ux/superboxselect/superboxselect.css?<?=filemtime('skin/extjs/ux/superboxselect/superboxselect.css');?>',
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			'skin/extjs/ux/superboxselect/SuperBoxSelect.js?<?=filemtime('skin/extjs/ux/superboxselect/SuperBoxSelect.js');?>',
			'modules/common/contacts.js?<?=filemtime('modules/common/contacts.js');?>',
			'modules/common/files.js?<?=filemtime('modules/common/files.js');?>',
			'modules/users/users.js?<?=filemtime('modules/users/users.js');?>',
			'modules/common/accounting.js?<?=filemtime('modules/common/accounting.js');?>',
			'modules/companies/companies.js?<?=filemtime('modules/companies/companies.js');?>'
		], function(){Ms.companies.run();}
	);
	<?
}
//————————————————————————————————————————————————————————————————————————————————————
function f_load_client_data() {
 global $SVARS, $group_id;
	$client_id=(int)$_REQUEST['client_id'];
	if (!$client_id) {echo '{success:false}'; return;}
	
	$sql="SELECT * FROM clients WHERE id=$client_id";
	
	$client=sql2array($sql,'','',1);
	if ($client['birthday']=='0000-00-00') $client['birthday']='';
	if ($client['tax_assessor']==0) unset($client['tax_assessor']);
	if ($client['tax_clerck']==0) unset($client['tax_clerck']);
	if ($client['taxing_start']=='0000-00-00') unset($client['taxing_start']);
	if ($client['tax_client_sourse']==0) unset($client['tax_client_sourse']);
	if ($client['tax_folder_type']==0) unset($client['tax_folder_type']);
	if ($client['tax_client_status']==0) unset($client['tax_client_status']);
	if ($client['date_status_update']==0) unset($client['date_status_update']);
	if ($client['tax_branch']==0) unset($client['tax_branch']);
	if ($client['date_last_report']==0) unset($client['date_last_report']);
	if ($client['license_date']=='0000-00-00') $client['license_date']='';
	if ($client['fam_status']==0) $client['fam_status']=1;
	if ($client['c_type']==2) {
		$client['hp']=$client['passport'];
		$client['bname']=$client['first_name'];
	} else {
		// $family=sql2array("SELECT * FROM crm_clients_links WHERE linked_client_id=0 AND client_id=$client_id");
		// $i=0;
		// if ($family) foreach ($family as $key=>$row) {
			// $client["f_type$i"]=$row['link_type'];
			// $client["f_name$i"]=$row['name'];
			// $client["f_tz$i"]=$row['passport'];
			// $client["f_bdate$i"]=($row['birthday']=='0000-00-00'?'':$row['birthday']);
			// $client["f_tel$i"]=$row['tel'];
			// $client["f_fax$i"]=$row['fax'];
			// $client["f_email$i"]=$row['email'];
			// $client["f_license_num$i"]=$row['license_num'];
			// $client["f_license_date$i"]=($row['license_date']=='0000-00-00'?'':$row['license_date']);
			// if ($row['link_type']=='בן' OR $row['link_type']=='בת') $client["childs_cnt"]++;
			// $i++;
		// }
	}
	# Set user_logo
	$photo = glob("files/crm_photos/$group_id/$client_id.*");
	$photo = $photo[0];
	if ($photo) $client['client_photo']="$photo?".filemtime($photo);
	
	echo '{success:true, total:1 ,data:'.array2json($client).'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_client() {
 global $SVARS, $CFG, $group_id;
	$id=(int)$_REQUEST['id'];
	$c_type=(int)$_POST['c_type'];
	$agent_id = (int)$_POST['agent_id'];
	$name = jsEscape(trim($_POST['name']));
	
	if ($id>0) {
		$sql="UPDATE clients SET "
		."', c_type='".(int)$_POST['c_type']
		// ."', agent_id='".(int)$_POST['agent_id']
		// ."', folder='".sql_escape($_POST['folder'])
		."', name='".($c_type==1 ? sql_escape($_POST['name']) : sql_escape($_POST['bname']))
		."', cnumber='".($c_type==1 ? sql_escape(sprintf("%09s", $_POST['passport'])) : sql_escape($_POST['hp']))
		// ."', team_id='".(int)$_POST['team_id']
		."', birthday='".str2time($_POST['birthday'],'Y-m-d')
		."', sex='".(int)$_POST['sex']
		."', cperson='".sql_escape($_POST['cperson'])
		."', bdefine='".sql_escape($_POST['bdefine'])
		."', emp_num='".sql_escape($_POST['emp_num']);
		$n=0;
		for ($i=0; $i<8; $i++) if ($_POST["tel$i"]) {$sql.="',tel_notes$n='".sql_escape($_POST["tel_notes$i"])."', tel_type$n='".(int)$_POST["tel_type$i"]."', tel$n='".sql_escape($_POST["tel$i"]); $n++;}
		for ($i=$n; $i<8; $i++) $sql.="',tel_notes$i='', tel_type$i='', tel$i='";
		$sql.="', email_notes='".sql_escape($_POST['email_notes'])."', email='".sql_escape($_POST['email']);
		$n=1;
		for ($i=1; $i<3; $i++) if ($_POST["email$i"]) {$sql.="', email_notes$n='".sql_escape($_POST["email_notes$i"])."', email_type$n='".(int)$_POST["email_type$i"]."', email$n='".sql_escape($_POST["email$i"]); $n++;}
		for ($i=$n; $i<3; $i++) $sql.="', email_notes$i='', email_type$i='', email$i='";
		$n=0;
		for ($i=0; $i<3; $i++) if ($_POST["city$i"] OR $_POST["zip$i"] OR $_POST["adr$i"]) {$sql.="', adr_type$n='".(int)$_POST["adr_type$i"]."', city$n='".sql_escape($_POST["city$i"])."', zip$n='".sql_escape($_POST["zip$i"])."', adr$n='".sql_escape($_POST["adr$i"]); $n++;}
		for ($i=$n; $i<3; $i++) $sql.="', adr_type$i='', city$i='', zip$i='', adr$i='";
		$n=0;
				
		if (isset($_POST["cont_pos0"])) for ($i=0; $i<3; $i++) if ($_POST["cont_pos$i"] OR $_POST["cont_name$i"]) {$sql.="', cont_pos$n='".sql_escape($_POST["cont_pos$i"])."', cont_name$n='".sql_escape($_POST["cont_name$i"])."', cont_tel$n='".sql_escape($_POST["cont_tel$i"])."', cont_fax$n='".sql_escape($_POST["cont_fax$i"])."', cont_email$n='".sql_escape($_POST["cont_email$i"]); $n++;}

		$sql.=(isset($_POST["work"])? "', work='".sql_escape($_POST['work']):'')
		.(isset($_POST["fam_status"])? "', fam_status='".(int)$_POST['fam_status']:'')
		.(isset($_POST["partner_work"])? "', partner_work='".(int)$_POST['partner_work']:'')
		.(isset($_POST["job"])? "', job='".sql_escape($_POST['job']):'')
		.(isset($_POST["salary"])? "', salary='".(int)$_POST['salary']:'')
		.(isset($_POST["url"])? "', url='".sql_escape($_POST['url']):'')
		.(isset($_POST["icq"])? "', icq='".sql_escape($_POST['icq']):'')
		.(isset($_POST["skype"])? "', skype='".sql_escape($_POST['skype']):'')
		.(isset($_POST["msn"])? "', msn='".sql_escape($_POST['msn']):'')
		.(isset($_POST["facebook"])? "', facebook='".sql_escape($_POST['facebook']):'')
		.(isset($_POST["gorem_mufne"])? "', gorem_mufne='".sql_escape($_POST['gorem_mufne']):'')
		.(isset($_POST["darkon"])? "', darkon='".sql_escape($_POST['darkon']):'')
		.(isset($_POST["client_group"])? "', client_group='".sql_escape($_POST['client_group']):'')

		// .(isset($_POST["license_num"])? "', license_num='".sql_escape($_POST['license_num']):'')
		// .(isset($_POST["license_type"])? "', license_type='".sql_escape($_POST['license_type']):'')
		// .(isset($_POST["license_date"])? "', license_date='".str2time($_POST['license_date'],'Y-m-d'):'')

		.(isset($_POST["type_arraival"])? "', type_arraival='".sql_escape($_POST['type_arraival']):'')
		.(isset($_POST["notes"])? "', notes='".sql_escape($_POST['notes']):'')
		."' WHERE company_id=$SVARS[cid]";
		
	}else {//Insert Client
		// $acc_card = sql2array("SELECT (MAX(acc_card)+1) AS acc_card FROM clients WHERE group_id=$group_id",'acc_card','acc_card');
		
		$sql="INSERT INTO clients SET acc_card='$acc_card"
		."', company_id='".$SVARS['cid'])
		."', c_type='".(int)$_POST['c_type']
		// ."', agent_id='".(int)$_POST['agent_id']
		// ."', folder='".sql_escape($_POST['folder'])
		."', name='".($c_type==1 ? sql_escape($_POST['name']) : sql_escape($_POST['bname']))
		."', cnumber='".($c_type==1 ? sql_escape(sprintf("%09s", $_POST['passport'])) : sql_escape($_POST['hp']))
		."', birthday='".str2time($_POST['birthday'],'Y-m-d')
		."', sex='".(int)$_POST['sex']
		.(isset($_POST["fam_status"])? "', fam_status='".(int)$_POST['fam_status']:'')
		.(isset($_POST["partner_work"])? "', partner_work='".(int)$_POST['partner_work']:'')
		// ."', license_num='".sql_escape($_POST['license_num'])
		// ."', license_type='".sql_escape($_POST['license_type'])
		// ."', license_date='".str2time($_POST['license_date'],'Y-m-d')
		// ."', cperson='".sql_escape($_POST['cperson'])
		// ."', bdefine='".sql_escape($_POST['bdefine'])
		// ."', emp_num='".sql_escape($_POST['emp_num'])
		;
		$n=0;
		for ($i=0; $i<8; $i++) if ($_POST["tel$i"]) {$sql.="', tel_type$n='".(int)$_POST["tel_type$i"]."', tel$n='".sql_escape($_POST["tel$i"]); $n++;}
		$sql.="', email='".sql_escape($_POST['email']);
		$n=1;
		for ($i=1; $i<3; $i++) if ($_POST["email$i"]) {$sql.="', email_type$n='".(int)$_POST["email_type$i"]."', email$n='".sql_escape($_POST["email$i"]); $n++;}
		$n=0;
		for ($i=0; $i<3; $i++) if ($_POST["city$i"] OR $_POST["zip$i"] OR $_POST["adr$i"]) {$sql.="', adr_type$n='".(int)$_POST["adr_type$i"]."', city$n='".sql_escape($_POST["city$i"])."', zip$n='".sql_escape($_POST["zip$i"])."', adr$n='".sql_escape($_POST["adr$i"]); $n++;}
		$n=0;
		for ($i=0; $i<3; $i++) if ($_POST["cont_pos$i"] OR $_POST["cont_name$i"]) {$sql.="', cont_pos$n='".sql_escape($_POST["cont_pos$i"])."', cont_name$n='".sql_escape($_POST["cont_name$i"])."', cont_tel$n='".sql_escape($_POST["cont_tel$i"])."', cont_fax$n='".sql_escape($_POST["cont_fax$i"])."', cont_email$n='".sql_escape($_POST["cont_email$i"]); $n++;}

		// $sql.="', work='".sql_escape($_POST['work'])
		// ."', job='".sql_escape($_POST['job'])
		// ."', salary='".(int)$_POST['salary']
		// ."', url='".sql_escape($_POST['url'])
		// ."', icq='".sql_escape($_POST['icq'])
		// ."', skype='".sql_escape($_POST['skype'])
		// ."', msn='".sql_escape($_POST['msn'])
		// ."', facebook='".sql_escape($_POST['facebook'])
		// ."', gorem_mufne='".sql_escape($_POST['gorem_mufne'])
		// ."', darkon='".sql_escape($_POST['darkon'])
		// ."', client_group='".sql_escape($_POST['client_group'])
		// ."', type_arraival='".sql_escape($_POST['type_arraival'])
		// ."', notes='".sql_escape($_POST['notes'])
		// ."'";
	}
	
	if (runsql($sql)) {
		if (!$id) $id = mysql_insert_id();
		
		if (is_array($_FILES) AND !$_FILES['photo']['error'] AND is_file($_FILES['photo']['tmp_name'])) upload_photo($id);
		/*
		if (isset($_POST["fam_status"])) {// Update Family data
			runsql("DELETE FROM crm_clients_links WHERE client_id=$id AND linked_client_id=0");
			for ($i=0; $i<7; $i++) if ($_POST["f_name$i"]) {
				$links_sql.=($links_sql ? ', ':'')."($id,'".sql_escape($_POST["f_type$i"])."','".sql_escape($_POST["f_name$i"])."','".$_POST["f_tz$i"]."','".str2time($_POST["f_bdate$i"],'Y-m-d')."','".sql_escape($_POST["f_tel$i"])."','".sql_escape($_POST["f_fax$i"])."','".sql_escape($_POST["f_email$i"])."','".sql_escape($_POST["f_license_num$i"])."','".str2time($_POST["f_license_date$i"],'Y-m-d')."','$SVARS[user_id]')";
			}
			if ($links_sql AND !runsql("INSERT INTO crm_clients_links (client_id,link_type,name,passport,birthday,tel,fax,email,license_num,license_date,user_id) VALUES ".$links_sql)) die ("{success:false, msg:'תקלה בשמירת נתונים!<br>אנא נסה שנית מאוחר יותר.'}");
		}
		$tax_sql=(isset($_POST["tax_assessor"])	? "tax_assessor=".(int)$_POST['tax_assessor']:'')
			.(isset($_POST["tax_clerck"])		? ", tax_clerck=".(int)$_POST['tax_clerck']:'')
			.(isset($_POST["tax_agent"])		? ", tax_agent=".(int)$_POST['tax_agent']:'')
			.(isset($_POST["tax_broker"])		? ", tax_broker=".(int)$_POST['tax_broker']:'')
			.(isset($_POST["taxing_start"])		? ", taxing_start=".quote(str2time($_POST['taxing_start'],'Y-m-d')):'')
			.(isset($_POST["tax_programm"])		? ", tax_programm=".quote($_POST['tax_programm']):'')
			.(isset($_POST["tax_client_sourse"])? ", tax_client_sourse=".(int)$_POST['tax_client_sourse']:'')
			.(isset($_POST["tax_folder_type"])	? ", tax_folder_type=".(int)$_POST['tax_folder_type']:'')
			.(isset($_POST["tax_folder_place"])	? ", tax_folder_place=".quote($_POST['tax_folder_place']):'')
			.(isset($_POST["tax_client_status"])? ", tax_client_status=".(int)$_POST['tax_client_status']:'')
			.(isset($_POST["medical_commission"])?", medical_commission=".quote($_POST['medical_commission']):'')
			.(isset($_POST["date_status_update"])?", date_status_update=".quote(str2time($_POST['date_status_update'],'Y-m-d')):'')
			.(isset($_POST["tax_branch"])		? ", tax_branch=".(int)$_POST['tax_branch']:'')
			.(isset($_POST["date_last_report"])	? ", date_last_report=".quote(str2time($_POST['date_last_report'],'Y-m-d')):'')
			.(isset($_POST["tax_commission"])	? ", tax_commission=".quote($_POST['tax_commission']):'')
			.(isset($_POST["vat_included"])		? ", vat_included=".(int)$_POST['vat_included']:'')
			.(isset($_POST["represented"])		? ", represented=".quote($_POST['represented']):'')
			.(isset($_POST["tax_notes"])		? ", tax_notes=".quote($_POST['tax_notes']):'');
		
		if ($tax_sql AND !runsql("INSERT INTO tax_clients SET client_id=$id, $tax_sql ON DUPLICATE KEY UPDATE $tax_sql")) die ("{success:false, msg:'תקלה בשמירת נתונים!<br>אנא נסה שנית מאוחר יותר.'}");
		*/
		
		echo "{success:true, id:$id, name:'$name'}"; 
		
	}else die ("{success:false, msg:'תקלה בשמירת נתונים!<br>אנא נסה שנית מאוחר יותר.'}");
}

?>