<?

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function cats_load($table, $not_active=0) {
	if ($GLOBALS['DB_connect_error']) return;
	elseif (!$GLOBALS['DB_connected']) MySqlConnect();

	$sql="SELECT id,parent_id,title,active FROM $table ".($not_active?'':" WHERE active='1'")." ORDER BY sort, title";//
	$result=mysql_query($sql);
	if (!$result) return;
	while($row=mysql_fetch_array($result,MYSQL_ASSOC) ) {
		$cats[$row['id']]=array('title'=>$row['title'], 'parent_id'=>$row['parent_id'], 'active'=>$row['active'] );
	}
	mysql_free_result($result);
 return $cats;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function cats_select_tree($cfg, $start_id=0, $maxlevel=0, $level=0) {
  if ($cfg['cats'] && $cfg['catSubs']) { $cats=&$cfg['cats']; $catSubs=&$cfg['catSubs']; }
  else global $cats, $catSubs;
	
	if (!is_array($cats)) return "<select name='$name'></select>\n";
	foreach ($catSubs[$start_id] as $key) {
		if (!$level) $bullet='&#9679;';
		elseif ($level==1) $bullet='&#9675;';
		elseif ($level==2) $bullet='&#9632;';
		else $bullet='&#9633;';
		$prefix = !$level ? "$bullet " : str_repeat('&nbsp; &nbsp; &nbsp; ', $level)."$bullet ";
		
		if (is_array($cfg['selected']) AND in_array($key, $cfg['selected'])) $selected=true;  elseif ($key==$cfg['selected']) $selected=true;  else $selected=false;
		if ($cfg['sub_only'] AND $catSubs[$key]) $html.="<option value='' style='color:#808080'>$prefix".htmlspecialchars($cats[$key]['title'])."\n";
		else $html.="<option value='$key'".($selected?' selected':'').($cats[$key]['active']?'':" style='color:#808080'").">$prefix".htmlspecialchars($cats[$key]['title'])."\n";
		if ($catSubs[$key] AND (!$maxlevel OR ($maxlevel>$level+1))) $html.=cats_select_tree(&$cfg, $key, $maxlevel, $level+1);
	}
	if (!$level) {
		$select_html="<select name='$cfg[name]'".($cfg['add']?" $cfg[add]":'')." style='font-family:Arial; font-size:12px'>\n";
		if (is_array($cfg['first_row'])) $select_html.="<option value='{$cfg[first_row][0]}'".($cfg['first_row'][0]===$cfg['selected']?' selected':'').">{$cfg[first_row][1]}\n";
		elseif ($cfg['first_row']) $select_html.="<option value='' style='color:#808080'>$cfg[first_row]</option>\n";
		$html = $select_html.$html."</option></select>\n";
	}
 return $html;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function cats_full_name($id, $cfg='') {
	if ($cfg['cats'] && $cfg['catSubs']) { $cats=&$cfg['cats']; $catSubs=&$cfg['catSubs']; }
	else global $cats, $catSubs;
	if (!$cats[$id]) return;
	if (!$cfg['split']) $cfg['split']=' &gt; ';

	if ($cfg['root']) $cat_full_name.=$cfg['root'].$cfg['split'];

	if ($cats[$id]['parent_id']) {
		$parent=$id;
		while ($parent=$cats[$parent]['parent_id']) {
			if ($cfg['link']) $title="<a href='$cfg[link]$parent' dir=rtl>".htmlspecialchars($cats[$parent]['title'])."</a>";
			else $title="<span dir=rtl>".htmlspecialchars($cats[$parent]['title'])."</span>";
			$cat_full_name=$title.$cfg['split'].$cat_full_name;
		}
	}
	
	# add this category
	if ($cfg['link']) $title="<a href='$cfg[link]$id' dir=rtl>".htmlspecialchars($cats[$id]['title'])."</a>";
	else $title="<span dir=rtl>".htmlspecialchars($cats[$id]['title'])."</span>";
	$cat_full_name.="<span dir=rtl>$title</span>";
 return $cat_full_name;
}#cats_full_name()



//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function cats_get_subs($start_id, $maxlevel=0, $level=0) {
 global $cats, $catSubs;
	if (!$start_id) return;
	if (!is_array($cats)) return;
	foreach ($catSubs[$start_id] as $key) {
		$subs[]=$key;
		if ($catSubs[$key] AND (!$maxlevel OR ($maxlevel>$level+1))) $subs = array_merge($subs, cats_get_subs($key, $maxlevel, $level+1) );
	}
 return $subs;
}

/*
//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function cats_tree($start_id=0, $maxlevel=0, $level=0) {
 global $cats, $catSubs;
	if (!is_array($cats)) return;
	foreach ($catSubs[$start_id] as $key=>$val) {
		$prefix = !$level ? "� " : str_repeat('&nbsp; &nbsp; &nbsp;', $level).' &#9702; ';
		$html.= "$prefix $key - {$cats[$key][title]} ($level)<br>\n";
		if ((isset($catSubs[$key])) AND (!$maxlevel OR ($maxlevel>$level+1))) $html.=makebranch($key, $maxlevel, $level+1);
	}
 return $html;
}
*/


/*
#############################################################################################
#get all cats from $table to array, if $disabled true include disabled too
function cats_load($table, $not_active=0) {
 global $ln;
	$sql="SELECT * FROM $table ".($not_active?'':" WHERE active='1'")." ORDER BY sort, title";
	$cats_ar=sql2array($sql, 'id');
 return $cats_ar;
}


#############################################################################################
# for admin use!!!
# bulletlist_cat(array cats_ar, int [cat_id], str [bulletlist]) - return bullet three of all categories in array
# $cats_ar - array of categories *** $cat_id AND $bulletlist for internal use
function bulletlist_cat($cats_ar, $cat_id='', $bulletlist='') {
 global $PHP_SELF,$table;
	if (!is_array($cats_ar)) return false;
	reset($cats_ar);
	$bulletlist.="<ul dir=rtl>";
	
	foreach ($cats_ar as $key=>$row) {
		$bulletlist.="<li>$key - $row[title] ($row[pointers])</li>";
	}
	$bulletlist.="</ul>\n";	
 return $bulletlist;
}

#############################################################################################
# for admin (shows disabled)!!!
# array cats_to_ar(str table, int [cat_id], int [level], array [cats_ar]) - return array of categories
# table - table where to get categories
# cat_id - category to start from
# level - levels to go down
# cats_ar - pointer to existing array (for internal use, must be clear)
function cats_to_ar($table, $cat_id='', $level=0, $cats_ar='') {
	if ($start_cat=='' AND $cat_id!='') $start_cat=$cat_id;
	if ($cat_id=='') $query="SELECT * FROM $table WHERE (pointers='' OR pointers IS NULL) ORDER BY name";
	else $query="SELECT * FROM $table WHERE (pointers LIKE '% $cat_id' OR pointers='$cat_id') ORDER BY name";
	//echo "\$query=$query<br>";
	$result=mysql_query($query);
	if (mysql_num_rows($result)) {
		while($row=mysql_fetch_array($result,MYSQL_ASSOC) ) {
			$cats_ar[$row[cat_id]]=array('name'=>$row['name'], 'pointers'=>$row['pointers'], 'enable'=>$row['enable']);
			if ($level>0) cats_to_ar($table, $row[cat_id],$level-1, &$cats_ar);
		}
	}
	mysql_free_result($result);
 return $cats_ar;
}




######################################################################################
function cats_has_sub($cats_ar, $id) {
	$id=(int)$id;
	if (!$cats_ar OR !$id) return;
	if ($cats_ar[$id]['sub']) return true;

	foreach ($cats_ar as $key=>$row) {
		if ($id==$row['parent_id']) {
			$cats_ar[$id]['sub']=true;
			return true;
		}		
	}
}


#############################################################################################
# returns array with pointers to all parent categories
function cats_get_pointers_ar($cats_ar, $id, $pointers=false) {
	$pointers=cats_get_pointers($cats_ar, $id);
	if (!$pointers) return;
	$pointers_ar=explode(" ", $pointers);
 return $pointers_ar;
}

#############################################################################################
# returns string with pointers to all parent categories
function cats_get_pointers($cats_ar, $id, $pointers='') {
	if (!$cats_ar OR !$id) return;
	if (!$cats_ar[$id]['parent_id']) return $pointers;
	if ($cats_ar[$id]['parent_id']) {
		$pointers=( $pointers ? "{$cats_ar[$id]['parent_id']} $pointers" : $cats_ar[$id]['parent_id'] );
		if ($id==$cats_ar[$id]['parent_id']) return;	# points to itself!
		$pointers=cats_get_pointers($cats_ar, $cats_ar[$id]['parent_id'], $pointers);
		return $pointers;
	}
}


#############################################################################################
# get array of sub categories
function cats_get_subcats_list($cats_ar, $id, $levels=0, $subcats='') {
	$id=(int)$id;
	if (!$cats_ar OR !$id) return;

	if (cats_has_sub($cats_ar, $id)) {
		foreach ($cats_ar as $cat=>$cat_ar) {
			if ($cats_ar[$cat]['parent_id']==$id) {
				$subcats[]=$cat;
				if ($levels AND cats_has_sub($cats_ar, $cat) AND $id!=$cats_ar[$id]['parent_id']) cats_get_subcats_list($cats_ar, $cat, $levels-1, &$subcats);
			}
		}
		return $subcats;
	}
}


#############################################################################################
# str cat_full_name(int id, array cats_ar, link{1/str})
# get - $id category name, $cats_ar - array of all categories, $links - if true-shows links
# return string with full cat name (Main\name1\name2)
function cats_cat_full_name($id, $cats_ar, $link='') {
 global $LNG, $ln;
	if (!$cats_ar[$id]) return;
	//if ($link==1) $link=$GLOBALS['SELF'];
	$split=' -&gt; ';//"<IMG src='skins/default/bs_cat.gif' width=12 height=6 align=middle border=0>";

	$pointers_ar=cats_get_pointers_ar($cats_ar, $id);
	
	if (is_array($pointers_ar)) {
		//$pointers_ar=explode(" ", $cats_ar[$id]['pointers']);
		foreach ($pointers_ar as $cat) {
			if ($link) $cat_name = "<A HREF='{$link}$cat'>{$cats_ar[$cat][title]}</A>";
			else $cat_name = $cats_ar[$cat]['title'];
			
			if ($ln=='he') $cat_name="<span dir=rtl>$cat_name</span>";
			$cat_full_name.=($cat_full_name?$split:''). $cat_name;
		}
	}

	# add this category
	$cat_name = $cats_ar[$id]['title'];
	if ($ln=='he') $cat_name="<span dir=rtl>$cat_name</span>";
	$cat_full_name.=($cat_full_name?$split:''). $cat_name;
	
 return "<div dir=ltr align=right><a href='./'>$LNG[Home]</a>{$split}$cat_full_name</div>";
}#cats_cat_full_name()
*/


?>