
//Agent Add-Edit Window
var adrStore     = [[1,'בית'],[2,'חברה/עסק'],[3,'עבודה'],[4,'למשלוח דואר'],[0,'אחר']];
SalesAgetWin = function(cfg) {
	var d=cfg.data;
	var modulesAccess = new Object();
	var modulesSubElements = new Object();
	
	this.addTel=function(){
		Ext.getDom('fax_tels').options.add(new Option(Ext.getCmp('fax_tel').getValue()));
		Ext.getCmp('fax_tel').setValue('');
	};
	this.removeTel=function(){
		var sel=Ext.getDom('fax_tels');
		sel.remove(sel.selectedIndex);
	};
	this.checkTel=function(t){
		t=t.replace(/[^0-9 -]+/g, '');
		Ext.getCmp('fax_tel').setRawValue(t);
		var t1=t.replace(/[^0-9]+/g, '');
		var isValid=(/^0[0-9]{8,9}$/).test(t1);
		if (isValid) Ext.getCmp('fax_tel_add').enable();
		else Ext.getCmp('fax_tel_add').disable();
		return true;
	};

	this.tabs = new Ext.TabPanel({
		baseCls:'x-plain', plain:true, activeTab:0,
		defaults: {xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'padding:5px 5px 0 5px;', height:240},
			items: [
				{title:'כללי', autoHeight:true,
				items: [
					{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, 
					items:[
						 {xtype:'label', text:'שם פרטי :'},{name:'fname', xtype:'textfield', width:135,  allowBlank:false}
						,{xtype:'label', text:'שם משפחה :', style:'padding-right:10px'},{name:'lname', xtype:'textfield', width:135, allowBlank:false}
						,{xtype:'label', text:'מין :'},{hiddenName:'sex', xtype:'combo', width:60, emptyText:'-בחר-', store:[[1,'זכר'],[2,'נקבה']], mode:'local', listClass:'rtl',typeAhead:true, triggerAction:'all', forceSelection:true,allowBlank:false}
						,{xtype:'label', text:'מספר ת.ז.:', style:'padding-right:10px'},{name:'passport', xtype:'textfield', width:135,allowBlank:false}
						,{xtype:'label', text:'תאריך לידה :'},{name:'birthday', xtype:'datefield', width:85, format:'d/m/Y', cls:'ltr', vtype:'daterange', value:(d ? d.birthday : ''),allowBlank:false},
						// ,{xtype:'label', text:'מחלקה :', style:'padding-right:10px'},{hiddenName:'department', xtype:'combo', allowBlank:false, width:135, store:Departments, typeAhead:true, triggerAction:'all', forceSelection:true,
							// listeners: {
								// select: function(e,r){
									// var enab_mod = r.data.field3.toString().split(',');
									// if(enab_mod[0] == "") enab_mod = [];
									// reset previous values
									// var btn = null;
									// Ext.each(this.tabs.find('name','module_groups'),function(group){
										// btn  = group.find('xtype','splitbutton')[0];
										// btn.toggle(false);
										// btn.handler(btn);
									// },this);
									
									// for(var i = 0; i < enab_mod.length; i++) {// set new values
										// btn = this.tabs.find('name','mod['+enab_mod[i]+']')[0];
										// if (btn){
											// btn.toggle(true);
											// btn.handler(btn);
										// }
									// }
							// },scope:this}
						// }
										
					]}
				,{xtype:'fieldset', title:'טלפונים / פקסים', labelAlign:'top', cls:'rtl', autoHeight:true, style:'margin-top:3px; padding:5px',
					items:[
						 {layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', items:[
						 {xtype:'label', text:'טל 1 :'},{name:'tel0', xtype:'textfield', width:100,allowBlank:false}
						,{xtype:'label', text:'טל 2 :'},{name:'tel1', xtype:'textfield', width:100}
						,{xtype:'label', text:'פקס  :'},{name:'fax', xtype:'textfield',  width:100}
						,{xtype:'label', text:'דוא"ל :'},{name:'email', xtype:'textfield', width:300, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						,{xtype:'label', text:'מספרי פקס נכנסים :'},{name:'fax_in', xtype:'textfield',  width:200}
						]}
					]
				}
				,{xtype:'fieldset', title:'כתובת', cls:'rtl', style:'margin-top:5px; padding:2px', autoHeight:true,
					items:[
						{layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain', id:'adr0', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{html:'סוג:'},{html:'עיר:',style:'margin:0 5 0 5px;'},{html:'מיקוד:'},{html:'כתובת/ת.ד.:', colspan: 2,style:'margin:0 5 0 5px;'},
							{xtype:'combo', hiddenName:'adr_type0', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore,allowBlank:false},
							{name:'city0', xtype:'textfield',style:'margin:0 5 0 5px;', width:120,allowBlank:false},
							{name:'zip0', xtype:'textfield', width:60},
							{name:'adr0', xtype:'textfield',style:'margin:0 5 0 5px;', width:380,allowBlank:false}
						]}
					]
				}
				,{xtype:'fieldset', title:'שימוש במערכת', labelAlign:'top', cls:'rtl', autoHeight:true, style:'margin-top:3px; padding:0 5px',
					items:[
						{layout:'table', layoutConfig: {columns:2}, baseCls: 'x-plain', items:[
							{xtype:'checkbox',checked:true, boxLabel:'פעיל', name:'active'},
							// {xtype:'checkbox',hidden:(!user.isAdmin),checked:false, boxLabel:'הרשאות אדמין', name:'isAdmin'}
						]},
						{layout:'table', layoutConfig: {columns:5}, baseCls: 'x-plain', items:[
							 {xtype:'label', text:'שם משתמש :'},{html:'<b>(אותיות לועזיות)</b>', baseCls: 'x-plain',style:'color:green;font-size:8px;'},{name:'username', xtype:'textfield', width:150}
							,{xtype:'label',name:'passLabel', text:'סיסמה :'},{name:'password',inputType:'password',minLength:6, xtype:'textfield', width:150}
						]}
					]
				}
				,{xtype:'fieldset', title:'הערות', labelAlign:'top', cls:'rtl', autoHeight:true, style:'margin-top:3px; padding:0 5px',
					items:{xtype:'textarea', name:'notes', width:'100%', height:29, grow:true, growMin:18, growMax:100, growAppend:''}
				}
				,{xtype:'hidden', name:'id'}
			]}
			// ,{title:'מודולים', autoHeight:true, autoScroll:true, layout:'table', name:'modulesItems', layoutConfig:{columns:5}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl', style:'margin:14px'}}
		]
	});
	
	this.tabs.on('tabchange', function(tabPanel, tab) {this.tabs.doLayout(); this.syncShadow();},this);
	this.form = new Ext.form.FormPanel({
		labelWidth:75, autoHeight:true, baseCls:'x-plain',
		items:[this.tabs]
	});
	
	SalesAgetWin.superclass.constructor.call(this, {
		title:(d.id ? 'עידכון' : 'הוספת חדש/ה'),
		iconCls:(d.id ? 'edit' : 'add'),
		layout:'fit', modal:true,
		width:800, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:0 5px;', border:false,
		items:this.form,
		buttons:[
	         {text:'שמור', iconCls:'save', handler:function(){this.submitForm();}, scope:this},
	         {text:'ביטול', iconCls:'close', handler:function(){this.close();}, scope:this}]
	});
	
	this.loadWindowData = function(){
//		if(!user.isAdmin){
//			this.tabs.find('name','modulesItems')[0].items.each(function(el){
//				el.items.items[1].addListener('check',function(o,checked){o.reset();},this);
//			},this);
//		}
		if (d.id)this.formLoad();
		this.syncShadow();
	};
	
	this.on('show', function(){
		Ext.Ajax.request({// get salesagent modules list
			url:'?m=common/common_func&f=get_modules_list',
			params:{user_status:1},// 1-salesagent
			success: function(r,o){
				r = Ext.decode(r.responseText);
				if (!ajRqErrors(r)){
					var modulesItems = new Array();
					
					for(var i = 0; i < r.data.length; i++){
						var menuItems = new Array();
						if(user.isAdmin)
							for(var j = 0; j < r.data[i].elements.length; j++){
								menuItems.push({xtype:'checkbox', moduleName:'mod['+r.data[i].id+']',style:'margin-right:3px;margin-left:8px;',
									checked:false, boxLabel:r.data[i].elements[j].element_name_heb, name:r.data[i].elements[j].element_id, handler:function(o, checked){
										if(typeof(modulesSubElements[o.moduleName]) == 'undefined')
											modulesSubElements[o.moduleName] = new Object();
										modulesSubElements[o.moduleName][o.name] = checked;
									}});
							}
						
						modulesItems.push(
							{xtype:'container', name:'module_groups', items:[
							    {html:"<div class='"+(r.data[i].shortcutIconCls ? r.data[i].shortcutIconCls : 'demo-grid-shortcut')+
							    	"' style='width:100%; height:64px; background-position:center top; background-repeat:no-repeat;'" +
							    	" onClick='var cmp = Ext.getCmp(\"mod["+r.data[i].id+"]\"); cmp.pressButton(cmp, !cmp.pressed);'></div>",
							    	style:'text-align:center;',baseCls:'x-plain',cls:'rtl'}     
							   ,{xtype:'splitbutton', pressed:false, enableToggle:user.isAdmin, width:120, name:'mod['+r.data[i].id+']', id:'mod['+r.data[i].id+']', iconCls:'disable', text:r.data[i].title_he,
								   handler:function(o){
									   if(!user.isAdmin) return;
									   var iconCls  = o.pressed ? 'enable' : 'disable';
									   o.setIconClass(iconCls);
									   modulesAccess[o.name] = o.pressed;
									},
									pressButton:function(o,state){
										if(!user.isAdmin) return;
										o.toggle(state);
										o.handler(o);
									},
									menu:new Ext.menu.Menu({items:menuItems})
								}
							 ]}	
						);
					}
					this.form.find('name','modulesItems')[0].add(modulesItems);
					this.loadWindowData();
				}
			},
			failure: function(r){Ext.Msg.alert('Error', 'Connection error. Unable to load modules list.');},
			scope: this
		});
	}, this);
	
	this.formLoad=function() {
		// this.getEl().mask('...טוען');
		this.form.load({
			url: "?m=salesagents/salesagents&f=salesagent_info",
			params:{id:d.id},
			reader: new Ext.data.JsonReader({
				root: 'data',
				successProperty: 'success',
				totalProperty: 'total'
			}),
			success: function(f,a){
				d=a.result.data;

				var cmp = null;
				for(var m in d.modules) {
					if (typeof(parseInt(m)) == 'number'){
						cmp = this.form.find('name','mod['+m+']')[0];
						if(cmp){
							if(!user.isAdmin){
								cmp.setIconClass('enable');
							}else{
								cmp.pressButton(cmp, true);
								for(var k = 0; k < d.modules[m].length; k++){
									cmp.menu.find('name',d.modules[m][k])[0].setValue(true);
								}
							}
						}
					}
				}

				this.form.form.setValues(d);
				this.syncShadow();
				this.getEl().unmask();
			},
			failure: function(){ this.getEl().unmask(); },
			scope:this
		});
	};
	
	this.proceedSubmit = function(){
		for(var mod in modulesAccess){
			if(!modulesAccess[mod]){
				delete modulesAccess[mod];
				continue;
			}
			modulesAccess[mod] = new Array();
			for(var elId in modulesSubElements[mod]){
				if(modulesSubElements[mod][elId]) modulesAccess[mod].push(elId);
			}
			modulesAccess[mod] = Ext.encode(modulesAccess[mod]);
		}

		this.form.form.submit({
			url: "?m=salesagents/salesagents&f=edit",
			waitTitle:'אנא המתן...', waitMsg:'טוען...',scope:this,
			params:modulesAccess,
			success: function(f,a){if (cfg.callBack && typeof(cfg.callBack)=='function') cfg.callBack(); this.close();},
			failure: function(r,o){this.close(); ajRqErrors(o.result);}
		});
	};
	
	this.submitForm = function() {
		if(!this.form.form.isValid()){
			Ext.MessageBox.show({width:300, title:'הודעת המערכת', msg:'חובה להזין את כל השדות הנדרשים!',icon:Ext.MessageBox.ERROR, buttons:Ext.MessageBox.OK, scope:this});
			return;
		}
		
		var username = this.form.find('name','username')[0].getValue();
		var fax_in_num = this.form.find('name','fax_in')[0].getValue();
		
		// if(username != ''){
		// 	Ext.Ajax.request({
		// 		url:'?m=agents/agents&f=validate_username',
		// 		params:{username:username, id:d.id},
		// 		success: function(r,o){
		// 			var res = Ext.decode(r.responseText);
		// 			if(res.success) 
		// 				this.proceedSubmit();
		// 			else	
		// 				Ext.MessageBox.show({width:300, title:'הודעת המערכת', msg:'שם משתמש כבר קיים במערכת. נא לבחור שם משתמש אחר.',icon:Ext.MessageBox.ERROR, buttons:Ext.MessageBox.OK, scope:this});
		// 		},
		// 		failure: function(r){Ext.Msg.alert('Error', 'Request failed.');},
		// 		scope: this
		// 	});
		// }else this.proceedSubmit();

		if(username != ''){
			Ext.Ajax.request({
				url:'?m=agents/agents&f=validate_username',
				params:{username:username, id:d.id},
				success: function(r,o){
							var res = Ext.decode(r.responseText);
							if(!res.success) 
								Ext.MessageBox.show({width:300, title:'הודעת המערכת', msg:'שם משתמש כבר קיים במערכת. נא לבחור שם משתמש אחר.',icon:Ext.MessageBox.ERROR, buttons:Ext.MessageBox.OK, scope:this});
				},
				failure: function(r){Ext.Msg.alert('Error', 'Request failed.');},
				scope: this
			});
		} 
		if(fax_in_num != ''){
			Ext.Ajax.request({
				url:'?m=salesagents/salesagents&f=faxnumber_check',
				params:{fax_in:fax_in_num},
				success: function(r,o){
					var res = Ext.decode(r.responseText);
					if(res.success) 
						this.proceedSubmit();
					else	
						Ext.MessageBox.show({width:300, title:'הודעת המערכת', msg:'מספר פקס לא שייך לחברה.',icon:Ext.MessageBox.ERROR, buttons:Ext.MessageBox.OK, scope:this});
				},
				failure: function(r){Ext.Msg.alert('Error', 'Request failed.');},
					scope: this
			});
				
		} else this.proceedSubmit();
		
	};
};
Ext.extend(SalesAgetWin, Ext.Window);

SalesAgetTab = function(d) {
	this.d=d;
	this.salesAgetWin = function(){var win=new SalesAgetWin({data:this.d, callBack:function(d){this.onSalesagentChange();}.createDelegate(this)}); win.show(); win.center();};
	
	this.onSalesagentChange = function() {
		Ext.Ajax.request({
			url: "?m=salesagents/salesagents&f=salesagent_info",
			params:{id:this.d.id},
			success: function(r) {
				var data = Ext.decode(r.responseText).data;
				data.full_name=data.fname + '  ' + data.lname;
				Ext.getCmp('salesagentInfo'+this.d.data.id).tpl.overwrite(Ext.getCmp('salesagentInfo'+this.d.data.id).body, data);
			},
			failure: function() {Ext.Msg.alert('Fail', 'Request failed.');},
			scope: this
		});	
	};
		
	var items = [];
	var elementsAccess = new Object();
	for(var i = 0; i < Ms.salesagents.user_element_access.length; i++){
		for(var prop in Ms.salesagents.user_element_access[i])
			elementsAccess[prop] = Ms.salesagents.user_element_access[i][prop];
	}

	this.salesagentInfo = {
		xtype:'panel',
		title:'פרטי מפקחים/עובדים',
		id:'salesagentInfo'+this.d.data.id,bodyStyle:'background-color:#FFFFEE;'
		,baseCls: 'x-plain'
		,tbar:[{text:'עדכון פרטים', iconCls:'edit', handler:this.salesAgetWin, scope:this}]
		,tpl: new Ext.Template(
				'<table id=user_info dir=rtl width=100%><tr>',
				'<td nowrap><img src="skin/icons/user.gif" align=absmiddle> <b>שם:</b></td><td width=30%><b>{full_name}</b></td>',
				'<td nowrap><img src="skin/icons/zehut.gif" align=absmiddle> <b>ח.פ./ת.ז:</b></td><td width=10% nowrap>{passport}</td>',
				'<td nowrap><img src="skin/icons/date.gif" align=absmiddle> <b>ת.לידה:</b></td><td width=15%>{birthday}</td>',
				'</tr>',
				'<tr><td nowrap><img src="skin/icons/telephone.gif" align=absmiddle> <b>טלפונים:</b></td><td width=30%>{phone_txt}</td>',
				'<td nowrap><img src="skin/icons/fax.gif" align=absmiddle> <b>פקס:</b></td><td width=30%>{fax}</td>',
				'<td nowrap><img src="skin/icons/email.gif" align=absmiddle> <b>דוא"ל:</b></td><td>{email_txt}</td>',
				'</tr>',
				'<tr>',
					'<td nowrap><img src="skin/icons/house.gif" align=absmiddle><b>מחלקה:</b></td><td width=30%>{depart}</td>',
					'<td nowrap><img src="skin/icons/house.gif" align=absmiddle><b>כתובת:</b></td><td width=30%>{adr_txt}</td>',
				'</tr>',
				'<tr><td nowrap><img src="skin/icons/note.gif" align=absmiddle><b>הערות:</b></td><td colspan=6>{notes}</td></tr></table>'
		)
		,listeners:{
			render:function(){this.onSalesagentChange();},
			scope:this
		}
	};
	
	items.push(this.salesagentInfo);
	if(user.isAdmin || elementsAccess.files){
		this.filesList 	= new FilesGrid({client_id:this.d.id, client_type:'salesagent'});
		this.filesList.on('activate', function(tabPanel, tab) {this.filesList.store.load();},this);
		items.push(this.filesList);
	}
	if(user.isAdmin || elementsAccess.treatmentManagment){
		this.contactsList= new ContactGrid({
			user_id:this.d.id,
			user_name:this.d.data.fname + '  ' + this.d.data.lname,
			email:this.d.data.email
		});
		this.contactsList.on('activate', function(tabPanel, tab) {this.contactsList.store.load();},this);
		items.push(this.contactsList);
	}
	if(user.isAdmin || elementsAccess.agentsList){
		this.agentsList = new AgetsGrid({user_id:this.d.id, user_type:'salesagent'});
		//this.agentsList.on('activate', function(tabPanel, tab) {this.agentsList.store.load();},this);
		items.push(this.agentsList);
	}
	
	SalesAgetTab.superclass.constructor.call(this, {
		id:'userTab_'+this.d.id,
		title:d.data.lname+' '+d.data.fname,
		iconCls:'user-business', closable:true, autoScroll:true, activeTab:0,
		items:items
    });
};
Ext.extend(SalesAgetTab, Ext.TabPanel);

SalesAgetsGrid = function(cfg) {
	Ext.apply(this, cfg);

	this.store = new Ext.data.JsonStore({
		url:'?m=salesagents/salesagents&f=list', 
		totalProperty:'total', 
		root:'data',
		fields:['id','fname','lname','isAdmin','date_added','department','tel0','tel1','fax','email','fax_in'],
		sortInfo:{field:'name', direction:'ASC'},
		remoteSort:true
	});

	this.salesAgetWin = function(d){var win=new SalesAgetWin({data:d, callBack:function(d){this.store.reload();}.createDelegate(this)}); win.show(); win.center();};

	this.searchField = new GridSearchField({store:this.store, width:150});
		
	this.hlStr = hlStr;
	
	this.nameRenderer = function(str, p, record){
		return "<span style='display:block;height:16px;padding-right:18px;background:url(skin/icons/user.gif) no-repeat right top;'>" +this.hlStr(str)+ "</span>";
	};
	
    this.columns = [
		{dataIndex:'email', id:'email', header:'דוא"ל', width:70, sortable:true, renderer:function(val){return '<a href=# onClick="callEmailCompose(\''+val+'\')">'+this.hlStr(val)+'</a>'; }, scope:this},
		{dataIndex:'fax', header:'פקס', width:80, sortable:true, renderer:this.hlStr.createDelegate(this) }, 
		{dataIndex:'tel0', header:'טלפונים', width:140, renderer:function(v,p,r){return this.hlStr(r.json.tel0+(r.json.tel0 && r.json.tel1 ? ', ':'' )+r.json.tel1);}, scope:this }, 
		// {dataIndex:'department', header:'מחלקה', width:170, sortable:true },
		{dataIndex:'date_added', header:'תאריך קליטה', width:70, sortable:true},
		{dataIndex:'fname', header:'שם', width:170, sortable:true, renderer:function(v,p,r){return "<span style='display:block;height:16px;padding-right:18px;background:url(skin/icons/user.gif) no-repeat right top;'>"+this.hlStr(r.json.lname)+" "+this.hlStr(v)+ "</span>";}, scope:this}
	];
	
	SalesAgetsGrid.superclass.constructor.call(this, {
		title:'רשימת עובדים',iconCls:'grid',
		enableColLock:false,loadMask:true,stripeRows:true,enableHdMenu: false,
		autoExpandColumn: 'email',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'חיפוש:', this.searchField
			,'-' , 'סטאטוס:'
			, new Ext.form.ComboBox({store:[[1,'פעילים'],[0,'מבוטלים']], id:'activeFilter', value:1, width:80, listClass:'rtl', triggerAction:'all', forceSelection:true, 
				listeners:{
					select:function(f){	this.store.reload();}, 
					change:function(f){if(f.getRawValue()==''){f.setValue(0);this.store.reload();} },
					scope:this
				}
			})
			,'-' ,{ text:'חדש', iconCls:'add', handler:function(){this.salesAgetWin({});}, scope:this }
			,'-' ,{ text:'עדכן',id:'salesagent_edit', iconCls:'edit', handler:function(){this.salesAgetWin(this.getSelectionModel().getSelected());}, disabled:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
    });

	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			q:this.searchField.getRawValue().trim(),
		    active:Ext.getCmp('activeFilter').getValue()
		};
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);
	
	// this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('salesagent_edit').setDisabled(sm.getCount()!=1);
	}, this);
	
	// open user in new tab
	this.openSalesagent = function(d){
		var tab;
		if(!(tab=this.tabs.getItem('userTab_'+d.id))) {tab=new SalesAgetTab(d); this.tabs.add(tab); }
		this.tabs.setActiveTab(tab);
	};

	// doubleClick
	this.on('rowdblclick', function(grid, row, e){
		var data = grid.getSelectionModel().getSelected();
		this.openSalesagent(data);
	});
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.getSelectionModel().selectRow(row,false);
		this.srData=this.store.getAt(row);
		
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'פתח כרטיס', iconCls:'user_open', scope:this, handler:function(){this.openSalesagent(this.srData);} }
				,{text:'עדכן פרטים', iconCls:'edit', scope:this, handler:function(){this.salesAgetWin(this.srData);} }
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.on('render', function(){ this.store.load();});
};
Ext.extend(SalesAgetsGrid, Ext.grid.GridPanel);

Ms.salesagents.run = function(cfg){

	var m=Ms.salesagents;
	//var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var salesagentsTabs = new Ext.TabPanel({activeTab:0, border:false, defaults:{autoScroll:true}, enableTabScroll:true});
		salesagentsTabs.add(new SalesAgetsGrid({tabs:salesagentsTabs}));
		win = Crm.desktop.createWindow({ maximized:false,width:Crm.desktop.getWinWidth()-100,height:Crm.desktop.getWinHeight()-50,items: salesagentsTabs}, m);
	}
	win.show();
};