CREATE SCHEMA `crm_5` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;


USE `crm_5`;

/* Create table in target */
CREATE TABLE `acc_accept_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`accept_type` enum('accept','invoice') COLLATE hebrew_general_ci NOT NULL  , 
	`debt_card` int(11) NOT NULL  COMMENT 'debt/credit card' , 
	`accept_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`ptype` enum('מזומן','שיק','אשראי','ה.קבע/ה.בנקאית') COLLATE hebrew_general_ci NOT NULL  , 
	`sum` decimal(10,2) NOT NULL  , 
	`payments_num` tinyint(4) NOT NULL  COMMENT 'מספר תשלומים' , 
	`bank` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`dept` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`account` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`number` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`payment_status` tinyint(4) NOT NULL  , 
	`paid` float NOT NULL  COMMENT 'שולם' , 
	`invoice_id` int(11) NOT NULL  COMMENT 'חשבונית' , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_bank_accounts`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`bank` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`branch` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`number` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`type` int(11) NOT NULL  , 
	`acc_card` int(11) NOT NULL  , 
	`contact_person` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`tel` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`fax` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`balance` float NOT NULL  , 
	`masab_debt_company` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_debt_salary` varchar(8) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_debt_supplier` varchar(8) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_credit_company` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_credit_billing` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_banks`(
	`bank_code` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`bank_name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`branch_code` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`branch_name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`address` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`zip` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`city` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`tel` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`fax` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`parent` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`date_open` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`date_close` varchar(255) COLLATE hebrew_general_ci NOT NULL  
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_billing_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`billing_type` enum('bank','card') COLLATE hebrew_general_ci NOT NULL  , 
	`client_id` int(11) NOT NULL  , 
	`doc_id` int(11) NOT NULL  , 
	`directive_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`ptype` enum('מזומן','שיק','אשראי','ה.קבע/ה.בנקאית') COLLATE hebrew_general_ci NOT NULL  , 
	`sum` float NOT NULL  , 
	`bank` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`dept` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`account` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`invoice` int(11) NOT NULL  COMMENT 'חשבונית' , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_cards`(
	`id` int(11) NOT NULL  auto_increment , 
	`class_id` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`card_id` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sub1` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sub2` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sub3` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) , 
	UNIQUE KEY `card_id_3`(`class_id`,`card_id`) , 
	KEY `card_id`(`class_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_counters`(
	`group_id` int(11) NOT NULL  , 
	`expenses` int(11) NOT NULL  , 
	`accept` int(11) NOT NULL  , 
	`accept_tmp` int(11) NOT NULL  , 
	`orders` int(11) NOT NULL  , 
	`invoice_credit` int(11) NOT NULL  , 
	`invoice_debt` int(11) NOT NULL  , 
	`invoice_accept` int(11) NOT NULL  , 
	`deposit` int(11) NOT NULL  , 
	`refund` int(11) NOT NULL  , 
	`cheque` int(11) NOT NULL  , 
	`payments` int(11) NOT NULL  , 
	`set_balance` int(11) NOT NULL  , 
	`store_receive` int(11) NOT NULL  , 
	`store_send` int(11) NOT NULL  , 
	PRIMARY KEY (`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_deposit_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`deposit_id` int(11) NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`accept_date` date NOT NULL  , 
	`cheque_id` int(11) NOT NULL  , 
	`sum` float NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `depozit_id`(`deposit_id`,`type`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_deposits`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`num` int(11) NOT NULL  , 
	`account_id` int(11) NOT NULL  , 
	`sum` float NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`printed` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `account_id`(`account_id`) , 
	KEY `user_id`(`user_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_docs`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`num` int(11) NOT NULL  , 
	`client_id` int(11) NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	`parent_doc_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sum` float NOT NULL  , 
	`account_id` int(11) NOT NULL  , 
	`debt` float NOT NULL  , 
	`credit` float NOT NULL  , 
	`VAT` float NOT NULL  , 
	`vat_sum` float NOT NULL  COMMENT 'סכום של מע\"מ' , 
	`paid` float NOT NULL  , 
	`currency` enum('NIS','USD','EUR') COLLATE hebrew_general_ci NOT NULL  COMMENT 'מטבע' , 
	`rate` float NOT NULL  COMMENT 'שער' , 
	`tax_clear` tinyint(4) NOT NULL  COMMENT 'ניקוי מס במקור' , 
	`orders_ids` varchar(255) COLLATE hebrew_general_ci NOT NULL  COMMENT 'הזמנות' , 
	`over_order` float NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`printed` tinyint(4) NOT NULL  , 
	`exported` tinyint(4) NOT NULL  , 
	`export_date` datetime NOT NULL  COMMENT 'תאריך העברה' , 
	`storno` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `client_id`(`client_id`) , 
	KEY `user_id`(`user_id`) , 
	KEY `type`(`type`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_expense_ext`(
	`doc_id` int(11) NOT NULL  , 
	`sup_num` varchar(255) COLLATE hebrew_general_ci NOT NULL  COMMENT 'עוסק מורשה של ספק' , 
	`sup_doc_date` date NOT NULL  , 
	`sup_doc_num` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`expense_type` tinyint(4) NOT NULL  , 
	`rep_period` varchar(255) COLLATE hebrew_general_ci NOT NULL  COMMENT 'חודש דיווח' , 
	`paid` float NOT NULL  , 
	KEY `doc_id`(`doc_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_expense_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`acc_card` int(11) NOT NULL  COMMENT 'מפתח חשבשבת' , 
	`doc_id` int(11) NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`unit` int(11) NOT NULL  , 
	`amount` float NOT NULL  , 
	`price` float NOT NULL  , 
	`sum` float NOT NULL  , 
	`VAT` decimal(5,2) NOT NULL  , 
	`VAT_sum` decimal(10,2) NOT NULL  , 
	`sum_tax` float NOT NULL  COMMENT 'sum+vat' , 
	KEY `id`(`id`) , 
	KEY `card_id`(`doc_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_expenses_group`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`VAT` decimal(5,2) NOT NULL  , 
	`acc_card` int(11) NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_invoice_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`invoice_id` int(11) NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`code` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`unit` int(11) NOT NULL  , 
	`amount` float NOT NULL  , 
	`price` float NOT NULL  , 
	`sum` float NOT NULL  , 
	`VAT` smallint(6) NOT NULL  , 
	`sum_tax` float NOT NULL  COMMENT 'sum+vat' , 
	KEY `id`(`id`) , 
	KEY `invoice_id`(`invoice_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_operations`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`doc_id` int(11) NOT NULL  , 
	`credit_class` int(11) NOT NULL  COMMENT 'מיון' , 
	`credit_card` int(11) NOT NULL  , 
	`credit_subcard` int(11) NOT NULL  , 
	`debt_class` int(11) NOT NULL  COMMENT 'מיון' , 
	`debt_card` int(11) NOT NULL  , 
	`debt_subcard` int(11) NOT NULL  , 
	`sum` float(12,2) NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `credit_class`(`credit_class`,`credit_card`) , 
	KEY `debt_class`(`debt_class`,`debt_card`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_orders`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`client_id` int(11) NOT NULL  , 
	`agent_id` int(11) NOT NULL  , 
	`pattern_id` int(11) NOT NULL  , 
	`num` int(11) NOT NULL  , 
	`num_add` tinyint(4) NOT NULL  , 
	`date` date NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`start` datetime NOT NULL  , 
	`end` datetime NOT NULL  , 
	`payments` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`pay_start` datetime NOT NULL  , 
	`status` int(11) NOT NULL  , 
	`sms` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`importance` enum('0','1','2') COLLATE hebrew_general_ci NOT NULL  , 
	`sum` decimal(10,2) NOT NULL  , 
	`paid` decimal(10,2) NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`deleted` enum('0','1') COLLATE hebrew_general_ci NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	`invoice` int(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `car_id`(`client_id`) , 
	KEY `user_id`(`user_id`) , 
	KEY `status`(`status`) , 
	KEY `deleted`(`deleted`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `clients`(
	`id` smallint(5) unsigned NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`login` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`attendant` smallint(5) unsigned NOT NULL  , 
	`supervisor` smallint(5) NOT NULL  , 
	`agent` smallint(5) NOT NULL  , 
	`parent_agent_id` smallint(6) NOT NULL  , 
	`username` varchar(30) COLLATE utf8_general_ci NOT NULL  DEFAULT '' , 
	`cnumber` varchar(32) COLLATE utf8_bin NOT NULL  , 
	`isAdmin` tinyint(4) NOT NULL  DEFAULT '0' , 
	`agent_size` tinyint(4) NOT NULL  , 
	`agent_num` smallint(5) unsigned NOT NULL  , 
	`department` tinyint(4) NOT NULL  , 
	`dispatcher_access` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`fname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`lname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`passport` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`birthday` date NOT NULL  DEFAULT '0000-00-00' , 
	`sex` tinyint(4) NOT NULL  DEFAULT '0' , 
	`fam_status` tinyint(4) NOT NULL  , 
	`date_end` date NOT NULL  , 
	`state` tinyint(4) NOT NULL  , 
	`agency` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`bank_details` text COLLATE utf8_general_ci NOT NULL  , 
	`cheque_delay` tinyint(4) NOT NULL  , 
	`cheque_payments` tinyint(4) NOT NULL  , 
	`credit_payments` tinyint(4) NOT NULL  , 
	`agent_prc` decimal(9,2) NOT NULL  , 
	`branch_id` smallint(6) NOT NULL  , 
	`position` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`send_doc_type` int(11) NOT NULL  , 
	`tel_type0` tinyint(4) NOT NULL  , 
	`tel0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type1` tinyint(4) NOT NULL  , 
	`tel1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type2` tinyint(4) NOT NULL  , 
	`tel2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type3` tinyint(4) NOT NULL  , 
	`tel3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type4` tinyint(4) NOT NULL  , 
	`tel4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`fax` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type0` tinyint(4) NOT NULL  , 
	`area0` tinyint(4) NOT NULL  , 
	`city0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type1` tinyint(4) NOT NULL  , 
	`area1` tinyint(4) NOT NULL  , 
	`city1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type2` tinyint(4) NOT NULL  , 
	`area2` tinyint(4) NOT NULL  , 
	`city2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`wh0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`wh1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`wh2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type1` tinyint(4) NOT NULL  , 
	`email2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type2` tinyint(4) NOT NULL  , 
	`url` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	`date_added` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' COMMENT 'תאריך קליטה' , 
	`mailinglist` tinyint(4) NOT NULL  DEFAULT '1' , 
	`sms_active` tinyint(4) NOT NULL  , 
	`fax_active` tinyint(4) NOT NULL  , 
	`fax_in` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`modules` varchar(255) COLLATE utf8_general_ci NOT NULL  COMMENT 'Module permissions' , 
	`pay_types` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`active` tinyint(4) NOT NULL  DEFAULT '0' , 
	`deleted` tinyint(4) NOT NULL  , 
	`vat_period` tinyint(4) NOT NULL  , 
	`current_vat_period` date NOT NULL  , 
	`income_tax` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `username`(`username`) , 
	KEY `agent`(`agent`) , 
	KEY `parent_agent_id`(`parent_agent_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `contacts`(
	`contact_id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  DEFAULT '0' , 
	`client_id` int(11) NOT NULL  DEFAULT '0' , 
	`client_type` set('client','group','agent') COLLATE utf8_general_ci NOT NULL  , 
	`receiver` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`sender` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`account_id` int(11) NOT NULL  , 
	`adr` varchar(255) COLLATE utf8_general_ci NOT NULL  COMMENT 'כתובת למעקב דואר יוצא' , 
	`link` varchar(255) COLLATE utf8_general_ci NOT NULL  COMMENT 'קישור לתביעה או פוליסה' , 
	`user_id` int(11) NOT NULL  DEFAULT '0' , 
	`type` set('letter','phone','meeting','mail','sms','rashum','ragil','havila','express','mail_in','documentation') COLLATE utf8_general_ci NOT NULL  , 
	`date` timestamp NOT NULL  DEFAULT CURRENT_TIMESTAMP , 
	`date_modified` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`modified_by` int(11) NOT NULL  , 
	`created_date` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`event_date` timestamp NULL  , 
	`result` tinyint(4) NOT NULL  , 
	`result_text` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`informSMS` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`informMail` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`metapel` int(11) NOT NULL  , 
	`importance` tinyint(4) NOT NULL  DEFAULT '0' , 
	`firm` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`agent_id` int(11) NOT NULL  , 
	`subject` text COLLATE utf8_general_ci NOT NULL  , 
	`back_date` timestamp NULL  , 
	`remind_text` text COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	`mail_data` mediumtext COLLATE utf8_general_ci NOT NULL  , 
	`attachment_data` text COLLATE utf8_general_ci NOT NULL  , 
	`attach_ids` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tupal` tinyint(1) NOT NULL  DEFAULT '0' , 
	`deleted` tinyint(1) NOT NULL  DEFAULT '0' , 
	`ext_id` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	PRIMARY KEY (`contact_id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `client_id`(`client_id`) , 
	KEY `deleted`(`deleted`) , 
	KEY `back_date`(`back_date`) , 
	KEY `user_id`(`user_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `groups`(
	`id` tinyint(3) unsigned NOT NULL  auto_increment , 
	`name` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `patterns`(
	`id` int(11) NOT NULL  auto_increment , 
	`type` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';


/* Create table in target */
CREATE TABLE `permissions`(
	`group_id` tinyint(4) NOT NULL  , 
	`module_id` smallint(6) NOT NULL  , 
	`permissions` set('r','w','d','R','W','D') COLLATE utf8_general_ci NOT NULL  
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `systememail_conf`(
	`id` tinyint(4) NOT NULL  auto_increment , 
	`email` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`smtp_host` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`smtp_port` int(5) NOT NULL  , 
	`smtp_sec` enum('','ssl') COLLATE utf8_general_ci NOT NULL  , 
	`smtp_auth` tinyint(1) NOT NULL  , 
	`username` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`password` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `user_login_log`(
	`user_id` smallint(6) NOT NULL  , 
	`time` timestamp NOT NULL  DEFAULT CURRENT_TIMESTAMP , 
	`ip` varchar(15) COLLATE utf8_general_ci NOT NULL  , 
	`proxy` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	KEY `user_id`(`user_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `users`(
	`id` smallint(5) unsigned NOT NULL  auto_increment , 
	`group_id` tinyint(3) unsigned NOT NULL  , 
	`username` varchar(30) COLLATE utf8_general_ci NOT NULL  DEFAULT '' , 
	`password` varbinary(32) NOT NULL  , 
	`is_agent` tinyint(4) NOT NULL  , 
	`fname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`lname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`passport` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`fam_status` tinyint(4) NOT NULL  , 
	`birthday` date NOT NULL  DEFAULT '0000-00-00' , 
	`sex` tinyint(4) NOT NULL  DEFAULT '0' , 
	`religion` tinyint(4) NOT NULL  , 
	`tel_type0` tinyint(4) NOT NULL  , 
	`tel0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type1` tinyint(4) NOT NULL  , 
	`tel1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type2` tinyint(4) NOT NULL  , 
	`tel2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type3` tinyint(4) NOT NULL  , 
	`tel3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type4` tinyint(4) NOT NULL  , 
	`tel4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type0` tinyint(4) NOT NULL  , 
	`city0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type1` tinyint(4) NOT NULL  , 
	`city1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type2` tinyint(4) NOT NULL  , 
	`city2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`city` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type1` tinyint(4) NOT NULL  , 
	`email1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type2` tinyint(4) NOT NULL  , 
	`email2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	`date_added` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`ip` varchar(15) COLLATE utf8_general_ci NOT NULL  , 
	`cookie` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`last_enter` datetime NOT NULL  , 
	`mailinglist` tinyint(4) NOT NULL  DEFAULT '1' , 
	`signature` text COLLATE utf8_general_ci NOT NULL  , 
	`active` tinyint(4) NOT NULL  DEFAULT '0' , 
	`work_end` date NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `type`(`group_id`) , 
	KEY `username`(`username`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `waretypes`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`viewSort` int(11) NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `wareunits`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';

INSERT INTO `users` (`id`,`group_id`,`username`,`password`,`is_agent`,`fname`,`lname`,`passport`,`fam_status`,`birthday`,`sex`,`religion`,`tel_type0`,`tel0`,`tel_type1`,`tel1`,`tel_type2`,`tel2`,`tel_type3`,`tel3`,`tel_type4`,`tel4`,`adr_type0`,`city0`,`zip0`,`adr0`,`adr_type1`,`city1`,`zip1`,`adr1`,`adr_type2`,`city2`,`zip2`,`adr2`,`city`,`email`,`email_type1`,`email1`,`email_type2`,`email2`,`notes`,`date_added`,`ip`,`cookie`,`last_enter`,`mailinglist`,`signature`,`active`,`work_end`) VALUES (1,1,'Admin','e3afed0047b08059d0fada10f400c1e5',0,'Admin','Admin','',0,'0000-00-00',0,0,0,'',0,'',0,'',0,'',0,'',0,'','','',0,'','','',0,'','','','','',0,'',0,'','','0000-00-00 00:00:00','79.181.2.1','3|1|m466f5|mda9ay27no','2012-11-10 19:44:58',1,'',1,'0000-00-00');

CREATE SCHEMA `crm_6` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;


USE `crm_6`;

/* Create table in target */
CREATE TABLE `acc_accept_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`accept_type` enum('accept','invoice') COLLATE hebrew_general_ci NOT NULL  , 
	`debt_card` int(11) NOT NULL  COMMENT 'debt/credit card' , 
	`accept_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`ptype` enum('מזומן','שיק','אשראי','ה.קבע/ה.בנקאית') COLLATE hebrew_general_ci NOT NULL  , 
	`sum` decimal(10,2) NOT NULL  , 
	`payments_num` tinyint(4) NOT NULL  COMMENT 'מספר תשלומים' , 
	`bank` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`dept` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`account` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`number` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`payment_status` tinyint(4) NOT NULL  , 
	`paid` float NOT NULL  COMMENT 'שולם' , 
	`invoice_id` int(11) NOT NULL  COMMENT 'חשבונית' , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_bank_accounts`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`bank` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`branch` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`number` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`type` int(11) NOT NULL  , 
	`acc_card` int(11) NOT NULL  , 
	`contact_person` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`tel` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`fax` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`balance` float NOT NULL  , 
	`masab_debt_company` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_debt_salary` varchar(8) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_debt_supplier` varchar(8) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_credit_company` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`masab_credit_billing` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_banks`(
	`bank_code` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`bank_name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`branch_code` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`branch_name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`address` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`zip` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`city` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`tel` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`fax` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`parent` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`date_open` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`date_close` varchar(255) COLLATE hebrew_general_ci NOT NULL  
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_billing_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`billing_type` enum('bank','card') COLLATE hebrew_general_ci NOT NULL  , 
	`client_id` int(11) NOT NULL  , 
	`doc_id` int(11) NOT NULL  , 
	`directive_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`ptype` enum('מזומן','שיק','אשראי','ה.קבע/ה.בנקאית') COLLATE hebrew_general_ci NOT NULL  , 
	`sum` float NOT NULL  , 
	`bank` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`dept` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`account` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`invoice` int(11) NOT NULL  COMMENT 'חשבונית' , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_cards`(
	`id` int(11) NOT NULL  auto_increment , 
	`class_id` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`card_id` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sub1` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sub2` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sub3` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) , 
	UNIQUE KEY `card_id_3`(`class_id`,`card_id`) , 
	KEY `card_id`(`class_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_counters`(
	`group_id` int(11) NOT NULL  , 
	`expenses` int(11) NOT NULL  , 
	`accept` int(11) NOT NULL  , 
	`accept_tmp` int(11) NOT NULL  , 
	`orders` int(11) NOT NULL  , 
	`invoice_credit` int(11) NOT NULL  , 
	`invoice_debt` int(11) NOT NULL  , 
	`invoice_accept` int(11) NOT NULL  , 
	`deposit` int(11) NOT NULL  , 
	`refund` int(11) NOT NULL  , 
	`cheque` int(11) NOT NULL  , 
	`payments` int(11) NOT NULL  , 
	`set_balance` int(11) NOT NULL  , 
	`store_receive` int(11) NOT NULL  , 
	`store_send` int(11) NOT NULL  , 
	PRIMARY KEY (`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_deposit_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`deposit_id` int(11) NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`accept_date` date NOT NULL  , 
	`cheque_id` int(11) NOT NULL  , 
	`sum` float NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `depozit_id`(`deposit_id`,`type`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_deposits`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`num` int(11) NOT NULL  , 
	`account_id` int(11) NOT NULL  , 
	`sum` float NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`printed` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `account_id`(`account_id`) , 
	KEY `user_id`(`user_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_docs`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`num` int(11) NOT NULL  , 
	`client_id` int(11) NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	`parent_doc_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`sum` float NOT NULL  , 
	`account_id` int(11) NOT NULL  , 
	`debt` float NOT NULL  , 
	`credit` float NOT NULL  , 
	`VAT` float NOT NULL  , 
	`vat_sum` float NOT NULL  COMMENT 'סכום של מע\"מ' , 
	`paid` float NOT NULL  , 
	`currency` enum('NIS','USD','EUR') COLLATE hebrew_general_ci NOT NULL  COMMENT 'מטבע' , 
	`rate` float NOT NULL  COMMENT 'שער' , 
	`tax_clear` tinyint(4) NOT NULL  COMMENT 'ניקוי מס במקור' , 
	`orders_ids` varchar(255) COLLATE hebrew_general_ci NOT NULL  COMMENT 'הזמנות' , 
	`over_order` float NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`printed` tinyint(4) NOT NULL  , 
	`exported` tinyint(4) NOT NULL  , 
	`export_date` datetime NOT NULL  COMMENT 'תאריך העברה' , 
	`storno` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `client_id`(`client_id`) , 
	KEY `user_id`(`user_id`) , 
	KEY `type`(`type`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_expense_ext`(
	`doc_id` int(11) NOT NULL  , 
	`sup_num` varchar(255) COLLATE hebrew_general_ci NOT NULL  COMMENT 'עוסק מורשה של ספק' , 
	`sup_doc_date` date NOT NULL  , 
	`sup_doc_num` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`expense_type` tinyint(4) NOT NULL  , 
	`rep_period` varchar(255) COLLATE hebrew_general_ci NOT NULL  COMMENT 'חודש דיווח' , 
	`paid` float NOT NULL  , 
	KEY `doc_id`(`doc_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_expense_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`acc_card` int(11) NOT NULL  COMMENT 'מפתח חשבשבת' , 
	`doc_id` int(11) NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`unit` int(11) NOT NULL  , 
	`amount` float NOT NULL  , 
	`price` float NOT NULL  , 
	`sum` float NOT NULL  , 
	`VAT` decimal(5,2) NOT NULL  , 
	`VAT_sum` decimal(10,2) NOT NULL  , 
	`sum_tax` float NOT NULL  COMMENT 'sum+vat' , 
	KEY `id`(`id`) , 
	KEY `card_id`(`doc_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_expenses_group`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`VAT` decimal(5,2) NOT NULL  , 
	`acc_card` int(11) NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_invoice_rows`(
	`id` int(11) NOT NULL  auto_increment , 
	`invoice_id` int(11) NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`code` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`unit` int(11) NOT NULL  , 
	`amount` float NOT NULL  , 
	`price` float NOT NULL  , 
	`sum` float NOT NULL  , 
	`VAT` smallint(6) NOT NULL  , 
	`sum_tax` float NOT NULL  COMMENT 'sum+vat' , 
	KEY `id`(`id`) , 
	KEY `invoice_id`(`invoice_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_operations`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`date` date NOT NULL  , 
	`doc_id` int(11) NOT NULL  , 
	`credit_class` int(11) NOT NULL  COMMENT 'מיון' , 
	`credit_card` int(11) NOT NULL  , 
	`credit_subcard` int(11) NOT NULL  , 
	`debt_class` int(11) NOT NULL  COMMENT 'מיון' , 
	`debt_card` int(11) NOT NULL  , 
	`debt_subcard` int(11) NOT NULL  , 
	`sum` float(12,2) NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `credit_class`(`credit_class`,`credit_card`) , 
	KEY `debt_class`(`debt_class`,`debt_card`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `acc_orders`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`client_id` int(11) NOT NULL  , 
	`agent_id` int(11) NOT NULL  , 
	`pattern_id` int(11) NOT NULL  , 
	`num` int(11) NOT NULL  , 
	`num_add` tinyint(4) NOT NULL  , 
	`date` date NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`type` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`start` datetime NOT NULL  , 
	`end` datetime NOT NULL  , 
	`payments` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`pay_start` datetime NOT NULL  , 
	`status` int(11) NOT NULL  , 
	`sms` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`importance` enum('0','1','2') COLLATE hebrew_general_ci NOT NULL  , 
	`sum` decimal(10,2) NOT NULL  , 
	`paid` decimal(10,2) NOT NULL  , 
	`notes` text COLLATE hebrew_general_ci NOT NULL  , 
	`deleted` enum('0','1') COLLATE hebrew_general_ci NOT NULL  , 
	`user_id` int(11) NOT NULL  , 
	`invoice` int(11) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `car_id`(`client_id`) , 
	KEY `user_id`(`user_id`) , 
	KEY `status`(`status`) , 
	KEY `deleted`(`deleted`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `clients`(
	`id` smallint(5) unsigned NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`login` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`attendant` smallint(5) unsigned NOT NULL  , 
	`supervisor` smallint(5) NOT NULL  , 
	`agent` smallint(5) NOT NULL  , 
	`parent_agent_id` smallint(6) NOT NULL  , 
	`username` varchar(30) COLLATE utf8_general_ci NOT NULL  DEFAULT '' , 
	`cnumber` varchar(32) COLLATE utf8_bin NOT NULL  , 
	`isAdmin` tinyint(4) NOT NULL  DEFAULT '0' , 
	`agent_size` tinyint(4) NOT NULL  , 
	`agent_num` smallint(5) unsigned NOT NULL  , 
	`department` tinyint(4) NOT NULL  , 
	`dispatcher_access` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`name` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`fname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`lname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`passport` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`birthday` date NOT NULL  DEFAULT '0000-00-00' , 
	`sex` tinyint(4) NOT NULL  DEFAULT '0' , 
	`fam_status` tinyint(4) NOT NULL  , 
	`date_end` date NOT NULL  , 
	`state` tinyint(4) NOT NULL  , 
	`agency` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`bank_details` text COLLATE utf8_general_ci NOT NULL  , 
	`cheque_delay` tinyint(4) NOT NULL  , 
	`cheque_payments` tinyint(4) NOT NULL  , 
	`credit_payments` tinyint(4) NOT NULL  , 
	`agent_prc` decimal(9,2) NOT NULL  , 
	`branch_id` smallint(6) NOT NULL  , 
	`position` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`send_doc_type` int(11) NOT NULL  , 
	`tel_type0` tinyint(4) NOT NULL  , 
	`tel0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type1` tinyint(4) NOT NULL  , 
	`tel1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type2` tinyint(4) NOT NULL  , 
	`tel2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type3` tinyint(4) NOT NULL  , 
	`tel3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type4` tinyint(4) NOT NULL  , 
	`tel4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`fax` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type0` tinyint(4) NOT NULL  , 
	`area0` tinyint(4) NOT NULL  , 
	`city0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type1` tinyint(4) NOT NULL  , 
	`area1` tinyint(4) NOT NULL  , 
	`city1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type2` tinyint(4) NOT NULL  , 
	`area2` tinyint(4) NOT NULL  , 
	`city2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`wh0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`wh1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`wh2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type1` tinyint(4) NOT NULL  , 
	`email2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type2` tinyint(4) NOT NULL  , 
	`url` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cperson4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_tel4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`cp_email0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	`date_added` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' COMMENT 'תאריך קליטה' , 
	`mailinglist` tinyint(4) NOT NULL  DEFAULT '1' , 
	`sms_active` tinyint(4) NOT NULL  , 
	`fax_active` tinyint(4) NOT NULL  , 
	`fax_in` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`modules` varchar(255) COLLATE utf8_general_ci NOT NULL  COMMENT 'Module permissions' , 
	`pay_types` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`active` tinyint(4) NOT NULL  DEFAULT '0' , 
	`deleted` tinyint(4) NOT NULL  , 
	`vat_period` tinyint(4) NOT NULL  , 
	`current_vat_period` date NOT NULL  , 
	`income_tax` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `username`(`username`) , 
	KEY `agent`(`agent`) , 
	KEY `parent_agent_id`(`parent_agent_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `contacts`(
	`contact_id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  DEFAULT '0' , 
	`client_id` int(11) NOT NULL  DEFAULT '0' , 
	`client_type` set('client','group','agent') COLLATE utf8_general_ci NOT NULL  , 
	`receiver` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`sender` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`account_id` int(11) NOT NULL  , 
	`adr` varchar(255) COLLATE utf8_general_ci NOT NULL  COMMENT 'כתובת למעקב דואר יוצא' , 
	`link` varchar(255) COLLATE utf8_general_ci NOT NULL  COMMENT 'קישור לתביעה או פוליסה' , 
	`user_id` int(11) NOT NULL  DEFAULT '0' , 
	`type` set('letter','phone','meeting','mail','sms','rashum','ragil','havila','express','mail_in','documentation') COLLATE utf8_general_ci NOT NULL  , 
	`date` timestamp NOT NULL  DEFAULT CURRENT_TIMESTAMP , 
	`date_modified` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`modified_by` int(11) NOT NULL  , 
	`created_date` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`event_date` timestamp NULL  , 
	`result` tinyint(4) NOT NULL  , 
	`result_text` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`informSMS` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`informMail` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`metapel` int(11) NOT NULL  , 
	`importance` tinyint(4) NOT NULL  DEFAULT '0' , 
	`firm` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`agent_id` int(11) NOT NULL  , 
	`subject` text COLLATE utf8_general_ci NOT NULL  , 
	`back_date` timestamp NULL  , 
	`remind_text` text COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	`mail_data` mediumtext COLLATE utf8_general_ci NOT NULL  , 
	`attachment_data` text COLLATE utf8_general_ci NOT NULL  , 
	`attach_ids` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tupal` tinyint(1) NOT NULL  DEFAULT '0' , 
	`deleted` tinyint(1) NOT NULL  DEFAULT '0' , 
	`ext_id` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	PRIMARY KEY (`contact_id`) , 
	KEY `group_id`(`group_id`) , 
	KEY `client_id`(`client_id`) , 
	KEY `deleted`(`deleted`) , 
	KEY `back_date`(`back_date`) , 
	KEY `user_id`(`user_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `groups`(
	`id` tinyint(3) unsigned NOT NULL  auto_increment , 
	`name` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `patterns`(
	`id` int(11) NOT NULL  auto_increment , 
	`type` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE latin1_swedish_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='latin1';


/* Create table in target */
CREATE TABLE `permissions`(
	`group_id` tinyint(4) NOT NULL  , 
	`module_id` smallint(6) NOT NULL  , 
	`permissions` set('r','w','d','R','W','D') COLLATE utf8_general_ci NOT NULL  
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `systememail_conf`(
	`id` tinyint(4) NOT NULL  auto_increment , 
	`email` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`smtp_host` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`smtp_port` int(5) NOT NULL  , 
	`smtp_sec` enum('','ssl') COLLATE utf8_general_ci NOT NULL  , 
	`smtp_auth` tinyint(1) NOT NULL  , 
	`username` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`password` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `user_login_log`(
	`user_id` smallint(6) NOT NULL  , 
	`time` timestamp NOT NULL  DEFAULT CURRENT_TIMESTAMP , 
	`ip` varchar(15) COLLATE utf8_general_ci NOT NULL  , 
	`proxy` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	KEY `user_id`(`user_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `users`(
	`id` smallint(5) unsigned NOT NULL  auto_increment , 
	`group_id` tinyint(3) unsigned NOT NULL  , 
	`username` varchar(30) COLLATE utf8_general_ci NOT NULL  DEFAULT '' , 
	`password` varbinary(32) NOT NULL  , 
	`is_agent` tinyint(4) NOT NULL  , 
	`fname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`lname` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`passport` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`fam_status` tinyint(4) NOT NULL  , 
	`birthday` date NOT NULL  DEFAULT '0000-00-00' , 
	`sex` tinyint(4) NOT NULL  DEFAULT '0' , 
	`religion` tinyint(4) NOT NULL  , 
	`tel_type0` tinyint(4) NOT NULL  , 
	`tel0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type1` tinyint(4) NOT NULL  , 
	`tel1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type2` tinyint(4) NOT NULL  , 
	`tel2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type3` tinyint(4) NOT NULL  , 
	`tel3` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`tel_type4` tinyint(4) NOT NULL  , 
	`tel4` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type0` tinyint(4) NOT NULL  , 
	`city0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr0` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type1` tinyint(4) NOT NULL  , 
	`city1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr_type2` tinyint(4) NOT NULL  , 
	`city2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`zip2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`adr2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`city` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type1` tinyint(4) NOT NULL  , 
	`email1` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`email_type2` tinyint(4) NOT NULL  , 
	`email2` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`notes` text COLLATE utf8_general_ci NOT NULL  , 
	`date_added` timestamp NOT NULL  DEFAULT '0000-00-00 00:00:00' , 
	`ip` varchar(15) COLLATE utf8_general_ci NOT NULL  , 
	`cookie` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`last_enter` datetime NOT NULL  , 
	`mailinglist` tinyint(4) NOT NULL  DEFAULT '1' , 
	`signature` text COLLATE utf8_general_ci NOT NULL  , 
	`active` tinyint(4) NOT NULL  DEFAULT '0' , 
	`work_end` date NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `type`(`group_id`) , 
	KEY `username`(`username`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';


/* Create table in target */
CREATE TABLE `waretypes`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE hebrew_general_ci NOT NULL  , 
	`viewSort` int(11) NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='hebrew';


/* Create table in target */
CREATE TABLE `wareunits`(
	`id` int(11) NOT NULL  auto_increment , 
	`group_id` int(11) NOT NULL  , 
	`name` varchar(255) COLLATE utf8_general_ci NOT NULL  , 
	`deleted` tinyint(4) NOT NULL  , 
	PRIMARY KEY (`id`) , 
	KEY `group_id`(`group_id`) 
) ENGINE=MyISAM DEFAULT CHARSET='utf8';

INSERT INTO `users` (`id`,`group_id`,`username`,`password`,`is_agent`,`fname`,`lname`,`passport`,`fam_status`,`birthday`,`sex`,`religion`,`tel_type0`,`tel0`,`tel_type1`,`tel1`,`tel_type2`,`tel2`,`tel_type3`,`tel3`,`tel_type4`,`tel4`,`adr_type0`,`city0`,`zip0`,`adr0`,`adr_type1`,`city1`,`zip1`,`adr1`,`adr_type2`,`city2`,`zip2`,`adr2`,`city`,`email`,`email_type1`,`email1`,`email_type2`,`email2`,`notes`,`date_added`,`ip`,`cookie`,`last_enter`,`mailinglist`,`signature`,`active`,`work_end`) VALUES (1,1,'Admin','e3afed0047b08059d0fada10f400c1e5',0,'Admin','Admin','',0,'0000-00-00',0,0,0,'',0,'',0,'',0,'',0,'',0,'','','',0,'','','',0,'','','','','',0,'',0,'','','0000-00-00 00:00:00','79.181.2.1','3|1|m466f5|mda9ay27no','2012-11-10 19:44:58',1,'',1,'0000-00-00');
