//Name:		ExtMenuWithTooltip2e.js
Ext.BLANK_IMAGE_URL = '/Libs/ext-3.0.0/resources/images/default/s.gif';

var ttMgr;
var idGoodOption;


Ext.onReady(

function () {
	Ext.QuickTips.init();

	ttMgr = new ttManager(); //My helper tool to manually show and position a tooltip.
	idGoodOption = new Ext.ToolTip({
		target: '3',
		width: 140,
		title: 'this good option',
		tools: [{id:'help', handler:h}
			,{id: 'close'}
		],
		html: 'there is really something good, if you select this item! ...',
		dismissDelay: 8000,
		showDelay: 1000
	});


	//Ext::animal:MenuItem override to allow tooltips, v3
	Ext.override(
	Ext.menu.Item, {
		onRender: function (container, position) {
			if (!this.itemTpl) {
				this.itemTpl = Ext.menu.Item.prototype.itemTpl = new Ext.XTemplate('<a id="{id}" class="{cls}" hidefocus="true" unselectable="on" href="{href}"', '<tpl if="hrefTarget">', ' target="{hrefTarget}"', '</tpl>', '>', '<img src="{icon}" class="x-menu-item-icon {iconCls}"/>', '<span class="x-menu-item-text">{text}</span>', '</a>');
			}
			var a = this.getTemplateArgs();
			this.el = position ? this.itemTpl.insertBefore(position, a, true) : this.itemTpl.append(container, a, true);
			this.iconEl = this.el.child('img.x-menu-item-icon');
			this.textEl = this.el.child('.x-menu-item-text');
			if (this.tooltip) {
				this.tooltip = new Ext.ToolTip(Ext.apply({
					target: this.el
				}, Ext.isObject(this.tooltip) ? this.tooltip : {
					html: this.tooltip
				}));
			}
			Ext.menu.Item.superclass.onRender.call(this, container, position);
		},
		getTemplateArgs: function () {
			var result = {
				id: this.id,
				cls: this.itemCls + (this.menu ? ' x-menu-item-arrow' : '') + (this.cls ? ' ' + this.cls : ''),
				href: this.href || '#',
				tooltip: this.tooltip,
				hrefTarget: this.hrefTarget,
				icon: this.icon || Ext.BLANK_IMAGE_URL,
				iconCls: this.iconCls || '',
				text: this.itemText || this.text || ' '
			};
			return result;
		}
	});

	function clickHandler() {
		alert('Clicked on a menu item');
	}

	function checkHandler() {
		alert('Checked a menu item');
	}

	var menu = new Ext.menu.Menu({
		id: 'basicMenu',
		items: [{
			id: '1',
			text: 'Disable view auto-update',
			icon: 'help.png',
			checked: true,
			handler: clickHandler,
			tooltip: {
				title: 'View auto-update',
				html: 'New received events will automatically be put into the view, but possibly outside of the visible area. With the default "view auto-update" on, the view will scroll to the new event. This behavior might be disabled here.',
				width: 180,
				dismissDelay: 8000,
				tools: [{
					id: 'help',
					handler: h
				},
				{
					id: 'close',
					handler: c
				}],
				trackMouse: false,
				autoHide: false
			}
		},
		{
			id: '2',
			text: 'Disable window auto-open',
			handler: clickHandler,
			tooltip: {
				title: 'Window auto-open',
				html: 'New received events will automatically be put into the view, but possibly outside of the visible area. With the default "view auto-update" on, the view will scroll to the new event. This behavior might be disabled here.',
				width: 180,
				dismissDelay: 8000,
				showDelay: 1500,
				tools: [{
					id: 'help',
					handler: h
				},
				{
					id: 'close',
					handler: c
				}],
				mouseOffset: [-25, -25],
				trackMouse: false
			}
		},
		{
			id: '3',
			text: 'another good option',
			handler: clickHandler,
			listeners: {
				activate: function () {
					ttMgr.showTip(this, idGoodOption)
				},
				deactivate: function () {
					ttMgr.clearTip(idGoodOption)
				}
			}
		}]
	});

	var mb = new Ext.SplitButton({
		text: 'SimpleMenu'
	});
	mb.render('menubutton');
	mb.menu = menu;


	function c(evt, tool, panel, cfg) {
		panel.hide();
	}

	function h(evt, tool, panel, cfg) {
		Ext.Msg.alert("Help", "someday ....");
	}

	function ttManager() //Helper tool.
	{
		var tt = null;
		var timer = null;
		var elemPos;

		this.showTip = function (elem, nextTT) {
			if (tt != null) dismiss();
			tt = nextTT;
			elemPos = XYPos(Ext.get(elem.id).dom);
			elemPos[0] += 50;
			elemPos[1] += 15;
			timer = window.setTimeout(show, tt.showDelay);
		};

		this.clearTip = function (aTT) {
			aTT.hide();
			if (tt != null) dismiss();
		};

		function show() {
			//Ext.get("dbgExpand").dom.innerHTML = elemPos;
			if (tt != null) {
				tt.showAt(elemPos);
				tt.focus();
			}
		};

		function dismiss() {
			if (tt != null) {
				window.clearTimeout(timer);
				tt.hide();
				tt = null;
			}
		}
	}

	function XYPos(elem) {
/*
					Purpose:	Find the position of the element, traversing higher in the object hierarchy,
							until no more elements are found, and add all their offsets to the result.
				*/

		var left = elem.offsetLeft;
		var top = elem.offsetTop;
		var parent;

		while (elem.offsetParent != null) {
			parent = elem.offsetParent;
			left += parent.offsetLeft;
			top += parent.offsetTop;
			elem = parent
		}

		return [left, top];
	}

});

/*
Scratch

{
	id: '1',
	text: 'An item',
	cls: 'xx-menuitem-dummy',
	href: '#',
	hrefTarget: '_new',
	iconCls: 'xx-menuitem-dummy-class',
	//handler: clickHandler
	tooltip: { title: 'Title', text: 'SomeText' }
}

*/