<?

##################################################################################
function theme_pages_sub($row) {
 global $AUSER, $LNG, $page, $pages_cats;

	if ($pages_cats[$page]['type']=='allsub' AND $row['short_text']) $row['short_text'].="<div class=sm10><font color=gray>{$pages_cats[$row[parent_id]][title]}</font></div>";

	if ($row['img_ext']) {	# add image
		$image_file="files/pages/$row[cat_id].{$row[img_ext]}";
		if (file_exists($image_file)) $row['short_text']="<img src='$image_file' align='$row[img_pos]' border=0 hspace=10 vspace=2>".$row['short_text'];
	}

	if ($row['full_text'] OR page_has_sub($pages_cats, $row['cat_id'])) {
		$row['title']="<a href='?m=pages&page=$row[cat_id]'>$row[title]</a>";
		if ($row['short_text']) $row['short_text'].= "<div align=right><a href='?m=pages&page=$row[cat_id]'><font color=#ff9933 class=sm12><b>$LNG[read_more]</b></font></a></div>";	
	}
		
	if ($SVARS['is_admin']) $row['title'].= " &nbsp; <a href='?m=pages&a=edit&cat_id=$row[cat_id]'><img src='skin/icons/edit.gif' border=0 alt='$LNG[Edit]'></a>";

 return "<table border=0 width=100%><tr><td class=sm14><b>$row[title]</b></td></tr><tr><td>$row[short_text]</td></tr></table><br>";
}


#############################################################################################
function pages_menu_three(&$cats_ar) {
	//echo "<pre>"; print_r($cats_ar); echo "</pre>";
	if (!is_array($cats_ar)) return false;
	foreach ($cats_ar as $cat_id=>$cat_ar) {
		# Main categories
		if ($cat_ar['parent_id']==0 AND $cat_ar['active']==1) {
			$cats_ar[$cat_id]['sub']=page_has_sub($cats_ar, $cat_id);	// check if have subcats
			$cats_three.=pages_menu_row($cats_ar[$cat_id]);				// show main cats
			
			# show subcats
			if ($cat_ar['cat_id']['sub']) {
				$cats_three.=pages_menu_sub_rows($cats_ar, $cat_ar['cat_id']);
			}
		}
	}

	$cats_three="<table dir=ltr border=0 width=100% cellpadding=0 cellspacing=0>$cats_three</table>"; //<tr><td>&nbsp;</td></tr>

 return $cats_three;
}
#############################################################################################
function pages_menu_row($row) {
	if ($row['type']=='html') {
		$title = $row['short_text'];
	}elseif ($row['type']=='link') {
		$title = $row['title'];
		$link = $row['short_text'];
	}else {
		$title = $row['title'];
		$link  = "?m=pages&page=".($row['page_name'] ? $row['page_name'] : $row['cat_id']);
	}

	if ($row['sub']) {	// onclick open subcats
		$onclick="toggle_show('sub$row[cat_id]');";
	}elseif ($link) {	// onclick goto link
		$title = "<a href='$link'>$title</a>";
		$onclick="location.href='$link';";
	}

 return "<tr class=rm onMouseOver=this.className='rm_over'; onMouseOut=this.className='rm';".($onclick?" onClick=$onclick":'')."><td width=100% dir=rtl>$title</td><td height=22>&nbsp;</td></tr>";
}

#############################################################################################
function pages_menu_sub_rows($cats_ar, $cat_id) {
	# get subcats to array
	foreach ($cats_ar as $key=>$row) {
 		if ($row['parent_id']==$cat_id AND $row['active']==1) $subcats[$key]=$row;
	}
	if (!$subcats) return;

	if ($_GET['page'] AND $selected_cat_id=get_page_id($_GET['page'], $cats_ar)) {	# find opened root category
		$pointers=page_get_pointers_ar($cats_ar, $selected_cat_id);
		$open_cat_id=$pointers[0];
	}
	
	$sub_html="<tr id=sub$cat_id".( ($cat_id!=$open_cat_id)?" style='display:none'":'' )."><td colspan=2 align=right>"
		."<table border=0 width=100% cellspacing=1>";

	foreach ($subcats as $row) {
		if ($row['type']=='html') {
			$title = $row['short_text'];
		}elseif ($row['type']=='link') {
			$link  = $row['short_text'];
			$title = "<a href='$link'>{$row[title]}</a>";
		}else {
			$link  = "?m=pages&page=".($row['page_name'] ? $row['page_name'] : $row['cat_id']);
			$title = "<a href='$link'>{$row[title]}</a>";
		}	
		
		$sub_html.="<tr class="
			.( ($row['cat_id']!=$_GET['page']) ? "rm_sub onMouseOver=this.className='rm_sub_over'; onMouseOut=this.className='rm_sub';" : 'rm_sub_over' )
			.($link?" onClick=location.href='$link';":'')."><td width=100% align=right><span dir=rtl>$title</span><img border=0 src='skin/rm_bullet.gif' width=22 height=10></td></tr>";
	}

	$sub_html.="</table></td></tr>";

 return $sub_html;
}


//
function theme_block($title, $content, $style='', $width='') {
 global $m;
  if ($m=='shop/checkout' AND $style=='form') return theme_win_gray($title, $content, '100%'); 
  
  switch ($style) {
	case 'left':	return theme_block_left($title, $content);
	case 'right':	return theme_block_right($title, $content, '141');
	case 'center':	return theme_win_gray($title, $content, $width);
	case 'itemFull':return theme_win_w1($title, $content, '100%');
	case 'form':	return theme_form($title, $content, $width);
	case 'cart':
	case 'chekoutCart':
					return theme_win_gray($title, $content, '100%');
	default:		return $content;
  }
}


//
function theme_win_gray($title, $content, $width='') {
 return "<table width='$width' class=win_gray cellspacing=0 cellpadding=0>"
 		."<tr><th>$title</th></tr><tr><td style='padding:5px'>"
		.$content
		."</td></tr></table><br>";
}

//
function theme_win_w1($title, $content, $width='') {
	if(is_numeric($width)) $width.='px';
	return "<div class=w1".($width?" style='width:$width'":'').">"
		."<div class=t><b></b></div> "
		."<div class=m><div class=c>".($title?"<h6>$title</h6>":'')."$content</div></div>"
		."<div class=b><b></b></div>"
		."</div><br>";
}


//
function theme_win_w2($title, $content, $width='') {
	if(is_numeric($width)) $width.='px';

 return "
<div class=fl".($width?" style='width:$width'":'')."><div class=c><h1>$title</h1>
	$content
</div><div class=b><b></b></div></div>
";
	
	return "<div class=w1".($width?" style='width:$width'":'').">"
		."<div class=t><b></b></div> "
		."<div class=m><div class=c>".($title?"<h6>$title</h6>":'')."$content</div></div>"
		."<div class=b><b></b></div>"
		."</div><br>";
}



#################################################################
function theme_form($title, $content, $width='') {
	return "<table class=form border=1 width='$width' cellspacing=0 cellpadding=0 style='border-collapse:collapse;' bordercolor=#E9F1FE>
<tr><th bgcolor=#f0f0f0>$title</th></tr>
<tr><td style='padding:5px;'>$content</td></tr>
</table><br>";
}

#################################################################
function theme_block_center($title, $content, $width='100%') {
	$content=
	"<div class=tl><div class=tr style='padding:10px; height:35px; width:100%;'></div></div>".
	"<div class=p style='padding:10px; width:100%; border-left: solid 1px #CACDE0; border-right: solid 1px #CACDE0;'>".
	$content.
	"</div><div style='background-image:url(skin/portal/bg1.png); background-repeat:no-repeat; zoom:1; background-position:right bottom; height:8px; padding-right:10px;'>
	<div style='background-image:url(skin/portal/bg1.png); background-repeat:no-repeat; zoom:1; background-position:left bottom; height:8px;'></div></div>";

	//return ($title ? "<h4>$title</h4>" : '').$content;
	return $content;
}

#################################################################
function theme_news_list($title, $content, $width='100%') {
 return "<table border=0 width='$width' cellspacing=0 cellpadding=2>"
	."<tr><td><b>$title</b></td></tr>"
	."<tr><td>$content</td></tr>"
	."</table><br>";
}
#################################################################
function theme_news_show($title, $content, $width='100%') {
	return "<h4>$title</h3>$content<br>";
}

#################################################################
function theme_admin_edit_line($content) {
	return "<div dir=ltr align=left class=sm10 style='background-color:#FFFFEA; border:1px solid #ff6600; padding-left:5px;'><b>Admin:</b>&nbsp; $content</div>";
}

#################################################################
function theme_block_left($title, $content, $width='150') {
	return "<table border=1 width='$width' cellspacing=0 cellpadding=2 bordercolor=#CCCCCC style='border-collapse: collapse'>
<tr><th bgcolor=#CCCCCC>$title</th></tr>
<tr><td>$content</td></tr>
</table><br>";
}

#################################################################
function theme_block_right($title, $content, $width='80') {
	return theme_block_left($title, $content, $width);
}

//
function set_permissions() {
 global $CFG, $SVARS, $m, $DEF_PRS;
	if (!$SVARS['user_id']) return;
	$PRS=$DEF_PRS;//Effective permissions
	
	if (mem_isset("PRS$SVARS[user_id]")) {
		$user=mem_get("PRS$SVARS[user_id]");
	}else {
		$user=sql2array("SELECT user_id, parent_id, permissions FROM users WHERE user_id=$SVARS[user_id] OR user_id='$SVARS[parent_id]'", 'user_id','',0);//
		mem_set("PRS$SVARS[user_id]", $user, 3600);
	}
	
	if ($user[$SVARS['parent_id']]['permissions']) 	$PARENT_PRS	=unserialize($user[$SVARS['parent_id']]['permissions']);//Parent permissions
	if ($user[$SVARS['user_id']]['permissions']) 	$USER_PRS	=unserialize($user[$SVARS['user_id']]['permissions']);//User permissions

	if (is_array($PARENT_PRS)) {//Applaing Group permissions
		foreach ($DEF_PRS as $p=>$panel) { //Panels
			if ($PARENT_PRS[$p]==1) $PRS[$p][1]=1; elseif ($PARENT_PRS[$p]==-1) $PRS[$p][1]=0; //Update permissions for panels
			foreach ($panel as $b=>$but) { //Buttons
				if (is_array($but) AND $PARENT_PRS[$but[0]]==1)	$PRS[$p][$b][3]=1; elseif ($PARENT_PRS[$but[0]]==-1) $PRS[$p][$b][3]=0; //Update permissions for buttons
				if (is_array($but['sub'])) foreach ($but['sub'] as $s=>$sub) //SubButton (Savsem malenkiy knopka)
					if (is_array($sub) AND $PARENT_PRS[$sub[0]]==1)	$PRS[$p][$b]['sub'][$s][3]=1; elseif ($PARENT_PRS[$sub[0]]==-1) $PRS[$p][$b]['sub'][$s][3]=0; //Update permissions for sub-buttons 
			}
		}
	}
	if (is_array($USER_PRS)) {//Applaing User permissions
		foreach ($DEF_PRS as $p=>$panel) { //Panels
			if ($USER_PRS[$p]==1) $PRS[$p][1]=1; elseif ($USER_PRS[$p]==-1) $PRS[$p][1]=0; //Update permissions for panels
			foreach ($panel as $b=>$but) { //Buttons
				if (is_array($but) AND $USER_PRS[$but[0]]==1) $PRS[$p][$b][3]=1; elseif ($USER_PRS[$but[0]]==-1) $PRS[$p][$b][3]=0; //Update permissions for buttons
				if (is_array($but['sub'])) foreach ($but['sub'] as $s=>$sub) {
					//echo "<div dir=ltr align=left><pre>[$p][$b][sub][$s]".print_r($PRS[$p][$b]['sub'][$s],1)."</pre></div>";//SubButton (Savsem malenkiy knopka)
					if (is_array($sub) AND $USER_PRS[$sub[0]]==1) $PRS[$p][$b]['sub'][$s][3]=1; elseif ($USER_PRS[$sub[0]]==-1) $PRS[$p][$b]['sub'][$s][3]=0; //Update permissions for sub-buttons 
				}
			}
		}
	}
  return $PRS;
}


//
function theme_fl($module, $title, $icon='') {
 global $m;  if (!$icon) $icon=$module;
	//if ($title=='CRM') echo "<a href='/?m=crm' class=crm><img src='skin/0.gif' style=\"background:url(crm/skin/micons/crm_big.png); _background:transparent; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='skin/micons/crm_big.png', sizingMethod='crop');\"></a>";
	//else 
	echo "<a href='/?m=$module'".($m==$module?' class=on':'')."><u><div style=\"background:url(skin/micons/$icon.png) no-repeat; _background:transparent; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='skin/micons/$icon.png', sizingMethod='crop');\"></div>$title</u></a>";
}

/*
//
function theme_tabs_menu() {
 global $CFG, $SVARS, $m, $PRS;
	if (!$SVARS['user_type']) return;
	
	echo "<div id=topTabs>";
	$p=0;
	if ($PRS) foreach ($PRS as $tkey=>$tab) {
		if (is_array($tab) AND $tab[1]==1) {
			$GLOBALS['top_tabs'][]="{contentEl:'t_$tkey', title:'".addslashes($tab[0])."'}"; # tab names for js
			echo "<div id=t_$tkey class=x-hide-display>";
			echo "<div class=mm>";
			echo "<div class=gr><div class=grc>";
			foreach ($tab as $button) {
				if (is_array($button) AND $button[3]==1 AND $button[0]) {
					if (!$button[2]) $button[2]=$button[1];
					$is_active = $button[0]==$m.($m=='pages'?"&id=$_GET[id]":'') ? true : false;
					echo "<a href='/?m=$button[0]'".($is_active?' class=on':'')."><u><div style=\"background:url(skin/micons/$button[2].png) no-repeat; _background:transparent; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='skin/micons/$button[2].png', sizingMethod='crop');\"></div>$button[1]</u></a>";
					if ($is_active) $GLOBALS['activeTab']=$p;
				}
			}
			echo "</div></div>"; //<h5>$tab[0]</h5>
			echo "</div>";
			echo "</div>";
			$p++;
		}
	}
	echo "</div>";
}
*/

//
function rewrite_links($str) {
 return $str;
/*
 global $CFG;
	if (!$CFG['rewrite']) return $str;
	$find[]='|/?\?m=user(?:&a=([\w]+))?(&?)|e';		$repl[]="'/user'.('$1'?'/$1':'').('$2'?'?':'')";
	//$find[]='|/?\?m=pages&id=([a-z0-9_-]+)|';		$repl[]='/pages/$1';
	$find[]='|/?\?m=pages&id=([a-z0-9_-]+)|';		$repl[]='/$1.htm';
	$find[]='|/?\?m=contact(&?)|e';					$repl[]="'/contact'.('$1'?'?':'')";
	$find[]='|/?\?m=search(&?)|e';					$repl[]="'/search'.('$1'?'?':'')";
	$find[]='|/?\?m=cart(&?)|e';					$repl[]="'/cart'.('$1'?'?':'')";
	$find[]='|/?\?m=checkout(&?)|e';				$repl[]="'/checkout'.('$1'?'?':'')";
	$find[]='|/?\?m=catalog(?:&cat_id=([0-9-]+))?(?:&id=(\d*))?(?:&([^\'\"<>]*))?|e';		$repl[]="'/catalog'.('$1'?'/$1':'').('$2'?'/$2':'').('$3'?'/?$3':'')";
	$find[]='|/?\?m=auctions(?:&cat_id=([0-9-]+))?(?:&id=(\d*))?(?:&a=([a-z0-9_-]*))?(?:&([^\'\"<>]*))?|e';	$repl[]="'/auctions'.('$1'?'/$1':'').('$2'?'/$2':'').('$3'?'/$3':'').('$4'?'/?$4':'')";
	$str=preg_replace($find, $repl, $str);
 return $str;*/
}

//
function OUT_extmenu() {
 global $CFG, $SVARS, $HTML, $CFG, $m;
//<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
?>
<html><head><title>CNF - ξςψλϊ πιδεμ χωψι μχεηεϊ</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<?
if ($CFG['site_keywords']) echo '<meta name="keywords" content="'.htmlspecialchars($CFG['site_keywords']).'">';
if ($CFG['site_description']) echo '<meta name="description" content="'.htmlspecialchars($CFG['site_description']).'">';
?>
<link rel="shortcut icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/vnd.microsoft.icon"><link rel="icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/vnd.microsoft.icon">

<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all-rtl.css?<?=filemtime('skin/extjs/resources/css/ext-all-rtl.css');?>" />
<link rel="stylesheet" type="text/css" href="skin/style.css?<?=filemtime('skin/style.css');?>">

<script type="text/javascript" src="skin/extjs/ext-all.js?<?=filemtime('skin/extjs/ext-all.js')?>"></script>
<script type="text/javascript" src="skin/extjs/ext-rtl.js?<?=filemtime('skin/extjs/ext-rtl.js');?>"></script>
<script type="text/javascript" src="skin/extjs/src/locale/ext-lang-he.js"></script>

<script type="text/javascript" src="skin/aj.js?<?=filemtime('skin/aj.js');?>"></script>

<script type="text/javascript">
try{document.execCommand("BackgroundImageCache",false,true);} catch(err){}

var filteredAlerts, alertsWin, alertsPrevCount=0, checkCnt=0;
var alertsStore = new Ext.data.JsonStore({fields:['id', 'type', 'client_id', 'name', 'notes', 'end', 'remind']});

alertsStore.reload=function(){
	Ext.Ajax.request({
		url:'/?m=tasks/alerts_f&f=load_events',
		success:function(r){
			var d; try{ d=Ext.decode(r.responseText); }catch(e){}
			if (d){
				alertsStore.loadData(d);
				window.setTimeout("checkAlerts();",2000);
				window.setTimeout("checkAlerts();",60000);
				window.setTimeout("checkAlerts();",120000);
				window.setTimeout("checkAlerts();",180000);
				window.setTimeout("checkAlerts();",240000);
			}
		},failure:function(){}
	});
};

checkAlerts=function(){
	filteredAlerts = alertsStore.queryBy(function(record){return(record.data.remind<=new Date().format('Y-m-d H:i'))}).items;
	var count = filteredAlerts.length;
	if (!count && (!alertsWin || alertsWin.closed)) { return; }
	// else if (!count && alertsWin && !alertsWin.closed) { alertsWin.close(); return; }
	
	else if ((!alertsWin || alertsWin.closed) && alertsPrevCount<count) alertsWin=window.open('http://'+location.host+'/?m=tasks/alerts', 'alerts', 'width=400,height=250,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	else if (alertsWin.alertsV) { alertsWin.alertsV.syncStore(); if (alertsPrevCount<count) alertsWin.focus(); }
	alertsPrevCount=count;
};

Ext.onReady(function() {
	//Ext.QuickTips.init();

	Ext.TaskMgr.start({run:alertsStore.reload, interval:300000});
	// for (var i=0,len=alertsStore.data.items.length; i<len; ++i) Dump(alertsStore.data.items[i].data);
	
	Ext.History.init();
	
	Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
		onRender:function(ct, position){ this.el=ct.createChild({tag:'iframe', id:'if_'+this.id, name:this.id, frameBorder:0, src:this.url});}
	});

	var tabs = new Ext.TabPanel({
		region:'center',
		margins:'3 0 0 0',
		height:90,
		activeTab:0,
		border:false,
		id:'topTabs',
		listeners: { render: function(el) {document.getElementById('topTabs').firstChild.style.border=0; } },
		items:[
		<?
		if ($GLOBALS['PRS']) foreach ($GLOBALS['PRS'] as $tkey=>$tab) {
			if (is_array($tab) AND $tab[1]==1) {
				if ($tkey!='home_pan') echo ',';
				echo "{ title:'".addslashes($tab[0])."', xtype:'tabpanel', "
				."id:'$tkey', "
				."listeners: { render: function(el){document.getElementById('$tkey').firstChild.className+=' mystab';}"
							.",tabchange: function(tabPanel,tab){Ext.History.add(tabPanel.id+':'+tab.id);}"
				."}, "
				."activeTab:0";
				foreach ($tab as $button) {
					if (is_array($button) AND $button[3]==1 AND $button[0]) {
						if ($button[0]=='crm') $button[0]='crm/crm1';
						if (!$button[2]) $button[2]=$button[1];
						$item="{layout:'fit', id:'$button[0]', parent:'$tkey'"
							.",title:'<div style=\"float:right;\"><div style=\"width:40px; height:40px; margin:0 auto; background:url(/skin/micons/$button[2].png) no-repeat center; _background:transparent; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/skin/micons/$button[2].png\', sizingMethod=\'crop\');\"></div>$button[1]</div>'";
						if (eregi('^pages',$button[0])) $item.=",autoLoad: {url:'/?m=$button[0]'}"; # html page
						else $item.=",items:new Ext.ux.IFrameComponent({id:'if_$button[2]', url:'http://$_SERVER[HTTP_HOST]/?m=$button[0]'})";
						$item.='}';
						$items[]=$item;
					}
				}
				if ($items) echo ", items:[".implode(',',$items)."]";
				unset($items);
				echo "}\r\n";
			}
		}
		?>
		]
	});
	

	Ext.History.on('change', function(token){
		if(token){
			var parts = token.split(':');
			var tabPanel = Ext.getCmp(parts[0]);
			var tabId = parts[1];
			tabPanel.show();
			tabPanel.setActiveTab(tabId);
		}
	});
	
	var href=top.location.href;
	if (href.indexOf("#")>=0) {	// activate tab
		try{
			var token=href.substr(href.indexOf("#")+1);
			var parts=token.split(':');
			if (parts[1]) {
				tabs.setActiveTab(parts[0]);
				tabs.getItem(Ext.getCmp(parts[0])).setActiveTab(parts[1]);
			}else if (Ext.getCmp(token).parent) {
					tabs.setActiveTab(Ext.getCmp(token).parent);
					tabs.getItem(Ext.getCmp(token).parent).setActiveTab(token);
			}else tabs.setActiveTab(token);
		} catch(e){}
	}

	
	new Ext.Viewport ({
		layout: 'border',
		defaults: { collapsible:false, split:false },
		items:tabs
	});
});
</script>
</head>
<body>

<form id="history-form" class="x-hidden">
    <input type="hidden" id="x-history-field" />
    <iframe id="x-history-frame"></iframe>
</form>

</body></html>
<?
}


//
function OUT() {
 global $CFG, $HTML, $CFG, $m;
//<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	if (!$CFG['site_title']) $CFG['site_title']='CNF - ξςψλϊ πιδεμ χωψι μχεηεϊ';
?>
<html><head><title><?=htmlspecialchars($CFG['site_title'])?></title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<?
if ($CFG['site_keywords']) echo '<meta name="keywords" content="'.htmlspecialchars($CFG['site_keywords']).'">';
if ($CFG['site_description']) echo '<meta name="description" content="'.htmlspecialchars($CFG['site_description']).'">';
?>
<link rel="shortcut icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/vnd.microsoft.icon"><link rel="icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/vnd.microsoft.icon">

<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all-rtl.css?<?=filemtime('skin/extjs/resources/css/ext-all-rtl.css');?>" />
<link rel="stylesheet" type="text/css" href="skin/style.css?<?=filemtime('skin/style.css');?>">
<?
if ($_SERVER['HTTP_HOST']=='shaacnf.loc') {	# use debug version on localhost
	echo '<script type="text/javascript" src="skin/extjs/adapter/ext/ext-base-debug.js?'.filemtime('skin/extjs/adapter/ext/ext-base-debug.js').'"></script>';
	echo '<script type="text/javascript" src="skin/extjs/ext-all-debug.js?'.filemtime('skin/extjs/ext-all-debug.js').'"></script>';
}else echo '<script type="text/javascript" src="skin/extjs/ext-all.js?'.filemtime('skin/extjs/ext-all.js').'"></script>';
?>
<script type="text/javascript" src="skin/extjs/ext-rtl.js?<?=filemtime('skin/extjs/ext-rtl.js');?>"></script>
<script type="text/javascript" src="skin/extjs/src/locale/ext-lang-he.js"></script>

<script type="text/javascript" src="skin/aj.js?<?=filemtime('skin/aj.js');?>"></script>
<script type="text/javascript" src="skin/boxover.js"></script>
<script>try{document.execCommand("BackgroundImageCache",false,true);} catch(err){}</script>

</head>
<body scroll=no dir=ltr>

<?
// $_GET['sMode']!='focus' AND 
if (!ereg('^shop|admin/shop|login|crm/fax|admin',$m) AND !$_GET['UID'] AND !$_GET['init'] AND !$_GET['client_id']) { # redirect to /?mainmenu (if not in frame)
	echo "<script> if (top==self) { top.location.href='/?mainmenu".($m?"#$m":'')."'; } </script>";
}
?>

<div dir=rtl style='width:100%; height:100%; overflow:auto; zoom:1'>
<? if (ereg('^admin/shop/',$m)) shop_admin_tabs(); ?>
<?=$HTML['center'];?>
</div>

</body></html>
<?
}
//
function OUT_home() {
 global $CFG, $HTML, $CFG, $m;
?>
<html dir=rtl><head><title>CNF - ξςψλϊ πιδεμ χωψι μχεηεϊ</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<?
if ($CFG['site_keywords']) echo '<meta name="keywords" content="'.htmlspecialchars($CFG['site_keywords']).'">';
if ($CFG['site_description']) echo '<meta name="description" content="'.htmlspecialchars($CFG['site_description']).'">';
?>
<link rel="shortcut icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/vnd.microsoft.icon"> 
<link rel="icon" href="http://<?=$_SERVER['HTTP_HOST']?>/favicon.ico" type="image/vnd.microsoft.icon">
<link rel=stylesheet href="skin/style.css">
<SCRIPT language=JavaScript src="skin/home/writeflash.js"></SCRIPT>
</head>

<body dir=rtl bgcolor=#7184B0 topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 marginwidth=0 marginheight=0>
<div align=center>

<table border=0 cellpadding=0 cellspacing=0 width=756><tr>
<td><SCRIPT language=JavaScript>writeflash("skin/home/top.swf",756,117);</SCRIPT><noscript><embed src="skin/home/top.swf" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width=756 height=117 quality=High></noscript></td>
</tr><tr>
<td valign=top style="background-image: url('skin/home/bg.jpg'); background-repeat: repeat-x" bgcolor=#FFFFFF>

<table border=0 cellpadding=0 cellspacing=0 width=100% style="background-repeat: repeat-x" height=440>
<tr valign=top>
<td style="background-image: url('skin/home/shadow_right.png'); background-repeat: repeat-y"><img border=0 src="skin/home/shadow_right.png" width=8 height=8></td>
<td style="padding:15px">

<table border=0 cellpadding=0 cellspacing=0 width=170 style="background-image: url('skin/home/login_win.png'); background-repeat: no-repeat" height=143>
<tr><td align=center style='padding-top:5px'>

<table border=0 cellpadding=0 cellspacing=3 width=150>
<script>
var isIE=false; var isMaxthon=false;
if (navigator.userAgent.indexOf("Opera")==-1 && document.all) isIE=true;
if (isIE && navigator.userAgent.indexOf('Maxthon')>0) isMaxthon=true;

function openbafi(frm) {
	if (login_form.username.value=='' || login_form.password.value=='') { alert('ΰπΰ δχμγ ΰϊ ων ξωϊξω εριρξδ'); login_form.username.focus(); return false; }
	if (isIE && !isMaxthon) {
		var win=top.window.open('','bafi','width=800,height=550,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
		if (win) {
			frm.target='bafi';
			win.focus();
			window.setTimeout("winClose()",1000);
		}
	}
}

function winClose() {
	top.window.opener=null;
	top.window.open('', '_parent', '');
	top.window.close();
}
</script>
<form name=login_form action="?m=login" method=post target=_top onSubmit="return openbafi(this);">
<tr><td width=50%><b>ων ξωϊξω</b></td><td><input type=text class=text name=username size=7 style="width:100%" dir=ltr></td></tr>
<tr><td><b>ριρξΰ</b></td><td><input type=password class=text name=password size=7 style="width:100%" dir=ltr></td></tr>
<tr><td colspan=2 align=center><br><input type=image src="skin/home/b_login.png" width=62 height=23 style='border:0'></td></tr>
<input type=hidden name=submit value=1>
</form>
</table>
<script>
login_form.username.focus();
</script>
</td></tr></table>

<div align=center style='padding-top:10px'>
<a href='?m=home/register'><img border=0 src="skin/home/b_new_user.png" width=86 height=23></a><br><br><br>
<div align=right style="color:#0f719d;"><b>ξωϊξω <font color="#ff9900">χιιν</font>, ΰπΰ δχμγ ων ξωϊξω
εριρξΰ εδξωκ αςαεγδ πςιξδ.<br><br>
ξωϊξω <font color="#ff9900">ηγω</font>, δχμχ ςμ "ρελο ηγω" ,
ξμΰ ΰϊ δθετρ δξφ"α επηζεψ ΰμικ αδχγν.
</b></div>
</div>

<? if ($HTML['right']) echo "<br>$HTML[right]"; ?>

<div align=center style='padding-top:57px'>
<a href='http://ex4.b-com.co.il/' target=_blank><img border=0 src="skin/home/advertise_here.png" width=84 height=22></a>
</div>


</td>
<td width=100% style="background-image: url('skin/home/bg_left.jpg'); background-repeat: no-repeat; background-position: left top; padding-right:5px; padding-top:15px; padding-bottom:15px">

<table border=0 cellpadding=0 cellspacing=0 width=519>
<tr><td><img border=0 src="skin/home/w1_top.png" width=519 height=6></td></tr>
<tr><td bgcolor=#FFFFFF style="border-left: 1px solid #7090AC; border-right: 1px solid #7090AC; padding-left: 15px; padding-right: 15px; padding-top:10px; padding-bottom:10px" height=380 valign=top>

<?
echo $HTML['top'];

if ($HTML['center']) echo $HTML['center'];
else echo "&nbsp;";
?>

</td></tr>
<tr><td><img border=0 src="skin/home/w1_bottom.png" width=519 height=6></td></tr>
</table>

</td>
<td style="background-image: url('skin/home/shadow_left.png'); background-repeat: repeat-y"><img border=0 src="skin/home/shadow_left.png" width=7 height=7></td>
</tr></table>
</td>
</tr>
<tr><td background="skin/home/bottom.png" height=36 valign=top style="padding-top:5px; padding-right:15px; padding-left:15px;">
<table border=0 cellpadding=0 cellspacing=0 width=100%><tr>
<td style="color:#4E779B">λμ δζλειεϊ ωξεψεϊ</td>
<td style="color:#4E779B" align="left">λϊεαϊ: ψηεα δϊςωιδ 51 πωψ, ξιχεγ 36601 &nbsp; &nbsp; ωιψεϊ μχεηεϊ: 073-2609050, τχρ: 073-2609051</td>
</tr></table>
</td></tr></table>
</div>
</body></html>
<?}?>