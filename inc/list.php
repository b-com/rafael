<?

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function get_list(&$TABLE) {
 global $PHP_SELF, $p, $next_prev_links;
	if ($TABLE['max_per_page']) $max_per_page=$TABLE['max_per_page'];
	//elseif (!$TABLE['no_pages']) $max_per_page=30;		#set default
	
	if ($TABLE['collapse_by'] AND !$TABLE['primary']) die('Use $TABLE[collapse_by] need $TABLE[primary] and $TABLE[trigger]');

	
	#set $fields_list
	if (is_array($TABLE['fields'])) {
		reset($TABLE['fields']);
		while($field=each($TABLE['fields'])) if ($field[1]['db']) $fields_list.=($fields_list?',':'').($field[1]['db_name'] ? $field[1]['db_name'] : $field[0]);
	}elseif ($TABLE['fields']) $fields_list=$TABLE['fields'];
	else $fields_list='*';
	
	#set ORDER BY
	if ( $_REQUEST['ORDER_BY'] AND ($TABLE['order_allow']=='*' OR (is_array($TABLE['order_allow']) AND in_array($_REQUEST['ORDER_BY'],$TABLE['order_allow']))) ) $ORDER_BY=$_REQUEST['ORDER_BY'];
	if (!$ORDER_BY AND $TABLE['order_by']) $ORDER_BY=$TABLE['order_by'];	# set to default
	if ($ORDER_BY) $ORDER=" ORDER BY $ORDER_BY".($_REQUEST['DESC']?' DESC':'');

	# set $LIMIT
	if ($max_per_page) {
		$page_start = $p>0 ? ($max_per_page*$p) : 0;
		$LIMIT="LIMIT $page_start, $max_per_page";
	}

	# Load data
	$sql="SELECT".($LIMIT?' SQL_CALC_FOUND_ROWS':'')." $fields_list FROM $TABLE[table]"
		.($TABLE['join']  ? " $TABLE[join]":'')
		.($TABLE['where'] ? " WHERE $TABLE[where]" : '')
		.($TABLE['group_by'] ? " GROUP BY $TABLE[group_by]" : '')
		.($TABLE['having'] ? " HAVING $TABLE[having]" : '')
		.($ORDER ? " $ORDER" :'')
		.($LIMIT ? " $LIMIT" :'');
	//echo "<div dir=ltr>\$sql=$sql</div><hr>";

	$TABLE['rows'] = sql2array($sql, ($TABLE['primary']?$TABLE['primary']:'') );
	if ($TABLE['collapse_by'] AND $TABLE['rows']) foreach ($TABLE['rows'] as $k=>$r) $TABLE['rowsSubs'][$r['parent_id']][$r[$TABLE['primary']]]=&$TABLE['rows'][$k];

	$re_get=list_reget(&$TABLE);

	# count number of rows
	if (!$LIMIT) $TABLE['num_rows']=count($TABLE['rows']);
	else $TABLE['num_rows']=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if ($max_per_page) {
		#set number of pages and start/end page
		$page_end=$page_start+$max_per_page;	if ($page_end>$TABLE['num_rows']) $page_end=$TABLE['num_rows'];
		$pages_num=floor($TABLE['num_rows']/$max_per_page); if (floor($TABLE['num_rows']/$max_per_page)==($TABLE['num_rows']/$max_per_page)) $pages_num--;
		$TABLE['pages_num']=$pages_num;
		if ($TABLE['aj_link']) $aj_link="onClick='return alink(this);'";
		# Set $next_prev_links - Next and Prev page links
		$next_prev_links='';
		$max_links=19; # set maximum number of links +1 (1 2 3..)
		if (!$TABLE['no_pages'] AND $pages_num>0) {
			if ($p>0) $next_prev_links.="<A HREF=\"?$re_get&p=".($p-1)."\" $aj_link>&lt;&lt;&lt;</A> &nbsp;";		#show previous link
			$start_page=$p-floor($max_links/2); if ($start_page<0) $start_page=0;
			$end_page=$start_page+$max_links; if ($end_page>$pages_num) { $end_page=$pages_num; $start_page=$pages_num-$max_links; if ($start_page<0) $start_page=0; }
			# show 1 2 3... links
			for($i=$start_page; $i<=$end_page; $i++) {
				if ($i==$start_page AND $start_page!=0) $next_prev_links.="<A HREF=\"?$re_get\" $aj_link>1</A> ... ";
				$next_prev_links.=( $p==$i ? "<b>".($i+1)."</b>" : "<A HREF=\"?$re_get&p=$i\" $aj_link>".($i+1)."</A>" );
				if ($i==$end_page AND $i<$pages_num) $next_prev_links.=" ... <A HREF=\"?$re_get&p=$pages_num\" $aj_link>$pages_num</A>";
				$next_prev_links.=" &nbsp;";
			}
			if ($p<$pages_num) $next_prev_links.="<A HREF=\"$PHP_SELF?$re_get&p=".($p+1)."\" $aj_link>&gt;&gt;&gt;</A>";		#show next link
			$TABLE['next_prev_links']=$next_prev_links;
		}
	}
 return $TABLE['rows'];
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function print_TABLE(&$TABLE) {
	echo set_TABLE(&$TABLE);
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function list_reget(&$TABLE, $ar=false) {
	$re_get_vars=$TABLE['re_get'];
	$re_get_vars[]='m';
	$re_get_vars[]='a';
	$re_get_vars[]='ORDER_BY';
	$re_get_vars[]='DESC';
	foreach ($re_get_vars as $var) {
		if (isset($ar[$var])) {if ($ar[$var]!=='') $re_get_ar[$var]="$var=".urlencode($ar[$var]);}
		elseif ($_REQUEST[$var]!='') $re_get_ar[$var]="$var=".urlencode($_REQUEST[$var]);
	}
	if (is_array($re_get_ar)) $re_get=implode('&',$re_get_ar);
	//echo "<div dir=ltr align=left>\$re_get: $re_get</div>";
 return $re_get;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function set_TABLE(&$TABLE) {
	if (!is_array($TABLE['rows'])) { $table_html.=($TABLE['no_record']?$TABLE['no_record']:''); return; }
 	if (!$TABLE['id']) $TABLE['id']='list_table';
 	
	$table_html="<div id='$TABLE[id]_container'><a name='a$TABLE[id]'></a>";

	if (!$TABLE['id']) $TABLE['id']='list_table';
	if (!$TABLE['class']) $TABLE['class']='t_list';

	#print table start
	$table_html.="\n<table border=0 width='$TABLE[width]' cellspacing=0 cellpadding=1 id='$TABLE[id]' class=$TABLE[class]".($TABLE['add']?" $TABLE[add]":'').">\n";


	if ($TABLE['caption']) $table_html.="<caption>$TABLE[caption]</caption>\n";
	
	#count number of columns and field_titles
	foreach ($TABLE['fields'] as $row) { if ($row['type']) $cols++; if ($row['title']) $field_titles=true; if ($row['width'] OR $row['align'] OR $row['class'] OR $row['col_add']) $colgroups=true; }	
	
	#show COLGROUPs
	if ($TABLE['colgroups']) $table_html.=$TABLE['colgroups'];
	elseif ($colgroups) {
		$table_html.="<colgroup>";
		foreach ($TABLE['fields'] as $fld=>$row) 
			if ($row['type']) $table_html.='<col'. ($row['width']?" width=$row[width]":'') . ($row['align']?" align=$row[align]":'') . ($row['class']?" class=$row[class]":'') . ($row['style']?" style='$row[style]'":'') . ($row['dir']?" dir=$row[dir]":'') . ($row['col_add']?" $row[col_add]":'') .'>';
		$table_html.="</colgroup>\n";
	}

	#show field_titles row
	if ($field_titles) {
		$table_html.="\n<thead><tr>";
		foreach ($TABLE['fields'] as $fld=>$row) {
			if ($row['type']) {
				if (!$row['title']) $row['title']='&nbsp;';
				if ($TABLE['checkboxes'] AND $fld==$TABLE['trigger']) $row['title']="<span style='float:right'><input type=checkbox class=inputc onClick='checkAll(this)'></span>$row[title]";
				if ( $row['order'] OR ($TABLE['order_allow']=='*' AND $row['db']) OR (is_array($TABLE['order_allow']) AND in_array($fld,$TABLE['order_allow'])) ) 
					$table_html.=
					($TABLE['aj_link'] ? 
						"<th onClick=\"Aj1.get('?".list_reget(&$TABLE, array('ORDER_BY'=>$fld,'DESC'=>''))."&tonly=1', '$TABLE[id]_container'); return false;\" onContextmenu=\"Aj1.get('?".list_reget(&$TABLE, array('ORDER_BY'=>$fld,'DESC'=>1))."&tonly=1', '$TABLE[id]_container'); return false;\">" :
						"<th onClick=\"location='?".list_reget(&$TABLE, array('ORDER_BY'=>$fld,'DESC'=>''))."'\" oncontextmenu=\"location='?".list_reget(&$TABLE, array('ORDER_BY'=>$fld,'DESC'=>1))."'; return false;\">")
					.($_REQUEST['ORDER_BY']==$fld ? "<img src='skin/icons/".($_REQUEST['DESC']?"orderu.gif":"orderd.gif")."' align=middle width=8 height=7 border=0>" :'')
					."$row[title]</th>";
				else $table_html.="<th>$row[title]</th>";
			}
		}
		$table_html.="</tr></thead>\n";
	}

	//$table_html.="<tbody>\n";
	$table_html.=set_TABLE_rows(&$TABLE);
	//$table_html.="</tbody>\n";

	
	$table_html.="{$TABLE['end_row']}</table>\n";		# close table
	
	//if ($TABLE['js_sortable'] OR $TABLE['js_scrollable']) 


	#print next_prev_links
	if ($TABLE['next_prev_links']) $table_html.="<center><b>$TABLE[next_prev_links]</b></center>";
	else $table_html.="<br>";
	
	$table_html.="</div>";
	
	if (!$_GET['tonly']) 
		$table_html.="
<script type='text/javascript' src='skin/table.js'></script>\n
<script>
".(!$TABLE['expanded']?'collapseAllRows();':'')."
addEvent('$TABLE[id]', 'mouseover', listTableOver);
addEvent('$TABLE[id]', 'mouseout', listTableOut);
//addEvent('$TABLE[id]', 'click', listTableClick);
alink=function (el) {
	Aj1.get(el.href+'&tonly=1', '$TABLE[id]_container');
	location='#a$TABLE[id]';
	return false;
}
</script>";
if ($TABLE['js_sortable']) $table_html.="<script type='text/javascript'>var t=new SortableTable(document.getElementById('$TABLE[id]'));</script>\n";
if ($TABLE['js_scrollable']) $table_html.="<script type='text/javascript'>var t=new ScrollableTable(document.getElementById('$TABLE[id]'),200);</script>\n";

 return $table_html;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function get_parent($start_id, $maxlevel=0, $level=0) {
	while ($parent=$cats[$parent]['parent_id']) {
		if ($cfg['link']) $title="<a href='$cfg[link]$parent' dir=rtl>".htmlspecialchars($cats[$parent]['title'])."</a>";
		else $title="<span dir=rtl>".htmlspecialchars($cats[$parent]['title'])."</span>";
		$cat_full_name=$title.$cfg['split'].$cat_full_name;
	}
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function set_TABLE_rows(&$TABLE, $sub_id=0, $level=0) {
	if ($TABLE['collapse_by']) $rows=&$TABLE['rowsSubs'][$sub_id];
	else $rows=&$TABLE['rows'];
	
	if ($rows) foreach ($rows as $k=>$r) {
		//echo "<div dir=ltr align=left><pre>".print_r($r,1)."</pre></div>";
		if ($r['prepend_rows']) $table_html.=$r['prepend_rows'];
		
		$r['patch'][] = $parent = $k; # self id
		if ($TABLE['collapse_by']) while ($parent=$TABLE['rows'][$parent]['parent_id']) array_unshift($r['patch'], $parent);	# add parents
		//echo implode('-',$r['patch']).'<br>';
		
		
		/**/
		$table_html.="<tr id='".implode('-',$r['patch'])."' class=tr$level{$TABLE['row_add']}{$r['row_add']}{$r['row_add']}>";
		foreach ($TABLE['fields'] as $f=>$c) {
			if ($c['type']=="text") {
				if ($TABLE['checkboxes'] AND $f==$TABLE['trigger']) $r[$f]="<input type=checkbox class=inputc name='c[]' value='{$r[$TABLE['primary']]}'> ".$r[$f];

				if (trim($r[$f])=='') $r[$f]='&nbsp;';
				elseif ($TABLE['collapse_by'] AND $TABLE['trigger']==$f) {
					if ($TABLE['rowsSubs'][$k]) $r[$f]="<div class=".($TABLE['expanded']?'close':'open')." onclick='toggleRows(this)'></div>".$r[$f];
					else $r[$f]="<div class=blank></div>".$r[$f];
				}
				$table_html.="<td{$TABLE['cell_add']}{$c['add']}>{$r[$f]}</td>";
			}
		}
		
		$table_html.="</tr>\n";
		if ($r['append_rows']) $table_html.=$r['append_rows'];
		
		
		//echo "<div dir=ltr align=left><pre>$k - ".print_r($TABLE['rowsSubs'][$k],1)."</pre></div>";
		if ($TABLE['collapse_by'] AND $TABLE['rowsSubs'][$k]) $table_html.=set_TABLE_rows(&$TABLE, $k, $level+1);
	}
 return $table_html;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function csv_export(&$TABLE) {
	$csv='';
	if (!is_array($TABLE['rows'])) return;
	$fieldsar=&$TABLE['fields'];

	#count columns number and field_titles
	foreach ($fieldsar as $row) if ($row['title']) $field_titles=true;
	
	#show field_titles row
	if ($field_titles) {
		reset($fieldsar);
		while($field=each($fieldsar)) {
			if ($field[1]['type']) $csv.=($csv?',':'').'"'. str_replace('"', '""', $field[1]['title']) .'"';
		}
		$csv.="\n";
	}

	#show data rows
	for ($i=0; $i<sizeof($rows); $i++) {
		$row=$rows[$i];
		reset($fieldsar);
		$line='';
		while($field=each($fieldsar)) {
			if ($field[1]['type']=="text") $line.=($line?',':'').'"'. str_replace('"', '""', $row[$field[0]]) .'"';
		}
		$csv.="$line\n";
	}
 return $csv;
}


?>