
function createMailSuperCombo(someConf,config){
	
	MailSuperCombo = function(someConf,config) {
		this.store = new Ext.data.Store({
				proxy: new Ext.data.HttpProxy({url:'?m=mail_new/mail_f&f=email_list'}),
				reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['name','email','mail_to'])
			});
	
	var mailto = new Array();
	
	if(config && someConf.name=='emails' && (config.action=='reply' || config.action=='replyall')){
		mailto[0] = config.prevMail.replyto ? config.prevMail.replyto[0].name : config.mailData.from[0];
		mailto[1] = config.prevMail.replyto ? config.prevMail.replyto[0].mailto : config.mailData.from[1];
			var Record = Ext.data.Record.create(['name','email']);
		var rec = new Record({name:(mailto[0] ? mailto[0] : mailto[1]),email:mailto[1]});
			this.store.add(rec);
		if(config.action=='reply')
			this.setValue(mailto[1]);
		}
	
	// Look for
	var propertyName = someConf.name == 'emails' ? 'to' : someConf.name;
	if(config && config.action=='replyall' && propertyName != '' && config.prevMail[propertyName]){
			var Record = Ext.data.Record.create(['name','email']);
		var cc = null;
		var rec = null;

		for (var i=0; i<config.prevMail[propertyName].length; i++) {
			cc = config.prevMail[propertyName][i];
			if(cc.mailto == config.account.email) continue;// exclude self email
			rec = new Record({name:(cc.name ? cc.name : cc.mailto),email:cc.mailto});
				this.store.add(rec);
				mailto.push(cc.mailto);
			}
			this.setValue(mailto.join(','));
		}
		
		config = {name:someConf.name,allowBlank:someConf.allowBlank};
		
		MailSuperCombo.superclass.constructor.call(this, {
			xtype:'superboxselect',
			store:this.store,
			//setAutoScroll:true,
			ctCls:'mailSCB',
			autoScroll:true,
			msgTarget:'qtip',
			allowAddNewData:true,
			width:(MailComposeWindowCfg.width-180),
			itemDelimiterKey:{'9':true,'13':true,/*'32':true,'59':true,*/'222':true},
			emptyText: 'הקלד דוא"ל',
			colspan:3,
			resizable: true,
			minChars: 3,
		// pageSize:10, // # $_POST[start],POST[limit] parameters
			name: 'm_client',
			valueField: 'email',
			displayField:'name',
			regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/,
			tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">&lt;{email}&gt;<i>{name}</i></div></tpl>'),
			itemSelector: 'div.search-item',
			displayFieldTpl: '{name}',
			forceSelection : true,	
			extraItemCls:'x-tag',
			listeners: {
				newitem: function(bs,v){
					v = v.toLowerCase();
					if (/^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/.test(v)){
						var newObj = {email: v,name: v};
						bs.addNewItem(newObj);
					}
				}
			}
		});
		Ext.apply(this, config);
	};
	Ext.extend(MailSuperCombo, Ext.ux.form.SuperBoxSelect);
	
	return new MailSuperCombo(someConf,config);
}

AttUploadWin = function(timestart) {
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',fileUpload:true,labelWidth:55,defaultType:'textfield',
		items: [
			{xtype:'fileuploadfield', name:'file', emptyText:'בחר קובץ...', fieldLabel:'שם קובץ', anchor:'95%', allowBlank:false,
				regex:/^[^\/\\\?:*<>"|]+$/, invalidText:'שם קובץ אינו יכול להכיל את התווים הבאים:<br><b>\\ / * ? " &lt; &gt; |</b>',
				listeners: { scope:this,
					fileselected:function(fld,v){ 
						v = v.replace(/\//g, '\\');
						v = v.replace(/^([^\\]+\\)*/g, '');	// remove path
						var regs = /^(.+)\.(.{2,4})$/.exec(v);
						if (regs && regs[2]) { fld.el.dom.value=regs[1]; this.fileExt=regs[2]; }
						else fld.el.dom.value=v;
						fld.validate();
						fld.el.dom.setAttribute('name','file_name');
						fld.el.dom.removeAttribute('readOnly');
						fld.el.dom.style.color='black';
					}
				}
			 }
		]
	});

	AttUploadWin.superclass.constructor.call(this, {
		title:'העלאת מסמך',	iconCls:'upload', width:430,height:400, modal:true, resizable:false, layout:'fit',	plain:true,	bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		var fileName = this.form.form.findField('file_name').getRawValue().trim();
		if (this.fileExt) fileName=fileName+'.'+this.fileExt;
	//	var fileExt=this.fileExt;
		
		this.form.form.submit({
			url: '?m=common/files&f=upload_files&client_id=-1&client_type=mail',
			params:{timestart:timestart},
			waitTitle: 'אנא המתן...', waitMsg: 'טוען את הקובץ...',
			scope:this,
			success: function(r,o){Ext.getCmp('MailComposeWindow').getAttachments(); this.close();},
			failure: function(r,o){this.close();ajRqErrors(o.result);}
		});
	};	
};
Ext.extend(AttUploadWin, Ext.Window);


MailComposeWindow = function(icon, menuTree, email, altConfig) {
	var parentsNodes = null;
	var menuItems =[];
	var defaultId = 0;
	var sentFolder='';
	var template = null;
	
	if(menuTree){
		this.menuTree = menuTree;
		parentsNodes = this.menuTree.getRootNode();
	}else{
		if(altConfig){
			menuItems = altConfig.menuItems;
			defaultId = altConfig.defaultId;
			sentFolder = altConfig.sentFolder;
		}
	}
	
	this.sending = false;
//	checkedMail={}; // [id]-> check
//	MailbyId={};	// id -> mail
//	idsChecked=[];	// ids that checked
	this.ScanWin = function(timestart){
		var win=window.open(CFG.host+'/crm/?m=common/scan&client_id=-1&client_type=mail&timestart='+timestart, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
		win.focus();
	};
	
	this.delFile = function(file_name,size){
	Ext.MessageBox.show({
		width:400, title:'למחוק קבצים?', msg:'האם ברצונך למחוק את קבצים:<br>'+file_name+' ?', buttons:Ext.MessageBox.YESNO, scope:this,
		icon:Ext.MessageBox.QUESTION,
		fn: function(btn) { if(btn=='yes') {
			Ext.Ajax.request({
				url: '?m=mail_new/mail_f&f=del_attachment&file_name='+file_name,
				params:{timestart:this.form.find('name','timestart')[0].getValue()},
				success: function(r,o){
					r=Ext.decode(r.responseText);
					if (!ajRqErrors(r)) {
						var tb = this.form.find('hiddenName','attach')[0].getTopToolbar();
						tb.remove(tb.find('text',file_name+'<span dir=ltr> ('+size+')</span>')[0]);
					}	
				},
				failure: function(r){ Ext.Msg.alert('Error', 'Request failed.'); },
				scope: this
			});
			}}
		});
	};
	
	this.preview = function(file_name) {
		var previewWin = Ext.getCmp('previewWinmail');
		var url = CFG.host+'/crm/?m=common/get&get_attachment=1&filetimestart='+this.form.find('name','timestart')[0].getValue()+'&file='+encodeURIComponent(file_name);

		if (!previewWin) previewWin = new Ext.Window({
			id:'previewWinmail', html:'<iframe id=prevIframe src="about:blank" style="width:100%; height:100%;" frameBorder=0></iframe>',
			x:5, y:50, width:(Ext.getBody().getWidth()-270), height:(Ext.getBody().getHeight()-55), plain:true, border:false, layout:'fit', closable:false,
			listeners: { hide:function(){Ext.get('prevIframe').dom.src='about:blank';} }
		});

		if ((/\.(pdf|jpg|jpeg|gif|png|txt)$/i).test(file_name)) {
			previewWin.setTitle('<div class="x-tool x-tool-close" style="float:right" onClick="Ext.getCmp(\'previewWinmail\').close();"></div> <span>'+file_name+'</span>');
			previewWin.show();
			var iframe = Ext.get('prevIframe');
			if (iframe.dom.src!='about:blank') iframe.dom.src='about:blank';
			iframe.dom.src=url;
		}else window.open(url+'&download=1', '_blank');
	};

	this.getAttachments = function(addParams){
		var paramsObj = new Object();
		if(typeof addParams == 'object')
			paramsObj = addParams;
		paramsObj.timestart = this.form.find('name','timestart')[0].getValue();
		
		Ext.Ajax.request({
				url:'?m=mail_new/mail_f&f=get_attachments',
				params:paramsObj,
				success: function(r,o){
					r=Ext.decode(r.responseText);
					var tb = this.form.find('hiddenName','attach')[0].getTopToolbar();
					if (r.data.length){
						tb.remove(tb.find('text','הסר הכל')[0]);
						Ext.each(r.data, function(r){
							// tb.remove(tb.find('text',r.file_name+'<span dir=ltr> ('+r.size+')</span>')[0]);
							if (!tb.find('text',r.file_name+'<span dir=ltr> ('+r.size+')</span>')[0])
							tb.insert(tb.items.length,{text:r.file_name+'<span dir=ltr> ('+r.size+')</span>', hiddenName:r.file_name, iconCls:file_cls(r.file_name) ,menu: {items: [
								{text:'הצג',iconCls:'preview', scope:this, handler:function(){this.preview(r.file_name);}},
								{text:'הסר',iconCls:'delete', scope:this, handler:function(){this.delFile(r.file_name,r.size);}},
							]}});
						},this);	
						tb.insert(tb.items.length,{text:'הסר הכל', iconCls:'delete', scope:this, handler:function(){
							Ext.Ajax.request({
								url:'?m=mail_new/mail_f&f=del_attachments',
								params:{timestart:this.form.find('name','timestart')[0].getValue()},
								success: function(r,o){
									r=Ext.decode(r.responseText);
									for (var i=tb.items.items.length-1; i>0; i--) tb.remove(tb.items.items[i]);
								},scope:this
							});

						}});
						this.form.find('hiddenName','attach')[0].doLayout();
					}
					tb.remove(tb.find('name','attachLoad')[0]);
					this.form.find('name','send_btn')[0].enable();
				},
				failure: function(r){ 
					tb.remove(tb.find('name','attachLoad')[0]);
					this.form.find('name','send_btn')[0].enable();
					this.getEl().unmask(); 
					Ext.Msg.alert('Error', 'Request failed.');},
				scope: this
			});
	};
	if(parentsNodes){
		for(var i = 0; i < parentsNodes.childNodes.length; i++ ){
			if(!parentsNodes.childNodes[i].attributes.sendReady) continue;
			if(parentsNodes.childNodes[i].attributes.primary){
				defaultId = parentsNodes.childNodes[i].attributes.root_id;
				sentFolder= parentsNodes.childNodes[i].attributes.sentFolder;
			}
			if (parentsNodes.childNodes[i].attributes.root_id) menuItems.push({text:parentsNodes.childNodes[i].attributes.email, sentFolder:parentsNodes.childNodes[i].attributes.sentFolder, account:parentsNodes.childNodes[i].attributes.root_id, scope:this,handler:
				function(me){
					this.form.find('name','account')[0].setValue(me.account);
					this.form.find('name','sentFolder')[0].setValue(me.sentFolder);
					sentFolder = me.sentFolder;
					Ext.each(this.menu.items.items,function(item){item.setIconClass('');},this);
					me.setIconClass('online');
				}
			});
		}
	}
	
	this.menu = new Ext.menu.Menu({autoWidth:true, items:menuItems});
		
	this.form = new Ext.form.FormPanel({id:'MailForm',
		baseCls: 'x-plain',
		items: [
			{xtype:'container', name:'upperContainer', layout:'table',layoutConfig:{columns:4},baseCls:'x-plain', items:[
				 {xtype:'button',iconAlign:'top',name:'send_btn',iconCls:'send-mail',text:'שלח',style:'margin:5px;',width:70,height:40, rowspan:3, handler:function(){this.submitForm();}, scope:this}
				,{xtype:'label',style:'display:block;align:left;',text: 'אל :'},createMailSuperCombo({name:'emails', allowBlank:false},email)
			//	,{xtype:'button',width:20, iconCls:'group',style:'display:block; text-align:left; '	,scope:this,handler:function(){var win=new sendToGroup('emails'); win.show(); win.center();}}
				,{xtype:'label',style:'display:block;align:left;',text:'Cc :'},createMailSuperCombo({name:'cc', allowBlank:true},email)
			//	,{xtype:'button',width:20, iconCls:'group',style:'display:block; text-align:left; ', scope:this,handler:function(){ var win=new sendToGroup('cc'); win.show(); win.center();}}
				,{xtype:'label',style:'display:block;align:left;',text:'Bcc :'},createMailSuperCombo({name:'bcc', allowBlank:true},email)				

				,{xtype:'container', colspan:5,layout:'column', items:[
					 {xtype:'button',text:'חשבון',width:70,style:'margin:0 5 5 0;', scope:this, menu:this.menu}
					,{xtype:'label',text:'נושא:', style:'display:block;align:left;'}
					,{name:'subject',width:(MailComposeWindowCfg.width-295),xtype:'textfield', allowBlank:false }
					,{xtype:'label', text: 'חשיבות:'}
					,{xtype:'combo', hiddenName:'mail_importance', width:60, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:[[4,'נמוכה'],[0,'רגילה'],[2,'גבוהה']]}
				]}

//				,{xtype:'button',text:'חשבון',width:70,rowspan:2,style:'margin:0 5 5 5;', scope:this, menu:this.menu}
//				,{xtype:'label',text:'נושא:'},{name:'subject',width:(MailComposeWindowCfg.width-180),xtype:'textfield', allowBlank:false }
//				
//				,{xtype:'label',style:'display:block;align:left;margin-top:2px;',text: 'חשיבות:'}
//				,{xtype:'combo', hiddenName:'mail_importance', width:60, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:[[4,'נמוכה'],[0,'רגילה'],[2,'גבוהה']]}
				
				//,{xtype:'label',style:'display:block;align:left;',text:'Bcc :'},{name:'bcc',width:740,xtype:'textfield'}
			 ]}
			,{xtype:'panel', baseCls:'x-plain', anchor:'-7', hiddenName:'attach',tbar:[
					{text:'צרף קובץ', iconCls:'mail_attachment',
						menu: {
							items: [
							{text:'קובץ מהסורק',iconCls:'scan', scope:this,	handler:function(){this.ScanWin(this.form.find('name','timestart')[0].getValue());}},
							//{text:'... קובץ מתיק',iconCls:'user', scope:this, handler:function(){var win=new AttUploadFromFilesWin(this.form.find('name','timestart')[0].getValue()); win.show(); win.center();}},
							{text:'קובץ מהמחשב',iconCls:'upload', scope:this, handler:function(){var win=new AttUploadWin(this.form.find('name','timestart')[0].getValue()); win.show(); win.center();}}
							]
						}
					},'-'
			]}
			,{xtype:'container',layout:'fit', baseCls:'x-plain', items:[
				{xtype:'htmleditor', name:'message',height:(MailComposeWindowCfg.height-220),
                    listeners: {
                        activate:function(){
                            if (MailComposeWindowCfg.fontname) {
                                fonts = ['Arial','Courier New','Tahoma','Times New Roman','Verdana'];
                                this.execCmd('fontname',fonts[MailComposeWindowCfg.fontname]);
                                this.execCmd('fontsize',MailComposeWindowCfg.fontsize);
                            }}
                        ,initialize:function(){this.getEditorBody().style.direction='rtl';}
                    }
                }
			]}
			,{xtype:'hidden', name:'account'}
			,{xtype:'hidden', name:'sentFolder'}
			,{xtype:'hidden', name:'timestart', value: Math.round(new Date().getTime()/1000)}
			,{xtype:'hidden', name:'tfutsa_flag', value:0}	/*tfutsa_flag - אם זה מייל תפוצה אז לא צריך לעשות בדיקה כששולחים מייל כדי להוסיפו ל אנשי קשר - כי הם כבר קיימים במערכת*/
		]
	});
	
	MailComposeWindow.superclass.constructor.call(this, {
		title:'שליחת דוא"ל',iconCls:icon, id:'MailComposeWindow',
		layout:'fit',closeAction:'close',minWidth:580, minHeight:260,bodyStyle:'padding:5px;', plain: true,
		resizable:true,	
		width:MailComposeWindowCfg.width,
		height:MailComposeWindowCfg.height,
		items: this.form
	});

	this.setMessageBeforeShow  = function(m){
		template = m;
	};
	
	this.on('close',function(){
		if (!this.sending) Ext.Ajax.request({
			url:'?m=mail_new/mail_f&f=del_attachments',
			params:{timestart:this.form.find('name','timestart')[0].getValue()},
			scope:this});
	},this);
	
	this.setDefaultAccount = function(){
		if(!defaultId){
			alert('.לא נמצא חשבון לשליחה! הדוא"\ל ישלח מטעם המערכת.');
		}else{
			this.form.find('name','account')[0].setValue(defaultId);
			this.form.find('name','sentFolder')[0].setValue(sentFolder);
			this.menu.find('account',defaultId)[0].setIconClass('online');
		}
	};
	
	this.on('beforeshow',function(){
		if(email){
			var to = [];
			if (email.prevMail) for(var i = 0; i < email.prevMail.to.length; i++){
				to.push(email.prevMail.to[i].mailto);
			}
			
			// try format date if not formated
			var sent_d = Date.parseDate(email.prevMail.sent, 'Y-m-d H:i:s');
			sent_d = sent_d ? sent_d.format('d/m/Y H:i') : email.prevMail.sent;
			template = '<br><br><br><hr><blockquote dir=rtl><table cellpadding=0 cellspacing=0 style="text-align:right;font-size:inherit;">'+
				'<tr><th colspan=3>-----מכתב מקורי-----</th></tr>'+
				'<tr>'+
					'<td dir=rtl><b>מ:</b></td>'+
					'<td align="left">'+email.mailData.from[0]+'</td>'+
					'<td align="left">&lt;'+email.mailData.from[1]+'&gt;</td>'+
				'</tr>'+
				'<tr><td dir=rtl><b>נשלח ב:</b></td><td colspan=2 align="left">'+sent_d+'</td></tr>'+
				'<tr><td dir=rtl><b>אל:</b></td><td colspan=2 align="left">'+to.toString()+'</td></tr>'+
				'</table>'+
				'<table cellpadding=0 cellspacing=0 style="text-align:right;font-size:inherit;">'+
					'<tr><td dir=rtl><b>נושא:&nbsp;&nbsp;</b></td><td>'+email.prevMail.subject+'</td></tr>'+
				'</table></blockquote><br><br>';
			template += '<div>'+email.prevMail.html+'</div>';
			this.form.find('name','subject')[0].setValue((email.action=='forward' ?'FW:':'RE:')+email.mailData.subject);
			this.form.find('name','message')[0].setValue(template);
			if(this.menu.find('account',email.account.root_id)[0]){
				this.form.find('name','account')[0].setValue(email.account.root_id);
				this.form.find('name','sentFolder')[0].setValue(email.account.sentFolder);
				this.menu.find('account',email.account.root_id)[0].setIconClass('online');
			}else{
				this.setDefaultAccount();
			}
		} else {// Set default account
			this.setDefaultAccount();
		}
		
		this.form.find('name','message')[0].focus(true);
		
		Ext.Ajax.request({
			url:'?m=common/common_func&f=signature&a=load',
			success: function(r){
				r=Ext.decode(r.responseText);
				if(r.success){
					if (template) this.form.find('name','message')[0].setValue('<br/><br/><br/><br/><br/>'+r.data.signature+''+template);
					else this.form.find('name','message')[0].setValue('<br/><br/><br/><br/><br/>'+r.data.signature);
				}
			},
			scope: this
		});
	},this);
	
	this.on('resize', function(w,width,height){
		if (MailComposeWindowCfg.width!=width || MailComposeWindowCfg.height!=height) {
			this.form.find('name','emails')[0].setWidth(width-170);
			this.form.find('name','cc')[0].setWidth(width-170);
			this.form.find('name','bcc')[0].setWidth(width-170);
			this.form.find('name','subject')[0].setWidth(width-275);
			this.form.find('name','message')[0].setHeight(height-212);
			MailComposeWindowCfg.width=width;
			MailComposeWindowCfg.height=height;
			setObjView({a:'mail_cfg', module:'mail', object:'MailComposeWindow', cfg:'{width:'+width+', height:'+height+'}'});
		}
	},this);
	
	if (email && email.action == 'forward') {
		this.form.find('name','send_btn')[0].disable();
		this.form.find('hiddenName','attach')[0].getTopToolbar().add({text:'<img src="skin/icons/ajax-loader.gif" align=right>טוען קבצים...</img>',xtype:'tbtext',name:'attachLoad'});
		this.getAttachments({account:email.mailData.account,folder:email.mailData.folder, uid:email.mailData.uid});
	} 
		
	this.submitForm = function() {
		if(!this.form.form.isValid()) return;
		var account = 0, uid = 0, folder = '', mailto=this.form.find('name','emails')[0].getValue();
		if(email){// Is reply
			account = email.account.root_id;
			folder = email.mailData.folder;
			uid = email.mailData.uid;
		}
		this.sending = true;
		Ext.Ajax.request({
			url: "?m=mail_new/mail_f&f=send_mail",
			params:Ext.apply(this.form.form.getValues(), {msg_account:account, folder:folder, uids:uid, mailto:mailto, mailcc:this.form.find('name','cc')[0].getValue(), mailbcc:this.form.find('name','bcc')[0].getValue()}),
			timeout:180000,
			success: function(r,o){
				r=Ext.decode(r.responseText);
				if (r.success){
					Crm.mailAlerts.msg('הודעת מערכת','<span dir=rtl>הדוא"ל נשלח בהצלחה ל<b><u>'+mailto+'</u></b></span> &nbsp; <img src="skin/icons/bmail/mail_new.gif" align="absmiddle">');
					if(altConfig && typeof(altConfig.callback) == 'function')
						altConfig.callback(o.params);
				}else Crm.mailAlerts.msg('הודעת מערכת','<img src="skin/icons/bmail/icon-error.gif" align=left><br>שליחת דוא"ל אל נכשלה<br>'+r.msg+'</img>');
			},
			failure: function(){
				Ext.MessageBox.show({width:400, title:'Error', icon:Ext.MessageBox.ERROR, msg:o.result.msg, buttons:Ext.MessageBox.OK,
				fn:function(o){
					Ext.MessageBox.show({
						width:220, title:'הודעת מערכת', msg:'בדוק הגדרות שרת דואר יוצא.', icon:Ext.MessageBox.ERROR,buttons:Ext.MessageBox.OK, scope:this});
					}, scope:this});
			},
			scope: this
		});
		this.close();
	};
};
Ext.extend(MailComposeWindow, Ext.Window);