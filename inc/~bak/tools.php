<?

#################################################################################################################
function validateEmail($email) {return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);}
function validateURL($url) {return eregi("^((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $url);}

function validateHTTPURL($url) {return eregi("^((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $url);}

function convertURLS($text) {return eregi_replace("((ht|f)tp://www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)", "http://www.\\3", $text);}
function convertMail($text)	{return eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a href='mailto:\\0'>\\0</a>", $text);}
function convertAllLinks($text){$text = convertURLS($text); $text = convertMail($text); return $text; }


#############################################################################################
# generate random id
function rand_id($length) {
	$rand_id=''; $chars='0123456789abcdefghijklmnopqrstuvwxyz'; $chars_len=strlen($chars)-1;
	for($a=0;$a<$length;$a++) $rand_id=$rand_id.substr($chars, rand(0,$chars_len), 1);
  return $rand_id;
}

#################################################################################################################
function send_mail($to, $subject, $message, $from='') {
  global $CFG;
	if (!$from AND $CFG['mail_from']) $from=$CFG['mail_from'];
	else {	# set default from adress to 'contact@thisdomain'
		eregi("(www\.)?([a-z0-9\.-]+)", $_SERVER['HTTP_HOST'], $host_ar);	# get current domain name without www
		$from="contact@$host_ar[2]";
	}

	if ( !$CFG['smtp_host'] AND ($_SERVER['HTTP_HOST']=='localhost' OR $GLOBALS['CFG']['localhost']) ) {
		$message="To: $to\nFrom: $from\nSubject: $subject\n==========================\n\n$message\n\n\n";
		return write2file('emails.txt', $message, 'ab');
	}else {
		return smtp_mail($to, $subject, $message, $from);
	}
}


#################################################################################################################
function smtp_mail($to, $subject, $message, $from) {
 global $CFG;
	if (!$CFG['smtp_host']) { error("smtp_host not set"); return; }
	include_once 'Mail.php';
	include_once 'Mail/mime.php';

	$recipients = $to;
	$body				= $message;
	$headers['From']    = $from;
	$headers['Subject'] = $subject;

	# Set charset to Hebrew (Windows)
	$param["head_charset"] = $param["text_charset"] = $param["html_charset"] = "windows-1255";

	# Set host parameters
	if ($CFG['smtp_host']) $params['host']		= $CFG['smtp_host'];
	if ($CFG['smtp_port']) $params['port']		= $CFG['smtp_port'];
	if ($CFG['smtp_user']) $params['username']	= $CFG['smtp_user'];
	if ($CFG['smtp_pass']) $params['password']	= $CFG['smtp_pass'];

	$mime = new Mail_mime();
	$mime->setTXTBody(html2text($message));
	$mime->setHTMLBody($message);

	$body = $mime->get($param);
	$headers = $mime->headers($headers);

	$mailer = &Mail::factory('smtp', $params);
	$mailer_error = $mailer->send($recipients, $headers, $body);

	if (PEAR::isError($mailer_error)) {
		$errorMessage=$mailer_error->getMessage();
		//error($errorMessage);
		return "$mailer_error: $errorMessage";
	}else return;
}


#################################################################################################################
function text_mail($to, $subject, $message, $from='') {
  global $CFG;
	if (!$from) $from=$GLOBALS['CFG']['mail_from'];

	if ( !$CFG['smtp_host'] AND ($_SERVER['HTTP_HOST']=='localhost' OR $GLOBALS['CFG']['localhost']) ) {
		$message="To: $to\nFrom: $from\nSubject: $subject\n==========================\n\n$message\n\n\n";
		return write2file('emails.txt', $message, 'ab');
	}else {
		if ($CFG['smtp_host']) return smtp_mail($to, $subject, $message, $from);
		else return mail($to, $subject, $message, "From: $from");
	}
}


#################################################################################################################
function html_mail($to, $subject, $html, $from='') {
	if (!$from) {
		if ($GLOBALS['CFG']['mail_from']) $from=$GLOBALS['CFG']['mail_from'];		# set default From
		else {	# set default from adress to 'contact@thisdomain'
			eregi("(www\.)?([a-z0-9\.-]+)", $_SERVER['HTTP_HOST'], $host_ar);	# get current domain name without www
			$from="contact@$host_ar[2]";
		}
	}

	$charset="windows-1255";	# set default charset

	#prepere MIME Headers
	$headers="MIME-Version: 1.0
From: $from
Content-Type: multipart/alternative;
	boundary=\"----=_NextPart_000_0000_01C396A9.C6F9B980\"
X-Priority: 3 (Normal)
X-MSMail-Priority: Normal
X-Mailer: Microsoft Outlook IMO, Build 9.0.2416 (9.0.2910.0)
Importance: Normal
X-MimeOLE: Produced By Microsoft MimeOLE V5.00.2615.200";
//Reply-To: $from
//Content-type: text/html; charset=windows-1255
//Content-Transfer-Encoding: 7bit


	#prepere MIME Body
	$msg_body='<HTML><HEAD><META http-equiv=Content-Type content="text/html; charset='.$charset.'"></HEAD><BODY>';
	$msg_body.=$html;
	$msg_body.='</BODY></HTML>';

	$msg_body='
This is a multi-part message in MIME format.

------=_NextPart_000_0000_01C396A9.C6F9B980
Content-Type: text/plain;
	charset="'.$charset.'"
Content-Transfer-Encoding: 8bit

'.html2text($msg_body).'

------=_NextPart_000_0000_01C396A9.C6F9B980
Content-Type: text/html;
	charset="'.$charset.'"
Content-Transfer-Encoding: quoted-printable

'.quotedPrintableEncode($msg_body).'

------=_NextPart_000_0000_01C396A9.C6F9B980--

';
	//echo "<pre>$msg_body</pre>";

	if (eregi("localhost", $_SERVER['HTTP_HOST']) OR $GLOBALS['CFG']['localhost']) {
		return write2file('email.txt', "To: $to\n\n$headers\n\n$subject\n\n$msg_body", 'ab');
	}else {
		return mail($to, $subject, $msg_body, $headers);
	}
}


#################################################################################################################
# quoteadPrintableEncode() - Encodes data to quoted-printable standard.
# $input	- The data to encode
# $line_max	- Optional max line length. Should not be more than 76 chars
function quotedPrintableEncode($input , $line_max = 76) {
	$lines  = preg_split("/\r?\n/", $input);
	$eol    = "\r\n";	//MAIL_MIMEPART_CRLF;
	$escape = '=';
	$output = '';
	while(list(, $line) = each($lines)) {
		$linlen  = strlen($line);
		$newline = '';
		for ($i = 0; $i < $linlen; $i++) {
			$char = substr($line, $i, 1);
			$dec  = ord($char);
			if (($dec == 32) AND ($i == ($linlen - 1))) $char = '=20';   // convert space at eol only
			elseif ($dec == 9) ;									 // Do nothing if a tab.
			elseif ( ($dec == 61) OR ($dec < 32 ) OR ($dec > 126) ) $char=$escape.strtoupper(sprintf('%02s', dechex($dec)));

			if ((strlen($newline) + strlen($char)) >= $line_max) {        // MAIL_MIMEPART_CRLF is not counted
				$output  .= $newline . $escape . $eol;                    // soft line break; " =\r\n" is okay
				$newline  = '';
			}
			$newline .= $char;
		}
		$output .= $newline . $eol;
	}
	$output = substr($output, 0, -1 * strlen($eol)); // Don't want last crlf
 return $output;
}


##########################################################################################
# Encrypt/Decrypt string with RC4 protocol
function rc4encrypt($data, $pwd) {
	$key[]=$box[]='';
	$pwd_length = strlen($pwd);
	$data_length = strlen($data);
	for ($i = 0; $i < 256; $i++) {
		$key[$i] = ord($pwd[$i % $pwd_length]);
		$box[$i] = $i;
	}
	for ($j = $i = 0; $i < 256; $i++) {
		$j = ($j + $box[$i] + $key[$i]) % 256;
		$box[$i] ^= $box[$j];
		$box[$j] ^= $box[$i];
		$box[$i] ^= $box[$j];
	}
	for ($a = $j = $i = 0; $i < $data_length; $i++) {
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$box[$a] ^= $box[$j];
		$box[$j] ^= $box[$a];
		$box[$a] ^= $box[$j];
		$k = $box[(($box[$a] + $box[$j]) % 256)];
		$cipher .= chr(ord($data[$i]) ^ $k);
	}
 return $cipher;
}
function rc4decrypt($data, $pwd) {
	return rc4encrypt($data, $pwd);
}


######################################################################################
# Return page source with header
function getUrl($url, $timeout=10) {
	if (!$url) return;
	$parsed_url = parse_url($url);
	$host=$parsed_url["host"];
	$path=$parsed_url["path"];	if(!$path || $path=='' || !isset($path)) $path = "/";
	$query = $parsed_url["query"];	if($query!='') $path = "$path?$query";
	if(!isset($parsed_url["port"])) $port=80;	else $port=$parsed_url["port"];

	$REQUEST="GET $path HTTP/1.1\r\n"
	."Host: $host\r\n"
	."User-Agent: Mozilla/4.0 (compatible; MSIE 5; Windows 98)\r\n"
	//."User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\r\n"
	."Connection: close\r\n\r\n";
	//echo "\$REQUEST=$REQUEST<br>";

	$fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
	if (!$fp) { echo "fsockopen Error: $errstr - $errno<br>"; return; }

	fputs($fp,$REQUEST);		// complete the request
	$timeout=time()+$timeout;	// Time+time_out out in seconds
	$output='';
	while (!feof($fp) && time()<$timeout) $output.=fread($fp,2048);
	fclose ($fp);

 return $output;
}



######################################################################################
# Parse html table data to array
function mine_table_data($data) {
	preg_match_all("/<TR[^>]*>(.*?)<\\/TR>/si", $data, $tbl_rows);
	for ($y=0; $y<count($tbl_rows[1]); $y++) {
		preg_match_all("/<T[D,H][^>]*>(.*?)<\\/T[D,H]>/si", $tbl_rows[1][$y], $tbl_row_data);
		for ($x=0; $x<count($tbl_row_data[1]); $x++) {
			$mined_data[$y][$x]=$tbl_row_data[1][$x];
		}
	}
 return $mined_data;
}

######################################################################################
function xml2array($data) {
	$p = xml_parser_create();
	xml_parser_set_option($p, XML_OPTION_SKIP_WHITE, 1);
	xml_parse_into_struct($p, $data, &$vals, &$index);
	xml_parser_free($p);

	$tree =	array();
	$i = 0;
	//array_push($tree,	array('tag'	=> $vals[$i]['tag'], 'attributes'=>	$vals[$i]['attributes'],							'children' => ));
	$tree =	GetChildren($vals, $i);
	return $tree;
}
function GetChildren($vals,	&$i) {
	$children =	array();
	if ($vals[$i]['value'])	array_push($children, $vals[$i]['value']);
	$prevtag = "";
	while (++$i	< count($vals))	{	// so pra nao botar	while true ;-)
	  switch ($vals[$i]['type']) {
		case 'cdata':
			array_push($children, $vals[$i]['value']);
			break;
		case 'complete':
			$children[ strtolower($vals[$i]['tag']) ] = $vals[$i]['value'];
			break;
		case 'open':
		   //restartindex on unique	tag-name
		   $j++;
		   if ($prevtag	<> $vals[$i]['tag']) {
				$j=0;
				$prevtag=$vals[$i]['tag'];
			}
			$children[ strtolower($vals[$i]['tag']) ][$j] = GetChildren($vals,$i);
			break;
		case 'close':
			return $children;
		}
	}
}

######################################################################################


?>