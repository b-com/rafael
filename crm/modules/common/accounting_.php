﻿<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

// header("Content-Type: text/html;charset=windows-1255");

/*
קודי מיון:
100 - קופה
200 - לקוחות
300 - בנק
400 - ספקים
500 - מיסיס
550 - מע"מ
5502- מע"מ עסקעות
5503- מע"מ תשומות
5504- מע"מ תשומות רכוש
5505- ניקוי מס במקור
700 - הוצאות
600 - עובדיםFf-DEP
650 - תשלומים
800 - הכנסות
8001- הכנסות חייבות מע"מ
8002- הנחות חייבות מע"מ
8003- הכנסות פתורות מע"מ
8004- הנחות פתורות מע"מ
*/

$NO_SKIN=1;

$VAT = 16;

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else die('{success:false, msg:"function name error"}');
//————————————————————————————————————————————————————————————————————————————————————
function f_add_cash_payment() {
 global $SVARS;
	$clientID = (int)$_POST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	
	foreach($_REQUEST as $k=>$v) $_REQUEST[$k]=iconv("UTF-8", "windows-1255", $v);
	$docNum=sql2array("SELECT payments FROM acc_counters",'','payments',1);
	$docNum++;

	$sql="INSERT INTO acc_docs SET num=$docNum, client_id=$clientID"
		." , date='".str2time($_REQUEST['date'],'Y-m-d')
		."', type='cash_payment"
		."', sum='".round($_REQUEST['sum'],2)
		."', notes='".sql_escape($_REQUEST['notes'])
		."', user_id=".$SVARS['user']['id'];
		
	if (runsql($sql)) {
		$docID = mysql_insert_id();
		runsql("INSERT INTO acc_operations (date,doc_id,credit_class,credit_card,debt_class,debt_card,sum,user_id) VALUES ('".str2time($_REQUEST['date'],'Y-m-d')."',$docID,100,1001,200,$clientID,'".round($_REQUEST['sum'],2)."',{$SVARS['user']['id']})");
		runsql("UPDATE acc_counters SET payments=payments+1");
		if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET $type=1");

		echo "{success:true, id:$docID, num:$docNum}";
	} else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";	
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_order() {
 global $SVARS;
	$clientID = (int)$_POST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	$cardID = (int)$_POST['id'];
	$cardNum = (int)$_POST['card_num'];
	$rdata = json_decode($_POST['rdata']);
	// if (!$rdata) die('{success:false, msg:"No rows data!"}');

	
	foreach($_REQUEST as $k=>$v) $_REQUEST[$k]=iconv("UTF-8", "windows-1255", $v);
	if (!$cardID) {
		$cardNum=sql2array("SELECT orders FROM acc_counters",'','orders',1);
		$cardNum++;
	}
	
	$sql=($cardID ? 'UPDATE acc_orders SET':"INSERT INTO acc_orders SET num=$cardNum, client_id=$clientID, ")
		."   name='".sql_escape($_REQUEST['name'])
		."', date='".str2time($_REQUEST['date'],'Y-m-d H:i')
		."', start='".str2time($_REQUEST['start'],'Y-m-d H:i')
		."', end='".str2time($_REQUEST['end'],'Y-m-d H:i')
		."', status='".sql_escape($_REQUEST['status'])
		."', sms='".sql_escape($_REQUEST['sms'])
		."', importance='".(int)$_REQUEST['importance']
		."', deleted='0"
		."', sum=".quote($_REQUEST['sum'])
		." , notes='".sql_escape($_REQUEST['notes'])
		."', user_id='".$SVARS['user']['id']
		."'".($cardID ? " WHERE id=$cardID":'');
		
	// echo $sql;
	$success=false;
	if (runsql($sql)) {
		if (!$cardID) {
			$cardID = mysql_insert_id();
			runsql("UPDATE acc_counters SET orders=orders+1");
			if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET orders=1");
		}
		runsql("DELETE FROM acc_order_rows WHERE card_id=$cardID");
		if ($rdata) {
			foreach($rdata as $row) if ($row->name) 
				$Rsql.=($Rsql?',':'')."($cardID,'".sql_escape($row->type)."','".sql_escape(iconv("UTF-8", "windows-1255",$row->code))."','".sql_escape(iconv("UTF-8", "windows-1255",$row->name))."','".sql_escape($row->unit)."','".round($row->amount,2)."','".round($row->price,2)."','".round($row->sum,2)."','".(int)$row->VAT."','".round($row->sum_tax,2)."')";
			$Rsql="INSERT INTO acc_order_rows (card_id,type,code,name,unit,amount,price,sum,VAT,sum_tax) VALUES ".$Rsql;
			if (runsql($Rsql)) $success=true; else{
				runsql("DELETE FROM acc_orders WHERE id=$cardID");
				runsql("UPDATE acc_counters SET orders=orders-1");
			}
		} $success=true;
	}
	
	if ($success) {
		// if ($_REQUEST['status']==2 AND $_REQUEST['sms'] AND $SVARS['sms_active'] AND $tel=ereg_replace('[^0-9]+','',$_REQUEST['sms']) AND ereg('^(0|972)(5[024][0-9]{7})$', $tel, $regs)) {
			// include_once 'inc/sms.php';
			// $send_sms = send_sms('972'.$regs[2], 'הטיפול ברכ הסתיים', $SVARS['sms_originator']);
		// }
		echo "{success:true, id:$cardID, cardNum:".(int)$cardNum."}"; 
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_order_rows() {
	$card_id = (int)$_REQUEST['order_id'];
	if (!$card_id) die('{success:false, msg:"card_id Error"}');
	$rows = sql2array("SELECT * FROM acc_order_rows WHERE card_id=$card_id ORDER BY id");
	$rowsCount = count($rows);
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_accept() {
 global $SVARS, $group_id, $VAT;
	$clientID = (int)$_POST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	$idata = json_decode($_POST['idata']);
	$adata = json_decode($_POST['adata']);
	$doc_type='accept';
	$income_group = $_REQUEST['income_group'];
	$vat_rate =sql2array("SELECT t1.vat_rate as vat_rate FROM VAT_Rates t1 , VAT_Rates t2, acc_docs
				     where t1.from_date<(select acc_docs.date as date from acc_docs order by id desc limit 1) 
					 and (select acc_docs.date as date from acc_docs order by id desc limit 1)<t2.from_date 
				     and t1.id = t2.id-1 limit 1",0,'vat_rate',1) ;
	// $orders_ids = $_REQUEST['orders_ids'];

	//echo "/*<div dir=ltr align=left><pre>". print_r($Vatsql,1)."</pre></div>*/"; die('{success:false, msg:"No rows data!"}');
	
	if (!$idata) die('{success:false, msg:"No rows data!"}');
	
	// foreach($_REQUEST as $k=>$v) $_REQUEST[$k]=iconv("UTF-8", "windows-1255", $v);
	$types=array('part'=>1,'material'=>2,'service'=>3,'fserv'=>4,'other'=>5);
	
	if ((int)$_REQUEST['tax_clear']) $paid = round(100*round($_REQUEST['asum'],2)/(100-(int)$_REQUEST['tax_clear']),2);
	else $paid = round($_REQUEST['asum'],2);
	
	/*
	if ($orders_ids) {
		$result = sql2array("SELECT id, sum, paid FROM acc_orders WHERE id IN ($orders_ids)");
		if ($result) foreach($result as $k=>$v){
			$diver = $v['sum'] - $v['paid'];
			if ($paid>0){
				$updateOrders[$v['id']] = min(round($paid,2),$diver);
				$paid -= min(round($paid,2),$diver);
			} else $updateOrders[$v['id']]=0;
		}
	}
	*/
	$num=sql2array("SELECT $doc_type FROM acc_counters",'',$doc_type,1); $num++;
	
	
	
	echo"/**vat-Rate: $vat_rate**/";
	echo"/**$num*/";
	$sql="INSERT INTO acc_docs SET  num=$num, client_id=$clientID, income_group = $income_group"
		." , parent_doc_id=".(int)($_REQUEST['parent_doc_id'])
		." , date='".str2time($_REQUEST['date'],'Y-m-d')
		."', type='".$doc_type
		."', sum='".round($_REQUEST['asum'],2)
		."', VAT=$vat_rate"
		." , vat_sum=".quote(round($_REQUEST['vat_sum'],2))
		." , tax_clear='".(int)$_REQUEST['tax_clear']
		// ."', over_order='".round($paid,2)
		."', notes='".sql_escape($_REQUEST['notes'])
		
		."', user_id=".$SVARS['user']['id'];	
	
	// echo $sql;
	if (runsql($sql)) {
		$docID = mysql_insert_id();
		/*
		if ($updateOrders) foreach($updateOrders as $id=>$paid) {
			#Update orders payment
			runsql("UPDATE acc_orders SET invoice=$docID, paid=paid+$paid, notes=CONCAT(notes,' הופקה חשבונית מס ".($_REQUEST['type']=='invoice_accept'?' קבלה':'')." $num') WHERE id =$id LIMIT 1");
			#Generate SQL for insert to matching table
			$matching_sql.=($matching_sql?',':'')."($id,'order-$_REQUEST[type]',$docID)";
		}
		
		#Insett matchings into DB
		if ($matching_sql) runsql("INSERT INTO acc_matching (doc1,type,doc2) VALUES $matching_sql");
		*/
		if ($adata) {
			foreach($adata as $row) {
				$ptype=$row->ptype;
				$bank =$row->bank;
				if ($ptype=='מזומן') {$debt_class=100; $debt_card=1001; if (!$row->date) $row->date=date('d/m/Y');}
				elseif ($ptype=='שיק') {$debt_class=100; $debt_card=1002;}
				elseif ($ptype=='אשראי' AND $bank=='וויזה') {$debt_class=100; $debt_card=1004;}
				elseif ($ptype=='אשראי' AND $bank=='ישראכרט/מאסטרכארד') {$debt_class=100; $debt_card=1005;}
				elseif ($ptype=='אשראי' AND $bank=='דיינרס') {$debt_class=100; $debt_card=1006;}
				elseif ($ptype=='אשראי' AND $bank=='אמריקן אקספרס') {$debt_class=100; $debt_card=1007;}
				elseif ($ptype=='אשראי' AND $bank=='לאומי קארד') {$debt_class=100; $debt_card=1008;}
				elseif ($ptype=='ה.קבע/ה.בנקאית') {$debt_class=300; $debt_card=$row->account;}
				else {
					runsql("DELETE FROM acc_docs WHERE id=$docID");
					die ("{success:false, msg:'אופן תשלום לא תקין.'}");
				}

				#document rows
				if ($ptype=='אשראי') {
					
					$s =round($row->sum/$row->payments_num);
					$s1=round($row->sum-$s*($row->payments_num-1),2);
					
					for ($i=1; $i<=(int)$row->payments_num; $i++) {
						if (str2time($row->date,'d')<16) $payment_day=str2time($row->date,'Y-m-').'02'; else $payment_day=str2time($row->date,'Y-m-').'08';
						$payment_date=date('Y-m-d', strtotime("$payment_day +$i month"));

						$Rsql.=($Rsql?',':'')."($debt_card,$docID,'$payment_date','$ptype','".($i==1 ? $s1:$s)."','".sql_escape($row->payments_num)."','".sql_escape($bank)."','$i','','".sql_escape($row->number)."')";//,1
					}
				
				}
				else $Rsql.=($Rsql?',':'')."($debt_card,$docID,'".str2time($row->date,'Y-m-d')."','$ptype','".round($row->sum,2)."',1,'".sql_escape($bank)."','".sql_escape($row->dept)."','".sql_escape($row->account)."','".sql_escape($row->number)."')";
				
				#document operations
				$csql.=($csql?',':'')."('".str2time($_REQUEST['date'],'Y-m-d')."',$docID,200,$clientID,$debt_class,$debt_card,'".sql_escape($row->sum)."',{$SVARS['user']['id']})";
			}
			$Rsql="INSERT INTO acc_accept_rows (debt_card,accept_id,date,ptype,sum,payments_num,bank,dept,account,number) VALUES ".$Rsql;
			if ((int)$_REQUEST['tax_clear']) $csql.=($csql?',':'')."('".str2time($_REQUEST['date'],'Y-m-d')."',$docID,200,$clientID,550,5505,'".round((int)$_REQUEST['tax_clear']*round($_REQUEST['asum'],2)/(100-(int)$_REQUEST['tax_clear']),2)."',{$SVARS['user']['id']})";
		}
		
		if ($idata) {
			foreach($idata as $row) {
				$type=$row->type;
				$tax=($type=='round' ? 0: ($row->sum_tax-$row->sum));
				$taxTotal+=$tax;
				if ($row->name) {
					$invoice_rows_sql.=($invoice_rows_sql?',':'')."($docID,'$type','".sql_escape($row->code)."','".sql_escape($row->name)."','".sql_escape($row->unit)."','".round($row->amount,2)."','".round($row->price,2)."','".round($row->sum,2)."','".round($row->VAT,2)."','".round($row->sum_tax,2)."')";
					/*
					if ($_REQUEST['type']=='invoice_debt') {
						if ($type=='discount') 	{$credit_class=800; $credit_card=($tax?8002:8004); $debt_class=200; $debt_card=$clientID; $sum=-round($row->sum,2);}
						elseif ($type=='round') {$credit_class=200; $credit_card=$clientID;	$debt_class=900; $debt_card=9001; $sum=round($row->sum_tax,2);}
						else 					{$credit_class=200; $credit_card=$clientID;	$debt_class=800; $debt_card=($tax?8001:8003); $sum=round($row->sum,2);}
					} else {
						if ($type=='discount') 	{$credit_class=200; $credit_card=$clientID; $debt_class=800; $debt_card=($tax?8002:8004); $sum=-round($row->sum,2);}
						elseif ($type=='round') {$credit_class=900; $credit_card=9001;	$debt_class=200; $debt_card=$clientID; $sum=round($row->sum_tax,2);}
						else					{$credit_class=800; $credit_card=($tax?8001:8003);	$debt_class=200; $debt_card=$clientID; $sum=round($row->sum,2);}
					}
					$csql.=($csql?',':'')."('".str2time($_REQUEST['date'],'Y-m-d')."',$docID,$credit_class,$credit_card,$debt_class,$debt_card,'$sum',$SVARS[user_id])";
					*/
				}
			}
			/*
			if ($taxTotal) {
				if ($_REQUEST['type']=='invoice_debt') $csql.=($csql?',':'')."(,'".str2time($_REQUEST['date'],'Y-m-d')."',$docID,200,$clientID,550,5502,'$taxTotal',$SVARS[user_id])";
				else $csql.=($csql?',':'')."('".str2time($_REQUEST['date'],'Y-m-d')."',$docID,550,5502,200,$clientID,'$taxTotal',$SVARS[user_id])";
			}
			*/
		}
		

		if (!$Rsql OR runsql($Rsql)) {
			#Insert Invoice rows into DB
			if ($invoice_rows_sql) runsql("INSERT INTO acc_invoice_rows (invoice_id,type,code,name,unit,amount,price,sum,VAT,sum_tax) VALUES ".$invoice_rows_sql);
			
			#Insert accountig operations into DB
			if ($csql) runsql("INSERT INTO acc_operations (date,doc_id,credit_class,credit_card,debt_class,debt_card,sum,user_id) VALUES ".$csql);
			
			#Update document counter
			runsql("UPDATE acc_counters SET $doc_type=$doc_type+1");
			if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET $doc_type=1");
			/*
			# The Invoice match to payments rows
			if ($_REQUEST['payments_ids']) {
				#Update payments rows set invioce match
				runsql("UPDATE acc_accept_rows SET invoice=1 WHERE id IN ($_REQUEST[payments_ids])");
				#Write matching rows into DB
				foreach(explode(',',$_REQUEST['payments_ids']) as $payment_id) $matching_sql.=($matching_sql?',':'')."($docID,'invoice-accept',$payment_id)";
				if ($matching_sql) runsql("INSERT INTO acc_matching (doc1,type,doc2) VALUES $matching_sql");
			}
			*/
			echo "{success:true, id:$docID, num:$num}";
		}else{
			runsql("DELETE FROM acc_docs WHERE id=$docID");
			echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
		}
	} else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";	
}
//————————————————————————————————————————————————————————————————————————————————————
function f_send_invoices_list() {
 global $SVARS, $docTypes;

	$sql = "SELECT SQL_CALC_FOUND_ROWS acc_docs.* "
		// .", TRIM( CONCAT(lname,' ',fname)) AS client_name"
		.", clients.send_doc_type, clients.name AS client_name, email, fax "
		."FROM acc_docs "
		."LEFT JOIN clients ON acc_docs.client_id = clients.id "
		."WHERE printed=0 AND type='invoice_accept'"
		."ORDER BY date ASC "
		;
	
	// echo $where;
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	if ($rows) foreach ($rows as $k=>$r) {
		$row=&$rows[$k];
		if ($row['send_doc_type']=='פקס') $row['send_doc_to']=$row['fax'];
		elseif ($row['send_doc_type']=='דוא"ל') $row['send_doc_to']=$row['email'];
		
		$row['netto'] = round($row['sum']-$row['vat_sum'],2);
		$row['type'] = $docTypes[$row['type']];
	}

	echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_send_invoices() {
 global $SVARS, $docTypes;
	ini_set('memory_limit','1024M');
	set_time_limit(180);
 	$data = json_decode($_POST['data']);
	if (!$data) die('{success:false, msg:"No rows data!"}');
	$f='add_contact';
	$SVARS['is_admin']=1;
	include_once "modules/common/print_doc.php";
	
	foreach($data as $invoice) {
		if ($invoice->send_doc_type=='פקס') {
			include_once "../inc/fax.php";

			include_once "../inc/wkhtmltopdf.php";		
			// wkhtmltopdf(orderBAFI($id), 'content');
			$filepath="../../temp/r$_POST[attach_id]"."c$_POST[link_id].pdf";
			
			$pdf = wkhtmltopdf(printDoc($invoice->id,'invoice_accept',1,0,false), 'save',$filepath,$footer_params);			

			# add to db
			$sql="INSERT INTO fax_out SET status=1"
				.", user_id={$SVARS['user']['id']}"
				// .", jobid=$jobid"
				.", date='".date('Y-m-d H:i:s')."'"
				.", recipient_fax=".quote($recipient_fax)
				.", recipient_name=".quote($_POST['recipient_name'])
				.", sender_name=".quote($_POST['sender_name'])
				.", sender_fax=".quote($_POST['sender_fax'])
				.", sender_tel=".quote($_POST['sender_tel'])
				.", subject=".quote($_POST['subject'])
				.", text=".quote($_POST['text'])
				.", logo=".(int)$_POST['logo']
				.", link_to='clients'"				# enum('crm_faxes','fax_out','clients')
				.", link_id=".$invoice->client_id	# client_id / fax id 
				// .($_POST['link'] ? ", link=".quote($_POST['link']) :'')				# claim_id
				// .($_POST['attach_id'] ? ", attach_id=$_POST[attach_id]" :'')		# file_id
				// .($filepath ? ", file=".quote($filepath) :'')
				;
			runsql($sql);
			$fax_id=mysql_insert_id();
			if (!$fax_id) die('{success:false, msg:"Error.."}');
			
			# send to server
			// $send_fax='OK#1';
			$send_fax=send_fax($fax_id, $invoice->send_doc_to, $pdf, $coverpage);
			if (ereg("OK#([0-9]+)", $send_fax, $regs)) {
				runsql("UPDATE fax_out SET jobid=$regs[1] WHERE id=$fax_id");
				runsql("UPDATE clients SET send_doc_type='פקס', fax='".$invoice->send_doc_to."' WHERE id=".$invoice->client_id);
				runsql("UPDATE acc_docs SET printed=1 WHERE id=".$invoice->id);
			}else { 
				$errors.=($errors?'<br>':'')."שגיאה בשליחת חשבונית מס קבלה מס' ".$invoice->num.($SVARS['is_admin'] ? ':=>'.$send_fax :'');
			}
			
			if (is_file($pdf)) unlink($pdf);


			
		} elseif ($invoice->send_doc_type=='דוא"ל') {
			include_once "../inc/mail/sendmail.php";
			
			$CFG['debug']=0;
			
			$from=sql2array("SELECT id, CONCAT( fname, ' ', lname ) AS fullname, email, signature FROM users WHERE id={$SVARS['user']['id']}",'user_id');
			if ($from[$SVARS['user']['id']]['email']) $replyto=$from[$SVARS['user']['id']]['email'];
			else $replyto='support@bafi.co.il';

			include_once "../inc/wkhtmltopdf.php";		
						
			$footer_params='';

			$file_data = array(wkhtmltopdf(printDoc($invoice->id,'invoice_accept',1,0,false), 'content','doc.pdf',$footer_params), 'Invoice.pdf');
			if (send_mail($invoice->send_doc_to, "חשבונית מס קבלה מס' 2", "חשבונית מס קבלה מס' 2", array('from'=>$from[$SVARS['user_id']]['fullname']." ($replyto) <support@bafi.co.il>", 'replyto'=>"$from[fullname] <$replyto>", 'files'=>$attach_files, 'file_data'=>$file_data)) ) {
				$subject="נשלחה חשבונית מס קבלה מס' 1 (אל: ".$invoice->send_doc_to.")";
				runsql("INSERT INTO contacts SET client_id=".$invoice->client_id.", client_type=".quote($_POST['client_type']).", user_id={$SVARS['user']['id']}, type='mail', date=".date('Y-m-d').", subject=".quote($subject).", notes=".quote($_POST['notes']));
				runsql("UPDATE clients SET send_doc_type='דוא\"ל', email='".$invoice->send_doc_to."' WHERE id=".$invoice->client_id);
				runsql("UPDATE acc_docs SET printed=1 WHERE id=".$invoice->id);
			} else 	$errors.=($errors?'<br>':'')."שגיאה בשליחת חשבונית מס קבלה מס' ";

		} elseif ($invoice->send_doc_type=='הדפסה') {
			runsql("UPDATE clients SET send_doc_type='הדפסה' WHERE id=".$invoice->client_id);
			runsql("UPDATE acc_docs SET printed=1 WHERE id=".$invoice->id);
		}
	}
	if ($errors) echo "{success:false, msg:'".jsEscape($errors)."'}";
	else echo "{success:true}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_expense() {
 global $SVARS, $VAT;
	$clientID = (int)$_POST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	$rowsdata = json_decode($_POST['rowsdata']);
	if (!$rowsdata) die('{success:false, msg:"No rows data!"}');
	$date = str2time($_REQUEST['sup_doc_date'],'Y-m-d');
	$num=sql2array("SELECT expenses FROM acc_counters",'',"expenses",1);
	$num++;
	
	$taxTotal=0;
	foreach($rowsdata as $row) {
		$taxTotal+=round((round($row->sum,2)/(100+$VAT)*$VAT)*round($row->VAT,2)/100,2);
		$sumTotal+=round($row->sum,2);
	}
	$sql="INSERT INTO acc_docs SET num=$num, client_id=$clientID"
		." , date='".$date
		."', type='expense"
		."', sum='".$sumTotal
		."', VAT=$VAT"
		." , vat_sum=".quote(round($taxTotal,2))
		." , notes=".quote($_REQUEST['notes'])
		." , user_id=".$SVARS['user']['id'];	
	
	if (runsql($sql)) {
		$docID = mysql_insert_id();
		
		$rep_period=str2time(max($date,$SVARS['company']['current_vat_period']),'ym');
		
		runsql("INSERT INTO acc_expense_ext SET doc_id=$docID, sup_num=".quote($_REQUEST['sup_num']).",sup_doc_date='".str2time($_REQUEST['sup_doc_date'],'Y-m-d')."', sup_doc_num=".quote($_REQUEST['sup_doc_num']).", expense_type=".(int)$_REQUEST['expense_type'].", rep_period='$rep_period'");

		foreach($rowsdata as $row) {
			if ($row->sum) {
				#Add new expenses group
				if (!(int)$row->group_id) {
					runsql("INSERT INTO acc_expenses_group SET name=".quote($row->group_name).", VAT=".quote($row->VAT));
					$row->group_id = mysql_insert_id();
				}
				#document rows
				$sum=round($row->sum,2)-round((round($row->sum,2)/(100+$VAT)*$VAT)*round($row->VAT,2)/100,2);
				$Rsql.=($Rsql?',':'')."($docID,".(int)$row->group_id.",".quote($row->name).",'".round($row->sum,2)."','".sql_escape($row->VAT)."','".(round($row->sum,2)-$sum)."')";
				#document operations
				$csql.=($csql?',':'')."('$date',$docID,400,$clientID,700,".(int)$row->acc_card.",'$sum',{$SVARS['user']['id']})";
			}
		}
		
		$csql.=($csql?',':'')."('$date',$docID,400,$clientID,550,".($_REQUEST['expense_type']==1 ? 5503:5504).",'$taxTotal',{$SVARS['user']['id']})";
		
		$Rsql="INSERT INTO acc_expense_rows (doc_id,group_id,name,sum,VAT,VAT_sum) VALUES ".$Rsql;

		if (runsql($Rsql)) {
			if ($csql) runsql("INSERT INTO acc_operations (date,doc_id,credit_class,credit_card,debt_class,debt_card,sum,user_id) VALUES ".$csql);
			runsql("UPDATE acc_counters SET expenses=expenses+1");
			if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET expenses=1");
			echo "{success:true, id:$docID, num:$num}";
		}else{
			runsql("DELETE FROM acc_docs WHERE id=docID");
			echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
		}
		
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";	
}
//————————————————————————————————————————————————————————————————————————————————————
function f_deposit() {
 global $SVARS;
	$ids = implode(',',json_decode($_REQUEST['payments']));
	$accountId = (int)$_REQUEST['account'];
	if (!$ids OR !$accountId) die('{success:false, msg:"Data error!"}');
	if (runsql("UPDATE acc_accept_rows SET payment_status=1, deposit=$accountId WHERE id IN ($ids)")) {
		$payments=sql2array("SELECT * FROM acc_accept_rows WHERE deposit=$accountId AND id IN ($ids)");
		
		$sql="SELECT acc_accept_rows.*, "
			."TRIM(clients.name) AS client_name "
			."FROM acc_accept_rows "
			."LEFT JOIN acc_docs ON (acc_accept_rows.accept_id=acc_docs.id) "
			."LEFT JOIN clients ON (acc_docs.client_id=clients.id) "
			."WHERE deposit=$accountId AND acc_accept_rows.id IN ($ids) ORDER BY acc_accept_rows.date, acc_accept_rows.id";
		$payments = sql2array($sql);
		
		$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html dir="rtl"><head><title>טופס הפקדה</title>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />';

		
		$html.= "<link rel=stylesheet href=skin/style.css>
		<style>
		body {font-family:Tahoma; font-size:9pt;}
		.tbl1 { border-collapse:collapse; border:1px solid gray; width:100%;}
		.tbl1 td, .tbl1 th { padding:2px 5px; border:1px solid gray; font-size:8pt;}
		h2 {font-family:Tahoma; font-size:14pt; margin:0}
		.tbl1 th {text-align:right; background-color:#F0F0F0; }
		.tbl1 .bold td {font-weight:bold;}
		@media print{ .userinput{border-bottom:0} }
		</style>
		</head>
		<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים את המסמך\")) window.print()'>
		<div align=right style='margin-bottom:5px; font-size:9pt;'>".$user_logo."<h2>".htmlspecialchars($agency[$group_id]['agency'])."</h2>".trim(($agency[$group_id]['c_id']? "ח.פ.: ".$agency[$group_id]['c_id']:'').($agency[$group_id]['c_authorized']? ($PRS['garge_pan'][1]==1 ? ' מוסך':' עוסק')." מורשה: ".$agency[$group_id]['c_authorized']:''))."<br>$address<br>$phones</div>
		<div style='clear:both; border-bottom:1px solid gray;'>
		<table width=100% cellpadding=0 cellspacing=0><tr><td valign=top style='padding:0; text-align:right; font-size:9pt;'>";

		$html.= "<br><b>בנק: ".htmlspecialchars($doc['bank_name'])."</b><br><b>סניף</b>: ".htmlspecialchars("$doc[branch_code] - $doc[branch_name]")."<br>".($doc['tel']?"<b>טלפון</b>: $doc[tel]  ":'').($doc['fax']?"<b>פקס</b>: $doc[fax]  ":'');
		$html.= "</td><td valign=bottom style='text-align:left; font-size:10px;'>תאריך הדפסה: ".date('d/m/Y H:i')."

		</td></tr></table>

		</div><br>
		<table width=100%><tr>
			<td colspan=2 align=center nowrap width=40%><h2>טופס הפקדה</h2>".($tax_clean ? '<br><b>לחברה פטור מלא מניקוי מס במקור</b>':'')."</td>
		</tr><tr>
			<td style='font-size:9pt;'><b>תאריך: ".str2time($doc['date'],'d/m/Y')."</b></td>
			<td align=left nowrap><b style='font-size:18px'>".((!$doc['printed'] OR $original_only) ? 'מקור':'העתק משוחזר')."</b></td>
		</tr></table>
		<div style='padding:15px 5px; font-family:Tahoma; font-size:10pt; border-top:1px solid gray'></div>";
		$total=0;
		
		if ($payments) foreach($payments as $r) {
			$n++;$total+=$r['sum']; $sum=price($r['sum'],2); $ptype=$r['ptype'];
			$rowsHTML.= "<tr><td>$n</td><td>".str2time($r['date'],'d/m/Y')."</td><td>".htmlspecialchars($r['client_name'])."</td><td>".htmlspecialchars($r['ptype'])."</td><td>".htmlspecialchars($r['bank'])."</td><td>".htmlspecialchars(($ptype=='אשראי'? "$r[dept]/" :'').$r['account'])."</td><td>".htmlspecialchars($r['number'])."</td><td>$sum</td></tr>";
		}
		
		if ($ptype=='אשראי') 
			$docHTML.= "<br><br><br><br><table width=100% class=tbl1><tr><th width=10>#</th><th nowrap>תאריך קבלה</th><th nowrap>שם לקוח</th><th width=100>סוג תשלום</th><th width=150>חברת אשראי</th><th width=120>תשלום</th><th nowrap>מס' כרטיס אשראי</th><th width=100>סכום</th></tr>".$rowsHTML;
		else
			$docHTML.= "<br><br><br><br><table width=100% class=tbl1><tr><th width=10>#</th><th nowrap>תאריך פרעון</th><th nowrap>שם לקוח</th><th width=100>סוג תשלום</th><th width=150>בנק</th><th width=120>חשבון</th><th nowrap>מס' שיק</th><th width=100>סכום</th></tr>".$rowsHTML;
		
		$docHTML.= '<tr><td colspan=7 align=left><b>סה"כ</b>:</td><td><b>'.($doc['type']=='debt'?'(':'').price($total,2).($doc['type']=='debt'?')':'').'</b></td></tr></table>';

		if ($docHTML) $html.=$docHTML;

		echo $html;
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_deposit() {
 global $SVARS;
	$ddata = json_decode($_POST['ddata']);
	$depositDate = str2time($_REQUEST['date'],'Y-m-d');
	$depositSum = number_format($_REQUEST['sum'], 2, '.', '');
	$accountId = (int)$_REQUEST['account'];
	if (!$ddata OR !$depositDate OR !$depositSum OR !$accountId) die('{success:false, msg:"Data error!"}');
	$bank=iconv("UTF-8", "windows-1255", $_REQUEST['bank']);

	
	foreach($_REQUEST as $k=>$v) $_REQUEST[$k]=iconv("UTF-8", "windows-1255", $v);
	
	$num=sql2array("SELECT deposit FROM acc_counters",'','deposit',1);
	$num++;
	$sql="INSERT INTO acc_docs SET  num=$num"
		." , date='$depositDate"
		."', type='deposit"
		."', account_id=$accountId"
		." , sum='$depositSum"
		."', notes='".sql_escape($_REQUEST['notes'])
		."', user_id=".$SVARS['user']['id'];	
	
	if (runsql($sql)) {
		$depositID = mysql_insert_id();
		if ($ddata) {
			foreach($ddata as $row) {
				if ($row->deposit) {
					$ptype = iconv("UTF-8", "windows-1255",$row->ptype);
					
					if ($ptype=='מזומן') {$cashCard=1001; $payment_id=0;}
					elseif ($ptype=='שיק') {$cashCard=1002; $payment_id=(int)$row->id; $cheques.=($cheques?',':'').$payment_id;}
					elseif ($ptype=='אשראי') {
						$payment_id=(int)$row->payment_id;
						$paymets_sum[$payment_id] = round($row->sum,2);
						if ($bank=='וויזה') $cashCard=1004;
						elseif ($bank=='ישראכרט/מאסטרכארד') $cashCard=1005;
						elseif ($bank=='דיינרס') $cashCard=1006;
						elseif ($bank=='אמריקן אקספרס') $cashCard=1007;
						elseif ($bank=='לאומי קארד') $cashCard=1008;
					} else $cashCard=1000;

					#document rows
					$rsql.=($rsql?',':'')."($depositID,'$ptype',$payment_id,'".round($row->sum,2)."')";
					#document operations
					$csql.=($csql?',':'')."('$depositDate',$depositID,100,$cashCard,300,$accountId,'".round($row->sum,2)."',{$SVARS['user']['id']})";
				}
			}
		}
		
		if ($rsql AND $csql) {
			runsql("INSERT INTO acc_deposit_rows (deposit_id,type,cheque_id,sum) VALUES ".$rsql);
			runsql("INSERT INTO acc_operations (date,doc_id,credit_class,credit_card,debt_class,debt_card,sum,user_id) VALUES ".$csql);
			
			if ($cheques) runsql("UPDATE acc_accept_rows SET cheque_status=1 WHERE id IN ($cheques)");
			
			if ($paymets_sum) foreach($paymets_sum as $p=>$sum) runsql("UPDATE acc_accept_rows SET cheque_status=cheque_status+1, paid=paid+$sum WHERE id=$p");
			
			runsql("UPDATE acc_bank_accounts SET balance=(balance+$depositSum) WHERE id=$accountId");
			runsql("UPDATE acc_counters SET deposit=deposit+1");
			if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET deposit=1");
			echo "{success:true, id:$depositID, num:$num}";
		}else{
			runsql("DELETE FROM acc_deposits WHERE id=$depositID");
			echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
		}
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";	}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_refund() {
 global $SVARS;
 	$clientID = (int)$_POST['client_id'];
	if (!$clientID AND $_REQUEST['refundType']!='bankcash') die('{success:false, msg:"נא לבחור לקוח!"}');

	$ddata = json_decode($_POST['ddata']);
	$depositDate = str2time($_REQUEST['date'],'Y-m-d');
	$depositSum = number_format($_REQUEST['sum'], 2, '.', '');
	$accountId = (int)$_REQUEST['account'];

	if (!$ddata OR !$depositDate OR !$depositSum OR !$accountId) die('{success:false, msg:"Data error!"}');
	
	foreach($_REQUEST as $k=>$v) $_REQUEST[$k]=iconv("UTF-8", "windows-1255", $v);

	$num=sql2array("SELECT refund FROM acc_counters",'','refund',1);
	$num++;
	$sql="INSERT INTO acc_docs SET  num=$num, client_id=$clientID"
		." , date='$depositDate"
		."', type='refund_".($_REQUEST['refundType'])
		."', account_id=$accountId"
		." , sum='$depositSum"
		."', notes='".sql_escape($_REQUEST['notes'])
		."', user_id=".$SVARS['user']['id'];	
	
	if (runsql($sql)) {
		if ($_REQUEST['refundType']=='bankclient')		{$cheque_status=2; $cheque_operation="300,$accountId,200,$clientID";}
		elseif ($_REQUEST['refundType']=='bankcash')	{$cheque_status=3; $cheque_operation="300,$accountId,100,1002";}
		elseif ($_REQUEST['refundType']=='cashclient')	{$cheque_status=2; $cheque_operation="100,1002,200,$clientID";}
		
		$depositID = mysql_insert_id();
		if ($ddata) {
			foreach($ddata as $row) {
				if ($row->deposit) {
					if (iconv("UTF-8", "windows-1255",$row->ptype)=='שיק') $cheques.=($cheques?',':'').$row->id;
					#document rows
					$rsql.=($rsql?',':'')."($depositID,'".iconv("UTF-8", "windows-1255",$row->ptype)."','".sql_escape($row->id)."','".round($row->sum,2)."')";
					#document operations
					$csql.=($csql?',':'')."('$depositDate',$depositID,$cheque_operation,'".round($row->sum,2)."',{$SVARS['user']['id']})";
				}
			}
		}
		
		if ($rsql AND $csql AND $cheques) {
			runsql("INSERT INTO acc_deposit_rows (deposit_id,type,cheque_id,sum) VALUES ".$rsql);
			runsql("INSERT INTO acc_operations (date,doc_id,credit_class,credit_card,debt_class,debt_card,sum,user_id) VALUES ".$csql);
			runsql("UPDATE acc_accept_rows SET cheque_status=$cheque_status WHERE id IN ($cheques)");
			runsql("UPDATE acc_bank_accounts SET balance=(balance-$depositSum) WHERE id=$accountId");
			runsql("UPDATE acc_counters SET refund=refund+1");
			if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET refund=1");
			echo "{success:true, id:$depositID, num:$num}";
		}else{
			runsql("DELETE FROM acc_deposits WHERE id=$depositID");
			echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
		}
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";	}
//————————————————————————————————————————————————————————————————————————————————————
function f_client_account() {
 global $VAT;
	$client_id = (int)$_REQUEST['client_id'];
	if (!$client_id) die("{success:true, total:0, data:[]}");

	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$sql="SELECT acc_docs.* FROM acc_docs WHERE acc_docs.client_id = $client_id "
		." ORDER BY date ASC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	echo "/*$sql*/";		
	$rows=sql2array($sql);
	$balance=0;
	if ($rows) foreach($rows as $k=>$r) {
		if (in_array($r['type'], array('accept','invoice_accept','invoice_debt','expense'))) $r['debt']=$r['sum'];
		if (in_array($r['type'], array('refund_bankclient','refund_cashclient','invoice_accept','invoice_credit','cash_payment'))) $r['credit']=$r['sum'];
		if ($r['date']!='0000-00-00') $rows[$k]['date']=str2time($r['date'],'d/m/Y');
		$balance+=$r['debt']-$r['credit'];
		$total['debt']+=$r['debt'];
		$total['credit']+=$r['credit'];
		$rows[$k]['balance']=$balance;
		if (!$r['debt']) $rows[$k]['debt']=''; else $rows[$k]['debt']=$r['debt'];
		if (!$r['credit']) $rows[$k]['credit']=''; else $rows[$k]['credit']=$r['credit'];
	}
	
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').',summary:{debt:'.round($total['debt'],2).', credit:'.round($total['credit'],2).'}}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_client_orders() {
 global $prms_groups, $VAT;
	$docTypes=array('credit'=>'חשבונית מס','debt'=>'חשבונית זיכוי','invoice_accept'=>'חשבונית מס קבלה','accept'=>'קבלה','accept_tmp'=>'קבלה זמנית','refund'=>'החזרת שיקים','expense'=>'קליטת הוצאות');
	$id = (int)$_REQUEST['id'];
	if (!$id) die("{success:true, total:0, data:[]}");

	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$sql="SELECT acc_orders.* FROM acc_orders, clients WHERE acc_orders.id = $id AND acc_orders.id=clients.id"
		." ORDER BY id ASC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	// echo $sql;		
	$rows=sql2array($sql);
	$balance=0;
	if ($rows) foreach($rows as $k=>$r) {
		if ($r['date']!='0000-00-00') $rows[$k]['date']=str2time($r['date'],'d/m/Y');
	}
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_client_balance() {
 global $prms_groups;
	$clientID = (int)$_REQUEST['id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	$sql="SELECT (SUM(IF ((acc_operations.debt_class=200 AND acc_operations.debt_card=clients.id), acc_operations.sum, 0 )) -	SUM(IF ((acc_operations.credit_class=200 AND acc_operations.credit_card=clients.id), acc_operations.sum, 0 ))) AS balance FROM clients LEFT JOIN acc_operations ON ((acc_operations.credit_class=200 AND acc_operations.credit_card=clients.id) OR (acc_operations.debt_class=200 AND acc_operations.debt_card=clients.id)) WHERE clients.id=$clientID GROUP BY clients.id";
	$balance=sql2array($sql, false, 'balance', true);
	echo "{success:true, balance:'".round($balance,2)."'}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_import_invoice_rows() {
 global $VAT;

	$doc_ids = (int)$_REQUEST['ids'];
	$client_id = (int)$_REQUEST['client_id'];
	
	if (!$doc_ids AND !$client_id) die('{success:false, msg:"Error"}');
	
	if ($_REQUEST['from']=='last_invoice' AND $client_id) {
		$doc_id = sql2array("SELECT id FROM acc_docs WHERE client_id=$client_id AND type LIKE 'invoice%' ORDER BY id DESC LIMIT 1",0,'id',1);
		if ($doc_id) $rows = sql2array("SELECT * FROM acc_invoice_rows WHERE invoice_id=$doc_id ORDER BY id");
	} elseif ($_REQUEST['from']=='order')	$rows = sql2array("SELECT * FROM acc_order_rows WHERE card_id IN ($doc_ids) ORDER BY id");
	elseif ($_REQUEST['from']=='invoice_credit' OR $_REQUEST['from']=='invoice_accept')	$rows = sql2array("SELECT * FROM acc_invoice_rows WHERE invoice_id IN ($doc_ids) ORDER BY id");
	
	if ($rows) foreach($rows as $k=>$r) {
		if ($r['type']!='round') $rows[$k]['sum_tax'] = round(($r['sum']*(100+$VAT*$r['VAT'])/100),2);
		if (!$r['unit']) unset($rows[$k]['unit']);
		if (!$r['amount']) unset($rows[$k]['amount']);
		if (!$r['price']) unset($rows[$k]['price']);
		if (!$r['sum']) unset($rows[$k]['sum']);
	}
	echo "{success:true, data:".($rows ? array2json($rows):'[]').'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_incomes_list() {
 global $prms_groups, $VAT;
 
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if ($_POST['q']) {
		$make_where_search=make_where_search('clients.last_name,clients.first_name', iconv("UTF-8", "windows-1255", $_POST['q']));
		if (ereg('^[0-9.]*$',$_POST['q'])) $make_where_search="($make_where_search OR num=$_POST[q] OR ROUND(sum)=".round($_POST['q']).")"; # if a number search in polices
		elseif (ereg("([0-9]{1,2})[./]?([0-9]{1,2})[./]?([0-9]{2,4})( ([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?)?", $_POST['q']) ) $make_where_search="(date='".str2time($_POST['q'],'Y-m-d')."' OR $make_where_search)"; # if a number search in polices
	}
	
	$sql="SELECT SQL_CALC_FOUND_ROWS acc_docs.*, TRIM(CONCAT(clients.last_name,' ',clients.first_name)) AS client_name FROM acc_docs, clients WHERE acc_docs.type IN ('invoice_debt','invoice_credit','invoice_accept')  AND acc_docs.id=clients.id "
		.($make_where_search ? " AND $make_where_search":'' )
		." ORDER BY date DESC, id DESC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	// echo "/*$sql*/";	
	$rows=sql2array($sql);
	if ($rows) $rows=array_reverse($rows);

	if ($rows) foreach($rows as $k=>$r) {
		// $rows[$k]['price_tax']=round(($r['price']*(1+$r['VAT']*$vat/100)),2);
		if ($r['date']!='0000-00-00') $rows[$k]['date']=str2time($r['date'],'d/m/Y');
		if ($r['type']=='invoice_debt') {$rows[$k]['sum']=-$r['sum'];$rows[$k]['vat_sum']=-$r['vat_sum'];}
	}
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_expenses_list() {
 global $prms_groups, $VAT;
 
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;

	if ($_POST['q']) {
		$make_where_search=make_where_search('clients.last_name,clients.first_name', iconv("UTF-8", "windows-1255", $_POST['q']));
		if (ereg('^[0-9.]*$',$_POST['q'])) $make_where_search="($make_where_search OR num=$_POST[q] OR ROUND(sum)=".round($_POST['q']).")"; # if a number search in polices
		elseif (ereg("([0-9]{1,2})[./]?([0-9]{1,2})[./]?([0-9]{2,4})( ([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?)?", $_POST['q']) ) $make_where_search="(date='".str2time($_POST['q'],'Y-m-d')."' OR $make_where_search)"; # if a number search in polices
	}

	$sql="SELECT SQL_CALC_FOUND_ROWS acc_docs.*, TRIM(CONCAT(clients.last_name,' ',clients.first_name)) AS client_name FROM acc_docs, clients WHERE acc_docs.type='expense' AND acc_docs.id=clients.id "
		.($make_where_search ? " AND $make_where_search":'' )
		." ORDER BY date DESC, id DESC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	// echo "/*$sql*/";	
	$rows=sql2array($sql);
	if ($rows) $rows=array_reverse($rows);

	if ($rows) foreach($rows as $k=>$r) {
		if ($r['date']!='0000-00-00') $rows[$k]['date']=str2time($r['date'],'d/m/Y');
	}
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cash_list() {
 
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if ($_POST['q']) {
		$make_where_search=make_where_search('clients.last_name,clients.first_name', iconv("UTF-8", "windows-1255", $_POST['q']));
		if (ereg('^[0-9.]*$',$_POST['q'])) $make_where_search="(num=$_POST[q] OR ROUND(sum)=".round($_POST['q'])." OR $make_where_search)"; # if a number search in polices
		elseif (ereg("([0-9]{1,2})[./]?([0-9]{1,2})[./]?([0-9]{2,4})( ([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?)?", $_POST['q']) ) $make_where_search="(date='".str2time($_POST['q'],'Y-m-d')."' OR $make_where_search)"; # if a number search in polices
	}
	
	$sql="SELECT SQL_CALC_FOUND_ROWS acc_docs.id AS doc_id,acc_docs.date, acc_docs.type, acc_docs.num,acc_docs.sum, acc_docs.user_id,acc_docs.notes, TRIM(CONCAT(clients.last_name,' ',clients.first_name)) AS client_name FROM acc_docs LEFT JOIN clients ON acc_docs.id=clients.id "
		."WHERE acc_docs.type IN ('accept','accept_tmp','invoice_accept','deposit','refund_cashclient','refund_bankcash','cash_payment')"
		.($make_where_search ? " AND $make_where_search":'' )
		." ORDER BY date DESC, doc_id DESC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	// echo "/*$sql*/";	
	$rows=sql2array($sql);
	if ($rows) $rows=array_reverse($rows);
	
	if ($rows) foreach($rows as $k=>$r) {
		if ($r['date']!='0000-00-00') $rows[$k]['date']=str2time($r['date'],'d/m/Y');
		if (in_array($r['type'], array('accept','refund_bankcash','invoice_accept'))) $r['debt']=$r['sum'];
		if (in_array($r['type'], array('deposit','refund_cashclient', 'cash_payment'))) $r['credit']=$r['sum'];
		
		$balance+=$r['debt']-$r['credit'];
		$rows[$k]['balance']=$balance;
		if (!$r['debt']) $rows[$k]['debt']=''; else $rows[$k]['debt']=$r['debt'];
		if (!$r['credit']) $rows[$k]['credit']=''; else $rows[$k]['credit']=$r['credit'];
	}
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_branchs_list() {
 global $prms_groups, $VAT;
 	$branchs=sql2array("SELECT branch_code, CONCAT(branch_code,' - ',branch_name) AS branch_name,tel,fax FROM acc_banks WHERE bank_code='$_POST[bank]'");
	// $branchs=sql2array("SELECT branch_code,branch_name,tel,fax FROM acc_banks WHERE bank_code='$_POST[bank]'");
	echo "{success:true, total:".count($branchs).", data:".($branchs ? array2json($branchs):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cards_list() {
	$sql="SELECT IFNULL(acc_user_cards.number, CONCAT(card_id, subcard_id)) AS number, acc_cards.* FROM acc_cards LEFT JOIN acc_user_cards ON (acc_cards.id=acc_user_cards.card_id) ORDER BY card_id, subcard_id";
	$sql="SELECT number, CONCAT(acc_cards.id) AS num, acc_cards.* FROM acc_cards LEFT JOIN acc_user_cards ON (acc_cards.id=acc_user_cards.card_id) ORDER BY acc_cards.class_id";
	$cards=sql2array($sql,'num');
	// echo "<div dir=ltr align=left><pre>".print_r($cards,1)."</pre></div>";
	foreach($cards as $k=>$c) {
		if (!$c['number']) $cards[$k]['number']="$c[card_id]$c[subcard_id]";
		if ($c['subcard_id']) $cards[$k]['parent_name']=$cards["$c[card_id]0"]['name']; else $cards[$k]['parent_name']='';
	}
	sort($cards);
	// echo "<div dir=ltr align=left><pre>".print_r($cards,1)."</pre></div>";
	echo "{success:true, total:".count($cards).", data:".($cards ? array2json($cards):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cheques_list() {
	$sql="SELECT acc_accept_rows.* FROM acc_accept_rows "
		."INNER JOIN acc_docs ON (acc_docs.type IN ('accept','invoice_accept') AND acc_docs.id=acc_accept_rows.accept_id ) "
		."WHERE ptype='שיק'"
		.($_REQUEST['id'] ? " AND id=".(int)$_REQUEST['id']:'')
		.(((int)$_REQUEST['cheque_status']==="$_REQUEST[cheque_status]") ? " AND cheque_status=".(int)$_REQUEST['cheque_status'] : " AND cheque_status IN (".sql_escape($_REQUEST['cheque_status']).") ")
		." ORDER BY date";
 	echo "/*$sql*/";
	$cheques=sql2array($sql);
	
	if ($_REQUEST['cashBalance']) {
		$sql="SELECT IF (credit_class=100, credit_card, debt_card) AS card, SUM(IF (credit_class=100, -sum, sum)) as balance FROM acc_operations "
		." WHERE debt_card=1001 OR credit_card=1001 GROUP BY card";
		$balance=sql2array($sql,'','balance',1);
		if ($cheques) array_unshift($cheques, array('id'=>0,'deposit'=>false,'date'=>'','ptype'=>'מזומן','sum'=>$balance,'bank'=>'','dept'=>'','account'=>'','number'=>''));
		else $cheques=array('id'=>0,'deposit'=>false,'date'=>'','ptype'=>'מזומן','sum'=>$balance,'bank'=>'','dept'=>'','account'=>'','number'=>'');
		// echo "/*<div dir=ltr align=left><pre>".print_r($balance,1)."</pre></div>*/";
	}

	echo "{success:true, total:".count($cheques).", cashBalance:".(int)$balance.", data:".($cheques ? array2json($cheques):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_credit_payments_list() {

	$_REQUEST['bank'] = iconv("UTF-8", "windows-1255", $_REQUEST['bank']); 

	$sql="SELECT accept_id, acc_accept_rows.id AS payment_id, ptype, acc_accept_rows.date AS accept_date, acc_accept_rows.sum, acc_accept_rows.paid, bank, account, number, cheque_status, acc_docs.num "
		.", TRIM( CONCAT(last_name,' ',first_name)) AS client_name "
		."FROM acc_accept_rows "
		."INNER JOIN acc_docs ON (acc_docs.type IN ('accept','invoice_accept') AND acc_docs.id=acc_accept_rows.accept_id ) "
		."INNER JOIN clients ON (acc_docs.id=clients.id ) "
		."WHERE ptype='אשראי'"
		.($_REQUEST['id'] ? " AND id=".(int)$_REQUEST['id']:'')
		.(($_REQUEST['bank'] AND $_REQUEST['bank']!='כולם') ? " AND bank=".quote($_REQUEST['bank']):'')
		." AND cheque_status".($_REQUEST['status']==1 ? '=':'<')."account"
		." ORDER BY acc_accept_rows.date";
		
 	// echo "/*$sql*/";
	$payments=sql2array($sql);
	
	// echo "/*<div dir=ltr align=left><pre>".print_r($payments,1)."</pre></div>*/";

	if ($payments) foreach($payments as $key=>$p) {
		if ((int)$p['account']) $p['sum']=round(($p['sum']-$p['paid'])/($p['account']-$p['cheque_status']),2);
		$i=$p['cheque_status'];
		// for($i=$p['cheque_status']; $i<$p['account']; $i++) {
			if (/*$i==0 AND */str2time($p['accept_date'],'d')<16) $payment_day=substr($p['accept_date'],0,8).'02'; else $payment_day=substr($p['accept_date'],0,8).'08';
			$p['date']=date('Y-m-d', strtotime("$payment_day +".($i+1)." month"));
			$p['dept']=($i+1)."/$p[account]";
			$rows[]=$p;
		// }
	}
	
	echo "{success:true, total:".count($rows).", data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_cheque() {
 global $SVARS;
 	$clientID = (int)$_POST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');

	$chequeDate = str2time($_REQUEST['date'],'Y-m-d');
	$chequeSum = number_format($_REQUEST['sum'], 2, '.', '');
	$accountId = (int)$_REQUEST['account'];

	if (!$chequeDate OR !$chequeSum OR !$accountId) die('{success:false, msg:"Data error!"}');
	
	foreach($_REQUEST as $k=>$v) $_REQUEST[$k]=iconv("UTF-8", "windows-1255", $v);

	$num=sql2array("SELECT cheque FROM acc_counters",'','cheque',1);
	$num++;
	$sql="INSERT INTO acc_docs SET  num=$num, client_id=$clientID"
		." , date='$chequeDate"
		."', type='cheque"
		."', account_id=$accountId"
		." , sum='$chequeSum"
		."', notes='".sql_escape($_REQUEST['notes'])
		."', user_id=".$SVARS['user']['id'];	
	
	if (runsql($sql)) {
		$depositID = mysql_insert_id();
		
		if ($rsql AND $csql AND $cheques) {
			// runsql("INSERT INTO acc_deposit_rows (deposit_id,type,cheque_id,sum) VALUES ".$rsql);
			runsql("INSERT INTO acc_operations (date,doc_id,credit_card,credit_subcard,credit_sub1,debt_card,debt_subcard,debt_sub1,sum,user_id) VALUES ".$csql);
			runsql("UPDATE acc_bank_accounts SET balance=(balance-$chequeSum) WHERE id=$accountId");
			runsql("UPDATE acc_counters SET cheque=cheque+1");
			if (!mysql_affected_rows()) runsql("INSERT INTO acc_counters SET cheque=1");
			echo "{success:true, id:$depositID, num:$num}";
		}else{
			runsql("DELETE FROM acc_deposits WHERE id=$depositID");
			echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
		}
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";	}
//————————————————————————————————————————————————————————————————————————————————————
function f_products_shortList() {
 global $prms_groups, $vat;
 	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	if ($_POST['query']) $make_where_search=make_where_search('name,code,supplier_code', iconv("UTF-8", "windows-1255", $_POST['query']));
	$sql="SELECT SQL_CALC_FOUND_ROWS * FROM crm_store WHERE crm_store.deleted='0' ".($make_where_search?" AND $make_where_search":'');
	$rows=sql2array($sql);
	if ($rows) foreach($rows as $k=>$r) $rows[$k]['price_tax']=round(($r['price']*(1+$r['VAT']*$vat)),2);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_get_client_last_payment() {
 global $SVARS;
	$clientID = (int)$_REQUEST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	$sql = "SELECT acc_accept_rows.* ,acc_docs.income_group as group_id, acc_income_groups.name as group_name
		FROM acc_docs 
		LEFT JOIN acc_accept_rows ON (acc_accept_rows.accept_id=acc_docs.id) 
		LEFT JOIN acc_income_groups on (acc_income_groups.id = acc_docs.income_group ) 
		WHERE client_id=$clientID AND type IN ('accept','invoice_accept') 
		ORDER BY acc_docs.date DESC, acc_accept_rows.id DESC LIMIT 0,1";
	echo"/*$sql*/";
	$payment = sql2array($sql);
	$sql = "SELECT acc_invoice_rows.* FROM acc_docs LEFT JOIN acc_invoice_rows ON (acc_invoice_rows.invoice_id=acc_docs.id) WHERE client_id=$clientID AND acc_docs.type IN ('invoice_credit','invoice_accept') ORDER BY acc_docs.id DESC, acc_invoice_rows.id ASC LIMIT 0,1";
	
	$invoice = sql2array($sql);

	echo "{success:true, payment:".($payment ? array2json($payment):'[]').", invoice:".($invoice ? array2json($invoice):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————

function f_get_supplier_last_supply() {
 global $SVARS;
	$clientID = (int)$_REQUEST['client_id'];
	if (!$clientID) die('{success:false, msg:"clientID error"}');
	$sql="SELECT acc_expense_rows.name, acc_expense_rows.group_id, acc_expenses_group.name AS group_name, acc_expense_rows.VAT, acc_expense_ext.expense_type "
		."FROM acc_docs "
		."LEFT JOIN acc_expense_rows ON (acc_expense_rows.doc_id=acc_docs.id) "
		."LEFT JOIN acc_expenses_group ON (acc_expenses_group.id=acc_expense_rows.group_id) "
		."LEFT JOIN acc_expense_ext ON (acc_expense_ext.doc_id=acc_docs.id) "
		."WHERE client_id=$clientID AND acc_docs.type IN ('expense') ORDER BY acc_docs.id DESC, acc_expense_rows.id ASC LIMIT 0,1";
	echo"/*$sql*/";
	$expense = sql2array($sql);
	echo "{success:true, expense:".($expense ? array2json($expense):'[]').'}';
}
?>