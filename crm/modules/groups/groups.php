﻿<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
	?>
	Ext.ux.Loader.load([
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'skin/extjs/resources/css/TreeGridRTL.css?<?=filemtime('skin/extjs/resources/css/TreeGridRTL.css');?>',
			'skin/extjs/resources/css/ColumnHeaderGroup.css?<?=filemtime('skin/extjs/resources/css/ColumnHeaderGroup.css');?>',
			'skin/extjs/ux/TreeGridRTL.js?<?=filemtime('skin/extjs/ux/TreeGridRTL.js');?>',
			'skin/extjs/ux/ColumnHeaderGroup.js?<?=filemtime('skin/extjs/ux/ColumnHeaderGroup.js');?>',
			'modules/groups/groups.js?<?=filemtime('modules/groups/groups.js');?>'
		], function(){Ms.groups.run();}
	);
	<?
}

//————————————————————————————————————————————————————————————————————————————————————	
function f_group_permissions() {
 global $CFG, $SVARS;
 
 	$group_id=(int)$_REQUEST['group_id'];
	
	$company_modules=sql2array("SELECT modules FROM companies WHERE id=$SVARS[cid]",'','modules',1);
	
	if (!ereg('^[0-9]+(,[0-9]+)*$',$company_modules)) die('{success:false, msg:"no active modules"}');
	
	$modules=sql2array("SELECT * FROM modules WHERE id IN($company_modules) OR parent_id IN($company_modules) ORDER BY id, parent_id",'','',0);
	
	$permissions=sql2array("SELECT * FROM permissions WHERE permissions.group_id=$group_id",'module_id','permissions');
	
	if ($modules) foreach($modules as $m) {
		$prm_data[$m['id']]=array(
			'_id'=>$m['id'],
			'_parent'=>$m['parent_id'],
			'_level'=>($m['parent_id']?'2':'1'),
			'title'=>($m['parent_id']?'':'<b>').jsEscape($m['title_he']).($m['parent_id']?'':'</b>'),
			'r'=>(ereg('r|w|d',$permissions[$m['id']]) ? 1:0),
			'w'=>(ereg('w|d',$permissions[$m['id']]) ? 1:0),
			'd'=>(ereg('d',$permissions[$m['id']]) ? 1:0),
			'R'=>(ereg('R|W|D',$permissions[$m['id']]) ? 1:0),
			'W'=>(ereg('W|D',$permissions[$m['id']]) ? 1:0),
			'D'=>(ereg('D',$permissions[$m['id']]) ? 1:0),
			'_is_leaf'=>true
		);
		if ($m['parent_id']) $prm_data[$m['parent_id']]['_is_leaf']=false;
	
	}
	sort($prm_data);
	
	echo array2json($prm_data);
}

//————————————————————————————————————————————————————————————————————————————————————
function f_save_permissions() {
 	$group_id=(int)$_REQUEST['group_id'];
	if (!$group_id) die('{success:false, msg:"no group_id"}');
	$data = json_decode($_POST['data']);
	if ($data) {
		runsql("DELETE FROM permissions WHERE group_id=$group_id;");

		foreach($data as $p) {
			$module=(int)$p->_id;
			if ($module) {
				$permissions=(int)(($p->r?1:0)+($p->w?2:0)+($p->d?4:0)+($p->R?8:0)+($p->W?16:0)+($p->D?32:0));
				if ($permissions) $insSql[]="($group_id,$module,$permissions)";
			}
		}
		if ($insSql) runsql("INSERT INTO permissions (group_id,module_id,permissions) VALUES ".implode(',',$insSql));
	}
	echo "{success:true}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_list() {
	$sql="SELECT * FROM groups";
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	echo '{total:"'.count($rows).'", data:'.array2json($rows).'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_edit() {
	$id=(int)$_REQUEST['id'];
	$sql="groups SET name=".quote($_POST['name']).", notes=".quote($_POST['notes']);
	$sql = ($id ? "UPDATE $sql WHERE id=$id" : "INSERT INTO $sql");
	if (runsql($sql)) echo "{success:true}"; else echo '{success:false}';		
}

//————————————————————————————————————————————————————————————————————————————————————
function f_delete() {
	$users = sql2array("SELECT id FROM users WHERE group_id IN ($_GET[ids])");
	if ($users) {echo '{success:false, msg:"קיימים משתמשים בקבוצה!<br>מחיקה אסורה!"}'; return;}
	if (runsql("DELETE FROM permissions WHERE group_id IN($_GET[ids])") AND runsql("DELETE FROM groups WHERE id IN($_GET[ids])")) echo '{success:true}';
	else echo '{success:false, msg:"delete error"}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_userlist() {
	$group_id=(int)$_REQUEST['group_id'];
	$sql="SELECT *, TRIM(CONCAT(fname,' ',lname)) AS name FROM users".($group_id ? " WHERE group_id=$group_id":'');
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	echo '{total:"'.count($rows).'", data:'.array2json($rows).'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_load_data() {
 global $SVARS, $group_id;
	// echo "aaaaaaaaa";
	$id=(int)$_REQUEST['id'];
	if (!$id) {echo '{success:false}'; return;}
	
	$groups=$group_id;
	
	$sql="SELECT * FROM employees WHERE id=$id AND group_id IN($groups)";
	$data=sql2array($sql,'','',1);
	if ($data['birthday']=='0000-00-00') $data['birthday']='';
	if ($data['fam_status']==0)	$data['fam_status']=1;
	if ($data['salary']==0)		$data['salary']='';
	if ($data['branch_id']==0)	$data['branch_id']='';
	if ($data['dep0']==0)		$data['dep0']='';
	if ($data['dep1']==0)		$data['dep1']='';
	if ($data['dep2']==0)		$data['dep2']='';

	# Set user_logo
	$photo = glob("files/employee_photos/$group_id/$id.*");
	$photo = $photo[0];
	if ($photo) $data['photo']="$photo?".filemtime($photo);
	
	echo '{success:true, total:1 ,data:'.array2json($data).'}';
}

?>