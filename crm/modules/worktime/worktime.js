
var CFG={
	// values in minutes
	 dayExtra: 8.5*60
	,dayExtraYoung: 8*60

	,maxDayExtra: 3*60
	// ,maxDayExtraYoung: 3*60

	,maxWeekHours: 55*60
	,maxYoungWeekHours: 40*60

	,youngFirstHour: 6*60
	,youngLastHour: 22*60
};


var timeField = Ext.extend(Ext.form.TimeField, {format:'H:i'});


// getWeekStart=function(date){ 
	// return date.add(Date.DAY, -date.format("w"))
// };


time2min=function(time) {
	if (time=='' || !(/^[0-9]{1,2}:[0-9]{1,2}$/).test(time)) return null;
	time=time.split(':');
	return Number(time[0])*60 + Number(time[1]);
};

min2time=function(min) {
	if (min===null) return '';
	var hours = Math.floor(min/60).toString();
	var min= (min - hours*60).toString();
	if (hours.length<2) hours='0'+hours;
	if (min.length<2) min='0'+min;
	return hours+':'+min;
};


EmployeeWeek = function(cfg) {
	// this.emp=cfg.emp; // employee data
	// this.dep=cfg.dep; // department data
	Ext.apply(this, cfg);
	this.holiday = this.emp.religion==2 ? 0 : (this.emp.religion==3 ? 5 : 6);	// 0 to 6, 0=sunday
	this.isYoung = (this.emp.age<18);
	
	
	this.d=[];	// days data
	this.alerts='';
	this.weekTime=0;
	this.weekPayed=0;
	this.weekExtra=0;
	
	this.store = new Ext.data.SimpleStore({fields:['title', 'd0', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6'], data:[['<b>כניסה 1</b>'],['<b>יציאה 1</b>'],['<b>כניסה 2</b>'],['<b>יציאה 2</b>']]});
	// if (this.emp.d) this.store.loadData(this.emp.d);
	
	
	
	var day, dayHeaders=[], dayCss=[];
	for (var d=0; d<7; d++) {
		day = this.dep.weekStart.add(Date.DAY, d);
		dayHeaders[d] = day.format('D')+"'&nbsp; "+day.format('d/m');
		dayCss[d] = (d==this.holiday) ? 'border:1px solid #ffd3cc;' : null;
		this.d[d]={date:day.format('Y-m-d')};
	}
	
	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store, region:'east', width:522, autoHeight:true, stripeRows:true, enableColumnResize:false, 
		clicksToEdit:1, border:false, style:'direction:ltr;', selModel:new Ext.grid.RowSelectionModel({singleSelect:true}),
		columns: [
			{header:'<b>סה"כ</b>', dataIndex:'', width:65, align:'center'}
			,{header:dayHeaders[6], dataIndex:'d6', width:55, editor:new timeField(), align:'center', css:dayCss[6]}
			,{header:dayHeaders[5], dataIndex:'d5', width:55, editor:new timeField(), align:'center', css:dayCss[5]}
			,{header:dayHeaders[4], dataIndex:'d4', width:55, editor:new timeField(), align:'center', css:dayCss[4]}
			,{header:dayHeaders[3], dataIndex:'d3', width:55, editor:new timeField(), align:'center', css:dayCss[3]}
			,{header:dayHeaders[2], dataIndex:'d2', width:55, editor:new timeField(), align:'center', css:dayCss[2]}
			,{header:dayHeaders[1], dataIndex:'d1', width:55, editor:new timeField(), align:'center', css:dayCss[1]}
			,{header:dayHeaders[0], dataIndex:'d0', width:55, editor:new timeField(), align:'center', css:dayCss[0]}
			,{header:'תאריך:', dataIndex:'title', width:70}
		]
	});

	
	this.calcTimeDiff=function(start, end) {
		if (start===null || end===null) return 0;
		var diff=end-start;
		if (diff<0) diff=diff+24*60;
		return diff;
	};

	this.alertsPanel=new Ext.Panel({border:false, style:'color:#f40000;'});
	
	this.refreshSummary=function(){
		var rs=this.grid.store.getRange()
			,shift1
			,shift2;
		this.alerts='';

		this.weekTime=0;
		this.weekPayed=0;
		this.weekExtra=0;
		
		for(var d=0; d<7; d++){
			this.d[d]['in1'] = time2min(rs[0].data['d'+d]);
			this.d[d]['out1']= time2min(rs[1].data['d'+d]);
			this.d[d]['in2'] = time2min(rs[2].data['d'+d]);
			this.d[d]['out2']= time2min(rs[3].data['d'+d]);
			
			shift1 = this.calcTimeDiff(this.d[d]['in1'], this.d[d]['out1']);
			shift2 = this.calcTimeDiff(this.d[d]['in2'], this.d[d]['out2']);
			
			this.d[d]['time'] = shift1+shift2;
			this.weekTime+=this.d[d]['time'];
			
			// ניכוי הפסקות
			if		(shift1>690) shift1-=90;	// מעבר ל- 11.5 שעות נוכחות- שעה וחצי הפסקה
			else if (shift1>360) shift1-=60;	// בין 6 עד 11.5 שעות- ניכוי של שעה
			else if (shift1>210) shift1-=30;	// משמרת שבין 3.5 עד 6 שעות- ניכוי חצי שעת הפסקה
			
			if		(shift2>690) shift2-=90;	// מעבר ל- 11.5 שעות נוכחות- שעה וחצי הפסקה
			else if (shift2>360) shift2-=60;	// בין 6 עד 11.5 שעות- ניכוי של שעה
			else if (shift2>210) shift2-=30;	// משמרת שבין 3.5 עד 6 שעות- ניכוי חצי שעת הפסקה
			
			this.d[d]['payed'] = shift1+shift2;
			
			this.weekPayed+=this.d[d]['payed'];			

			// extra hours
			this.d[d]['extra']=0;
			if (this.d[d]['payed']>CFG.dayExtra) {
				this.d[d]['extra'] = this.d[d]['payed'] - CFG.dayExtra;
				this.weekExtra+=this.d[d]['extra'];
				if (this.d[d]['extra']>CFG.maxDayExtra) this.alerts+='עובד יותר מ-'+(CFG.maxDayExtra/60)+' שעות נוספות ביום <b>'+Date.dayNames[d]+"'</b><br>";
			}
			
			if (this.isYoung) { // check min max hours
				if ( (this.d[d]['in1']!==null && this.d[d]['in1']<CFG.youngFirstHour) || (this.d[d]['in2']!==null && this.d[d]['in2']<CFG.youngFirstHour) ) 
					this.alerts+='נוער משובץ לפני '+min2time(CFG.youngFirstHour)+' בבוקר ביום <b>'+Date.dayNames[d]+"'</b><br>";
				if ( (this.d[d]['out1']!==null && this.d[d]['out1']>CFG.youngLastHour) || (this.d[d]['out2']!==null && this.d[d]['out2']>CFG.youngLastHour) ) 
					this.alerts+='נוער משובץ לאחר '+min2time(CFG.youngLastHour)+' ביום <b>'+Date.dayNames[d]+"'</b><br>";
				
				if (this.d[d]['payed']>CFG.dayExtraYoung) this.alerts+='נוער עובד יותר מ-'+(CFG.maxDayExtraYoung/60)+' שעות ביום <b>'+Date.dayNames[d]+"'</b><br>";
			}
		}
				
		this.renderDaysSummary();
		this.renderPayedDaysSummary();
		if (this.weekExtra) {
			this.renderDaysExtraSummary();
			this.grid.view.extraSummary.dom.style.display='';
		}else this.grid.view.extraSummary.dom.style.display='none';
		
		// max hours in week
		if (this.isYoung && this.weekPayed>CFG.maxYoungWeekHours) this.alerts+='עובד נוער משובץ יותר מ-'+(CFG.maxYoungWeekHours/60)+' שעות בשבוע<br>';
		else if (this.weekPayed>CFG.maxWeekHours) this.alerts+='העובד משובץ יותר מ-'+(CFG.maxWeekHours/60)+' שעות בשבוע<br>';

		// 7 days one after another
		if (this.d[0]['time'] && this.d[1]['time'] && this.d[2]['time'] && this.d[3]['time'] && this.d[4]['time'] && this.d[5]['time'] && this.d[6]['time']) 
			this.alerts+='לא משובץ ליום מנוחה שבועי<br>';
		
		// אם העובד משובץ 4 שבתות ברציפות- התראה. שבת היא 25 שעות, מכניסת שבת למשך 25 שעות. 
		// שתהייה אפשרות להגדרת שבת כ- 36 שעות
		
		// אם העובד משובץ יותר מ- 7 משמרות לילה במהלך 21 ימי עבודה- התראה
		
		// if (this.alertsPanel && this.alertsPanel.body) 
		this.alertsPanel.body.update(this.alerts);
	};

	
	this.renderDaysSummary=function(){
        var cs=this.grid.view.getColumnData().reverse();
        var config=this.grid.getColumnModel().config;
        var buf=[], c, p={}, cf, last=cs.length-1;
        
		buf[0] = this.cellTpl.apply({value:'<b>סה"כ נוכח:</b>', style:cs[0].style});
		for(var i=1; i<8; i++){
			c=cs[i];  cf=config[i];  p.id=c.id;  p.style=c.style;  p.value=this.d[(i-1)]['time'] ? min2time(this.d[(i-1)]['time']) : 0;
			buf[i] = this.cellTpl.apply(p);
        }
		buf[9] = this.cellTpl.apply({value:'<b>'+(this.weekTime ? min2time(this.weekTime) : 0)+'</b>', style:cs[8].style+';text-align:center;background-color:#dbdbdb;'});
		
		if (!this.grid.view.daySummary) this.grid.view.daySummary = Ext.DomHelper.insertAfter(this.grid.view.mainBody.dom, {tag:'div'}, true);
		this.grid.view.daySummary.update(this.rowTpl.apply({cells:buf.reverse().join(''), tstyle:'width:'+this.grid.view.getTotalWidth()+';'}));
	};	
	
	this.renderPayedDaysSummary=function(){
        var cs=this.grid.view.getColumnData().reverse();
        var config=this.grid.getColumnModel().config;
        var buf=[], c, p={}, cf, last=cs.length-1;
        
		buf[0] = this.cellTpl.apply({value:'<b>נוכח לשכר:</b>', style:cs[0].style});
		for(var i=1; i<8; i++){
			c=cs[i];  cf=config[i];  p.id=c.id;  p.style=c.style;  p.value=this.d[(i-1)]['payed'] ? min2time(this.d[(i-1)]['payed']) : 0;
			buf[i] = this.cellTpl.apply(p);
        }
		buf[9] = this.cellTpl.apply({value:'<b>'+(this.weekPayed ? min2time(this.weekPayed) : 0)+'</b>', style:cs[8].style+';text-align:center;background-color:#dbdbdb;'});
		
		if (!this.grid.view.paySummary) this.grid.view.paySummary = Ext.DomHelper.insertAfter(this.grid.view.mainBody.dom, {tag:'div'}, true);
		this.grid.view.paySummary.update(this.rowTpl.apply({cells:buf.reverse().join(''), tstyle:'width:'+this.grid.view.getTotalWidth()+';'}));
	};
	
	this.renderDaysExtraSummary=function(){
        var cs=this.grid.view.getColumnData().reverse();
        var config=this.grid.getColumnModel().config;
        var buf=[], c, p={}, cf;
        
		buf[0] = this.cellTpl.apply({value:'שעות נוספות:', style:cs[0].style+'color:darkred;'});
		for(var i=1; i<8; i++){
			c=cs[i];  cf=config[i];  p.id=c.id;  p.style=c.style+'color:darkred;';  p.value=this.d[(i-1)]['extra'] ? min2time(this.d[(i-1)]['extra']) : 0;
			buf[i] = this.cellTpl.apply(p);
        }
		buf[9] = this.cellTpl.apply({value:'<b>'+(this.weekExtra ? min2time(this.weekExtra) : 0)+'</b>', style:cs[8].style+';background-color:#dbdbdb;color:darkred;'});
		
		if (!this.grid.view.extraSummary) this.grid.view.extraSummary = Ext.DomHelper.insertAfter(this.grid.view.mainBody.dom, {tag:'div', style:{display:'none'}}, true);
		this.grid.view.extraSummary.update(this.rowTpl.apply({cells:buf.reverse().join(''), tstyle:'width:'+this.grid.view.getTotalWidth()+';'}));
	};


	this.init=function(){
		this.grid.view.afterMethod('refresh', this.refreshSummary, this);
		this.grid.view.afterMethod('refreshRow', this.refreshSummary, this);
		this.grid.view.afterMethod('onUpdate', this.refreshSummary, this);
	
		if(!this.rowTpl){
			this.rowTpl = new Ext.Template(
				'<div class="x-grid3-row-summary row-summary1" style="{tstyle}">',
				'<table class="x-grid3-summary-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
					'<tbody><tr>{cells}</tr></tbody>',
				'</table></div>'
			);
			this.rowTpl.disableFormats=true;
		}
		this.rowTpl.compile();

		if(!this.cellTpl){
			this.cellTpl = new Ext.Template(
				'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}">',
				'<div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on">{value}</div>',
				"</td>"
			);
			this.cellTpl.disableFormats=true;
		}
		this.cellTpl.compile();
	};
	
	this.notesStore = new Ext.data.SimpleStore({
		fields:['date', 'text']//, data:[['10/01/2010','asd fasdf asd '],['10/01/2010','aaaaaaaaaaaaaa']]
	});
	this.userNotes=new Ext.grid.GridPanel({ 
		store:this.notesStore, height:90, border:false, autoExpandColumn:'text', style:'direction:ltr;',
		hideHeaders:true,
		columns:[{header:'הערות העובד:', id:'text', dataIndex:'text'},{header:'תאריך', width:65, dataIndex:'date'}]
	});
	
	this.getData=function(){
		return {
			emp_id:this.emp.id
			,weekTime:this.weekTime
			,weekPayed:this.weekPayed
			,weekExtra:this.weekExtra
			,days:this.d
		};
	};
	
	this.restoreData=function(d) {
		// Dump(d);
		var data=[['<b>כניסה 1</b>'],['<b>יציאה 1</b>'],['<b>כניסה 2</b>'],['<b>יציאה 2</b>']];
		for(var i=0; i<7; i++){
			data[0].push(min2time(d[i].in1));
			data[1].push(min2time(d[i].out1));
			data[2].push(min2time(d[i].in2));
			data[3].push(min2time(d[i].out2));
		}
		this.store.loadData(data);
	};
	
	var tools = [
		 // {id:'down', handler:function(){Ext.Msg.alert('Message', 'The Settings tool was clicked.')} }
		{id:'close', handler:function(e, target, panel){panel.ownerCt.remove(panel, true);} }
	];

	// this.form = new Ext.form.FormPanel({
	EmployeeWeek.superclass.constructor.call(this, {
		title:this.emp.name,
		frame:true,
		tools:tools,
		cls:'empPanel',
		items:[
			{layout:'table', layoutConfig:{columns:3, tableAttrs:{width:'100%'}}, baseCls:'x-plain', bodyStyle:'direction:rtl;', defaults:{cellCls:'valign-top'},
				items: [
					 this.grid
					 ,{width:5, border:false}
					 ,{width:'100%', cellCls:'valign-top width100p', border:false, items:[
						{html:'<div class=x-grid3-header style="padding:2px 5px"><b>הערות העובד</b></div>', border:false}
						,this.userNotes
						,{html:'<div class=x-grid3-header style="padding:2px 5px"><b>התראות:</b></div>', border:false}
						,this.alertsPanel
					 ]}
				]
			}
		]
		// ,tbar:[
			// {xtype:'combo', fieldLabel:'עובד', store:[[1,'סלבה'],[2,'מושה']], value:1, allowBlank:false, hiddenName:'status', width:100, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
		// ]
	});
	
	this.alertsPanel.on('afterrender',function(){
		this.init();
		this.renderDaysExtraSummary();
		this.renderPayedDaysSummary();
		this.renderDaysSummary();
		if (this.emp.d) this.restoreData(this.emp.d);
		this.refreshSummary();
	},this);
};
Ext.extend(EmployeeWeek, Ext.Panel);




Ext.ns("Ext.util");
Ext.util.WeekPicker=Ext.extend(Ext.DatePicker, {
	onRender:function (container, position) { Ext.util.WeekPicker.superclass.onRender.call(this, container, position); },
	updateRange:function() {
		if (this.startDate && this.endDate) {
			var st=this.startDate.clearTime().getTime();
			var et=this.endDate.clearTime().getTime();
			this.cells.each(function(c){
				var dt=c.dom.firstChild.dateValue;
				if (st <= dt && dt <= et) c.addClass("x-date-selected");
				else c.removeClass("x-date-selected");
			});
		}
	},
	setRange:function(startDate, endDate) { this.startDate=startDate; this.endDate=endDate; },
	update:function(date, forceRefresh) {
		this.value = this.startDate = date.add(Date.DAY, -date.format("w"));
		this.endDate = this.startDate.add(Date.DAY, 6);
		// Dump(date.format("Y-m-d"));
		Ext.util.WeekPicker.superclass.update.call(this, this.value, forceRefresh);
		this.updateRange();
		
		// Dump(this.getValue().format("Y-m-d"));
	}
});



DepWorkWeek = function(cfg) {
	// this.weekStart = getWeekStart(new Date());
	var now=new Date();
	this.weekStart = now.add(Date.DAY, -now.format("w"));
	this.weekEnd   = this.weekStart.add(Date.DAY, 6);
	this.empById={};
	this.empInList={};
	
	Ext.apply(this, cfg);

	this.getWeekData=function(){
		var data=[];
		var items=Ext.getCmp('depWorkers').items.items;
		for(var i=0; i<items.length; i++) data.push( items[i].getData() );
		// Dump(data,3);
		return data;
	};
	
	this.saveData=function(){
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			method:'POST',
			url:'?m=departments/worktime_f&f=save_data&branch_id='+this.d.branch_id+'&dep_id='+this.d.id,
			params:{data:Ext.encode(this.getWeekData())},
			success:function(r){
				// var d=Ext.decode(r.responseText);  if (ajRqErrors(d)) return;
				this.getEl().unmask();
			},
			failure:function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope:this
		});
	};
	
	this.loadData=function(){
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url:'?m=departments/worktime_f&f=load_data&weekStart='+this.weekStart.format('Y-m-d')+'&branch_id='+this.d.branch_id+'&dep_id='+this.d.id,
			scope:this,
			success:function(r){
				var d=Ext.decode(r.responseText);  if (ajRqErrors(d)) return;
				this.empStore.loadData(d.employees);
				this.empById={}; for(k in d.employees) this.empById[d.employees[k].id]=d.employees[k];
				
				this.restoreData(d.data);
				this.getEl().unmask();
			},
			failure:function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); }
		});
	};
	
	this.restoreData=function(d){
		Ext.getCmp('depWorkers').removeAll();
		this.empInList={};
		for(k in d) {
			this.empById[k].d=d[k];
			this.addEmployee(this.empById[k]);
		}
		this.filterEmpStore();
	};
	
	this.weekPicker = new Ext.util.WeekPicker({value:new Date()});
	this.weekPicker.on('select', function(){
		if (this.weekStart.format('Y-m-d') != this.weekPicker.value.format('Y-m-d')) {
			this.weekStart = this.weekPicker.value;
			// Dump(this.weekPicker.value.format('Y-m-d'));
			this.loadData();
		}
	},this);
	
	// this.empStore = new Ext.data.ArrayStore({fields:['id','name']});
	this.empStore = new Ext.data.JsonStore({fields:['id','name','position','sex','religion','age']});
	this.filterEmpStore=function(){
		this.empStore.filterBy(function(rec,id){ if (!this.empInList[id]) return true; }, this);
	};
	this.empGrid=new Ext.grid.GridPanel({
		title:'עובדים', store:this.empStore, autoExpandColumn:'name',
		columns:[{id:'name', dataIndex:'name', header:'שם', sortable:true}]
	});
	this.empGrid.on('rowdblclick', function(grid, row, e){
		this.addEmployee( this.empStore.getAt(row).data );
	}, this);
	
	
	this.addEmployee=function(emp){
		this.empInList[emp.id]=true;
		this.filterEmpStore();
		Ext.getCmp('depWorkers').add( new EmployeeWeek({dep:this, emp:emp}) );
		this.doLayout();
	};

	DepWorkWeek.superclass.constructor.call(this, {
		id:'deptab_'+this.d.id,
		title:this.d.name, layout:'border', border:false, closable:true, bodyStyle:'background-color:#dae6f4;', //, region:'center'
		items:[
			{id:'depWorkers', region:'center', border:false, autoScroll:true, bodyStyle:'padding:0 10px;', items:[
				//new EmployeeWeek({panel:this})
			]}
			,{region:'east', layout:'border', width:188, border:false, bodyStyle:'border-left:1px solid #99bbe8;', baseCls:'x-plain', items:[
				 {region:'north', height:205, border:false, baseCls:'x-plain', bodyStyle:'padding:5px;', items:this.weekPicker}
				,{region:'center', layout:'fit', border:false, baseCls:'x-plain', bodyStyle:'padding:5px;', items:this.empGrid}
			]}
		],
		tbar:[
			{text:'שמור', iconCls:'save', handler:this.saveData, scope:this}
			// ,'-', {text:'Add', iconCls:'', handler:this.addEmployee, scope:this}
		]
	});
	
	this.on('render', function(){
		this.loadData()
		Ext.getCmp('depWorkers').on('remove', function(ct,cp){
			delete(this.empInList[cp.emp.id]);
			this.filterEmpStore();
		},this);
	});
};
Ext.extend(DepWorkWeek, Ext.Panel);


// Ext.onReady(function() {Ext.QuickTips.init();
	// var viewport = new Ext.Viewport({layout:'border', items:new DepWorkWeek()});
// });
