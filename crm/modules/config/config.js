ConfigPan = function(d) {
	ConfigPan.superclass.constructor.call(this, {id:'ConfigPan', region:'center', html:configPanelHTML});
};
Ext.extend(ConfigPan, Ext.Panel);

Ms.config.run = function(cfg){
	var m=Ms.config;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		win = Crm.desktop.createWindow({width:900, height:600, layout:'border',	items:new ConfigPan()}, m);
	}
	win.show();
};