
Ext.grid.CheckColumn = function(config){
    Ext.apply(this, config);
    if(!this.id) this.id = Ext.id();
    this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown : function(e, t){
        if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            var record = this.grid.store.getAt(index);
			if (this.dataIndex=='w' && !record.data[this.dataIndex]) record.set('r', true);
			if (this.dataIndex=='d' && !record.data[this.dataIndex]){record.set('w', true); record.set('r', true);}
			if (this.dataIndex=='R' && !record.data[this.dataIndex]) record.set('r', true);
			if (this.dataIndex=='W' && !record.data[this.dataIndex]){record.set('r', true);record.set('w', true);record.set('R', true);}
			if (this.dataIndex=='D' && !record.data[this.dataIndex]){record.set('r', true);record.set('w', true);record.set('d', true); record.set('W', true); record.set('R', true);}
            record.set(this.dataIndex, !record.data[this.dataIndex]);
			permisonsChanged=true;
        }
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
    }
};


//Permisson Group window
GroupEditWin = function(cfg) {

	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain', labelWidth:65,
		items: [
			 {xtype:'textfield', name:'name', fieldLabel:'שם קבוצה', anchor:'95%'}
			,{xtype:'textarea', name:'notes', fieldLabel:'הערות', anchor:'95%'}
			,{xtype:'hidden', name:'id'}
		]
	});

	GroupEditWin.superclass.constructor.call(this, {
		title: ((cfg && cfg.data) ? 'קבוצת הרשאות חדשה':'עדכן קבוצת הרשאות'),
		iconCls:'group', width:430, modal:true,	resizable:false, layout:'fit',	plain:true,
		bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	if (cfg && cfg.data) this.form.form.loadRecord(cfg.data);
	
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		this.form.form.submit({
			url: ((cfg && cfg.url) ? cfg.url : '?m=groups/groups&f=edit'),
			waitTitle: 'אנא המתן...', waitMsg: 'טוען את הקובץ...',
			scope:this,
			success: function(r,o){if (cfg && cfg.callBack) cfg.callBack(); this.close();},
			failure: function(r,o){this.close(); ajRqErrors(o.result);}
		});
	}	
}
Ext.extend(GroupEditWin, Ext.Window, {});

GroupUsersGrid = function(cfg) {
	Ext.apply(this, cfg);

	this.changeGroup = function(group_id) {
		this.store.removeAll();
		if (!group_id) return;
		this.store.reload({params:{group_id:group_id}});
	}
	
	this.store = new Ext.data.JsonStore({
		url:'?m=groups/groups&f=userlist', totalProperty:'total', root:'data',
		fields:['id','name','username'],
		sortInfo:{field:'name', direction:'ASC'},
		remoteSort:true
	});


	this.nameRenderer = function(str, p, record){
		return "<span style='display:block;height:16px;padding-right:18px;background:url(/crm/skin/icons/1/user"+(record.json.sex==2?'-female':(record.json.sex==3?'_business':''))+".gif) no-repeat right top;'>" +this.hlStr(str)+ "</span>";
	}	
	
    this.columns = [
		{dataIndex:'username', header:'שם משתמש', width:140}, 
		{dataIndex:'name', id:'name', header:'שם', sortable:true}
	];
	
	GroupUsersGrid.superclass.constructor.call(this, {
		title:'רשימת משתמשים',id:'GroupUsersGrid',iconCls:'user',layout:'fit', width:500,
		enableColLock:false, loadMask:true, stripeRows:true, enableHdMenu: false,
		autoExpandColumn: 'name',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false})
		// ,tbar: [
			// { text:'הוסף משתמש', iconCls:'add', handler:function(){this.userAdd({})}, scope:this }
			// ,'-' ,{ text:'חסר משתמש', id:'delete', iconCls:'delete', handler:function(){this.delClientPromt();}, disabled:true, scope:this }
		// ]
    });

	// this.store.on('beforeload', function(store, options){this.store.removeAll();}, this);
	

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('delete').setDisabled(!sm.getCount());
	}, this);
	
	this.delClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:250, title:'למחוק משתמש?', msg:'<img src="/crm/skin/icons/1/user_del48.gif" align=left><br>האם ברצונך לבטל את המשתמש<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delClient(records); }
		});
	}
	
	this.delClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '/?m=company/employees_f&f=delete&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('משתמש בוטל','משתמש <b>'+d.data.name+'</b> בוטל בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
};
Ext.extend(GroupUsersGrid, Ext.grid.GridPanel);

GroupPermissionsPanel = function(cfg) {
	var permisonsChanged=false;
	
	this.changeGroup = function(group_id) {
		if (permisonsChanged) {
			Ext.MessageBox.show({
				width:250, icon:Ext.MessageBox.WARNING, title:'שינוי הרשאות', msg:'הרשאות השתנו!<br> האם ברצונך לשמור את השינויים?', buttons:Ext.MessageBox.YESNO, scope:this,
				fn: function(btn) {
					if(btn=='yes') this.saveGroupPermissions();
					this.store.removeAll();
					if (!group_id) {this.group_id=0; return;}
					this.group_id = group_id;
					this.store.load({params:{group_id:group_id}});
				}
			});
		} else {
			this.store.removeAll();
			if (!group_id) {this.group_id=0; return;}
			this.group_id = group_id;
			this.store.load({params:{group_id:group_id}});
		}
		permisonsChanged = false;
	}
	
    var record = Ext.data.Record.create([
   		{name:'title'},
     	{name:'r', type: 'bool'},
     	{name:'w', type: 'bool'},
     	{name:'d', type: 'bool'},
     	{name:'R', type: 'bool'},
     	{name:'W', type: 'bool'},
     	{name:'D', type: 'bool'},
     	{name:'_id', type:'int'},
     	{name:'_parent', type:'int'},
     	{name:'_level', type:'int'},
     	{name:'_is_leaf', type: 'bool'}
   	]);

    this.store = new Ext.ux.maximgb.tg.NestedSetStore({
		url:'?m=groups/groups&f=group_permissions', params:{group_id:this.group_id}, totalProperty:'total', root:'data',
		fields:['title','r','w','d','R','W','D','_id','_level','_lft','_rgt','_is_leaf'],
		reader: new Ext.data.JsonReader({id: '_id'}, record)
    });

    var columnsGroups = new Ext.ux.grid.ColumnHeaderGroup({rows: [[{header:' ', align:'right'},{header:'של משתמש', colspan:3, align:'center'},{header:'של משתמשים אחרים', colspan:3, align:'center'}]]});
	// var columnsGroups = new Ext.ux.grid.ColumnHeaderGroup({
        // rows: [
            // [
                // {},
                // {colspan: 3, header: 'Marks'},
                // {colspan: 3, header: 'Marks'}
            // ]
        // ]
    // });

	var rColumn = new Ext.grid.CheckColumn({header:"לראות", dataIndex:'r', width:42, css:'background-color:red'});
	var wColumn = new Ext.grid.CheckColumn({header:"לערוך", dataIndex:'w', width:42});
	var dColumn = new Ext.grid.CheckColumn({header:"למחוק", dataIndex:'d', width:42});
	
	var RColumn = new Ext.grid.CheckColumn({header:"לראות", dataIndex:'R', width:42});
	var WColumn = new Ext.grid.CheckColumn({header:"לערוך", dataIndex:'W', width:42});
	var DColumn = new Ext.grid.CheckColumn({header:"למחוק", dataIndex:'D', width:42});
	
    this.grid = new Ext.ux.maximgb.tg.EditorGridPanel({
		store:this.store, cls:'rtl',
		master_column_id:'title',
		autoExpandColumn: 'title',
		border:false, stripeRows:true,	
		plugins: [columnsGroups, rColumn,wColumn,dColumn,RColumn,WColumn,DColumn],
		columns: [
			{id:'title',  header:"מודול", dataIndex:'title'}
			,rColumn,wColumn,dColumn,RColumn,WColumn,DColumn
		],
		viewConfig: {enableRowBody: true}
    });
	
	this.saveGroupPermissions = function() {
		var store_data=[];
		this.store.each(function(r){store_data.push(r.data);});
		Ext.Ajax.request({
			url: '?m=groups/groups&f=save_permissions',
			params:{group_id:this.group_id,data:Ext.encode(store_data)},
			success: function() {permisonsChanged = false;},
			failure: function() {Ext.Msg.alert('Error', 'Request failed.');},
			scope: this
		});
	}

	GroupPermissionsPanel.superclass.constructor.call(this, {
		title:'הרשאות קבוצה',id:'GroupPermissionsPanel',iconCls:'grid',layout:'fit',border:false,
		items:this.grid,
		buttons:[{text:'שמור', handler:this.saveGroupPermissions, scope:this}]
    });
};
Ext.extend(GroupPermissionsPanel, Ext.Panel);

GroupsGrid = function(cfg) {
	Ext.apply(this, cfg);

	this.store = new Ext.data.JsonStore({
		url:'?m=groups/groups&f=list', totalProperty:'total', root:'data',
		fields:['id','name','notes'],
		sortInfo:{field:'name', direction:'ASC'},
		remoteSort:true
	});

	this.groupEditWin = function(d){var win=new GroupEditWin({data:d, callBack:function(){this.store.reload()}.createDelegate(this)}); win.show(); win.center();}

	this.nameRenderer = function(v, p, r){
		return "<span ext:qtip='"+Ext.util.Format.htmlEncode(r.data.notes)+"' style='display:block;height:16px;padding-right:18px;background:url(skin/icons/users.gif) no-repeat right top;'>" +v+ "</span>";
	}	
	
	GroupsGrid.superclass.constructor.call(this, {
		region:'east',
		layout:'fit',
		width:240,
		autoExpandColumn:'groupCol',
		enableColLock:false,loadMask:true,stripeRows:true,enableHdMenu: false,
		cm: new Ext.grid.ColumnModel([{dataIndex:'name', header:'שם', id:'groupCol', sortable:true, renderer:this.nameRenderer}]),
		selModel:new Ext.grid.RowSelectionModel({singleSelect:true}),
		tbar: [
			{ text:'קבוצה חדשה', iconCls:'add', handler:function(){this.groupEditWin({})}, scope:this }
			,{text:'עדכן',id:'group_edit', iconCls:'edit', handler:function(){this.groupEditWin(this.getSelectionModel().getSelected());}, disabled:true, scope:this }
			,{text:'מחק', id:'group_del', iconCls:'delete', handler:function(){this.delGroup();}, disabled:true, scope:this }
		]
    });

	this.store.on('beforeload', function(store, options){this.store.removeAll();}, this);
	
	// this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('group_edit').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('group_del').setDisabled(sm.getCount()!=1);
	}, this);
	
	// Click
	this.on('click', function(){
		Ext.getCmp('GroupUsersGrid').changeGroup(this.getSelectionModel().getSelected().data.id);
		Ext.getCmp('GroupPermissionsPanel').changeGroup(this.getSelectionModel().getSelected().data.id);
	});
	
	// doubleClick
	this.on('rowdblclick', function(grid){this.groupEditWin(grid.getSelectionModel().getSelected());});
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row);
		
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'עדכן קבוצה', iconCls:'edit', scope:this, handler:function(){this.groupEditWin(this.srData);} }
				,{text:'מחק קבוצה', iconCls:'delete', scope:this, handler:function(){this.delGroup(this.srData);} }
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.delGroup = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:250, title:'למחוק קבוצה?', msg:'האם ברצונך למחוק את הקבוצה?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) {
				if(btn=='yes') {
					var ids=[];
					Ext.each(records, function(r){ids.push(r.data.id);});

					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=groups/groups&f=delete&ids='+ids.join(','),
						success: function(r,o){
							d=Ext.decode(r.responseText);
							if (!ajRqErrors(d)) {
								Ext.each(records, function(d){
									row=this.store.indexOf(d);
									this.selModel.selectRow(row);
									this.view.getRow(row).style.border='1px solid red';
									Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
									
									Ext.getCmp('GroupUsersGrid').changeGroup();
									Ext.getCmp('GroupPermissionsPanel').changeGroup();

								},this);
							}
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				} 
			}
		});
	}
	
	this.on('render', function(){ this.store.load() });
};
Ext.extend(GroupsGrid, Ext.grid.GridPanel);


Ms.groups.run = function(cfg){
	var m=Ms.groups;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var groupTabs = new Ext.TabPanel({region:'center', activeTab:0, border:false, style:'padding-top:2px', defaults:{autoScroll:true}, enableTabScroll:true
			,items:[new GroupUsersGrid(), new GroupPermissionsPanel()]
		}); 

		win = Crm.desktop.createWindow({width:900, height:600, layout:'border',	items:[new GroupsGrid(), groupTabs]}, m);
	}
	win.show();
};