<?php
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }
// if ($SVARS['user_type']!=3 AND $SVARS['parent_id']) $group_id=$SVARS['parent_id']; else $group_id=$SVARS['user_id'];
// if (!$group_id) die('{success:false, msg:"group_id error"}');
// if ($SVARS['user_type']==10 OR $SVARS['user_type']==3) $is_adm=1;

$client_id=(int)$_REQUEST['client_id'];
$client_type=$_REQUEST['client_type'];
if (!$_GET['init']) $NO_SKIN=1;

if (function_exists("f_{$f}")) call_user_func("f_{$f}");
else die('{success:false, msg:"function name error"}');

//————————————————————————————————————————————————————————————————————————————————————
function f_clients_list() {
 global $SVARS, $group_id;

 	$start=(int)$_GET['start'];
 	$limit=(int)$_GET['limit'];
 	
 	$where="crm_clients.group_id=$group_id";
	 
	if ($_GET['query']) $_GET['query']=trim(sql_escape( $_GET['query']));	# decode UTF-8 (encodeURIComponent)	

	if ($_GET['query']) {
		$make_where_search = make_where_search('first_name,last_name',$_GET['query']);
		$where.=" AND ".$make_where_search;//search in user data
	}
 	
	$sql="SELECT client_id, TRIM(CONCAT(first_name,' ',last_name)) AS client, passport, DATE_FORMAT(birthday,'%d/%m/%y') AS birthday FROM crm_clients WHERE $where ORDER BY client LIMIT $start,$limit";
	$rows=sql2array($sql);
	
	if (is_array($rows)) {
		foreach ($rows as $key=>$row) {
			$responce.= ($responce?',':'')
			."{
				id:$row[client_id],
				client:'".sql_escape($row['client'])."',
				passport:'$row[passport]',
				birthday:'$row[birthday]'
			}";
		}
		$responce = $_GET['callback'].'({total:'.count($rows).',data:['.$responce.']});';
		echo $responce;
	} else echo $_GET['callback'].'({total:0,data:[]});';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_email_list() {
 global $CFG, $SVARS, $m, $a, $group_id, $deleted;
 	$client_contacts[]=array('name'=>'Slava Rozhnev','mail'=>'slava@bafi.co.il');
 	$client_contacts[]=array('name'=>'סלבה','mail'=>'slava@bafi.co.il');
 	//echo '{total:'.count($client_contacts).', data:'.array2json($client_contacts).',success:true}';

 	$start=(int)$_POST['start'];
 	$limit=(int)$_POST['limit'];
 	
	if ($_POST['query']) $_POST['query']=trim(sql_escape($_POST['query']));	# decode UTF-8 (encodeURIComponent)	

	if ($_POST['query']) {
		$where_client_search = " AND ".make_where_search('fname,lname,name,email',$_POST['query']);
		$where_user_search = " AND ".make_where_search('fname,lname,email',$_POST['query']);
		$where_group_search = " AND ".make_where_search('name,email',$_POST['query']);
		$where_phones_search = " AND ".make_where_search('user_phones.first_name,user_phones.last_name,user_phones.email',$_POST['query']);
	}
 	
	$sql="SELECT TRIM(CONCAT(fname,' ',lname,' ',name)) AS name, email FROM clients "
		."WHERE email<>'' $where_client_search ORDER BY name";
	$clients=sql2array($sql);

	$sql="SELECT TRIM(CONCAT(fname,' ',lname)) AS name, email FROM users "
		."WHERE email<>'' $where_user_search ORDER BY name";
	$agents=sql2array($sql);

	$sql="SELECT TRIM(CONCAT(user_phones.first_name,' ',user_phones.last_name)) AS name, user_phones.email FROM user_phones "//, phones_group.first_name
		."LEFT JOIN user_phones AS phones_group ON (phones_group.id=user_phones.parent_id) "
		."WHERE (phones_group.user_id={$SVARS['user']['id']} OR phones_group.user_id=0) AND user_phones.email<>'' $where_phones_search ORDER BY name";
	$contacts=sql2array($sql);
	
	$rows=array();
	if ($clients) $rows=array_merge($rows, $clients);
	if ($agents) $rows=array_merge($rows, $agents);
	if ($groups) $rows=array_merge($rows, $groups);
	if ($contacts) $rows=array_merge($rows, $contacts);
	
	//echo "<div dir=ltr align=left>Clients<pre>".print_r($clients,1)."</pre></div>";	
	//echo "<div dir=ltr align=left>Contacts<pre>".print_r($contacts,1)."</pre></div>";	
	//echo "<div dir=ltr align=left>Rows<pre>".print_r($rows,1)."</pre></div>";	

	if (is_array($rows)) {
		$rows=array_slice($rows, $start, $limit);
		echo '{success:true, total:'.count($rows).', data:'.array2json($rows).'}';
	}else
		echo '{success:true, total:0, data:[]};';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_list_load() {
 global $SVARS, $client_id, $client_type;
 
 	if (!$client_id OR !$client_type) die ('{success:false}');
	
	$_REQUEST['limit'] = (int)$_REQUEST['limit'] ? (int)$_REQUEST['limit'] : 25;
	$_REQUEST['start'] = (int)$_REQUEST['start'] ? (int)$_REQUEST['start'] : 0 ;
	
	if ($_REQUEST['type_select']=='phone' OR 
		$_REQUEST['type_select']=='meeting' OR 
		$_REQUEST['type_select']=='sms' OR 
		$_REQUEST['type_select']=='mail') 
		$contact_type_where="AND contacts.type=".quote($_REQUEST['type_select'])." ";
	
	elseif($_REQUEST['type_select']=='post') 
		$contact_type_where="AND (contacts.type='ragil' OR contacts.type='rashum' OR contacts.type='havila' OR contacts.type='express') ";
	
	else $contact_type_where='';
	
	if (!$_REQUEST['type_select'])  $_REQUEST['type_select']='all';
	
	if($_REQUEST['sort']) $order_by=" {$_REQUEST['sort']} {$_REQUEST['dir']} ";
	else $order_by=" date ASC ";
	
	if ($_REQUEST['type_select']=='all' OR ($_REQUEST['type_select']!='tasks' AND $_REQUEST['type_select']!='faxes') OR $_REQUEST['type_select']=='deleted') 
		$sql=
			"SELECT SQL_CALC_FOUND_ROWS contacts.id AS contact_id, user_id, date, importance, remind_date, '' AS end, agent_id, subject, contacts.type, contacts.notes, contacts.attach_ids, contacts.deleted "//, treated, remind_text, '' AS recipient_name, '' AS recipient_fax "
			."FROM contacts "
			// ."LEFT JOIN crm_clients ON crm_clients.client_id=contacts.client_id "
			// ."LEFT JOIN users ON users.id=contacts.user_id "
			// ."LEFT JOIN crm_agents ON crm_agents.agent_id=contacts.agent_id "
			."WHERE contacts.client_type='$client_type' "
			."AND contacts.client_id=$client_id "
			.$contact_type_where
			."AND contacts.deleted=".($_REQUEST['type_select']=='deleted' ? '1' : '0')
			.($_REQUEST['link']?" AND link='$_REQUEST[link]'":'');
	/*		
	if ($_REQUEST['type_select']=='all' OR $_REQUEST['type_select']=='tasks' OR $_REQUEST['type_select']=='deleted')
		$sql.=
			($sql? " UNION ":"")
			."SELECT id AS contact_id, user_id, start AS date, importance, remind AS remind_date, end, 0 AS agent_id, name AS subject, 'task' AS type, tasks.notes, 0 AS attach_ids, tasks.deleted, status AS treated, '' AS remind_text ,'' AS recipient_name, '' AS recipient_fax "
			."FROM tasks "
			// ."LEFT JOIN crm_clients ON crm_clients.client_id=tasks.client_id "
			// ."LEFT JOIN users ON users.id=tasks.user_id "
			."WHERE tasks.client_type='$client_type' "
			."AND tasks.client_id=$client_id "
			."AND tasks.deleted=".($_REQUEST['type_select']=='deleted' ? '1' : '0')
			.($_REQUEST['link']?" AND link='$_REQUEST[link]'":'');
			
	if ($_REQUEST['type_select']=='all' OR $_REQUEST['type_select']=='faxes' OR $_REQUEST['type_select']=='deleted')
		$sql.=
			($sql? " UNION ":"")
			."SELECT id AS contact_id, user_id, date, '' AS importance, '' AS remind_date, '' AS end, 0 AS agent_id, subject AS subject, 'fax_out' AS type, text AS notes, attach_id AS attach_ids, 0 AS deleted, status AS treated, '' AS remind_text, recipient_name, recipient_fax "
			."FROM fax_out "
			// ."LEFT JOIN crm_clients ON crm_clients.client_id=fax_out.link_id "
			// ."LEFT JOIN users ON users.id=fax_out.user_id "
			."WHERE fax_out.link_to='$client_type' "
			."AND fax_out.link_id=$client_id "
			.($_REQUEST['type_select']=='deleted' ? "AND fax_out.deleted!='0000-00-00'" : "AND fax_out.deleted='0000-00-00'")
			// .($_REQUEST['link']?" AND link='$_REQUEST[link]'":'')
			;
	*/		
	echo "/*$sql*/";

	$sql.=($sql? " ORDER BY $order_by "."LIMIT $_REQUEST[start],$_REQUEST[limit]": '');
		
	$client_contacts=sql2array($sql);
	
	$rowsCount=count($client_contacts);
	
	if ($rowsCount==$_REQUEST['limit']) $rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if ($client_contacts) foreach ($client_contacts as $key=>$contact) {
	
		$client_contacts[$key]['date']=str2time($contact['date'],'d/m/Y H:i');
		$client_contacts[$key]['end']=str2time($contact['end'],'d/m/Y H:i');
		$client_contacts[$key]['remind_date']=str2time($contact['remind_date'],'d/m/Y H:i');
		if( $client_contacts[$key]['recipient_fax']) $client_contacts[$key]['subject'] = $client_contacts[$key]['subject'].'(אל: '.$client_contacts[$key]['recipient_fax'].($client_contacts[$key]['recipient_name']? ' לכבוד: '.$client_contacts[$key]['recipient_name'] : '').')';
		
		if ($client_contacts[$key]['type']!='task' AND $client_contacts[$key]['remind_date'] AND !$contact['treated'] AND !$contact['deleted']) $client_contacts[$key]['treated']=-1;
		
		if ($client_contacts[$key]['attach_ids'] AND $client_contacts[$key]['type']=='mail') {
			$attach_names = sql2array("SELECT file_name FROM files WHERE id IN (".$client_contacts[$key]['attach_ids'].")",'','file_name');
			if ($attach_names) $client_contacts[$key]['attach_names']= implode(",", $attach_names);
		}
		
		if ($contact['deleted']) $delCount++;
		
	}else $client_contacts=array();
	
	if($_REQUEST['print_rep']){
		
		if(!$client_contacts) { echo "אין מידע להצגה"; return;}
		
		header("Content-Type: text/html;charset=windows-1255");
		

		echo "<thml><head>".printStyle()."</head>
		<meta content='text/html; charset=windows-1255' http-equiv='Content-Type' />
		<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
		
		echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
		echo "<center><h1>דו''ח טיפול שוטף</h1></center><br>";

		echo "<table width=100% class=t_list>";
		echo '<tr><th width=8>#</th>'
			.'<th width=50>סטאטוס</th>'
			.'<th>תאריך</th>'
			.'<th>יעד סיום</th>'
			.'<th>תזקורת</th>'
			.'<th>סוכן</th>'
			.'<th>חברה</th>'
			.'<th>נושא</th>'
			.'<th>הערות</th>'
			.'<th>משתמש</th></tr>';

		foreach ($client_contacts as $k=>$row) {

			
			if (!$row['recipient_fax']){
				if ($row['treated']==-1) $row['status'] = 'טרם טופל';
				elseif ($row['treated']==1) $row['status'] = 'טופל';
				elseif ($row['treated']==3) $row['status'] = 'הודעה בלבד';
			}
			else{
				if ($row['status']==1) $row['status'] = 'ממתין';
				elseif ($row['status']==2) $row['status'] = 'נשלח בהצלחה';
				elseif ($row['status']==3) $row['status'] = 'נכשל';
			}
			
			echo "<tr>";
			
			echo "<td><div>".($k+1)."</div></td>"
				."<td style='font-size:8px'>$row[status]</td>"
				."<td>".str2time($row['date'],'d/m/Y H:i')."</td>"
				."<td>".str2time($row['end'],'d/m/Y H:i')."</td>"
				."<td>".str2time($row['remind_date'],'d/m/Y H:i')."</td>"
				."<td>$row[agent]</td>"
				."<td>$row[firm]</td>"
				. "<td>$row[subject]</td>"
				."<td>$row[notes]</td>"
				."<td>$row[user]</td>";
			
			echo "</tr>";
			

		}
		echo "</table>"; 
		echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
		echo "</body></html>";
	}
	else echo '{total:'.$rowsCount.', delCount:'.(int)$delCount.', data:'.array2json($client_contacts).",signature:'".jsEscape($signature)."', success:true}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_add_contact() {
 global $SVARS, $CFG, $client_id, $client_type;
	$id=(int)$_POST['id'];
 	if (!$client_id OR !$client_type) {echo "{success:false, msg:'!$client_id OR !$client_type'}"; return;}
	
	# decode UTF-8 (encodeURIComponent)
	if ($_POST['date'])  $_POST['date'] = str2time($_POST['date'], 'Y-m-d H:i:s'); else $_POST['date']='NULL';
	if ($_POST['remind_date']) $_POST['remind_date']="'".str2time($_POST['remind_date'], 'Y-m-d H:i:s')."'"; else $_POST['remind_date']='NULL';
	
	if ($_POST['emails']) $type='mail';
	elseif ($_POST['tels']) $type='sms';
	elseif ($_POST['type']=='meeting') $type='meeting';
	else $type='phone';

	if ($type=='sms') {	# SMS
		if (!$_POST['tels'] OR !$_POST['notes']) die('{success:false}');
		
		include_once '../inc/sms.php';
		$send_sms = send_sms($_POST['tels'], $_POST['notes'], $_POST['originator']);
		if (!is_array($send_sms)) die("{success:false, msg:'".jsEscape($send_sms)."'}");
		list($send_success,$send_fail) = $send_sms;
		
		$_POST['subject'] = 'SMS אל '.implode(', ', $send_success);
		
	}elseif ($type=='mail' AND !$id) {
		include_once "../inc/mail/sendmail.php";
		$CFG['debug']=0;
		
		$from=sql2array("SELECT CONCAT(fname,' ',lname) as fullname, email, signature FROM users WHERE id=".$SVARS['user']['id'],'','',1);
		
		if ($from['email']) $replyto=$from['email'];
		else die ("{success:false, msg:'נא להגדיר כתובת דוא\"ל שלך!'}");
		
		if($from['signature']) $signature=$from['signature'];
		
		if ($_POST['attach']) {
			if (!ereg('^[0-9]+(,[0-9]+)*$',$_POST['attach'])) die('{success:false, msg:"wrong ids"}');
			$files=sql2array("SELECT * FROM files WHERE id IN($_POST[attach])");
			foreach($files as $f) {
				$file_path="../../crm-files/$SVARS[cid]/".substr($f['id'], -2)."/$f[id]".($f['file_ext']?".$f[file_ext]":'');
				if (!is_file($file_path)) die('{success:false, msg:"File not found!"}');
				$attach_files[]=array(realpath($file_path), $f['file_name']);				
			}
		}

		$mail_html="<div dir=rtl align=right style='font-family:Tahoma,Arial,Helvetica;'>$_POST[notes]</div>";
		
		if (send_mail($_POST['emails'], $_POST['them'], $mail_html, array('from'=>"$from[fullname] ($replyto) <support@bafi.co.il>", 'replyto'=>"$from[fullname] <$replyto>", 'files'=>$attach_files)) ) {
			if (!$id) $_POST['them']="$_POST[them] (אל: $_POST[emails])";
		}else die('{success:false, msg:"Error sending email"}');	
	}
	
	if ($id>0) $sql="UPDATE contacts SET client_id=$client_id, link='$_POST[link]', client_type='$_POST[client_type]', user_id={$SVARS['user']['id']}, date='$_POST[date]', importance='$_POST[importance]', agent_id='$_POST[agent_id]', subject=".quote($_POST['subject']).", remind_date=$_POST[remind_date], remind_text=".quote($_POST['remind_text']).", notes=".quote($_POST['notes'])." WHERE id=$id AND client_id=$client_id";	
	else $sql="INSERT INTO contacts SET client_id=$client_id, link='$_POST[link]', client_type='$_POST[client_type]', user_id={$SVARS['user']['id']}, type='$type', date='$_POST[date]', importance='$_POST[importance]', agent_id='$_POST[agent_id]', subject=".quote($_POST['subject']).", remind_date=$_POST[remind_date], remind_text=".quote($_POST['remind_text']).", notes=".quote($_POST['notes']);
	
	if (runsql($sql)) echo '{success:true}'; else echo '{success:false}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_del_contact() {
 global $SVARS, $client_id;
	if (!$client_id OR (!$_GET['contact_ids'] AND !$_GET['task_ids'] AND !$_GET['fax_out_ids'])) {echo '{success:false}'; return;}

	$sql1 = ($_GET['contact_ids'] ? "UPDATE contacts SET deleted=1 WHERE client_id=$client_id AND id IN ($_GET[contact_ids])":'');
	if ($sql1) $flag1=runsql($sql1);
	
	$sql2 = ($_GET['task_ids']    ? "UPDATE tasks SET deleted=1 WHERE client_id=$client_id AND id IN ($_GET[task_ids])" : '');
	if ($sql2) $flag2=runsql($sql2);
	
	$sql3 = ($_GET['fax_out_ids']    ? "UPDATE fax_out SET deleted='".date('Y-m-d')."' WHERE link_id=$client_id AND id IN ($_GET[fax_out_ids])" : '');
	if ($sql3) $flag3=runsql($sql3);

	if ($flag1 OR $flag2 OR $flag3) echo '{success:true}'; else echo '{success:false}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_change_status() {
 global $SVARS, $client_id, $client_type;
 	if (!$client_id OR !$_GET['contact_id']) {echo '{success:false}'; return;}
	$sql="UPDATE contacts SET treated='".($_GET['treated']==-1?'1':'0')."' WHERE client_id=$client_id AND id=$_GET[contact_id]";
	if (runsql($sql)) echo '{success:true, total:'.mysql_affected_rows().'}'; else echo '{success:false}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_rest_contact() {
 global $SVARS, $client_id;
	if (!$client_id OR (!$_GET['contact_ids'] AND !$_GET['task_ids'] AND !$_GET['fax_out_ids'])) {echo '{success:false}'; return;}

	$sql1 = ($_GET['contact_ids'] ? "UPDATE contacts SET deleted=0  WHERE client_id=$client_id AND id IN ($_GET[contact_ids])":'');
	if ($sql1) $flag1=runsql($sql1);
	
	$sql2 = ($_GET['task_ids']    ? "UPDATE tasks	 SET deleted=0  WHERE client_id=$client_id AND id IN ($_GET[task_ids])" : '');
	if ($sql2) $flag2=runsql($sql2);
	
	$sql3 = ($_GET['fax_out_ids'] ? "UPDATE fax_out  SET deleted='' WHERE link_id = $client_id AND id IN ($_GET[fax_out_ids])" : '');
	if ($sql3) $flag3=runsql($sql3);

	if ($flag1 OR $flag2 OR $flag3) echo '{success:true}'; else echo '{success:false}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_erase_contact() {
 global $SVARS, $client_id;
 	if (!$client_id) {echo '{success:false}'; return;}
	
	$sql1="DELETE FROM contacts	WHERE client_id=$client_id AND deleted=1";
	$sql2="DELETE FROM tasks 	WHERE client_id=$client_id AND deleted=1";
	$sql3="DELETE FROM fax_out	WHERE link_to='client' AND link_id=$client_id AND deleted!=''";

	if (runsql($sql1) AND runsql($sql2) AND runsql($sql3)) echo '{success:true}'; else echo '{success:false}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_get_adr() {
 global $SVARS, $prms_groups;
	$r=sql2array("SELECT city0, adr0, zip0 FROM clients WHERE group_id IN ($prms_groups) AND client_id=$_REQUEST[client_id]",'','',1);
	if (trim($r["city0"]) OR $r["zip0"] OR trim($r["adr0"])) {
		$a=$r["city0"];
		if ($r["adr0"])  $a.=($a?', ':'').$r["adr0"];
		if ($r["zip0"])  $a.=($a?', ':'').$r["zip0"];
		echo "{success:true, adr:'".jsEscape($a)."'}";
	} else echo "{success:true, adr:''}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_add_post() {
  global $SVARS, $group_id, $prms_groups;
 
	$contact_id = (int)$_REQUEST['contact_id'];
	$client_id = (int)$_REQUEST['client_id'];
	
	if ($client_id AND !$_REQUEST['receiver']) $_REQUEST['receiver']=sql2array("SELECT TRIM(CONCAT(lname,' ',fname,' ',name)) AS client_name FROM clients WHERE id=$client_id",'','client_name',1);

	$sql=($contact_id ? 'UPDATE':"INSERT INTO")
		." contacts SET client_id=$client_id"
		.($client_id ?  ", client_type='client'":'')
		//.", user_id=".$SVARS['user_id']
		.", user_id=".(int)$_REQUEST['user_id']
		.", receiver=".quote($_REQUEST['receiver'])
		.", adr=".quote($_REQUEST['adr'])
		.", type=".quote($_REQUEST['type'])
		.", date='".str2time($_REQUEST['date'],'Y-m-d H:i')."'" 
		.", subject=".quote($_REQUEST['subject'])
		.", notes=".quote($_REQUEST['notes'])
		.($contact_id ? " WHERE id=$contact_id":'');
	
	if (runsql($sql)) {
		if (!$contact_id) $contact_id = mysql_insert_id(); 
		echo "{success:true, data:{contact_id:$contact_id}}";
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_signature() {
 global $SVARS;
	if ($_REQUEST['a']=='save') {
		$sql = "UPDATE users SET signature=".quote($_REQUEST['htmlCode'])." WHERE id=".$SVARS['user']['id'];
		if (runsql($sql)) echo "{success:true}"; else echo '{success:false}';
	}
	elseif ($_REQUEST['a']=='load') {
		$signature=sql2array("SELECT signature FROM users WHERE id=".$SVARS['user']['id'],'','',1);
		if ($signature) echo '{total:1,data:'.array2json($signature).',success:true}'; else  echo '{total:1,data:"",success:true}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_pattern() {
 global $SVARS, $group_id;
	$_REQUEST['name'] = iconv("UTF-8", "windows-1255", $_REQUEST['name']);
	
	if ($_REQUEST['a']=='save') {
		$id=sql2array("SELECT id FROM crm_letters WHERE name='".sql_escape($_REQUEST['name'])."' AND group_id=$group_id ORDER BY group_id DESC",'','id',1);
		if ($id) $sql = "UPDATE crm_letters SET text='".sql_escape(iconv("UTF-8", "windows-1255",$_REQUEST['htmlCode']))."', user_id=$SVARS[user_id] WHERE id=$id AND group_id=$group_id";
		else 	 $sql = "INSERT INTO crm_letters SET group_id=$group_id , name='".sql_escape($_REQUEST['name'])."', type='".sql_escape($_REQUEST['type'])."', text='".sql_escape(iconv("UTF-8", "windows-1255",$_REQUEST['htmlCode']))."', user_id=$SVARS[user_id]";
		if (runsql($sql)) echo "{success:true}"; else echo '{success:false}';
	
	}elseif ($_REQUEST['a']=='load') {
		$pattern=sql2array("SELECT * FROM patterns WHERE id=".(int)$_REQUEST['id'],'','',1);
		
		if ($_REQUEST['sms']){
			$pattern['text']=str_replace('&nbsp;',' ', $pattern['text']); 
			$pattern['text']=str_replace('<br>',' ', $pattern['text']); 
			$pattern['text']=str_replace('"','""', strip_tags($pattern['text'])); 
		}
		
		if ($pattern) echo '{total:1,data:'.array2json($pattern).',success:true}'; else  echo '{total:0,data:"",success:true}';
	}elseif ($_REQUEST['a']=='list') {
		$letter=sql2array("SELECT * FROM crm_letters WHERE group_id IN (0,$group_id) AND type <> 'client_content' ORDER BY group_id ASC",'name');
		if ($letter) {
		sort($letter);
			echo '{success:true, total:'.count($letter).', data:'.array2json($letter).'}'; 
		}else echo '{total:0, data:[]}';
	}elseif ($_REQUEST['a']=='delete') {
		if (runsql("DELETE FROM crm_letters WHERE id IN ($_REQUEST[ids])")) echo '{success:true}'; else  echo '{success:false}';
	}
}
?>