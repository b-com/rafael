// General properties for mail panel
function showMsg(d,r,obj){

		if (r.success===false) { obj.body.update('Error: '+(r.msg?r.msg:'Error loading message.') ); return; }
		var html='<div id="mailheaders">', to=[], cc=[];//,terms=[];
		// html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">נושא:</span>  <b>'+Ext.util.Format.htmlEncode(d.subject)+'</b>&nbsp;</div>';
		html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">מאת:</span>  <b>'+d.from[0]+' <span dir=ltr>&lt'+d.from[1]+'&gt</span></b>&nbsp;</div>';
		html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">נושא:</span>  <b>'+d.subject+'</b>&nbsp;</div>';
		// if (d.from) html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">מאת:</span> <span'+(d.from[1]?' title="'+d.from[1]+'"':'')+'>'+Ext.util.Format.htmlEncode(d.from[0]||d.from[1])+'</span></div>';
		if (d.sent) html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">נשלח:</span> '+d.sent+'</div>';
		if (r.to) {
			// for(var i=0; i<r.to.length; i++) to.push( '<span dir=ltr title="'+Ext.util.Format.htmlEncode(r.to[i]['mailto'])+'">'+Ext.util.Format.htmlEncode(r.to[i]['name'])+'</span>' );
			for(var i=0; i<r.to.length; i++) to.push( '<span>'+r.to[i]['name']+' <span dir=ltr>&lt'+r.to[i]['mailto']+'&gt</spam></span>' );
			html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">אל:</span> '+to.join('; &nbsp;')+'</div>';
		}
		if (r.cc) {
			// for(var i=0; i<r.cc.length; i++) cc.push( '<span dir=ltr title="'+r.cc[i]['mailto']+'">'+(r.cc[i]['name']?r.cc[i]['name']:r.cc[i]['mailto'])+'</span>' );
			for(var i=0; i<r.cc.length; i++) cc.push( '<span>'+r.cc[i]['name']+' <span dir=ltr>&lt'+r.cc[i]['mailto']+'&gt</spam></span>' );
			html+='<div><span style="color:#2865b7; width:40px; display:inline-block;">Cc:</span> '+cc.join('; &nbsp;')+'</div>';
		}
		html+='<div id=attachmentsTb_'+d.account+''+d.uid+'_'+d.type+'></div>';
		
		html='<div dir=rtl style="padding:3px; line-height:16px; background-color:#f3f7fd; border-bottom:3px double #99bbe8">'+html+'</div>';
		// if (html) html+='<div style="padding:10px">'+r.html+'</div>';
		if (html) html=html+'</div><iframe name="mailframe" id="mailframe'+d.type+d.uid+'" frameborder="0" scrolling="auto" width="100%" marginwidth="0" marginheight="0" src="?m=mail_new/mail_f&f=load_msg_html&account='+d.account+'&folder='+encodeURIComponent(d.folder)+'&uid='+d.uid+'"></iframe>';
	
	
	if (html && obj.q) { // highlight
		  obj.q=obj.q.trim();
		  var newText = "";
		  highlightStartTag = "<font style='color:blue; background-color:yellow;'>";
		  highlightEndTag = "</font>";
		  var i = -1;
		  var lcSearchTerm = obj.q.toLowerCase();
		  var lcBodyText = html.toLowerCase();
		  var bodyText = html;
		  var searchTerm = obj.q;
		  while (bodyText.length > 0) {
			i = lcBodyText.indexOf(lcSearchTerm, i+1);
			if (i < 0) {
			  newText += bodyText;
			  bodyText = "";
			} else {
			  if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
				if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
				  newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
				  bodyText = bodyText.substr(i + searchTerm.length);
				  lcBodyText = bodyText.toLowerCase();
				  i = -1;
				}
			  }
			}
		  }
		
		html = newText;			
				
	}
	obj.body.update(html);
	
	// attachments bar
	if (r.attachments) {
		var at, pid = null;
		var items=['<span style="color:#2865b7;"><img src="skin/icons/bmail/mail-attachment.gif" align=absmiddle> קבצים מצורפים:</span>'];
		for(var i=0; i<r.attachments.length; i++) {
			at=r.attachments[i];
			at.filename = at.name.substr( 0, at.name.lastIndexOf( '.' ) );
			var menu=new Ext.menu.Menu({width:110, items:[
				{text:'פתח', iconCls:'preview', scope:this, handler:function(){ window.open('?m=mail_new/get&account='+encodeURIComponent(d.account)+'&folder='+encodeURIComponent(d.folder)+'&uid='+d.uid+'&pid='+pid, '_blank');} }
				,{text:'שמור', iconCls:'save', scope:this, handler:function(){window.open('?m=mail_new/get&download=1&account='+d.account+'&folder='+d.folder+'&uid='+d.uid+'&pid='+pid, '_blank');} }
			]});
			items.push({menu:menu, handler:function(b){pid=b.pid;}, text:Ext.util.Format.htmlEncode(at['name'])+' ('+readable_size(r.attachments[i]['size'])+')', pid:at['pid'], iconCls:file_cls(at['name'])});
			items.push('-');
		}
		// for(var i=0; i<r.attachments.length; i++) att.push( '<a href="?m=mail_new/get&folder='+folder+'&uid='+d.uid+'&pid='+r.attachments[i]['pid']+'">'+Ext.util.Format.htmlEncode(r.attachments[i]['name'])+'</a>' +' ('+readable_size(r.attachments[i]['size'])+')' );
		// html+='<div style="color:gray"><span style="color:#2865b7;"><img src="skin/icons/1/mail-attachment.gif" align=absmiddle> קבצים מצורפים:</span> <span dir=ltr>'+att.join(',&nbsp; ')+'</span></div>';
		
		new Ext.Toolbar({
			renderTo:'attachmentsTb_'+d.account+''+d.uid+'_'+d.type,
			enableOverflow:true,
			items:items
		});
	}
	var objHeight = 0;
	if (d.type == 'full') {
		objHeight=$('big_read_p_'+d.account+''+d.uid).parentNode.clientHeight-$('mailheaders').clientHeight-99;
		if (r.attachments) objHeight=objHeight-18;
	} else objHeight =obj.body.dom.clientHeight-$('mailheaders').clientHeight-12;
	$('mailframe'+d.type+d.uid).style.height=objHeight+'px';
}
