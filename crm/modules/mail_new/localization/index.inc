<?php

/*
 +-----------------------------------------------------------------------+
 | program/localization/index.inc                                        |
 |                                                                       |
 | This file is part of the Roundcube Webmail client                     |
 | Copyright (C) 2005-2009, Roundcube Dev. - Switzerland                 |
 | Licensed under the GNU GPL                                            |
 |                                                                       |
 | PURPOSE:                                                              |
 |   Provide a centralized location table                                |
 |   for keeping track of available languages                            |
 |                                                                       |
 +-----------------------------------------------------------------------+
 | Author: Thomas Bruederli <roundcube@gmail.com>                        |
 +-----------------------------------------------------------------------+

 $Id: index.inc 3989 2010-09-25 13:03:53Z alec $

*/

// langage codes according to ISO 639-1
// country codes according to ISO 3166-1 (Alpha-2)
// original names from http://www.mediaglyphs.org/mg/p/langnames.html

$rcube_languages = array(
  'sq_AL' => 'Albanian (Shqip)',
  'ar_SA' => 'Arabic (״§„״¹״±״¨״©)',
  'hy_AM' => 'Armenian (ױ€ױ¡ױµױ¥ײ€ױ¥ױ¶)',
  'ast'	  => 'Asturiana (Asturianu)',
  'az_AZ' => 'Azerbaijani (Azֹ™rbaycanca)',
  'eu_ES' => 'Basque (Euskara)',
  'bn_BD' => 'Bengali (א¦¬א¦¾א¦‚א¦²א¦¾)',
  'bs_BA' => 'Bosnian (Boֵ¡njaִki)',
  'br'	  => 'Breton (Brezhoneg)',
  'bg_BG' => 'Bulgarian (׀‘ׁ׀»׀³׀°ׁ€ׁ׀÷׀¸)',
  'ca_ES' => 'Catalan (Catalֳ )',
  'zh_CN' => 'Chinese (ח®€ה½“ה¸­ז–‡)',
  'zh_TW' => 'Chinese (ז­£י«”ה¸­ז–‡)',
  'hr_HR' => 'Croatian (Hrvatski)',
  'cs_CZ' => 'Czech (ִesky)',
  'da_DK' => 'Danish (Dansk)',
  'fa_AF' => 'Dari (ן»¯ן÷­ן÷©)',
  'de_DE' => 'Deutsch (Deutsch)',
  'de_CH' => 'Deutsch (Schweiz)',
  'nl_NL' => 'Dutch (Nederlands)',
  'en_GB' => 'English (GB)',
  'en_US' => 'English (US)',
  'eo'    => 'Esperanto',
  'et_EE' => 'Estonian (Eesti)',
  'fi_FI' => 'Finnish (Suomi)',
  'nl_BE' => 'Flemish (Vlaams)',
  'fr_FR' => 'French (Franֳ§ais)',
  'gl_ES' => 'Galician (Galego)',
  'ka_GE' => 'Georgian (בƒ¥בƒבƒ בƒ—בƒ£בƒבƒ˜)',
  'el_GR' => 'Greek (־•־»־»־·־½־¹־÷־¬)',
  'he_IL' => 'Hebrew (׳¢׳‘׳¨׳™׳×)',
  'hi_IN' => 'Hindi (א₪¹א₪¿א₪¨א₪¦א¥€)',
  'hu_HU' => 'Hungarian (Magyar)',
  'is_IS' => 'Icelandic (ֳslenska)',
  'id_ID' => 'Indonesian (Bahasa Indonesia)',
  'ga_IE' => 'Irish (Gaedhilge)',
  'it_IT' => 'Italian (Italiano)',
  'ja_JP' => 'Japanese (ז—¥ז¬ט×)',
  'kh_KH' => 'Khmer (ב—ב¶בב¶בב’ב˜ב‚ב)',
  'ko_KR' => 'Korean (ם•ךµ­ל–´)',
  'ku'    => 'Kurdish (Kurmancֳ®)',
  'lv_LV' => 'Latvian (Latvieֵ¡u)',
  'lt_LT' => 'Lithuanian (Lietuviֵ¡kai)',
  'mk_MK' => 'Macedonian (׀׀°׀÷׀µ׀´׀¾׀½ׁ׀÷׀¸)',
  'ms_MY' => 'Malay (Bahasa Melayu)',
  'mr_IN' => 'Marathi (א₪®א₪°א₪¾א₪ א¥€)',
  'ne_NP' => 'Nepali (א₪¨א¥‡א₪×א₪¾א₪²א¥€)',
  'nb_NO' => 'Norwegian (Bokmֳ¥l)',
  'nn_NO' => 'Norwegian (Nynorsk)',
  'ps' 	  => 'Pashto',
  'fa'    => 'Persian (״¯״±)',
  'pl_PL' => 'Polish (Polski)',
  'pt_BR' => 'Portuguese (Brasil)',
  'pt_PT' => 'Portuguese (Portuguֳ×s)',
  'ro_RO' => 'Romanian (Romֳ¢neֵte)',
  'ru_RU' => 'Russian (׀ ׁƒׁׁ׀÷׀¸׀¹)',
  'sr_CS' => 'Serbian (׀¡ׁ€׀¿ׁ׀÷׀¸)',
  'si_LK' => 'Sinhalese (א·ƒא·’א¶‚א·„א¶½)',
  'sk_SK' => 'Slovak (Slovenִina)',
  'sl_SI' => 'Slovenian (Slovenֵ¡ִina)',
  'es_AR' => 'Spanish (Argentina)',
  'es_ES' => 'Spanish (Espaֳ±ol)',
  'sv_SE' => 'Swedish (Svenska)',
  'ta_IN' => 'Tamil (א®₪א®®א®¿א®´א¯)',
  'th_TH' => 'Thai (א¹„א¸—א¸¢)',
  'tr_TR' => 'Turkish (Tֳ¼rkֳ§e)',
  'uk_UA' => 'Ukrainian (׀£׀÷ׁ€׀°ׁ—׀½ׁׁ׀÷׀°)',
  'vi_VN' => 'Vietnamese (Tiב÷¿ng Viב»‡t)',
  'cy_GB' => 'Welsh (Cymraeg)',
);

$rcube_language_aliases = array(
  'am' => 'hy_AM',
  'ar' => 'ar_SA',
  'az' => 'az_AZ',
  'bg' => 'bg_BG',
  'bs' => 'bs_BA',
  'ca' => 'ca_ES',
  'cn' => 'zh_CN',
  'cs' => 'cs_CZ',
  'cz' => 'cs_CZ',
  'da' => 'da_DK',
  'de' => 'de_DE',
  'ee' => 'et_EE',
  'el' => 'el_GR',
  'en' => 'en_US',
  'eu' => 'eu_ES',
  'ga' => 'ga_IE',
  'ge' => 'ka_GE',
  'gl' => 'gl_ES',
  'he' => 'he_IL',
  'hi' => 'hi_IN',
  'hr' => 'hr_HR',
  'ja' => 'ja_JP',
  'ko' => 'ko_KR',
  'kr' => 'ko_KR',
  'ne' => 'ne_NP',
  'no' => 'nn_NO',
  'ms' => 'ms_MY',
  'mr' => 'mr_IN',
  'pl' => 'pl_PL',
  'tw' => 'zh_TW',
  'si' => 'si_LK',
  'sl' => 'sl_SI',
  'sr' => 'sr_CS',
  'sr_cyrillic' => 'sr_CS',
  'sr_latin' => 'bs_BA',
  'se' => 'sv_SE',
  'sv' => 'sv_SE',
  'uk' => 'uk_UA',
  'vn' => 'vi_VN',
  'vi' => 'vi_VN',
  'zh' => 'zh_CN',
);

?>
