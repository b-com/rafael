﻿<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
 
 	$ln='he';
	
	$modules=sql2array("SELECT * FROM modules WHERE active=1 AND parent_id=0 ORDER BY id",'','',0,'rafael');
	
	if ($modules)  foreach($modules as $m) {
		$Ms[]=array('defaults'=>"{baseCls:'x-plain', cls:'rtl'}",
			'items'=>array(
				array(
					'html'=>"<div class='".($m['shortcutIconCls']? $m['shortcutIconCls']: 'demo-grid-shortcut')."' style='width:100%; height:48px; background-position:center top; background-repeat:no-repeat;' onClick='Ext.getCmp(\"mod$m[id]\").setValue(!Ext.getCmp(\"mod$m[id]\").getValue());'></div>",
					'style'=>'text-align:center;','baseCls'=>'x-plain','cls'=>'rtl'
				),
				array('xtype'=>'checkbox', 'checked'=>false, 'boxLabel'=>$m["title_$ln"],	'name'=>"mod[$m[id]]")
			)
		);
	}
	
	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end FROM users ORDER BY fullName",'id');
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;
	}

	#Load contacts patterns
	$patterns_ar=sql2array("SELECT id, type, name FROM patterns ORDER BY name",'id');
	if ($patterns_ar) foreach($patterns_ar as $id=>$pattern) {
		if ($pattern['type']=='sms') $smsPatterns.=($smsPatterns ? ',':'')."[$id,'".jsEscape($pattern['name'])."']";
	}
	
	$client_groups_ar=sql2array("SELECT * FROM client_groups",'id','name');
	
	if ($client_groups_ar) foreach($client_groups_ar as $id=>$client_group) $client_groups.=($client_groups ? ',':'')."[$id,'".jsEscape($client_group)."']";

	?>
	
	var Modules=<?=array2json($Ms)?>;
	
	var smsActive=<?=($SVARS['user']['sms_active'] ? 'true' : 'false')?>;
	var faxActive=<?=($SVARS['user']['fax_active'] ? 'true' : 'false')?>;
	var fax_numbers=[];

	var users=[<?=$users;?>];
	var agents=[<?=$agents;?>];
	var users_obj=<?=array2json($users_ar);?>;
	var	sms_originator='';
	var smsPatterns=[<?=$smsPatterns;?>];
	var client_groups=[<?=$client_groups;?>];
	
	Ext.ux.Loader.load([
			'skin/extjs/ux/superboxselect/superboxselect.css?<?=filemtime('skin/extjs/ux/superboxselect/superboxselect.css');?>',
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			'skin/extjs/ux/superboxselect/SuperBoxSelect.js?<?=filemtime('skin/extjs/ux/superboxselect/SuperBoxSelect.js');?>',
			'modules/common/contacts.js?<?=filemtime('modules/common/contacts.js');?>',
			'modules/common/files.js?<?=filemtime('modules/common/files.js');?>',
			'modules/users/users.js?<?=filemtime('modules/users/users.js');?>',
			'modules/common/accounting.js?<?=filemtime('modules/common/accounting.js');?>',
			'modules/companies/accounting.js?<?=filemtime('modules/companies/accounting.js');?>',
			'modules/companies/companies.js?<?=filemtime('modules/companies/companies.js');?>'
		], function(){Ms.companies.run();}
	);
	<?
}
//————————————————————————————————————————————————————————————————————————————————————
function f_list() {

	$where ="WHERE active=".(int)$_POST['active'];
	
	if ($_POST['q']) {
		$make_where_search=make_where_search('name,contact_person,city,street,homenum,tel0,tel1,fax,email,notes', $_POST['q']);
		if ($make_where_search) $where.=" AND ($make_where_search)";
	}

	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if ($_POST['sort']=='tz')  $order_by='passport';
	elseif ($_POST['sort']=='adr') $order_by='city';
	elseif ($_POST['sort']=='tel') $order_by='work_phone';
	elseif ($_POST['sort']=='fax') $order_by='fax';
	else $order_by='name';

	$sql="SELECT * FROM clients $where" 
		." ORDER BY $order_by".($_POST['dir']=='DESC'?' DESC':'')
		." LIMIT $_POST[start],$_POST[limit]";
	;
	
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true,'rafael');
	foreach($rows as $k=>$r) {
		if ($r['date_end']) $rows[$k]['date_end']=str2time($r['date_end'],'d/m/Y'); else $rows[$k]['date_end']='';
		$rows[$k]['modules'] = explode(',',$r['modules']);
	}
	echo '{total:"'.count($rows).'", data:'.array2json($rows).'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_faxes_list() {
	$sql="SELECT fax_number FROM fax_numbers WHERE company_id=".(int)$_REQUEST['id'];
	$rows=sql2array($sql);
	echo '{fax_numbers:'.($rows ? array2json($rows) : '[]').'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_load_data() {
 global $SVARS;
	$sql="SELECT * FROM clients WHERE id=$SVARS[cid]";
	$data=sql2array($sql,0,0,1,'rafael');
	echo '{data:'.($data ? array2json($data) : '[]').'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_edit() {
 global $SVARS;
 
	$id=(int)$_REQUEST['id'];
	
	$sql="clients SET name=".quote($_POST['name'])
		.", group_id=".(int)$_POST['croup_id']
		.", cnumber=".quote($_POST['cnumber'])
		.", tel0=".quote($_POST['tel0'])
		.", tel1=".quote($_POST['tel1'])
		.", fax=".quote($_POST['fax'])
		.", email=".quote($_POST['email'])
		.", url=".quote($_POST['url'])
		.", vat_period=".quote($_POST['vat_period'])
		.", notes=".quote($_POST['notes'])
		.(isset($_POST["active"])	? ($_POST["active"]=='true' ? ", active=1":", active=0"):'')
		.(isset($_POST["date_end"])	? ", date_end='".str2time($_POST['date_end'],'Y-m-d')."'":'')
		;

	if ($_POST['mod']) $sql.=", modules=".quote(implode(',',array_keys($_POST['mod'])));
	
	$n=0;
	for ($i=0; $i<3; $i++) if ($_POST["city$i"] OR $_POST["zip$i"] OR $_POST["adr$i"]) {$sql.=", adr_type$n=".(int)$_POST["adr_type$i"].", city$n=".quote($_POST["city$i"]).", zip$n=".quote($_POST["zip$i"]).", adr$n=".quote($_POST["adr$i"]); $n++;}
	for ($i=$n; $i<3; $i++) $sql.=", adr_type$i='', city$i='', zip$i='', adr$i=''";

	$n=0;
	for ($i=0; $i<5; $i++) if ($_POST["cperson$i"] OR $_POST["cp_tel$i"] OR $_POST["cp_email$i"]) {$sql.=", cperson$n=".quote($_POST["cperson$i"]).", cp_tel$n=".quote($_POST["cp_tel$i"]).", cp_email$n=".quote($_POST["cp_email$i"]); $n++;}
	for ($i=$n; $i<5; $i++) $sql.=", cperson$i='', cp_tel$i='', cp_email$i=''";

	$sql = ($id ? "UPDATE $sql WHERE id=$id" : "INSERT INTO $sql");
	
	if (runsql($sql,'rafael')) {
		if (!$id) $id = mysql_insert_id();
		
		if ($_POST['fax_numbers']) {
			runsql("DELETE FROM fax_numbers WHERE company_id=$id");
			$fax_numbers=explode(',',$_POST['fax_numbers']);
			// echo "/*<div dir=ltr align=left><pre>".print_r($fax_numbers,1)."</pre></div>*/";
			if ($fax_numbers) foreach($fax_numbers as $k=>$number) runsql("INSERT INTO fax_numbers SET company_id=$id, fax_number=".quote($number));
		}
		
		$name = jsEscape(trim($_POST['name']));
		echo "{id:$id, name:'$name', success:true}"; 
	}else echo '{success:false}';
}
?>