CompanyForm= function() {
	var adrStore = [[2,'חברה/עסק'],[4,'למשלוח דואר'],[0,'אחר']];
	var rep_periods1=[[1,'ינואר'],[2,'פברואר'],[3,'מרץ'],[4,'אפריל'],[5,'מאי'],[6,'יוני'],[7,'יולי'],[8,'אוגוסט'],[9,'ספטמבר'],[10,'אוקטובר'],[11,'נובמבר'],[12,'דצמבר']];
	var rep_periods2=[[1,'ינואר-פברואר'],[3,'מרץ-אפריל'],[5,'מאי-יוני'],[7,'יולי-אוגוסט'],[9,'ספטמבר-אוקטובר'],[11,'נובמבר-דצמבר']];

	CompanyForm.superclass.constructor.call(this, {layout:'column', items:[
	    {xtype:'panel', width: 560, bodyStyle:'padding:10px', border:false, labelWidth:25,
			items:[
				{layout:'table', layoutConfig:{columns:6}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl', style:'margin-top:5px'}, items:[
					{xtype:'label', text:'שם עסק:'},{name:'name', xtype:'textfield', tabIndex:1, width:280, colspan:3, allowBlank:false}
					,{xtype:'label', text:'מספר ח.פ.:', style:'padding-right:7px'},{name:'cnumber', xtype:'textfield', tabIndex:2, width:100}
					,{xtype:'label', text:'טל 1:'},{name:'tel0', xtype:'textfield', tabIndex:3, width:100}
					,{xtype:'label', text:'טל 2:', style:'padding-right:7px'},{name:'tel1', xtype:'textfield', tabIndex:4, width:100}
					,{xtype:'label', text:'פקס:', style:'padding-right:7px'},{name:'fax', xtype:'textfield', tabIndex:5, width:100}
					,{xtype:'label', text:'דוא"ל:'},{name:'email', xtype:'textfield', tabIndex:6, width:100, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
					,{xtype:'label', text:'אתר אינטרנט:', style:'padding-right:7px'},{name:'url', xtype:'textfield', tabIndex:7, width:'100%', colspan:3}
				]}
				,{xtype:'fieldset', title:'כתובת', cls:'rtl', style:'margin-top:5px; padding:2px', defaults:{style:'margin:3px'} ,autoHeight:true,
					items:[
						{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adr0', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{html:'סוג:'},{html:'עיר:'},{html:'מיקוד:'},{html:'כתובת/ת.ד.:'},
							{xtype:'combo', hiddenName:'adr_type0', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city0', xtype:'textfield', width:90},
							{name:'zip0', xtype:'textfield', width:60},
							{name:'adr0', xtype:'textfield', width:230}
						]}
						,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet1', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'combo', hiddenName:'adr_type1', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city1', xtype:'textfield', width:90},
							{name:'zip1', xtype:'textfield', width:60},
							{name:'adr1', xtype:'textfield', width:230}
						]}
						,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet2', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'combo', hiddenName:'adr_type2', width: 100, emptyText:'-בחר-', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city2', xtype:'textfield', width:90},
							{name:'zip2', xtype:'textfield', width:60},
							{name:'adr2', xtype:'textfield', width:230}
						]}
					]
				}
				,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', items:[
					{xtype:'label', text:'לוגו: ', style:'display:block; width:30px; text-align:right;'}
					,{xtype:'fileuploadfield', name:'firmLogo', emptyText:'בחר קובץ...', width:330}
				]}
				,{xtype:'fieldset', title:'הנהלת חשבונות', cls:'rtl', style:'margin-top:5px; padding:2px', defaults:{style:'margin:3px'},autoHeight:true,
					items:[
						{layout:'table', layoutConfig:{columns:8}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'label', text:'דיווח כל:', style:'display:block; padding-right:7px;'}
							,{xtype:'combo',store:[[1,'חודש'],[2,'חודשיים']],allowBlank:false,hiddenName:'vat_period',width:80,listClass:'rtl',triggerAction:'all',editable:false,forceSelection:true
								,listeners:{select:function(){
									if(this.find('hiddenName','vat_period')[0].getValue()==1) {
										this.find('hiddenName','current_vat_month')[0].store.loadData(rep_periods1);
										this.find('hiddenName','current_vat_month')[0].setValue(this.find('hiddenName','current_vat_month')[0].getValue());

									}
									else if(this.find('hiddenName','vat_period')[0].getValue()==2) {
										this.find('hiddenName','current_vat_month')[0].store.loadData(rep_periods2);
										this.find('hiddenName','current_vat_month')[0].setValue((this.find('hiddenName','current_vat_month')[0].getValue()%2==0?this.find('hiddenName','current_vat_month')[0].getValue()-1:this.find('hiddenName','current_vat_month')[0].getValue()));
									}
								},scope:this}
							}
							,{xtype:'label', text:'תקופת מע"מ נוכחית:'}		
							,{xtype:'combo', store:rep_periods1,hiddenName:'current_vat_month', width:120,mode:'local',listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false,editable:false}
							,{xtype:'combo', store:years,hiddenName:'current_vat_year',  width:50, mode:'local',listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false,editable:false}

							,{xtype:'label', text:'מקדמות מס:',style:'display:block; padding-right:14px;'}
							,{xtype:'numberfield', name:'income_tax', width:20}							
							,{xtype:'label', text:' %'}
							,{xtype:'label', html:'<br>תקופת מע"מ נוכחית מוגדרת כאן בתחילת העבודה באופן חד-פעמי.<br>בהמשך נקבעת ע"י נעילת תקופת דיווח במודול הנהלת חשבונות. ',colspan:8,style:'color:red;font-color:red;font-weight:bold'}
						]}
					]
				}
			]
		}
		,{xtype:'panel', columnWidth:1, width:235, baseCls:'x-plain', style:'margin:10px; align:center;', cls:'rtl', html:"<img id='firmLogo' src='"+firmLogo+"' width="+img_w+" height="+img_h+" style='-ms-interpolation-mode:bicubic;' align='left'>", listeners:{render:function(){window.setTimeout('$("firmLogo").src="'+firmLogo+'";',100);}}}
	]});
	
	this.on('render', function(){
			this.getEl().mask('...טוען');
			Ext.Ajax.request({
			url: "?m=companies/companies&f=load_data&id="+cid,
			success: function(r,o){
				r=Ext.decode(r.responseText);
				if (!ajRqErrors(r)) {
					this.form.setValues(r.data);
					if(r.data.current_vat_period!='0000-00-00') {
						this.find('hiddenName','current_vat_month')[0].setDisabled(1);
						this.find('hiddenName','current_vat_year')[0].setDisabled(1);
					}
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	}, this)
};

Ext.extend(CompanyForm, Ext.form.FormPanel);

Ms['config/company'].run = function(cfg){
	var m=Ms['config/company'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var companyForm = new CompanyForm();
		
		win = Crm.desktop.createWindow({
			width:700, height:400, items:companyForm
			,buttons:[
				{text:'שמור', handler:function(){win.submitForm();}}
				,{text:'ביטול', handler:function(){win.close();}}
			]
		}, m);
		
		win.submitForm = function(overwrite) {
			if(!companyForm.form.isValid()) return;
			companyForm.form.submit({
				url: "?m=companies/companies&f=edit&id="+cid,
				waitTitle:'אנא המתן...', waitMsg:'טוען...',
				scope:this,
				success: function(f,a){win.close();},
				failure: function(r,o){win.close(); ajRqErrors(o.result);}
			});
		}
		
	}
	win.show();
};
