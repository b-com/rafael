

/*
Ext.ux.Loader
http://www.sencha.com/forum/showthread.php?107796-Ext.ux.Loader-Load-js-css-Files-Dynamically&highlight=script+loader
usage example:
	var td = new Date();
	Ext.ux.Loader.load([
		'js/extjs/examples/ux/Spinner.js',
		'js/extjs/examples/ux/SpinnerField.js',
		'js/extjs/examples/ux/css/Spinner.css',
		'spinnerexample.js'
		],
		function() {    // callback when finished loading
			console.log('Loaded files successfully.  (loadtime: %dms)', td.getElapsed());
			Example.Spinner();
		},
		this  // scope
	);
*/
Ext.namespace('Ext.ux');
Ext.ux.Loader = Ext.apply({}, {
	// loaded:{},
	
	// Loads a given set of .js files. Calls the callback function when all files have been loaded
	// Set preserveOrder to true to ensure non-parallel loading of files if load order is important
	// fileList - Array of all files to load
	// callback - Callback to call after all files have been loaded
	// scope - The scope to call the callback in
	// preserveOrder - True to make files load in serial, one after the other (defaults to false)
	load: function(fileList, callback, scope, preserveOrder) {
		var scope       = scope || this,
			// head        = document.getElementsByTagName("head")[0],
			// fragment    = document.createDocumentFragment(),
			numFiles    = fileList.length,
			loadedFiles = 0,
			me          = this;
		
		// Loads a particular file from the fileList by index. This is used when preserving order
		var loadFileIndex = function(index) {
			// if (Ext.ux.Loader.loaded[fileList[index]]==true) return;
			head.appendChild(me.buildScriptTag(fileList[index], onFileLoaded));
			// Ext.ux.Loader.loaded[fileList[index]]=true;
		};
		
		// Callback function which is called after each file has been loaded. This calls the callback passed to load once the final file in the fileList has been loaded
		var onFileLoaded = function() {
			loadedFiles++;
			//if this was the last file, call the callback, otherwise load the next file
			if (numFiles==loadedFiles && typeof callback=='function') callback.call(scope);
			else if (preserveOrder===true) loadFileIndex(loadedFiles);
		};
		
		if (preserveOrder===true) loadFileIndex.call(this, 0);
		else { //load each file (most browsers will do this in parallel)
			Ext.each(fileList, function(file, index){
				// if (Ext.ux.Loader.loaded[file]!=true) {
					// console.log('Ext.ux.Loader: %s', file);
					// fragment.appendChild(this.buildScriptTag(file, onFileLoaded));
					document.getElementsByTagName("head")[0].appendChild(this.buildScriptTag(file, onFileLoaded));

					// fragment.appendChild(this.buildScriptTag(file, function(){Dump(file);}));
					// Ext.ux.Loader.loaded[file]=true;
				// }
			}, this);
			// head.appendChild(fragment);
		}
	},
	
	buildScriptTag: function(filename, callback) {
		if ((/\.css(\?[0-9]+)?$/.test(filename))) { // CSS
			// Dump(filename);
			var style=document.createElement('link'); style.rel='stylesheet'; style.type='text/css'; style.href=filename;
			callback();
			return style;
		}else {// Javascript
			var script=document.createElement('script'); script.type="text/javascript"; script.src=filename;
			if(script.readyState) {	//IE has a different way of handling <script> loads, so we need to check for it here
				script.onreadystatechange = function(){ if (script.readyState=="loaded" || script.readyState=="complete") { script.onreadystatechange=null; callback(); } };
			}else script.onload=callback;
			return script;
		}
	}
});

/*
var sms_originator='';
CmpLoad = function(config){ Ext.apply(this, config); };
CmpLoad = Ext.extend(Ext.Container, {
	initComponent: function() {
		CmpLoad.superclass.initComponent.call(this, arguments);
		this.on('render', this.load, this);
	},
	load:function(){
		// window.setTimeout(function(){this.el.mask('...����')}.createDelegate(this), 1);
		// this.add({html:'aaa'});
		Ext.ux.Loader.load(['modules/crm/sms.js'],
			function() {
				// console.log('Loaded files successfully.  (loadtime: %dms)', td.getElapsed());
				// var win=new BulkSmsWin({closable:true,modal:false}).show();
				// win.show();
				// Dump('loaded');
				//this.doLayout();
			},this
		);
	}
});
*/

/*	unload JS
	this.unloadJS=function(file,path){  
		path=path?path:'';  
		var script = document.getElementsByTagName("script");  
		var i,j;  
		for(i=0,j=script.length;i<j;i++){    
			if (script[i].getAttribute("src") == (path+file)){    
				var t=Ext.get(script[i]);  
				t.remove();  
				for(i=0,j=this.JS.length;i<j;i++){  
					if(this.JS[i].file==file && this.JS[i].path==path){  
						this.setJSStatus(file,false,path);  
					}  
				}  
				return;  
			}    
		}   
	}
	
	this.chkJSLoaded=function(file,path){  
		path=path?path:'';  
		var i,j;  
		for(i=0,j=this.JS.length;i<j;i++){  
			if(this.JS[i].file==file && this.JS[i].path==path){  
				return this.JS[i].loaded;  
			}  
		}  
		return false;  
	} 
*/

// Ext.ux.JSLoader = function(options) {
	// Dump(1);
	// Ext.ux.JSLoader.scripts[++Ext.ux.JSLoader.index] = {
		// url: options.url,
		// success: true,
		// options: options,
		// onLoad: options.onLoad || Ext.emptyFn,
		// onError: options.onError || Ext.ux.JSLoader.stdError
	// };
  
  // Ext.Ajax.request({
    // url: options.url,
    // scriptIndex: Ext.ux.JSLoader.index,
    // success: function(response, options) {
      // var script = 'Ext.ux.JSLoader.scripts[' + options.scriptIndex + ']';
      // window.setTimeout('try { ' + response.responseText + ' } catch(e) { '+script+'.success = false; '+script+'.onError('+script+'.options, e); }; if ('+script+'.success) '+script+'.onLoad('+script+'.options);', 0);
    // },
    // failure: function(response, options) {
      // var script = Ext.ux.JSLoader.scripts[options.scriptIndex];
      // script.success = false;
      // script.onError(script.options, response.status);
    // }
  // });

// };
// Ext.ux.JSLoader.index = 0;
// Ext.ux.JSLoader.scripts = [];

