<?


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function frm_load_vals(&$FORM, $primary_id) {
	if (!$primary_id) return;

	#set $fields_list
	foreach ($FORM['fields'] as $fld=>$row) if ($row['db'] OR $row['load']==1) {
		if ($fields_list) $fields_list.=',';
		if ($row['db_name']) $fields_list.=$row['db_name'];
		elseif ($row['type']=='date' OR $row['type']=='datetime') {
			if (!$row['format']) $row['format'] = ($row['type']=='datetime') ? '%d/%m/%Y %H:%i' : '%d/%m/%Y';	# sed default date format
			$fields_list.="IF ($fld, DATE_FORMAT($fld,'$row[format]'), '') AS $fld";
		}elseif ($row['db']==2) $fields_list.=$FORM['table2'].'.'.$fld;
		else $fields_list.=$fld;
		//$fields_list.=($fields_list?',':'').($row['db_name'] ? $row['db_name'] : $fld);
	}
	
	$sql="SELECT $fields_list FROM $FORM[table] ".($FORM['join']?"$FORM[join]":"")." WHERE  $FORM[table].$FORM[primary]='$primary_id'";
	$row=sql2array($sql,'','',1);
	foreach ($FORM['fields'] as $key=>$value) {
		if ($FORM['fields'][$key]['db'] OR $FORM['fields'][$key]['load']) 
			$FORM['fields'][$key]['value']=$row[$key];
	}
 return $row;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function frm_prepere_sql(&$FORM, $action, $t=1) {//$t - table id
	$query='';
	$fieldsar=$FORM['fields'];

	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		
		# convert date format
		if ($fld[0]=='date' AND ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4})", $fld['value'], $regs) ) {
			if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
			$fld['value']="$regs[3]-$regs[2]-$regs[1]";
		}
		# convert datetime format
		if ($fld[0]=='datetime' AND ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4}) ([0-9]{1,2}):([0-9]{1,2})", $fld['value'], $regs) ) {
			if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
			$fld['value']="$regs[3]-$regs[2]-$regs[1] $regs[4]:$regs[5]";
		}		
		if (!$fld['name']) $fld['name']=$key;	# set name
		if ($fld['db']==$t) $query.= ($query?', ':'') . "$fld[name]='".sql_escape($fld['value'])."'";
	}

	if ($query!='') {	#add insert/update to query.
		$table=$FORM['table'.($t!=1?$t:'')];
		if ($action=='INSERT') $query="INSERT INTO $table SET ".$query;
		elseif ($action=='UPDATE') $query="UPDATE $table SET ".$query." WHERE $FORM[primary]='".$FORM['fields'][$FORM['primary']]['value']."'";
		else { $FORM['error_text']="prepere_sql_FORM ERROR: Action name error."; return false; }
	}
 return $query;
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function frm_set_value(&$fld, $method='') {
	if ( isset($fld['value']) ) return $fld['value'];
	elseif ( $fld[0]=='upload' AND $_FILES[$fld['name']]['name']!='' ) $fld['value']=$_FILES[$fld['name']];
	elseif ( $method=='GET' AND isset($_GET[$fld['name']]) ) $fld['value']=$_GET[$fld['name']];
	elseif ( isset($_POST[$fld['name']]) ) $fld['value']=$_POST[$fld['name']];
	
	if ($fld['trim']!==0 AND !$fld['notrim'] AND !is_array($fld['value'])) $fld['value']=trim($fld['value']);
 return $fld['value'];
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function frm_check(&$FORM) {	#takes config array and check form
 global $L_ERRORS;
	//echo "<pre>"; print_r($FORM); echo "</pre>";
	
	if (!$FORM['fields']) return;
	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		if (!$fld['name']) $fld['name']=$key;	# set name
		
		$fld['value'] = frm_set_value($fld, $FORM['method']);

		if ($fld['required'] AND $fld[0] AND $fld['value']=='') {
			if ($fld[0]=='select') $fld['error']=sprintf($L_ERRORS['please_select'], "<u><b>$fld[title]</b></u>");
			else $fld['error']=sprintf($L_ERRORS['required'], "<u><b>$fld[title]</b></u>");
		
		}elseif ($fld['value']!='') {
			if ($fld['minlength'] AND (strlen($fld['value']) < $fld['minlength']) ) $fld['error']=sprintf($L_ERRORS['minimum'], "<u><b>$fld[title]</b></u>", "<b>$fld[minlength]</b>");
			if ($fld['maxlength'] AND (strlen($fld['value']) > $fld['maxlength']) ) $fld['error']=sprintf($L_ERRORS['maximum'], "<u><b>$fld[title]</b></u>", "<b>$fld[maxlength]</b>");

			if ($fld['ereg'] AND !ereg($fld['ereg'], $fld[value]))
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['regexp'] AND !preg_match($fld['regexp'], $fld[value]))
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['ercheck']=="email" AND !eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $fld[value]) )	
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['ercheck']=="url" AND  !eregi("^((ht|f)tp://)?((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $fld[value]))
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['ercheck']=="date") {
				if ( ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4})", $fld['value'], $regs) ) {
					if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
					$day=$regs[1]; $month=$regs[2]; $year=$regs[3];
				}elseif ( ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $fld['value'], $regs) ) {
					$day=$regs[3]; $month=$regs[2]; $year=$regs[1];
				}else $date_error=1;
				
				if ( $date_error OR !@checkdate($month, $day, $year) ) {
					$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");
				}
			}
			if ($fld['ercheck']=="datetime") {
				if ( ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4}) ([0-9]{1,2}):([0-9]{1,2})", $fld['value'], $regs) ) {
					if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
					$day=$regs[1]; $month=$regs[2]; $year=$regs[3]; 
				}elseif ( ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})", $fld['value'], $regs) ) {
					$day=$regs[3]; $month=$regs[2]; $year=$regs[1];
				}else $date_error=1;
				
				if ( $date_error OR !@checkdate($month, $day, $year) OR $regs[4]>23 OR $regs[4]<0 OR $regs[5]>59 OR $regs[5]<0 ) {
					$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");
				}
			}
			if ($fld[0]=='upload' AND is_array($fld['value'])) {
				if ( $fld['error']=FORM_check_upload_file($fld['value'], $fld['max_size']) ) {
					$fld['error']="<u><b>$fld[title]</b></u> $fld[error]";
				}			

				# check image
				if ($fld['ercheck']=="image" AND $fld['error2']=FORM_check_image($fld['value'], $fld['max_x'], $fld['max_y'], $fld['types_ar'], $fld['img_info'])) {
					$fld['error']=($fld['error']?"$fld[error]<br>":''). "<u><b>$fld[title]</b></u> $fld[error2]";
				}
			}
			
		}
		if ($fld['error'] AND $fld['custom_error']) $fld['error']=$fld['custom_error'];
		
		# Add error to $FORM['error_text']
		if ($fld['error']) $FORM['error_text'].=$fld['error'].'<br>';
	}
	if ($FORM['error_text']) $FORM['error_text']="<u>$L_ERRORS[error_text]</u><br>$FORM[error_text]";	# Add error text title
}


//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function frm_parse(&$FORM) {
 global $CFG;
	if (!$FORM) return;

	if ($FORM['js_check'] AND !$FORM['onsubmit']) $FORM['onsubmit']="return ".($FORM['js_check']==2?'validateCompleteForm':'validateStandard')."(this);";
	
	$FORM['form_tag']="<form"
		.($FORM['id']?" id='$FORM[id]'":'')
		.($FORM['name']?" name='$FORM[name]'":'')
		.($FORM['action']?" action='$FORM[action]'":'')
		." method='".($FORM['method']=='GET'?'GET':'POST')."'"
		.($FORM['onsubmit']?" onSubmit=\"$FORM[onsubmit]\"":'')
		.($FORM['form_tag_add']?" $FORM[form_tag_add]":'')
		.">";

	if ($FORM['js_check'] AND !$GLOBALS['f_jsval_indluded']) {
		$FORM['form_tag'].="<SCRIPT language=javascript src='$CFG[WEB]/inc/forms/jsval2.js'></SCRIPT>";
		$GLOBALS['f_jsval_indluded']=true;
	}	

	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		if (!$fld['name'] AND is_string($key)) $fld['name']=$key;
		if (!$fld['id'] AND $fld['name']) $fld['id']=$fld['name'];

		# field html
		frm_fld(&$fld, $FORM['js_check']);
		# label
		if ($fld['required'])   $fld['class'].=($fld['class']?' ':'').'required';

		if (is_string($fld[1])) $fld['label'] = "<label".($fld['id']?" for='$fld[id]'":'').($fld['class']?" class='$fld[class]'":'').">$fld[1]:</label>";
		
		# shortcuts
		if ($fld['html'])  $f[$key]=&$fld['html'];
		if ($fld['label']) $l[$key]=&$fld['label'];
	}
 return array($f,$l);
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function frm_fld($fld, $js_check=false) {
 global $CFG;
	if (!$fld[0] OR is_numeric($fld[0])) return;

	if ($js_check AND ($fld['required'] OR $fld['ercheck'] OR $fld['regexp'] OR $fld['minlength'] OR $fld[0]=='date')) 
		$js_check_add="onBlur='validate_one(this)' onClick='validate_one(this)'"
			.($fld[0]=='select'? " onChange='validate_one(this)'":'')
			//.($fld[0]=='radio'	? " onClick='validate_one(this)'":'')
			.($fld[0]=='date'	? " onChange='validate_one(this)' onkeyup='validate_one(this)'":'')
			.(is_string($fld[1])	?" realname=\"".htmlspecialchars($fld[1],ENT_QUOTES)."\"":'')
			.($fld['minlength']		? " minlength={$fld[minlength]}":'')
			.($fld['required']		? " required=1":'')
			.($fld['regexp']		? " regexp=\"$fld[regexp]\"":'')
			.($fld['ercheck']=='email' ? " regexp=email":'')
			.($fld['ercheck']=='date'  ? " regexp=date":'')	;

	if ($fld['error']) $fld['class'].=($fld['class']?' ':'').'error';
	
	if ($fld['width'])	$fld['style'].=($fld['style']?';':'')."width:$fld[width]";
	if ($fld['height']) $fld['style'].=($fld['style']?';':'')."height:$fld[height]";
	

	if ($fld[0]=='txt')				$fld['html'] = text2html($fld['value']);
	elseif ($fld[0]=='text')		$fld['html'] = "<input type=text class='text".($fld['class']?" $fld[class]":'')."' id='$fld[id]' name='$fld[name]' value=\"".htmlspecialchars($fld['value'])."\"".($fld['size'] ? " size='$fld[size]'":'').($fld['style'] ? " style='$fld[style]'":'').($fld['maxlength'] ? " maxlength=$fld[maxlength]":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
	elseif ($fld[0]=='password') 	$fld['html'] = "<input type=password class='text".($fld['class']?" $fld[class]":'')."' id='$fld[id]' name='$fld[name]'".($fld['size'] ? " size='$fld[size]'":'').($fld['style'] ? " style='$fld[style]'":'').($fld['maxlength'] ? " maxlength=$fld[maxlength]":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
	elseif ($fld[0]=='checkbox') 	$fld['html'] = "<input type=checkbox class='inputc".($fld['class']?" $fld[class]":'')."' id='$fld[id]' name='$fld[name]' class=inputc value=1".($fld['value'] ?' checked':'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
	elseif ($fld[0]=='submit') 		$fld['html'] = "<input type=submit id='$fld[id]' name='$fld[name]' value='".$fld['title']."'".($fld['add']?" $fld[add]":'').">";
	elseif ($fld[0]=='button') 		$fld['html'] = "<input type=button id='$fld[id]' name='$fld[name]' value='".$fld['title']."'".($fld['add']?" $fld[add]":'').">";
	elseif ($fld[0]=='hidden') 		$fld['html'] = "<input type=hidden id='$fld[id]' name='$fld[name]' value=\"".htmlspecialchars($fld['value'])."\"".($fld['add']?" $fld[add]":'').">";
	elseif ($fld[0]=='textarea') 	$fld['html'] = "<textarea".($fld['class']?" class='$fld[class]'":'')." id='$fld[id]' name='$fld[name]'".($fld['rows']?" rows='$fld[rows]'":'').($fld['cols']?" cols='$fld[cols]'":'').($fld['style'] ? " style='$fld[style]'":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">".htmlspecialchars($fld['value'])."</textarea>";
	elseif ($fld[0]=='upload')    { $fld['html'] = "<input type=file class='text".($fld['class']?" $fld[class]":'')."' id='$fld[id]' name='$fld[name]'".($fld['size'] ? " size='$fld[size]'":'').($fld['style'] ? " style='$fld[style]'":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
		/*if ($fld['max_size'] OR 'ercheck'=='image') $fld['html'].="<br><font class=sm10>("
			.($fld['max_size'] ? "Max file size <b>$fld[max_size] Kb</b>.":'')
			.(($fld['max_x'] AND $fld['max_y']) ? " Max image size <b>$fld[max_x]x$fld[max_y]</b>.":'')
			.(is_array($fld['types_ar']) ? " Alowed image types <b>".implode(',',$fld['types_ar'])."</b>" :'')
			.")</font>";*/
	}elseif ($fld[0]=='select') {
		$select_html.="<select".($fld['class']?" class='$fld[class]'":'')." id='$fld[id]' name='$fld[name]'".($fld['size'] ? " size='$fld[size]'":'').($fld['style'] ? " style='$fld[style]'":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
		if (is_array($fld['values'])) foreach ($fld['values'] as $value=>$text) $select_html.="<option value=\"".htmlspecialchars($value)."\"".($fld['value']=="$value"?' selected':'').">".htmlspecialchars($text);
		$select_html.="</option></select>";
		$fld['html'] = $select_html;

	}elseif ($fld[0]=='radio') {
		$i=0; foreach ($fld['values'] as $value=>$text) { $fld['html'].="<input type=radio class=inputc id='$fld[id]_$i' name='$fld[name]' value=\"".htmlspecialchars($value)."\"".( (isset($fld['value']) AND $fld['value']==$value) ? ' checked':'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'')."> <label for='$fld[id]_$i'>".htmlspecialchars($text)."</label>".($fld['vertical']?'<br>':' &nbsp; &nbsp;'); $i++; }
		
	}elseif ($fld[0]=='date' OR $fld[0]=='datetime') {
		if ($fld['value']=='0000-00-00') $fld['value']='';
		# set default format
		if (!$fld['format'] AND $fld[0]=='date') $fld['format']='%d/%m/%Y';
		elseif (!$fld['format'] AND $fld[0]=='datetime') $fld['format']='%d/%m/%Y %H:%M';

		if (!$GLOBALS['f_calendar_indluded']) {	# include calendar js & css
			$fld['html'].="<SCRIPT type=text/javascript src='$CFG[WEB]/inc/forms/calendar.js'></SCRIPT>"
						."<LINK media=all type=text/css rel=stylesheet href='$CFG[WEB]/inc/forms/calendar-win2k-1.css'>";
			$GLOBALS['f_calendar_indluded']=true;
		}
		# [add readOnly attribute if you don't want user enter date manually]
		$fld['html'].="<input dir=ltr class='text".($fld['class']?" $fld[class]":'')."' id='$fld[id]' name='$fld[name]' size='".($fld['size']?$fld['size']:($fld[0]=='datetime'?15:10))."' value=\"".htmlspecialchars($fld[value])."\"".($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">"
					."<img id='$fld[id]_trigger' title='Select Date' src='$CFG[WEB]/inc/forms/cal.gif' align=absmiddle style='cursor:pointer; border:1px solid orange;' onmouseover=\"this.style.background='orange';\" onmouseout=\"this.style.background=''\">"
					."<script type=text/javascript>\n"
						."Calendar.setup({"
							."inputField	: '$fld[name]',"
							."button		: '$fld[name]_trigger',"
							.($fld[0]=='datetime' ?	# show date&time
											 "ifFormat		: '$fld[format]',"
											."showsTime		: true,"
											."timeFormat	: '24',"
							 /*date only*/	:"ifFormat		: '$fld[format]'," )
							."align			: 'Tl',"
							."weekNumbers	: false,"
							."singleClick	: true"
						."});\n"
					."</script>";
	
	}elseif ($fld[0]=='spaw') {
		$spaw = new SPAW_Wysiwyg($fld['name'] /*name*/, $fld['value'] /*value*/, '' /*language*/, ($fld['toolbar']?$fld['toolbar']:'full1')/*toolbar mode*/, 'default' /*theme*/, ($fld['width']?$fld['width']:'100%') /*width*/, ($fld['height']?$fld['height']:'100px') /*height*/ /*, 'skin/style.css'*/ /*stylesheet file*/);
		$fld['html'] = '<span dir=ltr>'.$spaw->getHtml().'</span>';
	}
	
	if ($fld['prefix']) $fld['html'] = $fld['prefix'].$fld['html'];
	if ($fld['suffix']) $fld['html'].= $fld['suffix'];
	
 return $fld['html'];
}

?>