var values = {
	1:[2.63,3.4,3],
	2:[2.1,2.95,2.75],
	3:[1.95,3,3.05],
	4:[1.5,3,4],
	5:[1.5,3.15,3.8],
	6:[2.05,3,2.8],
	7:[2.6,2.95,1.9],
	8:[1.4,3.7,5.2],
	9:[2.85,1.75,3.05],
	10:[1.5,3,4],
	11:[1.5,3.15,3.8],
	12:[2.05,3,2.8],
	13:[2.6,2.95,1.9],
	14:[1.4,3.7,5.2],
	15:[2.85,1.75,3.05]
}
ExpectedCombo = function(config) {
	ExpectedCombo.superclass.constructor.call(this, {xtype:'combo', itemCls:'rtl', width:45, listWidth:35, cls:'toto_mr8t3', store:[[1,1],[2,'X'],[4,2],[12,'1+X'],[24,'X+2'],[14,'1+2']], emptyText:'-', triggerAction:'all', mode:'local'});
	Ext.apply(this, config);
};
Ext.extend(ExpectedCombo, Ext.form.ComboBox);


TotoFormPanel = function(cfg) {

	this.tableBuild = function(){
		var fld;
		for (g in values) {
			fld = this.form.find('name','g['+g+'][1]');	fld[0].setValue(values[g][0]);
			fld = this.form.find('name','g['+g+'][2]');	fld[0].setValue(values[g][1]);
			fld = this.form.find('name','g['+g+'][4]');	fld[0].setValue(values[g][2]);
		}
	}
	
	this.doTable = function(){
		var arr = this.form.form.getValues();
		var min=0,max=0,sum=0, d=0;
		for (i=1; i<16; i++) {
			sum = (parseFloat(arr['g['+i+'][1]'])+parseFloat(arr['g['+i+'][2]'])+parseFloat(arr['g['+i+'][4]']));
			min = min + Math.min(parseFloat(arr['g['+i+'][1]']),parseFloat(arr['g['+i+'][2]']),parseFloat(arr['g['+i+'][4]']))/sum;
			max = max + Math.max(parseFloat(arr['g['+i+'][1]']),parseFloat(arr['g['+i+'][2]']),parseFloat(arr['g['+i+'][4]']))/sum;
		}
		min=parseInt(min*1000000);
		max=parseInt(max*1000000);
		d=(max-min)/10;
		
		for (i=1; i<11; i++) {
			this.form.find('name','sum_min'+i)[0].setValue(min); min=min+d;
			this.form.find('name','sum_max'+i)[0].setValue(min-1+parseInt(i/10));
		}
		Ext.getCmp('bldFilesBtn').enable();
	}
	
	// this.previewWin = new Ext.Window({
		// id:'previewWin', html:'<iframe id=prevIframe src="about:blank" style="width:100%; height:100%;" frameBorder=0></iframe>',
		// x:5, y:50, width:(Ext.getBody().getWidth()-270), height:(Ext.getBody().getHeight()-55), plain:true, border:false, layout:'fit', closeAction:'hide', closable:false,
		// listeners: { hide:function(){Ext.get('prevIframe').dom.src='about:blank';} }
	// });


	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',region:'center', id:'totoForm',
		items: [
			{layout:'table', layoutConfig: {columns:16}, baseCls: 'x-plain', items:[
			 {xtype:'label',  cls:'toto_p5', style:"width:25px;"}
			 ,{xtype:'label',  cls:'toto_p5', text:'משחק 1'},{xtype:'label', cls:'toto_p5', text:'משחק 2'},{xtype:'label',  cls:'toto_p5', text:'משחק 3'},{xtype:'label',  cls:'toto_p5', text:'משחק 4'},{xtype:'label',  cls:'toto_p5', text:'משחק 5'}
			 ,{xtype:'label',  cls:'toto_p5', text:'משחק 6'},{xtype:'label',  cls:'toto_p5', text:'משחק 7'},{xtype:'label',  cls:'toto_p5', text:'משחק 8'},{xtype:'label',  cls:'toto_p5', text:'משחק 9'},{xtype:'label',  cls:'toto_p5', text:'משחק 10'}
			 ,{xtype:'label',  cls:'toto_p5', text:'משחק 11'},{xtype:'label',  cls:'toto_p5', text:'משחק 12'},{xtype:'label',  cls:'toto_p5', text:'משחק 13'},{xtype:'label',  cls:'toto_p5', text:'משחק 14'},{xtype:'label',  cls:'toto_p5', text:'משחק 15'}
			 ,{xtype:'label',  cls:'toto_p5', text:'1'}
			 ,{xtype:'textfield', name:'g[1][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[2][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[3][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[4][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[5][1]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'textfield', name:'g[6][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[7][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[8][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[9][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[10][1]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'textfield', name:'g[11][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[12][1]', allowBlank:true, width:45, cls:'toto_mr8t3'} ,{xtype:'textfield', name:'g[13][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[14][1]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[15][1]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'label',  cls:'toto_p5', text:'X'}
			 ,{xtype:'textfield', name:'g[1][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[2][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[3][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[4][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[5][2]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'textfield', name:'g[6][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[7][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[8][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[9][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[10][2]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'textfield', name:'g[11][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[12][2]', allowBlank:true, width:45, cls:'toto_mr8t3'} ,{xtype:'textfield', name:'g[13][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[14][2]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[15][2]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'label',  cls:'toto_p5', text:'2'}
			 ,{xtype:'textfield', name:'g[1][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[2][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[3][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[4][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[5][4]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'textfield', name:'g[6][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[7][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[8][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[9][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[10][4]', allowBlank:true, width:45, cls:'toto_mr8t3'}
			 ,{xtype:'textfield', name:'g[11][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[12][4]', allowBlank:true, width:45, cls:'toto_mr8t3'} ,{xtype:'textfield', name:'g[13][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[14][4]', allowBlank:true, width:45, cls:'toto_mr8t3'},{xtype:'textfield', name:'g[15][4]', allowBlank:true, width:45, cls:'toto_mr8t3'}
 			 ,{xtype:'label',  cls:'toto_p5', text:'צפוי'}
			 ,new ExpectedCombo({hiddenName:'g[1][exp]'}),new ExpectedCombo({hiddenName:'g[2][exp]'}),new ExpectedCombo({hiddenName:'g[3][exp]'}),new ExpectedCombo({hiddenName:'g[4][exp]'}),new ExpectedCombo({hiddenName:'g[5][exp]'})
			 ,new ExpectedCombo({hiddenName:'g[6][exp]'}),new ExpectedCombo({hiddenName:'g[7][exp]'}),new ExpectedCombo({hiddenName:'g[8][exp]'}),new ExpectedCombo({hiddenName:'g[9][exp]'}),new ExpectedCombo({hiddenName:'g[10][exp]'})
			 ,new ExpectedCombo({hiddenName:'g[11][exp]'}),new ExpectedCombo({hiddenName:'g[12][exp]'}),new ExpectedCombo({hiddenName:'g[13][exp]'}),new ExpectedCombo({hiddenName:'g[14][exp]'}),new ExpectedCombo({hiddenName:'g[15][exp]'})
 			
			,{xtype:'label',  cls:'toto_p5', text:'#'}
			 ,new Ext.Panel ({region:'west', autoScroll:true, height:360, colspan:15, style:'margin-top:10px; margin-bottom:10px; padding-right:8px; width:806px;',
				items:[
					{xtype:'button', text:'בנה טבלה', height:18,  cls:'toto_p5', handler:function(){this.doTable()}, scope:this}
					,{layout:'table', layoutConfig: {columns:11}, baseCls: 'x-plain', defaults: {xtype:'textfield'}, items:[
						{xtype:'label',  cls:'toto_p5', text:'עשיריה'}
						,{xtype:'checkbox', name:'f1', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f2', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f3', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f4', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f5', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f6', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f7', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f8', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f9', checked:true,  cls:'toto_mr25'}
						,{xtype:'checkbox', name:'f10',checked:true,  cls:'toto_mr25'}
						,{xtype:'label', cls:'toto_p5', text:'נמוך'}
						,{name:'sum_min1', cls:'toto_inp50'},{name:'sum_min2', cls:'toto_inp50'},{name:'sum_min3', cls:'toto_inp50'},{name:'sum_min4', cls:'toto_inp50'},{name:'sum_min5', cls:'toto_inp50'},{name:'sum_min6', cls:'toto_inp50'},{name:'sum_min7', cls:'toto_inp50'},{name:'sum_min8', cls:'toto_inp50'},{name:'sum_min9', cls:'toto_inp50'},{name:'sum_min10', cls:'toto_inp50'}
						,{xtype:'label', cls:'toto_p5', text:'גבוה'}
						,{name:'sum_max1', cls:'toto_inp50'},{name:'sum_max2', cls:'toto_inp50'},{name:'sum_max3', cls:'toto_inp50'},{name:'sum_max4', cls:'toto_inp50'},{name:'sum_max5', cls:'toto_inp50'},{name:'sum_max6', cls:'toto_inp50'},{name:'sum_max7', cls:'toto_inp50'},{name:'sum_max8', cls:'toto_inp50'},{name:'sum_max9', cls:'toto_inp50'},{name:'sum_max10', cls:'toto_inp50'}
						,{xtype:'button', text:'בנה קבצים', disabled:true, id:'bldFilesBtn', height:18, colspan:11, cls:'toto_p5', handler:function(){this.submitForm('build_files')}, scope:this}
						
						,{xtype:'label', cls:'toto_p5', text:"סך טורים"}
						,{name:'count1', cls:'toto_inp50'},{name:'count2', cls:'toto_inp50'},{name:'count3', cls:'toto_inp50'},{name:'count4', cls:'toto_inp50'},{name:'count5', cls:'toto_inp50'},{name:'count6', cls:'toto_inp50'},{name:'count7', cls:'toto_inp50'},{name:'count8', cls:'toto_inp50'},{name:'count9', cls:'toto_inp50'},{name:'count10', cls:'toto_inp50'}
						
						,{xtype:'label',  cls:'toto_p5', text:'סינונים'}
						,{xtype:'panel', border:true, layout:'table', layoutConfig: {columns:8}, baseCls: 'x-plain', style:"margin-top:8px;", colspan:10, items:[
						  {xtype:'label',  text:'1) משקל טור בין:', style:"margin-right:8px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'sum_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'sum_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:'6) כמות תוצאות צפויות:', style:"margin-right:28px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'exp_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'exp_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"2) כמות זוגות בטור בין:", style:"margin-right:8px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'pair_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'pair_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"7) כמות שבירות בטור בין:", style:"margin-right:28px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'break_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'break_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"3) כמות '1' בטור בין:", style:"margin-right:8px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'count1_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'count1_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"8) כמות זוגות '1' בטור בין:", style:"margin-right:28px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'pair1_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'pair1_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"4) כמות 'X' בטור בין:", style:"margin-right:8px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'count0_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'count0_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"9) כמות זוגות 'X' בטור בין:", style:"margin-right:28px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'pair0_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'pair0_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"5) כמות '2' בטור בין:", style:"margin-right:8px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'count2_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'count2_max', width:35, cls:'toto_mr8t3'}
						 
						 ,{xtype:'label',  text:"10) כמות זוגות '2' בטור בין:", style:"margin-right:28px; text-align:right; display:block; width:160px;"}
						 ,{xtype:'textfield', name:'pair2_min', width:35, cls:'toto_mr8t3'}
						 ,{xtype:'label',  cls:'toto_p5', text:'לבין:', style:"margin-right:8px; width:25px;"}
						 ,{xtype:'textfield', name:'pair2_max', width:35, cls:'toto_mr8t3'}
						]}
						
						,{xtype:'label',  cls:'toto_p5', text:''}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',1)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',2)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',3)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',4)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',5)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',6)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',7)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',8)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',9)}, scope:this}
						,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',10)}, scope:this}
						
						,{xtype:'label', cls:'toto_p5', text:"טורים אחרי סינון"}
						,{name:'countf1', cls:'toto_inp50'},{name:'countf2', cls:'toto_inp50'},{name:'countf3', cls:'toto_inp50'},{name:'countf4', cls:'toto_inp50'},{name:'countf5', cls:'toto_inp50'},{name:'countf6', cls:'toto_inp50'},{name:'countf7', cls:'toto_inp50'},{name:'countf8', cls:'toto_inp50'},{name:'countf9', cls:'toto_inp50'},{name:'countf10', cls:'toto_inp50'}
						
						,{xtype:'label',  cls:'toto_p5', text:''}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo1_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo2_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo3_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo4_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo5_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo6_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo7_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo8_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo9_f.str'}, scope:this}
						,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto/demo10_f.str'}, scope:this}
					]}
				]
			  })
			]}


		]
	});
	
	TotoFormPanel.superclass.constructor.call(this, {
		// title:'הרשאות קבוצה',
		layout:'fit',border:false, region:'center',
		items:[
			this.form
		]
    });
	
	this.on('render', function(){this.tableBuild()},this);
	
	this.submitForm = function(fn,num) {
	
		// if (!this.form.form.isValid()) return;

		// this.getEl().mask('...טוען');
		
		var win = new Ext.Window({
			id:'waitWin', modal:true, html:'<iframe name=waitFrame src="about:blank" style="width:99%; height:99%;" frameBorder=3></iframe>',
			x:5, y:50, width:400, height:400, plain:true, border:false, layout:'fit'//, //closeAction:'hide', closable:false,
		});
		
		win.show();
		
		
		(function(){
			var f=this.form.form.getEl().dom;
			f.target = 'waitFrame';
			f.action = '?m=toto/toto&NOGZ=1&f='+fn+'&file_num='+num;
			f.submit();
		}).defer(2000, this);
		
		// this.form.form.submit({
			// url: '?m=toto/toto&f='+fn,	timeout:3600,
			// params:{file_num:num},
			// waitTitle: 'אנא המתן...', waitMsg: 'טוען את הקובץ...',
			// scope:this,
			// success: function(f,a){
				// this.getEl().unmask(); 
				// Ext.Msg.alert('בניית קובץ הסתיימה!', 'בניית קבצים הסתיימה ב-'+a.result.msg+' שניות.');
				// if (fn=='build_files') for (g in a.result.cnt) this.form.find('name','count'+g)[0].setValue(a.result.cnt[g]);
				// else if (fn=='filter') this.form.find('name','countf'+num)[0].setValue(a.result.cnt);
			// },
			// failure: function(r,o){this.getEl().unmask(); ajRqErrors(o.result);}
		// });
	}
};
Ext.extend(TotoFormPanel, Ext.Panel);

Ms.toto.run = function(cfg){
	var m=Ms.toto;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		win = Crm.desktop.createWindow({width:900, height:600, iconCls:'toto', layout:'border',	items:[new TotoFormPanel]}, m);
	}
	win.show();
};