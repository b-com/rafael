function $(sId) { if (sId) return document.getElementById(sId); }

var folderCombo,linkCombo;
var tw1,tw2;

function setCookie(name, value, expDays) {
	var today = new Date();
	var expireDate = new Date(today.getTime()+expDays*24*60*60*1000);
	document.cookie = name+'='+escape(value)+'; expires='+expireDate.toGMTString();
}
function getCookie(id) {
	var re=new RegExp('(^|; ?)'+id+'=([^;]*)');
	var value=re.exec(document.cookie);
	if (value) return value[2];
}


Ext.onReady(function(){
	//Ext.QuickTips.init();

	tw1=$('tw1');
	tw2=$('tw2');
	folderCombo = new Ext.form.ComboBox({
		hiddenName:'folder',
		renderTo:'folderCont',
		store: foldersStore,
		width:170,
		listClass:'rtl',
		triggerAction:'all',
		allowBlank:false,
		value:selectedFolder
	});

	// linkCombo = new Ext.form.ComboBox({
		// hiddenName:'link',
		// renderTo:'linkCont',
		// store: linksStore,
		// value:(link ? link : ''),
		// disabled:(link ? true : false),
		// width:170,
		// listClass:'rtl',
		// triggerAction:'all',
		// forceSelection:true
	// });
	// Dump(linksStore);
	linkCombo = new Ext.ux.IconCombo({
		hiddenName:'link',
		renderTo:'linkCont',
		value:(link ? link : ''),
		disabled:(link ? true : false),
		width:170, listWidth:360,
		itemCls:'rtl',
		valueField: 'id',
		displayField: 'name',
		iconClsField: 'icon',
		triggerAction: 'all',
		mode: 'local',
		forceSelection:true,
		store: new Ext.data.SimpleStore({fields: ['id', 'name', 'icon', 'group'], data: linksStore})		
	});
	
	pageonload();
	initTb();	
});

var em="";
var ViewMode=1;

function pixelTypeChange(type) {
	if (type=='V15') $('pixelTypeMsg').innerHTML="";
	else $('pixelTypeMsg').innerHTML="שים לב!<br>סריקה באפור או בצבע מגדילה את גודל הקובץ וגורמת לזמן ארוך יותר בשמירה ופתיחה של המסמך.";
	$('PixelFlavorChk').style.display = (type=='V15'?'':'none');
}

//====================Page Onload  Start==================//
function CheckIfImagesInBuffer() {
	if (tw1.HowManyImagesInBuffer==0) {
		//alert("There is no image in buffer");
		em=em + "There is no image in buffer.\n";
		$('emessage').innerHTML=em;
		return;
	}
}
/*
function CheckErrorString() {
	if (tw1.ErrorCode != 0) {
		em=em + tw1.ErrorString + "\n";
		//alert(tw1.HTTPPostResponseString);
		$('emessage').innerHTML=em;
		return;
	}
}*/

var iLeft;
var iTop;
var iRight;
var iBottom;

function pageonload() {
	iLeft=0;
	iTop=0;
	iRight=0;
	iBottom=0;
			
	tw1.MouseShape=true;
	tw1.MaxImagesInBuffer=100;
	tw1.SetViewMode(1,4);
	
	tw2.MaxImagesInBuffer=1;
	tw2.IfFitWindow=true;
	tw2.MouseShape=true;
	tw2.SetViewMode(-1,-1);
		
	var i;
	$('source').options.length=0;
	for(i=0;i<tw1.SourceCount;i++) $('source').options.add(new Option(tw1.SourceNameItems(i),"i"));
	
	var selSource=getCookie('source');
	if (selSource && $('source').options[selSource]) $('source').selectedIndex=selSource;
	
	$('PixelFlavor').checked = (getCookie('pxfl')>0);
	
	addEvent('scanTb', 'mouseout', scanTbEvents);
	
	//tw1.LoadImage("d:/WWW/bafi/crm-files/90/34/123634/381375-1.pdf");
	//tw1.HTTPDownload(location.host, "/crmview/381373/309997203.pdf");
	UpdateImage(false);
}
//====================Page Onload End====================//


//====================Preview Group Start====================//
function btnFirstImage_onclick() {
	CheckIfImagesInBuffer();
	tw1.CurrentImageIndexInBuffer=0;
	UpdateImage(true);
}
function btnPreImage_onclick() {
	CheckIfImagesInBuffer();
	if (tw1.CurrentImageIndexInBuffer==0) return;
	tw1.CurrentImageIndexInBuffer=tw1.CurrentImageIndexInBuffer - 1;
	UpdateImage(true);
}
function btnNextImage_onclick() {
	CheckIfImagesInBuffer();
	if (tw1.CurrentImageIndexInBuffer==tw1.HowManyImagesInBuffer - 1) return;
	tw1.CurrentImageIndexInBuffer=tw1.CurrentImageIndexInBuffer + 1;
	UpdateImage(true);
}
function btnLastImage_onclick() {
	CheckIfImagesInBuffer();
	tw1.CurrentImageIndexInBuffer=tw1.HowManyImagesInBuffer - 1;
	UpdateImage(true);
}
function btnZoomIn_onclick() {
		CheckIfImagesInBuffer();
		if (ViewMode==1) {
			tw2.SetViewMode(-1,-1);
			ViewMode=0;
		}
		tw2.Zoom=tw2.Zoom*1.1;
		//alert(tw2.Zoom);
}
function btnZoomOut_onclick() {
		CheckIfImagesInBuffer();
		if (ViewMode==1) {
			tw2.SetViewMode(-1,-1);
			ViewMode=0;
		}
		tw2.Zoom=tw2.Zoom*0.9;
		//alert(tw2.Zoom);
}
function btnFit_onclick(){
	tw2.IfFitWindow=true;
	$("btnFit").disabled=true;		$("btnFit").className='disabled';
	$("btnFact").disabled=false;	$("btnFact").className='';
}
function btnFact_onclick(){
	tw2.IfFitWindow=false;
	$("btnFit").disabled=false;		$("btnFit").className='';
	$("btnFact").disabled=true;		$("btnFact").className='disabled';
}
function btnPointer_onclick(){
	tw2.MouseShape=false;
	$("btnPointer").disabled=true;	$("btnPointer").className='disabled';
	$("btnHand").disabled=false;	$("btnHand").className='';
}
function btnHand_onclick(){
	tw2.MouseShape=true;
	$("btnPointer").disabled=false;	$("btnPointer").className='';
	$("btnHand").disabled=true;		$("btnHand").className='disabled';
}
function btnRemoveCurrentImage_onclick() {
	CheckIfImagesInBuffer();
	tw1.RemoveImage(tw1.CurrentImageIndexInBuffer);
	if (tw1.HowManyImagesInBuffer==0){
		// frmScan.TotalImage.value=tw1.HowManyImagesInBuffer;
		// frmScan.CurrentImage.value="";
		tw2.RemoveAllImages();
		UpdateImage(true);
		return;
	}
	else{
		// frmScan.TotalImage.value=tw1.HowManyImagesInBuffer;
		// frmScan.CurrentImage.value=tw1.CurrentImageIndexInBuffer+1;
	}
	UpdateImage(true);
}
function btnRemoveAllImages_onclick() {
	CheckIfImagesInBuffer();
	//frmScan.TotalImage.value="0";
	//frmScan.CurrentImage.value="";
	tw1.RemoveAllImages();
	tw2.RemoveAllImages();
	UpdateImage(true);
}
//====================Preview Group End====================//


//====================Edit Image Group Start=====================//
function btnShowImageEditor_onclick() {
	CheckIfImagesInBuffer();
	tw1.ShowImageEditor();
}

function btnSwitch_onclick(){
	if(tw1.HowManyImagesInBuffer > 1) {
		var i;
		// with(document.all){
			$('selimg1').options.length=0;
			$('selimg2').options.length=0;
			for(i=0;i<tw1.HowManyImagesInBuffer;i++) {
				$('selimg1').options.add(new Option(i+1,"i"));
				$('selimg2').options.add(new Option(i+1,"i"));
			}
			$('selimg1').options[0].selected=true;
			$('selimg2').options[1].selected=true;
		// }
		$('switch').style.visibility="visible";
	}
}

function btnSwitchOk_onclick(){
	// with(document.all){
		if($('selimg1').selectedIndex==$('selimg2').selectedIndex) {
			em=em + "Invalid Operation.\n"
			$('emessage').innerHTML=em;
		}else {
			tw1.SwitchImage($('selimg1').selectedIndex, $('selimg2').selectedIndex);
			UpdateImage(true);
		}
	// }
	$("switch").style.visibility="hidden";
}

function btnSwitchCancel_onclick(){
	$("switch").style.visibility="hidden";
}

function btnRotateRight_onclick() {
	CheckIfImagesInBuffer();
	tw1.RotateRight(tw1.CurrentImageIndexInBuffer);
	tw2.RotateRight(tw2.CurrentImageIndexInBuffer);
}

function btnRotateLeft_onclick() {
	CheckIfImagesInBuffer();
	tw1.RotateLeft(tw1.CurrentImageIndexInBuffer);
	tw2.RotateLeft(tw2.CurrentImageIndexInBuffer);
}

function btnMirror_onclick() {
	CheckIfImagesInBuffer();
	tw1.Mirror(tw1.CurrentImageIndexInBuffer);
	tw2.Mirror(tw2.CurrentImageIndexInBuffer);
}

function btnFlip_onclick() {
	CheckIfImagesInBuffer();
	tw1.Flip(tw1.CurrentImageIndexInBuffer);
	tw2.Flip(tw2.CurrentImageIndexInBuffer);
}


/*----------------------Crop Method---------------------*/
function btnCrop_onclick() {
	if (tw1.HowManyImagesInBuffer==0) {
		em=em + "There is no image in buffer.\n"
		$('emessage').innerHTML=em;
		return;
	}
	if (iLeft==0 && iRight==0 && iTop==0 && iBottom==0) {
		$("Crop").style.visibility="visible";
	}else {
		tw1.Crop(tw1.CurrentImageIndexInBuffer, iLeft, iTop, iRight, iBottom);
		iLeft=0;
		iTop=0;
		iRight=0;
		iBottom=0;
	}
	UpdateImage(true);
}

function btnCropCancel_onclick(){
	$("Crop").style.visibility="hidden";
}

function btnCropOK_onclick(){
	if($('img_left').value==""){
		em=em + "Please input left value.\n"
		$('emessage').innerHTML=em;
		return;
	}
	if($('img_top').value==""){
		em=em + "Please input top value.\n"
		$('emessage').innerHTML=em;
		return;    
	}
	if($('img_right').value==""){
		em=em + "Please input right value.\n"
		$('emessage').innerHTML=em;
		return;   
	}
	if($('img_bottom').value==""){
		em=em + "Please input bottom value.\n"
		$('emessage').innerHTML=em;
		return;    
	}
	//window.clipboardData.clearData();
	tw1.Crop(
	tw1.CurrentImageIndexInBuffer,
	$('img_left').value,
	$('img_top').value,
	$('img_right').value,
	$('img_bottom').value);
	  
	tw1.CropToClipboard( tw1.CurrentImageIndexInBuffer,
	$('img_left').value,
	$('img_top').value,
	$('img_right').value,
	$('img_bottom').value);
	tw2.RemoveAllImages();
	tw2.LoadDibFromClipboard();
	//window.clipboardData.clearData();
	$("Crop").style.visibility="hidden";
	UpdateImage(true);
}
/*-----------------------------------------------------*/
//====================Edit Image Group End==================//


function btnScan_onclick(showUI) {
	if($('Insertimage').checked) tw1.IfAppendImage=false;
	else tw1.IfAppendImage=true;
	AcquireImage(showUI);
}

function AcquireImage(showUI){
	var i;

	tw1.SelectSourceByIndex($('source').selectedIndex);
	tw1.IfShowUI=showUI;
	
	tw1.CloseSource();
	if (!tw1.OpenSource()) {
		$('emessage').innerHTML="סורק לא זמין";
		return;
	}

	// color mode
	for(i=0; i<3; i++) { if(document.getElementsByName("PixelType").item(i).checked==true) tw1.PixelType=i; }
	
	tw1.PixelFlavor = $('PixelFlavor').checked ? 1 : 0;
	
	//alert('IfFeederEnabled='+tw1.IfFeederEnabled+'\nIfDuplexEnabled='+tw1.IfDuplexEnabled+'\n');
	//alert('tw1.Brightness='+tw1.Brightness); return;
	
	tw1.IfDisableSourceAfterAcquire=true;
	
	// Brightness
	if ($('Brightness').value=='auto') {
		tw1.$('Brightness')=0;
		tw1.IfAutoBright=true;
	}else {
		tw1.IfAutoBright=false;
		tw1.Brightness=$('Brightness').value;
	}
	

	tw1.Resolution=$('Resolution').value;
	if (tw1.Resolution != $('Resolution').value) {
		$('emessage').innerHTML="לא היתה אפשרות להגדיר רזולוציה<br>סורק לא זמין או לא תומך רזולוציה זו";
		return;
	}
	
	//if (tw1.IfAutoFeed) 
	tw1.IfFeederEnabled=$('Feeder').checked;
	//tw1.IfDuplexEnabled=$('Duplex').checked ;
	
	tw1.AcquireImage();

	//tw1.SetViewMode(PreviewMode.selectedIndex+1,PreviewMode.selectedIndex+1);
	// UpdateImage(true);
	//if (!showUI) window.setTimeout(function(){tw1.CloseSource();}, 100);
}


/*-----------------Upload Image Group---------------------*/
function btnUpload_onclick(){

	if (tw1.HowManyImagesInBuffer==0) {
		em=em + "There is no image in buffer.\n";
		$('emessage').innerHTML=em;
		return;
	}
	
	if($('txt_fileName').value==""){
		alert('אנא מלא שם הקובץ');
		$('txt_fileName').focus();
		//em=em + "please input file name.\n";
		//$('emessage').innerHTML=em;
		return;
	}

	var folder = folderCombo.getRawValue().trim();
	if (folderCombo.getRawValue().trim()=='') linkCombo.setValue('');
	var link = linkCombo.getValue();
	
	var url='crm/?m=common/files&f=upload_files&client_id='+client_id+'&client_type='+client_type+'&mode='+$('mode').value+'&id='+$('id').value+'&link='+link+'&folder='+encodeURIComponent(folder)+'&file_name='+encodeURIComponent($('txt_fileName').value.trim())+'&notes='+encodeURIComponent($('notes').value)+'&SES='+getCookie('SES');
	tw1.HTTPUploadAllThroughPostAsPDF(
		location.host,
		url,
		'file.pdf'
	);
	
	if (tw1.ErrorCode!=0) alert(tw1.ErrorString);
	else {
		
		$('mode').value='';
		$('txt_fileName').value='';
		$('notes').value='';
		if (!link) { linkCombo.setValue(''); linkCombo.setRawValue(''); }
		tw1.RemoveAllImages();
		tw2.RemoveAllImages();
		UpdateImage(true);
		//window.returnValue=1;
		
		//var tree=dialogArguments;
		//tree.reload();
		try {with(opener){if (grid_id) Ext.getCmp('FilesGrid'+grid_id).store.reload();}} catch(e){}	// update files grid

		try {opener.filesGrid.store.reload();} catch(e){}	// update files grid
		callBack();
		
		alert("קובץ נשמר בהצלחה");		
		
		if ($('id').value) window.close();
		$('id').value='';
	}
}


/*------------------select menu response----------------------------*/

//function slPreviewMode(){
//    tw1.SetViewMode($('PreviewMode').selectedIndex+1,$('PreviewMode').selectedIndex+1);
//    //with(document.all)
//    //PreviewMode.options.
//}

function tw1_OnPostTransfer() {	
	if ($('DiscardBlank').checked==true) {
		var LastImage=tw1.HowManyImagesInBuffer -1;
		if (tw1.IsBlankImage(LastImage)) tw1.RemoveImage(LastImage);
	}
	UpdateImage(true);
}


function tw1_OnPostAllTransfer() {	
	tw1.CloseSource();
}


function tw1_OnMouseClick(index) {
	tw1.CurrentImageIndexInBuffer=index;
	UpdateImage(true);
}


function tw2_OnImageAreaSelected(index,left,top,right,bottom) {
	iLeft=left;
	iTop=top;
	iRight=right;
	iBottom=bottom;
	$('btnScan').disabled=true;
	$('btnFirstImage').disabled=true;
	$('btnPreImage').disabled=true;
	$('btnNextImage').disabled=true;
	$('btnLastImage').disabled=true;
	$('btnFit').disabled=true;
	$('btnFact').disabled=true;
	$('btnPointer').disabled=true;
	$('btnHand').disabled=true;
	$('btnRemoveCurrentImage').disabled=true;
	$('btnRemoveAllImages').disabled=true;
	// $('btnShowImageEditor').disabled=true;
	// $('btnSwitch').disabled=true;
	// $('btnRotateRight').disabled=true;
	// $('btnRotateLeft').disabled=true;
	// $('btnMirror').disabled=true;
	// $('btnFlip').disabled=true;
	// $('btnChangeImageSize').disabled=true;
	// $('btnSave').disabled=true;
	$('btnUpload').disabled=true;
}


function tw2_OnImageAreaDeselected(index) {
	iLeft=0;
	iTop=0;
	iRight=0;
	iBottom=0;
	$('btnScan').disabled=false;
	$('btnFirstImage').disabled=false;
	$('btnPreImage').disabled=false;
	$('btnNextImage').disabled=false;
	$('btnLastImage').disabled=false;
	$('btnPointer').disabled=false;
	$('btnHand').disabled=false;
	$('btnRemoveCurrentImage').disabled=false;
	$('btnRemoveAllImages').disabled=false;
	
	// $('btnShowImageEditor').disabled=false;
	// $('btnSwitch').disabled=false;
	// $('btnRotateRight').disabled=false;
	// $('btnRotateLeft').disabled=false;
	// $('btnMirror').disabled=false;
	// $('btnFlip').disabled=false;
	// $('btnChangeImageSize').disabled=false;
	// $('btnSave').disabled=false;
	
	$('btnUpload').disabled=false;
	UpdateImage(true);
}


function UpdateImage(flag){

	if (tw1.HowManyImagesInBuffer==0) {
		$('pageCounter').innerHTML = '0/0';
		$('btnFirstImage').disabled=true;	$('btnFirstImage').className='disabled';
		$('btnPreImage').disabled=true;		$('btnPreImage').className='disabled';
		$('btnNextImage').disabled=true;	$('btnNextImage').className='disabled';
		$('btnLastImage').disabled=true;	$('btnLastImage').className='disabled';
		$('btnFit').disabled=true;			$('btnFit').className='disabled';
		$('btnFact').disabled=true;			$('btnFact').className='disabled';
		// $('btnPointer').disabled=true;		$('btnPointer').className='disabled';
		// $('btnHand').disabled=true;			$('btnHand').className='disabled';
		$('btnRotateRight').disabled=true;	$('btnRotateRight').className='disabled';
		$('btnRotateLeft').disabled=true;	$('btnRotateLeft').className='disabled';
		$('btnRemoveCurrentImage').disabled=true;	$('btnRemoveCurrentImage').className='disabled';
		$('btnRemoveAllImages').disabled=true;		$('btnRemoveAllImages').className='disabled';
		
		$('btnUpload').disabled=true;
		// $('btnZoomIn').disabled=true; 
		// $('btnZoomOut').disabled=true;
		
		// $('btnShowImageEditor').disabled=true;
		// $('btnSwitch').disabled=true;
		// $('btnMirror').disabled=true;
		// $('btnFlip').disabled=true;
		// $('btnCrop').disabled=true;
		// $('btnChangeImageSize').disabled=true;
		// $('btnSave').disabled=true;
	}else {
		$('pageCounter').innerHTML = (tw1.CurrentImageIndexInBuffer+1) +'/'+ tw1.HowManyImagesInBuffer;
		if(tw2.IfFitWindow==true) {
			$('btnFact').disabled=false;	$('btnFact').className='';
			$('btnFit').disabled=true;		$('btnFit').className='disabled';
		}else {
			$('btnFact').disabled=false;	$('btnFact').className='';
			$('btnFit').disabled=true;		$('btnFit').className='disabled';
		}
		
		//if(tw2.MouseShape==false) {
			// $('btnPointer').disabled=!tw2.MouseShape;
			// $('btnHand').disabled=tw2.MouseShape;
		//}
		
		// if ($('btnPointer').disabled==true) $('btnHand').disabled=false;
		// else $('btnHand').disabled=true;
		
		$('btnRemoveCurrentImage').disabled=false;	$('btnRemoveCurrentImage').className='';
		$('btnRemoveAllImages').disabled=false;		$('btnRemoveAllImages').className='';
		$('btnRotateRight').disabled=false;			$('btnRotateRight').className='';
		$('btnRotateLeft').disabled=false;			$('btnRotateLeft').className='';

		$('btnUpload').disabled=false;
		// $('btnZoomIn').disabled=false; 
		// $('btnZoomOut').disabled=false;

		// $('btnSave').disabled=false;
		// $('btnShowImageEditor').disabled=false;
		// $('btnFlip').disabled=false;
		// $('btnMirror').disabled=false;
		// $('btnCrop').disabled=false;
		// $('btnChangeImageSize').disabled=false;

		// if(tw1.HowManyImagesInBuffer >= 2) $('btnSwitch').disabled=false;
		// else $('btnSwitch').disabled=true;
		
		if ($('btnFact').disabled==true) $('btnFit').disabled=false;
		else $('btnFit').disabled=true;


		if (tw1.CurrentImageIndexInBuffer >= 1) {
			$('btnFirstImage').disabled=false;	$('btnFirstImage').className='';
			$('btnPreImage').disabled=false;	$('btnPreImage').className='';
		}else {
			$('btnFirstImage').disabled=true;	$('btnFirstImage').className='disabled';
			$('btnPreImage').disabled=true;		$('btnPreImage').className='disabled';
		}

		if (tw1.CurrentImageIndexInBuffer < tw1.HowManyImagesInBuffer - 1) {
			$('btnNextImage').disabled=false;	$('btnNextImage').className='';
			$('btnLastImage').disabled=false;	$('btnLastImage').className='';
		}else {
			$('btnNextImage').disabled=true;	$('btnNextImage').className='disabled';
			$('btnLastImage').disabled=true;	$('btnLastImage').className='disabled';
		}
		
	}
		
	// if (tw1.HowManyImagesInBuffer==4) $('btnScan').disabled=true;  
	// if (tw1.HowManyImagesInBuffer< 4) $('btnScan').disabled=false; 

	// Dump('A tw1.HowManyImagesInBuffer='+tw1.HowManyImagesInBuffer);
	// Dump('A tw1.CurrentImageIndexInBuffer='+tw1.CurrentImageIndexInBuffer);
	// var imgInBuf=tw1.HowManyImagesInBuffer;
	//window.open("", "", "width=600,height=600");
	
	if(tw1.HowManyImagesInBuffer==0) {
	tw2.RemoveAllImages();
		return;
	}
	
	// Dump('B tw1.HowManyImagesInBuffer='+tw1.HowManyImagesInBuffer);
	// Dump('B tw1.CurrentImageIndexInBuffer='+tw1.CurrentImageIndexInBuffer);
	
	tw1.CopyToClipboard(tw1.CurrentImageIndexInBuffer);
	tw2.RemoveAllImages();
	tw2.LoadDibFromClipboard();
}


/*----------------Change Image Size--------------------*/
/*
function btnChangeImageSize_onclick(){
	if (tw1.HowManyImagesInBuffer==0) {
		em=em + "There is no image in buffer.\n"
		$('emessage').innerHTML=em;
		return;
	}
	$("ImgSizeEditor").style.visibility="visible";
}

function btnCancel_onclick() {
	$("ImgSizeEditor").style.visibility="hidden";
}

function btnOK_onclick(){
	if($('img_height').value==""){
		em=em + "Please input the height.\n";
		$('emessage').innerHTML=em;
		return;    
	}
	if($('img_width').value==""){
		em=em + "Please input the width.\n";
		$('emessage').innerHTML=em;
		return;   	  
	}

	tw1.ChangeImageSize(
		tw1.CurrentImageIndexInBuffer,
		$('img_width').value,
		$('img_height').value,
		$('InterpolationMethod').selectedIndex + 1
	);
	//window.clipboardData.clearData();
	tw1.CopyToClipboard(tw1.CurrentImageIndexInBuffer);
	tw2.RemoveAllImages();
	tw2.LoadDibFromClipboard();
	//window.clipboardData.clearData();
	$ ("ImgSizeEditor").style.visibility="hidden";
}
*/

/*-----------------Save Image Group---------------------*/
/*
function btnSave_onclick(){
	if (tw1.HowManyImagesInBuffer==0) {
		em=em + "There is no image in buffer.\n"
		$('emessage').innerHTML=em;
		return;
	}
	var i,strimgType_save;
	var imgtype;
	for(i=0;i<5;i++){
		if(document.getElementsByName("imgType_save").item(i).checked==true){
			strimgType_save =document.getElementsByName("imgType_save").item(i).value;
			imgtype=i;
			break;
		}
	}
	var strFilePath=$('txt_filePathforSave').value+$('txt_fileNameforSave').value+"."+strimgType_save;

	if(strimgType_save=="tif" && $('MultiPageTIFF_save').checked) {
		tw1.SaveAllAsMultiPageTIFF(strFilePath);
	}else if(strimgType_save=="pdf" && $('MultiPagePDF_save').checked) {
		tw1.SaveAllAsPDF(strFilePath);
	}else {
		with(document.all){
			switch(i){
				case 0:tw1.saveasbmp(strFilePath , tw1.CurrentImageIndexInBuffer);break;
				case 1:tw1.SaveasJPEG(strFilePath ,tw1.CurrentImageIndexInBuffer);break;
				case 2:tw1.SaveasTIFF(strFilePath ,tw1.CurrentImageIndexInBuffer);break;
				case 3:tw1.SaveasPNG(strFilePath , tw1.CurrentImageIndexInBuffer);break;
				case 4:tw1.SaveasPDF(strFilePath , tw1.CurrentImageIndexInBuffer);break;
			} 
		}
	}
	if (tw1.HowManyImagesInBuffer != 0) {
		$('txt_fileSize').value=tw1.GetImageSizeWithSpecifiedType(tw1.CurrentImageIndexInBuffer, imgtype);
	}
	//alert("save successful");
	CheckErrorString();
}
*/

