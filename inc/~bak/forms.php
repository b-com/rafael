<?

if ($ln=='en') {
	$L_ERRORS['error_text']="Please correct the following:";
	$L_ERRORS['not_valid']="%s not valid";
	$L_ERRORS['please_select']="please select %s";
	$L_ERRORS['required']="%s required";
	$L_ERRORS['minimum']="%s must be at least %s characters";
	$L_ERRORS['maximum']="%s must be not longer than %s characters";
}elseif ($ln=='ru') {
	$L_ERRORS['error_text']="Ķąéäåķū īųčįźč čėč ķåēąļīėķåķķūå ōīšģū:";
	$L_ERRORS['not_valid']="%s ķå ļšąāčėüķūé";
	$L_ERRORS['please_select']="ļīęąėóéńņą āūįåščņå %s";
	$L_ERRORS['required']="%s äīėęåķ įūņü ēąļīėķåķ";
	$L_ERRORS['minimum']="%s äīėęåķ įūņü õīņ’ įū %s ńčģāīėīā";
	$L_ERRORS['maximum']="%s äīėęåķ įūņü ķå įīėüųå ńčģāīėīā";
}elseif ($ln=='he') {
	$L_ERRORS['error_text']="šīöąå łāéąåś įīéģåé äčåōń:";
	$L_ERRORS['not_valid']="%s ąéšå śé÷šé";
	$L_ERRORS['please_select']="éł ģįēåų %s";
	$L_ERRORS['required']="šą ģäęéļ ņųź įłćä: %s";
	$L_ERRORS['minimum']="%s ąīåų/ä ģėģåģ ģōēåś %s śåéķ";
	$L_ERRORS['maximum']="%s ąīåų/ä ģėģåģ ģą éåśų ī %s śåéķ";
}


############################################################################################
function FORM_load_vals(&$FORM, $primary_id) {
	if (!$primary_id) return;

	#set $fields_list
	foreach ($FORM['fields'] as $fld=>$row) if ($row['db']==1 OR $row['load']==1) {
		if ($fields_list) $fields_list.=',';
		if ($row['db_name']) $fields_list.=$row['db_name'];
		elseif ($row['type']=='date' OR $row['type']=='datetime') {
			if (!$row['format']) $row['format'] = ($row['type']=='datetime') ? '%d/%m/%Y %H:%i' : '%d/%m/%Y';	# sed default date format
			$fields_list.="IF ($fld, DATE_FORMAT($fld,'$row[format]'), '') AS $fld";
		}
		else $fields_list.=$fld;
		//$fields_list.=($fields_list?',':'').($row['db_name'] ? $row['db_name'] : $fld);
	}
	
	
	$sql="SELECT $fields_list FROM $FORM[table] ".($FORM['join']?"$FORM[join]":"")." WHERE $FORM[table].$FORM[primary]='$primary_id'";
	$row=sql2array($sql,'','',1);

	foreach ($FORM['fields'] as $key=>$value) {
		if ($FORM['fields'][$key]['db'] OR $FORM['fields'][$key]['load']) 
			$FORM['fields'][$key]['value']=$row[$key];
	}

 return $row;
}


############################################################################################
function FORM_prepere_sql(&$FORM, $action) {
	$query='';
	$fieldsar=$FORM['fields'];

	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		
		# convert date format
		if ($fld['type']=='date' AND ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4})", $fld['value'], $regs) ) {
			if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
			$fld['value']="$regs[3]-$regs[2]-$regs[1]";
		}
		# convert datetime format
		if ($fld['type']=='datetime' AND ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4}) ([0-9]{1,2}):([0-9]{1,2})", $fld['value'], $regs) ) {
			if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
			$fld['value']="$regs[3]-$regs[2]-$regs[1] $regs[4]:$regs[5]";
		}		
		if (!$fld['name']) $fld['name']=$key;	# set name
		if ($fld['db']==1) $query.= ($query?', ':'') . "$fld[name]='".sql_escape($fld['value'])."'";
	}

	if ($query!='') {	#add insert/update to query.
		if ($action=='INSERT') $query="INSERT INTO $FORM[table] SET ".$query;
		elseif ($action=='UPDATE') $query="UPDATE $FORM[table] SET ".$query." WHERE $FORM[primary]='".$FORM['fields'][$FORM['primary']]['value']."'";
		else { $FORM['error_text']="prepere_sql_FORM ERROR: Action name error."; return false; }
	}

 return $query;
}


############################################################################################
function FORM_set_value(&$fld, $method='') {
	if ( isset($fld['value']) ) return $fld['value'];
	elseif ( $fld['type']=='upload' AND $_FILES[$fld['name']]['name']!='' ) $fld['value']=$_FILES[$fld['name']];
	elseif ( $method=='GET' AND isset($_GET[$fld['name']]) ) $fld['value']=$_GET[$fld['name']];
	elseif ( isset($_POST[$fld['name']]) ) $fld['value']=$_POST[$fld['name']];
	
	if ($fld['trim']!==0 AND !$fld['notrim'] AND !is_array($fld['value'])) $fld['value']=trim($fld['value']);
 return $fld['value'];
}


//
function FORM_set_values(&$FORM) {
	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		if (!$fld['name']) $fld['name']=$key;	# set name
		$fld['value'] = FORM_set_value($fld, $FORM['method']);
	}
}


############################################################################################
function FORM_check(&$FORM) {	#takes config array and check form
 global $L_ERRORS;
	//echo "<pre>"; print_r($FORM); echo "</pre>";
	
	if (!$FORM['fields']) return;
	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		if (!$fld['name']) $fld['name']=$key;	# set name
		
		$fld['value'] = FORM_set_value($fld, $FORM['method']);

		if ($fld['required'] AND $fld['type'] AND $fld['value']=='') {
			if ($fld['type']=='select') $fld['error']=sprintf($L_ERRORS['please_select'], "<u><b>$fld[title]</b></u>");
			else $fld['error']=sprintf($L_ERRORS['required'], "<u><b>$fld[title]</b></u>");
		
		}elseif ($fld['value']!='') {
			if ($fld['minlength'] AND (strlen($fld['value']) < $fld['minlength']) ) $fld['error']=sprintf($L_ERRORS['minimum'], "<u><b>$fld[title]</b></u>", "<b>$fld[minlength]</b>");
			if ($fld['maxlength'] AND (strlen($fld['value']) > $fld['maxlength']) ) $fld['error']=sprintf($L_ERRORS['maximum'], "<u><b>$fld[title]</b></u>", "<b>$fld[maxlength]</b>");

			if ($fld['ereg'] AND !ereg($fld['ereg'], $fld[value]))
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['regexp'] AND !preg_match($fld['regexp'], $fld[value]))
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['ercheck']=="email" AND !eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $fld[value]) )	
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['ercheck']=="url" AND  !eregi("^((ht|f)tp://)?((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $fld[value]))
				$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");

			if ($fld['ercheck']=="date") {
				if ( ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4})", $fld['value'], $regs) ) {
					if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
					$day=$regs[1]; $month=$regs[2]; $year=$regs[3];
				}elseif ( ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $fld['value'], $regs) ) {
					$day=$regs[3]; $month=$regs[2]; $year=$regs[1];
				}else $date_error=1;
				
				if ( $date_error OR !@checkdate($month, $day, $year) ) {
					$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");
				}
			}
			if ($fld['ercheck']=="datetime") {
				if ( ereg("([0-9]{1,2})[./]([0-9]{1,2})[./]([0-9]{2,4}) ([0-9]{1,2}):([0-9]{1,2})", $fld['value'], $regs) ) {
					if (strlen($regs[3])==2) $regs[3]="20$regs[3]";
					$day=$regs[1]; $month=$regs[2]; $year=$regs[3]; 
				}elseif ( ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})", $fld['value'], $regs) ) {
					$day=$regs[3]; $month=$regs[2]; $year=$regs[1];
				}else $date_error=1;
				
				if ( $date_error OR !@checkdate($month, $day, $year) OR $regs[4]>23 OR $regs[4]<0 OR $regs[5]>59 OR $regs[5]<0 ) {
					$fld['error']=sprintf($L_ERRORS['not_valid'], "<u><b>$fld[title]</b></u>");
				}
			}
			if ($fld['type']=='upload' AND is_array($fld['value'])) {
				if ( $fld['error']=FORM_check_upload_file($fld['value'], $fld['max_size']) ) {
					$fld['error']="<u><b>$fld[title]</b></u> $fld[error]";
				}			

				# check image
				//if ($fld['ercheck']=="image" AND $fld['error2']=FORM_check_image($fld['value']['tmp_name'], $fld['max_x'], $fld['max_y'], $fld['types_ar'], $fld['img_info'])) {
				if ($fld['ercheck']=="image" AND $fld['error2']=FORM_check_image($fld['value'], $fld['max_x'], $fld['max_y'], $fld['types_ar'], $fld['img_info'])) {
					$fld['error']=($fld['error']?"$fld[error]<br>":''). "<u><b>$fld[title]</b></u> $fld[error2]";
				}
			}
			
		}
		if ($fld['error'] AND $fld['custom_error']) $fld['error']=$fld['custom_error'];
		
		# Add error to $FORM['error_text']
		if ($fld['error']) $FORM['error_text'].=$fld['error'].'<br>';
	}
	if ($FORM['error_text']) $FORM['error_text']="<u>$L_ERRORS[error_text]</u><br>$FORM[error_text]";	# Add error text title
}


############################################################################################
function FORM_print(&$FORM) {	#takes config array and prints form
	echo theme_form($FORM['title'], FORM_set(&$FORM), $FORM['width']);
}

############################################################################################
function FORM_set(&$FORM) {
	if (!$FORM) return;
	parse_FORM(&$FORM);

	$FORM['HTML']="$FORM[form_tag]\n<table width='$FORM[width]' border=0 cellspacing=1 cellpadding=0 align=center class=t_form>";
	//if ($FORM['title']) $FORM['HTML'].="<tr><th colspan=3>$FORM[title]</th></tr>\n";
	if ($FORM['error_text']) $FORM['HTML'].="<tr><td colspan=3 style='color:red'>$FORM[error_text] <embed src='c:/WINDOWS/Media/chord.wav' autostart=true hidden=true loop=false></td></tr>\n";

	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		//echo "<div dir=ltr align=left><pre>"; print_r($fld); echo "</pre></div>";

		# Start row/fieldset
		if ($fld['type']=='fieldset_start') { 
			$fieldset=true; $vertical=$fld['vertical'];
			$FORM['HTML'].="<tr><td colspan=3><fieldset>".($fld['title']?"<legend".($fld['align']?" align='$fld[align]'":'').">$fld[title]</legend>":'')."<table".($fld['width']?" width='$fld[width]'":'')." border=0 cellspacing=1 cellpadding=0 style='margin:5px'>";
		}elseif ($fld['type']=='row_start') { 
			$row=true; $rowStart=true; $vertical=$fld['vertical'];
			$FORM['HTML'].="<tr".($fld['add']?" $fld[add]":'').">".($fld['title']?"<td>$fld[status_img]</td><td>$fld[title]</td><td>":'<td colspan=3>')."<table".($fld['width']?" width='$fld[width]'":'')." border=0 cellspacing=0 cellpadding=0><tr>";
		}elseif ($fld['type']=='cell_start') { 
			$cell=true;
			$FORM['HTML'].="<tr valign=top><td colspan=3".($fld['align']?" align='$fld[align]'":'').">";
		}elseif ($fld['type'] AND !$row AND !$cell AND $fld['type']!="hidden") $FORM['HTML'].="<tr valign=top>";

		if ($vertical AND !$fld['vertical']) $fld['vertical']=$vertical;
		
		# Show cells
		if ($fld['type']=='clear')	$FORM['HTML'].= "<td colspan=3>&nbsp;</td>";
		elseif ($fld['type']=='note')	$FORM['HTML'].= "<td colspan=3>$fld[html]</td>";
		elseif ($fld['type']=='center')	$FORM['HTML'].= "<td colspan=3 align=center>$fld[html]</td>";
		elseif ($fld['type']=='line')	$FORM['HTML'].= "<td colspan=3><hr size=1></td>";
		elseif ($fld['type']=='header')	$FORM['HTML'].= "<th colspan=3>$fld[html]</th>";
		elseif ($fld['type']=='spaw')	$FORM['HTML'].= "<td>$fld[status_img]</td><td>$fld[title]</td><td></td></tr></tr><td colspan=3>$fld[html]</td>";
		elseif ($fld['type']=='submit' AND !$cell) $FORM['HTML'].= "<td colspan=3 align=center>$fld[html]</td>";
		elseif ($fld['type']=='hidden' OR $fld['type']=="cell") $FORM['HTML'].= $fld['html'];
		elseif ( $fld['type'] AND !in_array($fld['type'],array('fieldset_start','fieldset_end','row_start','row_end','cell_start','cell_end','hidden','cell','header','line','clear','note','center','row')) ) {
			if ($fld['vertical']) $FORM['HTML'].="<td colspan=3>".($fld['title']?"$fld[status_img]$fld[title] ":'').($fld['notes']?"<span class=sm11>$fld[notes]</span>":'').(($fld['title'] OR $fld['notes'])?"<br>":'')."$fld[html]</td>";
			elseif ($cell) $FORM['HTML'].="$fld[html] ";
			else 
				if ($rowStart) {$FORM['HTML'].="<td></td><td></td><td>$fld[html] ".($fld['notes']?"<span class=sm11>$fld[notes]</span>":'')."&nbsp;</td>"; $rowStart=false;
				}else $FORM['HTML'].="<td>$fld[status_img]</td><td>".($rowStart?'':$fld['title'])."</td><td>$fld[html] ".($fld['notes']?"<span class=sm11>$fld[notes]</span>":'')."&nbsp;</td>";
		}else $FORM['HTML'].= $fld['html'];

		# End row/fieldset
		if	   ($fld['type']=='fieldset_end') { $FORM['HTML'].="</table></fieldset></td>"; $fieldset=$vertical=false; }
		elseif ($fld['type']=='row_end') { $FORM['HTML'].="</tr></table></td>"; $row=$vertical=false; }
		elseif ($fld['type']=='cell_end') { $FORM['HTML'].="</td>"; $cell=false; }
		elseif ($fld['type'] AND !$row AND !$cell AND $fld['type']!="hidden" AND $fld['type']!="row") $FORM['HTML'].="</tr>\n";
	}

	$FORM['HTML'].="</form></table>";
 return $FORM['HTML'];
}


############################################################################################
# Set html for all form fields and title
function parse_FORM(&$FORM) {
 global $CFG;
	if (!$FORM) return;
	if (!$FORM['method']) $FORM['method']='POST';		# Set default metod to POST

	if ($FORM['js_check'] AND !$FORM['onsubmit']) $FORM['onsubmit']="return ".($FORM['js_check']==2?'validateCompleteForm':'validateStandard')."(this);";
	if (!$FORM['form_name']) $FORM['form_name']='form1';
	
	$FORM['form_tag']="<form id='$FORM[form_name]' name='$FORM[form_name]'"
		.($FORM['action']?" action='$FORM[action]'":'')
		.($FORM['method']?" method='$FORM[method]'":'')
		.($FORM['onsubmit']?" onSubmit=\"$FORM[onsubmit]\"":'')
		.($FORM['form_tag_add']?" $FORM[form_tag_add]":'')
		.">";

	if ($FORM['js_check'] AND !$GLOBALS['f_jsval_indluded']) {
		$FORM['form_tag'].="<SCRIPT language=javascript src='$CFG[WEB]/inc/forms/jsval.js'></SCRIPT>";
		$GLOBALS['f_jsval_indluded']=true;
	}	

	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		if (!$fld['name'] AND !is_numeric($fld['name'])) $fld['name']=$key;
		FORM_set_fld(&$fld, $FORM['js_check']);	# Set field html

		# Set title
		//if ($fld['required']) $fld['title'].="<font color=red>*</font>";
		if ($fld['title'] AND $fld['title']!='&nbsp;' AND $fld['type']!='row_start') $fld['title']="<label".($fld['name']?" for='$fld[name]'":'').($fld['error']?" class=error":'').">"
			//.($fld['required']?"<img id='$fld[name]_status' src='inc/forms/required.gif' border=0 width=14 height=14 align=top alt='Required field'>":"<img id='$fld[name]_status' src='inc/forms/0.gif' border=0 width=14 height=14 align=top>")
			."$fld[title]:"
			//.($fld['required']?'<font color=red>*</font>':'')
			."</label>&nbsp;";

		//if ($fld['notes']) $fld['title'].="<div class=sm11>$fld[notes]</div>";
		
		if ($fld['title']) $fld['status_img']=($fld['required']?"<img id='$fld[name]_status' src='inc/forms/required.gif' border=0 width=14 height=14 align=top alt='Required field'>":"<img id='$fld[name]_status' src='inc/forms/0.gif' border=0 width=14 height=14 align=top>");
	}
}

############################################################################################
# takes field array and set $fld[html] and $fld[value]
function FORM_set_fld($fld, $js_check=false) {
 global $CFG;
	if (!isset($fld['value']) AND isset($GLOBALS[$fld['name']])) $fld['value']=$GLOBALS[$fld['name']];

	if ($fld['type']=='html' OR $fld['type']=='cell') if ($fld['data'] AND !$fld['html']) $fld['html'] = &$fld['data'];

	if ($js_check AND ($fld['required'] OR $fld['ercheck'] OR $fld['regexp'] OR $fld['minlength'] OR $fld['type']=='date')) $js_check_add="onBlur='validate_one(this)'"
					.(($fld['type']=='select') ? " onChange='validate_one(this)'":'')
					.(($fld['type']=='radio') ? " onClick='validate_one(this)'":'')
					.(($fld['type']=='date') ? " onChange='validate_one(this)' onkeyup='validate_one(this)'":'')
					.($fld['title'] ? " realname='$fld[title]'":'')
					.($fld['minlength'] ? " minlength={$fld[minlength]}":'')
					.($fld['required'] ? " required=1":'')
					.(($fld['ercheck']=='email') ? " regexp=email":'')
					.(($fld['ercheck']=='date') ? " regexp=date":'')
					.($fld['regexp'] ? " regexp=\"$fld[regexp]\"":'');

	if ($fld['error']) $fld['add'].=" class=error";
										
	if (!$fld['type']) return;
	elseif ($fld['type']=='txt')		$fld['html'] = text2html($fld['value']);
	elseif ($fld['type']=='text')		$fld['html'] = "<input type=text class=text id='$fld[name]' name='$fld[name]' size='$fld[size]' value=\"".htmlspecialchars($fld['value'])."\"".($fld['maxlength'] ? " maxlength=$fld[maxlength]":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
	elseif ($fld['type']=='password') 	$fld['html'] = "<input type=password class=text id='$fld[name]' name='$fld[name]' size='$fld[size]'".($fld['maxlength'] ? " maxlength=$fld[maxlength]":'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
	elseif ($fld['type']=='checkbox') 	$fld['html'] = "<input type=checkbox class=inputc id='$fld[name]' name='$fld[name]' class=inputc value=1".($fld['value'] ?' checked':'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
	elseif ($fld['type']=='submit') 	$fld['html'] = "<input type=submit id='$fld[name]' name='$fld[name]' value='".$fld['title']."'".($fld['add']?" $fld[add]":'').">";
	elseif ($fld['type']=='button') 	$fld['html'] = "<input type=button id='$fld[name]' name='$fld[name]' value='".$fld['title']."'".($fld['add']?" $fld[add]":'').">";
	elseif ($fld['type']=='hidden') 	$fld['html'] = "<input type=hidden id='$fld[name]' name='$fld[name]' value=\"".htmlspecialchars($fld['value'])."\"".($fld['add']?" $fld[add]":'').">";
	elseif ($fld['type']=='textarea') 	$fld['html'] = "<textarea id='$fld[name]' name='$fld[name]' rows={$fld[rows]} cols={$fld[cols]}".($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">".htmlspecialchars($fld[value])."</textarea>";
	elseif ($fld['type']=='upload') 	{
		$fld['html'] = "<input type=file class=text id='$fld[name]' name='$fld[name]' size='$fld[size]'".($fld['add']?" $fld[add]":'').">";
		if ($fld['max_size'] OR 'ercheck'=='image') $fld['html'].="<br><font class=sm10>("
			.($fld['max_size'] ? "Max file size <b>$fld[max_size] Kb</b>.":'')
			.(($fld['max_x'] AND $fld['max_y']) ? " Max image size <b>$fld[max_x]x$fld[max_y]</b>.":'')
			.(is_array($fld['types_ar']) ? " Alowed image types <b>".implode(',',$fld['types_ar'])."</b>" :'')
			.")</font>";
	}elseif ($fld['type']=='select') {
		$select_html.="<select id='$fld[name]' name='$fld[name]'".($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">";
		if (is_array($fld['values'])) foreach ($fld['values'] as $value=>$text) $select_html.="<option value=\"".htmlspecialchars($value)."\"".($fld['value']=="$value"?' selected':'').">".htmlspecialchars($text);
		$select_html.="</option></select>";
		$fld['html'] = $select_html;
	}elseif ($fld['type']=="radio") {
		foreach ($fld['values'] as $value=>$text) $radio_html.="<input type=radio class=inputc name='$fld[name]' value=\"".htmlspecialchars($value)."\"".( (isset($fld[value]) AND $fld[value]==$value) ? ' checked':'').($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'')."> $text".($fld['vertical']?'<br>':' &nbsp; &nbsp;');
		$fld['html'] = "<div style='padding-top:4px'>$radio_html";

	}elseif ($fld['type']=='date' OR $fld['type']=='datetime') {
		if ($fld['value']=='0000-00-00') $fld['value']='';
		# set default format
		if (!$fld['format'] AND $fld['type']=='date') $fld['format']='%d/%m/%Y';
		elseif (!$fld['format'] AND $fld['type']=='datetime') $fld['format']='%d/%m/%Y %H:%M';

		if (!$GLOBALS['f_calendar_indluded']) {	# include calendar js & css
			$fld['html'].="<SCRIPT type=text/javascript src='$CFG[WEB]/inc/forms/calendar.js'></SCRIPT>"
						."<LINK media=all type=text/css rel=stylesheet href='$CFG[WEB]/inc/forms/calendar-win2k-1.css'>";
			$GLOBALS['f_calendar_indluded']=true;
		}
		# [add readOnly attribute if you don't want user enter date manually]
		$fld['html'].="<INPUT id='$fld[name]' name='$fld[name]' size='".($fld['size']?$fld['size']:10)."' title='Date format: YYYY-MM-DD' value=\"".htmlspecialchars($fld[value])."\"".($fld['add']?" $fld[add]":'').($js_check_add?" $js_check_add":'').">"
					."<IMG id='$fld[name]_trigger' title='Select Date' src='$CFG[WEB]/inc/forms/cal.gif' align=absmiddle style='cursor:pointer; border:1px solid orange;' onmouseover=\"this.style.background='orange';\" onmouseout=\"this.style.background=''\">"
					."<SCRIPT type=text/javascript>\n"
						."Calendar.setup({"
							."inputField	: '$fld[name]',"
							."button		: '$fld[name]_trigger',"
							.($fld['type']=='datetime' ?	# show date&time
											 "ifFormat		: '$fld[format]',"
											."showsTime		: true,"
											."timeFormat	: '24',"
							 /*date only*/	:"ifFormat		: '$fld[format]'," )
							."align			: 'Tl',"
							."weekNumbers	: false,"
							."singleClick	: true"
						."});\n"
					."</SCRIPT>";
	
	}elseif ($fld['type']=='spaw') {
		$spaw = new SPAW_Wysiwyg($fld['name'] /*name*/, $fld['value'] /*value*/, '' /*language*/, ($fld['toolbar']?$fld['toolbar']:'full1')/*toolbar mode*/, 'default' /*theme*/, ($fld['width']?$fld['width']:'100%') /*width*/, ($fld['height']?$fld['height']:'100px') /*height*/ ,'' /*stylesheet file*/,''/*dropdown_data*/, $fld['designOnly']);
		$fld['html'] = '<span dir=ltr>'.$spaw->getHtml().'</span>';
	}
	
	if ($fld['prefix']) $fld['html'] = $fld['prefix'].$fld['html'];

	//if ($fld['required']) $fld['html'].= "<img id='$fld[name]_status' src='inc/forms/required.gif' align=top border=0 alt='Required field'>";

	//if ($fld['help']) {
	//	$fld['html'] .= "<script>document.write('".addslashes("<img src='inc/forms/help.gif' onmouseover=\"tip('".addslashes($fld['help'])."')\" onmouseout='hidetip()' border=0 align=absmiddle>")."');</script>";	
		//$fld['html'] .= "<img src='inc/forms/help.gif' onmouseover=\"tip('".addslashes($fld['help'])."')\" onmouseout='hidetip()' border=0 align=absmiddle>";
	//}
	
	if ($fld['suffix']) $fld['html'].= $fld['suffix'];
 return $fld['html'];
}


############################################################################################
function array2select($select_name, $array, $size=1, $multiple=0, $add='') {
	$select="<select size='$size' name='$select_name'".($multiple?' multiple':'').($add?" $add":'').">\n";
		while($key=@each($array)) $select.="<option value='$key[0]'".($GLOBALS[$select_name]=="$key[0]" ? ' selected':'').">".htmlspecialchars($key[1])."\n";
	$select.="</option></select>";
 return $select;
}


###############################################################################
function FORM_move_upload_files(&$FORM) {
	foreach ($FORM['fields'] as $key=>$value) {
		$fld=&$FORM['fields'][$key];
		if (!$fld['name']) $fld['name']=$key;	# set name

		if ( $fld['type']=='upload'	AND is_array($fld['value'])) FORM_move_upload_file($fld);		
	}	
}

###############################################################################
function FORM_move_upload_file($fld) {
	if (!$fld['target']) return "Target not specified.";
	if (!is_file($fld['value']['tmp_name'])) return "Temp file not found.";

	if (is_dir($fld['target'])) return "Directory named <b>$fld[target]</b> found, file not uploaded.";
	//elseif (is_file($fld['target']) AND !$_POST[overwrite]) $error.="Error: File <b>$up[name]</b> already exist, file not uploaded.<br>";
	else {
		if (move_uploaded_file($fld['value']['tmp_name'], $fld['target'])) chmod($fld['target'], 0777);
		else return "Error saving uploaded file <b>".basename($fld['target'])."</b>.";
	}
}
/*
###############################################################################
function FORM_move_upload_file($fld) {
	if (!$fld['target']) error("FORM_move_upload_file: Target not specified.");
	if (!is_file($fld['value']['tmp_name'])) error("FORM_move_upload_file: Temp file not found.");

	if (is_dir($fld['target'])) error("FORM_move_upload_file: Directory named <b>$fld[target]</b> found, file not uploaded.");
	//elseif (is_file($fld['target']) AND !$_POST[overwrite]) $error.="Error: File <b>$up[name]</b> already exist, file not uploaded.<br>";
	else {
		if (move_uploaded_file($fld['value']['tmp_name'], $fld['target'])) chmod($fld['target'], 0777);
		else error("FORM_move_upload_file: Error saving uploaded file <b>".basename($fld['target'])."</b>.");
	}
}
*/


###############################################################################
function FORM_check_upload_file($up, $max_size=0) {
	# Check error codes
	if ($up['error']==1) { return "The uploaded file exceeds the upload_max_filesize directive in php.ini"; }
	if ($up['error']==2) { return "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form."; }
	if ($up['error']==3) { return "The uploaded file was only partially uploaded."; }
	if ($up['error']==4) { return "No file was uploaded."; }
	if ($up['error']==5) { return "Missing a temporary folder."; }

	# Check file size
	if ($up['size']==0) { return "Upload Error: Uploaded file size is 0."; }
	if ($max_size AND ($up['size'] > ($max_size*1024)) ) {
		return "Uploaded file size is too big (".number_format(($up['size']/1024))." Kb), maximum alowed ".number_format($max_size)." Kb.";
	}
}

###############################################################################
/*function FORM_check_image($img, $max_x=0, $max_y=0, $types_ar=0, &$img_info) {
	if (!is_file($img)) return "File '<b>$img</b>' not found.";
	if (!$types_ar) $types_ar=array('jpg', 'gif', 'png');	# set default alowed types
	
	$img_info=GetImageSize($img);
	$img_types=array('', 'gif', 'jpg', 'png', 'swf', 'psd', 'bmp');
	$img_x  = $img_info['width'] = &$img_info[0];
	$img_y  = $img_info['height']= &$img_info[1];
	$img_ext= $img_info['ext']   = $img_types[$img_info[2]];
	
	# Check image type
	if (!in_array($img_ext, $types_ar)) return "Image type must be <b>".implode('/',$types_ar)."</b>";

	# Check image size
	if ( $max_x AND $max_y AND ($img_x>$max_x OR $img_y>$max_y) ) {
		return "Image size is too big ({$img_x}x{$img_y}), maximum <b>{$max_x}x{$max_y}</b>.";
	}	
}*/

//
function FORM_check_image(&$IMG, $width=0, $height=0, $types_ar=0) {
	if (!is_file($IMG['tmp_name'])) return "File '<b>$IMG[tmp_name]</b>' not found.";
	if (!$types_ar) $types_ar=array('jpg', 'gif', 'png');	# set default alowed types
	# add image info
	$IMG = $IMG + FORM_image_info($IMG['tmp_name']);
	# Check image type
	if (!in_array($IMG['ext'], $types_ar)) return "Image type must be <b>".implode('/',$types_ar)."</b>";
	# Check image size
	if ( $width AND $height AND ($IMG['width']>$width OR $IMG['height']>$height) ) {
		return "Image size is too big ({$IMG['width']}x{$IMG['height']}), maximum <b>{$width}x{$height}</b>.";
	}
}
//
function FORM_image_info($img) {
	if (!is_file($img)) return;
	$img_types=array('', 'gif', 'jpg', 'png', 'swf', 'psd', 'bmp');
	$IMG=getimagesize($img);
	$IMG['width'] = $IMG[0];
	$IMG['height']= $IMG[1];
	$IMG['ext']   = $img_types[$IMG[2]];
	unset($IMG[0],$IMG[1],$IMG[2],$IMG[3]);
 return $IMG;
}


//
function resize_img($src, $dst, $width, $height, $IMG='') {
 global $CFG;
	if (!is_file($src)) return;
	if (!$IMG) $IMG=FORM_image_info($src);
	if (!$IMG['ext']) return;

	// Get new dimensions
	$ratio = $IMG['width']/$IMG['height'];
	if ($width/$height > $ratio) $width = round( $height*$ratio );
	else $height = round( $width/$ratio );

	if ($CFG['ImageMagicCMD']) return resize_img_imagemagic($src, $dst, $width, $height);
	elseif (function_exists('imagecopyresampled')) return resize_img_gd($src, $dst, $width, $height, $IMG);
	else error("resize_img: No resize function");
}

//
function resize_img_imagemagic($src, $dst, $width, $height) {
 global $CFG;
	system("$CFG[ImageMagicCMD] -quality 80 -thumbnail {$width}x{$height} $src $dst");	//-geometry
	if (file_exists($dst)) return 1;
}

//
function resize_img_gd($src, $dst, $width, $height, $IMG) {
	ini_set('memory_limit', '128M');
	if     ($IMG['ext']=="jpg" AND function_exists('imagecreatefromjpeg')) $img_src=imagecreatefromjpeg($src);
	elseif ($IMG['ext']=="gif" AND function_exists('imagecreatefromgif'))  $img_src=imagecreatefromgif($src);
	elseif ($IMG['ext']=="png" AND function_exists('imagecreatefrompng'))  $img_src=imagecreatefrompng($src);
	if (!$img_src) return;
	$img_des = imagecreatetruecolor($width,$height);
	imagecopyresampled($img_des, $img_src, 0, 0, 0, 0, $width, $height, $IMG['width'], $IMG['height']);
	if ($width<400 && $height<400) $img_des=UnsharpMask($img_des);
 return imagejpeg($img_des, $dst, 85);	# save
}


//
function img_write($src, $dst=null, $size=null, $unsharp=1) {
	if (!$src OR !is_file($src)) return false;
	ini_set('memory_limit', '128M');
	list($width,$height)=explode('x',$size);

	$img_types=array('', 'gif', 'jpg', 'png', 'swf', 'psd', 'bmp');
	$srcinf=getimagesize($src);
	$I['src_width'] = $srcinf[0];
	$I['src_height']= $srcinf[1];
	$I['src_ext']   = $img_types[$srcinf[2]];
	if (!$I['src_width'] OR !$I['src_height'] OR !$I['src_ext'] OR !ereg('jpg|png|gif',$I['src_ext']) ) return false;
	

	if ($dst AND eregi('\.(jpg|png|gif)$', $dst, $regs)) $I['target_ext']=$regs[1];
	else $I['target_ext']='jpg';	# default output type
	
	if ($dst AND $I['src_ext']==$I['target_ext'] AND (!$width OR $I['src_width']<=$width) AND (!$height OR $I['src_height']<=$height)) {
		return copy($src, $dst);	# no need to convert - copy
	}

	$imagecreate="imagecreatefrom".str_replace('jpg', 'jpeg', $I['src_ext']);
	$imagesave  ="image".str_replace('jpg', 'jpeg', $I['target_ext']);
	if (!function_exists($imagecreate) OR !function_exists($imagesave)) { echo "image type not supported ($I[src_ext]/$I[target_ext])"; return false; }
	
	if (($width AND $I['src_width']>$width) OR ($height AND $I['src_height']>$height) OR $I['src_ext']!=$I['target_ext']) {
		if (!$img_src=$imagecreate($src)) return false;
		
		if (($width AND $I['src_width']>$width) OR ($height AND $I['src_height']>$height)) {
			if (!$width)  $width =$I['src_width'];
			if (!$height) $height=$I['src_height'];

			# new_img dimensions
			$ratio = $I['src_width']/$I['src_height'];
			if ($width/$height > $ratio) $width=round($height*$ratio);
			else $height=round($width/$ratio);

			$new_img = imagecreatetruecolor($width, $height);
			imagecopyresampled($new_img, $img_src, 0, 0, 0, 0, $width, $height, $I['src_width'], $I['src_height']);
			if ($unsharp AND $width<400 AND $height<400) $new_img=UnsharpMask($new_img);
		}else $new_img=&$img_src;
		
		if ($I['target_ext']=='jpg') return imagejpeg($new_img, $dst, 85);
		elseif ($I['target_ext']=='png') return imagepng($new_img, $dst, 9);
		elseif ($I['target_ext']=='gif') return imagegif($new_img, $dst);
	}
}

//
function UnsharpMask($img, $amount=25, $radius=1, $threshold=3) {
/*	Amount:    (typically 50 - 200) 
	Radius:    (typically 0.5 - 1) 
	Threshold: (typically 0 - 5)
	Unsharp Mask for PHP - version 2.1.1. http://vikjavev.no/computing/ump.php   Unsharp mask algorithm by Torstein H?nsi 2003-07. thoensi_at_netcom_dot_no. 
*/
	#! $img is an image that is already created within php using imgcreatetruecolor

	// Attempt to calibrate the parameters to Photoshop:
	if ($amount > 500) $amount = 500;
	$amount = $amount * 0.016;
	if ($radius > 50) $radius = 50;
	$radius = $radius * 2;
	if ($threshold > 255) $threshold = 255;
	
	$radius = abs(round($radius));     // Only integers make sense.
	if ($radius == 0) { return $img; imagedestroy($img); break; }
	$w = imagesx($img); $h = imagesy($img);
	$imgCanvas = imagecreatetruecolor($w, $h);
	$imgBlur = imagecreatetruecolor($w, $h);
	
	// Gaussian blur matrix:
	//    1    2    1
	//    2    4    2
	//    1    2    1
	if (function_exists('imageconvolution')) { // PHP >= 5.1
		$matrix = array(
			array( 1, 2, 1 ),
			array( 2, 4, 2 ),
			array( 1, 2, 1 )
		);
		imagecopy ($imgBlur, $img, 0, 0, 0, 0, $w, $h);
		imageconvolution($imgBlur, $matrix, 16, 0);
	}else {
		// Move copies of the image around one pixel at the time and merge them with weight according to the matrix. The same matrix is simply repeated for higher radii.
		for ($i = 0; $i < $radius; $i++) {
			imagecopy ($imgBlur, $img, 0, 0, 1, 0, $w - 1, $h); // left
			imagecopymerge ($imgBlur, $img, 1, 0, 0, 0, $w, $h, 50); // right
			imagecopymerge ($imgBlur, $img, 0, 0, 0, 0, $w, $h, 50); // center
			imagecopy ($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h);
			imagecopymerge ($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333 ); // up
			imagecopymerge ($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25); // down
		}
	}

	if($threshold>0){
		// Calculate the difference between the blurred pixels and the original and set the pixels
		for ($x = 0; $x < $w-1; $x++)    { // each row
			for ($y = 0; $y < $h; $y++)    { // each pixel
					
				$rgbOrig = ImageColorAt($img, $x, $y);
				$rOrig = (($rgbOrig >> 16) & 0xFF);
				$gOrig = (($rgbOrig >> 8) & 0xFF);
				$bOrig = ($rgbOrig & 0xFF);
				
				$rgbBlur = ImageColorAt($imgBlur, $x, $y);
				
				$rBlur = (($rgbBlur >> 16) & 0xFF);
				$gBlur = (($rgbBlur >> 8) & 0xFF);
				$bBlur = ($rgbBlur & 0xFF);
				
				// When the masked pixels differ less from the original than the threshold specifies, they are set to their original value.
				$rNew = (abs($rOrig - $rBlur) >= $threshold)
					? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig))
					: $rOrig;
				$gNew = (abs($gOrig - $gBlur) >= $threshold)
					? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig))
					: $gOrig;
				$bNew = (abs($bOrig - $bBlur) >= $threshold)
					? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig))
					: $bOrig;
				
				if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) {
					$pixCol = ImageColorAllocate($img, $rNew, $gNew, $bNew);
					ImageSetPixel($img, $x, $y, $pixCol);
				}
			}
		}
	}
	else{
		for ($x = 0; $x < $w; $x++)    { // each row
			for ($y = 0; $y < $h; $y++)    { // each pixel
				$rgbOrig = ImageColorAt($img, $x, $y);
				$rOrig = (($rgbOrig >> 16) & 0xFF);
				$gOrig = (($rgbOrig >> 8) & 0xFF);
				$bOrig = ($rgbOrig & 0xFF);
				
				$rgbBlur = ImageColorAt($imgBlur, $x, $y);
				
				$rBlur = (($rgbBlur >> 16) & 0xFF);
				$gBlur = (($rgbBlur >> 8) & 0xFF);
				$bBlur = ($rgbBlur & 0xFF);
				
				$rNew = ($amount * ($rOrig - $rBlur)) + $rOrig;
					if($rNew>255){$rNew=255;}
					elseif($rNew<0){$rNew=0;}
				$gNew = ($amount * ($gOrig - $gBlur)) + $gOrig;
					if($gNew>255){$gNew=255;}
					elseif($gNew<0){$gNew=0;}
				$bNew = ($amount * ($bOrig - $bBlur)) + $bOrig;
					if($bNew>255){$bNew=255;}
					elseif($bNew<0){$bNew=0;}
				$rgbNew = ($rNew << 16) + ($gNew <<8) + $bNew;
					ImageSetPixel($img, $x, $y, $rgbNew);
			}
		}
	}
	imagedestroy($imgCanvas);
	imagedestroy($imgBlur);
 return $img;
}


?>