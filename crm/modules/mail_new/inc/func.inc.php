<?

/** 
 * rcube_browser
 * 
 * Provide details about the client's browser based on the User-Agent header
 *
 * @package Core
 */
class rcube_browser {
	function __construct() {
		$HTTP_USER_AGENT = strtolower($_SERVER['HTTP_USER_AGENT']);

		$this->ver = 0;
		$this->win = strstr($HTTP_USER_AGENT, 'win');
		$this->mac = strstr($HTTP_USER_AGENT, 'mac');
		$this->linux = strstr($HTTP_USER_AGENT, 'linux');
		$this->unix  = strstr($HTTP_USER_AGENT, 'unix');

		$this->opera = strstr($HTTP_USER_AGENT, 'opera');
		$this->ns4 = strstr($HTTP_USER_AGENT, 'mozilla/4') && !strstr($HTTP_USER_AGENT, 'msie');
		$this->ns  = ($this->ns4 || strstr($HTTP_USER_AGENT, 'netscape'));
		$this->ie  = !$this->opera && strstr($HTTP_USER_AGENT, 'compatible; msie');
		$this->mz  = strstr($HTTP_USER_AGENT, 'mozilla/5');
		$this->chrome = strstr($HTTP_USER_AGENT, 'chrome');
		$this->khtml = strstr($HTTP_USER_AGENT, 'khtml');
		$this->safari = !$this->chrome && ($this->khtml || strstr($HTTP_USER_AGENT, 'safari'));

		if ($this->ns || $this->chrome) {
			$test = preg_match('/(mozilla|chrome)\/([0-9.]+)/', $HTTP_USER_AGENT, $regs);
			$this->ver = $test ? (float)$regs[2] : 0;
		}
		else if ($this->mz) {
			$test = preg_match('/rv:([0-9.]+)/', $HTTP_USER_AGENT, $regs);
			$this->ver = $test ? (float)$regs[1] : 0;
		}
		else if ($this->ie || $this->opera) {
			$test = preg_match('/(msie|opera) ([0-9.]+)/', $HTTP_USER_AGENT, $regs);
			$this->ver = $test ? (float)$regs[2] : 0;
		}

		if (preg_match('/ ([a-z]{2})-([a-z]{2})/', $HTTP_USER_AGENT, $regs))
			$this->lang =  $regs[1];
		else
			$this->lang =  'en';

		$this->dom = ($this->mz || $this->safari || ($this->ie && $this->ver>=5) || ($this->opera && $this->ver>=7));
		$this->pngalpha = $this->mz || $this->safari || ($this->ie && $this->ver>=5.5) ||
			($this->ie && $this->ver>=5 && $this->mac) || ($this->opera && $this->ver>=7) ? true : false;
	}
}

// Fixes some content-type names
function rcmail_fix_mimetype($name)
{
  // Some versions of Outlook create garbage Content-Type:
  // application/pdf.A520491B_3BF7_494D_8855_7FAC2C6C0608
  if (preg_match('/^application\/pdf.+/', $name))
    $name = 'application/pdf';

  return $name;
}

function rcmail_message_body($MESSAGE, $attrib=array()) {
	if (!is_array($MESSAGE->parts) && empty($MESSAGE->body)) return '';
	//print_ar($MESSAGE,'$MESSAGE');exit;
	if (!$attrib['id']) $attrib['id'] = 'rcmailMsgBody';
	$safe_mode = !$MESSAGE->is_safe;
	//print_ar($safe_mode,'$safe_mode');exit;
	
	$out = '';
	$header_attrib = array();
	foreach ($attrib as $attr => $value)
		if (preg_match('/^headertable([a-z]+)$/i', $attr, $regs))
			$header_attrib[$regs[1]] = $value;

	if (!empty($MESSAGE->parts)) {
		foreach ($MESSAGE->parts as $i => $part) {
			// print_ar($part,'$part');
			if ($part->type == 'headers')
				$out .= rcmail_message_headers($MESSAGE,sizeof($header_attrib) ? $header_attrib : NULL, $part->headers);
			else if ($part->type == 'content' && $part->size) {
				if (empty($part->ctype_parameters) || empty($part->ctype_parameters['charset']))
					$part->ctype_parameters['charset'] = $MESSAGE->headers->charset;
				
				// fetch part if not available
				if (!isset($part->body)){
					$part->body = $MESSAGE->get_part_content($part->mime_id);
				}
				
				// Sometimes encoding is wrong. We will test body for possible base64 encoding
				if ($part->encoding != 'base64' && base64_decode($part->body, true)){
					$part->encoding = 'base64';
					$part->body = $MESSAGE->get_part_content($part->mime_id);
				}

				$body = rcmail_print_body($part, array('safe' => $safe_mode, 'plain' => false));

				if ($part->ctype_secondary == 'html') {
					$body = rcmail_html4inline($body, $attrib['id'], 'rcmBody', $attrs);
					$div_attr = array('class' => 'message-htmlpart');
					$style = array();

					if (!empty($attrs)) {
						foreach ($attrs as $a_idx => $a_val)
							$style[] = $a_idx . ': ' . $a_val;
						if (!empty($style))
							$div_attr['style'] = implode('; ', $style);
					}
					
					$out .= html::div('message-htmlpart', $body);
				}
				else
					$out .= html::div('message-part', $body);
			}
		}
	}
	else {
		$out .= html::div('message-part', html::tag('pre', array(),
							rcmail_plain_body(Q($MESSAGE->body, 'strict', false))));
	}
	/******************************************************/
	$ctype_primary = strtolower($MESSAGE->structure->ctype_primary);
	$ctype_secondary = strtolower($MESSAGE->structure->ctype_secondary);

	// list images after mail body
	if ($CONFIG['inline_images'] && $ctype_primary == 'multipart' && !empty($MESSAGE->attachments)){
		foreach ($MESSAGE->attachments as $attach_prop) {
			// Content-Type: image/*...
			if (preg_match('/^image\//i', $attach_prop->mimetype) ||
				// ...or known file extension: many clients are using application/octet-stream
				($attach_prop->filename &&
					preg_match('/^application\/octet-stream$/i', $attach_prop->mimetype) &&
					preg_match('/\.(jpg|jpeg|png|gif|bmp)$/i', $attach_prop->filename))
			) {
				$out .= html::tag('hr') . html::p(array('align' => "center"),
					html::img(array(
						'src' => $MESSAGE->get_part_url($attach_prop->mime_id),
						'title' => $attach_prop->filename,
						'alt' => $attach_prop->filename,
					)));
				}
		}
	}
	// tell client that there are blocked remote objects
	// if ($REMOTE_OBJECTS && !$safe_mode)
		// $OUTPUT->set_env('blockedobjects', true);
	
	$someBody = html::div($attrib, $out);
	// print_ar($someBody,'$someBody');exit;
	return $someBody;
}


# Handle links and citation marks in plain text message
# @param string	Plain text string
# @param boolean Text uses format=flowed
# @return string Formatted HTML string
function rcmail_plain_body($body, $flowed=false) {
	global $RCMAIL;

	// make links and email-addresses clickable
	$replacer = new rcube_string_replacer;

	// search for patterns like links and e-mail addresses
	$body = preg_replace_callback($replacer->link_pattern, array($replacer, 'link_callback'), $body);
	$body = preg_replace_callback($replacer->mailto_pattern, array($replacer, 'mailto_callback'), $body);

	// split body into single lines
	$a_lines = preg_split('/\r?\n/', $body);
	$quote_level = 0;
	$last = -1;

	// find/mark quoted lines...
	for ($n=0, $cnt=count($a_lines); $n < $cnt; $n++) {
		if ($a_lines[$n][0] == '>' && preg_match('/^(>+\s*)+/', $a_lines[$n], $regs)) {
			$q = strlen(preg_replace('/\s/', '', $regs[0]));
			$a_lines[$n] = substr($a_lines[$n], strlen($regs[0]));

			if ($q > $quote_level)
				$a_lines[$n] = $replacer->get_replacement($replacer->add(
					str_repeat('<blockquote>', $q - $quote_level))) . $a_lines[$n];
			else if ($q < $quote_level)
				$a_lines[$n] = $replacer->get_replacement($replacer->add(
					str_repeat('</blockquote>', $quote_level - $q))) . $a_lines[$n];
			else if ($flowed) {
				// previous line is flowed
				if (isset($a_lines[$last]) && $a_lines[$n]
					&& $a_lines[$last][strlen($a_lines[$last])-1] == ' ') {
					// merge lines
					$a_lines[$last] .= $a_lines[$n];
					unset($a_lines[$n]);
				}
				else
					$last = $n;
			}
		}
		else {
			$q = 0;
			if ($flowed) {
				// sig separator - line is fixed
				if ($a_lines[$n] == '-- ') {
					$last = $n;
				}
				else {
					// remove space-stuffing
					if ($a_lines[$n][0] == ' ')
						$a_lines[$n] = substr($a_lines[$n], 1);

					// previous line is flowed?
					if (isset($a_lines[$last]) && $a_lines[$n]
						&& $a_lines[$last] != '-- '
						&& $a_lines[$last][strlen($a_lines[$last])-1] == ' '
					) {
						$a_lines[$last] .= $a_lines[$n];
						unset($a_lines[$n]);
					}
					else {
						$last = $n;
					}
				}
				if ($quote_level > 0)
					$a_lines[$last] = $replacer->get_replacement($replacer->add(
						str_repeat('</blockquote>', $quote_level))) . $a_lines[$last];
			}
			else if ($quote_level > 0)
				$a_lines[$n] = $replacer->get_replacement($replacer->add(
					str_repeat('</blockquote>', $quote_level))) . $a_lines[$n];
		}

		$quote_level = $q;
	}

	$body = join("\n", $a_lines);

	// quote plain text (don't use Q() here, to display entities "as is")
	$table = get_html_translation_table(HTML_SPECIALCHARS);
	unset($table['?']);
	$body = strtr($body, $table);

	// colorize signature (up to <sig_max_lines> lines)
	$len = strlen($body);
	$sig_max_lines = $RCMAIL->config->get('sig_max_lines', 15);
	while (($sp = strrpos($body, "-- \n", $sp ? -$len+$sp-1 : 0)) !== false) {
		if ($sp == 0 || $body[$sp-1] == "\n") {
			// do not touch blocks with more that X lines
			if (substr_count($body, "\n", $sp) < $sig_max_lines)
				$body = substr($body, 0, max(0, $sp))
					.'<span class="sig">'.substr($body, $sp).'</span>';
			break;
		}
	}

	// insert url/mailto links and citation tags
	$body = $replacer->resolve($body);

	return $body;
}

# Convert the given message part to proper HTML which can be displayed the message view
# @param object rcube_message_part Message part
# @param array	Display parameters array 
# @return string Formatted HTML string
function rcmail_print_body($part, $p = array()) {
	global $RCMAIL;
	
	$data=array('type'=>&$part->ctype_secondary, 'body'=>&$part->body, 'id'=>&$part->mime_id);
	$data = array_merge($data, $p);
	//return $data['body'];
	
	if ($data['type'] == 'html' && $data['plain']) {
		include_once 'modules/mail_new/inc/html2text.php';
		$txt = new html2text($data['body'], false, true);
		$body = $txt->get_text();
		$part->ctype_secondary = 'plain';
	}
	// text/html
	else if ($data['type'] == 'html') {
		$body = rcmail_wash_html($data['body'], $data, $part->replaces);
		$part->ctype_secondary = $data['type'];
	}
	// text/enriched
	else if ($data['type'] == 'enriched') {
		$part->ctype_secondary = 'html';
		require_once 'modules/mail_new/inc/enriched.inc.php';
		//require_once('lib/enriched.inc');
		$body = Q(enriched_to_html($data['body']), 'show');
	}
	else {
		// assert plaintext
		$body = $part->body;
		$part->ctype_secondary = $data['type'] = 'plain';
	}

	// free some memory (hopefully)
	unset($data['body']);

	// plaintext postprocessing
	if ($part->ctype_secondary == 'plain')
		$body = rcmail_plain_body($body, $part->ctype_parameters['format'] == 'flowed');

	
	$data = array('type'=>&$part->ctype_secondary, 'body'=>&$body, 'id'=>&$part->mime_id);

	return $data['type'] == 'html' ? $data['body'] : html::tag('pre', array(), $data['body']);
}


# Cleans up the given message HTML Body (for displaying)
# @param string HTML
# @param array	Display parameters 
# @param array	CID map replaces (inline images)
# @return string Clean HTML
function rcmail_wash_html($html, $p = array(), $cid_replaces) {
	global $REMOTE_OBJECTS;
	
	//$p += array('safe' => false, 'inline_html' => true);
	$p['inline_html'] = true;
	// special replacements (not properly handled by washtml class)
	$html_search = array(
		'/(<\/nobr>)(\s+)(<nobr>)/i',	// space(s) between <NOBR>
		'/<title[^>]*>.*<\/title>/i',	// PHP bug #32547 workaround: remove title tag
		'/^(\0\0\xFE\xFF|\xFF\xFE\0\0|\xFE\xFF|\xFF\xFE|\xEF\xBB\xBF)/',	// byte-order mark (only outlook?)
		'/<html\s[^>]+>/i',			// washtml/DOMDocument cannot handle xml namespaces
	);
	$html_replace = array(
		'\\1'.' &nbsp; '.'\\3',
		'',
		'',
		'<html>',
	);
	$html = preg_replace($html_search, $html_replace, $html);

	// PCRE errors handling (#1486856), should we use something like for every preg_* use?
	if ($html === null && ($preg_error = preg_last_error()) != PREG_NO_ERROR) {
		$errstr = "Could not clean up HTML message! PCRE Error: $preg_error.";

		if ($preg_error == PREG_BACKTRACK_LIMIT_ERROR)
			$errstr .= " Consider raising pcre.backtrack_limit!";
		if ($preg_error == PREG_RECURSION_LIMIT_ERROR)
			$errstr .= " Consider raising pcre.recursion_limit!";

		raise_error(array('code' => 600, 'type' => 'php',
				'line' => __LINE__, 'file' => __FILE__,
				'message' => $errstr), true, false);
		return '';
	}

	// fix (unknown/malformed) HTML tags before "wash"
	$html = preg_replace_callback('/(<[\/]*)([^\s>]+)/', 'rcmail_html_tag_callback', $html);

	// charset was converted to UTF-8 in rcube_imap::get_message_part(),
	// -> change charset specification in HTML accordingly
	$charset_pattern = '(<meta\s+[^>]*content=)[\'"]?(\w+\/\w+;\s*charset=)([a-z0-9-_]+[\'"]?)';
	if (preg_match("/$charset_pattern/Ui", $html)) {
		$html = preg_replace("/$charset_pattern/i", '\\1"\\2'.RCMAIL_CHARSET.'"', $html);
	}
	else {
		// add meta content-type to malformed messages, washtml cannot work without that
		if (!preg_match('/<head[^>]*>(.*)<\/head>/Uims', $html))
			$html = '<head></head>'. $html;
		$html = substr_replace($html, '<meta http-equiv="Content-Type" content="text/html; charset='.RCMAIL_CHARSET.'" />', intval(stripos($html, '<head>')+6), 0);
	}
	
	// turn relative into absolute urls
	$html = rcmail_resolve_base($html);
	
	// clean HTML with washhtml by Frederic Motte
	$wash_opts = array(
		'show_washed' => false,
		'allow_remote' => $p['safe'],
		'blocked_src' => "./program/blocked.gif",
		'charset' => RCMAIL_CHARSET,
		'cid_map' => $cid_replaces,
		'html_elements' => array('body'),
	);

	if (!$p['inline_html']) {
		$wash_opts['html_elements'] = array('html','head','title','body');
	}
	if ($p['safe']) {
		$wash_opts['html_elements'][] = 'link';
		$wash_opts['html_attribs'] = array('rel','type');
	}

	// overwrite washer options with options from plugins
	if (isset($p['html_elements']))
		$wash_opts['html_elements'] = $p['html_elements'];
	if (isset($p['html_attribs']))
		$wash_opts['html_attribs'] = $p['html_attribs'];

	$GLOBALS['CFG']['show_errors']=0;
	// initialize HTML washer
	$washer = new washtml($wash_opts);

	if (!$p['skip_washer_form_callback'])
		$washer->add_callback('form', 'rcmail_washtml_callback');

	// allow CSS styles, will be sanitized by rcmail_washtml_callback()
	if (!$p['skip_washer_style_callback'])
		$washer->add_callback('style', 'rcmail_washtml_callback');

	$html = $washer->wash($html);
	// $REMOTE_OBJECTS = $washer->extlinks;
	
	$GLOBALS['CFG']['show_errors']=1;

	return $html;
}

/**
 * Callback function for washtml cleaning class
 */
function rcmail_washtml_callback($tagname, $attrib, $content, $washtml)
{
  require_once 'modules/mail_new/inc/html.php';
  switch ($tagname) {
	case 'form':
	  $out = html::div('form', $content);
	  break;

	case 'style':
	  // decode all escaped entities and reduce to ascii strings
	  $stripped = preg_replace('/[^a-zA-Z\(:;]/', '', rcmail_xss_entity_decode($content));

	  // now check for evil strings like expression, behavior or url()
	  if (!preg_match('/expression|behavior|javascript:|import[^a]/i', $stripped)) {
		//$washtml->get_config('allow_remote');
		$allow_remote = true;
		if (!$allow_remote && stripos($stripped, 'url('))
		  $washtml->extlinks = true;
		else
		  $out = html::tag('style', array('type' => 'text/css'), $content);
		break;
	  }

	default:
	  $out = '';
  }

  return $out;
}

# Callback function for HTML tags fixing
function rcmail_html_tag_callback($matches) {
	$tagname = $matches[2];
	$tagname = preg_replace(array(
		'/:.*$/',			// Microsoft's Smart Tags <st1:xxxx>
		'/[^a-z0-9_\[\]\!-]/i',	// forbidden characters
		), '', $tagname);
	return $matches[1].$tagname;
}


/**
 * return table with message headers
 */
function rcmail_message_headers($MESSAGE,$attrib, $headers=NULL){
	global $IMAP;
	static $sa_attrib;

	// keep header table attrib
	if (is_array($attrib) && !$sa_attrib)
		$sa_attrib = $attrib;
	else if (!is_array($attrib) && is_array($sa_attrib))
		$attrib = $sa_attrib;

	if (!isset($MESSAGE))
		return FALSE;

	// get associative array of headers object
	if (!$headers)
		$headers = is_object($MESSAGE->headers) ? get_object_vars($MESSAGE->headers) : $MESSAGE->headers;

	// show these headers
	$standard_headers = array('subject', 'from', 'to', 'cc', 'bcc', 'replyto',
		'mail-reply-to', 'mail-followup-to', 'date');
	$output_headers = array();

	foreach ($standard_headers as $hkey) {
		if ($headers[$hkey])
			$value = $headers[$hkey];
		else if ($headers['others'][$hkey])
			$value = $headers['others'][$hkey];
		else
			continue;

		if ($hkey == 'date') {
			// if ($PRINT_MODE)
				// $header_value = format_date($value, $RCMAIL->config->get('date_long', 'x'));
			// else
				$header_value = format_date($value);
		}
		else if ($hkey == 'replyto') {
			if ($headers['replyto'] != $headers['from'])
				$header_value = rcmail_address_string($value, null, true, $attrib['addicon']);
			else
				continue;
		}
		else if ($hkey == 'mail-reply-to') {
			if ($headers['mail-replyto'] != $headers['reply-to']
				&& $headers['reply-to'] != $headers['from']
			)
				$header_value = rcmail_address_string($value, null, true, $attrib['addicon']);
			else
				continue;
		}
		else if ($hkey == 'mail-followup-to') {
			$header_value = rcmail_address_string($value, null, true, $attrib['addicon']);
		}
		else if (in_array($hkey, array('from', 'to', 'cc', 'bcc')))
			$header_value = rcmail_address_string($value, null, true, $attrib['addicon']);
		else if ($hkey == 'subject' && empty($value))
			$header_value = rcube_label('nosubject');
		else
			$header_value = trim($IMAP->decode_header($value));

		$output_headers[$hkey] = array(
				'title' => rcube_label(preg_replace('/(^mail-|-)/', '', $hkey)),
				'value' => $header_value, 'raw' => $value
		);
	}

	// $plugin = $RCMAIL->plugins->exec_hook('message_headers_output',
		// array('output' => $output_headers, 'headers' => $MESSAGE->headers));

	// compose html table
	$table = new html_table(array('cols' => 2));

	// foreach ($plugin['output'] as $hkey => $row) {
		// $table->add(array('class' => 'header-title'), Q($row['title']));
		// $table->add(array('class' => 'header '.$hkey), Q($row['value'], ($hkey == 'subject' ? 'strict' : 'show')));
	// }

	return $table->show($attrib);
}

/**
 * Convert all relative URLs according to a <base> in HTML
 */
function rcmail_resolve_base($body)
{
  // check for <base href=...>
  if (preg_match('!(<base.*href=["\']?)([hftps]{3,5}://[a-z0-9/.%-]+)!i', $body, $regs)) {
	require_once 'modules/mail_new/inc/main.inc.php';
    $replacer = new rcube_base_replacer($regs[2]);
    $body     = $replacer->replace($body);
  }

  return $body;
}


/**
 * modify a HTML message that it can be displayed inside a HTML page
 */
function rcmail_html4inline($body, $container_id, $body_id='', &$attributes=null, $allow_remote=false)
{
  $last_style_pos = 0;
  $pos = 0;
  $body_lc = strtolower($body);
  $cont_id = $container_id.($body_id ? ' div.'.$body_id : '');

  // find STYLE tags
  while ((strlen($body_lc) > $last_style_pos) && ($pos = strpos($body_lc, '<style', $last_style_pos)) && ($pos2 = strpos($body_lc, '</style>', $pos)))
  {
    $pos = strpos($body_lc, '>', $pos)+1;

    // replace all css definitions with #container [def]
    $styles = rcmail_mod_css_styles(
      substr($body, $pos, $pos2-$pos), $cont_id, $allow_remote);

    $body = substr($body, 0, $pos) . $styles . substr($body, $pos2);
    $body_lc = strtolower($body);
    $last_style_pos = $pos2;
  }

  // modify HTML links to open a new window if clicked
  $GLOBALS['rcmail_html_container_id'] = $container_id;
  $body = preg_replace_callback('/<(a|link)\s+([^>]+)>/Ui', 'rcmail_alter_html_link', $body);
  unset($GLOBALS['rcmail_html_container_id']);

  $body = preg_replace(array(
      // add comments arround html and other tags
      '/(<!DOCTYPE[^>]*>)/i',
      '/(<\?xml[^>]*>)/i',
      '/(<\/?html[^>]*>)/i',
      '/(<\/?head[^>]*>)/i',
      '/(<title[^>]*>.*<\/title>)/Ui',
      '/(<\/?meta[^>]*>)/i',
      // quote <? of php and xml files that are specified as text/html
      '/<\?/',
      '/\?>/',
      // replace <body> with <div>
      '/<body([^>]*)>/i',
      '/<\/body>/i',
      ),
    array(
      '<!--\\1-->',
      '<!--\\1-->',
      '<!--\\1-->',
      '<!--\\1-->',
      '<!--\\1-->',
      '<!--\\1-->',
      '&lt;?',
      '?&gt;',
      '<div class="'.$body_id.'"\\1>',
      '</div>',
      ),
    $body);

  $attributes = array();

  // Handle body attributes that doesn't play nicely with div elements
  $regexp = '/<div class="' . preg_quote($body_id, '/') . '"([^>]*)/';
  if (preg_match($regexp, $body, $m)) {
    $attrs = $m[0];
    // Get bgcolor, we'll set it as background-color of the message container
    if ($m[1] && preg_match('/bgcolor=["\']*([a-z0-9#]+)["\']*/', $attrs, $mb)) {
      $attributes['background-color'] = $mb[1];
      $attrs = preg_replace('/bgcolor=["\']*([a-z0-9#]+)["\']*/', '', $attrs);
    }
    // Get background, we'll set it as background-image of the message container
    if ($m[1] && preg_match('/background=["\']*([^"\'>\s]+)["\']*/', $attrs, $mb)) {
      $attributes['background-image'] = 'url('.$mb[1].')';
      $attrs = preg_replace('/background=["\']*([^"\'>\s]+)["\']*/', '', $attrs);
    }
    if (!empty($attributes)) {
      $body = preg_replace($regexp, rtrim($attrs), $body, 1);
    }

    // handle body styles related to background image
    if ($attributes['background-image']) {
      // get body style
      if (preg_match('/#'.preg_quote($cont_id, '/').'\s+\{([^}]+)}/i', $body, $m)) {
        // get background related style
        if (preg_match_all('/(background-position|background-repeat)\s*:\s*([^;]+);/i', $m[1], $ma, PREG_SET_ORDER)) {
          foreach ($ma as $style)
            $attributes[$style[1]] = $style[2];
        }
      }
    }
  }
  // make sure there's 'rcmBody' div, we need it for proper css modification
  // its name is hardcoded in rcmail_message_body() also
  else {
    $body = '<div class="' . $body_id . '">' . $body . '</div>';
  }

  return $body;
}


/**
 * decode address string and re-format it as HTML links
 */
function rcmail_address_string($input, $max=null, $linked=false, $addicon=null){
	global $IMAP, $RCMAIL;
	static $got_writable_abook = null;//??

	$a_parts = $IMAP->decode_address_list($input);

	if (!sizeof($a_parts))
		return $input;

	$c = count($a_parts);
	$j = 0;
	$out = '';
	//---------------> Rewrite    <--------------------------------//
	if ($got_writable_abook === null/* && $books = $RCMAIL->get_address_sources(true)*/) {
		$got_writable_abook = true;
	}

	foreach ($a_parts as $part) {
		$j++;

		$name	 = $part['name'];
		$mailto = $part['mailto'];
		$string = $part['string'];

		// IDNA ASCII to Unicode
		if ($name == $mailto)
			$name = rcube_idn_to_utf8($name);
		if ($string == $mailto)
			$string = rcube_idn_to_utf8($string);
		$mailto = rcube_idn_to_utf8($mailto);

		if ($PRINT_MODE) {
			$out .= sprintf('%s &lt;%s&gt;', Q($name), $mailto);
		}
		else if (check_email($part['mailto'], false)) {
			if ($linked) {
				$out .= html::a(array(
						'href' => 'mailto:'.$mailto,
						'onclick' => sprintf("return %s.command('compose','%s',this)", JS_OBJECT_NAME, JQ($mailto)),
						'title' => $mailto,
						'class' => "rcmContactAddress",
					),
				Q($name ? $name : $mailto));
			}
			else {
				$out .= html::span(array('title' => $mailto, 'class' => "rcmContactAddress"),
					Q($name ? $name : $mailto));
			}

			if ($addicon && $got_writable_abook) {
				$out .= '&nbsp;' . html::a(array(
						'href' => "#add",
						'onclick' => sprintf("return %s.command('add-contact','%s',this)", JS_OBJECT_NAME, urlencode($string)),
						'title' => rcube_label('addtoaddressbook'),
					),
					html::img(array(
						'src' => $CONFIG['skin_path'] . $addicon,
						'alt' => "Add contact",
					)));
			}
		}
		else {
			if ($name)
				$out .= Q($name);
			if ($mailto)
				$out .= (strlen($out) ? ' ' : '') . sprintf('&lt;%s&gt;', Q($mailto));
		}

		if ($c>$j)
			$out .= ','.($max ? '&nbsp;' : ' ');

		if ($max && $j==$max && $c>$j) {
			$out .= '...';
			break;
		}
	}

	return $out;
}

/**
 * parse link attributes and set correct target
 */
function rcmail_alter_html_link($matches){
  global $RCMAIL;

  // Support unicode/punycode in top-level domain part
  $EMAIL_PATTERN = '([a-z0-9][a-z0-9\-\.\+\_]*@[^&@"\'.][^@&"\']*\\.([^\\x00-\\x40\\x5b-\\x60\\x7b-\\x7f]{2,}|xn--[a-z0-9]{2,}))';

  $tag = $matches[1];
  $attrib = parse_attrib_string($matches[2]);
  $end = '>';

  // Remove non-printable characters in URL (#1487805)
  $attrib['href'] = preg_replace('/[\x00-\x1F]/', '', $attrib['href']);

  if ($tag == 'link' && preg_match('/^https?:\/\//i', $attrib['href'])) {
    $tempurl = 'tmp-' . md5($attrib['href']) . '.css';
    $_SESSION['modcssurls'][$tempurl] = $attrib['href'];
    $attrib['href'] = $RCMAIL->url(array('task' => 'utils', 'action' => 'modcss', 'u' => $tempurl, 'c' => $GLOBALS['rcmail_html_container_id']));
    $end = ' />';
  }
  else if (preg_match('/^mailto:'.$EMAIL_PATTERN.'(\?[^"\'>]+)?/i', $attrib['href'], $mailto)) {
    $attrib['href'] = $mailto[0];
    $attrib['onclick'] = sprintf(
      "return %s.command('compose','%s',this)",
      JS_OBJECT_NAME,
      JQ($mailto[1].$mailto[3]));
  }
  else if (!empty($attrib['href']) && $attrib['href'][0] != '#') {
    $attrib['target'] = '_blank';
  }

  return "<$tag" . html::attrib_string($attrib, array('href','name','target','onclick','id','class','style','title','rel','type','media')) . $end;
}

?>
