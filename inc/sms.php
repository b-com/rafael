<?

//————————————————————————————————————————————————————————————————————————————————————
function check_sms_credits($num, $user=null) {
 global $SVARS;
	if (!$SVARS['user']['id']) return 'No permission';
	if (!$SVARS['user']['sms_active']) return "לקבלת שרות SMS התקשרו: 074-7144333";	
	$sms_credits=sql2array("SELECT sms_credits FROM clients WHERE id=$SVARS[cid]",'','sms_credits',1,'Rafael');
	if (($sms_credits-$num)<0) return "כמות ההודעות לשליחה ($num)<br>גדולה מהכמת הנותרת ($sms_credits)<br>לבירור: 074-7144333";	
 return 'OK';
}

//————————————————————————————————————————————————————————————————————————————————————
function send_mass_sms($sms_ar, $total_credits=0) {
 global $SVARS;
	if (!$SVARS['user']['id']) return 'No permission';
	if (!is_array($sms_ar)) return 'Bad message array';
	
	// print_ar($total_credits,'$total_credits');
	// print_ar($sms_ar,'$sms_ar');
	
	if (!$total_credits) {	# count total number of credits needed
		foreach($sms_ar as $row) {
			$tels_count=count(explode(',', $row[0]));
			$message_len=strlen( str_replace("\r\n","\n",trim($row[1])) );
			if ($message_len>70) $msg_credits = $tels_count * ceil($message_len/67);
			else $msg_credits = $tels_count;
			$total_credits+=$msg_credits;
		}
	}
	
	if (($check_credits=check_sms_credits($total_credits))!='OK') return $check_credits;
	 
	$success_ar=array();
	$fail_ar=array();
	$credits=0;
	foreach($sms_ar as $row) {	# send
		$send_sms = send_sms($row[0], $row[1], $_POST['originator'], true);
		if (is_array($send_sms)) list($send_success, $send_fail, $credits_used) = $send_sms;
		else $errors[]=$send_sms;
		if ($send_success) $success_ar = array_merge($success_ar, $send_success);
		if ($send_fail)    $fail_ar = array_merge($fail_ar, $send_fail);
		if ($credits_used) $credits+=$credits_used;
	}
	// print_ar($success_ar,'$success_ar');
	// print_ar($fail_ar,'$fail_ar');
	// print_ar($errors,'$errors');
 return array($success_ar, $fail_ar, $credits, $errors);
}

//————————————————————————————————————————————————————————————————————————————————————
function send_sms($tels, $message, $originator, $skip_check=false) {
 global $SVARS, $CFG;
	ignore_user_abort(true);
	$username='jackobelfassy@gmail.com';
	// $password='A301E973';
	$password='zprq2dag';

	$message=str_replace("\r\n", "\n", trim($message));
	if (!$tels OR !$message) return 'phone number and message required';
	
	$tels=explode(',', $tels);
	
	# count number of sms credits needed ($msg_credits)
	$message_len=strlen($message);
	$tels_count=count($tels);
	if ($message_len>70) $msg_credits = $tels_count * ceil($message_len/67);
	else $msg_credits = $tels_count;
	
	if (!$skip_check OR !$SVARS['sms_group_id']) { # check group credits
		if (($check_credits=check_sms_credits($msg_credits))!='OK') return $check_credits;
	}
	
	# filter and prepare phone nummbers
	foreach($tels as $tel) {
		if ($tel=ereg_replace('[^0-9]+','',$tel) AND ereg('^(0|972)(5[024][0-9]{7})$', $tel, $regs)) $phones[] = '972'.$regs[2];
	}
	if (!$phones) return 'No phone numbers';
	$phones=array_unique($phones);
	
	//foreach( as $key=>$row) {}
	//echo "\$message=$message<br>";
	
	if (preg_match("|^[0-9a-z !\"#\$%&\^'\(\)*+,\./:;<=>?\\@_`{\|}~\[\]\n-]+$|i",$message)) {
		$msgtext = urlencode($message);
		$charset=0;
	}else {	
		$msgtext = utf8_to_ucs2_hex($message);
		$charset=6;
	}

	$phone_chunks=array_chunk($phones,300);
	$chunks_num=count($phone_chunks);
	//print_ar($phone_chunks,'');
	
	// if ($tel=ereg_replace('[^0-9]+','',$originator) AND ereg('^(0|972)(5[024][0-9]{7})$', $tel, $regs)) $originator='972'.$regs[2];
	// else $originator='972000000000';
	if ($tel=ereg_replace('[^0-9]+','',$originator) AND ereg('^(0|972)([0-9]{8,9})$', $tel, $regs)) $originator='972'.$regs[2];
	else $originator='972000000000';
	
	$send_success=array();
	$send_fail=array();
	foreach($phone_chunks as $nums) {
		$post="username=$username&password=$password&originator=$originator&tariff=1&charset=$charset&"
			 ."msgtext=$msgtext&phone=".implode(',',$nums);
		//echo "<br>POST: $post<br><br>";
		
		if ($_SERVER['HTTP_HOST']=='bafi.loc') $response='OK'; # for tests
		else $response=curl_request('http://216.25.78.128/bulksms/bulksend.go', $post);
		
		$response_ar=explode("\n", $response);
		if ($response_ar[0]=='OK') {
			$send_success = array_merge($send_success, $nums);
			error_log(date('Y-m-d H:i:s')." | response:$response  POST:$post\r\n\r\n", 3, "sms_succes.log");		# Write to log file
		}else {
			$send_fail = array_merge($send_fail, $nums);
			error_log(date('Y-m-d H:i:s')." | response:$response  POST:$post\r\n\r\n", 3, "sms_failed.log");		# Write to log file
			return $response;
		}
	}
	
	# count number of sms credits used
	if ($message_len>70) $credits_used = count($send_success) * ceil($message_len/67);
	else $credits_used = count($send_success);
	
	if (count($send_success)) {
		foreach($send_success as $k=>$v) $send_success[$k] = '0'.substr($v,3);
		# save history
		$sql="INSERT DELAYED INTO sms_history SET user_id={$SVARS['user']['id']}, "
			."send_time='".date('Y-m-d H:i:s')."', "
			."credits='$credits_used', "
			."numbers='".implode(',',$send_success)."', "
			."msgtext=".quote($message).", "
			."originator='".($originator!='972000000000' ? '0'.substr($originator,3) :'')."'";
		//echo "<br>$sql<br>";
		runsql($sql);
		# update group credits
		runsql("UPDATE clients SET sms_credits=sms_credits-$credits_used WHERE id=$SVARS[cid]",'Rafael');
	}
	
 return array($send_success, $send_fail, $credits_used);
}


//————————————————————————————————————————————————————————————————————————————————————
function curl_request($url, $post_vars='', $headers='') {
	set_time_limit(80);
	ignore_user_abort(true);
	
	$ch = curl_init($url);
	//curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');	# use fidder proxy (for testing)
	//curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	
	# add headers
	if ($headers && !is_array($headers)) $headers=explode("\r\n", trim($headers));	# split string headers
	$headers[]="User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";	# add User-Agent header
	$headers[]="Expect:";
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	# add post_vars
	if ($post_vars) { curl_setopt($ch, CURLOPT_POST, 1); curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars); }

	$curl_response = curl_exec($ch);
	//echo "<pre>\$curl_response="; print_r($curl_response); echo "</pre>";
	
	if ($curl_response===false) {
		$response['info']=curl_getinfo($ch);	# add request info array
		//echo "<font color=red>Fetch Error!</font><br>Request Info: ".print_r($response['info'],1);
		return false;
	}
	curl_close($ch);
 return $curl_response;
}

//————————————————————————————————————————————————————————————————————————————————————
//function ereg_get($pattern, $string, $num=1) { if (ereg($pattern, $string, $regs)) return $regs[$num]; }


//————————————————————————————————————————————————————————————————————————————————————
function utf8_to_ucs2_hex($str) {
	$unicode=utf8ToUnicode($str);
	if (!$unicode) return;
	foreach($unicode as $value) $entities.=sprintf("%04s", dechex($value));
 return $entities;
}

//————————————————————————————————————————————————————————————————————————————————————
function utf8ToUnicode(&$str) {
  $mState = 0;     // cached expected number of octets after the current octet until the beginning of the next UTF8 character sequence
  $mUcs4  = 0;     // cached Unicode character
  $mBytes = 1;     // cached expected number of octets in the current sequence
  $out = array();

  $len = strlen($str);
  for($i = 0; $i < $len; $i++) {
	$in = ord($str{$i});
	if (0 == $mState) { // When mState is zero we expect either a US-ASCII character or a multi-octet sequence.
	  if (0 == (0x80 & ($in))) { // US-ASCII, pass straight through.
		$out[] = $in;
		$mBytes = 1;
	  } else if (0xC0 == (0xE0 & ($in))) { // First octet of 2 octet sequence
		$mUcs4 = ($in);
		$mUcs4 = ($mUcs4 & 0x1F) << 6;
		$mState = 1;
		$mBytes = 2;
	  } else if (0xE0 == (0xF0 & ($in))) { // First octet of 3 octet sequence
		$mUcs4 = ($in);
		$mUcs4 = ($mUcs4 & 0x0F) << 12;
		$mState = 2;
		$mBytes = 3;
	  } else if (0xF0 == (0xF8 & ($in))) { // First octet of 4 octet sequence
		$mUcs4 = ($in);
		$mUcs4 = ($mUcs4 & 0x07) << 18;
		$mState = 3;
		$mBytes = 4;
	  } else if (0xF8 == (0xFC & ($in))) {
		// First octet of 5 octet sequence. This is illegal because the encoded codepoint must be either
		// (a) not the shortest form or (b) outside the Unicode range of 0-0x10FFFF.
		// Rather than trying to resynchronize, we will carry on until the end of the sequence and let the later error handling code catch it. 
		$mUcs4 = ($in);
		$mUcs4 = ($mUcs4 & 0x03) << 24;
		$mState = 4;
		$mBytes = 5;
	  } else if (0xFC == (0xFE & ($in))) { // First octet of 6 octet sequence, see comments for 5 octet sequence.
		$mUcs4 = ($in);
		$mUcs4 = ($mUcs4 & 1) << 30;
		$mState = 5;
		$mBytes = 6;
	  } else { // Current octet is neither in the US-ASCII range nor a legal first octet of a multi-octet sequence.
		return false;
	  }
	} else {   // When mState is non-zero, we expect a continuation of the multi-octet sequence
	  if (0x80 == (0xC0 & ($in))) { // Legal continuation.
		$shift = ($mState - 1) * 6;
		$tmp = $in;
		$tmp = ($tmp & 0x0000003F) << $shift;
		$mUcs4 |= $tmp;

		if (0 == --$mState) {
		  // End of the multi-octet sequence. mUcs4 now contains the final Unicode codepoint to be output. Check for illegal sequences and codepoints. From Unicode 3.1, non-shortest form is illegal
		  if (((2 == $mBytes) && ($mUcs4 < 0x0080)) ||
			  ((3 == $mBytes) && ($mUcs4 < 0x0800)) ||
			  ((4 == $mBytes) && ($mUcs4 < 0x10000)) ||
			  (4 < $mBytes) ||
			  (($mUcs4 & 0xFFFFF800) == 0xD800) ||	// From Unicode 3.2, surrogate characters are illegal
			  ($mUcs4 > 0x10FFFF)) {				// Codepoints outside the Unicode range are illegal
			return false;
		  }
		  if (0xFEFF != $mUcs4) { // BOM is legal but we don't want to output it
			$out[] = $mUcs4;
		  }
		  //initialize UTF8 cache
		  $mState = 0;
		  $mUcs4  = 0;
		  $mBytes = 1;
		}
	  } else { // ((0xC0 & (*in) != 0x80) && (mState != 0))  Incomplete multi-octet sequence. 
		return false;
	  }
	}
  }
  return $out;
}

?>