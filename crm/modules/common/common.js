ClientCombo = function(config) {
	ClientCombo.superclass.constructor.call(this, {
		store: new Ext.data.Store({
			proxy: new Ext.data.HttpProxy({url:'?m=common/clients&f=clients_shortList'}),
			reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['id','client','passport'])
		}),
		typeAhead: false, fieldLabel:'בחר לקוח:',	loadingText: '...טוען',	hiddenName:'client_id',	valueField:'id', displayField:'client',	minChars: 2, pageSize:4, hideTrigger:true,
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right"><i>{client}</i> ת.ז.:&nbsp; {passport}&nbsp; </div></tpl>'),
		itemSelector: 'div.search-item'
	});
	Ext.apply(this, config);
};
Ext.extend(ClientCombo, Ext.form.ComboBox);

userRenderer = function(val) {return (users_obj[val] ? users_obj[val].fullName : '');}
