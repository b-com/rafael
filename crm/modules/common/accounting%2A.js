﻿var today = new Date();

// month_first = function() {return (new Date(new Date(today.getFullYear(), today.getMonth(),1))).format('d/m/')+today.getFullYear();}
// month_last = function()  {return (new Date(new Date(today.getFullYear(), today.getMonth()+1,0))).format('d/m/')+today.getFullYear();}

var VAT=17, VAT_store = [[0,'פטור'],[1,VAT+' %']];

var credit_cards = [[1,'וויזה'],[2,'ישראכרט/מאסטרכארד'],[5,'לאומי קארד'],[3,'דיינרס'],[4,'אמריקן אקספרס']];
var credit_cards_names = ['וויזה','ישראכרט/מאסטרכארד','לאומי קארד','דיינרס','אמריקן אקספרס'];
var bank_payment_type = ['שיק','ה.קבע/ה.בנקאית'];
var creditCard = ['אשראי'];
var chequeStore = ['מזומן'];
var doc_types = {
	set_balance:'קליטת יתרות', order:'הזמנה', payment:'תשלום', cash_payment:'תשלום במזומן', invoice_credit:'חשבונית מס', invoice_debt:'חשבונית זיכוי', accept:'קבלה',accept_tmp:'קבלה זמנית',invoice_accept:'חשבונית מס קבלה',deposit:'הפקדה לבנק',
	refund_bankclient:'החזרת שיקים ללקוח',refund_cashclient:'החזרת שיקים ללקוח',refund_bankcash:'החזרת שיקים לקופה',expense:'קליטת הוצאות'
};

docTypeRender = function(v, p, r){return (doc_types[v] ? doc_types[v]+(r.json.storno==1 ? "<sup style='color:red'>סטורנו</sup>":''):'')};


var prod = {};

clientRenderer = function(str, p, record){
	if (!str) return;
	if (!record.json) return this.hlStr(record.data.client_name);//return;
	var qtip='';
	if (record.json.client_phones) qtip=qtip+'<tr><th>טלפון:</th></tr><tr><td>'+record.json.client_phones+'</td></tr>';
	if (record.json.email) qtip=qtip+'<tr><td><b>דוא"ל:</b> '+record.json.email+'</td></tr>';
	if (record.json.client_addresses) qtip=qtip+'<tr><th>כתובת:</th></tr><tr><td>'+record.json.client_addresses+'</td></tr>';
	qtip='<table dir=rtl cellspacing=0 class=tip-table>'+qtip+'</table>';
	qtip=qtip.replace(/'/g, '&#39;');
	return "<div ext:qtip='"+qtip+"'>"+this.hlStr(record.json.client_name)+"</div>";
}

Ext.grid.CheckColumn = function(config){
    Ext.apply(this, config);
    if(!this.id) this.id = Ext.id();
    this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown : function(e, t){
        if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            var record = this.grid.store.getAt(index);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
			if(typeof(this.refreshSummary)=='undefined' || this.refreshSummary==true) this.grid.plugins[0].refreshSummary();
        }
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
    }
};

numberField = Ext.extend(Ext.form.NumberField, {allowBlank:true, allowNegative:true,
	listeners:{
		blur: function(el){el.getEl().dom.style.display='none';	},
		show: function(el){el.getEl().dom.style.display=''; el.getEl().dom.style.height='18px';	}
	}
});

textField = Ext.extend(Ext.form.TextField, {allowBlank:true,
	listeners:{
		blur: function(el){el.getEl().dom.style.display='none';	},
		show: function(el){el.getEl().dom.style.display=''; el.getEl().dom.style.height='18px';	}
	}
});

roundSum = function(grid){
	var sum=0;
	var rs = grid.store.getRange();
	for(var j = 0, jlen = rs.length; j < jlen; j++){
		r = rs[j];
		if (r.data['type']=='round' || !r.data['name']) grid.store.removeAt(j);
		else if (r.data['sum_tax']) sum = sum + parseFloat(r.data['sum_tax']);
	}
	if (sum!=Math.round(sum))  {
		var Record = Ext.data.Record.create([{name: 'name', type: 'string'},{name: 'unit'},{name: 'amount'},{name: 'price'},{name: 'sum'}]);
		data = {name:'עיגול סכום',type:'round',VAT:-1,sum_tax:(Math.round(sum)-sum)};
		grid.store.insert(grid.store.getCount(),new Record(data)); 
		grid.plugins.refreshSummary();	
	}
};

discount = function(grid){
	var sum=0, d={};
	var rs = grid.store.getRange();
	d.ditype= grid.getTopToolbar().items.get('ditype').getValue();
	d.dval 	= grid.getTopToolbar().items.get('dval').getValue();
	d.dtype = grid.getTopToolbar().items.get('dtype').getValue();
	// Dump(d);
	if (d.dtype=='%') {
		for(var j = 0, jlen = rs.length; j < jlen; j++){
			r = rs[j];
			if (!r.data['name']) grid.store.removeAt(j); else 
			if (r.data['sum_tax'] && ((r.data['type']==d.ditype) || d.ditype=='')) sum = sum + parseFloat(r.data['sum_tax']);
		}
		d.sum_tax = - Math.round(sum * parseFloat(d.dval))/100;
	} else d.sum_tax = - parseFloat(d.dval);

	if (d.sum_tax)  {
		d.sum=Math.round(d.sum_tax*10000/(100+VAT))/100;
		var Record = Ext.data.Record.create([{name: 'name', type: 'string'},{name: 'unit'},{name: 'amount'},{name: 'price'},{name: 'sum'}]);
		data = {name:'הנחה '+(d.ditype=='' ? 'כללית' : waretypes_obj[d.ditype])+' '+d.dval+' '+d.dtype ,type:'discount',VAT:1,sum:d.sum,sum_tax:d.sum_tax};
		grid.store.insert(grid.store.getCount(),new Record(data)); 
		grid.plugins.refreshSummary();	
	}
};

gridAfterEdit = function(e){
	var Record = Ext.data.Record.create([{name: 'id'},{name: 'name', type: 'string'},{name: 'unit'},{name: 'amount'},{name: 'price'},{name: 'sum'}]);
	if (e.field == 'name') {
		if (prod.id) {
			e.record.set('code', prod.code);
			e.record.set('type', prod.type);
			e.record.set('unit', prod.unit);
			e.record.set('price',prod.price);
			e.record.set('VAT',  prod.VAT);
		}
		if (e.record.get('amount')) e.record.set('sum_tax', Math.round(e.record.get('price')*(100+VAT*e.record.get('VAT'))*e.record.get('amount'))/100);
		else {e.record.set('sum',0); e.record.set('sum_tax',0);}
		if ((e.grid.store.getCount()-e.row)==1) e.grid.store.insert(this.store.getCount(), new Record({VAT:1})); 
	} else if (e.field == 'amount' || e.field == 'price') {
		if (e.record.get('price') && e.record.get('amount')) {
			e.record.set('sum', e.record.get('price')*e.record.get('amount'));
			e.record.set('sum_tax', Math.round(e.record.get('price')*(100+VAT*e.record.get('VAT'))*e.record.get('amount'))/100);
		} else {e.record.set('sum',0); e.record.set('sum_tax',0);}
	} else if (e.field == 'VAT') {
		if (e.record.get('sum')) e.record.set('sum_tax', Math.round(e.record.get('sum')*(100+VAT*e.record.get('VAT')))/100);
		else e.record.set('sum_tax',0);
	} else if (e.field == 'sum_tax') {
		e.record.set('sum', Math.round(e.record.get('sum_tax')/(100+VAT*(e.record.get('VAT')==1 ? 1:0))*10000)/100);
		if (e.record.get('amount'))  e.record.set('price', Math.round(e.record.get('sum')/e.record.get('amount')*100)/100);
	}
	e.grid.plugins.refreshSummary();	
}

ClientCombo = function(config) {
	ClientCombo.superclass.constructor.call(this, {
		store: new Ext.data.Store({
			proxy: new Ext.data.HttpProxy({url:'?m=common/clients&f=clients_shortList'}),
			reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['id','client','passport'])
		}),
		typeAhead: false, fieldLabel:'בחר לקוח:', loadingText: '...טוען', hiddenName:'client_id', valueField:'id',	displayField:'client',
		minChars:3, pageSize:8,	hideTrigger:true,
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right"><b>{client}</b> ת.ז./ח.פ.:&nbsp; <b>{passport}</b>&nbsp; </div></tpl>'),
		itemSelector: 'div.search-item'
	});
	Ext.apply(this, config);
};
	
Ext.extend(ClientCombo, Ext.form.ComboBox);

var userAccounts = new Ext.data.JsonStore({
	url:'?m=config/config&f=accounts_list',
	totalProperty: 'total',
	root: 'data',
	fields: ['id','bank','branch','bank_name','branch_name','number','full_namber','type','contact_person','tel','fax','balance','notes'],
	sortInfo: {field:'name', direction:'ASC'},
	remoteSort: true
});

var invoiceGridSummary= new Ext.grid.GridSummary({cellTpl: new Ext.Template('<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="border-top:3px double blue; color:blue; font-weight:bold; {style}"><div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on">{value}</div></td>')});
var acceptGridSummary = new Ext.grid.GridSummary({cellTpl: new Ext.Template('<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="border-top:3px double green; color:green; font-weight:bold; {style}"><div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on">{value}</div></td>')});
var depositGridSummary= new Ext.grid.GridSummary({Calculations:{sum:function (v, record, field) {return parseFloat(v) + ( record.data.deposit ? parseFloat(record.data[field]):0);}}});

CashPaymentWindow = function(grid, d) {

	this.form = new Ext.form.FormPanel({
		labelWidth:50, autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl',
			items: [			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange'},
			{xtype:'label', hidden:grid.client_id, text:'לקוח:'},new ClientCombo({width:250, value:grid.client_id, hidden:grid.client_id, allowBlank:false}),
			{xtype:'label', text:'סכום:'},{xtype:'textfield', width:55, name:'sum', allowBlank:false},
			{xtype:'label', text:'הערות:'},{name:'notes', xtype:'textarea', width:489, height:42, colspan:5},
			{xtype:'hidden', name:'id'},{xtype:'hidden', name:'type', value:this.type}
		]}
	});

	CashPaymentWindow.superclass.constructor.call(this, {
		title:'תשלום במזומן', iconCls:'task', layout:'fit', modal:true,
		width:560, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons:[
			{text:'שמור', disabled:(d.id), handler:function(){
				if(!this.form.form.isValid()) return;
				Ext.MessageBox.show({
					width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, scope:this,
					fn: function(btn) {if(btn=='yes') this.submitForm(); }
				});
				}, scope:this},
			{text:'הדפס', disabled:(!d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type=cash_payment&id='+this.id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		var sum=0;

		this.form.form.submit({
			url: "?m=common/accounting&f=add_cash_payment",
			params:{sum:sum},//,vat_sum:vat_sum
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				grid.store.reload();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
};
Ext.extend(CashPaymentWindow, Ext.Window, {});

OrderWindow = function(grid, d) {
	this.order_id=d.id;

	var title= (this.order_id ? "עידכון הזמנה מס' "+d.data.num : 'הזמנה חדשה');
	
	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=order_rows&order_id='+this.order_id,
		totalProperty:'total', root:'data',
		fields: ['id','code','name','type','unit','amount','price','sum','VAT','sum_tax'],
		remoteSort: false
	});

	
	this.prodCombo = new Ext.form.ComboBox({
		store: new Ext.data.Store({
			proxy: new Ext.data.HttpProxy({url:'?m=common/accounting&f=products_shortList'}),
			reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['id','code','name'])
		}),
		width:145, typeAhead:true, loadingText:'...טוען', editable:true,// hiddenName:'part_id', valueField:'name',
		displayField:'name', minChars:3, pageSize:8, hideTrigger:true, hideMode:'display',
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">{name} <b>({code})</b></div></tpl>'),
		itemSelector: 'div.search-item'
		,listeners: {
			select: function(e,r){prod=r.json},
			render: function(){this.gridEditor.el.useDisplay=true;},
			show: 	function(el){el.getEl().dom.parentNode.style.width='240px'; el.getEl().dom.style.height='18px';}
		}
	});

	this.unitCombo = new Ext.form.ComboBox({
		store:wareunits, hiddenName:'unit', valueField:'id', displayField:'val', width:60, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false, value:1
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show: function(el){el.getEl().dom.parentNode.style.width='60px'; el.getEl().dom.style.height='18px';}
		}
	});
	
	this.taxCombo = new Ext.form.ComboBox({
		store:VAT_store, hiddenName:'unit', valueField:'id', displayField:'val', width:50, triggerAction:'all', forceSelection:true, allowBlank:false, value:1
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show: function(el){el.getEl().dom.parentNode.style.width='50px'; el.getEl().dom.style.height='18px';}
		}
	});
	
	this.typeCombo = new Ext.form.ComboBox({
		store:waretypes, hiddenName:'unit', valueField:'id', displayField:'val', width:60, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false, value:1
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show: function(el){el.getEl().dom.parentNode.style.width='60px'; el.getEl().dom.style.height='18px';}
		}
	});
	
	this.numberField = Ext.extend(Ext.form.NumberField, {allowBlank:false, allowNegative:false, maxValue:100000,
		listeners:{
			blur: function(el){el.getEl().dom.style.display='none';	},
			show: function(el){el.getEl().dom.style.display=''; el.getEl().dom.style.height='18px';	}
		}
	});
	
	this.store.on('datachanged', function(){this.grid.plugins.refreshSummary()}, this);
	
	var Record = Ext.data.Record.create([{name: 'id'},{name: 'name', type: 'string'},{name: 'unit'},{name: 'amount'},{name: 'price'},{name: 'sum'}]);
	
	this.client_id=0;
	
	this.form = new Ext.form.FormPanel({
		labelWidth:50, autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'rtl',
			items: [			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange'},
			{xtype:'label', hidden:grid.client_id, text:'לקוח:'},new ClientCombo({width:430, value:grid.client_id, hidden:grid.client_id, allowBlank:false}),
			{xtype:'label', text:'סטטוס:'},{xtype:'combo', store:[[1,'טרם בוצעה'],[2,'בוצעה']], value:1, allowBlank:false, hiddenName:'status', width:100, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true},
			{xtype:'label', text:'כותרת:'},{xtype:'textfield', width:466, name:'name'},
			{xtype:'label', text:'הערות:'},{name:'notes', xtype:'textarea', width:609, height:42, colspan:3},
			{xtype:'hidden', name:'id'},{xtype:'hidden', name:'type', value:this.type}
		]}
	});
	
	this.unitRenderer = function (str) {return wareunits_obj[str];}

	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:240,
		cls:'rtl',
		autoScroll:true,
		enableColumnResize:false,
		plugins: invoiceGridSummary,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		border: false,
		columns: [
			{dataIndex:'code', hidden:true}
			,{header:'שם פריט', width:240, dataIndex:'name', editor:this.prodCombo, summaryRenderer:function(){return 'סה"כ'}}
			,{header:'סוג', dataIndex:'type', width:60, renderer:function(str){return waretypes_obj[str];}, editor: this.typeCombo}
			,{header:'כמות',dataIndex:'amount', width:50, editor: new this.numberField}
			,{header:"יח'", dataIndex:'unit', width:60, renderer:this.unitRenderer, editor:this.unitCombo}
			,{header:"מחיר יח'", dataIndex:'price', width:50, editor: new this.numberField}
			,{header:'לפני מע"מ', dataIndex:'sum', width:70, renderer:function(str){return Ext.util.Format.ilMoney(str)}, summaryType:'sum'}
			,{header:'מע"מ', dataIndex:'VAT', width:50, renderer:function(str){return (str ? VAT+'%':'0%')}, editor: this.taxCombo}
			,{header:'סה"כ', dataIndex:'sum_tax', width:70, renderer:function(str){return Ext.util.Format.ilMoney(str)}, summaryType:'sum', editor: new this.numberField}
		]
		,tbar: {items:[
			{text:'הוסף שורה', iconCls:'add', handler:function(){this.store.insert(this.store.getCount(),new Record({VAT:1}));}, scope:this},
			{text:'מחק שורה',  id:'del_row',  disabled:true, iconCls:'delete', handler:function(){this.delRowPromt()}, scope:this}
		]}
	});
	
	this.grid.on('afteredit', gridAfterEdit);
	
	this.grid.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.grid.getTopToolbar().items.get('del_row').setDisabled(!sm.getCount());
	}, this);
	
	OrderWindow.superclass.constructor.call(this, {
		title:title, iconCls:'task', layout:'fit', modal:true,
		width:680, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[this.form, this.grid],
		buttons:[
			{text:'שמור', handler:function(){
				if(!this.form.form.isValid()) return;
				this.submitForm();
				}, scope:this},
			{text:'הדפס', disabled:(!d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crc/?m=common/accounting/print_doc&type=order&id='+this.id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:(this.order_id ? 'סגור':'בטל'), handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		var rdata=[], sum=0, vat_sum=0;
		this.store.each(function(r){rdata.push(r.data);sum=sum+(r.data.sum_tax==null ? 0: parseFloat(r.data.sum_tax));});

		this.form.form.submit({
			url: "?m=common/accounting&f=add_order",
			params:{sum:sum,rdata:Ext.encode(rdata)},//,vat_sum:vat_sum
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				grid.store.reload();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	
	this.delRowPromt = function(){
		var records = this.grid.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.id);});
		Ext.MessageBox.show({
			width:250, title:'למחוק פריט?', msg:'האם ברצונך לבטל את הפריט הזה ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delRow(records); }
		});
	};
	
	this.delRow = function(records){
		Ext.each(records, function(r){
			row=this.grid.store.indexOf(r);
			this.grid.selModel.selectRow(row);
			this.grid.view.getRow(row).style.border='1px solid red';
			Ext.fly(this.grid.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.grid.store.remove(this.grid.selModel.getSelected());}, scope:this });//this.grid.plugins.refreshSummary();
		},this);
		this.grid.plugins.refreshSummary();	
	};
	if (this.order_id) {
		this.form.form.loadRecord(d);
		this.store.load();
	} else	{
		this.store.insert(this.store.getCount(),new Record());
	}
};
Ext.extend(OrderWindow, Ext.Window, {});

NewClientWindow = function(cfg) {
	this.IdValidator = function(val){
	if (Ext.isEmpty(val)) return 'field is empty';
}
	this.form = new Ext.form.FormPanel({
		items:{layout:'table', layoutConfig: {columns:2}, baseCls: 'x-plain', cls:'rtl', defaults:{style:'margin:3px;'},
			items: [			
				{xtype:'label', text:'שם לקוח:'},{name:'name', xtype:'textfield', allowBlank:false, value:cfg.name},
				{xtype:'label', text:'ת.ז./ח.פ.:'},{name:'cnumber', xtype:'numberfield', allowBlank:false, value:cfg.cnumber
					},
				{xtype:'label', text:'דוא"ל:'},{name:'email', xtype:'textfield'}
			]
		}
	});
	

	NewClientWindow.superclass.constructor.call(this, {
		title:'לקוח חדש',
		width:220, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[this.form],
		buttons:[
			{text:'שמור', handler:function(){
				if(!this.form.form.isValid()) return;
				this.submitForm();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		
		
		this.form.form.submit({
			url: "?m=common/clients&f=add_client",
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				var data = Ext.decode(a.response.responseText);
				if (cfg.callBack) cfg.callBack(data.id, data.name );
				this.close(); 
			},
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	this.on('afterrender', function(){this.form.find('name','cnumber')[0].focus(true,800);}, this);
};
Ext.extend(NewClientWindow, Ext.Window, {});



InvoiceWindow = function(cfg) {//grid, t, d
	this.type=cfg.type;
	this.order_id=cfg.orders_ids;

	var title='קבלה / אישור תשלום';
	// if (this.type=='invoice_credit') title='חשבונית מס חדשה';
	// else if (this.type=='invoice_debt') title='חשבונית זיכוי חדשה';
	// else if (this.type=='accept') title='קבלה חדשה';
	// else if (this.type=='accept_tmp') title='קבלה זמנית חדשה';
	// else if (this.type=='invoice_accept') title='חשבונית מס קבלה חדשה';
	
	// this.clientWindow = function(name){
		// var win=new ClientWindow(
			// {callBack:function(data){this.clientCombo.setValue(data.id); this.clientCombo.setRawValue(data.name);/*this.submitForm();*/}.createDelegate(this)}
			// ,{first_name:name, type:'לקוח', c_type:1}
		// );
		// win.show(); win.center();
		// win.form.find('name','passport')[0].focus(true,600);
	// }
	
	this.clientWindow = function(name){	
		var win=new NewClientWindow({
			name:name, 
			callBack:function(id,name){	this.clientCombo.setValue(id);this.clientCombo.setRawValue(name);this.submitForm();	}.createDelegate(this)
		}); 
		win.show();
	};
	var incomeGroup={};
	
	this.IncomeGroupCombo =  new Ext.form.ComboBox({
			store: new Ext.data.Store({
				proxy: new Ext.data.HttpProxy({	url:'?m=config/config&f=income_group_list'}),
				reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['id','name'])
			
			}),
			
			onTriggerClick:function(el){
				var win;
				var incomegroupsGrid = new IncomesgroupsGrid({},{height:363, layout:'fit', combo:this});
				incomegroupsGrid.on('beforeedit', function(e){ return (e.record.data.id ? false : true) });
				incomegroupsGrid.on('rowdblclick', function(grid,row){
					var r = grid.store.getAt(row);
					console.log(r)
					this.combo.store.loadData({data:{id:r.data.id, name:r.data.name}});
					this.combo.setValue(r.data.id);
					win.close();
				   });
		
				
				win = new Ext.Window ({title:'קבוצת הכנסות', width:300, height:400, items:incomegroupsGrid});
				win.show();
			},
			width:170, typeAhead:true, loadingText:'...טוען', editable:true, hiddenName:'income_group', valueField:'id',
			displayField:'name', minChars:2, pageSize:8, hideMode:'display', triggerClass:'x-form-search-trigger',
			tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">{name} <b>({acc_card})</b></div></tpl>'),
			itemSelector: 'div.search-item',tabTip:'AAAAAA'
			,listeners: {
				select: function(e,r){incomeGroup=r.json;},
//				render: function(){expenseGroup={}; this.gridEditor.el.useDisplay=true;},
				show: 	function(el){incomeGroup={}; el.getEl().dom.parentNode.style.width='120px'; el.getEl().dom.style.height='18px';}
			}
		});
	
//	this.taxCombo = new Ext.form.ComboBox({
//		store:VAT_store, valueField:'id', displayField:'val', width:50, triggerAction:'all', forceSelection:true, allowBlank:false, value:1
//		,listeners: {
//			render: function(){this.gridEditor.el.useDisplay=true;},
//			show:	function(el){el.getEl().dom.parentNode.style.width='50px'; el.getEl().dom.style.height='18px';}
//		}
//	});
	this.store = new Ext.data.JsonStore({
		// url:'?m=common/accounting&f=import_invoice_rows',
		// params:{from:cfg.doc_type, ids:(cfg.orders_ids || cfg.doc_id)},
		root:'data',
		fields:['name'],
		remoteSort: false
	});

	var Record = Ext.data.Record.create(['name']);
	var AcceptRecord = Ext.data.Record.create(['date','ptype','sum','bank','dept','account','number']);
	
	this.sumEditor = Ext.extend(numberField, {enableKeyEvents:true, completeOnEnter:false, 
		listeners:{
			specialkey:function(el,e){
				if (e.keyCode==13 || e.keyCode==9)  {
					el.gridEditor.completeEdit();
					if (el.gridEditor.getValue()) {
						if((this.acceptStore.getCount()-el.gridEditor.row)==1) this.copyPaymentRecord(el.gridEditor.record);
					} else if (this.acceptStore.getCount()>1) this.acceptStore.remove(el.gridEditor.record);
				}
			},
			scope:this
		}
	});	
	

	this.prodEditor = Ext.extend(textField, {enableKeyEvents:true, completeOnEnter:false
		,listeners:{
			specialkey:function(el,e){
				if (e.keyCode==13 || e.keyCode==9)  {
					if (e.keyCode==13) el.gridEditor.completeEdit();
					if (el.gridEditor.getValue()){
						if((this.store.getCount()-el.gridEditor.row)==1) {
							this.store.insert(this.store.getCount(), new Record({}));
							if (this.grid.getHeight()<180) this.grid.setHeight(this.grid.getHeight()+20);
							// (function(){this.grid.startEditing(this.store.getCount()-1,0)}).defer(150,this);
						}
					} else if (this.store.getCount()>1) {
						this.store.remove(el.gridEditor.record);
						if (this.store.getCount()<8 && this.grid.getHeight()>60) this.grid.setHeight(this.grid.getHeight()-20);
					}
				}
			},scope:this
		}
	});	

	this.unitCombo = new Ext.form.ComboBox({
		store:wareunits, valueField:'id', displayField:'val', width:60, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false, value:1
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show:	function(el){el.getEl().dom.parentNode.style.width='60px'; el.getEl().dom.style.height='18px';}
		}
	});
	
	this.taxCombo = new Ext.form.ComboBox({
		store:VAT_store, valueField:'id', displayField:'val', width:100, triggerAction:'all', forceSelection:true, allowBlank:false, value:1
//		,listeners: {
//			render: function(){this.gridEditor.el.useDisplay=true;},
//			show:	function(el){el.getEl().dom.parentNode.style.width='50px'; el.getEl().dom.style.height='18px';}
//		}
	});
	
	this.typeCombo = new Ext.form.ComboBox({
		store:waretypes, valueField:'id', displayField:'val', width:60, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false, value:1
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show:	function(el){el.getEl().dom.parentNode.style.width='60px'; el.getEl().dom.style.height='18px';}
		}
	});
	
	this.bankCombo = new Ext.form.ComboBox({store:[], name:'bank', valueField:'name', displayField:'name',  width:84, listWidth:145, listClass:'rtl', triggerAction:'all', forceSelection:true});
	
	this.paymentTypeCombo = new Ext.form.ComboBox({store:['מזומן','שיק','אשראי','ה.קבע/ה.בנקאית'], hiddenName:'ptype', width:75, listWidth:110, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false	});
	
	this.datePicker = {name:'date', xtype:'datefield', width:90, format:'d/m/Y', cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false, value:(cfg.date ? cfg.date:today)},
	
	this.store.on('datachanged', function(){this.grid.plugins.refreshSummary()}, this);
	
	this.store.on('load', function(){
			if (d.isum) {
				var isum=0;
				this.store.each(function(r){isum=isum+(r.data.sum_tax==null ? 0: parseFloat(r.data.sum_tax));});
				if (d.isum!=isum) {
					var k=isum/d.isum;
					this.store.each(function(r){
						r.set('sum_tax', r.get('sum_tax')/k);
						r.set('sum', Math.round(r.get('sum_tax')/(100+VAT*(r.get('VAT')==1 ? 1:0))*10000)/100);
						r.set('amount', '');	r.set('price', '');
					});
				}
			}
	}, this);
	
	
	this.client_id=0;
	
	this.getClientLastPayment = function(client_id){
		//Load last payment row for accept
		if (this.type!='accept' && this.type!='accept_tmp' && this.type!='invoice_accept') return;
		this.acceptStore.removeAll();
		Ext.Ajax.request({
			url: '?m=common/accounting&f=get_client_last_payment&client_id='+client_id,
			success: function(r,o){
				r=Ext.decode(r.responseText);
				if (!ajRqErrors(r)) {
					if (r.payment[0]) {
						
						//alert(r.payment[0].group_name)
						
						this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:r.payment[0].ptype, date:new Date(), bank:r.payment[0].bank, dept:r.payment[0].dept, account:r.payment[0].account}));
						this.IncomeGroupCombo.store.loadData({data: {id: r.payment[0].group_id, name:r.payment[0].group_name}});
						this.IncomeGroupCombo.setValue(r.payment[0].group_id);
						if (r.payment[0].ptype=='אשראי') this.bankCombo.store.loadData(credit_cards_names);
						else if (r.payment[0].ptype=='שיק') this.bankCombo.store.loadData(banks_names);
					} else this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:'מזומן', date:new Date()}));
					if (r.invoice[0]) {
						this.store.insert(this.store.getCount(),new Record({name:r.invoice[0].name, amount:r.invoice[0].amount, sum_tax:r.invoice[0].sum_tax}));
					} else this.store.insert(this.store.getCount(),new Record({}));
					this.acceptGrid.getSelectionModel().selectRow(0,true);
					this.acceptGrid.startEditing(0,0);
					
				}
			},
			failure: function(r){
				this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:'מזומן', date:new Date()}));
				this.acceptGrid.getSelectionModel().selectRow(0,true);
				this.acceptGrid.startEditing(0,0);
			}, scope:this
		});
	};
	
	/*this.getLastIncomeGroup = function(client_id){
		//Load last income_group row for accept
		if (this.type!='accept' && this.type!='accept_tmp' && this.type!='invoice_accept') return;
		this.acceptStore.removeAll();
		Ext.Ajax.request({
			url: '?m=common/accounting&f=get_last_income_group&client_id='+client_id,
			success: function(r,o){
				r=Ext.decode(r.responseText);
				
				if (!ajRqErrors(r)) {
					if (r.payment[0]) {
						this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:r.payment[0].ptype, date:new Date(), bank:r.payment[0].bank, dept:r.payment[0].dept, account:r.payment[0].account}));
						if (r.payment[0].ptype=='אשראי') this.bankCombo.store.loadData(credit_cards_names);
						else if (r.payment[0].ptype=='שיק') this.bankCombo.store.loadData(banks_names);
					} else this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:'מזומן', date:new Date()}));
					if (r.invoice[0]) {
						this.store.insert(this.store.getCount(),new Record({name:r.invoice[0].name, amount:r.invoice[0].amount, sum_tax:r.invoice[0].sum_tax}));
					} else this.store.insert(this.store.getCount(),new Record({}));
					this.acceptGrid.getSelectionModel().selectRow(0,true);
					this.acceptGrid.startEditing(0,0);
				}
			},
			failure: function(r){
				this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:'מזומן', date:new Date()}));
				this.acceptGrid.getSelectionModel().selectRow(0,true);
				this.acceptGrid.startEditing(0,0);
			}, scope:this
		});
	};
	*/
	this.copyPaymentRecord = function(rec){
		this.acceptStore.insert(this.acceptStore.getCount(), new AcceptRecord({
			ptype:rec.get('ptype'),
			date: (rec.get('ptype')=='שיק' ? rec.get('date').add(Date.MONTH,1):rec.get('date')),
			bank:rec.get('bank'),
			dept:rec.get('dept'),
			account:rec.get('account'),
			number:((rec.get('ptype')=='שיק' && rec.get('number')!=null && parseFloat(rec.get('number'))!=NaN) ? parseFloat(rec.get('number'))+1 : rec.get('number')),
			sum:rec.get('sum')
		}));
		this.acceptGrid.plugins.refreshSummary();

		(function(){this.acceptGrid.startEditing(this.acceptStore.getCount()-1,7)}).defer(150,this);
	}
	
	this.clientCombo=new ClientCombo({width:170, value:cfg.client_id, hidden:cfg.client_id, allowBlank:false});
	this.clientCombo.on('select', function(e){
		// if (this.type!='accept' && this.type!='accept_tmp' && this.type!='invoice_accept') this.grid.startEditing(0,1); else 
		this.getClientLastPayment(e.getValue());
	}, this);
	/*
	this.clientCombo.on('blur', function(e){
		// if (!e.getValue() && !e.getRawValue()) Ext.Msg.alert('שגיאה!','חובה לבחור לקוח!');
		// else 
		if (e.getRawValue() && e.getValue()==e.getRawValue()) {
			Ext.MessageBox.show({
				width:220, title:'אזהרה!', modal:true, msg:'לקוח לא קיים במערכת!<br>האם להקים לקוח חדש?', buttons:Ext.MessageBox.YESNO, icon: Ext.MessageBox.QUESTION, scope:this,
				fn: function(btn) {if(btn=='yes')  this.clientWindow(this.clientCombo.getRawValue()); }
			});
		}
	}, this);
	*/
 //  this.incomegroupCombo = new IncomeGroupCombo(this)
 
 
 function daysInMonth(month,year) {
var m = [31,28,31,30,31,30,31,31,30,31,30,31];
if (month != 2) return m[month - 1];
if (year%4 != 0) return m[1];
if (year%100 == 0 && year%400 != 0) return m[1];
return m[1] + 1;
} 
 
 
var d = new Date;

var maxDate = new Date(d.getFullYear(),d.getMonth(), daysInMonth(d.getMonth()+1,d.getFullYear()));
var minDate = new Date(d.getFullYear(), d.getMonth()-1, 1);

	this.form = new Ext.form.FormPanel({
		labelWidth:50, autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'rtl',
			items: [
			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', maxValue:maxDate, minValue:minDate},
			{xtype:'label', hidden:cfg.client_id, text:'זיהוי לקוח:', style:'display:block; padding-right:6px'},this.clientCombo,
			{xtype:'label', hidden:cfg.client_id, text:'מע"מ:',name:'taxCombo',style:'display:block; padding-right:6px; text-align:right'},this.taxCombo,
			{xtype:'label', hidden:cfg.client_id, text: 'קבוצת הכנסות: ',name:'incom_group', style:'display:block; padding-right:6px'}, this.IncomeGroupCombo,
			{xtype:'label', text:'הערות:'},{name:'notes', xtype:'textfield', width:616, colspan:5, value:(cfg.notes ? cfg.notes:'')
				,listeners:{
					specialkey: function(field, e){
						if (e.getKey() == e.ENTER || e.getKey() == e.TAB) {
							if (!this.store.getCount()) this.store.insert(this.store.getCount(),new Record({VAT:0}));
							if (!this.acceptStore.getCount()) this.acceptStore.insert(this.acceptStore.getCount(),new Record({VAT:0}));
							this.grid.startEditing(0,1);
						}
					},scope:this
				},scope:this
			},
		
			{xtype:'hidden', name:'id'},
			{xtype:'hidden', name:'type', value:this.type}//,{xtype:'hidden', name:'parent_doc_id', value:this.order_id}
		]}
	});
	
	this.acceptWindow = function(d){
		var fd = {};
		if (d.data) {
			var dt = Date.parseDate(d.data.date,'d/m/Y');
			fd = {date:dt.add(Date.MONTH,1).format('d/m/Y'), ptype:d.data.ptype, bank:d.data.bank, dept:d.data.dept, account:d.data.account,number:( d.data.number!='' ? parseFloat(d.data.number)+1:''), sum:''};
		}
		var win=new AcceptWindow({data:fd, callBack:function(d){this.addAcceptRow(d);}.createDelegate(this)});
		win.show();
	}
	
	this.unitRenderer = function (str) {var units = {1:'יחידה',2:'שעה',3:'ליטר',4:'קילוגרם',5:'מטר'}; return units[str];}

	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:60,
		cls:'rtl',
		autoScroll:true,
		enableColumnResize:false,
		// plugins: invoiceGridSummary,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		clicksToEdit:1,
		autoExpandColumn:'name',
		border: false,
		columns: [
			{dataIndex:'code', hidden:true}
			,{header:'פרטים', /*width:530, */dataIndex:'name', id:'name', editor:new this.prodEditor/*, summaryRenderer:function(){return 'סה"כ'}*/}
			// ,{header:'סוג', dataIndex:'type', width:60, renderer:function(str){return waretypes_obj[str]}, editor: this.typeCombo}
			// ,{header:'כמות',dataIndex:'amount', width:50, editor: new numberField}
			// ,{header:"יח'", dataIndex:'unit', width:60, renderer:this.unitRenderer, editor:this.unitCombo}
			// ,{header:"מחיר יח'", dataIndex:'price', width:50, editor: new numberField}
			// ,{header:'לפני מע"מ', dataIndex:'sum', width:70, renderer:function(str){return Ext.util.Format.ilMoney(str)}, summaryType:'sum'}
			// ,{header:'מע"מ', dataIndex:'VAT', width:50, renderer:function(v){if (v==-1) return; else return (v ? VAT+'%':'0%')}, editor: this.taxCombo}
			// ,{header:'סה"כ', dataIndex:'sum_tax', width:70, renderer:function(str){return Ext.util.Format.ilMoney(str)}, summaryType:'sum', editor: new numberField}
		]

		// ,tbar: {
			// items:[
			// {text:'הוסף שורה', iconCls:'add', handler:function(){this.store.insert(this.store.getCount(),new Record()); }, scope:this}
			// ,'-',{text:'מחק שורה',  id:'del_row',  disabled:true, iconCls:'delete', handler:function(){this.delRowPromt()}, scope:this}
			// ,'-',{text:'עגל סכום', iconCls:'round_money', handler:function(){roundSum(this.grid)}, scope:this}
			// ,'-' , 'הנחה:', new Ext.form.ComboBox({id:'ditype', width:75, value:'', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:waretypes})
			// ,{xtype:'numberfield', id:'dval', width:50},new Ext.form.ComboBox({value:'%', id:'dtype', width:40, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:['%','₪']})
			// ,{text:'הוסף הנחה', iconCls:'discount', handler:function(){discount(this.grid)}, scope:this}
			// ]
		// }
	});
	
	// this.grid.on('beforeedit', function(e){	if (this.form.find('name','id')[0].getValue()) {e.cancel=true; return;}	},this);
	
	// this.grid.on('afteredit', gridAfterEdit);
	
	// this.grid.getSelectionModel().on('selectionchange', function(sm, row, r) {this.grid.getTopToolbar().items.get('del_row').setDisabled(!sm.getCount());}, this);
	
	this.acceptStore = new Ext.data.JsonStore({
		fields: ['date','ptype','sum','bank','dept','account','number'],
		remoteSort: true
	});
	
	// this.acceptStore.on('datachanged', function(){this.acceptGrid.plugins.refreshSummary()}, this);

	this.addAcceptRow = function(data) {
		var Row = Ext.data.Record.create(['date','ptype','sum','bank','dept','account','number']);
		var p = new Row(data);
		this.acceptStore.insert(this.acceptStore.getCount(), p);
		this.acceptGrid.selModel.selectRow(this.acceptStore.getCount()-1);
		this.acceptGrid.plugins.refreshSummary();	
	};
	
	this.cardPaymentWindow	= function(t,d){
		if (!this.clientCombo.getValue()) {Ext.Msg.alert('שגיאה!','חובה לבחור לקוח!'); return;}
		var win=new CardPaymentWindow({
			data: this.clientCombo.store.getById(this.clientCombo.getValue()) ? this.clientCombo.store.getById(this.clientCombo.getValue()).data:{},
			callBack:function(d){
				var consoleRef=window.open('','הדפסה',  'width=350,height=250,menubar=0,toolbar=0,status=0,scrollbars=0,resizable=1');
				consoleRef.document.writeln( '<html><head><title>Console</title></head><body bgcolor=white onLoad="self.focus(); self.print();"><pre>'+d.MerchantNote+'<br><hr><br>'+d.ClientNote+'</pre></body></html>');
				consoleRef.document.close()		
				this.addAcceptRow({date:new Date().format('d/m/Y'),ptype:'אשראי',sum:d.sum,bank:d.CreditCompany,account:d.PaimentsNum,number:d.number});
				}.createDelegate(this)
		}
		);	win.show();}
	
	this.acceptGrid = new Ext.grid.EditorGridPanel({ 
		store:this.acceptStore,
		height:160,
		width:'100%',
		cls:'rtl',
		id:'acceptGrid',
		autoScroll:true,
		enableColumnResize:false,
		plugins: acceptGridSummary,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		autoExpandColumn:'number',
		clicksToEdit:1,
		border: false,
		columns: [
			{header:'סוג תשלום', dataIndex:'ptype',  width:85, editor:this.paymentTypeCombo, summaryRenderer:function(){return 'סה"כ'}}
			,{header:'תאריך', dataIndex:'date', width:85, editor:this.datePicker, renderer:Ext.util.Format.dateRenderer('d/m/Y'), css:'direction:ltr;text-align:right;'}
			,{header:"בנק/ח.אשראי", dataIndex:'bank', editor:this.bankCombo, width:90}
			,{header:"סניף", dataIndex:'dept', width:50, editor: new numberField}
			,{header:'חשבון', dataIndex:'account', width:90, editor: new numberField}
			,{header:"מס'  שק/כ.אשראי", dataIndex:'number',id:'number', editor: new numberField}
			,{header:"מס' תשלומים", dataIndex:'payments_num', width:78, editor: new numberField}
			,{header:'סכום', dataIndex:'sum', width:70, editor:new this.sumEditor, summaryType:'sum', renderer:Ext.util.Format.ilMoney}
		]
		,tbar: [
			// {text:'חיוב אשראי', iconCls:'police_add', handler:function(){this.cardPaymentWindow({});}, scope:this},
			// {text:'הוסף שורה', iconCls:'add', handler:function(){
				// this.acceptWindow({})
				// this.acceptStore.insert(this.acceptStore.getCount(),new AcceptRecord({ptype:'מזומן',date: new Date()}));
				// this.acceptGrid.startEditing(this.acceptStore.getCount()-1,0);
				// }, scope:this}
			// ,'-',{text:'הכפל שורה', id:'copy_payment', iconCls:'add', disabled:true, handler:function(){
				// this.acceptWindow(this.acceptGrid.getSelectionModel().getSelected())
				// var rec=this.acceptGrid.getSelectionModel().getSelected();
				// if (rec) this.copyPaymentRecord(rec);

				// }, scope:this}
			// ,'-',{text:'מחק שורה', id:'del_payment', disabled:true, iconCls:'delete', handler:function(){this.delAcceptRowPromt()}, scope:this}
			// ,
			// '-',
			'ניכוי מס במקור:',{xtype:'numberfield', name:'tax_clear', width:30, listeners:{change:function(){acceptGridSummary.refreshSummary();}}},'%'

		]
	});

	this.acceptGrid.on('beforeedit', function(e){
		if (this.form.find('name','id')[0].getValue()) {e.cancel=true; return;}
		var d=this.acceptStore.getAt(e.row); 
		if (
			(d.data.ptype=='מזומן' && e.field!='sum' && e.field!='ptype' && e.field!='date') ||
			(d.data.ptype!='אשראי' && e.field=='payments_num') ||
			(d.data.ptype=='אשראי' && (e.field=='dept' || e.field=='account')) ||
			(d.data.ptype=='ה.קבע/ה.בנקאית' && (e.field=='bank' || e.field=='dept' || e.field=='number' || e.field=='payments_num'))
		)
		{e.cancel=true; this.acceptGrid.startEditing(e.row,e.column+1);}
		},this
	);
	
///////
 this.setBankAccountsEditor = function (){
  this.acceptGrid.getColumnModel().setEditor(4, new Ext.form.ComboBox({store:userAccounts, 
    hiddenName:'account', emptyText:'- בחר -', width:90, listWidth:380, triggerAction:'all', forceSelection:true,
    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right; overflow:hidden">חשבון <b>{number}</b>&nbsp; בנק: &nbsp;<b>{bank_name}</b> סניף: &nbsp; {branch_name}</div></tpl>'),
    itemSelector: 'div.search-item', 
    valueField:'id', displayField:'full_number', allowBlank:false})
    );
  this.acceptGrid.getColumnModel().setRenderer(4, function(v){ if (userAccounts.getById(v)) return userAccounts.getById(v).data.full_number; return '';});
 }
///////
	this.acceptGrid.on('afteredit', function(e){
		// Dump('afteredit');
		if (e.field == 'ptype') {
			// e.record.set('date','');
			e.record.set('dept','');
			e.record.set('bank','');
			e.record.set('account','');
			e.record.set('number','');

			if (e.record.get('ptype')=='אשראי') this.bankCombo.store.loadData(credit_cards_names);
			else if (e.record.get('ptype')=='שיק')  {
//				this.bankCombo.store.loadData(CFG.banks_names);
//				this.acceptGrid.getColumnModel().setEditor(4, new numberField);
//				this.acceptGrid.getColumnModel().setRenderer(4, function(v){return v;});
				this.bankCombo.store.loadData(banks_names);
			}
			else if (e.record.get('ptype')=='ה.קבע/ה.בנקאית') {
			    this.setBankAccountsEditor()
			}
		} 
	},this);

	InvoiceWindow.superclass.constructor.call(this, {
		title: title,
		iconCls:'task',
		layout:'fit', modal:true,
		width:700, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[	this.form, {title:'קבלה', border:false, items:this.grid},	this.acceptGrid],
		buttons:[
			{text:'שמור', disabled:(cfg.id), handler:function(){
				if(!this.form.form.isValid()) return;
				if (!cfg.client_id && this.clientCombo.getValue()==this.clientCombo.getRawValue()) {
					Ext.MessageBox.show({
						width:220, title:'אזהרה!', modal:true, msg:'לקוח לא קיים במערכת!<br>האם להקים לקוח חדש?', buttons:Ext.MessageBox.YESNO, icon: Ext.MessageBox.QUESTION, scope:this,
						fn: function(btn) {if(btn=='yes')  this.clientWindow(this.clientCombo.getRawValue()); }
					});
				} else this.submitForm();
				}, scope:this},
			{text:'הדפס', disabled:(!cfg.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type='+this.type+'&id='+this.id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		
		Ext.MessageBox.show({
			width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, icon: Ext.MessageBox.QUESTION, scope:this,
			fn: function(btn) {if(btn!='yes') return; }
		});
		
		var idata=[], adata=[], isum=0, asum=0, vat_sum=0, error='';
		this.acceptGrid.stopEditing();
		this.acceptStore.each(function(r){
			if (r.data.sum) {
				if (r.data.ptype=='אשראי' && !r.data.payments_num) {error+='חובה להזין כמות תשלומים בשורה '+(r.store.indexOf(r)+1); return false;}
				adata.push(r.data);asum=asum+parseFloat(r.data.sum);
			} else {this.acceptStore.remove(r)}
		}, this);

		this.store.each(function(r){if (r.data.sum_tax<r.data.sum) {error+='סכום '+r.data.name+' שגוי!'; return false;} idata.push(r.data);isum=isum+(r.data.sum_tax==null ? 0: parseFloat(r.data.sum_tax)); vat_sum=vat_sum+(r.data.sum_tax==null ? 0: parseFloat(r.data.sum_tax))-(r.data.sum==null ? 0: parseFloat(r.data.sum))});
		
		if (error!='') {Ext.Msg.alert('הזהרה!', error); return;} 
		
		if (cfg.isum && cfg.isum!=isum) {Ext.Msg.alert('הזהרה!', 'סכום החשבונית לא שווה לסכות תקבול ('+cfg.isum+' ₪) !'); return;} 
		// if (this.type=='invoice_accept' && isum!=asum) {Ext.Msg.alert('שגיאה!','סכום חשבונית לא שווה לסכום קבלה!'); return;}
		this.form.form.submit({
			url: "?m=common/accounting&f=add_accept",
			params:{isum:isum,idata:Ext.encode(idata),asum:asum,adata:Ext.encode(adata),vat_sum:vat_sum, orders_ids:this.orders_ids, payments_ids:cfg.payments_ids, tax_clear:this.acceptGrid.getTopToolbar().find('name','tax_clear')[0].getValue()},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				// this.grid.tbar.hide();
				// if (this.type=='invoice_credit' || this.type=='invoice_debt' || this.type=='invoice_accept') this.grid.tbar.dom.style.display='none';
				if (this.type=='accept' || this.type=='accept_tmp' || this.type=='invoice_accept') this.acceptGrid.tbar.dom.style.display='none';
				// this.syncSize();
				if (cfg.callBack) cfg.callBack();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
				this.syncShadow();
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	
	this.delRowPromt = function(){
		var records = this.grid.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.id);});
		Ext.MessageBox.show({
			width:250, title:'למחוק שורה?', msg:'האם ברצונך לבטל את השורה הזאת ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delRow(records); }
		});
	};
	
	this.delRow = function(records){
		Ext.each(records, function(r){
			row=this.grid.store.indexOf(r);
			this.grid.selModel.selectRow(row);
			this.grid.view.getRow(row).style.border='1px solid red';
			Ext.fly(this.grid.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.grid.store.remove(this.grid.selModel.getSelected());}, scope:this });//this.grid.plugins.refreshSummary();
		},this);
		this.grid.plugins.refreshSummary();	
	};
	
	this.delAcceptRowPromt = function(){
		var records = this.acceptGrid.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.id);});
		Ext.MessageBox.show({
			width:250, title:'למחוק שורה?', msg:'האם ברצונך לבטל את התשלום הזה ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delAcceptRow(records); }
		});
	};
	
	this.delAcceptRow = function(records){
		Ext.each(records, function(r){
			row=this.acceptGrid.store.indexOf(r);
			this.acceptGrid.selModel.selectRow(row);
			this.acceptGrid.view.getRow(row).style.border='1px solid red';
			Ext.fly(this.acceptGrid.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.acceptGrid.store.remove(this.acceptGrid.selModel.getSelected());}, scope:this });//this.acceptGrid.plugins.refreshSummary();
		},this);
		acceptGridSummary.refreshSummary();
	};
	
	this.on('render', function(){if (cfg.client_id) this.getClientLastPayment(cfg.client_id); else this.clientCombo.focus(true,600);},this);

};
Ext.extend(InvoiceWindow, Ext.Window, {});

ExpensePaymentWindow = function(cfg){
	
	var title = "תשלום הוצאות";
	//alert(cfg.total);
	this.payTypeCombo = new Ext.form.ComboBox({
		id:'payTypeCombo',store:[],hiddenName:'pType',width:75,forceSelection:true, allowBlank:false, listWidth:110, listClass:'rtl', triggerAction:'all'
	});
	this.bank_nameCombo = new Ext.form.ComboBox({
		id:'bankName',store:[],hiddenName:'bankName',width:75,forceSelection:true, allowBlank:false,listWidth:110,listClass:'rtl',triggerAction:'all'
	})
	if (cfg.payType =='אשראי' ){
		//alert (cfg.patType)
		this.payTypeCombo.store.loadData(creditCard);
		
		this.bank_nameCombo.store.loadData(credit_cards_names);
	}		
	else if(cfg.payType == 'בנק'){
		this.payTypeCombo.store.loadData(bank_payment_type);
	}
	else if (cfg.payType=='מזומן'){
		this.payTypeCombo.store.loadData(chequeStore);
		//this.creditNum.disable(this.creditNum);
		
	}
		
	var userAccounts = new Ext.data.Store({
	url:'?m=config/config&f=accounts_list',
	totalProperty: 'total',
	root: 'data',
	fields: ['id','bank','branch','bank_name','branch_name','number','full_namber','type','contact_person','tel','fax','balance','notes'],
	sortInfo: {field:'name', direction:'ASC'},
	remoteSort: true
});


		
	
	this.form = new Ext.FormPanel({
		/*region:'north',*/layout:'table', width:'100%',layoutConfig:{columns:4},autoHeight:true,
		items:[
			{xtype:'label', style:'margin-right:10;margin-left:10;', text:'תאריך:'},
	{name:'dateField',xtype:'datefield', id:'dateField', fieldLable:'תאריך',value: cfg.date ,format:'d/m/Y'
			,width:90,style:'text-align:center;margin-right:0;',allowBlank:false,hiddenName:'Date'},
			{xtype:'label',style:'margin-right:100;margin-left:0', text:'זיהוי ספק:'},{id:'sup_name',xtype:'textfield',style:'margin-right:10;margin-left:0;', value:cfg.supName,
			listeners:{
					specialkey: function(field, e){
						if (e.getKey() == e.ENTER || e.getKey() == e.TAB) {
							if (!this.grid.store.getCount()) this.grid.store.insert(this.grid.store.getCount(),new Record({VAT:0}));
							this.grid.getSelectionModel().selectRow(0,true);
							this.grid.startEditing(0,0);
						}
					},scope:this
				},scope:this}
	]
	});
	this.store = new Ext.data.JsonStore({root:'data', remoteSort:false, fields: ['payType','bank_name','creditNum','paymentNum','sum']});

this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:160, cls:'rtl', autoScroll:true,enableColumnResize:false, border: false, clicksToEdit:1,
		plugins: invoiceGridSummary,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		columns: [
			{header:'סוג תשלום', dataIndex:'ptype',  width:85, editor:this.payTypeCombo, forceSelection:true}
			,{header:"בנק/ח.אשראי", dataIndex:'bank',editor:this.bank_nameCombo, width:90, forceSelection:true}
			,{header:"מס'  שק/כ.אשראי", dataIndex:'credit-cheque_num',id:'number', editor: new numberField, forceSelection:true}
			,{header:'חשבון', dataIndex:'account',id:'accountNum', width:90, editor: new numberField}
			,{header:"מס' תשלומים", dataIndex:'payments_num',id:'payNum', width:78, editor: new numberField}
			,{header:'סכום',id:'sum', dataIndex:'sum', width:70, editor:new numberField, summaryType:'sum', renderer:Ext.util.Format.ilMoney, forceSelection:true}
			
		]
		
		
	});
	
	this.setBankAccountsEditor = function (){
  this.grid.getColumnModel().setEditor(4, new Ext.form.ComboBox({store:userAccounts, 
    hiddenName:'account', emptyText:'- בחר -', width:90, listWidth:380, triggerAction:'all', forceSelection:true,
    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right; overflow:hidden">חשבון <b>{number}</b>&nbsp; בנק: &nbsp;<b>{bank_name}</b> סניף: &nbsp; {branch_name}</div></tpl>'),
    itemSelector: 'div.search-item', 
    valueField:'id', displayField:'full_number', allowBlank:false})
    );
  this.grid.getColumnModel().setRenderer(4, function(v){ if (userAccounts.getById(v)) return userAccounts.getById(v).data.full_number; return '';});
 }
		
	this.gridAfterEdit = function(e){
	 //alert (e.grid.store.getCount());
		if ((e.grid.store.getCount()-e.row)==1) e.grid.store.insert(this.store.getCount(), new Record()); 
		e.grid.plugins.refreshSummary();
		 if (e.record.get('ptype')=='שיק')  {
//				this.bankCombo.store.loadData(CFG.banks_names);
//				this.acceptGrid.getColumnModel().setEditor(4, new numberField);
//				this.acceptGrid.getColumnModel().setRenderer(4, function(v){return v;});
				Ext.getCmp('bankName').store.loadData(banks_names);
			}
			else if (e.record.get('ptype')=='ה.קבע/ה.בנקאית') {
				
				this.setBankAccountsEditor()
			}
			
		
		
		
	}
	
	
	this.grid.on('afteredit',this.gridAfterEdit,this);
	
	
	
	this.grid.on('click', function(){
		if (!this.store.getCount()) {
			this.store.insert(this.store.getCount(),new Record());
			this.grid.startEditing(0,0);
		}
	},this);
				
			//Ext.getCmp('dateField').on('select', function(){alert('gg')}, true);
//			Ext.getCmp('payTypeCombo').on('click', function(){if (! Ext.getCmp('payTypeCombo').store.getCount()) {
//			Ext.getCmp('payTypeCombo').store.insert( Ext.getCmp('payTypeCombo').store.getCount(),new Record({VAT:0}));
//			this.grid.startEditing(0,0);
//			}
//			},true);
	
	this.store.on('datachanged', function(){this.grid.plugins.refreshSummary()}, this);
	ExpensePaymentWindow.superclass.constructor.call(this, {
		title:title,
		iconCls:'task',layout:'fit',//,align:'streach'//, modal:true,
		height:300 ,width:600, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[this.form,{title:'תשלום הוצאות', items:this.grid}],
		buttons:[
			{text:'שמור', handler:function(){
				if(!this.form.form.isValid()) return;
				 this.submitForm();
				}, scope:this},
			{text:'סריקת מסמך', handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/scan&client_id='+this.clientCombo.getValue()+'&acc_doc='+this.id+'&client_type=client&folder='+encodeURIComponent('הוצאות')+'&file_name='+encodeURIComponent("חשבונית ספק מס' "+this.form.find('name','sup_doc_num')[0].getValue()), 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this}	
			
		]
	});
	
	this.submitForm = function() {
		Ext.MessageBox.show({
			width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, icon: Ext.MessageBox.QUESTION, scope:this,
			fn: function(btn) {if(btn!='yes') return; }
		});

		var rowsdata=[], sum=0, error='';
		this.store.each(function(r){
			// if (r.data.sum && !r.data.acc_card) error=r.data.name;
			rowsdata.push(r.data);sum=sum+parseFloat(r.data.sum);});
		// if (error!='') {Ext.Msg.alert('הזהרה!', 'לא נבחרה קבוצת הוצאות עבור '+error+'!'); return;} 
		if (sum==0) {Ext.Msg.alert('הזהרה!', 'אין סכום הוצאות!'); return;} 
		this.form.form.submit({
			url: "?m=common/accounting&f=add_expense_pay_row",
			params:{sup_name:Ext.get('sup_name').getRawValue,date:dateField,rowsdata:Ext.encode(rowsdata)},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				Ext.MessageBox.show({
			width:280, title:'אישור', msg:'פעולה בוצעה בהצלחה', buttons:Ext.MessageBox.OK, icon: Ext.MessageBox.QUESTION, scope:this,
			fn: function(btn) {if(btn!='yes') return; }
		});
				// this.grid.tbar.dom.style.display='none';
				// this.syncSize();
				if (cfg.callBack) cfg.callBack();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				// this.buttons[2].setText('סגור');
				this.syncShadow();
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	
			
};
Ext.extend(ExpensePaymentWindow, Ext.Window, {});

ExpenseWindow = function(cfg) {
	var d=cfg.d, grid={};
	this.order_id=d.order_id;
	var expenseGroup;
	this.taxCombo = new Ext.form.ComboBox({
		store:[[100,'מע"מ מלא (100%)'],[66.66,'מע"מ חלקי (66.66%)'],[0,'ללא מע"מ'],[25,'מע"מ חלקי (25%)']], hiddenName:'VAT', valueField:'id', displayField:'val', width:70, triggerAction:'all', forceSelection:true, allowBlank:false, value:0
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show:	function(el){el.getEl().dom.parentNode.style.width='120px'; el.getEl().dom.style.height='18px';}
		}
	});
	//this.paymentTypeCombo = new Ext.form.ComboBox({ hiddenName:'ptype', width:75, listWidth:110, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false	});
//	this.payTypeCombo = new Ext.form.ComboBox({
//		hiddenName:'pType',width:75,forceSelection:true, allowBlank:false, listWidth:110, listClass:'rtl', triggerAction:'all'
//	});
	




	this.paymentCombo = new Ext.form.ComboBox({
		store:[[0,'מזומן'],[1,'אשראי'],[2,'בנק'],[3,'לא שולם']], hiddenName:'payment', valueField:'id', displayField:'val', width:70, triggerAction:'all', forceSelection:true, allowBlank:true,blankText:'שדה זה הכרחי',value:0
		,listeners: {
			render: function(){this.gridEditor.el.useDisplay=true;},
			show:	function(el){el.getEl().dom.parentNode.style.width='120px'; el.getEl().dom.style.height='18px';}
		}

	});
	//Ext.getCmp('selectList').setValue(store.getAt('0').get('id'));
        
	//{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange'}
	
	this.store = new Ext.data.JsonStore({root:'data', remoteSort:false, fields: ['id','name','group_id','group_name','VAT','sum']});
	
	var Record = Ext.data.Record.create(['id','name','group_id','group_name','VAT','sum']);
	
	this.getSupplierLastSupply = function(client_id){
		//Load last payment row for accept
		this.store.removeAll();
		Ext.Ajax.request({
			url: '?m=common/accounting&f=get_supplier_last_supply&client_id='+client_id,
			success: function(r,o){
				r=Ext.decode(r.responseText);
				if (!ajRqErrors(r)) {
					if (r.expense[0]) {
						this.form.find('hiddenName','expense_type')[0].setValue(r.expense[0].expense_type);
						this.store.insert(this.store.getCount(),new Record({name:r.expense[0].name, group_id:r.expense[0].group_id, group_name:r.expense[0].group_name, VAT:(r.expense[0].VAT ? parseFloat(r.expense[0].VAT):0)}));
					} else this.store.insert(this.store.getCount(),new Record({VAT:0}));
					// this.grid.getSelectionModel().selectRow(0,true);
					// this.grid.startEditing(0,0);
				}
			},
			failure: function(r){
				this.store.insert(this.store.getCount(),new Record({VAT:0}));
				// this.grid.getSelectionModel().selectRow(0,true);
				// this.grid.startEditing(0,0);
			}, scope:this
		});
	};
	
	
	this.clientWindow =  function() {	
		var win=new NewClientWindow({
			name:this.clientCombo.getRawValue(), cnumber:this.form.find('name','sup_num')[0].getValue(), 
			callBack:function(id,name){	this.clientCombo.setValue(id);this.clientCombo.setRawValue(name);this.submitForm();	}.createDelegate(this)
		}); 
		win.show();
	};

	this.expenseGroupCombo = function(prnt){
		return new Ext.form.ComboBox({
			store: new Ext.data.Store({
				proxy: new Ext.data.HttpProxy({	url:'?m=config/config&f=expenses_group_list'}),
				reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['id','code','name'])
			}),
			onTriggerClick:function(el){
				var win;
				var expensesgroupsGrid = new ExpensesgroupsGrid({},{height:363, layout:'fit', rec:prnt.grid.getSelectionModel().getSelected()});
				expensesgroupsGrid.on('beforeedit', function(e){ return (e.record.data.id ? false : true) });
				expensesgroupsGrid.on('celldblclick', function(g,r,c){
					var r = g.store.getAt(r).data;
					this.rec.set('group_id',r.id);
					this.rec.set('group_name',r.name);
					this.rec.set('VAT',parseFloat(r.VAT));
					win.close();
				});
				win = new Ext.Window ({title:'קבוצות הוצאות', width:300, height:400, items:expensesgroupsGrid});
				win.show();
			},
			width:170, typeAhead:true, loadingText:'...טוען', editable:true,// hiddenName:'part_id', valueField:'name',
			//width:170, typeAhead:true, loadingText:'...טוען', editable:true, hiddenName:'expense_group', valueField:'id',
			displayField:'name', minChars:2, pageSize:8, hideMode:'display', triggerClass:'x-form-search-trigger',
			tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">{name} <b>({acc_card})</b></div></tpl>'),
			itemSelector: 'div.search-item',tabTip:'AAAAAA'
			,listeners: {
				select: function(e,r){expenseGroup=r.json;},
				render: function(){expenseGroup={}; this.gridEditor.el.useDisplay=true;},
				show: 	function(el){expenseGroup={}; el.getEl().dom.parentNode.style.width='120px'; el.getEl().dom.style.height='18px';}
			}
		});
	}
	
	

	this.store.on('datachanged', function(){this.grid.plugins.refreshSummary()}, this);
	
	var rep_periods=[
		[today.format('ym'),today.format('m/Y')],
		[today.add(Date.MONTH,-1).format('ym'),today.add(Date.MONTH,-1).format('m/Y')]
	];
	

	this.client_id=0;
	
	this.clientCombo = new ClientCombo({width:272, value:grid.client_id, hidden:grid.client_id, colspan:3, allowBlank:false})
	this.clientCombo.on('select',function(e,r){
		if (r.data.passport) {
			this.getSupplierLastSupply(e.getValue());
			this.form.find('name','sup_num')[0].setValue(r.data.passport); 
			this.form.find('name','sup_doc_date')[0].focus(true,500);
		} else this.form.find('name','sup_num')[0].focus(true,500);
		e.removeClass('fieldFocusCls');
	},this);
	
	this.form = new Ext.form.FormPanel({
		autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl', 
			items: [			
			// {xtype:'label', text:'תאריך הפקה:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width:82, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false},
			{xtype:'label', hidden:grid.client_id, text:'ספק:'}, this.clientCombo,
			{xtype:'label', text:'עוסק מורשה:', style:'white-space:nowrap; display:block; width:86px;'},{name:'sup_num', xtype:'textfield', width:'100%', allowBlank:false},
			// {xtype:'label', text:'חודש דיווח:'},{xtype:'combo', store:rep_periods, hiddenName:'rep_period',width:82, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false, value:today.format('ym')},
			{xtype:'label', text:'תאריך הוצאה:', style:'display:block; width:78px;'},{id:'Date', name:'sup_doc_date', xtype:'datefield', fieldLabel:'תאריך', width:82, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false},
			{xtype:'label', text:"מס' אסמכתא:", style:'white-space:nowrap; display:block; width:80px;'},{name:'sup_doc_num', xtype:'textfield', width:110, allowBlank:false},
			{xtype:'label', text:'מאפיין למע"מ:', style:'display:block; width:86px;'},{xtype:'combo', store:[[1,'הוצאה'],[2,'ציוד ונכסים']], hiddenName:'expense_type', emptyText:'- בחר -', width:120, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false},
			{xtype:'label', text:'הערות:'},{name:'notes', xtype:'textfield', width:'100%', colspan:7, value:(d.order_num ? d.order_num:'')
				,listeners:{
					specialkey: function(field, e){
						if (e.getKey() == e.ENTER || e.getKey() == e.TAB) {
							if (!this.store.getCount()) this.store.insert(this.store.getCount(),new Record({VAT:0}));
							this.grid.getSelectionModel().selectRow(0,true);
							this.grid.startEditing(0,0);
						}
					},scope:this
				},scope:this
			},
			{xtype:'hidden', name:'id'}
		]}
	});
	
	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:160, cls:'rtl', autoScroll:true,enableColumnResize:false, border: false, clicksToEdit:1,
		plugins: invoiceGridSummary,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		columns: [
			{header:'שם פריט', width:235, dataIndex:'name', editor: new textField, summaryRenderer:function(){return 'סה"כ'}}
			,{header:'סוגי הוצאות', width:120, dataIndex:'group_name', editor:this.expenseGroupCombo(this), scope:this}
			// ,{header:'כמות',dataIndex:'amount', width:50, editor: new numberField}
			// ,{header:"יח'", dataIndex:'unit', width:60, renderer:this.unitRenderer, editor:this.unitCombo}
			// ,{header:"מחיר יח'", dataIndex:'price', width:50, editor: new numberField}
			// ,{header:'לפני מע"מ', dataIndex:'sum', width:70, renderer:function(str){return Ext.util.Format.ilMoney(str)}, summaryType:'sum'}
			,{header:'זיכוי מע"מ', dataIndex:'VAT', width:120, renderer:function(v){var obj={100:'מע"מ מלא (100%)','66.66':'מע"מ חלקי (66.66%)',0:'ללא מע"מ',25:'מע"מ חלקי (25%)'}; return obj[v];}, editor: this.taxCombo}
			,{header:'סה"כ',id:'totalSum', dataIndex:'sum', width:80, renderer:function(str){return Ext.util.Format.ilMoney(str)}, summaryType:'sum', editor: new numberField}
			,{header:'תשלום',id:'payment', dataIndex:'payment', width:120, renderer:function(v){var obj={0:'מזומן',1:'אשראי',2:'בנק',3:'לא שולם'}; return obj[v];}, editor: this.paymentCombo, forceSelection:true}
			,{dataIndex:'group_id', hidden:true}
		]
		
		
	});
//	this.paymentCombo.on('select',function(){
//		
//		var expensePayWin;
//		 if(this.paymentCombo.getRawValue()=='אשראי'){		
//				
//				
//				expensePayWin = new ExpensePaymentWindow({supName:this.clientCombo.getRawValue(), payType:this.paymentCombo.getRawValue()});
//				expensePayWin.show();
//		}
//		else if(this.paymentCombo.getRawValue()=='בנק'){		
//				
//			expensePayWin = new ExpensePaymentWindow({supName:this.clientCombo.getRawValue(), payType:this.paymentCombo.getRawValue()});
//				expensePayWin.show();
//		}
//	},this)
	
	this.gridAfterEdit = function(e){
		if (e.field == 'group_name') {
			if (expenseGroup.id) {
				e.record.set('VAT',expenseGroup.VAT);
				e.record.set('group_id',expenseGroup.id);
			}
		}
		if ((e.grid.store.getCount()-e.row)==1) e.grid.store.insert(this.store.getCount(), new Record({VAT:100})); 
		e.grid.plugins.refreshSummary();	
	}	
	
	this.grid.on('afteredit', this.gridAfterEdit);
	
	this.grid.on('click', function(){
		if (!this.store.getCount()) {
			this.store.insert(this.store.getCount(),new Record({VAT:0}));
			this.grid.startEditing(0,0);
		}
	},this);

	// this.grid.getSelectionModel().on('selectionchange', function(sm, row, r) {
		// this.grid.getTopToolbar().items.get('del_row').setDisabled(!sm.getCount());
	// }, this);
	
	ExpenseWindow.superclass.constructor.call(this, {
		title:'קליטת הוצאות - התקדם ב"Tab"',
		iconCls:'task',	layout:'fit', //modal:true,
		width:700, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[this.form,{title:'הוצאות', items:this.grid}],
		buttons:[
			{text:'שמור', disabled:(d.id), handler:function(){
				if(!this.form.form.isValid()) return;
				if (this.clientCombo.getValue()==this.clientCombo.getRawValue()) {
					Ext.MessageBox.show({
						width:220, title:'אזהרה!', modal:true, msg:'ספק לא קיים במערכת!<br>האם להקים ספק חדש?', buttons:Ext.MessageBox.YESNO, icon: Ext.MessageBox.QUESTION, scope:this,
						fn: function(btn) {if(btn=='yes') this.clientWindow(); }
					});
				} else this.submitForm();
				}, scope:this},
			{text:'סריקת מסמך', disabled:(!d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/scan&client_id='+this.clientCombo.getValue()+'&acc_doc='+this.id+'&client_type=client&folder='+encodeURIComponent('הוצאות')+'&file_name='+encodeURIComponent("חשבונית ספק מס' "+this.form.find('name','sup_doc_num')[0].getValue()), 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this}	
			//,{text:'הדפס', disabled:(!d.id), handler:function(){
					// var win=window.open('http://'+location.host+'/crm?m=common/print_doc&id='+this.id+'&type=expense', 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					// win.focus();
				// }, scope:this},
			//,{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		Ext.MessageBox.show({
			width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, icon: Ext.MessageBox.QUESTION, scope:this,
			fn: function(btn) {if(btn!='yes') return; }
		});
		var ifPayed;
		if(this.paymentCombo.getRawValue()==0 || this.paymentCombo.getRawValue()=='לא שולם'){
			ifPayed = 0;
		}
		else{
			ifPayed = 1;
		}
			
		
		var rowsdata=[], sum=0, error='';
		this.store.each(function(r){
			// if (r.data.sum && !r.data.acc_card) error=r.data.name;
			rowsdata.push(r.data);sum=sum+parseFloat(r.data.sum);});
		
		
		
		// if (error!='') {Ext.Msg.alert('הזהרה!', 'לא נבחרה קבוצת הוצאות עבור '+error+'!'); return;} 
		if (sum==0) {Ext.Msg.alert ('הזהרה!', 'אין סכום הוצאות!'); return;} 
		
		this.form.form.submit({
			url: "?m=common/accounting&f=add_expense",
			params:{ifPayed:ifPayed, sum:sum,rowsdata:Ext.encode(rowsdata)},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				var expensePayWin;
				//alert (this.paymentCombo.getRawValue())
		 if(this.paymentCombo.getRawValue()=='אשראי'){
			 
				expensePayWin = new ExpensePaymentWindow({supName:this.clientCombo.getRawValue(), payType:this.paymentCombo.getRawValue(),date:Ext.getCmp('Date').getRawValue()});
				expensePayWin.show();
		}
		else if(this.paymentCombo.getRawValue()=='בנק'){		
			expensePayWin = new ExpensePaymentWindow({total:document.getElementById("totalSum").value,supName:this.clientCombo.getRawValue(), payType:this.paymentCombo.getRawValue(), date:Ext.getCmp('Date').getRawValue()});			expensePayWin.show();
		}
		else if (this.paymentCombo.getRawValue()=='מזומן'){
			expensePayWin = new ExpensePaymentWindow({supName:this.clientCombo.getRawValue(), payType:this.paymentCombo.getRawValue(),date:Ext.getCmp('Date').getRawValue()});
			expensePayWin.show();
		}
				// this.grid.tbar.dom.style.display='none';
				// this.syncSize();
				if (cfg.callBack) cfg.callBack();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				// this.buttons[2].setText('סגור');
				this.syncShadow();
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	
	this.delRowPromt = function(){
		var records = this.grid.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.id);});
		Ext.MessageBox.show({
			width:250, title:'למחוק פריט?', msg:'האם ברצונך לבטל את הפריט הזה ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delRow(records); }
		});
	};
	
	this.delRow = function(records){
		Ext.each(records, function(r){
			row=this.grid.store.indexOf(r);
			this.grid.selModel.selectRow(row);
			this.grid.view.getRow(row).style.border='1px solid red';
			Ext.fly(this.grid.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.grid.store.remove(this.grid.selModel.getSelected());}, scope:this });//this.grid.plugins.refreshSummary();
		},this);
		this.grid.plugins.refreshSummary();	
	};
	
	// if (d.order_id) {this.store.load();	}
	
	// this.store.insert(this.store.getCount(),new Record({VAT:0})); 
	this.on('afterrender', function(){this.form.find('hiddenName','client_id')[0].focus(true,500);}, this);
};
Ext.extend(ExpenseWindow, Ext.Window, {});


DepositWindow = function(cfg) {//grid, d
	var title='הפקדה לחשבון בנק';
	var cashBalance=0;
    var checkColumn = new Ext.grid.CheckColumn({header:"#", dataIndex:'deposit',  width:30 });

	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=cheques_list',
		totalProperty:'total', root:'data',
		fields: ['id','deposit',{name:'date', type:'date', dateFormat:'Y-m-d'},'ptype','sum','bank','dept','account','number'],
		remoteSort:true
	});
	
	this.form = new Ext.form.FormPanel({
		autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'rtl', 
			items: [			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false},
			{xtype:'label', text:'חשבון:'},{xtype:'combo', store:userAccounts, hiddenName:'account', emptyText:'- בחר -', width:423, triggerAction:'all', forceSelection:true,
				tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right; overflow:hidden">חשבון <b>{number}</b>&nbsp; בנק: &nbsp;<b>{bank_name}</b> סניף: &nbsp; {branch_name}</div></tpl>'),
				itemSelector: 'div.search-item',
				valueField:'id', displayField:'full_namber', allowBlank:false},
			{xtype:'hidden', name:'id'}
		]}
	});
	
	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:160,	width:'100%', cls:'ltr', autoScroll:true, enableColumnResize:false, border: false,
		plugins: [depositGridSummary, checkColumn],
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		autoExpandColumn:'number',
		columns: [
			{header:'סכום', dataIndex:'sum', width:90, summaryType:'sum', renderer:Ext.util.Format.ilMoney, css:'direction:ltr; text-align:right;',
			   editor: new Ext.form.NumberField({allowBlank:false, allowNegative:false, maxValue:10000000,
				listeners:{
					blur: function(el){el.getEl().dom.style.display='none';	},
					show: function(el){el.getEl().dom.style.display=''; el.getEl().dom.style.height='18px';	}
				}
			})}
			,{header:"מס' שק", dataIndex:'number',id:'number'}
			,{header:'חשבון', dataIndex:'account', width:90}
			,{header:"סניף", dataIndex:'dept', width:50}
			,{header:"בנק", dataIndex:'bank', width:90}
			,{header:'תאריך', dataIndex:'date', width:75, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}//
			,{header:'סוג תשלום', dataIndex:'ptype',  width:75, summaryRenderer:function(){return 'סה"כ'}}
			,checkColumn
		]
	});
	
	this.grid.on('beforeedit', function(e){var d=this.store.getAt(e.row); if (d.data.ptype!='מזומן') e.cancel=true;});

	// this.store.on('load', function(){this.grid.plugins[0].refreshSummary()}, this);
	
	DepositWindow.superclass.constructor.call(this, {
		title: title, iconCls:'task', layout:'fit', modal:true,	width:630, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[	this.form, {title:'הפקדות',  items:	this.grid}],
		buttons:[
			{text:'שמור', disabled:(cfg.d.id), handler:function(){
				if(!this.form.form.isValid()) return;
				Ext.MessageBox.show({
					width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, scope:this,
					fn: function(btn) {if(btn=='yes') this.submitForm(); }
				});
				}, scope:this},
			{text:'הדפס', disabled:(!cfg.d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type=deposit&id='+this.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		var ddata=[], sum=0, sumCash=0;
		this.store.each(function(r){
			if (r.data.deposit) {
				ddata.push(r.data);sum=sum+(r.data.deposit ? parseFloat(r.data.sum):0);
				if (r.data.ptype=='מזומן') sumCash=sumCash+(r.data.deposit ? parseFloat(r.data.sum):0);
			}
		});
		// if (!sum || !ddata) {Ext.Msg.alert('הזהרה!', 'לא נבחרו שיקים להפקדה!'); return;}
		if (!ddata) {Ext.Msg.alert('הזהרה!', 'לא נבחרו שיקים להפקדה!'); return;}
		if (sumCash>cashBalance) {Ext.Msg.alert('הזהרה!', 'סכום הפקדת מזומנים מעל יתרת הקופה ('+Ext.util.Format.ilMoney(cashBalance)+')!'); return;}

		this.form.form.submit({
			url: "?m=common/accounting&f=add_deposit",
			params:{ddata:Ext.encode(ddata), sum:sum},
			waitTitle:'אנא המתן...', waitMsg:'טוען...',
			success: function(f,a){
				if (cfg.callBack) cfg.callBack();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	
	this.store.load({params:{cheque_status:'0,3', cashBalance:1}, callback:function(r){
		cashBalance=parseFloat(this.store.reader.jsonData.cashBalance);
		// var recId = 10000; // provide unique id for the record
		// var r = new this.store.recordType({id:0,deposit:false,date:'',ptype:'מזומן',sum:cashBalance,bank:'',dept:'',account:'',number:''}, ++recId); // create new record
		// this.store.insert(0, r);
		this.grid.plugins[0].refreshSummary()
	}, scope:this});
};
Ext.extend(DepositWindow, Ext.Window, {});

RefundWindow = function(grid, d) {
	// Dump(grid);
	// this.type=t;
	this.order_id=d.order_id;
	var title='החזרת שיקים';
	// if (userAccounts.getCount()==0) userAccounts.load(); 
	
    var checkColumn = new Ext.grid.CheckColumn({header:"#", dataIndex:'deposit',  width:30 });
	this.clientCombo = new ClientCombo({width:200, listWidth:300, value:grid.client_id, hidden:grid.client_id});//, allowBlank:false
	
	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=cheques_list',
		totalProperty: 'total',	root:'data',
		fields: ['id','deposit',{name:'date', type:'date', dateFormat:'Y-m-d'},'ptype','sum','bank','dept','account','number'],
		remoteSort: true
	});
	
	this.form = new Ext.form.FormPanel({
		autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:5}, baseCls: 'x-plain', cls:'rtl', 
			items: [			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false},
			{xtype:'label', hidden:grid.client_id, text:'סוג החזרה:', style:'display:block; padding-right:9px; text-align:right;'},
			{xtype:'combo', id:'refund_type', store:[['bankclient','מבנק ללקוח'],['bankcash','מבנק לקופת שיקים'],['cashclient','מקופת שיקים ללקוח']], hiddenName:'refundType', emptyText:'- בחר -', width:200, triggerAction:'all', forceSelection:true, allowBlank:false
				,listeners:{select:function(e){
					this.store.removeAll();this.clientCombo.setValue();
				},scope:this}
			},
			{xtype:'button', rowspan:2, text:'הצג שיקים', handler:function(){
					var refund_type=Ext.getCmp('refund_type').getValue();
					var client_id=this.clientCombo.getValue();
					var account = this.form.find('hiddenName','account')[0].getValue();
					
					if (refund_type=='') {Ext.Msg.alert('הזהרה!', 'נא לבחור סוג החזרה!'); return;}
					else if (refund_type=='cashclient' && !client_id) {Ext.Msg.alert('הזהרה!', 'נא לבחור לקוח!'); return;}
					else if (refund_type=='bankcash' && !account) {Ext.Msg.alert('הזהרה!', 'נא לבחור חשבון בנק!'); return;}
					else if (refund_type=='bankclient' && !account) {Ext.Msg.alert('הזהרה!', 'נא לבחור חשבון בנק!'); return;}
					else if (refund_type=='bankclient' && !client_id) {Ext.Msg.alert('הזהרה!', 'נא לבחור לקוח!'); return;}
					this.store.load({params:{cheque_status:(refund_type=='cashclient'? 3:1), client_id:client_id}});
				}, scope:this},
			{xtype:'label', text:'חשבון:'},{xtype:'combo', store:userAccounts, hiddenName:'account', emptyText:'- בחר -', width:220, listWidth:400, triggerAction:'all', forceSelection:true,
				tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right; overflow:hidden">חשבון <b>{number}</b>&nbsp; בנק: &nbsp;<b>{bank_name}</b> סניף: &nbsp; {branch_name}</div></tpl>'),
				itemSelector: 'div.search-item',
				valueField:'id',
				displayField:'number', allowBlank:false},
			{xtype:'label', hidden:grid.client_id, text:'לקוח:', style:'display:block; padding-right:9px; text-align:right;'},this.clientCombo,
			{xtype:'label', text:'הערות:'},{name:'notes', xtype:'textfield', width:560, colspan:4, value:(d.notes ? d.notes:'')},
			{xtype:'hidden', name:'id'}
		]}
	});
	
	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:160,
		width:'100%',
		cls:'rtl',
		autoScroll:true,
		enableColumnResize:false,
		plugins: [depositGridSummary, checkColumn],
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		autoExpandColumn:'number',
		border: false,
		columns: [
			checkColumn
			,{header:'סוג תשלום', dataIndex:'ptype',  width:75, summaryRenderer:function(){return 'סה"כ'}}
			,{header:'תאריך', dataIndex:'date', width:75, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}
			,{header:"בנק", dataIndex:'bank', width:90}
			,{header:"סניף", dataIndex:'dept', width:50}
			,{header:'חשבון', dataIndex:'account', width:90}
			,{header:"מס' שיק", dataIndex:'number',id:'number'}
			,{header:'סכום', dataIndex:'sum', width:90, summaryType:'sum', renderer:Ext.util.Format.ilMoney}
		]
	});
	
	this.store.on('load', function(){this.grid.plugins[0].refreshSummary()}, this);
	
	RefundWindow.superclass.constructor.call(this, {
		title: title,
		iconCls:'task',
		layout:'fit', modal:true,
		width:630, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[	this.form, {title:'שיקים להחזרה',  items:	this.grid}],
		buttons:[
			{text:'שמור', disabled:(d.id), handler:function(){
				if(!this.form.form.isValid()) return;
				Ext.MessageBox.show({
					width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, scope:this,
					fn: function(btn) {if(btn=='yes') this.submitForm(); }
				});
				}, scope:this},
			{text:'הדפס', disabled:(!d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type=refund_'+Ext.getCmp('refund_type').getValue()+'&id='+this.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		var ddata=[], sum=0;
		this.store.each(function(r){if (r.data.deposit) ddata.push(r.data); sum=sum+(r.data.deposit ? parseFloat(r.data.sum):0);});
		if (!sum || !ddata) {Ext.Msg.alert('הזהרה!', 'לא נבחרו שיקים להחזרה!'); return;}
		this.form.form.submit({
			url: "?m=common/accounting&f=add_refund",
			params:{ddata:Ext.encode(ddata), sum:sum},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				grid.store.reload();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
};
Ext.extend(RefundWindow, Ext.Window, {});

ChequeWindow = function(grid, d) {
	var title='הפקת שיק';
	
	this.clientCombo = new ClientCombo({width:195, listWidth:300, value:grid.client_id, hidden:grid.client_id, allowBlank:false});
	this.clientCombo.on('select', function(e){this.store.load({params:{status:1, client_id:e.getValue()}})},this);
	
	this.form = new Ext.form.FormPanel({
		autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'rtl', 
			items: [			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width: 100, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false},
			{xtype:'label', text:'חשבון:', style:'padding-right:5px'},{xtype:'combo', store:userAccounts, hiddenName:'account', emptyText:'- בחר -', width:195, listWidth:400, triggerAction:'all', forceSelection:true,
				tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right; overflow:hidden">חשבון <b>{number}</b>&nbsp; בנק: &nbsp;<b>{bank_name}</b> סניף: &nbsp; {branch_name}</div></tpl>'),
				itemSelector: 'div.search-item',
				valueField:'id',
				displayField:'number', allowBlank:false},
			{xtype:'label', text:'סכום:'},{xtype:'textfield', name:'sum', width:100, allowBlank:false},	
			{xtype:'label', hidden:grid.client_id, text:'לקוח:', style:'padding-right:5px'},this.clientCombo,
			{xtype:'hidden', name:'id'}
		]}
	});
	
	ChequeWindow.superclass.constructor.call(this, {
		title: title,
		iconCls:'task',
		layout:'fit', modal:true,
		width:410, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons:[
			{text:'שמור', disabled:(d.id), handler:function(){
				if(!this.form.form.isValid()) return;
				Ext.MessageBox.show({
					width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, scope:this,
					fn: function(btn) {if(btn=='yes')  this.submitForm(); }
				});
				}, scope:this},
			{text:'הדפס', disabled:(!d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type=cheque&id='+this.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		this.form.form.submit({
			url: "?m=common/accounting&f=add_cheque",
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				grid.store.reload();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
};
Ext.extend(ChequeWindow, Ext.Window, {});

DepositCreditsWindow = function(grid, d) {

    var checkColumn = new Ext.grid.CheckColumn({header:"#", dataIndex:'deposit',  width:30 });

	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=credit_payments_list',
		totalProperty:'total', root:'data',
		fields: ['payment_id','deposit',{name:'accept_date', type:'date', dateFormat:'Y-m-d'},{name:'date', type:'date', dateFormat:'Y-m-d'},'ptype','sum','bank','dept','number'],
		remoteSort:true
	});
	
	this.form = new Ext.form.FormPanel({
		autoHeight:true, baseCls:'x-plain',// style:'padding:0 6px',
		items:{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'rtl', 
			items: [			
			{xtype:'label', text:'תאריך:'},{name:'date', xtype:'datefield', fieldLabel:'תאריך', width:100, colspan:3, format:'d/m/Y', value: new Date(), cls:'ltr', style:'text-align:right', vtype:'daterange', allowBlank:false},
			{xtype:'label', text:'חברת אשראי:'},{xtype:'combo', hiddenName:'bank', width: 150, emptyText:'- בחר -', mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:['וויזה','ישראכרט/מאסטרכארד','לאומי קארד','דיינרס','אמריקן אקספרס'],
				listeners: {
					select: function(e){this.store.load({params:{bank:e.getValue()}})},
					scope:  this}

			},
			{xtype:'label', text:'חשבון:', style:'padding-right:10px;'},{xtype:'combo', store:userAccounts, hiddenName:'account', emptyText:'- בחר -', width:323, triggerAction:'all', forceSelection:true,
				tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right; overflow:hidden">חשבון <b>{number}</b>&nbsp; בנק: &nbsp;<b>{bank_name}</b> סניף: &nbsp; {branch_name}</div></tpl>'),
				itemSelector: 'div.search-item',
				valueField:'id', displayField:'full_namber', allowBlank:false},
			{xtype:'hidden', name:'id'}
		]}
	});
	
	this.grid = new Ext.grid.EditorGridPanel({ 
		store:this.store,
		height:160,	width:'100%', cls:'ltr', autoScroll:true, enableColumnResize:false, border: false,
		plugins: [depositGridSummary, checkColumn],
		selModel: new Ext.grid.RowSelectionModel({singleSelect:true}),
		autoExpandColumn:'number',
		columns: [
			{header:'סכום', dataIndex:'sum', width:90, summaryType:'sum', renderer:Ext.util.Format.ilMoney, css:'direction:ltr; text-align:right;',
			   editor: new Ext.form.NumberField({
				allowBlank:false, allowNegative:false, maxValue:100000,
				listeners:{
					blur: function(el){el.getEl().dom.style.display='none';	},
					show: function(el){el.getEl().dom.style.display=''; el.getEl().dom.style.height='18px';	}
				}
			})}
			,{header:"מס' כרטיס", dataIndex:'number',id:'number'}
			,{header:'תשלום', dataIndex:'dept', width:90}
			,{header:"ח. אשראי", dataIndex:'bank', width:90}
			,{header:'תאריך פרעון', dataIndex:'date', width:75, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}//
			,{header:'תאריך קבלה', dataIndex:'accept_date', width:75, css:'direction:ltr;text-align:right;', renderer:function(v) {return Ext.util.Format.date(v,'d/m/Y')}}//
			,{header:'סוג תשלום', dataIndex:'ptype',  width:75, summaryRenderer:function(){return 'סה"כ'}}
			,checkColumn
		]
	});
	
	this.store.on('load', function(){this.grid.plugins[0].refreshSummary()}, this);
	
	DepositCreditsWindow.superclass.constructor.call(this, {
		title:'קליטת תשלומי אשראי', iconCls:'task', layout:'fit', modal:true,	width:630, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[	this.form, {title:'הפקדות',  items:	this.grid}],
		buttons:[
			{text:'שמור', disabled:(d.id), handler:function(){
				if(!this.form.form.isValid()) return;
				Ext.MessageBox.show({
					width:280, title:'אזהרה!', msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', buttons:Ext.MessageBox.YESNO, scope:this,
					fn: function(btn) {if(btn=='yes') this.submitForm(); }
				});
				}, scope:this},
			{text:'הדפס', disabled:(!d.id), handler:function(){
					var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type=deposit&id='+this.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
					win.focus();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		var ddata=[], sum=0;
		this.store.each(function(r){if (r.data.deposit) ddata.push(r.data);sum=sum+(r.data.deposit ? parseFloat(r.data.sum):0);});
		if (!sum || !ddata) {Ext.Msg.alert('הזהרה!', 'לא נבחרו תשלומים להפקדה!'); return;}
		this.form.form.submit({
			url: "?m=common/accounting&f=add_deposit",
			params:{ddata:Ext.encode(ddata), sum:sum},
			waitTitle:'אנא המתן...', waitMsg:'טוען...',
			success: function(f,a){
				grid.store.reload();
				this.id = Ext.decode(a.response.responseText).id;
				this.form.find('name','id')[0].setValue(this.id);
				this.buttons[0].disable();
				this.buttons[1].enable();
				this.buttons[2].setText('סגור');
			},//this.close(); 
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
};
Ext.extend(DepositCreditsWindow, Ext.Window, {});