

Ext.app.App=function(config){
	Ext.apply(this, config);
	this.addEvents({
		'ready':true,
		'beforeunload':true,
		'moduleactioncomplete':true
	});
	Ext.onReady(this.initApp, this);
};

Ext.extend(Ext.app.App, Ext.util.Observable, {
	desktopConfig:null,
	memberInfo:null,		// The member info. Example:{ group:'demo', name:'Todd Murdock' }
	modules:null,
	plugins:null,
	privileges:null,
	isReady:false,
	connection:'services.php',
	requestQueue:[],
	init:Ext.emptyFn,

	initApp :function(){
		this.init();
		this.preventBackspace();
		this.desktop=new Ext.Desktop(Ext.applyIf({app:this}, this.desktopConfig));

		if(this.plugins){	// init plugins
			if(Ext.isArray(this.plugins)){
				for(var i=0, len=this.plugins.length; i < len; i++) this.plugins[i]=this.initPlugin(this.plugins[i]);
			}else this.plugins=this.initPlugin(this.plugins);
		}
		Ext.EventManager.on(window, 'beforeunload', this.onBeforeUnload, this);
		this.fireEvent('ready', this);
		this.isReady=true;
	},

	initPlugin :function(p){
		if(p.ptype && !Ext.isFunction(p.init)) p=Ext.ComponentMgr.createPlugin(p);
		else if(Ext.isString(p)){ p=Ext.ComponentMgr.createPlugin({ptype:p}); }
		p.init(this);
		return p;
	},

	
	requestModule: function(id){
		var m=Ms[id];
		if(m.loaded===true) { if (m.run) m.run(); }
		else this.loadModule(m);
	},
	
	loadModule :function(m){
		// Dump('loadModule('+m+')');
		// Dump(m.files);
		if(m.isLoading) return;
		m.isLoading=true;
		this.requestQueue.push(m.id);
		
		var id=m.id;
		var moduleName=m.launcher.text;
		
		// var notifyWin=this.desktop.showNotification({html:'Loading '+moduleName+'...', title:'Please wait'});
		var notifyWin=this.desktop.showNotification({html:'טוען מודול '+moduleName+'.', title:'טעינה'});/***boris***/
		
		var files=['?m='+id+'&f=init'];
		
		Ext.ux.Loader.load(files,
			function() {
				/***boris
				notifyWin.setIconClass('icon-done');
				notifyWin.setTitle('Finished');
				notifyWin.setMessage(moduleName + ' loaded.');
				***/
				this.desktop.hideNotification(notifyWin);
				notifyWin=null;
				
				this.loadModuleComplete(m);
			},this
		);
	},
	
	loadModuleComplete :function(m){
		m.isLoading=false;
		m.loaded=true;
		// m.init();

		// remove from queue
		var q=this.requestQueue, nq=[], found=false;
		for(var i=0, len=q.length; i<len; i++){
			if(found===false && q[i]===m.id) found=true;//q[i];
			else nq.push(q[i]);
		}
		this.requestQueue=nq;
		
		if (m.run) m.run();
	},

	instantiateModule :function(id){
		var p=Ms[id]; // get the placeholder
		// Dump(p,1);
		if(p && p.loaded===false){
			if( eval('typeof ' + p.className)==='function'){
				var m=eval('new ' + p.className + '()');
				m.app=this;

				var ms=this.modules;
				for(var i=0, len=ms.length; i < len; i++){ // replace the placeholder with the module
					if(ms[i].id===m.id){
						Ext.apply(m, ms[i]); // transfer launcher properties
						ms[i]=m;
					}
				}
				return m;
			}
		}
		return null;
	},

	// {string} v The id or moduleType you want returned
	// getModule :function(v){ return this.modules[v]; },

	// @param {string} method The module method
	// @param {string} id The module id property
	isAllowedTo :function(method, id){
		if(method !== '' && id != ''){
			var p=this.privileges;
			if(p[id] && Ext.isArray(p[id]) && p[id].indexOf(method) !== -1) return true;
		}
		return false;
	},

	getDesktop :function(){ return this.desktop; },

	// {Function} fn The function to call after the app is ready
	// {object} scope The scope in which to execute the function
	onReady :function(fn, scope){
		if(!this.isReady) this.on('ready', fn, scope);
		else fn.call(scope, this);
	},

	onBeforeUnload :function(e){
		if(this.fireEvent('beforeunload', this)===false) e.stopEvent();
	},

	// Prevent the backspace (history -1) shortcut
	preventBackspace :function(){
		var map=new Ext.KeyMap(document, [{
			key:Ext.EventObject.BACKSPACE,
			stopEvent:false,
			fn:function(key, e){
				var t=e.target.tagName;
				if(t!="INPUT" && t!="TEXTAREA") e.stopEvent();
			}
		}]);
	}
});