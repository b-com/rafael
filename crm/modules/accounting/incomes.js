var doc_types = {
	cash_payment:'תשלום במזומן', invoice_credit:'חשבונית מס', invoice_debt:'חשבונית זיכוי', accept:'קבלה',accept_tmp:'קבלה זמנית',invoice_accept:'חשבונית מס קבלה',deposit:'הפקדה לבנק',
	refund_bankclient:'החזרת שיקים ללקוח',refund_cashclient:'החזרת שיקים ללקוח',refund_bankcash:'החזרת שיקים לקופה',expense:'קליטת הוצאות'
};

numberField = Ext.extend(Ext.form.NumberField, {allowBlank:false, allowNegative:false, maxValue:100000,
	listeners:{
		blur: function(el){el.getEl().dom.style.display='none';	},
		show: function(el){el.getEl().dom.style.display=''; el.getEl().dom.style.height='18px';	}
	}
});

dateField = Ext.extend(Ext.form.DateField, {allowBlank:false, listeners:{ show: function(el){el.focus(true,100);}}});

ClientCombo = function(config) {
	ClientCombo.superclass.constructor.call(this, {
		store: new Ext.data.Store({
			proxy: new Ext.data.HttpProxy({url:'?m=common/clients&f=clients_shortList'}),
			reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['id','client','cnumber'])
		}),
		typeAhead: false, fieldLabel:'בחר לקוח:', loadingText: '...טוען', hiddenName:'client_id', valueField:'id',	displayField:'client',
		minChars:3, pageSize:8,	hideTrigger:true,
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right"><i>{client}</i> ת.ז.:&nbsp; {passport}&nbsp; </div></tpl>'),
		itemSelector: 'div.search-item'
	});
	Ext.apply(this, config);
};
Ext.extend(ClientCombo, Ext.form.ComboBox);

var Record = Ext.data.Record.create(['id',{name:'date', type:'date', dateFormat:'d/m/Y'},'num','client_name','type','VAT','sum','sum_tax']);

IncomesGrid= function(d) {
	this.saveInvoice = function(d){
		d.date = d.date.format('Y-m-d');
		Ext.Ajax.request({
			url: '?m=accounting/incomes&f=add_invoice',
			params:d,
			success: function(r,o){
				var d=Ext.decode(r.responseText);
			},
			failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
		return true;
	}
	
	this.gridAfterEdit = function(e){
		if (e.field == 'sum_tax') {
			e.record.set('sum', Math.round(e.record.get('sum_tax')/(100+parseFloat(e.record.get('VAT')))*10000)/100);
			if (this.saveInvoice(e.record.data)) {
				if ((this.store.getCount()-this.store.indexOf(e.record))==1) {//If last record
					e.grid.store.insert(this.store.getCount(), new Record({date:e.record.get('date'), num:e.record.get('num')+1, VAT:e.record.get('VAT')}));
					e.grid.startEditing(this.store.getCount()-1,0); 
				}
			}
		}
	}

	this.store = new Ext.data.JsonStore({
		url:'?m=accounting/incomes&f=incomes_list',
		totalProperty:'total', delCount:'deleted',	root:'data',
		fields: ['id',{name:'date', type:'date', dateFormat:'d/m/Y'},'num','client_name','type','VAT','sum','sum_tax']
	});
	
	this.hlStr = hlStr;
	this.searchField = new GridSearchField({store:this.store, width:200});

	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {q:this.searchField.getRawValue().trim()};
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);
	
	this.store.on('load', function(store, options){
		var data = {date: new Date(), VAT:16};
		if (store.getCount()) {
			record = store.getAt(store.getCount()-1);
			data.num = record.data.num + 1;
			data.VAT = record.data.VAT;
		}
		store.insert(store.getCount(), new Record(data));
	}, this);

	this.invoiceWindow = function(t,d){var win=new InvoiceWindow(this,t,d);	win.show();}
	
	this.printDoc = function(d){
		var win=window.open('http://'+location.host+'/?m=accounting/print_doc&id='+d.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	};
	
	IncomesGrid.superclass.constructor.call(this, {
	    title:'רשימת הכנסות', store:this.store, stripeRows:true, autoExpandColumn:'client_name', iconCls:'plus',
		enableColumnResize:false, clicksToEdit:1,
	    columns: [
			{header:'תאריך', dataIndex:'date', width:95, css:'text-align:right; direction:ltr;', editor:new dateField(), renderer:Ext.util.Format.dateRenderer('d/m/Y') }
			,{header:'אסמכתא', dataIndex:'num', editor: new numberField()}
			,{header:'לקוח', dataIndex:'client_name', id:'client_name', width:150, renderer:this.hlStr.createDelegate(this), editor: new ClientCombo()}
			// ,{header:'סוג פעולה', dataIndex:'type', renderer:function(v){return doc_types[v]}}
			,{header:'סכום לפני מע"מ, ₪', dataIndex:'sum',css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'מע"מ, %', dataIndex:'VAT', css:'direction:ltr; text-align:right;', renderer:function(v){return Ext.util.Format.round(v, 2)+' %';}, editor: new numberField()}
			,{header:'סכום כולל מע"מ, ₪', dataIndex:'sum_tax', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney, editor: new numberField()}
	    ]
		,tbar: [ 'חיפוש:', this.searchField, '-' , 
			{text:'מסמך חדש', iconCls:'police_add', 
				menu:{defaults:{scope:this},
				items: [
					{text:'חשבונית מס', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_credit',{});}},
					{text:'חשבונית זיכוי', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_debt',{});}},
					{text:'קבלה', iconCls:'police_add', handler:function(){this.invoiceWindow('accept',{});}},
					{text:'חשבונית מס - קבלה', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_accept',{});}}
					], scope:this}
			},{text:'הדפס', iconCls:'print', id:'prind_doc', /*disabled:true, */scope:this, handler:function(){this.printDoc(this.getSelectionModel().getSelected().data);}}
		]
		,bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})
		,initComponent: function() {IncomesGrid.superclass.initComponent.call(this);}
	});
	
	this.on('afteredit', this.gridAfterEdit);
	
	this.on('rowcontextmenu', function(grid, row, e){
		var r=this.store.getAt(row);
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'פתח חשבונית', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_credit',r);}, scope:this},
				{text:'הדפס חשבונית',iconCls:'print', handler:function(){this.invoiceWindow('invoice_accept',r);}, scope:this}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);
	
	// this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		// this.getTopToolbar().items.get('prind_doc').setDisabled(sm.getCount()!=1);
	// }, this);

	this.store.load();

};

Ext.extend(IncomesGrid, Ext.grid.EditorGridPanel);

Ms['accounting/incomes'].run = function(cfg){
	var m=Ms['accounting/incomes'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var  incomesGrid = new IncomesGrid()
		win = Crm.desktop.createWindow({ 
			width:900,	
			height:600,	
			items:incomesGrid
		}, m);
	}
		
	win.on('show', function() {
		if (incomesGrid.store.getCount()>0) incomesGrid.startEditing(incomesGrid.store.getCount()-1,0);
	});	
	
	win.show();
};
