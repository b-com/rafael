

var departmentTabs;

Ms.worktime.run = function(cfg){
	// Ext.apply(this, cfg);
	
	var m=Ms.worktime;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		// alert(2);
		departmentTabs = new Ext.TabPanel({activeTab:0, border:false, defaults:{autoScroll:true}, enableTabScroll:true, items:[new DepartmentsGrid()]});

		win = Crm.desktop.createWindow({
            // id:m.id+'-win',
            width:1100,
			height:700,
            // title:l.text,
            // iconCls:l.iconCls,
            // taskbuttonTooltip: '<b>Grid Window</b><br />A window with a grid',
			
            // animCollapse:false,
            // constrainHeader:true,
            // layout: 'fit',
            // shim:false,
            // tools: [{id: 'refresh', handler: Ext.emptyFn, scope: this}],
			items: departmentTabs
         }, m);
	}
	win.show();
};


DepartmentWindow = function(grid, d) {
	this.grid=grid;
	this.form = new Ext.form.FormPanel({
		id:'accForm', autoHeight:true, baseCls:'x-plain', labelWidth:78,
		items:[{xtype:'fieldset', title:'כללי', cls:'rtl', style: 'margin-top:3px; padding:5px', autoHeight:true,
				items: [{
					layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', defaults:{baseCls:'x-plain', cls:'rtl'},
					items:[
						{xtype:'label', text:'סניף:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'combo', store:branches, hiddenName:'branch_id', width:325, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, allowBlank:false}
						,{xtype:'label', text:'שם המחלקה:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'name', width:325, allowBlank:false}
					]
				}]
			},{	xtype:'fieldset', title:'התקשרות', cls:'rtl', style:'margin-top:3px; padding:5px', autoHeight:true,
				items: [{
					layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', defaults:{baseCls:'x-plain', cls:'rtl'},
					items:[
						{xtype:'label', text:'טלפון 1:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'tel1', width:120, allowBlank:true}
						,{xtype:'label', text:'טלפון 2:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'tel2', width:120, allowBlank:true}
						,{xtype:'label', text:'פקס:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'fax', width:120, allowBlank:true}
						,{xtype:'label', text:'דוא"ל:',style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'email', width:120, colspan:3, allowBlank:true}
						// ,{xtype:'label', text:'ישוב:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'city', width:120, allowBlank:true}
						// ,{xtype:'label', text:'מיקוד:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'zip', width:120, allowBlank:true}
						// ,{xtype:'label', text:'כתובת:', style:'display:block; padding-right:5px; text-align:right; width:76px'},{xtype:'textfield', name:'address', width:325, colspan:3, allowBlank:true}
					]
				}]
			},{fieldLabel:'הערות', labelStyle:'text-align:right; padding-right:8px', text:'הערות', height:30, name:'notes', xtype:'textarea', cls:'rtl',  width:325},{xtype:'hidden', name:'id'}
		]
	});
	
	// if (d && d.bank) {this.departments.reload({params: { bank: d.bank }}); Ext.getCmp('departmentCombo').setValue(d.department); Ext.getCmp('departmentCombo').setRawValue(d.department_name);}
	
	DepartmentWindow.superclass.constructor.call(this, {
		title:(d ? 'עידכון פרטי מחלקה קיים':'הוספת מחלקה חדשה'),
		layout:'fit', modal:true,
		width:450, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons:[{text:((d && d.data.id) ? 'עדכן':'הוסף'), handler:function(){this.submitForm();}, scope:this},{text:'סגור', handler:function(){this.close();}, scope:this}]
	});
	
	if (d && d.data.id) this.form.form.loadRecord(d);
	
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		this.form.form.submit({
			url: "?m=departments/departments_f&f=add_department",
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			scope:this,
			success: function(f,a){this.close(); grid.store.reload();},
			failure: function(r,o){ this.close(); ajRqErrors(o.result);}
		});
	}
};
Ext.extend(DepartmentWindow, Ext.Window, {});



DepartmentsGrid = function() {
	// this.store = new Ext.data.JsonStore({
		// url:'?m=departments/departments_f&f=departments_list',
		// totalProperty: 'total',
		// root: 'data',
		// fields: ['id','branch_id','name','tel1','tel2','fax','email','notes'],
		// sortInfo: {field:'name', direction:'ASC'},
		// remoteSort: true
	// });
	
	this.store = new Ext.data.GroupingStore({
		proxy: new Ext.data.HttpProxy({url:'?m=departments/departments_f&f=departments_list', method:'GET'}),
		reader: new Ext.data.JsonReader({root:'data', totalProperty:'total'}, ['id','branch_id','name','city','email','notes']),
		remoteSort:true,
		sortInfo:{field:'name'},//, direction:'DESC'
		groupField:'branch_id'
	});
	
	this.departmentWindow = function(d){
		var win=new DepartmentWindow(this,d);
		win.show(); win.center();
	}

	this.searchField = new GridSearchField({store:this.store, width:120});
	
	this.hlStr = hlStr;
	
	// Dump(branchesById);
	
	DepartmentsGrid.superclass.constructor.call(this, {
		title:'רשימת מחלקות',
		store:this.store,
		iconCls:'grid',
		enableColLock:false,
		loadMask:true,
		stripeRows:true,
		enableHdMenu:false,
		autoExpandColumn:'city',
		view: new Ext.grid.GroupingView({
			groupTextTpl:'<span dir=rtl style="height:16px;line-height:16px; padding-right:18px; background:url(skin/icons/folder.gif) no-repeat top right; zoom:1; display:block;"><b>{[branchesById[values.rs[0].data.branch_id]]}</b></span>'
			// ,enableRowBody:true
		}),
		columns: [
			{id:'branch_id', dataIndex:'branch_id', hidden:true }
			,{id:'city', dataIndex:'city', header:'ישוב', width:300, sortable:true, renderer:this.hlStr.createDelegate(this)}
			// ,{dataIndex:'branch_id', header:'branch_id', width:100, sortable:true, renderer:this.hlStr.createDelegate(this)}
			,{id:'name', dataIndex:'name', header:'שם', width:300, sortable:true, renderer:this.hlStr.createDelegate(this)}
		],
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'חיפוש:', this.searchField
			,'-' , 'סניף:', {xtype:'combo', store:[['','כל הסניפים']].concat(branches), id:'branchFilter', width:100, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, value:'', listeners:{select:function(f){this.store.load()},scope:this} }
			,'-' ,'סטאטוס:', new Ext.form.ComboBox({store:[[0,'פעיל'],[1,'מבוטל']], id:'department_deleted', value:0, width:80, listClass:'rtl', triggerAction:'all', forceSelection:true, 
				listeners:{
					select:function(f){
						var deleted=Ext.getCmp('department_deleted').getValue();
						if (deleted==1) {
							this.getTopToolbar().items.get('del_department').hide();
							this.getTopToolbar().items.get('rest_department').show();
						} else {
							this.getTopToolbar().items.get('del_department').show();
							this.getTopToolbar().items.get('rest_department').hide();
						}
						this.store.load();
					}, 
					scope:this
				}
			})
			, '-' ,{ text:'מחלקה חדשה', iconCls:'add', handler:function(){this.departmentWindow()}, scope:this }
			, '-' ,{ text:'עדכן', id:'edit_department', iconCls:'edit', handler:function(){this.departmentWindow(this.getSelectionModel().getSelected())}, scope:this }
			, '-' ,{ text:'בטל',  id:'del_department', iconCls:'delete', handler:function(){this.delDepartmentPromt();}, disabled:true, scope:this }
				  ,{ text:'שחזר', id:'rest_department', iconCls:'restore', handler:function(){this.restoreDepartmentPromt();}, disabled:true, hidden:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store,pageSize:100})
    });
	
	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			q: this.searchField.getRawValue().trim()
			,deleted: Ext.getCmp('department_deleted').getValue()
			,branch_id: Ext.getCmp('branchFilter').getValue()
		}
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);

	this.store.on('load', function(){	// check for errors
		var callback=function() { this.store.load(); };
		ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
	}, this);
	

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('edit_department').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('del_department').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('rest_department').setDisabled(!sm.getCount());
	}, this);
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row);
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'עדכן', iconCls:'edit', scope:this, handler:function(){this.departmentWindow(this.srData)}, scope:this },
				((Ext.getCmp('department_deleted').getValue()=='deleted') ? {text:'שחזר', iconCls:'restore', scope:this, handler:this.restoreDepartmentPromt} : {text:'בטל', iconCls:'delete', scope:this, handler:this.delDepartmentPromt} )
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});


	this.openDepartment = function(d){	// open department in new tab
		var tab;
		if(!(tab = departmentTabs.getItem('deptab_'+d.id))){ tab=new DepWorkWeek({d:d, grid:this}); departmentTabs.add(tab); }
		departmentTabs.setActiveTab(tab);
	};
	
	this.on('rowdblclick', function(grid, row, e){
		if (e.getTarget().tagName.toLowerCase()=='button') return;
		var d = this.store.getAt(row).data;
		this.openDepartment(d);
	});	

	this.delDepartmentPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		Ext.MessageBox.show({
			width:250, title:'למחוק מחלקה'+(records.length==1?'':'ם')+'?', msg:'האם ברצונך למחוק את המחלקות המסומנים ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delDepartment(records); }
		});
	}
	
	this.delDepartment = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=departments/departments_f&f=del_department&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.restoreDepartmentPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'לשחזר מחלקה'+(records.length==1?'':'ם')+'?', msg:'האם ברצונך לשחזר את המחלקה'+(records.length==1?'':'ים המסומנים')+'?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.restoreDepartment(records); }
		});
	}
	
	this.restoreDepartment = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=departments/departments_f&f=rest_department&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid green';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.on('render', function(){ this.store.load() });
};
Ext.extend(DepartmentsGrid, Ext.grid.GridPanel);

