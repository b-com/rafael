prevPeriods_1 = function(cfg) {


this.typeRenderer = function (str) {var store_types = {part:'חלופים',material:'חומרים',service:'עבודה',fserv:'עבודת חוץ',other:'אחר'}; return store_types[str];}
var IncExp = [[0,'שיטת מעמ'],[1,'שיטה מפורטת']]
var year = cfg.year;
var month = cfg.month;
var rep_period = cfg.rep_period;

this.submitFunc = function(){
	var isum = Ext.getCmp('Form').getForm().findField("taxAbleDls").getValue()+Ext.getCmp('Form').getForm().findField("taxFreeDls").getValue();
	var eTax1 = Ext.getCmp('Form').getForm().findField("realTax").getValue();
	var eTax2 = Ext.getCmp('Form').getForm().findField("ExpTax").getValue();
	var year = Ext.getCmp('Year').getValue();
	var month = Ext.getCmp('Month').getValue();
	//alert(isum);
	Ext.getCmp('Form').form.submit({
			url: "?m=common/accounting&f=prevPeriods",
			params:{month:month,year:year,
				isum:Ext.encode(isum),eTax1:Ext.encode(eTax1),eTax2:Ext.encode(eTax2),formType:1},
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...'
				        })
}


//this.submitFunc = function(){
//	alert(Ext.getCmp('Month').getRawValue())
//}

this.form = new Ext.form.FormPanel({
	id:'Form',x:0,y:0,border:true, height:300,width:300,layout:'absolute',cls:'prevPeriodsBack',bodyStyle: 'background:transparent;',
	items:[
		{ x:20,y:40,xtype:'label', text:'בחר את שיטת ההזנה הרצויה',cls:'x-prevPeriodsTitle'},
		{ x:70,y:60,xtype:'label', text:'(נא למלא נתונים)',cls:'x-prevPeriodsTitle'},
		{
                                xtype: 'radio',
		             id:'m1',
                                boxLabel: 'דו"ח מעמ',
                                name: 'framework',
                                inputValue: 'דו"ח מעמ',
			chacked:true,
			x:200,
			y:100		
                            },
{
                                xtype: 'radio',
		          id:'m2',
                                boxLabel: 'שיטה מפורטת',
                                name: 'framework',
                                inputValue: 'שיטה מפורטת',
			x:30,
			y:100
                            },
		//{ x:200,y:80,id:'comboIncExp',xtype:'combo', store:IncExp, hiddenName:'month', style:'margin-right:78px;', width:100, listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false},
		{x:115,y:140,xtype:'button', text:'הבא',cls:'x-prevPeriodsButt', height: 60, width: 80,
			handler: function(){
//alert(Ext.getCmp('comboIncExp').getRawValue())				
			var win;
	if(Ext.getCmp('m1').getValue()){

		win = new prevPeriods({rep_period:rep_period, month:month, year:year })
				win.show()
	}else if (Ext.getCmp('m2').getValue()){
		//alert(Ext.getCmp('comboIncExp').getRawValue())
		win = new prevPeriodsType2({rep_period:rep_period, month:month, year:year })
				win.show()
	}
}}
//{
//                        xtype: 'radiogroup',
//                        fieldLabel: 'Choose your favorite',
//                        //arrange Radio Buttons into 2 columns
//                        columns: 2,
//                        itemId: 'myFavorite',
//		
//                        items: [
//                            {
//                                xtype: 'radio',
//                                boxLabel: 'ExtJs',
//                                name: 'framework',
//                                checked: true,
//                                inputValue: 'extjs'
//                            },
//                            {
//                                xtype: 'radio',
//                                boxLabel: 'DoJo',
//                                name: 'framework',
//                                inputValue: 'dojo'
//                            },
//                            {
//                                xtype: 'radio',
//                                boxLabel: 'jQuery',
//                                name: 'framework',
//                                inputValue: 'jQuery'
//                            },
//                            {
//                                xtype: 'radio',
//                                boxLabel: 'Prototype',
//                                name: 'framework',
//                                inputValue: 'prototype'
//                            }
//                        ]
//                    },
//                    {
//                        xtype: 'button',
//                        text: 'Press here to see Selection below...'
//                    },
//                    {
//                        xtype: 'displayfield',
//                        value: '',
//                        itemId: 'yourSelection',
//                        fieldLabel: '<b>You have Selected</b>',
//                        anchor: '100%'
//                    }
		
	]
});



	prevPeriods_1.superclass.constructor.call(this, 
	{resizable:false, border:false,layout:'absolute', bodyStyle: 'background:transparent;',
		items:[this.form]
		})

//this.selectedItem = function(){
//	alert(Ext.getCmp('Form').getForm().findField("comboIncExp").getValue());
//}

};
Ext.extend(prevPeriods_1, Ext.Panel);