<?php
//********************************************************************************************************************
function client_phones($r, $separator=', ',$tel_type='') {
	$telStore = array(1=>'בית',2=>'עבודה',3=>'נייד',4=>'פקס בבית',5=>'פקס בעבודה',0=>'אחר');
 
	if ($tel_type=='phone') 	
		{for ($i=0; $i<8; $i++) if ($r["tel$i"] AND $r["tel_type$i"]<4) $tel.=($tel ? $separator : '').$telStore[(int)$r["tel_type$i"]].': '.$r["tel$i"];}
	elseif($tel_type=='fax')
		{for ($i=0; $i<8; $i++) if ($r["tel$i"] AND $r["tel_type$i"]>=4) $tel.=($tel ? $separator : '').$telStore[(int)$r["tel_type$i"]].': '.$r["tel$i"];}
	else
		{for ($i=0; $i<8; $i++) if ($r["tel$i"]) $tel.=($tel ? $separator : '').$telStore[(int)$r["tel_type$i"]].': '.$r["tel$i"];}
	return htmlspecialchars($tel);
}
//********************************************************************************************************************
function client_emails($r, $separator=', ') {
	$emailStore= array(1=>'בית',2=>'עבודה',0=>'אחר');
	for ($i=1; $i<3; $i++) if ($r["email$i"]) $email.=($email ? $separator : '').$emailStore[(int)$r["email_type$i"]].': '.$r["email$i"];
	return htmlspecialchars($email);
}
//********************************************************************************************************************
function client_addresses($r, $separator='; <br>', $amount=3) {
	$adrStore = array(1=>'בית',2=>'חברה/עסק',3=>'עבודה',4=>'למשלוח דואר',0=>'אחר');

	for ($i=0; $i<$amount; $i++) {
		if (trim($r["city$i"]) OR $r["zip$i"] OR trim($r["adr$i"])) {
			$a=$r["city$i"];
			if ($r["adr$i"])  $a.=($a?', ':'').$r["adr$i"];
			if ($r["zip$i"])  $a.=($a?', ':'').$r["zip$i"];
		$adr.= ($adr ? $separator : '').($a ? $adrStore[$r["adr_type$i"]].': ':'').htmlspecialchars($a);
		}
	}
	return ($adr?$adr:'');
}

?>