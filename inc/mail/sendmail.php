<?
//———————————————————————————————————————————————————————————————————————————————————————
function send_mail($to, $subject, $message, $cfg=NULL) {//$from=NULL, $template='template1') {
  global $CFG;
 
 //echo "/*<br><div dir=ltr align=left><pre>".print_r($to,1)."</pre></div><br>*/";exit;

	set_time_limit(1800);
	ini_set('memory_limit', '64M');
	// iconv("UTF-8", "windows-1255", $subject);
	// iconv("UTF-8", "windows-1255", $message);
	if (!$cfg['from']) {	# set default From address
		if ($CFG['mail']['from']) $cfg['from']=$CFG['mail']['from'];
		else {	# set default from adress to 'postmaster@thisdomain'
			eregi("(www\.)?([a-z0-9\.-]+)", $_SERVER['HTTP_HOST'], $host_ar);	# get current domain name without www
			$cfg['from']="postmaster@$host_ar[2]";
		}
	}
	
	//if (!$cfg['template']) $cfg['template']='template1';	# default template
	
	# split $to address and add them to $TO array
	$to_ar=split('[,; ]+', $to);
	if ($to_ar) { foreach ($to_ar as $row) { $row=trim($row); if ($row) $TO[]=$row; } }
	if (!is_array($TO)) { $GLOBALS['send_mail_result'] = "No to address";  return; }

	$V=substr(phpversion(), 0, 1);
	if ($V==5) {
		include_once ( dirname(__FILE__).'/htmlMimeMail5/htmlMimeMail5.php' );
		$mail = new htmlMimeMail5();
	}else {
		include_once ( dirname(__FILE__).'/htmlMimeMail/htmlMimeMail.php' );
		$mail = new htmlMimeMail();
	}

	$cfg['charset']='utf-8';	# set default charset
	if ($cfg['charset']) {	# Set Charset (default utf-8)
		$mail->setHeadCharset($cfg['charset']);
		$mail->setTextCharset($cfg['charset']);
		$mail->setHTMLCharset($cfg['charset']);
	}
	
	$message_text=html2text($message);
	
	$mail->setFrom($cfg['from']);
	if ($cfg['replyto']) $mail->setReplyTo($cfg['replyto']);
	$mail->setSubject($subject);

	if ($cfg['template'] AND is_dir(dirname(__FILE__)."/$cfg[template]")) $template_dir=realpath(dirname(__FILE__)."/$cfg[template]");
	if (is_dir($template_dir) AND is_file("$template_dir/index.htm")) {
		$message=str_replace('{TEXT}', $message, file_get_contents("$template_dir/index.htm"));
		if ($V==5) $mail->setHTML($message, "$template_dir/");
		else $mail->setHtml($message, NULL, "$template_dir/");
	}else {
		$mail->setHTML($message);
	}

	$mail->setText($message_text);
	//if ($cfg['reply'])	$mail->setReturnPath($cfg['reply']);
	if ($cfg['cc'])		$mail->setCc($cfg['cc']);
	if ($cfg['bcc'])	$mail->setBcc($cfg['bcc']);
	
	# attach files 
	if ($cfg['file_data'])	$mail->addAttachment( new stringAttachment($cfg['file_data'][0], $cfg['file_data'][1] ) );
	if ($cfg['file'])	$mail->addAttachment( new stringAttachment(file_get_contents($cfg['file'][0]), ($cfg['file'][1]?$cfg['file'][1]:basename($cfg['file'][0])) ) );
	if ($cfg['files']) {	# $files[]=array('file path', 'file name');
		foreach($cfg['files'] as $file) {
			$mail->addAttachment( new stringAttachment(file_get_contents($file[0]), ($file[1]?$file[1]:basename($file[0])) ) );
		}
	}
	
	if ($CFG['mail']['host'] AND $CFG['mail']['user'] AND $CFG['mail']['pass']) {	// connect to MTA server
		if (!$CFG['mail']['port']) $CFG['mail']['port']=25; # default smtp port
		# setSMTPParams(string host, integer port, string helo, boolean auth, string user, string pass)
		$mail->setSMTPParams($CFG['mail']['host'], $CFG['mail']['port'], 'localhost', true, $CFG['mail']['user'], $CFG['mail']['pass']);
		$method='smtp';
	}else $method='mail';
	
	$result = $mail->send($TO, $method);
	if (!$result) {
		$GLOBALS['send_mail_result']=$mail->errors;
		error("<pre>send_mail Error:\n errors:".print_r($GLOBALS['send_mail_result'],1)."\n result=$result\n V=$V\n method=$method\n to=$to\n TO=".print_r($TO,1)." subject=$subject\n from=$from\n</pre>message=$message");
	}else {
		//error("<pre>send_mail Sent OK:\n result=$result\n V=$V\n method=$method\n to=$to\n TO=".print_r($TO,1)." subject=$subject\n from=$from\n</pre>message=$message");
	}
 return $result;
}

?>