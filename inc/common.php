<?
function mem_set($key, $val=0, $ttl=0) {if(function_exists('xcache_set')) {
	// echo "mem_set<div dir=ltr align=left><pre>".print_r($GLOBALS['CFG']['mem_prefix'].$key,1)."</pre></div>";
	// echo "mem_set<div dir=ltr align=left><pre>".print_r($val,1)."</pre></div>";
	return xcache_set($GLOBALS['CFG']['mem_prefix'].$key, $val, $ttl);}}
function mem_inc($key, $val=0, $ttl=0) {if(function_exists('xcache_inc')) return xcache_inc($GLOBALS['CFG']['mem_prefix'].$key, $val, $ttl);}
function mem_dec($key, $val=0, $ttl=0) {if(function_exists('xcache_dec')) return xcache_dec($GLOBALS['CFG']['mem_prefix'].$key, $val, $ttl);}
function mem_get($key)   {if(function_exists('xcache_get')) return xcache_get($GLOBALS['CFG']['mem_prefix'].$key);}
function mem_isset($key) {if(function_exists('xcache_isset')) return xcache_isset($GLOBALS['CFG']['mem_prefix'].$key);}
function mem_unset($key) {if(function_exists('xcache_unset')) return xcache_unset($GLOBALS['CFG']['mem_prefix'].$key);}
function mem_unset_by_prefix($key){if(function_exists('xcache_unset_by_prefix')) return xcache_unset_by_prefix($GLOBALS['CFG']['mem_prefix'].$key);}

//————————————————————————————————————————————————————————————————————————————————————
function sql2array($sql, $primary='', $single_col='', $single_row=false, $db='') {
	$result=runsql($sql, $db);
	
	
	
	if ($result===false) return false;
	if (!$result OR !mysql_num_rows($result)) return;

	if ($primary===1) { $primary=''; $fetch_mode=MYSQL_NUM; }
	else $fetch_mode=MYSQL_ASSOC;
	
	while($row=mysql_fetch_array($result, $fetch_mode) ) {
		if ($primary) $rows[$row[$primary]] = $single_col ? $row[$single_col] : $row;
		else $rows[] = $single_col ? $row[$single_col] : $row;
		if ($single_row) { $rows=$rows[0]; break; }
	}
	mysql_free_result($result);
 return $rows;
}

//————————————————————————————————————————————————————————————————————————————————————
function runsql($sql, $db='') {
 global $CFG, $SVARS;
	if (!$sql) { error("runsql: sql is empty"); return; }
	if ($GLOBALS['DB_connect_error']) return;
	elseif (!$GLOBALS['DB_connected']) MySqlConnect();
	
	if (!$db) {
		if ($SVARS['cid']) $db="Rafael";
		else error("No DB selected");
	}
	
	if ( $GLOBALS['DB_last_query_time']-time()>60 ) {	# check connection
		if (!mysql_ping()) {
			MySqlDisconnect();
			MySqlConnect();
		}
	}

	if ($GLOBALS['CFG']['debug']>1) $queryStartTime=array_sum(explode(' ',microtime()));
	
	$result=@mysql_db_query($db, $sql);
	
	$GLOBALS['DB_last_query_time']=time();

	if ($GLOBALS['CFG']['debug']>1) {
		$queryTime=number_format((array_sum(explode(" ",microtime())) - $queryStartTime),6);
		$GLOBALS['DEBUG'][]="SQL ($queryTime sec): $sql";
		//if ($queryTime>3) error("SQL ($queryTime sec): $sql", 1);
		# log all queries
		// error_log("SQL ($queryTime sec): $sql\r\n", 3, "sql.log");
	}
	
	if (!$result) { error("runsql Error Query: <b>'$sql'</b><br>".mysql_errno().": ".mysql_error()); return false; }

 return $result;
}

//————————————————————————————————————————————————————————————————————————————————————
function MySqlConnect() {	# Connect to MySQL Server
 global $CFG, $mysql_connection;
	if ($GLOBALS['DB_connected']) return true;

	if ($GLOBALS['CFG']['debug']>1) $queryStartTime=array_sum(explode(' ',microtime()));
	
	# Connect to server
	$mysql_connection=@mysql_connect($CFG['dbHost'], $CFG['dbUser'], $CFG['dbPass']);
	if (!$mysql_connection) {
		error("connect_to_mysql: ".mysql_errno().": ".mysql_error());
		$GLOBALS['DB_connect_error']=true;
		return;
	}
	
	mysql_set_charset('utf8');

	// mysql_query("SET NAMES 'hebrew'");
	// mysql_query("SET CHARACTER SET utf8");

	// if ($GLOBALS['CFG']['debug']>1) {
		// $queryTime=round((array_sum(explode(" ",microtime())) - $queryStartTime),6);
		// $GLOBALS['DEBUG'][]="mysql_connect ($queryTime sec)";
	// }
	
	$GLOBALS['DB_connected']=true;
	// $GLOBALS['DB_last_query_time']=time();
 return true;
}

//————————————————————————————————————————————————————————————————————————————————————
function MySqlDisconnect() {	# Connect to MySQL Server
 global $CFG, $mysql_connection;
	mysql_close();
	$mysql_connection=false;
	$GLOBALS['DB_connected']=false;
}

//————————————————————————————————————————————————————————————————————————————————————
function exec_SQL_file($file_SQL_name){
 global $CFG;
	if (!is_file($file_SQL_name)) die('SQL file not exist');
	exec("$CFG[mysqlExe] -h $CFG[dbHost] -uroot".($CFG['dbPass']?" -p$CFG[dbPass]":'')." $CFG[dbName] < ".realpath($file_SQL_name), $output);
 return $output;
}

//————————————————————————————————————————————————————————————————————————————————————
function sql_escape($str) {
 global $CFG, $mysql_connection;
 	if (!$str) return $str;
	if ($mysql_connection) $str=mysql_real_escape_string($str);
	else $str=mysql_escape_string($str);
 return $str;
}
/*if (!function_exists('sql_escape')) {
	function sql_escape($str) {
		$search = array ('\\',   "'",   "\n", "\r", '"',  "\x00", "\x1a");
		$replace= array ('\\\\', "\\'", '\n', '\r', '\"', '\0',   '\Z');
		return str_replace($search, $replace, $str);
	}
}*/
//————————————————————————————————————————————————————————————————————————————————————
# same as sql_escape but adds '' if needed
function quote($str, $quotesNull=false) {
 global $CFG, $mysql_connection;
	if (is_null($quotesNull) AND is_null($str)) return 'NULL';
	elseif($str==='' OR is_null($str)) return "''";
	if ("$str"==='0' OR ereg('^[1-9][0-9]*$',$str)) return ($quotesNull ? "'$str'" : $str);	# number
	if ($mysql_connection) $str=mysql_real_escape_string($str);
	else $str=mysql_escape_string($str);
	return "'$str'";
}


//————————————————————————————————————————————————————————————————————————————————————
# convert date in formats d/m/[y/Y][ H:i[:s]] or Y-m-d[ H:i[:s]] to unix timestamp
function str2time($str, $format='') {
	if ( ereg("0000-00-00( (00:00)(:00)?)?", $str) ) 
		$timestamp=0;
	elseif ( ereg("(0{1,2})[./]?(0{1,2})[./]?(0{2,4})( (00:00)(:00)?)?", $str) ) 
		$timestamp=0;
	elseif ( ereg("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})( ([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?)?", $str, $r) ) 
		$timestamp=mktime($r[5], $r[6], $r[8], $r[2], $r[3], $r[1]);
	elseif ( ereg("([0-9]{1,2})[./]?([0-9]{1,2})[./]?([0-9]{2,4})( ([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?)?", $str, $r) ) {
		if (strlen($r[3])==2) $r[3]="20$r[3]";
		$timestamp=mktime($r[5], $r[6], $r[8], $r[2], $r[1], $r[3]);
	}
	
	if ($format) {
		if (!$timestamp) return '';
		else return date($format, $timestamp);
	}else return $timestamp;
}


//————————————————————————————————————————————————————————————————————————————————————
function error($error, $quiet=0) {
	//if ($GLOBALS['CFG']['debug'] AND !$quiet) echo "<div align=left dir=ltr>$error</div>";
	//$GLOBALS['errors'][]=$error;	
	$backtrace=debug_backtrace();
	//if (in_array($backtrace[1]['function'], array('write2file','runsql','htmlspecialchars','mysql_num_rows','','',''))) {}
	$error.="\r\n";
	$error.="	-> URL: http".($_SERVER['HTTPS']?'s':'')."://$_SERVER[HTTP_HOST]".($_SERVER['SERVER_PORT']!=80?":$_SERVER[SERVER_PORT]":'')."$_SERVER[REQUEST_URI]\r\n";
	if ($_POST) $error.="	-> POST:".preg_replace(array("|\n *|", '| => |'), array(' ','=>'), var_export($_POST,1))."\r\n";
	for($i=2; $i<=count($backtrace)-1; $i++) {
		$d=$backtrace[$i];
		$args='';
		// if ($d['args']) foreach($d['args'] as $var) $args.=($args?', ':'').preg_replace(array("|\n *|", '| => |'), array(' ','=>'), var_export($var,1));
		// if ($d['args']) foreach($d['args'] as $var) $args.=($args?', ':'').preg_replace(array("|\n *|", '| => |'), array(' ','=>'), print_r($var,1));
		// $error.="	-> $d[file]($d[line]): $d[function]($args)\r\n";
	}
	error_log(date('Y-m-d H:i:s')." | $error\r\n", 3, "errors.log");		# Write to log file
	if ($GLOBALS['CFG']['show_errors'] AND !$quiet) echo "<div align=left dir=ltr>$error</div>";
}

//————————————————————————————————————————————————————————————————————————————————————
function php_error_handler($code, $str, $file, $line) {
	if ($code==E_NOTICE OR $code==E_USER_NOTICE) return;
	elseif ($code==E_ERROR OR $code==E_USER_ERROR OR $code==E_COMPILE_ERROR) $errtype='Error';
	elseif ($code==E_WARNING OR $code==E_USER_WARNING OR $code==E_COMPILE_WARNING) $errtype='Warning';
	elseif ($code==E_PARSE) $errtype='Parse Error';
	error("<b>$errtype:</b> $str in <b>$file</b> on line <b>$line</b>", 0);
}

//————————————————————————————————————————————————————————————————————————————————————
function text2html($v0=null,$v1=null,$v2=null,$v3=null,$v4=null,$v5=null,$v6=null,$v7=null,$v8=null,$v9=null,$v10=null,$v11=null,$v12=null,$v13=null,$v14=null,$v15=null,$v16=null,$v17=null,$v18=null,$v19=null,$v20=null,$v21=null,$v22=null,$v23=null,$v24=null,$v25=null,$v26=null,$v27=null,$v28=null,$v29=null,$v30=null) {
	$num_args=func_num_args();
	for($i=0; $i<func_num_args(); $i++) {
		if (!is_null(${"v$i"})) {
			$text=&${"v$i"};
			$text=htmlspecialchars($text, ENT_QUOTES);
			$text=eregi_replace("  ", "&nbsp; ", $text);
			$text=eregi_replace("\r\n|\n", "<br>", $text);		
		}
	}
 if ($num_args==1) return $text;
}
//————————————————————————————————————————————————————————————————————————————————————
function html2text($text) {
	$text=strip_tags($text);
	$text=str_replace(array('&nbsp;', '&lt;', '&gt;', '&quot;', '&#039;', '&#032;', '&#034;'), array(' ', '<', '>', '"', "'"," ", '"'), $text);
 return $text;
}

//————————————————————————————————————————————————————————————————————————————————————
# array2file(array $array, str $ar_name, str [$filename])
function array2file1($array, $ar_name, $filename='') {
	if ("$ar_name"=='' OR !is_array($array)) return;
	if ("$filename"=='') $filename="$ar_name.php";
	$file_data = "<?\n\$$ar_name=". var_export($array, true) .";\n?".'>';
 return write2file($filename, $file_data, 'wb');
}

//————————————————————————————————————————————————————————————————————————————————————
function array2file($array, $ar_name, $filename='') {
	if ("$ar_name"=='' OR !is_array($array)) return;
	if ("$filename"=='') $filename="$ar_name.php";

	$file_data="<?\n";
	while($key=each($array)) {
		if (is_array($key[1])) {
			$file_data.="\${$ar_name}['$key[0]']=".var_export($key[1], true).";\n";
		}else {
			$key[1]=str_replace("\\", "\\\\", $key[1]);
			$key[1]=str_replace("'", "\'", $key[1]);
			$file_data.="\$$ar_name".(is_int($key[0])?"[$key[0]]":"['$key[0]']")." = ".(is_int($key[0])?"$key[1]":"'$key[1]'").";\n";
		}
	}
	$file_data.='?'.'>';
 return write2file($filename, $file_data, 'wb');
}

//————————————————————————————————————————————————————————————————————————————————————
function write2file($filename, $string, $mode='wb') {
	if (!$filename) { error("write2file() - no filename"); return; }
	if (is_file($filename) AND !is_writable($filename)) { error("write2file($filename) - file is not writable"); return; }
	
	if (!$fp=@fopen($filename, $mode)) return;			# open file
	if (!flock($fp, LOCK_EX)) { fclose($fp); return; }	# do an exclusive lock
	$is_writen=fwrite($fp, $string);					# write
	flock($fp, LOCK_UN);								# release the lock
	fclose ($fp);
	if (!$is_writen) return;
 return true;
}



//————————————————————————————————————————————————————————————————————————————————————
function write2file_d($filename, $string, $mode='wb', $timeout=10) {	# write to file delayed (wait if file is busy)
	while (!$is_writen=write2file($filename, $string, $mode) AND $timeout>0) { sleep(1); $timeout--; }
	if (!$is_writen) error("write2file_d($filename) - file write failed (after $timeout seconds)");
 return $is_writen;
}

//————————————————————————————————————————————————————————————————————————————————————
# return WHERE SQL search string
# $fields - list of db fields separated by coma or space,  mode - search mode [phrase/or/and],  $s_string - the search string.
function make_where_search($fields, $s_string, $mode='and') {
	$mode=strtolower($mode);
	$s_string=addcslashes($s_string, "%_");		# add slashes before % and _ characters
	$fieldsar=split("[, ]+", trim($fields));

	if ($mode=='phrase') {
		$sqlstr="(";
		for ($i=0; $i<count($fieldsar); $i++) {
			if ($i>0) $sqlstr.=" OR ";
			$sqlstr.="$fieldsar[$i] LIKE '%$s_string%'";
		}$sqlstr.=")";

	}else {
		$s_string=eregi_replace("[-+ ,\n\r\t]+", " ", $s_string);
		$s_stringar=split(" ", trim($s_string));

		$x=0;
		for ($n=0; $n<count($s_stringar); $n++) if (strlen(trim($s_stringar[$n]))>0) { $s_stringar2[$x]=trim($s_stringar[$n]); $x++; }
		$s_stringar=$s_stringar2;

		if ($s_stringar) {
			$sqlstr="(";
			for ($n=0; $n<count($s_stringar); $n++) {
				if ($n>0) { if ($mode=='or') $sqlstr.=" OR ("; else $sqlstr.=" AND ("; }
				for ($i=0; $i<count($fieldsar); $i++) {
					if ($i>0) $sqlstr.=" OR ";
					$sqlstr.="$fieldsar[$i] LIKE '%".sql_escape($s_stringar[$n])."%'";
				}$sqlstr.=")";
			}
		}else return false;
	}#if
 return $sqlstr;
}#make_where_search()


//————————————————————————————————————————————————————————————————————————————————————
function send_headers() {	# send no-cache headers
	if (headers_sent()) return;
	// header("Content-Type: text/html;charset=windows-1255");
	header("Content-Type: text/html;charset=utf-8");
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");				// Date in the past
	header ("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");		// always modified
	if (eregi("MSIE", $_SERVER['HTTP_USER_AGENT'])) {
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
	}else {
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
	}
}


//————————————————————————————————————————————————————————————————————————————————————
function remove_magic_quotes() {
	if (get_magic_quotes_gpc()) {
		remove_quotes($_GET);
		remove_quotes($_POST);
		remove_quotes($_COOKIE);
		remove_quotes($_REQUEST);
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function remove_quotes(&$ar) {
	foreach($ar as $key=>$value) {
		if(is_array($value)) remove_quotes($ar[$key]);
		else $ar[$key]=stripslashes($value);
	}
}


//————————————————————————————————————————————————————————————————————————————————————
function LoadDir($dir) {
	if (!is_dir($dir)) return;
	if (!$dirhandle=@opendir($dir)) return;
	while (false!==($file=readdir($dirhandle))) {
		if ($file!="." && $file!="..") $dir_ar[]=$file;
	}
	closedir($dirhandle);
 return $dir_ar;
}


//————————————————————————————————————————————————————————————————————————————————————
function clear_html($html) {
 global $CFG;
	$html=trim($html);
	if ($html=='<P>&nbsp;</P>') $html='';
	#replace some tags
	$search=array("#<STRONG>#","#</STRONG>#","#<EM>#","#</EM>#");
	$replace=array("<B>","</B>","<I>","</I>");
	$html=preg_replace($search, $replace, $html);

	# Clear non releative path
	$replace_str="https?://$_SERVER[HTTP_HOST](:[0-9]+)?$CFG[WEB]/";
	$html=eregi_replace($replace_str, "", $html);
	# Clear tags
	$html=eregi_replace("</?(body|title|head|meta)[^>]*>", "", $html);
 return $html;
}


//————————————————————————————————————————————————————————————————————————————————————
function FormatDate($date_timestamp, $year=0) {
	if (!$date_timestamp) return;
	//$RusMonths=array('', '?םגאנ?', 'װוגנאכ?', 'ּאנעא', 'ְןנוכ?', 'ּא?', 'ָ‏ם?', 'ָ‏כ?', 'ְגדףסעא', 'ׁוםע?בנ?', '־ךע?בנ?', 'ֽמ?בנ?', 'ִוךאבנ?');
	//return date('j',$date_timestamp) .' '. $RusMonths[date('n',$date_timestamp)] . ($year ? ' '.date('Y',$date_timestamp) :'');
	return date('d/m/Y',$date_timestamp);
}

//————————————————————————————————————————————————————————————————————————————————————
function trace($var, $name='') {
	echo "<div dir=ltr align=left><pre>".($name?"$name=":''); print_r($var); echo "</pre></div>";
}

//————————————————————————————————————————————————————————————————————————————————————
function flipheb($text) {
	if (function_exists('fribidi_log2vis')) return fribidi_log2vis($text, FRIBIDI_LTR, FRIBIDI_CHARSET_CP1255); //{ error("flipheb: FriBiDi not insalled."); return; }
	else return hebrev($text);
}


//————————————————————————————————————————————————————————————————————————————————————
function check_login() {
 global $CFG, $SVARS, $m;
	$SVARS=false;
	if ($_GET['SES'] && ($m=='common/files' OR $m=='common/get')) $_COOKIE['SES']=$_GET['SES'];
	// echo "\$m:$m"; 
	// echo "<div dir=ltr align=left><pre>".print_r($_COOKIE,1)."</pre></div>";exit;
	if (!$_COOKIE['SES']) return;
	$ses=explode('|', $_COOKIE['SES']);
	
	// echo "<div dir=ltr align=left><pre>".print_r($ses,1)."</pre></div>";
	$company_id	= (int)base_convert($ses[0],36,10);	
	$user_id	= (int)base_convert($ses[1],36,10);
	# $ses[2] - ip (long base 32)
	// echo "\$company_id=$company_id; \$user_id=$user_id";  exit;
	if (!$company_id OR !$user_id OR !$ses[2] OR $ses[2]!=base_convert(ip2long($_SERVER['REMOTE_ADDR']),10,36) ) return;

	// echo "\$company_id=$company_id; \$user_id=$user_id";

	if ($SVARS=mem_get("SV$company_id/$user_id")) {	# record in cache
		// echo "<div dir=ltr align=left><pre>".print_r($SVARS,1)."</pre></div>";
		// echo "<div dir=ltr align=left><pre>".print_r($ses,1)."</pre></div>";
		// if ($SVARS['cookie']!=$ses[3]) {$SVARS=false; return; }	# check cookie
		// else return 1;
	}
	
	$company=sql2array("SELECT * FROM companies WHERE id=$company_id", 0, 0, 1, 'Rafael');
	// print_ar($CFG,'$CFG');
	// print_ar($company,'$company');
	// echo "/*<div dir=ltr align=left><pre>".print_r($company,1)."</pre></div>*/"; exit;
	if (!$company) return;
	
	$SVARS['cid']=$company_id;
	
	$mail_account = sql2array("SELECT * FROM mail_accounts WHERE user_id = $user_id",0,0,1,'Rafael');
	
	// echo "SELECT * FROM users WHERE id='$user_id' AND ip='$_SERVER[REMOTE_ADDR]' AND cookie=".quote($_COOKIE['SES']);
	$user=sql2array("SELECT * FROM users WHERE id='$user_id' AND ip='$_SERVER[REMOTE_ADDR]' AND cookie=".quote($_COOKIE['SES']), 0, 0, 1,'Rafael');
	// print_ar($user,'$user');
	// print_ar($SVARS,'$SVARS');
	if (!$user) return;
	
	// echo "<div dir=ltr align=left><pre>".print_r($SVARS,1)."</pre></div>";

	$last_enter=str2time($user['last_enter']);	# convert to timestamp
	if ( strtotime("+3 days", $last_enter) < time() ) return;		# timeout
	elseif ( strtotime("+30 minute", $last_enter) < time() ) {	# update last_enter.
		runsql("UPDATE LOW_PRIORITY users SET last_enter='".date('Y-m-d H:i:s')."' WHERE id='$user[id]'");
	}
	
	$SVARS['cookie']=$ses[3];
	
	$SVARS['company'] = array(
		'id'=>$company['id'],
		
		'name'=>$company['name'],
		'vat_period'=>$company['vat_period'],
		'current_vat_period'=>$company['current_vat_period']
	);

	$SVARS['user'] = array(
		'id'		=> $user['id'],
		'username'	=> $user['username'],
		'name'		=> trim("$user[lname] $user[fname]"),
		'sms_active'=> $user['sms_active'],
		'fax_active'=> $user['fax_active'],
		'fax_in'	=> $user['fax_in'],
		'groups'	=> $user['groups'],
		'email' 	=> $mail_account['email']
 	);
	if ($tel=ereg_replace('[^0-9]+','', $user['tel']) AND ereg('^(0|972)(5[0247][0-9]{7})$', $tel, $regs)) $SVARS['user']['sms_originator']='0'.$regs[2];

	// if ($_COOKIE['switch_user']) {
		// if ($user=sql2array("SELECT * FROM $CFG[prefix]users WHERE user_id='".sql_escape($_COOKIE['switch_user'])."'", 0, 0, 1)) {
			// $SVARS['origin_user_id']  	 = $SVARS['user_id'];
			// $SVARS['origin_username'] 	 = $SVARS['username'];
			// $SVARS['user_id']	  = $user['user_id'];
			// $SVARS['parent_id']	  = $user['parent_id'];
			// $SVARS['username']	  = $user['username'];
			// $SVARS['user_type']	  = $user['type'];
			// $SVARS['fname']		  = $user['fname'];
			// $SVARS['fullname']	  = trim("$user[lname] $user[fname]");
			// $SVARS['sms_active']  = $user['sms_active'];
			// if ($user['sms_credits']) $SVARS['sms_credits'] = $user['sms_credits'];
			// if ($tel=ereg_replace('[^0-9]+','', $user['tel']) AND ereg('^(0|972)(5[0247][0-9]{7})$', $tel, $regs)) $SVARS['sms_originator']='0'.$regs[2];
		// }
	// }
	// echo "/*<div dir=ltr align=left><pre>".print_r($SVARS,1)."</pre></div>*/";
	mem_set("SV$company_id/$user_id", $SVARS, 3600);
	// echo "<div dir=ltr align=left><pre>".print_r($SVARS,1)."</pre></div>";
	// echo "<div dir=ltr align=left><pre>".print_r(mem_get("SV$user_id"),1)."</pre></div>"; exit;
 return 1;
}


//—————————————————————————————————————————————————————————————————————————————————————————————
function num($num, $decimal=0) {
	$num=number_format($num, $decimal);
	if (!$num) $num='';
 return $num;
}

//—————————————————————————————————————————————————————————————————————————————————————————————
function price($num, $decimal=0) {
	$num=number_format($num, $decimal);
	if (!$num) $num=''; else $num.=' ₪';
 return $num;
}


//—————————————————————————————————————————————————————————————————————————————————————————————
function jsEscape($str) { 
	return str_replace(array("\\","\r","\n","'",chr(0x0c), chr(0x09), chr(0x08)), array('\\\\','\r','\n',"\\'",'\f', '\t', '\b'), $str);
}


//————————————————————————————————————————————————————————————————————————————————————
function price_format($num, $nocursign=false) {
 global $CFG;
	if ($num!=='') $num = ( $num!=sprintf("%d",$num) ? number_format($num,2) : number_format($num));
	if (!$nocursign) $num=$CFG['cursign'].$num;
	return $num;
}

//————————————————————————————————————————————————————————————————————————————————————
# Delete files using wildcards (*.jpg)
function delfile($str) {
	foreach(glob($str) as $fn) if(is_file($fn)) unlink($fn);
}


//————————————————————————————————————————————————————————————————————————————————————
function print_ar($v, $name='') {
	echo "<div dir=ltr align=left style='border:1px dashed silver; margin:2px 0; font-size:11px'><code>".($name?"<font color=#008040><b>".htmlspecialchars($name)."</b></font> = ":'').dump_var($v).";</code></div>";
}
function dump_var($v) {
	$is_object=is_object($v);
	if ($is_object OR is_array($v)) {
		if ($is_object OR count($v)) {
			foreach($v as $key=>$val) $properties[]= dump_var($key)." => ".dump_var($val);
			return "<font color=blue>".($is_object?'Object':'Array')."</font>(<blockquote style='margin:0 0 0 25px; font-family:Courier New;'>".implode(",<br>\r\n",$properties)."\r\n</blockquote>)";
		}else return "<font color=blue>".($is_object?'Object':'Array')."</font>()";
	}
	elseif(is_numeric($v))	return "<font color=blue>$v</font>";
	elseif(is_bool($v))		return "<font color=blue>".($v?'true':'false')."</font>";
	elseif($v===NULL) 		return "<font color=blue>null</font>";
	else 					return "`<font dir=ltr color=#fd00fd style='word-wrap:break-word; white-space:-moz-pre-wrap;'>".htmlspecialchars($v)."</font>`";
}


//————————————————————————————————————————————————————————————————————————————————————
function array2json($var) {
	if (is_array($var) OR is_object($var)) {
		if ( is_object($var) OR (count($var) && (array_keys($var) !== range(0, count($var)-1))) ) {
			foreach($var as $key=>$val) $properties[] = "'$key':".array2json($val);	//(eregi('^[0-9a-z_]$',$key) ? $key : "'$key'").':'.array2json($val);
			return '{'. join(',',$properties) .'}';
		}else {
			$elements=array_map('array2json', $var);
			return '['. join(',',$elements) .']';
		}
	}
	elseif(is_bool($var))	return ($var ? 'true' : 'false');
	elseif("$var"==="0" OR ereg('^[1-9][0-9]*$',$var)) return $var;
	elseif($var===NULL) 	return "null";
	else 					return "'".str_replace(array("\\","\r","\n","'",chr(0x0c), chr(0x09), chr(0x08)), array('\\\\','\r','\n',"\\'",'\f', '\t', '\b'), $var)."'";
}


/*

//—————————————————————————————————————————————————————————————————————————————————————————————
function backgroundRequest($url, $post='') {
	$u=parse_url($url);
	if (!$u['host']) $u['host']=$_SERVER['HTTP_HOST'];
	if (!$u['port']) $u['port']=80;
	if (!$fp=fsockopen($u['host'], $u['port'], $errno, $errstr, 10)) return false;
	$request = ($post?'POST':'GET')." $u[path]?$u[query] HTTP/1.1\r\nHost: $u[host]\r\n".($post ? "Content-Type: application/x-www-form-urlencoded\r\nContent-Length: ".strlen($post)."\r\nConnection: Close\r\n\r\n$post" : "\r\n");
	//error("\r\nbackgroundRequest: $request\r\n");
	$sent=fwrite($fp, $request);
	fclose($fp);
 return $sent;
}

//————————————————————————————————————————————————————————————————————————————————————
function check_client_login() {
 global $CFG, $cSVARS;
 	$cSVARS=false;
	if (!$_COOKIE['cSES']) return;
	$row=sql2array("SELECT *, UNIX_TIMESTAMP(last_enter) AS last_enter FROM crm_clients WHERE cookie='".sql_escape($_COOKIE['cSES'])."' AND ip='".ip2long(env('REMOTE_ADDR'))."'");
	if (count($row)!=1) return;

	$row=$row[0];
	
	if ( ($row['last_enter'] - strtotime("-600 minute")) <=0 ) {		# timeout
		$SVARS=array('timeout'=>1);
		return;
	}elseif ( ($row['last_enter'] - strtotime("-10 minute")) <=0 ) {	# update last_enter.
		runsql("UPDATE $CFG[prefix]crm_clients SET last_enter='".date('Y-m-d H:i:s')."' WHERE client_id='$row[client_id]'");
	}
	$cSVARS['client_id']	= $row['client_id'];
	$cSVARS['agent_id'] 	= $row['group_id'];
	$cSVARS['client_name'] = trim($row['first_name']." ".$row['last_name']);
 return 1;
}

//————————————————————————————————————————————————————————————————————————————————————
function update_svars() {
 global $SVARS;
	if (!is_array($SVARS)) return;
	unset($SVARS['sid']);	# remove session id
	runsql("UPDATE $CFG[prefix]user_sessions SET time='".time()."', svars='".serialize($SVARS)."' WHERE cookie='".sql_escape($_COOKIE['SES'])."'");
}

//————— Ajax responce ————————————————————————————————————————————————————————————————————
# syntax: Ajr(target id/js/alert, [value], [attribute(ha/hp/attribute)])
# $atr - default=innerHTML, ha=append, hp=prepend, any other=set html attribute (ex: 'style.border')
function Ajr($target, $value='', $atr='h') {
	if ($target=='alert') { $target='js'; $value="alert('".jsEscape($value)."')"; }
	if (!$value) $value="''"; elseif (!eregi("^[0-9a-z_\.]*$", $value)) $value='"'.str_replace('"', '&quot;', $value).'"';
	if ($target=='js' AND $value!="''") return "<x js=$value>\n";	// javascript
	elseif ($atr=='h' OR $atr=='ha' OR $atr=='hp') return "<x id=$target $atr=$value>\n"; // set/append/prepend innerHTML
	else {		// set atribute
		if (!eregi("^[0-9a-z_\.]*$", $atr)) $atr='"'.str_replace('"', '&quot;', $atr).'"';
		return "<x id=$target p=$atr v=$value>\n";
	}
}


function Ajr1($target, $value='', $atr='h') {
	if ($target=='alert') { $value="alert('".str_replace(array("\r\n","\n","\r","'"), array('\n','\n','\n',"\\'"), $value)."')"; }
	$value='"'.str_replace('"', '&quot;', $value).'"';

	$value=str_replace(array("\r\n","\n","\r","'"), array('\n','\n','\n',"\\'"), $value);
	
	if ($atr=='h') return "$I('$target','$value');\n";
	// OR $atr=='ha' OR $atr=='hp') return "<x id=$target $atr=$value>\n"; // set/append/prepend innerHTML
	
}
*/

/*
### used in shop ###
//————————————————————————————————————————————————————————————————————————————————————
function str2addons($str){
	if (!$str) return;
	$str1=explode(',',$str);
	if ($str1) foreach($str1 as $row) {
		list($k,$v)=explode(':',$row);
		$addons[$k]=$v;
	}
 return $addons;
}
//————————————————————————————————————————————————————————————————————————————————————
function addons2str($var) {
	if (is_array($var)) {
		foreach($var as $key=>$val) $properties[] =$key.':'.$val;
		return '{'. join(',',$properties) .'}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function urlenc($str) {
	$str=urlencode($str);
	$str=str_replace('%2F','/',$str);
	//$str=str_replace('%3A',':',$str);
 return $str;
}
*/
?>