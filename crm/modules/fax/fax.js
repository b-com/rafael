
var filesFolders=['כללי'];
if (typeof(fax_module_container)=='undefined') fax_module_container='';

// SearchField
Ext.app.SearchField = Ext.extend(Ext.form.TwinTriggerField, {
	initComponent: function(){
		if(!this.store.baseParams) this.store.baseParams = {};
		Ext.app.SearchField.superclass.initComponent.call(this);
		//this.on('specialkey', function(f, e){if(e.getKey()==e.ENTER) this.onTrigger2Click();}, this);
		this.on('keyup', this.keySearch, this);
		this.keyUpTask = new Ext.util.DelayedTask(this.onTrigger2Click, this);
	},

	enableKeyEvents:true,
	validationEvent:false,
	validateOnBlur:false,
	trigger1Class:'x-form-clear-trigger',
	trigger2Class:'x-form-search-trigger',
	hideTrigger1:true,
	width:180,
	hasSearch: false,
	paramName: 'q',

	keySearch: function(f,e){
		if (e.keyCode<48 && e.keyCode!=8) return;	// not word char or backspace
		else if (e.keyCode==13) { this.keyUpTask.cancel(); this.onTrigger2Click(); return; } // enter
		this.keyUpTask.delay(700);
	},
	
	onTrigger1Click: function(){
		if(this.hasSearch){
			this.store.baseParams[this.paramName] = '';
			this.store.removeAll();
			this.el.dom.value = '';
			this.triggers[0].hide();
			this.hasSearch = false;
			this.store.reload();
			this.focus();
		}
	},

	onTrigger2Click: function(){
		var v=this.getRawValue().trim();
		if(v.length<1){
			this.onTrigger1Click();
			return;
		}
		if(v.length<2){
			//Ext.Msg.alert('Invalid Search', 'You must enter a minimum of 2 characters to search the API');
			return;
		}
		this.store.removeAll();
		this.store.baseParams[this.paramName] = v;
		var o = {start:0};
		this.store.reload({params:o});
		this.hasSearch = true;
		this.triggers[0].show();
		this.focus();
	}
});

FaxesGrid = function(db_cols) {
	this.type='in';

	this.store = new Ext.data.GroupingStore({
		proxy: new Ext.data.HttpProxy({url:'?m=common/fax&f=fax_list', method:'GET'}),
		reader: new Ext.data.JsonReader({ root:'data', totalProperty:'total'}, ['id','type','date','to','fax','error','readed','status','text','tupal','metapel']),
		sortInfo:{field:'date', direction:'DESC'},
		remoteSort:true,
		groupField:'to'
	});
	
	//this.searchField = new Ext.app.SearchField({store:this.store, width:100});
	
	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var type = this.type;  if (Ext.getCmp('b_deleted') && Ext.getCmp('b_deleted').pressed) type='deleted';
		var setParams={type:type, metapel:this.metapelCombo.getValue() }; // ,q: this.searchField.getRawValue().trim()
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);
	
	this.store.on('load', function(){	// check for errors
		var callback=function() { this.store.load(); };
		ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		//Dump(this.store.reader.jsonData.deleted);
		this.delCount=this.store.reader.jsonData.deleted;
		this.updateDelCount();
		//if (this.store.reader.jsonData.deleted) Ext.getCmp('faxMenu').getTopToolbar().items.get('b_deleted').setText('מחוקים ('+this.store.reader.jsonData.deleted+')');
	}, this);
	
	this.delCount=0;
	this.updateDelCount = function(){
		Ext.getCmp('faxMenu').getTopToolbar().items.get('b_deleted').setText((this.delCount?'<font dir=ltr style="font-size:10px">('+this.delCount+')</font>':'')+' מחוקים');
	}

	
	this.linkWin = function(clientType){
		var win=new LinkWin(this,clientType);
		win.show();
	};

	this.metapelCombo = new Ext.form.ComboBox({store:users, hiddenName:'metapel_combo', width:120, listClass:'rtl', triggerAction:'all', forceSelection:true, 
		listeners:{
			select:function(f){ 
				this.store.reload();
			},
			scope:this
		}});
	
	this.faxRenderer = function(str, meta, record){
		meta.attr=	'ext:qtip="פקס: <b>'+Ext.util.Format.htmlEncode(record.data.fax)+'</b>'
				+'<br>תאריך: <b>'+Ext.util.Format.htmlEncode(record.data.date)+'</b>'
				+(record.data.metapel?'<br>לטיפול של: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.metapel].name)+'</b>':'')
				+((record.data.tupal && users_obj[record.data.tupal])?'<br>טיפל: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.tupal].name)+'</b>':'<br>טיפל: <b>לא טופל</b>')			
				+'"';

		var d=record.data;
		var text='';
		var pic='fax-in';
		if (d.type=='out') {
			if (d.status==1) var pic='fax-wait';
			else if (d.status==3) var pic='fax-failed';
			else var pic='fax-out'; // d.status==2
		}
		return "<img src='skin/icons/"+pic+".gif' align=absmiddle>"+str+text+' ';
	};
	
	this.dateRenderer = function(str, meta, record){
		meta.attr=	'ext:qtip="פקס: <b>'+Ext.util.Format.htmlEncode(record.data.fax)+'</b>'
				+'<br>תאריך: <b>'+Ext.util.Format.htmlEncode(record.data.date)+'</b>'
				+(record.data.metapel?'<br>לטיפול של: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.metapel].name)+'</b>':'')
				+((record.data.tupal && users_obj[record.data.tupal])?'<br>טיפל: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.tupal].name)+'</b>':'<br>טיפל: <b>לא טופל</b>')			
				+'"';
				// ext:qtitle="פקס: '+Ext.util.Format.htmlEncode(record.data.fax)+'"';

		return str;
	};
	
	this.metapelRenderer = function(str, meta, record){
		meta.attr=	'ext:qtip="פקס: <b>'+Ext.util.Format.htmlEncode(record.data.fax)+'</b>'
				+'<br>תאריך: <b>'+Ext.util.Format.htmlEncode(record.data.date)+'</b>'
				+(record.data.metapel?'<br>לטיפול של: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.metapel].name)+'</b>':'')
				+((record.data.tupal && users_obj[record.data.tupal])?'<br>טיפל: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.tupal].name)+'</b>':'<br>טיפל: <b>לא טופל</b>')			
				+'"';

		return (users_obj[str]?users_obj[str].name:'');
	};
	
	this.tupalRenderer = function(str, meta, record){
		meta.attr=	'ext:qtip="פקס: <b>'+Ext.util.Format.htmlEncode(record.data.fax)+'</b>'
				+'<br>תאריך: <b>'+Ext.util.Format.htmlEncode(record.data.date)+'</b>'
				+(record.data.metapel?'<br>לטיפול של: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.metapel].name)+'</b>':'')
				+((record.data.tupal && users_obj[record.data.tupal])?'<br>טיפל: <b>'+Ext.util.Format.htmlEncode(users_obj[record.data.tupal].name)+'</b>':'<br>טיפל: <b>לא טופל</b>')			
				+'"';
		
		return (users_obj[str]?users_obj[str].name:'');
	};
	
	
	
	
	// הגדרת עמודות: cols==default columns cfg, db_cols==database columns cfg, this.cols==shown columns
	this.cols = [];
	
	var cols = {
		 tupal:		{dataIndex:'tupal', id:'tupal', header:'טופל ע"י', width:75, sortable:true, renderer:this.tupalRenderer.createDelegate(this)}
		,metapel: 	{dataIndex:'metapel', id:'metapel', header:'לטיפול ע"י', width:75, sortable:true, renderer:this.metapelRenderer}
		,date:		{dataIndex:'date', id:'date', header:'תאריך', width:80, sortable:true, css:'direction:ltr;text-align:center;', renderer:this.dateRenderer.createDelegate(this)}
		,fax: 		{dataIndex:'fax', id:'fax', header:'פקס', sortable:true, css:'direction:rtl;text-align:right;', renderer:this.faxRenderer.createDelegate(this) }
		,to: 		{dataIndex:'to', id:'to', header:"מס'", hidden:true }
	};

	if (db_cols) {
		for (var c=0; c<db_cols.length; c++) {		
			ci=db_cols[c].dataIndex;
			cols[ci].hidden=(cols[ci].hidden || db_cols[c].hidden);
			cols[ci].width=(cols[ci].width || db_cols[c].width);
			this.cols.push(cols[ci]);
		}
	}else for (var c in cols) this.cols.push(cols[c]);
	
	this.onItemCheck = function (item, checked){
		var cm = this.getColumnModel();
		
		if (item.text=='תאריך')			cm.setHidden(cm.getIndexById('date'), !checked); 
		else if (item.text=='לטיפול ע"י')		cm.setHidden(cm.getIndexById('metapel'), !checked); 
		else if (item.text=='טופל ע"י')		cm.setHidden(cm.getIndexById('tupal'), !checked); 
    };
	
	this.tbar=[
		'מטפל :', this.metapelCombo
		,{text:'הגדרת עמודות', iconCls:'cols-icon', //x-menu-item-icon 
			menu: {items: [
					{text:'תאריך', checked:!cols['date'].hidden, checkHandler:this.onItemCheck, scope:this},
					{text:'לטיפול ע"י', checked:!cols['metapel'].hidden, checkHandler:this.onItemCheck, scope:this},
					{text:'טופל ע"י', checked:!cols['tupal'].hidden, checkHandler:this.onItemCheck, scope:this}
			]}
		}
	];
	
	this.colModel= new Ext.grid.ColumnModel({
        columns: this.cols,
		listeners:{
			columnmoved:function() 	{fax_grid_columns=this.columns; setObjView({a:'set_columns', module:'clients', object:'FaxGrid', columns:Ext.encode(this.columns)})}
			,hiddenchange:function(){fax_grid_columns=this.columns; setObjView({a:'set_columns', module:'clients', object:'FaxGrid', columns:Ext.encode(this.columns)})}
			// ,widthchange:function() {alert(555);setObjView({a:'set_columns', module:'clients', object:'ContactGrid', columns:Ext.encode(this.columns)})}
		}
    });
	
	FaxesGrid.superclass.constructor.call(this, {
		id:'faxGrid', region:'east',
		split:true,	width:330, cls:'ltr',
		minColumnWidth:18, enableColLock:false, loadMask:true, stripeRows:true,	enableHdMenu:false,
		autoExpandColumn:'fax',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		view: new Ext.grid.GroupingView({
			forceFit:true,
			groupTextTpl: '<span dir=rtl>{text}</span> ({[values.rs.length]})',
			enableRowBody: true,
			getRowClass: function(record) {
				return ''+ (record.data['readed']===0 ? 'bold' :'') + (record.data.error ? ' faxerror' :'');
			}
		}),
		// tbar:['מטפל :', this.metapelCombo],
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50})
    });

	this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		Ext.getCmp('faxMenu').getTopToolbar().items.get('del_fax').setDisabled(!sm.getCount());
		Ext.getCmp('faxMenu').getTopToolbar().items.get('restore_fax').setDisabled(!sm.getCount());
		Ext.getCmp('faxMenu').getTopToolbar().items.get('link_client').setDisabled(!sm.getCount());
		Ext.getCmp('faxMenu').getTopToolbar().items.get('link_user').setDisabled(!sm.getCount());
		Ext.getCmp('faxMenu').getTopToolbar().items.get('save').setDisabled(sm.getCount()!=1);
		Ext.getCmp('faxMenu').getTopToolbar().items.get('send_mail').setDisabled(sm.getCount()!=1);
		Ext.getCmp('faxMenu').getTopToolbar().items.get('fax-send').setDisabled(sm.getCount()!=1);
	}, this);
	

	// row icons actions
	this.on('rowclick', function(grid, row, e){
		this.sRow=null;
		Ext.fly(this.view.getRow(row)).removeClass('bold');
		var data=grid.getSelectionModel().getSelected().data;
		var iframe=Ext.get('if_fax_frame');
		iframe.show();
		if (iframe.dom.src) iframe.dom.src='http://'+location.host+'/crm/?m=common/fax_get&NOHD=1&type='+data.type+'&fax_id='+data.id;
	});

	// doubleClick
	// this.on('rowdblclick', function(grid, row, e){
		// if (e.getTarget().tagName.toLowerCase()=='span' && (/b-action /).test(e.getTarget().className)) return;
		// var data = grid.getSelectionModel().getSelected().data;
		// this.openClient(data);
	// });
	
	this.on('rowcontextmenu', function(grid, row, e){
		if (this.selModel.getCount()<2) this.selModel.selectRow(row);
		var data=grid.getSelectionModel().getSelected().data;
		this.sRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				 {text:(data.tupal ? 'סמן כלא טופל':'סמן כטופל'), iconCls:(data.tupal ? 'status1':'status2'), scope:this, handler:function(){this.changeTupal(data);}}
				,{text:'...לטיפול ע"י' ,iconCls:'user', scope:this,
					menu: {items: [{xtype:'combo', hiddenName:'user_id', store:users, emptyText:'-בחר-',forceSelection:true,
						listeners:{
							select: function(combo, record, index){
								Ext.Ajax.request({
									scope:this,url:'?m=common/fax&f=set_metapel&user_id='+record.data.field1+'&fax_id='+data.id+'&type='+data.type,
									success: function(r,o){
										r=Ext.decode(r.responseText);
										Ext.getCmp('faxGrid').store.reload();
									}
									,failure:function(r){}
								});
							}
						}
					}]}
				}
				,{text:'...שייך ל', iconCls:'link_add',scope:this, menu:{items:[
					{text:'שייך ללקוח', iconCls:'user-link', scope:this, handler:function(){ this.selModel.selectRow(this.sRow); this.linkWin('client');} }
					,{text:'שייך לעובד', iconCls:'business_link', scope:this, handler:function(){ this.selModel.selectRow(this.sRow); this.linkWin('user');} }
				]}}
				,{text:'שנה שם' ,iconCls:'edit', scope:this,
					menu: {items:{xtype:'panel', layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', 
						items:[{xtype:'button',text:'שמור', scope:this, handler: function(b){changeName(data,b.container.next().dom.firstChild.value);this.menu.hide();}},{xtype:'textfield', name:'fax_name'}]
						}
					}}	

				,{text:'שמור במחשב', iconCls:'save', scope:this, handler:this.downloadFax }
				,{text:'סמן כלא נקרא', iconCls:'mail', scope:this, handler:this.markNew}
				,{text:'שלח בדוא"ל', iconCls:'mail_send', scope:this, handler:function(){this.selModel.selectRow(this.sRow); sendMailWindow(this);}}
				,{text:'העבר בפקס', iconCls:'fax-send', scope:this, handler:function(){this.selModel.selectRow(this.sRow); sendFaxWindow(this);}}
				,(Ext.getCmp('b_deleted').pressed ? {text:'שחזר', iconCls:'restore', handler:this.restoreFaxPromt, scope:this } : {text:'מחק', iconCls:'delete', scope:this, handler:this.delFaxPromt} )
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.changeTupal = function(r){
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=common/fax&f=change_tupal&id='+r.id+'&type='+r.type+'&tupal='+r.tupal,
			success: function(r,o){
					// r=Ext.decode(r.responseText);
					if (!ajRqErrors(r)) this.store.reload();
					this.getEl().unmask();
				},
				failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
				scope: this
		});
	};
	
	var changeName = function(r,name){
		Ext.getCmp('faxGrid').menu.doHide();
		Ext.getCmp('faxGrid').getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=common/fax&f=change_name&id='+r.id+'&type='+r.type+'&name='+encodeURIComponent(name),
			success: function(r,o){
					if (!ajRqErrors(r)) Ext.getCmp('faxGrid').store.reload();
					Ext.getCmp('faxGrid').getEl().unmask();
				},
				failure: function(r){ Ext.getCmp('faxGrid').getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
				scope: this
		});
	};
	
	this.downloadFax = function(d){
		if (this.sRow!=null) {
			this.selModel.selectRow(this.sRow); // content menu
			Ext.fly(this.view.getRow(this.sRow)).removeClass('bold');
		}
		if (this.getSelectionModel().getCount()!=1) return;
		
		var data = this.getSelectionModel().getSelections()[0].data;
		var iframe=Ext.get('if_fax_frame');
		if (iframe.dom.src) iframe.dom.src='http://'+location.host+'/crm/?m=common/fax_get&NOHD=1&type='+data.type+'&fax_id='+data.id+'&download=1';
	}
	
	this.delFaxPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push('מ-<b dir=rtl>'+r.data.fax+'</b> התקבל ב-<span dir=ltr>'+r.data.date+'</span>');});
		
		Ext.get('if_fax_frame').hide();
		Ext.MessageBox.show({
			width:400, title:'למחוק פקס?', msg:'האם ברצונך למחוק את הפקסים:<br><font face=Arial>'+names.join(',<br>')+' ?<br><br>(לידיעתכם הפקסים יועברו לסל מחזור וימחקו לאחר 30 ימים)</font>', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delFax(records); Ext.get('if_fax_frame').show(); }
		});
	};
	
	this.delFax = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		Ext.get('if_fax_frame').dom.src='about:blank';
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=common/fax&f=fax_del&type='+this.type+'&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					this.delCount+=ids.length; this.updateDelCount();
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); this.view.refresh(); topMsg.msg('פקס נמחק','פקס <b>'+d.data.fax+'</b> <span dir=ltr>'+d.data.date+'</span>' +' נמחק בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};

	this.restoreFaxPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push('מ-<b>'+r.data.fax+'</b> <span dir=rtl>('+r.data.name+')</span> התקבל ב-<span dir=ltr>'+r.data.date+'</span>');});

		
		Ext.get('if_fax_frame').hide();
		Ext.MessageBox.show({
			width:400, icon:Ext.MessageBox.QUESTION, title:'לשחזר פקס?', msg:'האם ברצונך לשחזר את הפקסים:<br><font face=Arial>'+names.join(',<br>')+' ?</font>', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.restoreFax(records); Ext.get('if_fax_frame').show(); }
		});
	};
	
	this.markNew = function(){
		if (this.type=='out') return;
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		var records = this.getSelectionModel().getSelections();
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});
		Ext.get('if_fax_frame').dom.src='about:blank';
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=common/fax&f=fax_not_readed&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						Ext.fly(this.view.getRow(row)).addClass('bold');
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.restoreFax = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});
		Ext.get('if_fax_frame').dom.src='about:blank';
		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=common/fax&f=fax_restore&type='+this.type+'&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					this.delCount-=ids.length; this.updateDelCount();
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid green';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); this.view.refresh(); topMsg.msg('פקס שוחזר','פקס <b>'+d.data.fax+'</b> <span dir=ltr>'+d.data.date+'</span>' +' שוחזר בהצלחה');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
};
Ext.extend(FaxesGrid, Ext.grid.GridPanel);

// שייך
LinkWin = function(grid,clientType) {
	this.clientType=clientType;
	
	this.clientsStore = new Ext.data.JsonStore({url:'?m=common/fax&f=select_client&clientType='+this.clientType, totalProperty:'total', root:'data', fields:['id','name']});	
	this.clientsCombo = new Ext.form.ComboBox({
		fieldLabel:(this.clientType=='client' ? 'לקוח' : 'מעסיק'), 
		hiddenName:'client_id', store:this.clientsStore, valueField:'id', displayField:'name', minChars:2, forceSelection:true, hidden:(this.clientType=='claim'),
		selectOnFocus:true, width:380, loadingText:'טוען...', emptyText:'חיפוש...', listClass:'rtl', //triggerAction:'all', allowBlank:false,
		listeners:{
			select: function(combo, record, index){
				// this.policiesStore.removeAll();
				// this.policiesCombo.clearValue();
				if (this.clientsCombo.getValue()) {
					// this.policiesCombo.emptyText='טוען...';
					// this.policiesCombo.clearValue();
					// this.loadClientData(this.clientsCombo.getValue());
					Ext.getCmp('link_btn').enable();
				}else Ext.getCmp('link_btn').disable();
			},scope:this
		}
	});

	// this.loadClientData=function(client_id){
		// Ext.Ajax.request({scope:this
			// ,url:'?m=common/fax&f=select_police&client_id='+client_id+'&clientType='+this.clientType
			// ,success: function(r,o){
				// r=Ext.decode(r.responseText);
				// if (r.policies.length){ this.policiesStore.loadData(r.policies); this.policiesCombo.enable(); this.policiesCombo.emptyText='בחר פוליסה/תביעה...'; }
				// else { this.policiesCombo.disable(); this.policiesCombo.emptyText='אין פוליסות/תביעות'; }
				// this.policiesCombo.clearValue();
				
				// if (r.folders.length){
					// var folders=[];
					// Ext.apply(folders, filesFolders);
					// for(var i=0; i<r.folders.length; i++) if (!folders.inArray(r.folders[i])) folders.push(r.folders[i]);
					// this.folderCombo.store.loadData(folders);
					// this.folderCombo.setValue('כללי');
				// }
			// },failure:function(r){}
		// });
	// };
	
	this.folderCombo = new Ext.form.ComboBox({fieldLabel:'תיקייה', hiddenName:'file_cat', store:(this.clientType=='claim'?['תביעות']:filesFolders), value:(this.clientType=='claim'?'תביעות':'כללי'), readonly:(this.clientType=='claim'?true:false), triggerAction:'all', width:150, mode:'local', listClass:'rtl', allowBlank:false});
	
	this.form = new Ext.form.FormPanel({
		baseCls: 'x-plain',
		labelWidth:80,
		defaultType: 'textfield',
		items: [
			this.clientsCombo,  this.folderCombo 
			//,{xtype:'textfield', name:'file_name', fieldLabel:'שם קובץ', width:380}
			,{
				xtype:'panel', id:'fileName', layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', bodyStyle:'direction:rtl; padding-bottom:4px;',
				items: [
					 {xtype:'label', text:'שם קובץ:', cls:'x-form-item-label', width:80, style:'text-align:left;margin-right:3px; _margin-left:2px; display:block;' }
					,{xtype:'textfield', name:'file_name', fieldLabel:'שם קובץ', width:380}
				]
			}
			,{xtype:'textfield', name:'notes', fieldLabel:'הערות', width:380}
			,{xtype:'hidden', name:'ids', fieldLabel:'ids'} ]
	});

	if(this.clientType=='group') this.form.remove('policies_combo');

	LinkWin.superclass.constructor.call(this, {
		title: 'שייך פקס ל:',
		iconCls:(this.clientType=='client'?'user-link':'business_link'),
		width:500, modal:true, resizable:false,	layout:'fit', plain:true,
		bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{id:'link_btn', text:'<b>שייך</b>', disabled:true, handler:function(){this.onSubmit();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.on('show', function(){ Ext.get('if_fax_frame').hide(); });
	this.on('hide', function(){ Ext.get('if_fax_frame').show(); });

	this.show = function(){
		LinkWin.superclass.show.apply(this, arguments);

		this.records = grid.getSelectionModel().getSelections();
		var ids=[];
		Ext.each(this.records, function(r){ids.push(r.data.id);});
		this.form.find('name','ids')[0].setValue(ids.join(','));

		this.form.find('name','notes')[0].setValue('');
		if (ids.length==1) {
			var d=this.records[0].data;
			this.form.find('name','file_name')[0].enable();
			this.form.find('name','file_name')[0].setValue('פקס מ-'+d.fax);//+' #'+d.id +' ('+d.name+')'
			this.form.find('id','fileName')[0].show();
		}else {
			this.form.find('name','file_name')[0].disable();
			this.form.find('id','fileName')[0].hide();
		}
	};
		
	this.onSubmit = function() {
		if(this.form.form.isValid()){
			this.el.mask('...טוען', 'x-mask-loading');
			this.form.form.submit({
				url:'?m=common/fax&f=link_fax&type='+grid.type+'&clientType='+this.clientType, scope:this,
				success: function(r,o){
					this.el.unmask();
					this.close();
					this.hideLinked();
				},
				failure: function(r,o){
					this.el.unmask();
					this.syncShadow();
					ajRqErrors(o.result);
				}
			});
		}
	};
	
	this.hideLinked = function() {
		Ext.get('if_fax_frame').dom.src='about:blank';
		Ext.each(this.records, function(d){
			row=this.store.indexOf(d);
			this.selModel.selectRow(row);
			this.view.getRow(row).style.border='1px solid blue';
			Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('פקס שוייך','פקס <b>'+d.data.fax+'</b> <span dir=ltr>'+d.data.date+'</span>' +' שוייך בהצלחה');}, scope:this });
		},grid);
	};
};
Ext.extend(LinkWin, Ext.Window, {});

ShowMetapelWin = function(grid,clientType){
	var win=new MetapelWin(grid,clientType);
	win.show();
};

array_unique=function(arr) {
	if (!arr || !arr.length) return arr;
	var vic={};  
	for(var i=0; i<arr.length; i++) vic[arr[i]]='';
	arr=[];
	for(var i in vic) arr[arr.length] = i;
	return arr;
};


sendFaxWindow = function(grid){
	Ext.get('if_fax_frame').hide();
	if (!faxActive) { Ext.Msg.alert('שרות פקס', 'לקבלת שרות פקס התקשרו: 074-7144333'); return; }
	var d='';
	if (grid) d = grid.getSelectionModel().getSelections()[0].data;
	var win=new SendFax(d);
	win.show();
};
SendFax = function(d) {
	// if (!d) return;
	
	// this.checkFaxnum=function(fld){
		// var val=fld.getValue();
		// if (val=='') return true;
		// val=val.replace(/[^0-9,\n]+/g, '');
		// val=val.replace(/\n+/g, ',');
		// var found = val.match(/(0[0-9]{8,9})(,|$)/g);
		// for(var i=0; i<found.length; i++) found[i]=found[i].replace(',','');
		// found=array_unique(found);
		// Ext.getCmp('recipient_fax').setRawValue(found.join(', '));
	// }

	this.validateFaxnum=function(t){
		t=t.replace(/[^0-9]+/g, '');
		Ext.getCmp('recipient_fax').setRawValue(t);
		return (/^(0[0-9]{8,9})|(1[0-9]{9,11})$/).test(t);
	}

	this.form = new Ext.form.FormPanel({
		labelWidth:70, baseCls:'x-plain', autoHeight:true,
		items:[
			 {hidden:(d?false:true), html:'העבר פקס מ-<b dir=rtl>'+d.fax+'</b> שהתקבל ב-<span dir=ltr>'+d.date+'</span>', style:'padding:5px', baseCls:'x-plain' }
			// ,{fieldLabel:'מספרי פקס', name:'recipient_fax', id:'recipient_fax', xtype:'textarea', listeners:{change:this.checkFaxnum}, cls:'ltr', anchor:'100%', height:29, allowBlank:false, grow:true, growMin:18, growMax:100, growAppend:''}
			,{fieldLabel:'אל פקס', name:'recipient_fax', id:'recipient_fax', xtype:'textfield', validator:this.validateFaxnum, cls:'ltr', anchor:'50%'}
			,{hidden:(d?false:true), layout:'table', baseCls:'x-plain', style:'margin-bottom:5px', defaults:{baseCls:'x-plain'}, items:{xtype:'checkbox', boxLabel:'<b>עמוד מקדים</b>', name:'cover_page', id:'cover_page', checked:false, scope:this, handler:function(e){if(e.checked){Ext.getCmp('coverPageFields').show(); Ext.getCmp('preview_btn').show();} else {Ext.getCmp('coverPageFields').hide();Ext.getCmp('preview_btn').hide();}  this.syncShadow();}}}

			,{layout:'column', id:'coverPageFields', baseCls:'x-plain', defaults:{baseCls:'x-plain', border:false}, items:[
				{columnWidth:0.5, layout:'form', items:[
					{xtype:'textfield', fieldLabel:'לכבוד', name:'recipient_name', anchor:'100%'}
					,{xtype:'textfield', fieldLabel:'מאת (שם)', name:'sender_name', anchor:'100%', value:user.name}
					,{xtype:'textfield', fieldLabel:'מאת פקס', name:'sender_fax', anchor:'100%', value:user.fax}
					,{xtype:'textfield', fieldLabel:'טלפון', name:'sender_tel', anchor:'100%', value:user.tel}
				]},{columnWidth:0.5, labelWidth:20, style:'padding-right:20px', items:[
					{xtype:'checkbox', boxLabel:'לוגו', name:'logo', checked:true}
				]}
				,{columnWidth:1, labelAlign:'top', items:[
					{xtype:'label', text:'הנדון:', style:'display:block'}
					,{fieldLabel:'הנדון', name:'subject', xtype:'textfield', width:523, style:'clear:both'}
					,{xtype:'label', text:'טקסט:', style:'display:block; margin-top:3px'}
					,{xtype:'htmleditor', name:'text', width:523, height:140, style:'clear:both'
						,listeners:{initialize:function(){this.getEditorBody().style.direction='rtl'}}
						,enableColors:false, enableFont:false, enableLinks:false, enableSourceEdit:false
						// ,value:(d ? d.json.notes:''+(type=='mail'?this.grid.signature:''))
					}
				]}
			]}
			// ,{id:'coverPageFields2', baseCls:'x-plain', defaults:{layout:'form', baseCls:'x-plain', border:false}, items:[
				// {xtype:'label', text:'טקסט:'}
				// ,{xtype:'htmleditor', name:'text', width:473, height:180
					// ,listeners:{initialize:function(){this.getEditorBody().style.direction='rtl'}}
					// ,enableColors:false, enableFont:false, enableLinks:false, enableSourceEdit:false
					// ,value:fax_template//(d ? d.json.notes:''+(type=='mail'?this.grid.signature:''))
				// }
			// ]}
		
			,{name:'link_to', xtype:'hidden', value:(d ? (d.type=='out' ? 'fax_out' : 'faxes') :'') }
			,{name:'link_id', xtype:'hidden', value:(d ? d.id :'') }
		]
	});
	
	SendFax.superclass.constructor.call(this, {
		title:'שליחת פקס', items:this.form, iconCls:'fax', layout:'fit', modal:true, width:550, autoHeight:true, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		buttons: [
			{text:'<b>שלח פקס</b>', iconCls:'fax', handler:function(){this.submitForm();}, scope:this}
			,{text:'תצוגה מקדימה', id:'preview_btn', iconCls:'preview', hidden:true, handler:function(){
				// if(!this.form.form.isValid()) return;
				this.form.standardSubmit=true;
				this.form.form.el.dom.action='?m=common/fax&f=send_fax&a=preview';
				this.form.form.el.dom.target='coverpage';
				this.form.form.el.dom.submit();
			}, scope:this}
			,{text:'ביטול', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.on('show', function(){
		Ext.get('if_fax_frame').hide();
		if (d) {
			Ext.getCmp('coverPageFields').hide();
			if (d && d.type=='out') this.formLoad();
			
			this.syncShadow();
		}else {
			Ext.getCmp('preview_btn').show();
			Ext.getCmp('cover_page').setValue(true);
		}
	});
	// this.on('hide', function(){ Ext.get('if_fax_frame').show(); });
	
	this.submitForm = function() {
		if(!this.form.form.isValid()) return;
		if (!d && !this.form.find('name','subject')[0].getValue() && !this.form.find('name','text')[0].getValue()) {
			Ext.Msg.alert('', 'תוכן הפקס ריק.<br>אנא מלא הנדון או טקסט של הפקס.');
			return;
		}
		// this.form.form.getEl().dom.target='_blank';  this.form.form.submit();  return; // open in new window

		this.el.mask('...טוען', 'x-mask-loading');
		this.form.form.submit({
			url:'?m=common/fax&f=send_fax',
			timeout:180,
			scope:this,
			success:function(f,a){
				this.close();
				Ext.getCmp('faxGrid').store.reload();
				Ext.Msg.alert('פקס הועבר לתיבת פקסים יוצאים', 'פקס הועבר לתיבת פקסים יוצאים<br>וישלח מיידית.');
			},
			failure:function(f,a){ this.close(); ajRqErrors(a.result); }
		});
	};
	
	this.formLoad=function() {
		this.getEl().mask('...טוען');
		this.form.load({
			url: "?m=common/fax&f=load_fax_data&type=out&id="+d.id,
			reader: new Ext.data.JsonReader({root:'data', successProperty:'success', totalProperty:'total'}),
			success: function(f,a){
				this.syncShadow();
				this.getEl().unmask();
				this.syncShadow();
			},
			failure: function(){ this.getEl().unmask(); },
			scope:this
	   	});
	}
};
Ext.extend(SendFax, Ext.Window, {});

sendMailWindow = function(grid){
	var d = grid.getSelectionModel().getSelections()[0].data;
	var win=new SendMail(d);
	win.show();
}

MailSuperCombo = function(config) {
	MailSuperCombo.superclass.constructor.call(this, {
		store: new Ext.data.Store({
			proxy: new Ext.data.HttpProxy({url: '?m=common/contacts&f=email_list'}),
			reader:new Ext.data.JsonReader({root: 'data', totalProperty:'total'},['name','email'])
		}),
		fieldLabel:'אל (דוא"ל)', allowBlank:false, msgTarget:'under', allowAddNewData:true, name:'email',
		itemDelimiterKey:{'13':true,/*'32':true,*/'59':true,'188':true},
		xtype:'superboxselect', emptyText:'הקלד דוא"ל', resizable:true, valueField:'email',
		regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/,
		displayField: 'email', minChars: 2, anchor:'-7', pageSize:10,
		tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">&lt;{email}&gt;<i>{name}</i></div></tpl>'),
		itemSelector: 'div.search-item',
		displayFieldTpl: '{email}',
		forceSelection:true, extraItemCls:'x-tag',
		listeners: {
			newitem: function(bs,v){
				v = v.toLowerCase();
				if (/^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/.test(v)){
					var newObj = {email: v,name: v};
					bs.addNewItem(newObj);
				}
			}
		}
	});
	Ext.apply(this, config);
};
Ext.extend(MailSuperCombo, Ext.ux.form.SuperBoxSelect);

SendMail = function(d) {
	if (!d) return;
	//this.attach={ name:'Fax.pdf', id:d.id };
	


	this.form = new Ext.form.FormPanel({
		labelWidth:75,
		baseCls:'x-plain',
		items: [
		 new MailSuperCombo({allowBlank:false,anchor:'-7'})
		,{fieldLabel:'נושא', name:'subject', xtype:'textfield', anchor:'-7', allowBlank:false }
		,{fieldLabel:'הודעה', height:90, name:'message', xtype:'textarea', cls:'rtl',  anchor:'-7' }
		,{
			xtype:'panel', layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', bodyStyle:'direction:rtl',
			items: [
				 {xtype:'label', text:'קובץ מצורף:', cls:'label', width:80, style:'text-align:left; background:url(/skin/icons/attach.gif) no-repeat 67px 0; display:block' }
				,{html:'Fax.pdf', style:'padding-right:5px;color:darkblue;font-weight:bold;', baseCls:'x-plain' }
			]
		}
		,{name:'id', xtype:'hidden', value:d.id}
		,{name:'type', xtype:'hidden', value:d.type}
		]
	});
	
	SendMail.superclass.constructor.call(this, {
		title:'שליחת דוא"ל', items:this.form, layout:'fit', modal:true, width:460, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		buttons: [{text:'שלח', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('show', function(){ Ext.get('if_fax_frame').hide(); });
	this.on('hide', function(){ Ext.get('if_fax_frame').show(); });
	
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		this.el.mask('...טוען', 'x-mask-loading');
		this.form.form.submit({
			url: "?m=common/fax&f=send_mail",
			// waitTitle: 'אנא המתן...',
			// waitMsg: 'טוען...',
			scope:this,
			success: function(f,a){
				//this.el.unmask();
				this.close();
				// var json = Ext.decode(a.response.responseText);
				// if (json.data && this.grid.store) this.grid.store.loadData(json);
			},
			failure: function(r,o){ this.close(); Ext.get('if_fax_frame').hide(); ajRqErrors(o.result); }
		});
	}	
};
Ext.extend(SendMail, Ext.Window, {});

Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
	onRender:function(ct, position){ this.el=ct.createChild({tag:'iframe', id:this.id, name:this.id, frameBorder:0, src:this.url});}
});	



//var searchField = new Ext.app.SearchField({store:faxGrid.store, width:100});

FaxPanel = function(cfg) {//client_id,client_type,link,clientCell,clientEmail,module,columns
	this.content = new Ext.Panel({region:'center', layout:'fit', items: new Ext.ux.IFrameComponent({id:'if_fax_frame', url:'about:blank'})});
	this.faxGrid = new FaxesGrid(fax_grid_columns);

	FaxPanel.superclass.constructor.call(this, {
		region:'center', layout:'border',
		id:'faxMenu',
		baseCls:'x-plain',
		items:[this.faxGrid, this.content],
		tbar: [ '<b>פקסים:</b>',
			 { text:'נכנסים', id:'b_folder-in', iconCls:'folder-in', handler:function(){
					this.faxGrid.type='in';
					this.faxGrid.store.reload();
					this.getTopToolbar().items.get('del_fax').show();
					this.getTopToolbar().items.get('restore_fax').hide();
					this.faxGrid.store.groupBy('to');
				}, scope:this, enableToggle:true, toggleGroup:'folders', pressed:true, allowDepress:false }
			,{ text:'יוצאים', id:'b_folder-out', iconCls:'folder-out', hidden:!faxActive, handler:function(){
					this.faxGrid.type='out';
					this.faxGrid.store.reload();
					this.getTopToolbar().items.get('del_fax').show();
					this.getTopToolbar().items.get('restore_fax').hide();
					this.faxGrid.store.clearGrouping();
				}, scope:this, enableToggle:true, toggleGroup:'folders', allowDepress:false }
			,{ text:'מחוקים', id:'b_deleted', iconCls:'trash', handler:function(){
					this.faxGrid.store.reload();
					this.getTopToolbar().items.get('del_fax').hide();
					this.getTopToolbar().items.get('restore_fax').show();
					this.faxGrid.store.clearGrouping();
				}, scope:this, enableToggle:true, toggleGroup:'folders', allowDepress:false 
			}
			, '-' ,{ text:'מחק', id:'del_fax', iconCls:'delete', handler:this.faxGrid.delFaxPromt, scope:this.faxGrid, disabled:true }
			,{ text:'שחזר', id:'restore_fax', iconCls:'restore', handler:this.faxGrid.restoreFaxPromt, scope:this.faxGrid, disabled:true, hidden:true }
			, '-' ,{ text:'פקס חדש', id:'fax-new', iconCls:'fax', handler:function(){sendFaxWindow();}, scope:this }
			, '-' ,{ text:'שייך ללקוח', id:'link_client', iconCls:'user-link', handler:function(){ this.linkWin('client');}, scope:this.faxGrid, disabled:true }
			, '-' ,{ text:'שייך לעובד', id:'link_user', iconCls:'business_link', handler:function(){ this.linkWin('user');}, scope:this.faxGrid, disabled:true }
			, '-' ,{ text:'שמור', id:'save', iconCls:'save', handler:this.faxGrid.downloadFax, scope:this.faxGrid, disabled:true }	
			, '-' ,{ text:'העבר בפקס', id:'fax-send', iconCls:'fax-send', handler:function(){sendFaxWindow(this.faxGrid);}, disabled:true, scope:this }
			, '-' ,{ text:'שלח בדוא"ל', id:'send_mail', iconCls:'mail_send', handler:function(){sendMailWindow(this.faxGrid);}, disabled:true, scope:this }
		]
	});
};
Ext.extend(FaxPanel, Ext.Panel);

Ms.fax.run = function(cfg){
	var m=Ms.fax;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		win = Crm.desktop.createWindow({ width:900,	height:600,	items: new FaxPanel()}, m);
	}
	win.show();
};