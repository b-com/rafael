﻿<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end FROM users ORDER BY fullName",'id');
	
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;
	}

	?>
	
	var users_obj=<?=array2json($users_ar);?>;
	
	Ext.ux.Loader.load([
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			'modules/accounting/incomes.js?<?=filemtime('modules/accounting/incomes.js');?>'
		], function(){Ms['accounting/incomes'].run();}
	);
	<?
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_invoice() {
 global $SVARS;
	$sql="num=".quote($_POST['num'])
		.", client_id=".(int)$_POST['client_id']
		.", user_id={$SVARS['user']['id']}"
		.", date='".str2time($_POST['date'],'Y-m-d')
		."',type='invoice_credit'"
		.", sum=".round($_POST['sum'],2)
		.", VAT=".round($_POST['VAT'],2)
		.", vat_sum=".round(($_POST['sum_tax']-$_POST['sum']),2)
		.", sum_tax=".round($_POST['sum_tax'],2)
		.", notes=".quote($_POST['notes'],2)
	;
	
	if ($_POST['id']) $sql="UPDATE acc_docs SET ".$sql." WHERE id=".(int)$_POST['id']; else $sql="INSERT INTO acc_docs SET ".$sql;
	
	if (runsql($sql)) echo "{success:true}"; else "{success:false}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_incomes_list() {
 global $prms_groups, $VAT;
 
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if ($_POST['q']) {
		$make_where_search=make_where_search('clients.name,clients.lname,clients.fname', $_POST['q']);
		if (ereg('^[0-9.]*$',$_POST['q'])) $make_where_search="($make_where_search OR num=$_POST[q] OR ROUND(sum)=".round($_POST['q']).")";
		elseif (ereg("([0-9]{1,2})[./]?([0-9]{1,2})[./]?([0-9]{2,4})( ([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?)?", $_POST['q']) ) $make_where_search="(date='".str2time($_POST['q'],'Y-m-d')."' OR $make_where_search)";
	}
	
	$sql="SELECT SQL_CALC_FOUND_ROWS acc_docs.*, TRIM(CONCAT(clients.name,' ',clients.lname,' ',clients.fname)) AS client_name FROM acc_docs, clients WHERE acc_docs.type IN ('invoice_debt','invoice_credit','invoice_accept')"
		.($make_where_search ? " AND $make_where_search":'' )
		." ORDER BY date DESC, id DESC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	// echo "/*$sql*/";	
	$rows=sql2array($sql);
	if ($rows) $rows=array_reverse($rows);

	if ($rows) foreach($rows as $k=>$r) {
		if ($r['date']!='0000-00-00') $rows[$k]['date']=str2time($r['date'],'d/m/Y');
		if ($r['type']=='invoice_debt') {$rows[$k]['sum']=-$r['sum'];$rows[$k]['vat_sum']=-$r['vat_sum'];$rows[$k]['sum_tax']=-$r['sum_tax'];}
	}
	
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
?>