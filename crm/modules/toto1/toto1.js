/*
var values = {
	1:[2.63,3.4,3],
	2:[2.1,2.95,2.75],
	3:[1.95,3,3.05],
	4:[1.5,3,4],
	5:[1.5,3.15,3.8],
	6:[2.05,3,2.8],
	7:[2.6,2.95,1.9],
	8:[1.4,3.7,5.2],
	9:[2.85,1.75,3.05],
	10:[1.5,3,4],
	11:[1.5,3.15,3.8],
	12:[2.05,3,2.8],
	13:[2.6,2.95,1.9],
	14:[1.4,3.7,5.2],
	15:[2.85,1.75,3.05]
};
*/
ExpectedCombo = function(config) {
	ExpectedCombo.superclass.constructor.call(this, {xtype:'combo', itemCls:'rtl', width:45, listWidth:35, store:[[1,1],[2,'X'],[4,2],[12,'1+X'],[24,'X+2'],[14,'1+2']], emptyText:'-', triggerAction:'all', mode:'local'});
	Ext.apply(this, config);
};
Ext.extend(ExpectedCombo, Ext.form.ComboBox);

TeamCombo = function(config) {
	TeamCombo.superclass.constructor.call(this, {xtype:'combo', itemCls:'rtl', hideTrigger:true, width:90, listWidth:90, store:[[1,1],[2,'X'],[4,2],[12,'1+X'],[24,'X+2'],[14,'1+2']], triggerAction:'all', mode:'local'});
	Ext.apply(this, config);
};
Ext.extend(TeamCombo, Ext.form.ComboBox);

TotoFormPanel = function(cfg) {

	this.tableBuild = function(){
		var fld;
		for (g in values) {
			fld = this.form.find('name','g['+g+'][1]');	fld[0].setValue(values[g][0]);
			fld = this.form.find('name','g['+g+'][2]');	fld[0].setValue(values[g][1]);
			fld = this.form.find('name','g['+g+'][4]');	fld[0].setValue(values[g][2]);
		}
	}
	this.saveData = function() {
		Ext.Ajax.request({
			url:'?m=toto1/toto1&f=save_data',
			params:this.form.form.getValues(),
			success: function(r){Ext.Msg.alert('', 'הנתונים נשמרו.');},
			failure: function(r){Ext.Msg.alert('', 'תקלה בשירת נתונים.');},
			scope: this
		});
	}
	
	this.loadData = function() {
		Ext.Ajax.request({
			url:'?m=toto1/toto1&f=load_data',
			params:this.form.form.getValues(),
			success: function(r){Ext.Msg.alert('', 'הנתונים נשמרו.');},
			failure: function(r){Ext.Msg.alert('', 'תקלה בשירת נתונים.');},
			scope: this
		});
	}
	

	
	this.doTable = function(){
		var arr = this.form.form.getValues();
		var min=0,max=0,sum=0, d=0;
		for (i=1; i<16; i++) {
		
			// sum = (parseFloat(arr['g['+i+'][1]'])+parseFloat(arr['g['+i+'][2]'])+parseFloat(arr['g['+i+'][4]']));
			// min = min + Math.min(parseFloat(arr['g['+i+'][1]']),parseFloat(arr['g['+i+'][2]']),parseFloat(arr['g['+i+'][4]']))/sum;
			// max = max + Math.max(parseFloat(arr['g['+i+'][1]']),parseFloat(arr['g['+i+'][2]']),parseFloat(arr['g['+i+'][4]']))/sum;
			
			sum = (1/parseFloat(arr['g['+i+'][1]'])+1/parseFloat(arr['g['+i+'][2]'])+1/parseFloat(arr['g['+i+'][4]']));
			
			min = min + Math.min(1/parseFloat(arr['g['+i+'][1]']),1/parseFloat(arr['g['+i+'][2]']),1/parseFloat(arr['g['+i+'][4]']))/sum;
			max = max + Math.max(1/parseFloat(arr['g['+i+'][1]']),1/parseFloat(arr['g['+i+'][2]']),1/parseFloat(arr['g['+i+'][4]']))/sum;
			
		}
		min=parseInt(min*1000000);
		max=parseInt(max*1000000);
		// alert('Min='+min+'; Max='+max);
		d=parseInt((max-min)/10);
		
		for (i=1; i<11; i++) {
			this.form.find('name','sum_min'+i)[0].setValue(min); min=min+d;
			this.form.find('name','sum_max'+i)[0].setValue(min-1+parseInt(i/10));
		}
		this.form.find('name','sum_max10')[0].setValue(max);
		Ext.getCmp('bldFilesBtn').enable();
	}
	
	// this.previewWin = new Ext.Window({
		// id:'previewWin', html:'<iframe id=prevIframe src="about:blank" style="width:100%; height:100%;" frameBorder=0></iframe>',
		// x:5, y:50, width:(Ext.getBody().getWidth()-270), height:(Ext.getBody().getHeight()-55), plain:true, border:false, layout:'fit', closeAction:'hide', closable:false,
		// listeners: { hide:function(){Ext.get('prevIframe').dom.src='about:blank';} }
	// });


	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain', region:'center', id:'totoForm'
		,items: [{
			layout:'column', border:false, items:[
				{title:'1. משחקים', border:false, width:420, style:'border-left:1px solid #99BBE8; padding-bottom:10px;', items: 
					{layout:'table', layoutConfig: {columns:7}, baseCls: 'x-plain', cls:'toto_games_table', items:[
						 {xtype:'label', style:"width:25px;", text:'#'}
						,{xtype:'label', text:'קבוצת בית'}
						,{xtype:'label', text:'קבוצת חוץ'}
						,{xtype:'label', text:'1'}
						,{xtype:'label', text:'X'}
						,{xtype:'label', text:'2'}
						,{xtype:'label', text:'צפוי'}
						,{xtype:'label', text:'1'}
						,new TeamCombo({hiddenName:'g[1][home_team]'})
						,new TeamCombo({hiddenName:'g[1][foreign_team]'})
						,{xtype:'textfield', name:'g[1][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[1][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[1][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[1][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'2'}
						,new TeamCombo({hiddenName:'g[2][home_team]'})
						,new TeamCombo({hiddenName:'g[2][foreign_team]'})
						,{xtype:'textfield', name:'g[2][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[2][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[2][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[2][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'3'}
						,new TeamCombo({hiddenName:'g[3][home_team]'})
						,new TeamCombo({hiddenName:'g[3][foreign_team]'})
						,{xtype:'textfield', name:'g[3][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[3][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[3][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[3][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'4'}
						,new TeamCombo({hiddenName:'g[4][home_team]'})
						,new TeamCombo({hiddenName:'g[4][foreign_team]'})
						,{xtype:'textfield', name:'g[4][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[4][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[4][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[4][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'5'}
						,new TeamCombo({hiddenName:'g[5][home_team]'})
						,new TeamCombo({hiddenName:'g[5][foreign_team]'})
						,{xtype:'textfield', name:'g[5][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[5][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[5][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[5][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'6'}
						,new TeamCombo({hiddenName:'g[6][home_team]'})
						,new TeamCombo({hiddenName:'g[6][foreign_team]'})
						,{xtype:'textfield', name:'g[6][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[6][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[6][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[6][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'7'}
						,new TeamCombo({hiddenName:'g[7][home_team]'})
						,new TeamCombo({hiddenName:'g[7][foreign_team]'})
						,{xtype:'textfield', name:'g[7][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[7][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[7][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[7][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'8'}
						,new TeamCombo({hiddenName:'g[8][home_team]'})
						,new TeamCombo({hiddenName:'g[8][foreign_team]'})
						,{xtype:'textfield', name:'g[8][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[8][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[8][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[8][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'9'}
						,new TeamCombo({hiddenName:'g[9][home_team]'})
						,new TeamCombo({hiddenName:'g[9][foreign_team]'})
						,{xtype:'textfield', name:'g[9][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[9][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[9][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[9][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'10'}
						,new TeamCombo({hiddenName:'g[10][home_team]'})
						,new TeamCombo({hiddenName:'g[10][foreign_team]'})
						,{xtype:'textfield', name:'g[10][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[10][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[10][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[10][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'11'}
						,new TeamCombo({hiddenName:'g[11][home_team]'})
						,new TeamCombo({hiddenName:'g[11][foreign_team]'})
						,{xtype:'textfield', name:'g[11][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[11][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[11][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[11][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'12'}
						,new TeamCombo({hiddenName:'g[12][home_team]'})
						,new TeamCombo({hiddenName:'g[12][foreign_team]'})
						,{xtype:'textfield', name:'g[12][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[12][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[12][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[12][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'13'}
						,new TeamCombo({hiddenName:'g[13][home_team]'})
						,new TeamCombo({hiddenName:'g[13][foreign_team]'})
						,{xtype:'textfield', name:'g[13][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[13][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[13][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[13][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'14'}
						,new TeamCombo({hiddenName:'g[14][home_team]'})
						,new TeamCombo({hiddenName:'g[14][foreign_team]'})
						,{xtype:'textfield', name:'g[14][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[14][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[14][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[14][exp]'})
						,{xtype:'label',  cls:'toto_p5', text:'15'}
						,new TeamCombo({hiddenName:'g[15][home_team]'})
						,new TeamCombo({hiddenName:'g[15][foreign_team]'})
						,{xtype:'textfield', name:'g[15][1]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[15][2]', allowBlank:true, width:45}
						,{xtype:'textfield', name:'g[15][4]', allowBlank:true, width:45}
						,new ExpectedCombo({hiddenName:'g[15][exp]'})
						,{xtype:'button', text:'שמור נתונים', height:18, colspan:3, handler:function(){this.saveData()}, scope:this}
						// ,{xtype:'button', text:' נתונים', height:18, colspan:4, handler:function(){this.loadData()}, scope:this}

					]}
				
				}
				,{title:'2. סינונים', columnWidth:.5, border:false, items:
					{xtype:'panel', layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'toto_games_table', items:[
						  {xtype:'label',  text:'1) משקל טור בין:', style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'sum_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'sum_max', width:35}
						 
						 ,{xtype:'label',  text:"2) כמות זוגות בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'pair_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'pair_max', width:35}
						 
						 ,{xtype:'label',  text:"3) כמות '1' בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'count1_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'count1_max', width:35}
						 
						 ,{xtype:'label',  text:"4) כמות 'X' בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'count0_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'count0_max', width:35}
						 
						 ,{xtype:'label',  text:"5) כמות '2' בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'count2_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'count2_max', width:35}
						 
						 ,{xtype:'label',  text:'6) כמות תוצאות צפויות:', style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'exp_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'exp_max', width:35}
						 
						 ,{xtype:'label',  text:"7) כמות שבירות בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'break_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'break_max', width:35}
						 
						 ,{xtype:'label',  text:"8) כמות זוגות '1' בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'pair1_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'pair1_max', width:35}
						 
						 ,{xtype:'label',  text:"9) כמות זוגות 'X' בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'pair0_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'pair0_max', width:35}
						 
						 ,{xtype:'label',  text:"10) כמות זוגות '2' בטור בין:", style:"text-align:right; display:block; width:140px;"}
						 ,{xtype:'textfield', name:'pair2_min', width:35}
						 ,{xtype:'label',  cls:'toto_p5', text:'ל:'}
						 ,{xtype:'textfield', name:'pair2_max', width:35}
						]}
				
				}
			]}

		
		 ,new Ext.Panel ({region:'south', autoScroll:true, height:360, title:'3. חישובים', // style:'padding:10px;',
			items:[
				{xtype:'button', text:'בנה טבלה', height:18,  cls:'toto_p5', handler:function(){this.doTable()}, scope:this}
				,{layout:'table', layoutConfig: {columns:11}, baseCls: 'x-plain', defaults: {xtype:'textfield'}, items:[
					{xtype:'label',  cls:'toto_p5', text:'עשיריה'}
					,{xtype:'checkbox', name:'f1', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f2', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f3', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f4', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f5', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f6', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f7', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f8', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f9', checked:true,  cls:'toto_mr25'}
					,{xtype:'checkbox', name:'f10',checked:true,  cls:'toto_mr25'}
					,{xtype:'label', cls:'toto_p5', text:'נמוך'}
					,{name:'sum_min1', cls:'toto_inp50'},{name:'sum_min2', cls:'toto_inp50'},{name:'sum_min3', cls:'toto_inp50'},{name:'sum_min4', cls:'toto_inp50'},{name:'sum_min5', cls:'toto_inp50'},{name:'sum_min6', cls:'toto_inp50'},{name:'sum_min7', cls:'toto_inp50'},{name:'sum_min8', cls:'toto_inp50'},{name:'sum_min9', cls:'toto_inp50'},{name:'sum_min10', cls:'toto_inp50'}
					,{xtype:'label', cls:'toto_p5', text:'גבוה'}
					,{name:'sum_max1', cls:'toto_inp50'},{name:'sum_max2', cls:'toto_inp50'},{name:'sum_max3', cls:'toto_inp50'},{name:'sum_max4', cls:'toto_inp50'},{name:'sum_max5', cls:'toto_inp50'},{name:'sum_max6', cls:'toto_inp50'},{name:'sum_max7', cls:'toto_inp50'},{name:'sum_max8', cls:'toto_inp50'},{name:'sum_max9', cls:'toto_inp50'},{name:'sum_max10', cls:'toto_inp50'}
					,{xtype:'button', text:'בנה קבצים', disabled:true, id:'bldFilesBtn', height:18, colspan:11, cls:'toto_p5', handler:function(){this.submitForm('build_files')}, scope:this}
					
					,{xtype:'label', cls:'toto_p5', text:"סך טורים"}
					,{name:'count1', cls:'toto_inp50'},{name:'count2', cls:'toto_inp50'},{name:'count3', cls:'toto_inp50'},{name:'count4', cls:'toto_inp50'},{name:'count5', cls:'toto_inp50'},{name:'count6', cls:'toto_inp50'},{name:'count7', cls:'toto_inp50'},{name:'count8', cls:'toto_inp50'},{name:'count9', cls:'toto_inp50'},{name:'count10', cls:'toto_inp50'}
					

					,{xtype:'label',  cls:'toto_p5', text:''}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',1)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',2)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',3)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',4)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',5)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',6)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',7)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',8)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',9)}, scope:this}
					,{xtype:'button', text:'סנן', height:18, width:50, style:'margin:7px', handler:function(){this.submitForm('filter',10)}, scope:this}
					
					,{xtype:'label', cls:'toto_p5', text:"טורים אחרי סינון"}
					,{name:'countf1', cls:'toto_inp50'},{name:'countf2', cls:'toto_inp50'},{name:'countf3', cls:'toto_inp50'},{name:'countf4', cls:'toto_inp50'},{name:'countf5', cls:'toto_inp50'},{name:'countf6', cls:'toto_inp50'},{name:'countf7', cls:'toto_inp50'},{name:'countf8', cls:'toto_inp50'},{name:'countf9', cls:'toto_inp50'},{name:'countf10', cls:'toto_inp50'}
					
					,{xtype:'label',  cls:'toto_p5', text:''}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo1_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo2_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo3_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo4_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo5_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo6_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo7_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo8_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo9_f.str'}, scope:this}
					,{xtype:'button', text:'הורד', height:18, width:50, style:'margin:7px', handler:function(){location='modules/toto1/demo10_f.str'}, scope:this}
				]}
			]
		  })


		]
	});
	
	TotoFormPanel.superclass.constructor.call(this, {
		// title:'הרשאות קבוצה',
		layout:'fit',border:false, region:'center',
		items:[
			this.form
		]
    });
	
	this.on('render', function(){this.tableBuild()},this);
	
	this.submitForm = function(fn,num) {
	
		// if (!this.form.form.isValid()) return;

		var win = new Ext.Window({
			id:'waitWin', modal:true, html:'<iframe name=waitFrame src="about:blank" style="width:99%; height:99%;" frameBorder=3></iframe>',
			x:5, y:50, width:400, height:400, plain:true, border:false, layout:'fit'//, //closeAction:'hide', closable:false,
		});
		
		win.show();
		
		
		(function(){
			var f=this.form.form.getEl().dom;
			f.target = 'waitFrame';
			f.action = '?m=toto1/toto1&NOGZ=1&f='+fn+'&file_num='+num;
			f.submit();
		}).defer(2000, this);
	}
};
Ext.extend(TotoFormPanel, Ext.Panel);

Ms.toto1.run = function(cfg){
	var m=Ms.toto1;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		win = Crm.desktop.createWindow({width:760, height:700, iconCls:'toto', layout:'border',	items:[new TotoFormPanel]}, m);
	}
	win.show();
};