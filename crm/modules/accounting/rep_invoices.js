InvoicesRep = function(from,to) {
	this.repWindow = function(type) {
		var d_from=this.form.find('name','d_from')[0].getValue().format('Y-m-d'),
			d_to=this.form.find('name','d_to')[0].getValue().format('Y-m-d'),
			t=Ext.getCmp('t1').getValue(),
			q=this.searchField.getRawValue().trim(),
			SortState = this.store.getSortState(); 
		window.open('?m=accounting/reports&f=invoices&'+type+'=1&d_from='+d_from+'&d_to='+d_to+'&q='+q+'&user_col'+'&type'+t+'&sort='+SortState.field+'&dir='+SortState.direction/*+user_col*/,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	};
	
	this.form = new Ext.form.FormPanel({
		labelWidth:80, height:66, region:'north', border:false, style:'padding:5px 5px 0 0;',	cls:'x-panel-mc',
		items: [{layout:'table', layoutConfig: {columns:6}, baseCls: 'x-plain', cls:'rtl',//style:'padding-right:3px',
				items:[
				{xtype:'label', text:'תאריך הפקה בין:', style:'padding-right:10px;'},
				{xtype:'datefield', id: 'invoice_d_from', name:'d_from', width: 85, format:'d/m/Y', value:(from ? from : month_first()), cls:'ltr', allowBlank:false},//, vtype:'daterange'
				{xtype:'label', text:'ל:'},
				{xtype:'datefield', id: 'invoice_d_to', name:'d_to',width: 85, format:'d/m/Y', value:(to ? to : month_last()), cls:'ltr', allowBlank:false},//, vtype:'daterange'
				{xtype:'label', text:'סיווג:', style:'padding-right:10px;'},{xtype:'combo', id:'t1', name:'t1', width: 150, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:[[0,'כל חשבוניות'],['credit','חשבוניות מס'],['debt','חשבוניות מס זיכוי']]}
			]},{layout:'table', layoutConfig: {columns:4}, baseCls: 'x-plain', cls:'rtl', style:'padding:5px 120px',
			items:[
				{xtype:'button', cls:'btnRight', text:'הצג דו"ח', iconCls:'other_police', handler:function(){Ext.getCmp('b_cellOnly').setValue(0); this.store.reload();}, scope:this, width: 90},
				{xtype:'button', cls:'btnRight', id:'invoice_prn_btn', style:'margin-right:8px;', iconCls:'print', text:'הדפס דו"ח', disabled: true, width: 90, handler:function(){this.repWindow('print')}, scope: this},
				{xtype:'button', id:'invoice_prn_pay', style:'margin-right:8px;', iconCls:'print', text:'טופס תשלום', disabled:true, width:90, handler:function(){this.repWindow('payment')}, scope: this},
				{xtype:'button', cls:'btnRight', id:'invoice_exl_btn', style:'margin-right:8px;', iconCls:'excel', text:'Excel-ייצוא ל', disabled: true, width: 90, handler:function(){this.repWindow('export')}, scope: this}
			]
			},{xtype:'hidden', id:'b_cellOnly', name:'b_cellOnly'}
		]
	});
		
	this.store = new Ext.data.JsonStore({
		url:'?m=accounting/reports&f=invoices', totalProperty:'total', root:'data',
		fields: ['id','client_id','client_name','client_phones','client_addresses',{name:'date',type:'date', dateFormat:'Y-m-d'},'notes','num','type','netto','sum','vat_sum','username'],
		sortInfo: {field:'client_name', direction:'ASC'},
		timeout:1000, remoteSort:true
	});
	
	this.hlStr = hlStr;
	this.searchField = new GridSearchField({store:this.store, width:150});
	
	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			q: this.searchField.getRawValue().trim(),
			d_from: Ext.getCmp('invoice_d_from').getValue(),
			d_to: Ext.getCmp('invoice_d_to').getValue(),
			t: Ext.getCmp('t1').getValue(),
			cellOnly: Ext.getCmp('b_cellOnly').getValue()
		}
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
		// this.getEl().mask('...טוען');
	}, this);

	this.store.on('load', function(store, options){
		Ext.getCmp('invoice_prn_btn').setDisabled(!store.getCount());
		Ext.getCmp('invoice_prn_pay').setDisabled(!store.getCount());
		Ext.getCmp('invoice_exl_btn').setDisabled(!store.getCount());
		var Rec = this.store.recordType;
		var p = new Rec(this.store.reader.jsonData.summary);
		this.store.insert(this.store.getCount(), p);

		this.getEl().unmask();
	}, this);
	
	this.colModel = new Ext.grid.ColumnModel([
			{header:'תאריך הפקה', dataIndex:'date', width:85, sortable:true, css:'direction:ltr;', align:'center', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
			,{header:'סוג חשבונית', width:120, dataIndex:'type', renderer:docTypeRender, sortable:true}
			,{header:'מספר', width:120, dataIndex:'num', sortable:true}
			,{header:'שם לקוח', width:150, dataIndex:'client_name', renderer:clientRenderer, scope:this}
			,{header:'סכום', width:100, dataIndex:'netto', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'מע"מ', width:100, dataIndex:'vat_sum', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'סה"כ', width:100, dataIndex:'sum', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'הערות', id: 'notes', dataIndex:'notes', sortable:false, renderer:this.hlStr.createDelegate(this)}
	]);
	
	this.onItemCheck = function (item, checked){
		var cm = this.grid.getColumnModel();
		if (item.text=='משתמש')	cm.setHidden(0, !checked);// else
    };

	this.grid = new Ext.grid.GridPanel({
		stripeRows:true, layout:'fit', cls:'prod-rep', region:'center',	autoExpandColumn:'notes', border:false, enableHdMenu:false,
		store: this.store,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'חיפוש:', this.searchField
			,'-' ,{ text:'פתח לקוח', id:'user_open', iconCls:'user_open', handler:function(){openClientNewWin(this.grid.getSelectionModel().getSelected().data)}, disabled:true, scope:this }
			,'-' ,{text:'הגדרת עמודות', iconCls:'cols-icon', //x-menu-item-icon 
				menu: {items: [
					{text: 'משתמש', checked: false, checkHandler:this.onItemCheck, scope:this}
				]}} 
		],
		colModel: this.colModel,
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
	});
	
	this.grid.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.grid.getTopToolbar().items.get('user_open').setDisabled(sm.getCount()!=1);
	}, this);

	this.grid.on('rowdblclick', function(grid){
		var data = grid.getSelectionModel().getSelected().data;
		var type_id=0;
		var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type='+data.type+'&id='+data.id+'&type_id='+type_id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	});
	
	this.grid.on('rowcontextmenu', function(grid, row, e){
		var r=this.store.getAt(row);
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'פתח לקוח בחלון חדש', iconCls:'user_open', scope:this, handler: function(){openClientNewWin(r.data);} }
				,{text:'למסומנים SMS', iconCls:'send_sms', scope:this, 
					handler:function(){
						this.grid.getSelectionModel().selectRow(row,true);
						var records = this.grid.getSelectionModel().getSelections();
						var ids=[]; var lids=[];
						Ext.each(records, function(r){
							if (r.data.link) lids.push(r.data.id);
							else ids.push(r.data.client_id);
						});
						this.sendSmsWindow({ids:ids.join(','), lids:lids.join(',')});
					}
				}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);
	
	InvoicesRep.superclass.constructor.call(this, {border:false, region:'center', layout:'border', items:[this.form,this.grid]})

	this.store.load();
};
Ext.extend(InvoicesRep, Ext.Panel);