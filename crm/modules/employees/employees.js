var carTypes = ['����','�����','�����','�����','�������','������','���'];
var fuelTypes =['�����','����','��','���'];
var units = [[1,'�����'],[2,'���'],[3,'����'],[4,'�������']];

Ext.grid.GridSummary = function(config){Ext.apply(this, config);};


CarSmsWindow = function(cfg) {
	this.addTel=function(){
		Ext.getDom('sms_tels').options.add(new Option(Ext.getCmp('sms_tel').getValue()));
		Ext.getCmp('sms_tel').setValue('');
	}
	
	this.checkTel=function(t){
		t=t.replace(/[^0-9 -]+/g, '');
		Ext.getCmp('sms_tel').setRawValue(t);
		var t1=t.replace(/[^0-9]+/g, '');
		var isValid=(/^05[024][0-9]{7}$/).test(t1);
		if (isValid) Ext.getCmp('sms_tel_add').enable();
		else Ext.getCmp('sms_tel_add').disable();
		return ((t=='' && Ext.getDom('sms_tels').length) ? true : isValid);
	}
	
	this.checkOriginator=function(t){
		if (t=='') return true;
		t=t.replace(/[^0-9 -]+/g, '');
		Ext.getCmp('originator').setRawValue(t);
		var t1=t.replace(/[^0-9]+/g, '');
		return (/^0[0-9]{8,9}$/).test(t1);
	}
	
	this.removeTel=function(){
		var sel=Ext.getDom('sms_tels');
		sel.remove(sel.selectedIndex);
	}
	
	this.charCount=function(fld){
		var v=fld.getValue()
		var msg='����: <b>'+v.length+'</b> �����';
		if (v.length>70) msg+=' (<b>'+Math.ceil(v.length/67)+'</b>)';// <img src="skin/icons/help.gif" width=16 height=16 ext:qtip="">
		Ext.get('char_count').update(msg);
	}
	
	this.form = new Ext.form.FormPanel({
		id:'contactForm', labelWidth:80, baseCls:'x-plain',
		items: [{
			layout:'column',baseCls: 'x-plain',
			defaults: {xtype:'panel', columnWidth:0.5, layout:'form', baseCls:'x-plain', border:false},
			items:[{
				items: {fieldLabel:'����� ������', name:'back_date', xtype:'datefield', width:115, format:'d/m/Y H:i', cls:'ltr', vtype:'daterange', allowBlank:true}
			},{
				items: {fieldLabel:'�����', name:'date', xtype:'datefield', width: 115, format:'d/m/Y H:i', cls:'ltr', vtype:'daterange', value:new Date().format('d/m/Y H:i')}
			}]
		}
		,{xtype:'combo', fieldLabel:'������', name:'importance', hiddenName:'importance', valueField:'id', displayField:'val', width: 115, value:0, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:false, store:[[0,'����'],[1,'����'],[2,'����']]}
		,{xtype:'hidden', name:'client_type', value:(cfg.client_type ? cfg.client_type :'client') }
		,{xtype:'hidden', name:'link', value:(cfg.link ? cfg.link :'') }
		,{xtype:'textfield', fieldLabel:'���� �����', name:'originator', id:'originator', cls:'ltr', value:(sms_originator?sms_originator:''), width:100, enableKeyEvents:true, validator:this.checkOriginator }
		,{
			layout:'column', baseCls:'x-plain', border:false, defaults:{baseCls:'x-plain', border:false},
			items:[{
				columnWidth:.5, labelAlign:'top',
				items: [{xtype:'label', text:'�����:', cls:'label', style:'display:block; line-height:20px'},
						{xtype:'textarea', name:'notes', height:72, width:200, allowBlank:false, enableKeyEvents:true, listeners:{scope:this, keyup:this.charCount, change:this.charCount} }
				]
			},{
				columnWidth:.4, bodyStyle:'padding-right:20px', defaults:{baseCls:'x-plain', border:false},
				items: [ {xtype:'label', text:'��:', cls:'label'}
						,{xtype:'textfield', id:'sms_tel', cls:'ltr', anchor:'100%', enableKeyEvents:true, validator:this.checkTel }
						,{bodyStyle:'padding-top:2px', html:"<select size=6 id=sms_tels dir=ltr style='width:142px; height:75px; border:1px solid #b5b8c8;'></select>"}
						,{name:'tels', xtype:'hidden'} ]
			},{
				columnWidth:.1,
				items: [ {xtype:'button', text:'����', id:'sms_tel_add', disabled:true, scope:this, handler:this.addTel}
						,{xtype:'button', text:'���', scope:this, handler:this.removeTel, style:'margin-top:5px'} ]
			}
			]
		}
		,{id:'char_count', html:'����:', baseCls:'x-plain', border:false, style:'line-height:18px'}
		]
	});
	
	if (cfg.number) Ext.getCmp('sms_tel').setValue(cfg.number);
	if (sms_msg_tpl) this.form.find('name','notes')[0].setValue(sms_msg_tpl);
	
	CarSmsWindow.superclass.constructor.call(this, {
		title:'����� SMS', iconCls:'send_sms', modal:true, resizable:false,	width:460, buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons: [{text:'���', handler:function(){this.submitForm();}, scope:this, iconCls:'send_sms'},{text:'�����', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		if (this.form.find('name','notes')[0].getValue().trim()==sms_msg_tpl.trim()) { this.form.find('name','notes')[0].markInvalid(); return; }
		
		// set tels value from select & text
		var ops=Ext.getDom('sms_tels').options;
		var tels=[];
		if (ops.length) for (i=0; i<ops.length; i++) tels.push(ops[i].text);
		if (Ext.getCmp('sms_tel').getValue()) tels.push(Ext.getCmp('sms_tel').getValue());		
		this.form.find('name','tels')[0].setValue(tels.join(','));
		
		if (tels.length==0) { Ext.getCmp('sms_tel').markInvalid(); return; }
		
		this.form.form.submit({
			url: "/?m=crm/contacts_f&f=add_contact&id="+cfg.id,
			waitTitle: '��� ����...', waitMsg: '����...',
			scope:this,
			success: function(f,a){
				this.close();
				var json = Ext.decode(a.response.responseText);
				if (cfg.callBack) cfg.callBack();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}	
};
Ext.extend(CarSmsWindow, Ext.Window, {});


CarContactWindow = function(grid,type,d,email) {
	this.grid=grid;
	
	if (this.grid.attach) {
		attach_names=[];  attach_ids=[];
		Ext.each(this.grid.attach, function(r){ attach_ids.push(r.data.id);  attach_names.push(r.data.name); });
		attach_names = attach_names.join('<br>');
		attach_ids = attach_ids.join(',');
	}	
	
	this.form = new Ext.form.FormPanel({id:'contactForm', labelWidth: 80, baseCls: 'x-plain',
		items: [{xtype:'panel',layout:'form', baseCls: 'x-plain', hidden:(type!='mail' || d),
			items:mailCombo = new MailCombo({value:(type=='mail'?email:''), allowBlank:(type!='mail')})//, {fieldLabel:'�� (���"�)', name:'email', xtype:'textfield', cls:'ltr', vtype:'email', width:150, allowBlank:(type!='mail')}
		},{layout:'column', baseCls: 'x-plain',	defaults: {xtype:'panel', columnWidth:0.5, layout:'form', baseCls: 'x-plain', border:false},
			items:[{bodyStyle:'padding:0 0 0 0',
				items: {fieldLabel:'����� ������', name:'back_date', xtype:'datefield', width: 115, format:'d/m/Y H:i', cls:'ltr', vtype:'daterange', allowBlank:true, value: (d ? d.json.back_date : '')}
			},{
				items: {fieldLabel:'�����', name:'date', xtype:'datefield', width: 115, format:'d/m/Y H:i', cls:'ltr', vtype:'daterange', value: (d ? d.json.date : new Date().format('d/m/Y H:i'))}
			}]
		}
		,{xtype:'combo', fieldLabel:'������', name:'importance', hiddenName:'importance', valueField:'id',  displayField:'val', width: 115, value: (d ? d.json.importance : 0), mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, allowBlank:false, store:[[0,'����'],[1,'����'],[2,'����']]}
		,{fieldLabel:'����', name:'them', xtype:'textfield', anchor:'-7', allowBlank:false }
		,{fieldLabel:(type=='mail' ? '�����' : '�����'), height:70, name:'notes', xtype:'textarea', cls:'rtl',  anchor:'-7', value:(d ? Ext.util.Format.stripTags(d.json.notes) :'') }
		,{name:'client_type', xtype:'hidden', value:this.grid.client_type}
		,{name:'link', xtype:'hidden', value:this.grid.link}
		,{name:'id', xtype:'hidden', value:(d ? d.json.contact_id : 0)}
		,{xtype:'panel', layout:'table', hidden:!this.grid.attach, layoutConfig:{columns:2}, baseCls:'x-plain', bodyStyle:'direction:rtl;',
			items: [
				 {xtype:'label', text:'����� �������:', cls:'label', width:92, style:'display:block; text-align:left; background:url(/crm/skin/icons/1/attach.gif) no-repeat right 1px;' }
				,{html:(this.grid.attach ? attach_names :''), style:'padding-right:5px;color:darkblue;font-weight:bold;', baseCls:'x-plain' }
			]
		},{name:'attach', xtype:'hidden', value:(this.grid.attach ? attach_ids :'')}
		]
	});
	
	if (d) this.form.find('name','them')[0].setValue(d.json.them);
	
	CarContactWindow.superclass.constructor.call(this, {
		title: (type=='mail' ? '����� ���"� <span style="font-weight:normal;font-size:10px">(���� ������� ����)</span>' : (d ? '����� �������' :'����� �������') ),
		layout: 'fit', modal:true, width: 460, closeAction:'close',	buttonAlign:'center', bodyStyle:'padding:5px;',	plain: true,
		items: this.form,
		buttons: [{text:(type=='mail' ? '���' : '����'), disabled:(d && (d.data.type=='mail' || d.data.type=='sms')), handler:function(){this.submitForm();}, scope:this},{text:'�����', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		this.form.form.submit({
			url: "/?m=crm/contacts_f&f=add_contact&id="+this.grid.id,
			waitTitle: '��� ����...', waitMsg: '����...', scope:this,
			success: function(f,a){
				this.close();
				var json = Ext.decode(a.response.responseText);
				if (this.grid.callBack) this.grid.callBack();
				else if (json.data && this.grid.store) this.grid.store.loadData(json);
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}	
};
Ext.extend(CarContactWindow, Ext.Window, {});


var CFG={
	// values in minutes
	 dayExtra: 8.5*60
	,dayExtraYoung: 8*60

	,maxDayExtra: 3*60
	// ,maxDayExtraYoung: 3*60

	,maxWeekHours: 55*60
	,maxYoungWeekHours: 40*60

	,youngFirstHour: 6*60
	,youngLastHour: 22*60
};



//Client Add-Edit Window
EmployeeWin = function(grid, d) {
	// if (d) this.id=d.id;
	// this.grid=grid;

	var p=1, e=1, a=1;
	var clientPhoto="skin/pict/man_icon.gif";
	
	var famStore = [[0,'-���-'],[1,'���'],[2,'���'],[3,'���'],[4,'���'],[5,'��'],[6,'��'],[7,'���']];
	var telStore = [[1,'���'],[2,'�����'],[3,'����'],[4,'��� ����'],[5,'��� ������'],[0,'���']];
	var adrStore = [[1,'���'],[2,'����/���'],[3,'�����'],[4,'������ ����'],[0,'���']];
	var emailStore=[[1,'���'],[2,'�����'],[0,'���']];
	
	this.addPhone = function() {
		if (p>4) return;
		Ext.getCmp('phoneSet'+p).show();
		this.syncShadow();
		p++;
	}

	this.addEmail = function() {if (e>2) return; Ext.getCmp('emailSet'+e).show(); this.syncShadow(); e++;}

	this.addAddress = function() {if (a>2) return; Ext.getCmp('adrSet'+a).show(); this.syncShadow(); a++;}
	
	// Dump(departments);
	this.departmentsStore = new Ext.data.SimpleStore({fields:['id','name'], data:[]});
	// this.departmentsStore = new Ext.data.ArrayStore({fields:['id','name']});
	
	this.selectBranch = function(combo, record, index){
		var branch_id=record.data.field1;
		if (branch_id) {
			this.departmentsStore.loadData(departments[branch_id]);
			Ext.getCmp('dep0cb').clearValue();  Ext.getCmp('dep0cb').enable();
			Ext.getCmp('dep1cb').clearValue();  Ext.getCmp('dep1cb').enable();
			Ext.getCmp('dep2cb').clearValue();  Ext.getCmp('dep2cb').enable();
		}else {
			this.departmentsStore.removeAll();
			Ext.getCmp('dep0cb').disable();
			Ext.getCmp('dep1cb').disable();
			Ext.getCmp('dep2cb').disable();
		}
	}
	
	
	this.alertsPanel=new Ext.Panel({border:false, bodyStyle:'color:#f40000; padding:2px;', html:'&nbsp;'});
	
	this.weekScheduleGrid=new WeekScheduleGrid({emp:{holiday:6, age:20}, open:true});
	this.weekScheduleGrid.on('alertsUpdate', function(){
		if (this.alertsPanel.body) this.alertsPanel.body.update(this.weekScheduleGrid.alerts);
		else this.alertsPanel.html=this.weekScheduleGrid.alerts;
	},this);

	
	this.tabs = new Ext.TabPanel({
		region:'center', baseCls:'x-plain', plain:true, activeTab:0,
		defaults: {xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'border:1px solid #8DB2E3; padding:5px'},
		items: [
			{title:'����', autoHeight:true,
				items: [{
					xtype:'panel', id:'private', layout:'form', baseCls:'x-plain',
					items:[
						{xtype:'fieldset', title:'����� ������', cls:'rtl', style:'padding:2px; margin-bottom:5px;', autoHeight:true, items:[
							{layout:'table', layoutConfig:{columns:6}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
								{html:'�� ����:'},{html:'�� �����:'},{html:'���� �.�:'},{html:'����� ����:'},{html:'���:'},{html:'��:'}
								,{name:'fname', id:'fname', xtype:'textfield', tabIndex:1, width:100, allowBlank:false}
								,{name:'lname', id:'lname', xtype:'textfield', tabIndex:2, width:100}
								,{name:'passport', id:'passport', xtype:'textfield', tabIndex:3, width:75, allowBlank:false}
								,{name:'birthday', tabIndex:4, xtype:'datefield', width:85, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:false, value: (d ? d.birthday : '')}
								,{hiddenName:'sex', xtype:'combo', tabIndex:5, width:60, value:0, store:[[0,'-���-'],[1,'���'],[2,'����']], mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
								,{hiddenName:'religion', xtype:'combo', tabIndex:6, width:70, value:0, store:[[0,'���'],[1,'�����'],[2,'�����'],[3,'������']], mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
							]}
						]}
						,{xtype:'fieldset', title:'�����', cls:'rtl', style:'margin-bottom:5px; padding:2px', autoHeight:true, items:[
							{layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
								{html:'����:'},{html:'�����:'},{html:'����� ����:'},{html:'����� 2:'},{html:'����� 3'}
								,{hiddenName:'branch_id', xtype:'combo', store:branches, width:100, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true
									,listeners:{select:this.selectBranch, scope:this}
								}
								,{name:'position', xtype:'textfield', width:100}
								,{hiddenName:'dep0', id:'dep0cb', disabled:true, xtype:'combo', width:100, store:this.departmentsStore, valueField:'id', displayField:'name', mode:'local', listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
								,{hiddenName:'dep1', id:'dep1cb', disabled:true, xtype:'combo', width:100, store:this.departmentsStore, valueField:'id', displayField:'name', mode:'local', listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
								,{hiddenName:'dep2', id:'dep2cb', disabled:true, xtype:'combo', width:100, store:this.departmentsStore, valueField:'id', displayField:'name', mode:'local', listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true}
							]}
							,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
								{html:'����� ����� �����:'}
								,{fieldLabel:'����� ����� �����', name:'work_start', xtype:'datefield', width:80, format:'d/m/Y', cls:'ltr', vtype:'daterange', allowBlank:true}
							]}
						]}
					]
				}
				
				,{layout:'column', baseCls:'x-plain', defaults:{xtype:'fieldset', columnWidth:0.5}, items:[
					{title: '�����/���', style:'margin-left:5px; padding:2px;', cls: 'rtl', width: 150, autoHeight:true, items:[
						{layout:'table', layoutConfig: {columns:3}, baseCls: 'x-plain', id:'phoneSet0', items:[
							{xtype:'combo', hiddenName:'tel_type0', width: 90, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel0', xtype:'textfield', cls:'ltr', width: 90},
							{baseCls: 'x-plain', html:'<img src="skin/icons/add_call.png" onClick="Ext.getCmp(\'employeeWin\').addPhone()" ext:qtip="���� ���� �����">'}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet1', items:[
							{xtype:'combo', hiddenName:'tel_type1', width: 90, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel1', xtype:'textfield', cls:'ltr', width:90}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet2', items:[
							{xtype:'combo', hiddenName:'tel_type2', width: 90, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel2', xtype:'textfield', cls:'ltr', width:90}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet3', items:[
							{xtype:'combo', hiddenName:'tel_type3', width: 90, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
							{name:'tel3', xtype:'textfield', cls:'ltr', width:90}
						]}
						,{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'phoneSet4', items:[
								{xtype:'combo', hiddenName:'tel_type4', width: 90, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:telStore},
								{name:'tel4', xtype:'textfield', cls:'ltr', width:90}
						]}
					]}
					,{title:'���"�', style:'margin-right:5px; padding:2px;', cls: 'rtl', autoHeight:true, items:[
						{layout:'table', layoutConfig: {columns:3}, baseCls: 'x-plain', id:'emailSet0', items:[
							{html:'�����:', width: 60, baseCls: 'x-plain', cls: 'rtl'},
							{name:'email', xtype:'textfield', cls:'ltr', width: 123, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/},
							{baseCls:'x-plain', html:'<img src="skin/icons/add_email.png" onClick="Ext.getCmp(\'employeeWin\').addEmail()" ext:qtip=\'���� ����� ���"�\'>'}
						]},{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'emailSet1', items:[
							{xtype:'combo', hiddenName:'email_type1', width: 60, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore},
							{name:'email1', xtype:'textfield', cls:'ltr', width:123, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						]},{layout:'table', layoutConfig:{columns:2}, baseCls:'x-plain', id:'emailSet2', items:[
							{xtype:'combo', hiddenName:'email_type2', width: 60, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:emailStore},
							{name:'email2', xtype:'textfield', cls:'ltr', width:123, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/}
						]}
					]}
				]}
				
				,{xtype:'fieldset', title:'�����', cls:'rtl', style:'margin-bottom:5px; margin-top:3px; padding:2px', autoHeight:true,
					items:[
						{layout:'table', layoutConfig:{columns:5}, baseCls:'x-plain', id:'adr0', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{html:'���:'},{html:'���:'},{html:'�����:'},{html:'�����/�.�.:', colspan: 2},
							{xtype:'combo', hiddenName:'adr_type0', width:100, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city0', xtype:'textfield', width:80},
							{name:'zip0', xtype:'textfield', width:50},
							{name:'adr0', xtype:'textfield', width:175},
							{baseCls: 'x-plain', html:'&nbsp;<img src="skin/icons/add_addr.png" onClick="Ext.getCmp(\'employeeWin\').addAddress()" ext:qtip=\'���� �����\'>'}
						]}
						,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet1', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'combo', hiddenName:'adr_type1', width:100, value: 1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city1', xtype:'textfield', width:80},
							{name:'zip1', xtype:'textfield', width:50},
							{name:'adr1', xtype:'textfield', width:175}
						]}
						,{layout:'table', layoutConfig:{columns:4}, baseCls:'x-plain', id:'adrSet2', defaults: {baseCls:'x-plain', cls:'rtl'}, items:[
							{xtype:'combo', hiddenName:'adr_type2', width:100, value:1, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true, store:adrStore},
							{name:'city2', xtype:'textfield', width:80},
							{name:'zip2', xtype:'textfield', width:50},
							{name:'adr2', xtype:'textfield', width:175}
						]}
					]
				}
				,{xtype:'fieldset', title:'�����', labelAlign:'top', cls:'rtl', autoHeight:true, style:'padding:0 3px;',
					items:{xtype:'textarea', name:'notes', hideLabel:true, width:'100%', height:29, grow:true, growMin:18, growMax:100, growAppend:''}
				}
				]
			}
			,{title:'����� ������', autoHeight:true, items:[
				{xtype:'panel', layout:'column', baseCls: 'x-plain', defaults:{xtype:'panel', layout:'form', baseCls:'x-plain', bodyStyle:'padding-top:5px', border:false},
					items:[{
						columnWidth:0.55, labelWidth:90,
						items:[
							{fieldLabel:'��� �������', name:'url', xtype:'textfield', width:120, cls:'ltr' },
							{fieldLabel:'ICQ', name:'icq', xtype:'textfield', width:120, cls:'ltr'},
							{fieldLabel:'Skype', name:'skype', xtype:'textfield', width:120, cls:'ltr'},
							{fieldLabel:'Messenger', name:'msn', xtype:'textfield', width:120, cls:'ltr'},
							{fieldLabel:'FaceBook', name:'facebook', xtype:'textfield', width:120, cls:'ltr'}
						]},{
						columnWidth:0.45, labelWidth:32, hideMode:'visibility',
						items:[
							{xtype:'panel', baseCls:'x-plain', cls:'rtl', html:'<img id=clientPhoto src="'+clientPhoto+'" align="left">����� ����: ', listeners:{render:function(){window.setTimeout("$('clientPhoto').src='"+clientPhoto+"';",100);}}}
							,{xtype:'fileuploadfield', name:'photo', emptyText:'��� ����...', fieldLabel:'����',labelWidth: 60, width: 160}
						]
					}]
				}
			]}
			
			,{title:'���� ����� ������', autoHeight:true, items:[
				this.weekScheduleGrid
				,{html:'<div class=x-grid3-header style="margin-top:3px; padding:2px 5px;"><b>������:</b></div>', baseCls:'x-plain', border:false}
				,this.alertsPanel
				,{xtype:'hidden', name:'weekSchedule'}
			]}
		]
	});
	
	
	this.tabs.on('tabchange', function(tabPanel, tab) { this.tabs.doLayout(); this.syncShadow(); }, this);
	
	this.form = new Ext.form.FormPanel({
		id:'clientForm', fileUpload:true, labelWidth:75, autoHeight:true, baseCls:'x-plain',
		items:this.tabs
	});
	
	this.form.find('name','birthday')[0].on('change', function(fld, birthday){ // update isYoung
		var now=new Date();
		this.weekScheduleGrid.isYoung = (now.add(Date.YEAR,-18).format('Ymd') < birthday.format('Ymd'));
	},this);
	
	// this.form.find('hiddenName','religion')[0].on('change', function(fld, religion){ // update holiday
		// this.weekScheduleGrid.holiday = religion==2 ? 0 : (religion==3 ? 5 : 6);
	// },this);
	
	
	EmployeeWin.superclass.constructor.call(this, {
		title:(d ? '������ ���� ����' : '����� ���� ���'),
		layout:'fit', id:'employeeWin', modal:true,
		width:555, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:0 5px;', plain:true, border:false,
		items:this.form,
		buttons:[{text:'����', handler:function(){this.submitForm();}, scope:this},{text:'�����', handler:function(){this.close();}, scope:this}]
	});
	
	this.on('show', function(){
		Ext.getCmp('phoneSet1').hide(); Ext.getCmp('phoneSet2').hide(); Ext.getCmp('phoneSet3').hide(); Ext.getCmp('phoneSet4').hide();
		Ext.getCmp('emailSet1').hide(); Ext.getCmp('emailSet2').hide();
		Ext.getCmp('adrSet1').hide(); Ext.getCmp('adrSet2').hide();
		// Ext.getCmp('business').hide();
		
		// Dump(d);
		if (d.branch_id) {
			Ext.getCmp('dep0cb').enable(); Ext.getCmp('dep1cb').enable(); Ext.getCmp('dep2cb').enable();
			this.selectBranch('', {data:{field1:d.branch_id}});
		}
		if (d.id) this.formLoad();
		// else if(d) this.form.form.setValues(d);
		this.syncShadow();
	});
	
	this.formLoad=function() {
		this.getEl().mask('...����');
		this.form.load({
			url:"/?m=employees/employees_f&f=load_data&id="+d.id,
			reader: new Ext.data.JsonReader({root:'data', totalProperty:'total', successProperty:'success'}),
			success: function(f,a){
				var d=a.result.data;
				for (var n=1; n<5; n++) if (d['tel'+n]) Ext.getCmp('phoneSet'+n).show();
				for (var n=1; n<3; n++) if (d['email'+n]) Ext.getCmp('emailSet'+n).show();
				for (var n=1; n<3; n++) if (d['city'+n] || d['zip'+n]!=0 || d['adr'+n]) Ext.getCmp('adrSet'+n).show();
				if (d['photo']) clientPhoto=d['photo'];
				
				if (d.weekSchedule) {
					this.weekScheduleGrid.restoreData( Ext.decode(d.weekSchedule) );
					// try {
						// Dump( Ext.decode(d.weekSchedule) );
					// } catch(e){alert('Error decoding weekSchedule')}
					// Ext.decode(d.weekSchedule);
					
				}
				
				
				this.syncShadow();
				this.getEl().unmask();
			},
			failure: function(){ this.getEl().unmask(); },
			scope:this
	   	});
	}

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		this.find('name','weekSchedule')[0].setValue( Ext.encode(this.weekScheduleGrid.d) );
		
		this.form.form.submit({
			url: "/?m=employees/employees_f&f=edit&id="+d.id,
			waitTitle:'��� ����...',
			waitMsg:'����...',
			scope:this,
			success: function(f,a){
				this.close();
				var data = Ext.decode(a.response.responseText);
				if (!d.id && grid.openEmployee) grid.openEmployee(data);
				else if (grid.store) grid.store.reload();
			},
			failure: function(r,o){ this.close(); ajRqErrors(o.result);}
		});
	};
	
	
	function get_age(birthday){
		if (!birthday || birthday=='00/00/0000') return '';
		var b=Date.parseDate(birthday,'d/m/Y');
		var now=new Date();
		var age = now.getFullYear() - b.getFullYear();
		if (now.format('md')<b.format('md')) age--;
		return age;
	}

};
Ext.extend(EmployeeWin, Ext.Window, {});


EmployeeTab = function(d) {
	this.d=d;
	this.caller='client_info';
	
	this.employeeWin = function(){var win=new EmployeeWin(this,this.d); win.show(); win.center();};
	
	this.onClientChange = function(d) {
		d.photo="skin/pict/man_icon.gif";
		if (d.sex==2) {d.title='��'; d.icon='user-female'; d.job_title='�����'; d.work_title='���� �����'}
		else {d.title='��'; d.icon='user'; d.job_title='�����'; d.work_title='���� �����'}
		// if (d.sex==3) {d.title='�� ���'; d.icon='user_business'; d.job_title="��' ������"; d.work_title='����'};
		Ext.getCmp('clientData_'+d.id).tpl.overwrite(Ext.getCmp('clientData_'+d.id).scope.body, d);
	};
	
	this.contactsList= new ClientContacts({id:this.d.id, client_type:'client', clientCell:this.d.clientCell}),
	this.contactsList.on('activate', function(tabPanel, tab) {this.contactsList.store.load();},this);

	this.filesList 	= new ClientFiles({id:this.d.id,client_type:'client'})
	this.filesList.on('activate', function(tabPanel, tab) {this.filesList.store.load();},this);

	this.clientInfo = {
		xtype:'panel', style:'border-bottom: 2px solid white; background-color:#f3f8fc;',
		id:'clientData_'+this.d.id,
		region:'north',	baseCls: 'x-plain',
		split:true,	height:90, border:true,
		initComponent: function() {
			this.tpl = new Ext.Template(
				'<table border=1 width=100% id=client_info dir=rtl class=client_info style="position:absolute; direction:rtl; "><tr>',
				'<td nowrap><img src="skin/icons/1/{icon}.gif" align=absmiddle> <b>{title}:</b></td><td width=30%><b>{name}</b></td>',
				'<td nowrap><img src="skin/icons/1/zehut.gif" align=absmiddle> <b>�.�./�.�:</b></td><td width=10% nowrap>{passport}</td><td nowrap><b>����� �����:</b> {license}</td>',
				'<td nowrap><img src="skin/icons/1/date.gif" align=absmiddle> <b>�.����:</b></td><td width=15%>{birthday}</td>',
				'<td rowspan=4><img id=client_photo src="{photo}"></td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/telephone.gif" align=absmiddle> <b>�������:</b></td><td width=30%>{tel_txt}</td>',
				'<td nowrap><img src="skin/icons/1/email.gif" align=absmiddle> <b>���"�:</b></td><td width=30% colspan=2><a href=# onClick="sendEmail({id},\'{email}\');">{email}</a></td>',
				'<td nowrap width=15%><b>{job_title}:</b></td><td>{position}</td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/house.gif" align=absmiddle> <b>�����:</b></td><td width=30%>{adr}</td>',
				'<td nowrap><img src="skin/icons/1/money.png" border=0 align=absmiddle> <b>���� ���:</b></td><td width=30% colspan=2><span style="color:red;" dir=ltr><b>{balance:ilMoney}</b></span></td>',
				'<td nowrap width=15%><b>{work_title}:</b></td><td>{work}</td></tr>',
				'<tr><td nowrap><img src="skin/icons/1/note.gif" align=absmiddle><b>�����:</b></td><td colspan=6>{notes}</td></tr></table>'
			);
			EmployeeTab.superclass.initComponent.call(this);
		},
		scope:this
	};
	
	this.tabs = new Ext.TabPanel({
			activeTab:0, baseCls:'x-plain', style:'border-bottom: 1px solid #8DB2E3;', region:'center',
			defaults: {xtype:'panel', layout:'form', baseCls: 'x-plain', style:'padding-top:10px'},
			items:[this.contactsList,this.filesList]
		});

	EmployeeTab.superclass.constructor.call(this, {
		id: 'emptab_'+this.d.id,
		iconCls: (this.d.sex==2 ? 'client-female' :(this.d.sex==3 ? 'client-business' :'client')),
		title: this.d.name,
		closable:true, autoScroll:true, layout:'border', 
		items: [this.clientInfo,this.tabs]
		,tbar: [ 
			{text:'���� ���� ����', iconCls:'edit', handler:this.employeeWin, scope:this}
			,'-' ,{text:'SMS ���', iconCls:'send_sms', handler:function(){this.contactsList.smsWindow();}, scope:this}
		]

    });
	this.on('render',function(){this.onClientChange(this.d);},this);
};
Ext.extend(EmployeeTab, Ext.Panel);

EmployeesList = function() {
	this.store = new Ext.data.JsonStore({
		url:'/?m=employees/employees_f&f=list', totalProperty:'total', root:'data',
		fields:['id','branch_id','name','passport','tel','tel_txt','fax','adr','email','birthday','notes','c_num','sex'],
		sortInfo:{field:'name', direction:'ASC'},
		remoteSort:true
	});

	this.employeeWin = function(d){var win=new EmployeeWin(this,d); win.show(); win.center();}

	// this.expander = new Ext.grid.RowExpander({
		// tpl: new Ext.Template(
			// '<div dir=rtl><table border=1 width=100% style="table-layout:auto; border-collapse:collapse" bordercolor=white bgcolor=#f3f8fc><tr>',
			// '<td nowrap><img src="skin/icons/1/email.gif" align=absmiddle> <b>����:</b></td><td width=30%><a href="mailto:{email:htmlEncode}">{email:htmlEncode}</a></td>',
			// '<td nowrap><img src="skin/icons/1/comments.gif" align=absmiddle> <b>���������:</b></td><td colspan=2 width=30%>{c_num}</td>',
			// '</tr><tr>',
			// '<td nowrap><img src="skin/icons/1/telephone.gif" align=absmiddle> <b>�������:</b></td><td width=30%>{tel_txt:htmlEncode}</td>',
			// '<td nowrap><img src="skin/icons/car.gif" align=absmiddle> <b></b></td><td colspan=2 width=30%></td>',
			// '</tr><tr>',
			// '<td nowrap><img src="skin/icons/1/house.gif" align=absmiddle> <b>�����:</b></td><td width=30%>{adr}</td>',
			// '<td nowrap><img src="skin/icons/1/folder_page.gif" align=absmiddle> <b>������:</b></td><td width=15%></td>',
			// '</tr><tr>',
			// '<td nowrap><img src="skin/icons/1/note.gif" align=absmiddle> <b>�����:</b></td><td colspan=5>{notes:htmlEncode}</td>',
			// '</tr></table></div>'
		// )
	// });

	this.searchField = new GridSearchField({store:this.store, width:150});

	this.store.on('load', function(){	// check for errors
			var callback=function() { this.store.load(); };
			ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		}, this);
		
	this.hlStr = hlStr;
	
	this.nameRenderer = function(str, p, record){
		return "<span style='display:block;height:16px;padding-right:18px;background:url(/skin/icons/1/user"+(record.json.sex==2?'-female':(record.json.sex==3?'_business':''))+".gif) no-repeat right top;'>" +this.hlStr(str)+ "</span>";
	}	
	
    this.columns = [
		{dataIndex:'adr', id:'adr', header:'�����', sortable:true, renderer:this.hlStr.createDelegate(this) },
		{dataIndex:'passport', header:'�.�./�.�.', width:70, sortable:true, renderer:this.hlStr.createDelegate(this) },
		{dataIndex:'fax', header:'���', width:80, sortable:true, renderer:this.hlStr.createDelegate(this) }, 
		{dataIndex:'tel', header:'�������', width:140, renderer:this.hlStr.createDelegate(this) }, 
		{dataIndex:'name', header:'��', width:200, sortable:true, renderer:this.nameRenderer.createDelegate(this) }
		// ,this.expander
	];
	
	
	
	// Dump(branches);
	// Dump(departments);
	var deps;
	this.branchDepStore=[[0,'-- �� ������� --']];
	for (var b=0; b<branches.length; b++) {
		this.branchDepStore.push([branches[b][0], '<b>'+branches[b][1]+'</b>']);
		deps=departments[branches[b][0]];
		if (deps) for (var i=0; i<deps.length; i++) this.branchDepStore.push([branches[b][0]+':'+deps[i][0], '&nbsp; &nbsp; '+deps[i][1]+'']);
	}
	// Dump(this.branchDepStore);
	this.filters={};
	this.filters.branch_id=0;
	this.filters.dep_id=0;
	
	this.branchDepCombo=new Ext.ux.HtmlCombo({
		name:'branchDepId', width:150, itemCls:'rtl', 
		store:new Ext.data.SimpleStore({fields:['id', 'name'], data:this.branchDepStore}),
		value:0, valueField:'id', displayField:'name',
		listeners:{
			select:function(cb,rec){this.store.load()}, scope:this
		}
	});

	//{xtype:'combo', store:[['','�� �������']].concat(branches), id:'branchFilter', width:120, listClass:'rtl', triggerAction:'all', editable:false, forceSelection:true, value:'', listeners:{select:function(f){this.store.load()},scope:this} }
	
	EmployeesList.superclass.constructor.call(this, {
		title:'����� ������',iconCls:'grid',
		enableColLock:false,loadMask:true,stripeRows:true,enableHdMenu: false,
		plugins: this.expander,
		autoExpandColumn: 'adr',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'�����:', this.searchField
			,'-' , '����/�����:', this.branchDepCombo
			,'-' , new Ext.form.ComboBox({store:[[0,'������'],[1,'�������']], id:'deletedFilter', value:0, width:80, listClass:'rtl', triggerAction:'all', forceSelection:true, 
				listeners:{
					select:function(f){
						this.store.reload();
						if (f.getValue()==1) {
							this.getTopToolbar().items.get('user_del').hide();
							this.getTopToolbar().items.get('user_restore').show();
						}else {
							this.getTopToolbar().items.get('user_del').show();
							this.getTopToolbar().items.get('user_restore').hide();
						}
					}, 
					change:function(f){if(f.getRawValue()==''){f.setValue(0);this.store.reload();} },
					scope:this
				}
			})
			,'-' ,{ text:'���� ���', iconCls:'user_add', handler:function(){this.employeeWin({})}, scope:this }
			,'-' ,{ text:'���� ����', id:'user_edit', iconCls:'edit', handler:function(){this.employeeWin(this.getSelectionModel().getSelected().data);}, disabled:true, scope:this }
			,'-' ,{ text:'��� ����', id:'user_del', iconCls:'delete', handler:function(){this.delClientPromt();}, disabled:true, scope:this }
				 ,{ text:'���� ����', id:'user_restore', iconCls:'restore', handler:function(){this.restoreClientPromt();}, disabled:true, hidden:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
    });

	this.store.on('beforeload', function(store, options){
		this.store.removeAll();
		var setParams = {
			q:this.searchField.getRawValue().trim()
			,deleted:Ext.getCmp('deletedFilter').getValue()
			,branchDepId:this.branchDepCombo.getValue()
		}
		Ext.apply(store.baseParams, setParams);
		Ext.apply(options.params, setParams);
	}, this);
	
	// this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('user_edit').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('user_del').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('user_restore').setDisabled(!sm.getCount());
	}, this);
	
	// open client in new tab
	this.openEmployee = function(d){
		var tab;
		if(!(tab=Tabs.getItem('emptab_'+d.id))){ tab=new EmployeeTab(d); Tabs.add(tab); }
		Tabs.setActiveTab(tab);
		// Ext.Ajax.request({
			// url:'/?m=employees/employees_f&f=load_data&id='+d.id,
			// success: function(r,o){
				// var dat=Ext.decode(r.responseText);
				// if (!ajRqErrors(dat)) {
					// var tab;
					// d.balance=dat.balance;
					// if(!(tab=Tabs.getItem('emptab_'+d.id))){ tab=new EmployeeTab(d); Tabs.add(tab); }
					// Tabs.setActiveTab(tab);
				// }
			// },
			// scope: this
		// });
	};

	// doubleClick
	this.on('rowdblclick', function(grid, row, e){
		// if (e.getTarget().tagName.toLowerCase()=='button') return;
		// var data = grid.getSelectionModel().getSelected().data;
		// this.openEmployee(data);
	});
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row).data;
		
		this.delRow=row;
		this.menu = new Ext.menu.Menu({
			items: [
				// {text:'��� ����', iconCls:'user_open', scope:this, handler:function(){this.openEmployee(this.srData);} }
				{text:'���� ����', iconCls:'user_edit', scope:this, handler:function(){this.employeeWin(this.srData);} }
				// ,( (Ext.getCmp('search_team_id').getValue()!='deleted') ? {text:'��� ���� ���� ���� ��������', iconCls:'key', scope:this, handler:function(){set_password(this.srData)}} : '' )
				// ,( (Ext.getCmp('search_team_id').getValue()=='deleted') ? {text:'���� ����', iconCls:'user_restore', scope:this, handler:this.restoreClientPromt} : {text:'��� ����', iconCls:'user_del', scope:this, handler:this.delClientPromt} )
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.delClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;

		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:250, title:'����� ����?', msg:'<img src="skin/icons/1/user_del48.gif" align=left><br>��� ������ ���� �� �����<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delClient(records); }
		});
	}
	
	this.delClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...����');
		Ext.Ajax.request({
			url: '/?m=employees/employees_f&f=delete&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('���� ����','���� <b>'+d.data.name+'</b> ���� ������');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};

	this.restoreClientPromt = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.name);});
		
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'����� ����?', msg:'��� ������ ����� �� �����<br><b>'+names.join(',<br>')+'</b> ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.restoreClient(records); }
		});
	}
	
	this.restoreClient = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...����');
		Ext.Ajax.request({
			url: '/?m=employees/employees_f&f=restore&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid green';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected()); topMsg.msg('���� �����','���� <b>'+d.data.name+'</b> ����� ������');}, scope:this });
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};
	
	this.on('render', function(){ this.store.load() });
};
Ext.extend(EmployeesList, Ext.grid.GridPanel);


var Tabs = new Ext.TabPanel({region:'center', activeTab:0, border:false, bodyStyle:'border-right-width:1px', defaults:{autoScroll:true}, enableTabScroll:true, items:new EmployeesList()});