textField = Ext.extend(Ext.form.TextField, {allowBlank:false, listeners:{ show: function(el){el.focus(true,100);}}});

var Record = Ext.data.Record.create(['id','name']);

WareunitsGrid= function(d) {
	this.saveClientgroup = function(rec){
		Ext.Ajax.request({
			url:'?m=config/config&f=add_wareunit',
			params:rec.data,
			success: function(r,o){
				var d=Ext.decode(r.responseText);
				rec.set('id',d.id);
			},
			failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
		return true;
	}
	
	this.gridAfterEdit = function(e){
		if (this.saveClientgroup(e.record)) {
			if ((this.store.getCount()-this.store.indexOf(e.record))==1) {//If last record
				e.grid.store.insert(this.store.getCount(), new Record());
				e.grid.startEditing(this.store.getCount()-1,0); 
			}
		}
	}

	this.store = new Ext.data.JsonStore({
		url:'?m=config/config&f=wareunits_list',
		totalProperty:'total', root:'data',
		fields: ['id','name']
	});
	
	this.store.on('beforeload', function(store, options){this.store.removeAll();}, this);
	
	WareunitsGrid.superclass.constructor.call(this, {
	    store:this.store, stripeRows:true, autoExpandColumn:'name', iconCls:'plus',
		enableColumnResize:false, clicksToEdit:2,
	    columns:[{header:'שם יחידה', dataIndex:'name', id:'name', editor: textField}]
		,tbar: [ 
			{text:'הוסף יחידה', iconCls:'add', scope:this, handler:function(){
				this.store.insert(this.store.getCount(), new Record());
				this.startEditing(this.store.getCount()-1,0);
			}}
			,{text:'מחק יחידה', iconCls:'delete', disabled:true, scope:this, handler:function(){this.delRow();}}
		]
		,bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})
		,initComponent: function() {WareunitsGrid.superclass.initComponent.call(this);}
	});
	
	this.on('afteredit', this.gridAfterEdit);
	
	this.getSelectionModel().on('selectionchange', function(sm) {
		this.getTopToolbar().items.items[1].setDisabled(false);
	}, this);
	
	this.delRow = function(){
		if (this.selModel.selection.record.data.id) {
			Ext.MessageBox.show({
				width:250, title:'למחוק פריט?', msg:'האם ברצונך למחוק יחידה זאת ?', buttons:Ext.MessageBox.YESNO, scope:this,
				fn: function(btn) {if(btn=='yes') {
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=config/config&f=del_wareunit&id='+this.getSelectionModel().selection.record.data.id,
						success: function(r,o){
							r=Ext.decode(r.responseText);
							if (!ajRqErrors(r)) {
								// Dump(this.selModel.selection);
								var index = this.selModel.selection.cell[0];
								this.view.getRow(index).style.border='1px solid red';
								Ext.fly(this.view.getRow(index)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.selection.record);}, scope:this });
							}
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				}}
			});
		}
	};
	
	this.store.load();
};

Ext.extend(WareunitsGrid, Ext.grid.EditorGridPanel);

Ms['config/wareunits'].run = function(cfg){
	var m=Ms['config/wareunits'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var  wareunitsGrid = new WareunitsGrid();
		win = Crm.desktop.createWindow({width:300, height:400
			,items:wareunitsGrid
		}, m);
	}
		
	win.on('show', function() {
		if (wareunitsGrid.store.getCount()>0) wareunitsGrid.startEditing(wareunitsGrid.store.getCount()-1,0);
	});	
	win.show();
};
