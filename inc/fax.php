<?

//————————————————————————————————————————————————————————————————————————————————————
function curl_post_files($url, $params, $files) {
	set_time_limit(180);
	
	foreach ($params as $key=>$val) if (is_array($val)) $params[$key] = implode(',', $val);
	
	$boundary = md5(time());
	$content[] = '--'.$boundary;
	
	foreach ($params as $key => $val) $content[] = 'Content-Disposition: form-data; name="'.$key.'"'."\r\n\r\n". $val ."\r\n--". $boundary;
	
	if ($files) foreach($files as $name=>$file) {
		// if (!isset($file['content']) AND is_file($file['filename'])) $file['content']=file_get_contents($file['filename']);
		if (!$file['filename']) $file['filename']=$name;
		$content[]='Content-Disposition: form-data; name="'.$name.'"; filename="'.$file['filename'].'"'."\r\n"
				  ."Content-Type: application/octet-stream\r\n\r\n"	// ."Content-Type: image/$type\r\n\r\n"
				  ."$file[content]\r\n--$boundary";
				  // .file_get_contents($file['content'])."\r\n--$boundary";
	}
	
	$content[]=array_pop($content).'--';
	$content = implode("\r\n", $content);
	
	$header[]='Content-Type: multipart/form-data; boundary='.$boundary;
	$header[]='Content-Length: '.strlen($content);

	$ch = curl_init($url);
	// $options=array(CURLOPT_TIMEOUT=>120, CURLOPT_POST=>1, CURLOPT_POSTFIELDS=>$post, CURLOPT_RETURNTRANSFER=>1, CURLOPT_HTTPHEADER=>$header, CURLOPT_SSL_VERIFYPEER=>0, CURLOPT_SSL_VERIFYHOST=>0);
	// curl_setopt_array($ch, $options);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	
	$result = curl_exec($ch);
	curl_close($ch);
 return $result;
}


//————————————————————————————————————————————————————————————————————————————————————
function get_fax_credits($user=null) {
 global $SVARS;
	if (!$SVARS['user']['id']) return 'No permission';
	if (!$SVARS['user']['fax_active']) return "לקבלת שרות SMS התקשרו: 074-7144333";	
	$fax_credits=sql2array("SELECT fax_credits FROM clients WHERE id=$SVARS[cid]",0,'fax_credits',1,'Rafael');
 return $fax_credits;
}


//————————————————————————————————————————————————————————————————————————————————————
function send_fax($fax_id, $recipient_fax, $file, &$coverpage='') {
 global $SVARS, $CFG, $group_id;
	$recipient_fax=ereg_replace('[^0-9]','',$recipient_fax);
	if (strlen($recipient_fax)<3) return 'Bad fax number';
	if (!$file AND !$coverpage) return 'nothing to send';
	if ($file AND !is_file($file)) return 'fax file not found';

	# !! set unique identifier for each site
	$params['identifier']="crm#$fax_id";
	if ($_SERVER['HTTP_HOST']=='crm.loc') $params['identifier']="10.0.0.1#$fax_id"; #(local for tests)
	
	$params['recipient_fax']=$recipient_fax;
	if ($file) $files['file']['content']=file_get_contents($file);
	if ($coverpage) $files['coverpage']['content']=$coverpage;
	
	if ($_SERVER['HTTP_HOST']=='crm.loc') $url='https://10.0.0.250/_fax/sendfax.php?a=sendfax'; # from office
	else $url='https://62.0.100.146:1443/_fax/sendfax.php?a=sendfax';		# b-com.openvoice.co.il
	// else $url='https://212.199.234.206/_fax/sendfax.php?a=sendfax';		# b-com.openvoice.co.il
	
	# send request to fax server
	$response = curl_post_files($url, $params, $files);
	if ($response===false) return "Can't connect to fax server";
	// echo $response;
	
	# get jobid
	eregi('jobid:([0-9]+)', $response, $regs);
	$jobid=$regs[1];
	if (!$jobid) return 'no jobid'.($SVARS['is_admin']? ", response:$response" :'');
	
 return "OK#$jobid";
}


//————————————————————————————————————————————————————————————————————————————————————
function fax_cover_html($d) {
	if ($d['recipient_name'] . $d['sender_name'] . $d['sender_fax'] . $d['sender_tel']) {
		$html='<table>';
		$html.='<tr><td style="width:13mm; font-size:9pt">תאריך:</td><td style="font-size:9pt">'. date('d/m/Y H:i') .'</td></tr>';
		$html.='<tr><td>&nbsp;</td></tr>';
		if ($d['recipient_name']) $html.='<tr><td style="width:13mm;">לכבוד:</td><td><b>'. htmlspecialchars($d['recipient_name']) .'</b></td></tr>';
		if ($d['recipient_fax']) $html.='<tr><td style="width:13mm">פקס:</td><td>'. $d['recipient_fax'] .'</td></tr>';
		$html.='<tr><td>&nbsp;</td></tr>';
		$html.='<tr><td style="width:13mm; font-size:10pt">מאת:</td><td style="font-size:10pt">'. htmlspecialchars($d['sender_name']) .'</td></tr>';
		if ($d['sender_fax']) $html.='<tr><td style="width:13mm; font-size:10pt">פקס:</td><td style="font-size:10pt">'. htmlspecialchars($d['sender_fax']) .'</td></tr>';
		if ($d['sender_tel']) $html.='<tr><td style="width:13mm; font-size:10pt">טלפון:</td><td style="font-size:10pt">'. htmlspecialchars($d['sender_tel']) .'</td></tr>';
		$html.='</table><br/>';
	}elseif ($d['logo']) $html.="<div>&nbsp;</div><br/><br/><br/><br/><br/><br/>";
	 
	if ($d['subject']) $html.='<div align="center"><u>הנדון:</u> <b>'.htmlspecialchars($d['subject']).'</b></div><br/>';
	else $html.='<br/>';
	if ($d['text'])    $html.=$d['text'];
	// echo htmlspecialchars($html); exit;
 return $html;
}


//————————————————————————————————————————————————————————————————————————————————————
function fax_cover_pdf($d) {
 global $SVARS, $group_id;
	$html=fax_cover_html($d);
	$pdf=html2pdf(&$html, $d['logo']);
 return $pdf;
}


//————————————————————————————————————————————————————————————————————————————————————
function html2pdf($html, $logo) {
 global $SVARS, $group_id;
	require_once('../inc/phpexcel/Classes/PHPExcel/Shared/PDF/tcpdf.php');
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 

	$l['a_meta_dir'] = 'rtl';
	$pdf->setLanguageArray($l); 
	$pdf->SetMargins(15, 7, 15);
	$pdf->SetAutoPageBreak(false, 0);
	$pdf->SetFont('arial', '', 12);
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	$pdf->SetCreator('B.A.F.I. - Best Advice for Insurance');

	$html=str_replace('&nbsp;', ' ', $html);
	$html=eregi_replace(' face="[^"]*"','', $html);
	$html=eregi_replace('font color="?#?([0-9A-Z]+)"? size="?([0-9]+)"?', 'font style="color:#\\1; font-size:\\28px;"', $html);
	$html=eregi_replace('font size="?1"?','font style="font-size:20px"', $html);
	$html=eregi_replace('font size="?2"?','font style="font-size:28px"', $html);
	$html=eregi_replace('font size="?3"?','font style="font-size:38px"', $html);
	$html=eregi_replace('font size="?4"?','font style="font-size:48px"', $html);
	$html=eregi_replace('font size="?5"?','font style="font-size:58px"', $html);
	$html=eregi_replace('font size="?6"?','font style="font-size:68px"', $html);
	$html=eregi_replace('font size="?7"?','font style="font-size:96px"', $html);
	$html=eregi_replace('font color="?#?([0-9A-Z]+)"?', 'font style="color:#\\1"', $html);
	$html=eregi_replace('<p', '<div', $html);
	$html=eregi_replace('</p>', '</div>', $html);
	$html=eregi_replace('<br>','<br/>', $html);


	if ($logo) {
		$user_logo = glob("files/users_logos/$group_id.*");
		if ($user_logo[0]) {
			$user_logo = $user_logo[0];
			list($w, $h) = getimagesize($user_logo);
			$img_w=160; $img_h=30;
			$ratio_orig = $w/$h;
			if ($img_w/$img_h > $ratio_orig) $img_w = round( $img_h*$ratio_orig );
			else $img_h = round( $img_w/$ratio_orig );
		}
	}
	
	// if (eregi('{[a-z]+}',$html)) $html=fax_coverReplaceVars($c, $html);
	

	$pdf->AddPage(); 
	if ($user_logo) $pdf->Image($user_logo,($ratio_orig>3 ? 105-$img_w/2 : 200-$img_w),8, $img_w, $img_h);
	if ($user_logo AND $ratio_orig>3) $html="<br/><br/><br/><br/><br/><br/>".$html;
	$pdf->writeHTML($html);
	
	// $resolution = array($pdf->getPageWidth(), $pdf->GetY()+3);
	// $pdf->deletePage(0);
	
	// $pdf->AddPage('P', $resolution);
	// if ($user_logo) $pdf->Image($user_logo,($ratio_orig>3 ? 105-$img_w/2 : 200-$img_w),8, $img_w, $img_h);
	// $pdf->writeHTML( iconv("windows-1255", "UTF-8", $html) );
 return $pdf;
}



/*
//————————————————————————————————————————————————————————————————————————————————————
function fax_coverReplaceVars($c, $html) {
	$html=str_replace('{clientName}', $c['client_name'], $html);
	$html=str_replace('{clientAddress}', $c['client_addresses'], $html);
	$html=str_replace('{clientCity}', $c['city0'], $html);
	$html=str_replace('{clientAdr}', $c['adr0'], $html);
	$html=str_replace('{clientZip}', $c['zip0'], $html);
	$html=str_replace('{insType}', $c['insType'], $html);
	$html=str_replace('{policeNum}', $c['police_num'], $html);
	$html=str_replace('{carNum}', $c['car_num'], $html);
	$html=str_replace('{policeEnd}', str2time($c['end'],'d/m/Y'), $html);
	$html=str_replace('{policeStart}', str2time($c['start'],'d/m/Y'), $html);
	$html=str_replace('{dateEnd}', str2time($c['end'],'d/m/Y'), $html);
	$html=str_replace('{dateStart}', str2time($c['start'],'d/m/Y'), $html);
	$html=eregi_replace('{today}', date('d/m/Y'), $html);
 return $html;
}
*/

?>