<?

$NO_SKIN=1;

include_once 'modules/common/common.php';
$docTypes = array('invoice_credit'=>'חשבונית מס','invoice_debt'=>'חשבונית מס זיכוי','invoice_accept'=>'חשבונית מס קבלה','accept'=>'קבלה','accept_tmp'=>'קבלה זמנית','deposit'=>'הפקדה לבנק',
		'refund'=>'החזרת שיקים','refund_bankclient'=>'החזרת שיקים ללקוח','refund_cashclient'=>'החזרת שיקים ללקוח','refund_bankcash'=>'החזרת שיקים לקופה','expense'=>'קליטת הוצאות'
);

$telStore = array(1=>'בית',2=>'עבודה',3=>'נייד',4=>'פקס בבית',5=>'פקס בעבודה',0=>'אחר');

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else die('{success:false, msg:"function name error"}');
//————————————————————————————————————————————————————————————————————————————————————
function f_docs() {
 global $prms_groups, $group_id, $docTypes;
 	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$where="acc_docs.group_id=$group_id AND ".($_REQUEST['d_type']? "acc_docs.type LIKE '".sql_escape($_REQUEST['d_type'])."' AND ":'')." acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'";

	$sql = "SELECT SQL_CALC_FOUND_ROWS acc_docs.* "
		.", TRIM( CONCAT(last_name,' ',first_name)) AS client_name"
		.", clients.email, tel_type0, clients.tel0, tel_type1, clients.tel1, tel_type2, clients.tel2, tel_type3, clients.tel3,tel_type4, clients.tel4 "
		.", clients.folder, adr_type0, clients.city0, adr0, zip0, adr_type1, clients.city1, adr1, zip1, adr_type2, clients.city2, adr2, zip2 "
		."FROM acc_docs "
		."LEFT JOIN clients ON acc_docs.client_id = clients.client_id "
		."LEFT JOIN users ON acc_docs.user_id = users.user_id "
		."WHERE $where "
		." ORDER BY ".($_REQUEST['sort'] ? $_REQUEST['sort']:'date').($_REQUEST['dir']?" $_REQUEST[dir]":' ASC')
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	if ($rows) foreach ($rows as $k=>$r) {
		$row=&$rows[$k];
		$total+=$r['sum'];
		$row['client_phones'] = client_phones($row);
		$row['client_addresses'] = client_addresses($row);
		unset($row['tel_type0'], $row['tel0'], $row['tel_type1'], $row['tel1'], $row['tel_type2'], $row['tel2'], $row['tel_type3'], $row['tel3'], $row['tel_type4'], $row['tel4'],$row['descript']);
		if (!$_GET['stickers'] AND !$_GET['letters']) unset($row['adr_type0'],$row['city0'],$row['adr0'],$row['zip0'],$row['adr_type1'],$row['city1'],$row['adr1'],$row['zip1'],$row['adr_type2'],$row['city2'],$row['adr2'],$row['zip2']);
	}
	
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
			header("Content-Type: text/html;charset=utf-8");
			echo "<thml><head>".printStyle()."</head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
			<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
			
			echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
			
			echo "<center><h1><u>רשימת מסמכים".($_REQUEST['d_type']? " מסוג ".$docTypes[$_REQUEST['d_type']]:'')."<h1><h3>מתאריך: ".str2time($_GET['d_from'],'d/m/Y')." &nbsp;&nbsp; עד תאריך: ".str2time($_GET['d_to'],'d/m/Y')."</h3></u><br><br>";
			if($agent) echo "סוכן ביטוח: <h2 style='margin:0'><h>".htmlspecialchars($crm_agents[$agent])."</u></h2>";
			if ($firm AND $firm!='כל החברות')  echo " חברת ביטוח:<u> ".htmlspecialchars($firm)."</u>";
			echo "</center><br>";
			
			echo "<table width=100% class=t_list>";
			echo "<tr><th width=8>#</th><th nowrap>תאריך הפקה</th><th>סוג מסמך</th><th>מספר</th><th>שם לקוח</th><th>סכום</th><th>הערות</th>";
			foreach($rows as $r) {
				$n++;
				echo "<tr><td widtd=8>$n</td><td nowrap>".str2time($r['date'],'d/m/Y')."</td><td>".$docTypes[$r['type']]."</td><td>$r[num]</td><td>".htmlspecialchars($r['client_name'])."</td><td align=right style='direction:ltr;'>".price($r['sum'],2)."</td><td>".text2html($r['notes'])."</td>";
			}
			echo "<tr><th colspan=5>סה\"כ (".count($rows)." מסמכים)</th><th align=right style='direction:ltr;'>".price($total,2)."</th><th></th>";

			echo "</table>"; 
			echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
			echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='tmp_receipts'.date('d-m-Y').'.csv';
		
		foreach($rows as $k=>$r) $rows[$k]['type'] = $docTypes[$r['type']];
		
		$columns = array('date'=>'תאריך',
					'type'=>'סוג מסמך',
					'num'=>'מספר',
					'client_name'=>'שם לקוח',
					'netto'=>'סכום',
					'sum'=>'סה"כ',
					'notes'=>'הערות');

		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]')
			.", summary: {num:'סה\"כ (".count($rows)." מסמכים)', sum: $total}"
			.'}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_orders() {
 global $prms_groups, $group_id;
 	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	// $_REQUEST['wtype'] = iconv("UTF-8", "utf-8", $_REQUEST['wtype']); 

	// if ($_REQUEST['wtype'] AND $_REQUEST['wtype']!='כולם') $where=" AND wtype=".quote($_REQUEST['wtype']);
	if ($_REQUEST['status']) $where=" AND status=".quote($_REQUEST['status']);
	// if ($_REQUEST['importance']!=3) $where=" AND importance=".quote($_REQUEST['importance'],1);
	
	// if ($_REQUEST['dtype']) $where.=" AND acc_orders.".($_REQUEST['dtype']==1?'start':'end').">='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_orders.".($_REQUEST['dtype']==1?'start':'end')."<='".str2time($_REQUEST['d_to'],'Y-m-d')."'";
	if ($_REQUEST['d_from'] AND $_REQUEST['d_to']) $where.=" AND acc_orders.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_orders.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'";

	if ($_REQUEST['q']) $make_where_search=make_where_search('last_name, first_name', iconv("UTF-8", "utf-8", $_REQUEST['q']));

	$sql="SELECT SQL_CALC_FOUND_ROWS acc_orders.*, users.fname, users.lname, users.username"
		.", TRIM( CONCAT(last_name,' ',first_name)) AS client_name"
		." FROM acc_orders "
		."LEFT JOIN clients ON acc_orders.client_id=clients.client_id "
		."LEFT JOIN users ON users.user_id=acc_orders.user_id "
		."WHERE acc_orders.group_id=$group_id AND acc_orders.deleted='0' $where".($make_where_search ? " AND $make_where_search ":'')
		." ORDER BY ".($_REQUEST['sort'] ? $_REQUEST['sort']:'id').($_REQUEST['dir']?" $_REQUEST[dir]":' ASC');
	$rows=sql2array($sql);
	
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if ($rows) foreach($rows as $k=>$r) {
		if ("$r[lname]$r[fname]")  $rows[$k]['username']=trim("$r[lname] $r[fname]");
		if ($r['start'] == '0000-00-00 00:00') $rows[$k]['start'] = ''; else $rows[$k]['start']=str2time($r['start'],'d/m/Y H:i');
		if ($r['end'] == '0000-00-00 00:00') $rows[$k]['end'] = ''; else $rows[$k]['end']=str2time($r['end'],'d/m/Y H:i');
		if ($r['date'] == '0000-00-00') $rows[$k]['date'] = ''; else $rows[$k]['date']=str2time($r['date'],'d/m/Y');
	}
	
		if ($_GET['print'] AND $rows) {//Open&Print PopUp window
			header("Content-Type: text/html;charset=utf-8");
			
			echo "<thml><head>".printStyle()."</head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
			<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";

			echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
			echo "<center><h1><u>רשימת הזמנות</h1>"
			.(($_GET['d_from'] AND $_GET['d_from']!='undefined') ? "<h3>מתאריך: ".str2time($_GET['d_from'],'d/m/Y')." &nbsp;&nbsp; עד תאריך: ".str2time($_GET['d_to'],'d/m/Y')."</h3>":'')
			."</u><br><br>";
			echo "<table width=100% class=t_list>";

			echo "<tr><th nowrap>מס' הזמנה</th><th>תאריך</th><th>לקוח</th><th>סטאטוס</th><th>כותרת</th><th>סכום</th>"
			.($_GET['user_col'] ? "<th width=66>מטפל</th>":'')."</tr>";
			//<th width=45>סטאטוס</th>
			$c=1;
			foreach ($rows as $row) {
				echo "<tr><td><div>$row[num]</div></td><td>".htmlspecialchars($row['date'])."</td><td nowrap>".htmlspecialchars($row['client_name'])."</td><td>".($row['status']==1?'בעבודה':'בוצע')."</td><td>".htmlspecialchars($row['name'])."</td><td nowrap>".price($row['sum'],2)."</td>";
			}
			echo "</table>"; 
			echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
			echo "</body></html>";
						
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='orders_'.date('d-m-Y').'.csv';
		$columns=array(
			'num'=>'מספר הזמנה',
			'date'=>'תאריך הזמנה',
			'client_name'=>'לקוח',
			// 'car_name'=>"רכב",
			'end'=>'תאריך יציאה',
			// 'wtype'=>"סוג עבודה",
			'status'=>'סטאטוס',
			'name'=>'שם',
			'sum'=>'סכום'
		);
		
		foreach ($rows as $k=>$r) $rows[$k]['status'] = ($row['status']==1?'בעבודה':'בוצע');

		export_to_excel($filename,$columns,$rows); return;
	}elseif ($_GET['stickers'] AND $rows) {//Stickers Print
		printStickers($rows);
	}elseif ($_GET['letters'] AND $rows) {//Stickers Print
		printLetters($rows,$_GET['email']);
	} else	echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_credits() {
 global $SVARS, $group_id;
 	$_REQUEST['q'] = iconv("UTF-8", "utf-8", $_REQUEST['q']); 
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$sql="SELECT clients.client_id, TRIM( CONCAT( last_name, ' ', first_name ) ) AS client_name"
		.", clients.email, tel_type0, clients.tel0, tel_type1, clients.tel1, tel_type2, clients.tel2, tel_type3, clients.tel3,tel_type4, clients.tel4 "
		.", clients.folder, adr_type0, clients.city0, adr0, zip0, adr_type1, clients.city1, adr1, zip1, adr_type2, clients.city2, adr2, zip2 "
		.", ROUND((SUM(IF((acc_operations.debt_class=200), acc_operations.sum, 0)) - SUM(IF((acc_operations.credit_class=200), acc_operations.sum, 0))),2) AS balance "
		.", MAX(IF((acc_operations.debt_class=200), acc_operations.date, 0)) AS first_credit "//invoice
		.", MIN(IF((acc_operations.credit_class=200), acc_operations.date, '3000-12-31')) AS last_debt "//accept
		."FROM clients "
		."LEFT JOIN acc_operations ON ((acc_operations.credit_class=200 AND acc_operations.credit_card = clients.client_id) OR (acc_operations.debt_class=200	AND acc_operations.debt_card = clients.client_id)) "
		."WHERE clients.group_id=$group_id "
		."GROUP BY clients.client_id "
		."HAVING balance<>0 "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	$sql="SELECT (SUM(IF((acc_operations.debt_class=200), acc_operations.sum, 0)) - SUM(IF((acc_operations.credit_class=200), acc_operations.sum, 0))) AS balance "
		." FROM acc_operations WHERE acc_operations.group_id=$group_id";
	$total_balance=sql2array($sql,'','balance',1);

	if ($rows) foreach ($rows as $k=>$r) {
		$row=&$rows[$k];
		$row['client_phones'] = client_phones($row);
		$row['client_addresses'] = client_addresses($row);
		$row['month'] = str2time(max($row['first_credit'],min(date('Y-m-d'),$row['last_debt'])), 'm/Y');
		
		unset($row['tel_type0'], $row['tel0'], $row['tel_type1'], $row['tel1'], $row['tel_type2'], $row['tel2'], $row['tel_type3'], $row['tel3'], $row['tel_type4'], $row['tel4'],$row['descript']);
		if (!$_GET['stickers'] AND !$_GET['letters']) unset($row['adr_type0'],$row['city0'],$row['adr0'],$row['zip0'],$row['adr_type1'],$row['city1'],$row['adr1'],$row['zip1'],$row['adr_type2'],$row['city2'],$row['adr2'],$row['zip2']);
	}
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
			header("Content-Type: text/html;charset=utf-8");
			echo "<thml><head>".printStyle()."</head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
			<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
			
			echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
			
			echo "<center><h1><u>דוח גיול חובות</h1><h3></center><br>";
			
			echo "<table width=100% class=t_list>";
			echo "<tr><th width=8>#</th><th>שם לקוח</th><th>קוד לקוח</th><th>טלפון</th><th>חודש גיול</th><th>סה\"כ</th>";
			foreach($rows as $r) {
				$n++;
				echo "<tr><td widtd=8>$n</td><td מם'רשפ>".htmlspecialchars($r['client_name'])."</td><td align=right style='direction:ltr;'>".htmlspecialchars($r['client_id'])."</td><td>".htmlspecialchars($r['client_phones'])."</td><td>$row[month]</td><td align=right style='direction:ltr;'>".price($r['balance'],2)."</td>";
			}
			echo "<tr><th width=8> </th><th colspan=4>סה\"כ (".count($rows)." לקוחות)</th><th align=right style='direction:ltr;'>".price($total_balance,2)."</th>";

			echo "</table>"; 
			echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
			echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='invoices'.date('d-m-Y').'.csv';
		
		$columns = array('client_name'=>'שם לקוח',
					'client_id'=>'קוד לקוח',
					'client_phones'=>'טלפון',
					'month'=>'חודש גיול',
					'balance'=>'סה"כ');

		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]')
			.", summary: {client_name:'סה\"כ', balance: '".round($total_balance,2)."'}"
			.'}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_real() {
 global $SVARS, $docTypes;
 	$_REQUEST['q'] = iconv("UTF-8", "utf-8", $_REQUEST['q']); 
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$where="acc_docs.type='expense' AND acc_expense_ext.expense_type=2";
	

	$sql="SELECT acc_expense_rows.*, acc_docs.date "
		.", clients.name AS client_name "
		."FROM acc_expense_rows "
		."LEFT JOIN acc_docs ON acc_expense_rows.doc_id=acc_docs.id "
		."LEFT JOIN clients ON acc_docs.client_id = clients.id "
		."LEFT JOIN acc_expense_ext ON acc_expense_ext.doc_id=acc_docs.id "
		."WHERE $where";
	$rows=sql2array($sql);
	
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
			header("Content-Type: text/html;charset=utf-8");
			echo "<thml><head>".printStyle()."</head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
			<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
			
			echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
			
			echo "<center><h1><u>דוח חשבוניות</h1><h3>מתאריך: ".str2time($_GET['d_from'],'d/m/Y')." &nbsp;&nbsp; עד תאריך: ".str2time($_GET['d_to'],'d/m/Y')."</h3></u><br><br>";
			if($agent) echo "סוכן ביטוח: <h2 style='margin:0'><h>".htmlspecialchars($crm_agents[$agent])."</u></h2>";
			if ($firm AND $firm!='כל החברות')  echo " חברת ביטוח:<u> ".htmlspecialchars($firm)."</u>";
			echo "</center><br>";
			
			echo "<table width=100% class=t_list>";
			echo "<tr><th width=8>#</th><th nowrap>תאריך הפקה</th><th>סוג חשבונית</th><th>מספר</th><th>שם לקוח</th><th>סכום</th><th>מע\"מ</th><th>סה\"כ</th><th>הערות</th>";
			foreach($rows as $r) {
				$n++;
				echo "<tr><td widtd=8>$n</td><td nowrap>".str2time($r['date'],'d/m/Y')."</td><td>$r[type]</td><td>$r[num]</td><td>".htmlspecialchars($r['client_name'])."</td><td align=right style='direction:ltr;'>".price($r['netto'],2)."</td><td align=right style='direction:ltr;'>".price($r['vat_sum'],2)."</td><td align=right style='direction:ltr;'>".price($r['sum'],2)."</td><td>".text2html($r['notes'])."</td>";
			}
			echo "<tr><th width=8> </th><th nowrap> </th><th>סה\"כ (".count($rows)." חשבוניות)</th><th> </th><th> </th><th align=right style='direction:ltr;'>".price($total['sum']-$total['vat_sum'],2)."</th><th align=right style='direction:ltr;'>".price($total['vat_sum'],2)."</th><th align=right style='direction:ltr;'>".price($total['sum'],2)."</th><th></th>";

			echo "</table>"; 
			echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
			echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='invoices'.date('d-m-Y').'.csv';
		
		$columns = array('date'=>'תאריך הפקה',
					'type'=>'סוג חשבונית',
					'num'=>'מספר',
					'client_name'=>'שם לקוח',
					'netto'=>'סכום',
					'vat_sum'=>'מע"מ',
					'sum'=>'סה"כ',
					'notes'=>'הערות');

		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]').'}';
	}	
}
//————————————————————————————————————————————————————————————————————————————————————

function f_invoices() {
 global $SVARS, $docTypes;
	$types = array('invoice_credit'=>'חשבונית מס','invoice_debt'=>'חשבונית מס זיכוי','invoice_accept'=>'חשבונית מס קבלה');
 	$_REQUEST['q'] = iconv("UTF-8", "utf-8", $_REQUEST['q']); 
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if (!$_REQUEST['d_from'] OR !$_REQUEST['d_to']) die("{success:false, msg:'Date out of range'}");
	
	
	$where="acc_docs.type IN ('invoice_credit','invoice_debt','invoice_accept')";
	

	$where.=" AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'";

	$sql = "SELECT SUM(IF(TYPE='invoice_debt', - sum, sum)) as sum, SUM(IF(TYPE='invoice_debt', -vat_sum, vat_sum)) as vat_sum FROM acc_docs WHERE $where";
	$total=sql2array($sql,'','',1);

	if ($_GET['payment']) {
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");				// Date in the past
		header ("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");		// always modified
		if (eregi("MSIE", $_SERVER['HTTP_USER_AGENT'])) {
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
		}else {
			header ("Cache-Control: no-cache, must-revalidate");
			header ("Pragma: no-cache");
		}
		
		set_include_path('../inc');
		
		require_once 'Zend/Pdf.php';
		
		$pdf = new Zend_Pdf();
		
		$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
		
		$font_nrkis = Zend_Pdf_Font::fontWithPath('../inc/Zend/Pdf/Fonts/nrkis1.ttf', Zend_Pdf_Font::EMBED_SUPPRESS_EMBED_EXCEPTION);
		$page->setFont($font_nrkis, 12);
		
		$width  = $page->getWidth();
		$height = $page->getHeight();
		
		
		$company_tax= sql2array("SELECT name, income_tax, vat_period, current_vat_period FROM clients WHERE id=$SVARS[cid]",0,0,1,'rafael');
		
		$clear_tax = sql2array("SELECT SUM(sum/(100-tax_clear)*tax_clear) AS tax_clear FROM acc_docs WHERE acc_docs.type='accept' AND acc_docs.date>='".date('Y-m-d',strtotime("-$company_tax[vat_period] month", strtotime($company_tax['current_vat_period'])))."' AND acc_docs.date<'".$company_tax['current_vat_period']."'",0,'tax_clear',1);
		
		$image = Zend_Pdf_Image::imageWithPath("skin/pict/tax.jpg");
		$page->drawImage($image, 50, $height-($width-110)*315/850-15, $width-50, $height-15);

		$page->drawText(round($total['sum'],2), 290, 757);
		$page->drawText(round($company_tax['income_tax'],2), 423, 757);
		$page->drawText(round($total['sum']*$company_tax['income_tax']/100,2), 460, 757);
		$page->drawText(round($clear_tax,2), 460, 726);
		$page->drawText(round($total['sum']*$company_tax['income_tax']/100,2)-round($clear_tax,2), 460, 681);
		$page->drawText(str2time($_REQUEST['d_from'],'d/m/Y')." - ".str2time($_REQUEST['d_to'],'d/m/Y'), 92, 735);
		$page->drawText(str2time($_REQUEST['d_from'],'Y'), 90, 790);
		// $page->drawText(hebrev($company_tax['name']), 390, 790 ,'UTF-8');
		$page->drawText(date('d/m/Y'), 156, 816);
		
		$pdf->pages[] = $page;		

		header("Content-type: application/pdf");
		
		echo $pdf->render();
		
		exit;
	
	}
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS acc_docs.* "
		.", name AS client_name"
		.", clients.email, tel_type0, clients.tel0, tel_type1, clients.tel1, tel_type2, clients.tel2, tel_type3, clients.tel3,tel_type4, clients.tel4 "
		.", adr_type0, clients.city0, adr0, zip0, adr_type1, clients.city1, adr1, zip1, adr_type2, clients.city2, adr2, zip2 "
		."FROM acc_docs "
		."LEFT JOIN clients ON acc_docs.client_id = clients.id "
		."WHERE $where "
		."ORDER BY date ASC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	if ($rows) foreach ($rows as $k=>$r) {
		$row=&$rows[$k];
		$row['client_phones'] = client_phones($row);
		$row['client_addresses'] = client_addresses($row);
		if ($row['type']=='invoice_debt' ) {
			$row['sum'] = -$row['sum']; 
			$row['vat_sum'] = -$row['vat_sum'];
		} 
		// else {
			// $row['sum'] = $row['credit'];
		// }
		
		$row['netto'] = round($row['sum']-$row['vat_sum'],2);
		// $row['type'] = $docTypes[$row['type']];
		unset($row['tel_type0'], $row['tel0'], $row['tel_type1'], $row['tel1'], $row['tel_type2'], $row['tel2'], $row['tel_type3'], $row['tel3'], $row['tel_type4'], $row['tel4'],$row['descript']);
		if (!$_GET['stickers'] AND !$_GET['letters']) unset($row['adr_type0'],$row['city0'],$row['adr0'],$row['zip0'],$row['adr_type1'],$row['city1'],$row['adr1'],$row['zip1'],$row['adr_type2'],$row['city2'],$row['adr2'],$row['zip2']);
	}
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
			header("Content-Type: text/html;charset=utf-8");
			echo "<thml><head>".printStyle()."</head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
			<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
			
			echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
			
			echo "<center><h1><u>דוח חשבוניות</h1><h3>מתאריך: ".str2time($_GET['d_from'],'d/m/Y')." &nbsp;&nbsp; עד תאריך: ".str2time($_GET['d_to'],'d/m/Y')."</h3></u><br><br>";
			if($agent) echo "סוכן ביטוח: <h2 style='margin:0'><h>".htmlspecialchars($crm_agents[$agent])."</u></h2>";
			if ($firm AND $firm!='כל החברות')  echo " חברת ביטוח:<u> ".htmlspecialchars($firm)."</u>";
			echo "</center><br>";
			
			echo "<table width=100% class=t_list>";
			echo "<tr><th width=8>#</th><th nowrap>תאריך הפקה</th><th>סוג חשבונית</th><th>מספר</th><th>שם לקוח</th><th>סכום</th><th>מע\"מ</th><th>סה\"כ</th><th>הערות</th>";
			foreach($rows as $r) {
				$n++;
				echo "<tr><td widtd=8>$n</td><td nowrap>".str2time($r['date'],'d/m/Y')."</td><td>$r[type]</td><td>$r[num]</td><td>".htmlspecialchars($r['client_name'])."</td><td align=right style='direction:ltr;'>".price($r['netto'],2)."</td><td align=right style='direction:ltr;'>".price($r['vat_sum'],2)."</td><td align=right style='direction:ltr;'>".price($r['sum'],2)."</td><td>".text2html($r['notes'])."</td>";
			}
			echo "<tr><th width=8> </th><th nowrap> </th><th>סה\"כ (".count($rows)." חשבוניות)</th><th> </th><th> </th><th align=right style='direction:ltr;'>".price($total['sum']-$total['vat_sum'],2)."</th><th align=right style='direction:ltr;'>".price($total['vat_sum'],2)."</th><th align=right style='direction:ltr;'>".price($total['sum'],2)."</th><th></th>";

			echo "</table>"; 
			echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
			echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='invoices'.date('d-m-Y').'.csv';
		
		$columns = array('date'=>'תאריך הפקה',
					'type'=>'סוג חשבונית',
					'num'=>'מספר',
					'client_name'=>'שם לקוח',
					'netto'=>'סכום',
					'vat_sum'=>'מע"מ',
					'sum'=>'סה"כ',
					'notes'=>'הערות');

		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]')
			.", summary: {type:'סה\"כ (".count($rows)." חשבוניות)', sum: $total[sum], netto: ".round($total['sum']-$total['vat_sum'],2).", vat_sum: $total[vat_sum]}"
			.'}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————

function f_incomeRep(){
	$_REQUEST['d_from']=date("Y-m-d", mktime(0, 0, 0, $_REQUEST['month'], 1, $_REQUEST['year']));
	$_REQUEST['d_to']=date("Y-m-d", mktime(0, 0, 0, $_REQUEST['month']+$_REQUEST['rep_period'], 0, $_REQUEST['year']));
	
	$_sql = "SELECT "
	."acc_income_groups.name as Group_Name, Sum(acc_docs.sum) "
	."from acc_income_groups, acc_docs "
	."where acc_docs.income_group = acc_income_groups.id " 
	
	."group by acc_income_groups.name ";
}

function f_vat() {
 global $SVARS;
	$types = array('invoice_credit'=>'חשבונית מס','invoice_debt'=>'חשבונית מס זיכוי','invoice_accept'=>'חשבונית מס קבלה','expense'=>'קליטת הוצאות');
 	$_REQUEST['q'] = iconv("UTF-8", "windows-1255", $_REQUEST['q']); 
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	$_REQUEST['d_from']=date("Y-m-d", mktime(0, 0, 0, $_REQUEST['month'], 1, $_REQUEST['year']));
	$_REQUEST['d_to']=date("Y-m-d", mktime(0, 0, 0, $_REQUEST['month']+$_REQUEST['rep_period'], 0, $_REQUEST['year']));
	
	if (!$_REQUEST['d_from'] OR !$_REQUEST['d_to']) die("{success:false, msg:'Date out of range'}");
	
	$company_tax= sql2array("SELECT name, income_tax, vat_period, current_vat_period FROM clients WHERE id=$SVARS[cid]",0,0,1,'rafael');
	$clear_tax = sql2array("SELECT SUM(sum/(100-tax_clear)*tax_clear) AS tax_clear FROM acc_docs WHERE acc_docs.type='accept' AND acc_docs.date>='".date('Y-m-d',strtotime("-$company_tax[vat_period] month", strtotime($company_tax['current_vat_period'])))."' AND acc_docs.date<'".$company_tax['current_vat_period']."'",0,'tax_clear',1);
		
	$sql = "SELECT "
		."(SUM(IF((TYPE IN ('invoice_credit','invoice_accept') AND vat_sum<>0), sum, 0))-SUM(IF((TYPE IN ('invoice_debt') AND vat_sum<>0), sum, 0))) as credit_with_vat, "//סכום חשבוניות חייבות במע"מ
		."(SUM(IF((TYPE IN ('invoice_credit','invoice_accept') AND vat_sum=0), sum, 0))-SUM(IF((TYPE IN ('invoice_debt') AND vat_sum=0), sum, 0))) as credit_no_vat, "//סכום חשבוניות ללא מע"מ
		."SUM(IF(TYPE IN ('invoice_credit','invoice_accept','invoice_debt'),IF(type='invoice_debt', -vat_sum, vat_sum),0)) as in_vat, "//סכום מע"מ
		."SUM(IF(TYPE='expense', sum, 0)) as debt, "//סכום כל ההוצאות
		."SUM(IF((TYPE='expense' AND expense_type=1), vat_sum, 0)) as ex_vat1, "
		."SUM(IF((TYPE='expense' AND expense_type=2), vat_sum, 0)) as ex_vat2 "
		."FROM acc_docs LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id "
		."WHERE (acc_docs.type IN ('invoice_debt','invoice_credit','invoice_accept') AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."') OR (acc_docs.type='expense' AND acc_expense_ext.rep_period>='".str2time($_REQUEST['d_from'],'ym')."' AND acc_expense_ext.rep_period<='".str2time($_REQUEST['d_to'],'ym')."')";
	$total=sql2array($sql,'','',1);
	
	if ($_GET['payment']) {
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");				// Date in the past
		header ("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");		// always modified
		if (eregi("MSIE", $_SERVER['HTTP_USER_AGENT'])) {
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
		}else {
			header ("Cache-Control: no-cache, must-revalidate");
			header ("Pragma: no-cache");
		}
		
		set_include_path('../inc');
		
		require_once 'Zend/Pdf.php';
		
		$pdf = new Zend_Pdf();
		$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
		
		$font_nrkis = Zend_Pdf_Font::fontWithPath('../inc/Zend/Pdf/Fonts/nrkis1.ttf', Zend_Pdf_Font::EMBED_SUPPRESS_EMBED_EXCEPTION);
		$page->setFont($font_nrkis, 16);
		
		$width  = $page->getWidth();
		$height = $page->getHeight();
			
		$in_vat = round($total['in_vat']);
		$credit_no_vat = round($total['credit_no_vat']);
		
		$ex_vat1 = round($total['ex_vat1']);
		$ex_vat2 = round($total['ex_vat2']);
		$credit_with_vat = round($total['credit_with_vat'])-$in_vat;

		$vat_pay = $in_vat-$ex_vat1-$ex_vat2;
		$credit_with_vat = "$credit_with_vat";
		
		$in_vat = "$in_vat";
		$credit_no_vat = "$credit_no_vat";
		$ex_vat1 = "$ex_vat1";
		$ex_vat2 = "$ex_vat2";
		
		if ($vat_pay>=0) {
			$vat_pay = "$vat_pay";
		
			$image = Zend_Pdf_Image::imageWithPath("skin/pict/vat_pay.jpg");
			$page->drawImage($image, 50, $height-($width-110)*920/1920-15, $width-50, $height-15);
			
		} else {
			$image = Zend_Pdf_Image::imageWithPath("skin/pict/vat_refund.jpg");
			$page->drawImage($image, 50, $height-($width-110)*920/1920-14, $width-50, $height-14);
			
			$vat_pay = abs($vat_pay);
			$vat_pay = "$vat_pay";
		}
				
		for($i=0; $i<=strlen($credit_no_vat); $i++) $page->drawText($credit_no_vat[(strlen($credit_no_vat)-$i)], 194-12.7*$i, 756);
		for($i=0; $i<=strlen($credit_with_vat); $i++) $page->drawText($credit_with_vat[(strlen($credit_with_vat)-$i)], 350-11.6*$i, 756);
		for($i=0; $i<=strlen($in_vat); $i++) $page->drawText($in_vat[(strlen($in_vat)-$i)], 520-14.7*$i, 755);
		for($i=0; $i<=strlen($ex_vat1); $i++) $page->drawText($ex_vat1[(strlen($ex_vat1)-$i)], 520-14.7*$i, 706);
		for($i=0; $i<=strlen($ex_vat2); $i++) $page->drawText($ex_vat2[(strlen($ex_vat2)-$i)], 520-14.7*$i, 730);
		for($i=0; $i<=strlen($vat_pay); $i++) $page->drawText($vat_pay[(strlen($vat_pay)-$i)], 520-14.7*$i, 637);
		
		
		//Income Tax
		$page->setFont($font_nrkis, 12);
		
		$image = Zend_Pdf_Image::imageWithPath("skin/pict/tax.jpg");
		$page->drawImage($image, 50, $height-($width-110)*315/850-315, $width-50, $height-315);

		$page->drawText(round(($total['credit_no_vat']+$total['credit_with_vat']-$total['in_vat']),2), 290, 457);
		$page->drawText(round($company_tax['income_tax'],2), 423, 457);
		$page->drawText(round(($total['credit_no_vat']+$total['credit_with_vat']-$total['in_vat'])*$company_tax['income_tax']/100,2), 460, 457);
		$page->drawText(round($clear_tax,2), 460, 426);
		$page->drawText(round(($total['credit_no_vat']+$total['credit_with_vat']-$total['in_vat'])*$company_tax['income_tax']/100,2)-round($clear_tax,2), 460, 381);
		$page->drawText(str2time($_REQUEST['d_from'],'d/m/Y')." - ".str2time($_REQUEST['d_to'],'d/m/Y'), 92, 435);
		$page->drawText(str2time($_REQUEST['d_from'],'Y'), 90, 490);
		// $page->drawText(hebrev($company_tax['name']), 390, 490 ,'UTF-8');
		$page->drawText(date('d/m/Y'), 156, 516);
		
		$page->setFont($font_nrkis, 12);
		$style = new Zend_Pdf_Style();
		$style->setFillColor(new Zend_Pdf_Color_HTML('#000077'));
		$page->setStyle($style);
	
		$page->drawText(hebrev(iconv("utf-8","windows-1255",'תשלום באמצעות אינטרנט')), 235, 310);
		$page->drawText('___________________', 235, 309);
		
		$image = Zend_Pdf_Image::imageWithPath("skin/pict/taxes-logo.jpg");
		$page->drawImage($image, $width/2-130, 260, $width/2+130, 300);
		$target = Zend_Pdf_Action_URI :: create( 'http://ozar.mof.gov.il/taxes' );
		$annotation = Zend_Pdf_Annotation_Link :: create( $width/2-130, 260, $width/2+130, 300, $target );
		$page->attachAnnotation( $annotation );
		
		$image = Zend_Pdf_Image::imageWithPath("skin/pict/btl-logo.jpg");
		$page->drawImage($image, $width/2-130, 200, $width/2+130, 240);
		$target = Zend_Pdf_Action_URI :: create( 'https://b2b.btl.gov.il/b2b/framesetlogonanonymous.asp' );
		$annotation = Zend_Pdf_Annotation_Link :: create( $width/2-130, 200, $width/2+130, 240, $target );
		$page->attachAnnotation( $annotation );
		$pdf->pages[] = $page;		

		header("Content-type: application/pdf");
		echo $pdf->render();
		exit;
	}
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS acc_docs.*, acc_expense_ext.expense_type,name AS client_name "
		."FROM acc_docs "
		."LEFT JOIN clients ON acc_docs.client_id = clients.id "
		."LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id "
		."WHERE (acc_docs.type IN ('invoice_debt','invoice_credit','invoice_accept') AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."') OR (acc_docs.type='expense' AND acc_expense_ext.rep_period>='".str2time($_REQUEST['d_from'],'ym')."' AND acc_expense_ext.rep_period<='".str2time($_REQUEST['d_to'],'ym')."') "
		."ORDER BY date ASC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	if ($rows) foreach ($rows as $k=>$r) {
		$row=&$rows[$k];
		if ($row['type']=='invoice_credit' OR $row['type']=='invoice_accept') {
			$row['debt'] ='';
			$row['in_vat'] = $row['vat_sum'];
			if ($row['in_vat']) $row['credit_with_vat'] =$row['sum']; else $row['credit_no_vat'] =$row['sum'];
		} 
		elseif ( $row['type']=='invoice_debt') {
			$row['debt'] ='';
			$row['in_vat'] = - $row['vat_sum'];
			if ($row['in_vat']) $row['credit_with_vat'] =-$row['sum']; else $row['credit_no_vat'] =-$row['sum'];
		} 
		elseif ($row['type']=='expense' ) {
			$row['ex_vat'.$row['expense_type']] = $row['vat_sum'];
			$row['debt'] =$row['sum'];
			$row['credit_with_vat'] ='';
		} 
		else {
			$row['in_vat'] = $row['vat_sum'];
			$row['debt'] ='';
		}
	}
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
		header("Content-Type: text/html;charset=utf-8");
		echo "<html><head>".printStyle()."</head>
		<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
		<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
		
		echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['user']['name']?$SVARS['user']['name']:$SVARS['user']['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
		
		echo "<center><h1><u>דוח מע\"מ ".($_REQUEST['rep_period']==2 ?'דו-':'')."חודשי מפורט</h1><h3>תקופת דיווח: ".str2time($_REQUEST['d_from'],'m/Y').($_REQUEST['rep_period']==2 ?'-'.str2time($_REQUEST['d_to'],'m/Y'):'')."</h3></u><br><br>";
		echo "</center><br>";
		
		echo "<table width=100% class=t_list>";
		echo '<tr>
			<th width=8>#</th>
			<th nowrap>תאריך הפקה</th>
			<th>אסמכתא</th>
			<th>מספר</th>
			<th>שם לקוח/ספק</th>
			<th>הכנסות פתורות מע"מ</th>
			<th>הכנסות כוללות מע"מ</th>
			<th>מע\"מ</th>
			<th>הוצאות כולל מע"מ</th>
			<th>מע\"מ תשומות וציוד</th>
			<th>מע\"מ תשומות אחרות</th>
		</tr>';
		foreach($rows as $r) {
			$n++;
			echo "<tr>
				<td widtd=8>$n</td>
				<td nowrap>".str2time($r['date'],'d/m/Y')."</td>
				<td>".$types[$r['type']]."</td>
				<td>$r[num]</td>
				<td>".htmlspecialchars($r['client_name'])."</td>
				<td align=right style='direction:ltr;'>".($r['credit_no_vat']?price($r['credit_no_vat'],2):'')."</td>
				<td align=right style='direction:ltr;'>".($r['credit_with_vat']?price($r['credit_with_vat'],2):'')."</td>
				<td align=right style='direction:ltr;'>".($r['in_vat']?price($r['in_vat'],2):'')."</td>
				<td align=right style='direction:ltr;'>".($r['debt']?price($r['debt'],2):'')."</td>
				<td align=right style='direction:ltr;'>".($r['ex_vat1']?price($r['ex_vat1'],2):'')."</td>
				<td align=right style='direction:ltr;'>".($r['ex_vat2']?price($r['ex_vat2'],2):'')."</td>
			</tr>";
		}
		echo "<tr>
			<th colspan=5>סה\"כ (".count($rows)." פעולות)</th>
			<th align=right style='direction:ltr;'>".price($total['credit_no_vat'],2)."</th>
			<th align=right style='direction:ltr;'>".price($total['credit_with_vat'],2)."</th>
			<th align=right style='direction:ltr;'>".price($total['in_vat'],2)."</th>
			<th align=right style='direction:ltr;'>".price($total['debt'],2)."</th>
			<th align=right style='direction:ltr;'>".price($total['ex_vat1'],2)."</th>
			<th align=right style='direction:ltr;'>".price($total['ex_vat2'],2)."</th>
		</tr>";
		echo "<tr>
			<th colspan=9> </th>
			<th style='text-align:left'>לתשלום:</th>
			<th align=right style='direction:ltr;'>".price($total['in_vat']-$total['ex_vat1']-$total['ex_vat2'],2)."</th>
		</tr>";

		echo "</table>"; 
		echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2011 CRM247 </span></div>";			
		echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='rep_vat'.date('d-m-Y').'.csv';
		
		$columns = array(
			'date'=>'תאריך הפקה',
			'type'=>'אסמכתא',
			'num'=>'מספר',
			'client_name'=>'שם לקוח',
			'credit_no_vat'=>'הכנסות פתורות מע"מ',
			'credit_with_vat'=>'הכנסות כולל מע"מ',
			'in_vat'=>'מע"מ',
			'debt'=>'הוצאות כולל מע"מ',
			'ex_vat1'=>'מע"מ תשומות וציוד',
			'ex_vat2'=>'מע"מ תשומות אחרות');
			
			foreach ($rows as $k=>$r) $rows[$k]['type'] = $types[$r['type']];

		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]')
			.",clear_tax:".round($clear_tax,2)
			.",income_tax:".round($company_tax['income_tax'],2)
			.", summary: {client_name:'סה\"כ:', credit_with_vat:".round($total['credit_with_vat'],2).", credit_no_vat:".round($total['credit_no_vat'],2).", debt:".round($total['debt'],2).", in_vat:".round($total['in_vat'],2).", ex_vat1:".round($total['ex_vat1'],2).", ex_vat2:".round($total['ex_vat2'],2)."}" 
			.'}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cash_balance() {
 global $SVARS, $group_id;
 
	$cashTypes = array(1001=>'מזומן',1002=>'שיק',1004=>'וויזה',1005=>'ישראכרט/מאסטרכארד',1006=>'דיינרס',1007=>'אמריקן אקספרס',1008=>'לאומי קארד');
 
	if (!$_REQUEST['date']) die("{success:false, msg:'Date out of range'}");
	
	$sql="SELECT IF (credit_class=100, credit_card, debt_card) AS card, SUM(IF (credit_class=100, -sum, sum)) as balance FROM acc_operations "
		." WHERE group_id=$group_id AND (credit_class=100 OR debt_class=100) AND date<='".str2time($_REQUEST['date'],'Y-m-d')
		."' GROUP BY card";
	$rows=sql2array($sql);
	
	if ($rows) foreach ($rows as $k=>$r) {
		$row=&$rows[$k];
		$row['type'] = $cashTypes[$r['card']];
		$total+=$r['balance'];
	}
	
	echo "{success:true, total:'".count($rows)."', data:".($rows ? array2json($rows):'[]').", summary: {type:'סה\"כ', balance: $total}}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cash() {
 global $SVARS, $group_id, $docTypes;
 	$_REQUEST['q'] = iconv("UTF-8", "utf-8", $_REQUEST['q']); 
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	$type = $_REQUEST['type'];
	
	if (!$_REQUEST['d_from'] OR !$_REQUEST['d_to']) die("{success:false, msg:'Date out of range'}");
	
	
	$where="acc_operations.group_id=$group_id AND ((acc_operations.credit_class=100 ".($type ? ($type==3 ? "AND acc_operations.credit_card IN (1004,1005,1006,1007)" : "AND acc_operations.credit_card=$type") : '' ).") OR (acc_operations.debt_class=100 ".($type ? ($type==3 ? "AND acc_operations.debt_card IN (1004,1005,1006,1007)" : "AND acc_operations.debt_card=$type") : '' )."))";

	$sql = "SELECT SQL_CALC_FOUND_ROWS acc_operations.id, acc_operations.credit_class, acc_operations.date, acc_operations.debt_class, acc_operations.sum, acc_docs.id AS doc_id, acc_docs.type, acc_docs.num, acc_docs.notes "
		.", TRIM( CONCAT(last_name,' ',first_name)) AS client_name "
		."FROM acc_operations "
		."LEFT JOIN acc_docs ON acc_docs.id = acc_operations.doc_id "
		."LEFT JOIN clients ON acc_docs.client_id = clients.client_id "
		."WHERE $where AND acc_operations.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_operations.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."' "
		."ORDER BY acc_operations.date ASC,  acc_operations.id ASC"
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	
	// echo "/*$sql*/";
	$rows=sql2array($sql);
	// echo "/*<div dir=ltr align=left><pre>".print_r($rows[0],1)."</pre></div>*/";
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	
	if ($rows) {
		$sql = "SELECT (SUM(IF(debt_class=100, sum, 0))-SUM(IF(credit_class=100, sum, 0))) as balance FROM acc_operations WHERE $where AND (acc_operations.date<'".$rows[0]['date']."' OR (acc_operations.date='".$rows[0]['date']."' AND id<".(int)$rows[0]['id']."))";
		$balance=sql2array($sql,'','balance',1);
		
		$sql = "SELECT SUM(IF(debt_class=100, sum, 0)) AS debt, SUM(IF(credit_class=100, sum, 0)) as credit FROM acc_operations WHERE $where AND acc_operations.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_operations.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'";
		$total=sql2array($sql,'','',1);
		
		foreach ($rows as $k=>$r) {
			$row=&$rows[$k];
			if ($row['credit_class']==100 ) $row['credit']=$row['sum']; 
			if ($row['debt_class']==100 ) $row['debt']=$row['sum']; 
			$balance=$balance+$row['debt']-$row['credit'];
			$row['balance']=$balance;
		}
	}
		if ($_GET['print'] AND $rows) {//Open&Print PopUp window
			header("Content-Type: text/html;charset=utf-8");
			echo "<thml><head>".printStyle()."</head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
			<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
			
			echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
			
			echo "<center><h1><u>דוח קופ".($type ? ($type==3 ? "ת אשראי" : ($type==1001 ? 'ת מזומנים':'ת שיקים')) : 'ה' )."</h1><h3>מתאריך: ".str2time($_GET['d_from'],'d/m/Y')." &nbsp;&nbsp; עד תאריך: ".str2time($_GET['d_to'],'d/m/Y')."</h3></u><br><br>";
			if($agent) echo "סוכן ביטוח: <h2 style='margin:0'><h>".htmlspecialchars($crm_agents[$agent])."</u></h2>";
			if ($firm AND $firm!='כל החברות')  echo " חברת ביטוח:<u> ".htmlspecialchars($firm)."</u>";
			echo "</center><br>";
			
			echo "<table width=100% class=t_list>";
			echo "<tr><th width=8>#</th><th nowrap>תאריך הפקה</th><th>סוג קבלה</th><th>מספר</th><th>שם לקוח</th><th>חובה</th><th>זכות</th><th>יתרה</th><th>הערות</th>";
			foreach($rows as $r) {
				$n++;
				echo "<tr><td widtd=8>$n</td><td nowrap>".str2time($r['date'],'d/m/Y')."</td><td>".$docTypes[$r['type']]."</td><td>$r[num]</td><td>".htmlspecialchars($r['client_name'])."</td><td align=right style='direction:ltr;'>".price($r['credit'],2)."</td><td align=right style='direction:ltr;'>".price($r['debt'],2)."</td><td align=right style='direction:ltr;'>".price($r['balance'],2)."</td><td>".text2html($r['notes'])."</td>";
			}
			echo "<tr><th width=8> </th><th nowrap> </th><th>סה\"כ (".count($rows)." חשבוניות)</th><th> </th><th> </th><th align=right style='direction:ltr;'>".price($total['credit'],2)."</th><th align=right style='direction:ltr;'>".price($total['debt'],2)."</th><th align=right style='direction:ltr;'></th><th></th>";

			echo "</table>"; 
			echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
			echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='invoices'.date('d-m-Y').'.csv';
		
		$columns = array('date'=>'תאריך הפקה',
					'type'=>'סוג מסמך',
					'num'=>'מספר',
					'client_name'=>'שם לקוח',
					'credit'=>'חובה',
					'debt'=>'זכות',
					'balance'=>'יתרה',
					'notes'=>'הערות');
					
		foreach ($rows as $k=>$r) {$row=&$rows[$k]; $row['type']=$docTypes[$r['type']];}
		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]')
			.", summary: {debt:".(int)$total['debt'].", credit:".(int)$total['credit']."}}";
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cheques() {
 global $SVARS;
	$Year = ($_REQUEST['year'] ? $_REQUEST['year']:date('Y'));
	$Month = ($_REQUEST['month'] ? $_REQUEST['month']:date('m'));
	
	$where =((isset($_REQUEST['deposit']) AND $_REQUEST['deposit']!=-1) ? " acc_accept_rows.deposit=".(int)$_REQUEST['deposit']:'');
	$where.=($_REQUEST['client_name'] ? ($where?' AND ':'')." clients.name LIKE '%".$_REQUEST['client_name']."%'" :'');

	if ($_REQUEST['export']) {
		$filename='cash'.date('d-m-Y').'.csv';
		
		$columns = array('year'=>'שנה',
					'month'=>'חודש',
					'date'=>'תאריך פרעון',
					'accept_num'=>"מס' קבלה",
					'client_name'=>'לקוח',
					'sum'=>'סכום',
					'bank_name'=>'הפקדה',
					'invoice_num'=>"מס' חשבונית");
					
		$sql="SELECT acc_accept_rows.id, acc_accept_rows.deposit, accept.client_id, ptype, acc_accept_rows.date AS date, acc_accept_rows.sum, acc_accept_rows.accept_id, acc_accept_rows.invoice_id "
			.", accept.num AS accept_num, invoice.num AS invoice_num, clients.name AS client_name "
			// .", CONCAT(acc_banks.bank_name,': ', acc_bank_accounts.bank,'-', acc_bank_accounts.branch,'-', acc_bank_accounts.number) AS bank_name "
			.", CONCAT(acc_banks.bank_name,': ', acc_bank_accounts.number) AS bank_name "
			."FROM acc_accept_rows "
			."INNER JOIN acc_docs accept ON (accept.id=acc_accept_rows.accept_id ) "
			."LEFT JOIN acc_docs invoice ON (invoice.id=acc_accept_rows.invoice_id ) "
			."INNER JOIN clients ON (accept.client_id=clients.id ) "
			."LEFT JOIN acc_bank_accounts ON (acc_accept_rows.deposit=acc_bank_accounts.id)"
			."LEFT JOIN acc_banks ON (acc_banks.bank_code=acc_bank_accounts.bank  AND acc_banks.branch_code=acc_bank_accounts.branch)"
			.($where ? " WHERE $where":'')
			." ORDER BY acc_accept_rows.date";
		$rows=sql2array($sql);
		
		if ($rows) foreach ($rows as $k=>$r) {
			$row=&$rows[$k]; 
			$row['year']=str2time($r['date'],'Y');
			$row['month']=str2time($r['date'],'m');
		}
		export_to_excel($filename,$columns,$rows);	return;
	}
	
	if ($_REQUEST['month'] OR (!$_REQUEST['month'] AND !$_REQUEST['year'])) {
		$sql="SELECT acc_accept_rows.id, acc_accept_rows.deposit, accept.client_id, ptype, acc_accept_rows.date AS date, acc_accept_rows.sum, acc_accept_rows.accept_id, acc_accept_rows.invoice_id "
			.", accept.num AS accept_num, invoice.num AS invoice_num, clients.name AS client_name "
			// .", CONCAT(acc_banks.bank_name,': ', acc_bank_accounts.bank,'-', acc_bank_accounts.branch,'-', acc_bank_accounts.number) AS bank_name "
			.", CONCAT(acc_banks.bank_name,': ', acc_bank_accounts.number) AS bank_name "
			."FROM acc_accept_rows "
			."INNER JOIN acc_docs accept ON (accept.id=acc_accept_rows.accept_id ) "
			."LEFT JOIN acc_docs invoice ON (invoice.id=acc_accept_rows.invoice_id ) "
			."INNER JOIN clients ON (accept.client_id=clients.id ) "
			."LEFT JOIN acc_bank_accounts ON (acc_accept_rows.deposit=acc_bank_accounts.id)"
			."LEFT JOIN acc_banks ON (acc_banks.bank_code=acc_bank_accounts.bank  AND acc_banks.branch_code=acc_bank_accounts.branch)"
			."WHERE acc_accept_rows.date>='".$Year."-".$Month."-"."01' AND acc_accept_rows.date<'".$Year."-".sprintf("%02s", ($Month+1))."-"."01' "
			.($where ? " AND $where":'')
			." ORDER BY acc_accept_rows.date";
		$month_cheques=sql2array($sql);
		if ($month_cheques) foreach ($month_cheques as $c=>$cheque) {
			$month_cheques[$c]['invoice_num']=($cheque['invoice_num']?$cheque['invoice_num']:'');	
			
			if (!$cheque['bank_name']) $month_cheques[$c]['bank_name']='';	
			
			if ($cheque['ptype']=='אשראי') $month_cheques[$c]['iconCls']='ccard';	
			elseif ($cheque['ptype']=='שיק') $month_cheques[$c]['iconCls']='cheque';	
			else $month_cheques[$c]['iconCls']='money';	
			
			$month_cheques[$c]['leaf']=true;
			// $month_cheques[$c]['cls']='x-tree-node-blue';
			
			if ($cheque['date']>date('Y-m-d')) $month_cheques[$c]['cls']='x-tree-node-red';
			elseif ($cheque['date']<date('Y-m-d')) $month_cheques[$c]['cls']='x-tree-node-blue';
			else $month_cheques[$c]['cls']='x-tree-node-yellow';
			
			$month_cheques[$c]['date']=str2time($cheque['date'],'d/m/Y');
		}
		
		// echo "/*$sql*/";
		// echo "/*".$Year."-".$Month."<div dir=ltr align=left><pre>".print_r($month_cheques,1)."</pre></div>*/";
	}
	if ($_REQUEST['month']) {echo ($month_cheques ? array2json($month_cheques):'[]'); return;}
	
	if ($_REQUEST['year'] OR (!$_REQUEST['month'] AND !$_REQUEST['year'])) {
	
		$sql="SELECT SUM(acc_accept_rows.sum) AS sum, DATE_FORMAT(acc_accept_rows.date, '%Y') AS year, DATE_FORMAT(acc_accept_rows.date, '%Y%m') AS yearmonth, DATE_FORMAT(acc_accept_rows.date, '%m') AS month, DATE_FORMAT(acc_accept_rows.date, 'חודש %m') AS date "
			."FROM acc_accept_rows "
			."INNER JOIN acc_docs accept ON (accept.id=acc_accept_rows.accept_id ) "
			."INNER JOIN clients ON (accept.client_id=clients.id ) "
			.($where ? "WHERE $where":'')
			." GROUP BY yearmonth "
			." HAVING year='".$Year."' ";
		$year_cheques=sql2array($sql);
		if ($year_cheques) 
			foreach ($year_cheques as $m=>$month) {
				$year_cheques[$m]['cls']='cash-node-group1 '.($month['yearmonth']>date('Ym')?'x-tree-node-red':'x-tree-node-blue');
			}
	}
	if ($_REQUEST['year']) {echo ($year_cheques ? array2json($year_cheques):'[]'); return;}

	$sql="SELECT DATE_FORMAT(acc_accept_rows.date, '%Y') AS year, SUM(acc_accept_rows.sum) AS sum, 'task-folder' AS iconCls "
		."FROM acc_accept_rows "
		."INNER JOIN acc_docs accept ON (accept.id=acc_accept_rows.accept_id ) "
		."INNER JOIN clients ON (accept.client_id=clients.id ) "
		.($where ? "WHERE $where":'')
		." GROUP BY year ";
	$all_cheques=sql2array($sql);
	
	if ($all_cheques) {
		foreach ($all_cheques as $y=>$year) {
			$all_cheques[$y]['cls']='cash-node-group2 '.($year['year']>date('Y')?'x-tree-node-red':'x-tree-node-blue');

			$all_cheques[$y]['date'] = 'שנת '.$year['year'];
			if ($year['year']==$Year) {
				if ($year_cheques) {
					foreach ($year_cheques as $m=>$month) {
						if ($month['month']==$Month) {
							if ($month_cheques) {
								$year_cheques[$m]['expanded'] = true;
								$year_cheques[$m]['children'] = $month_cheques;
							}
						}
					}
					$all_cheques[$y]['expanded'] = true;
					$all_cheques[$y]['children'] = $year_cheques;
				}
			}
		}
	}
	
	echo ($all_cheques ? array2json($all_cheques):'[]');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_cheques_old() {
 global $SVARS;
	$sql="SELECT acc_accept_rows.*, acc_docs.date AS accept_date, acc_docs.num "
		// .", TRIM( CONCAT(last_name,' ',first_name)) AS client_name "
		.", clients.name AS client_name "
		."FROM acc_accept_rows "
		."INNER JOIN acc_docs ON (acc_docs.type IN ('accept','invoice_accept') AND acc_docs.id=acc_accept_rows.accept_id ) "
		."INNER JOIN clients ON (acc_docs.client_id=clients.id ) "
		."WHERE ptype='שיק'"
		.($_REQUEST['client_id'] ? " AND lients.id=".(int)$_REQUEST['client_id']:'')
		.(((int)$_REQUEST['payment_status']==="$_REQUEST[payment_status]") ? " AND payment_status=".(int)$_REQUEST['payment_status'] : " AND payment_status IN (".sql_escape($_REQUEST['payment_status']).") ")
		." ORDER BY ".($_REQUEST['sort'] ? $_REQUEST['sort']:'id').($_REQUEST['dir']?" $_REQUEST[dir]":' ASC');
 	// echo "/*$sql*/";
	$rows=sql2array($sql);
	if ($rows) foreach ($rows as $k=>$r) $total+=$r['sum'];
	
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
		header("Content-Type: text/html;charset=utf-8");
		
		echo "<thml><head>".printStyle()."</head>
		<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
		<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";

		echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
		echo "<center><h1><u>רשימת שיקים בקופה</h1>"
		."<h3>תאריך: ".date('d/m/Y')." </h3>"
		."</u><br><br>";
		echo "<table width=100% class=t_list>";

		echo "<tr><th nowrap>סטאטוס</th><th>תאריך קבלה</th><th>תאריך פרעון</th><th>לקוח</th><th>סניף</th><th>בנק</th><th>חשבון</th><th>מס' שיק</th><th>סכום</th>"
		.($_GET['user_col'] ? "<th width=66>מטפל</th>":'')."</tr>";
		//<th width=45>סטאטוס</th>
		$c=1;
		foreach ($rows as $row) {
			echo "<tr><td><div>".($row['payment_status']==3 ? 'חזר':'')."</div></td><td>".str2time($row['accept_date'],'d/m/Y')."</td><td>".str2time($row['date'],'d/m/Y')."</td><td nowrap>".htmlspecialchars($row['client_name'])."</td><td>".htmlspecialchars($row['dept'])."</td><td>".htmlspecialchars($row['bank'])."</td><td>".htmlspecialchars($row['account'])."</td><td>".htmlspecialchars($row['number'])."</td><td nowrap>".price($row['sum'],2)."</td>";
		}
		echo "<tr><th colspan=8>סה\"כ</th><th>".price($total)."</th></tr></table>"; 
		echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
		echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='tmp_receipts'.date('d-m-Y').'.csv';
		
		$columns = array('payment_status'=>'סטאטוס',
					'accept_date'=>'תאריך קבלה',
					'date'=>'תאריך פרעון',
					'client_name'=>'שם לקוח',
					'dept'=>'סניף',
					'bank'=>'בנק',
					'account'=>'חשבון',
					'number'=>"מס' שיק",
					'sum'=>'סכום');

		foreach ($rows as $k=>$r) {$row=&$rows[$k]; $row['payment_status']=($r['payment_status']==3 ? 'חזר':'');}
		export_to_excel($filename,$columns,$rows);	return;
	} else echo "{success:true, total:".count($rows).", data:".($rows ? array2json($rows):'[]').", summary:{number:'סה\"כ', sum: $total}}";
}//————————————————————————————————————————————————————————————————————————————————————
function f_credit_payments() {
 global $SVARS, $group_id;

	$sql="SELECT accept_id, acc_accept_rows.date AS accept_date, acc_accept_rows.sum, acc_accept_rows.paid, bank, account, number, payment_status, acc_docs.num "
		.", TRIM( CONCAT(last_name,' ',first_name)) AS client_name "
		."FROM acc_accept_rows "
		."INNER JOIN acc_docs ON (acc_docs.type IN ('accept','invoice_accept') AND acc_docs.id=acc_accept_rows.accept_id ) "
		."INNER JOIN clients ON (acc_docs.client_id=clients.id ) "
		."WHERE acc_docs.group_id='$group_id' AND ptype='אשראי'"
		.($_REQUEST['client_id'] ? " AND client_id=".(int)$_REQUEST['client_id']:'')
		.(($_REQUEST['bank'] AND $_REQUEST['bank']!='כולם') ? " AND bank=".quote($_REQUEST['bank']):'')
		.(isset($_REQUEST['status']) ? " AND payment_status".($_REQUEST['status']==1 ? '=':'<')."account" : "")
		." ORDER BY acc_accept_rows.date";
		
 	// echo "/*$sql*/";
	$payments=sql2array($sql);
	
	// echo "/*<div dir=ltr align=left><pre>".print_r($payments,1)."</pre></div>*/";

	if ($payments) foreach($payments as $key=>$p) {
		if ((int)$p['account']) $p['sum']=round(($p['sum']-$p['paid'])/($p['account']-$p['payment_status']),2);
		for($i=$p['payment_status']; $i<$p['account']; $i++) {
			if (/*$i==0 AND */str2time($p['accept_date'],'d')<16) $payment_day=substr($p['accept_date'],0,8).'02'; else $payment_day=substr($p['accept_date'],0,8).'08';
			$p['date']=date('Y-m-d', strtotime("$payment_day +".($i+1)." month"));
			$rows[]=$p;
		}
	}

	// echo "/*<div dir=ltr align=left><pre>".print_r($rows,1)."</pre></div>*/";	
	
	if ($rows) foreach ($rows as $k=>$r) $total+=$r['sum'];
	
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
		header("Content-Type: text/html;charset=utf-8");
		
		echo "<thml><head>".printStyle()."</head>
		<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
		<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";

		echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
		echo "<center><h1><u>רשימת שיקים בקופה</h1>"
		."<h3>תאריך: ".date('d/m/Y')." </h3>"
		."</u><br><br>";
		echo "<table width=100% class=t_list>";

		echo "<tr><th nowrap>סטאטוס</th><th>תאריך קבלה</th><th>תאריך פרעון</th><th>לקוח</th><th>סניף</th><th>בנק</th><th>חשבון</th><th>מס' שיק</th><th>סכום</th>"
		.($_GET['user_col'] ? "<th width=66>מטפל</th>":'')."</tr>";
		//<th width=45>סטאטוס</th>
		$c=1;
		foreach ($rows as $row) {
			echo "<tr><td><div>".($row['payment_status']==3 ? 'חזר':'')."</div></td><td>".str2time($row['accept_date'],'d/m/Y')."</td><td>".str2time($row['date'],'d/m/Y')."</td><td nowrap>".htmlspecialchars($row['client_name'])."</td><td>".htmlspecialchars($row['dept'])."</td><td>".htmlspecialchars($row['bank'])."</td><td>".htmlspecialchars($row['account'])."</td><td>".htmlspecialchars($row['number'])."</td><td nowrap>".price($row['sum'],2)."</td>";
		}
		echo "<tr><th colspan=8>סה\"כ</th><th>".price($total)."</th></tr></table>"; 
		echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
		echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='tmp_receipts'.date('d-m-Y').'.csv';
		
		$columns = array('payment_status'=>'סטאטוס',
					'accept_date'=>'תאריך קבלה',
					'date'=>'תאריך פרעון',
					'client_name'=>'שם לקוח',
					'dept'=>'סניף',
					'bank'=>'בנק',
					'account'=>'חשבון',
					'number'=>"מס' שיק",
					'sum'=>'סכום');

		foreach ($rows as $k=>$r) {$row=&$rows[$k]; $row['payment_status']=($r['payment_status']==3 ? 'חזר':'');}
		export_to_excel($filename,$columns,$rows);	return;
	} else echo "{success:true, total:".count($rows).", data:".($rows ? array2json($rows):'[]').", summary:{number:'סה\"כ', sum: $total}}";
}
//————————————————————————————————————————————————————————————————————————————————————
function f_expenses() {
 global $SVARS;
	if (!$_REQUEST['d_from'] OR !$_REQUEST['d_to']) 
		die("{success:false, msg:'Date out of range'}");
	
	if ($_REQUEST['VAT']!=='' AND $_REQUEST['group_id']) {
		$year = str2time($SVARS['company']['current_vat_period'],'y');
	 	$sql="SELECT  acc_expense_rows.sum AS sum, acc_expense_rows.VAT, acc_expense_rows.VAT_sum AS VAT_sum, 'task-folder' AS iconCls "
			.", acc_docs.id, acc_docs.client_id, files.id AS file_id, clients.name AS client_name, acc_expense_ext.sup_doc_num, DATE_FORMAT(acc_expense_ext.sup_doc_date, '%d/%m/%Y') AS sup_doc_date "
			."FROM acc_expense_rows "
			."LEFT JOIN acc_docs ON acc_docs.id = acc_expense_rows.doc_id "
			."LEFT JOIN clients ON acc_docs.client_id = clients.id "
			."LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id "
			."LEFT JOIN files ON files.link=CONCAT('ad',acc_docs.id) AND files.client_id = acc_docs.client_id "
			."WHERE acc_docs.type = 'expense' AND acc_expense_ext.expense_type!=2 AND rep_period > '".($year-1)."12' AND rep_period < '".($year+1)."01' "
			
			."AND acc_expense_rows.group_id='".$_REQUEST['group_id']."'  "
			."AND acc_expense_rows.VAT='".$_REQUEST['VAT']."' ";
		//echo "/*$sql*/";	
		
		$VAT_type_expenses=sql2array($sql);
		
		if ($VAT_type_expenses) 
			foreach ($VAT_type_expenses as $t=>$expense) {
				$VAT_type_expenses[$t]['cls']='x-tree-node-red';
				$VAT_type_expenses[$t]['leaf']=true;
				$VAT_type_expenses[$t]['VAT_sum'] = round($expense['VAT_sum'],2);
			}
	}
	
	if ($_REQUEST['VAT'] AND $_REQUEST['group_id']) {echo ($VAT_type_expenses ? array2json($VAT_type_expenses):'[]'); return;} 
	
	if ($_REQUEST['VAT']!=='') {
		
		$year = str2time($SVARS['company']['current_vat_period'],'y');
		
	 	$sql="SELECT acc_expense_rows.group_id, SUM(acc_expense_rows.sum) AS sum, acc_expense_rows.VAT, SUM(acc_expense_rows.VAT_sum) AS VAT_sum,
			acc_expenses_group.name AS group_name, 'task-folder' AS iconCls "
			."FROM acc_expense_rows "
			."LEFT JOIN acc_docs ON acc_docs.id = acc_expense_rows.doc_id "
			."LEFT JOIN acc_expenses_group ON acc_expenses_group.id = acc_expense_rows.group_id "
			."LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id "
			."WHERE acc_docs.type = 'expense' AND acc_expense_ext.expense_type!=2 AND rep_period > '".($year-1)."12' AND rep_period < '".($year+1)."01'"
			
			."GROUP BY acc_expense_rows.group_id, acc_expense_rows.VAT "
			."HAVING acc_expense_rows.VAT='".$_REQUEST['VAT']."' ";
		echo "/*$sql*/";	
		$VAT_type_expenses=sql2array($sql);
		if ($VAT_type_expenses) 
			foreach ($VAT_type_expenses as $t=>$expense) {
				$VAT_type_expenses[$t]['cls']='cash-node-group1 x-tree-node-blue';
				$VAT_type_expenses[$t]['VAT_sum'] = round($expense['VAT_sum'],2);
			}
	}
	if ($_REQUEST['VAT']) {echo ($VAT_type_expenses ? array2json($VAT_type_expenses):'[]'); return;} 
	
 $year = str2time($SVARS['company']['current_vat_period'],'y');
 
 	$sql="SELECT SUM(acc_expense_rows.sum) AS sum, acc_expense_rows.VAT, SUM(acc_expense_rows.VAT_sum) AS VAT_sum, 'task-folder' AS iconCls "
		."FROM acc_expense_rows "
		."LEFT JOIN acc_docs ON acc_docs.id = acc_expense_rows.doc_id "
		."LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id "
		."WHERE acc_docs.type = 'expense' AND acc_expense_ext.expense_type!=2 AND rep_period > '".($year-1)."12' AND rep_period < '".($year+1)."01'"
		
		."GROUP BY acc_expense_rows.VAT "
		."ORDER BY VAT DESC";
		//echo "/*$sql*/";	
	$all_expenses=sql2array($sql);
	
	if ($all_expenses) {
		foreach ($all_expenses as $V=>$expense) {
			$all_expenses[$V]['cls']='cash-node-group2 x-tree-node-blue';

			$all_expenses[$V]['VAT_type'] = 'מע"מ '.$expense['VAT'].' %';
			$all_expenses[$V]['VAT_sum'] = round($expense['VAT_sum'],2);
			// if ($year['year']==$Year) {
				// if ($year_cheques) {
					// foreach ($year_cheques as $m=>$month) {
						// if ($month['month']==$Month) {
							// if ($month_cheques) {
								// $year_cheques[$m]['expanded'] = true;
								// $year_cheques[$m]['children'] = $month_cheques;
							// }
						// }
					// }
					// $all_expenses[$y]['expanded'] = true;
					// $all_expenses[$y]['children'] = $year_cheques;
				// }
			// }
		}
	}
	
	echo ($all_expenses ? array2json($all_expenses):'[]');
	
	exit;

	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	$type = $_REQUEST['type'];
	

	$where="acc_docs.type='expense' ";
	if ($_REQUEST['status']==1) $where.=" AND acc_docs.sum<>acc_docs.paid ";
	if ($_REQUEST['q']) $make_where_search=make_where_search('last_name,first_name', iconv("UTF-8", "utf-8", $_REQUEST['q']));
	
	
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS acc_docs.type, sup_doc_num, sup_doc_date, sum, acc_docs.notes "
		.", acc_docs.client_id, clients.name AS client_name "
		."FROM acc_docs "
		."LEFT JOIN acc_expense_ext ON acc_docs.id = acc_expense_ext.doc_id "
		."LEFT JOIN clients ON acc_docs.client_id = clients.id "
		."WHERE $where AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."' "
		.($make_where_search ? " AND $make_where_search" :'')
		."ORDER BY acc_docs.date ASC, acc_docs.id ASC"
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;
	
	
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);	
	
	if ($rows) {
		$sql = "SELECT SQL_CALC_FOUND_ROWS SUM(sum) AS sum "
		."FROM acc_docs "
		."LEFT JOIN clients ON acc_docs.client_id = clients.id "
		."WHERE $where AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."' "
		;
		$total=sql2array($sql,'','',1);
	}
	
	if ($_GET['print'] AND $rows) {//Open&Print PopUp window
		header("Content-Type: text/html;charset=utf-8");
		echo "<thml><head>".printStyle()."</head>
		<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
		<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים דוח\")) window.print()'>";
		
		echo "<table width=100%><tr><td align=right>הודפס ע''י: ".($SVARS['fullname']?$SVARS['fullname']:$SVARS['username'])."</td><td align=left>תאריך הדפסה: ".date('d/m/Y H:i')."</td></tr></table>";
		
		echo "<center><h1><u>רשימת הוצאות".($_REQUEST['status']==1?' שלא שולמו':'')."</h1><h3>מתאריך: ".str2time($_GET['d_from'],'d/m/Y')." &nbsp;&nbsp; עד תאריך: ".str2time($_GET['d_to'],'d/m/Y')."</h3></u><br><br>";
		echo "</center><br>";
		
		echo "<table width=100% class=t_list>";
		echo '<tr><th width=8>#</th><th>ספק</th><th>תאריך הוצאה</th><th>מס\' אסמכתא</th><th>סכום</th><th>הערות</th>';
		foreach($rows as $r) {
			$n++;
			echo "<tr><td widtd=8>$n</td><td>".htmlspecialchars($r['client_name'])."</td><td align=right style='direction:ltr;'>".str2time($r['sup_doc_date'],'d/m/Y')."</td><td align=right style='direction:ltr;'>".htmlspecialchars($r['sup_num'])."</td><td align=right style='direction:ltr;'>".price($r['sum'],2)."</td><td>".htmlspecialchars($r['notes'])."</td>"; 
			// $total+=$r['sum'];
		}
		echo "<tr><th width=8> </th><th colspan=3>סה\"כ</th><th align=right style='direction:ltr;'>".price($total['sum'],2)."</th><th></th>";

		echo "</table>"; 
		echo "<div style='font-size:7px'><br>הודפס ע''י באפי - <span dir=ltr style='font-size:7px'>© 2009-2010 B.A.F.I. Best Advice for Insurance </span></div>";			
		echo "</body></html>";
	}elseif ($_GET['export'] AND $rows) {//Export to Excel
		$filename='expenses'.date('d-m-Y').'.csv';
		
		$columns = array('client_name'=>'שם לקוח',
					'sup_doc_date'=>'תאריך הוצאה',
					'sup_num'=>"מס' אסמכתא",
					'sum'=>'סכום לפני מע"מ',
					'notes'=>'הערות'
					);
					
		foreach ($rows as $k=>$r) {$row=&$rows[$k]; $row['type']=$storeTypes[$r['type']];}
		export_to_excel($filename,$columns,$rows);	return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($rows ? array2json($rows):'[]').", summary:{client_name:'סה\"כ', sum:".round($total['sum'],2)."}}";
	}

}

//————————————————————————————————————————————————————————————————————————————————————
function f_export() {
 global $SVARS, $group_id, $acc_cards;
	$types = array('invoice_credit'=>'חשבונית מס','invoice_debt'=>'חשבונית מס זיכוי','invoice_accept'=>'חשבונית מס קבלה','expense'=>'קליטת הוצאות');
 	$_REQUEST['q'] = iconv("UTF-8", "utf-8", $_REQUEST['q']); 
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if (!$_REQUEST['d_from'] OR !$_REQUEST['d_to']) die("{success:false, msg:'Date out of range'}");
	
	$acc_cards=sql2array("SELECT * FROM acc_user_cards WHERE group_id=$group_id ORDER BY card_id",'card_id','number');
	
	// echo "<div dir=ltr align=left><pre>".print_r($acc_cards,1)."</pre></div>";
	
	$where="acc_operations.group_id IN($group_id) AND acc_operations.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_operations.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'";

	$docs		=sql2array("SELECT acc_docs.*, clients.client_id, TRIM(CONCAT(last_name,' ',first_name)) AS client_name, clients.acc_card AS client_account, passport FROM acc_docs LEFT JOIN clients ON acc_docs.client_id = clients.client_id WHERE acc_docs.group_id IN($group_id) AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'",'id');
	$acceptRows	=sql2array("SELECT acc_accept_rows.* FROM acc_accept_rows, acc_docs WHERE acc_docs.id=acc_accept_rows.accept_id AND acc_docs.group_id IN($group_id) AND acc_docs.date>='".str2time($_REQUEST['d_from'],'Y-m-d')."' AND acc_docs.date<='".str2time($_REQUEST['d_to'],'Y-m-d')."'",'id');
	
	// echo "<div dir=ltr align=left><pre>".print_r($acceptRows,1)."</pre></div>";
	// $rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	/*
	$sql = "SELECT acc_operations.* "//, SUM(sum) AS sum, acc_docs.type, acc_docs.num 
		// .", TRIM( CONCAT(last_name,' ',first_name)) AS client_name "
		."FROM acc_operations "
		// ."LEFT JOIN acc_docs ON acc_docs.id = acc_operations.doc_id "
		// ."LEFT JOIN clients ON acc_docs.client_id = clients.client_id "
		."WHERE $where "
		// ."GROUP BY doc_id, credit_card, credit_sub1, credit_sub2, credit_sub3, debt_card, debt_sub1, debt_sub2, debt_sub3 "
		."ORDER BY date ASC "
		.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
		;

	// echo $where;
	$rows=sql2array($sql);
	if ($rows) foreach($rows as $k=>$r) $docs[$r['doc_id']]['operations'][]=$r;*/
	
	if ($acceptRows) foreach($acceptRows as $k=>$r) $docs[$r['accept_id']]['operations'][]=$r;
	if ($docs) foreach($docs as $k=>$d) {
		$clients[$d['client_account']] = array ($d['client_id'],$d['client_name'],$d['passport']);
		if ($d['type']=='invoice_credit'){
			$expRow = &$export[];
			$expRow['type']	=$d['type'];
			$expRow['num']	=$d['num'];
			$expRow['date1']=$d['date'];
			$expRow['date2']=$d['date'];
			$expRow['notes']=$d['client_name'];
			// $expRow['debt1']=$d['client_account'];
			$expRow['debt1']=$d['client_id'];
			$expRow['credit1']		=$acc_cards[8001];
			$expRow['debt1sum']		=$d['sum'];
			$expRow['credit1sum']	=$d['sum']-$d['vat_sum'];
			$expRow['debt2']		='';
			$expRow['credit2']		=$acc_cards[5502];
			$expRow['debt2sum']		=0;
			$expRow['credit2sum']	=$d['vat_sum'];
		}elseif ($d['type']=='invoice_accept'){
			$expRow = &$export[];
			$expRow['type']	=$d['type'];
			$expRow['num']	=$d['num'];
			$expRow['date1']=$d['date'];
			$expRow['date2']=$d['date'];
			$expRow['notes']=$d['client_name'];
			// $expRow['debt1']=$d['client_account'];
			$expRow['debt1']=$d['client_id'];
			$expRow['credit1']		=$acc_cards[8001];
			$expRow['debt1sum']		=$d['sum'];
			$expRow['credit1sum']	=$d['sum']-$d['vat_sum'];
			$expRow['debt2']		='';
			$expRow['credit2']		=$acc_cards[5502];
			$expRow['debt2sum']		=0;
			$expRow['credit2sum']	=$d['vat_sum'];
			if ($d['operations']) foreach($d['operations'] as $o) {
				// if ($o['debt_class']==100) {//קבלה
					$expRow = &$export[];
					$expRow['type']	=$d['type'];
					$expRow['num']	=$o['number'];
					$expRow['num2']	=$d['num'];
					$expRow['date1']=$d['date'];
					$expRow['date2']=$d['date'];
					$expRow['notes']=$d['client_name'];
					$expRow['debt1']=$acc_cards[$o['debt_card']];
					// $expRow['credit1']=$d['client_account'];
					$expRow['credit1']=$d['client_id'];
					$expRow['debt1sum']		=$d['sum'];
					$expRow['credit1sum']	=$d['sum'];
					$expRow['debt2']		='';
					$expRow['credit2']		='';
					$expRow['debt2sum']		=0;
					$expRow['credit2sum']	=0;
				// }
			}
		}elseif ($d['type']=='invoice_debt'){
			$expRow = &$export[];
			$expRow['type']	=$d['type'];
			$expRow['num']	=$d['num'];
			$expRow['date1']=$d['date'];
			$expRow['date2']=$d['date'];
			$expRow['notes']=$d['client_name'];
			$expRow['debt1']=$acc_cards[8001];
			// $expRow['credit1']=$d['client_account'];		
			$expRow['credit1']=$d['client_id'];		
			$expRow['debt1sum']=$d['sum']-$d['vat_sum'];		
			$expRow['credit1sum']=$d['sum'];	
			$expRow['debt2']=$acc_cards[5502];		
			$expRow['credit2']='';		
			$expRow['debt2sum']=$d['vat_sum'];		
			$expRow['credit2sum']=0;	
		}elseif($d['type']=='accept'){
			if ($d['operations']) foreach($d['operations'] as $o) {
				$expRow = &$export[];
				$expRow['type']	=$d['type'];
				$expRow['num']	=$o['number'];
				$expRow['num2']	=$d['num'];
				$expRow['date1']=$d['date'];
				$expRow['date2']=$o['date'];
				$expRow['notes']=$d['client_name'];
				$expRow['debt1']=$acc_cards[$o['debt_card']];
				// $expRow['credit1']=$d['client_account'];
				$expRow['credit1']=$d['client_id'];
				$expRow['debt1sum']		=$o['sum'];
				$expRow['credit1sum']	=$o['sum'];
				$expRow['debt2']		='';
				$expRow['credit2']		='';
				$expRow['debt2sum']		=0;
				$expRow['credit2sum']	=0;
			}
		}elseif($d['type']=='cash_payment'){
			$expRow = &$export[];
			$expRow = array('type'=>$d['type'],'num'=>$d['number'],'date1'=>$d['date'],'date2'=>$d['date'],'notes'=>$d['client_name'],'debt1'=>$d['client_id'],'credit1'=>$acc_cards[1001],'debt1sum'=>$d['sum'],'credit1sum'=>$d['sum'],'debt2'=>'','credit2'=>'','debt2sum'=>0,'credit2sum'=>0);
		}elseif($d['type']=='refund_bankclient'){
			$expRow = &$export[];
			$expRow = array('type'=>$d['type'],'num'=>$d['number'],'date1'=>$d['date'],'date2'=>$d['date'],'notes'=>$d['client_name'],'debt1'=>$d['client_id'],'credit1'=>$acc_cards[1001],'debt1sum'=>$d['sum'],'credit1sum'=>$d['sum'],'debt2'=>'','credit2'=>'','debt2sum'=>0,'credit2sum'=>0);
		}elseif($d['type']=='refund_cashclient'){
			$expRow = &$export[];
			$expRow = array('type'=>$d['type'],'num'=>$d['number'],'date1'=>$d['date'],'date2'=>$d['date'],'notes'=>$d['client_name'],'debt1'=>$d['client_id'],'credit1'=>$acc_cards[1001],'debt1sum'=>$d['sum'],'credit1sum'=>$d['sum'],'debt2'=>'','credit2'=>'','debt2sum'=>0,'credit2sum'=>0);
		}elseif($d['type']=='refund_bankcash'){
			$expRow = &$export[];
			$expRow = array('type'=>$d['type'],'num'=>$d['number'],'date1'=>$d['date'],'date2'=>$d['date'],'notes'=>$d['client_name'],'debt1'=>$d['client_id'],'credit1'=>$acc_cards[1001],'debt1sum'=>$d['sum'],'credit1sum'=>$d['sum'],'debt2'=>'','credit2'=>'','debt2sum'=>0,'credit2sum'=>0);
		}
	}

	$rowsCount=count($export);

	if ($_GET['export'] AND $_GET['type']=='hashavshevet' AND $export) {//Export to Hashavshevet
			export_to_zip(operations_to_hashavshevet($export),clients_to_hashavshevet($clients)); return;
	}else{
		echo "{success:true, total:'$rowsCount', data:".($export ? array2json($export):'[]').'}';
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function export_to_excel($filename,$columns,$data) {
	header("Content-Type: text/csv");	
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	header("Content-Transfer-Encoding: binary");
	header('Accept-Ranges: bytes');

	// header('Content-Type: encoding/octet-stream');
	// header('Content-Type: encoding/force-download');
	// ob_end_clean();		// Make sure the headers have been sent to the client.
	
	foreach ($columns as $column) $title.=($title?',':'').'"'.iconv('utf-8','windows-1255',str_replace('"','""', $column)).'"'; echo $title."\r\n";
	
	foreach ($data as $k=>$r) {
		unset($row);
		foreach ($columns as $key=>$column) $row.=($row?',':'').'"'.iconv('utf-8','windows-1255',str_replace('"','""', strip_tags($r[$key]))).'"'; echo $row."\r\n";
	}
	flush();
	
	exit;
}
//————————————————————————————————————————————————————————————————————————————————————
function operations_to_hashavshevet($data) {
	foreach ($data as $k=>$r) {
		$content.= sprintf("%' 11s",  $r['num']);
		$content.= sprintf("%' 8s",  $r['num2']);
		$content.= str2time($r['date1'],'dmy');
		$content.= str2time($r['date2'],'dmy');
		$content.= sprintf("%' 22s", strrev(substr(iconv("utf-8","cp862", $r['notes']),0,22))); 
		$content.= sprintf("%' 8s",  $r['debt1']);
		$content.= sprintf("%' 8s",  $r['credit1']);
		$content.= sprintf("%' 12s", number_format($r['debt1sum'], 2, '.', ''));
		$content.= sprintf("%' 12s", number_format($r['credit1sum'], 2, '.', ''));
		$content.= sprintf("%' 8s", $r['debt2']);
		$content.= sprintf("%' 8s", $r['credit2']);
		$content.= sprintf("%' 12s", number_format($r['debt2sum'], 2, '.', ''));
		$content.= sprintf("%' 12s", number_format($r['credit2sum'], 2, '.', ''));
		$content.= "\n";
	}
	return $content;
}
//————————————————————————————————————————————————————————————————————————————————————
function clients_to_hashavshevet($data) {
 global $acc_cards;
	foreach ($data as $k=>$r) {
		$content.= sprintf("%' 8s",  $r[0]);
		$content.= sprintf("%' 20s", strrev(substr(iconv("utf-8","cp862", $r[1]),0,20))); 
		$content.= $acc_cards[2001];
		$content.= sprintf("%' 11s", $r[2]);
		$content.= "\n";
	}
	return $content;
}
//————————————————————————————————————————————————————————————————————————————————————
function export_to_zip($data,$accounts) {
	include_once 'inc/zip.php';
	$fileTime = date("D, d M Y H:i:s T");
	$zip = new Zip();
	$zip->setComment("Created on " . date('l jS \of F Y h:i:s A'));
	$zip->addFile($data, "Movein.DOC");
	$zip->addFile(file_get_contents("modules/accounting/Movein.PRM"), "Movein.PRM");
	$zip->addFile($accounts, "Heshin.DAT");
	$zip->addFile(file_get_contents("modules/accounting/Heshin.PRM"), "Heshin.PRM");
	$zip->sendZip("Movein.zip");
}
//————————————————————————————————————————————————————————————————————————————————————
function printLetters($rows) {
 global $SVARS, $group_id;
	if ($_GET['email']) $type='email';
	elseif ($_GET['sms']) $type='sms';
	else $type='pdf';
	
	if($type=='sms') $letter=trim($_POST['notes']);
	else $letter=trim($_REQUEST['htmlCode']);
	if (!$letter) die('{success:false, msg:"message is empty"}');
	
	if ($type=='email' OR $type=='sms') $letter = iconv("UTF-8", "utf-8", $letter);

	if ($type=='pdf') {
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=address_stickers');
		header('Connection: close');
		ob_end_clean();		// Make sure the headers have been sent to the client.
		
		require_once('inc/phpexcel/Classes/PHPExcel/Shared/PDF/tcpdf.php');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 

		// $l['a_meta_charset'] = 'utf-8';
		// $l['a_meta_language'] = 'iw';
		//$l['w_page'] = 'page';
		$l['a_meta_dir'] = 'rtl';
		$pdf->setLanguageArray($l); 
		$pdf->SetMargins(15, 15, 15);
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->SetFont('arial', '', 12);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetCreator('B.A.F.I. - Best Advice for Insurance');
		
		$letter=str_replace('&nbsp;', ' ', $letter);
		$letter=eregi_replace('font size="?1"?','font style="font-size:20px"', $letter);
		$letter=eregi_replace('font size="?2"?','font style="font-size:28px"', $letter);
		$letter=eregi_replace('font size="?3"?','font style="font-size:38px"', $letter);
		$letter=eregi_replace('font size="?4"?','font style="font-size:48px"', $letter);
		$letter=eregi_replace('font size="?5"?','font style="font-size:58px"', $letter);
		$letter=eregi_replace('font size="?6"?','font style="font-size:68px"', $letter);
		$letter=eregi_replace('font size="?7"?','font style="font-size:96px"', $letter);
		$letter=eregi_replace('font color="?#?([0-9A-Z]+)"?', 'font style="color:#\\1"', $letter);
		$letter=eregi_replace('<p', '<div', $letter);
		$letter=eregi_replace('</p>', '</div>', $letter);
		$letter=eregi_replace('<br>','<br/>', $letter);
	
	}elseif ($type=='email') { # get user email
		include_once "inc/mail/sendmail.php";
		$from=sql2array("SELECT CONCAT(fname,' ',lname) as fullname, email, o_email, c_email FROM users WHERE user_id=$SVARS[user_id]",'','',1);
		if ($from['email']) $replyto=$from['email'];
		elseif ($from['o_email']) $replyto=$from['o_email'];
		elseif ($from['c_email']) $replyto=$from['c_email'];
		else $replyto='support@bafi.co.il';
	}
		
	# check if has variables in html
	$hasVars=false; if (eregi('{[a-z]+}',$letter)) $hasVars=true;

	# group by client
	foreach($rows as $k=>$r) $rowsByClient[$r['client_id']][]=&$rows[$k];

	$policeCatNames=array('shuk'=>'שוק ההון', 'car'=>'רכב', 'home'=>'דירה', 'enterprise'=>'עסק', 'accident'=>'תאונות אישיות', 'liability'=>'חבויות', 'equipment'=>'צ.מ.ה.', 'road'=>'שרותי דרך');
	//$cat = $policeCatNames[$c['police_cat']] ? $policeCatNames[$c['police_cat']] : 'אחר';
	
	
	# walk through clients
	foreach($rowsByClient as $polices) {
		$policeIds=array();
		foreach($polices as $c) {	# walk through polices, get policeNums & policeTypes
			$policeNums[]=$c['police_num'];
			$cat = 'ביטוח '. ($policeCatNames[$c['police_cat']] ? $policeCatNames[$c['police_cat']] : 'אחר');
			$policeTypes[$cat]=true;		# police cat name grouped by type
			$policeIds[]=$c['police_id'];	# save police id numbers
		}
		$c['police_num']=implode(', ', $policeNums);
		$c['insType']=implode(', ', array_keys($policeTypes));
		$c['policeIds']=implode(',', $policeIds);
		unset($policeNums, $policeTypes, $policeIds);
		
		//print_ar("police_id:$c[police_id] - police_num:$c[police_num] - insType:$c[insType] - client_name:$c[client_name]",'');
		//echo "<hr>";
		$html=$letter;
		if ($hasVars) $html=letterReplaceVars($c, $html);
		
		if($type=='sms'){
			//print_ar($c);
			# get first cell phone number
			for($i=0; $i<=4; $i++) if ($tel=ereg_replace('[^0-9]+','',$c["tel$i"]) AND ereg('^(0|972)(5[024][0-9]{7})$', $tel, $regs)) {
				$tel='0'.$regs[2];
				if ($hasVars) $sms_ar_by_tel[$tel]=$html;	# different messages for different numbers (group by phone)
				else $tels[$tel]=true;						# same messsge for all numbers
				//$telPolices[$tel]=$c['policeIds'];			# police ids by phone
				$telClients[$tel]=$c;						# client by phone
				break;
			}
			

		}elseif ($type=='email' AND $c['email']) {
			if (send_mail($c['email'], 'מכתב חידוש', "<div dir=rtl>$html</div>", array('from'=>"$from[fullname] <support@bafi.co.il>", 'replyto'=>"$from[fullname] <$replyto>")) ) {
				$policeIds.=($policeIds?',':'').$c['policeIds']; $count++;
				$html=eregi_replace('</?font([^>]*)>','', $html);
				runsql("INSERT DELAYED contacts SET client_id=$c[client_id], client_type='$c[client_type]', user_id=$SVARS[user_id], type='mail', them='מכתב חידוש (אל: $c[email])', notes='".sql_escape($html)."'");
			}
		}elseif($type=='pdf'){
			$policeIds.=($policeIds?',':'').$c['policeIds'];
			$pdf->AddPage(); $pdf->writeHTML( iconv("utf-8", "UTF-8", $html) ); 
			$html=eregi_replace('</?font([^>]*)>','', $html);
			runsql("INSERT DELAYED contacts SET client_id=$c[client_id], client_type='$c[client_type]', user_id=$SVARS[user_id], type='mail', them='מכתב חידוש', notes='".sql_escape($html)."'");
		}
	}
	

	if ($type=='pdf') $pdf->Output('Letters.pdf', 'I');//Close and output PDF document
	elseif ($type=='email') echo "{success:true, total:".(int)$count."}";
	elseif ($type=='sms') {
		if ($hasVars) {		# different messages for different numbers (rebuild array)
			foreach($sms_ar_by_tel as $tel=>$msg) $sms_ar[]=array($tel, $msg);
		}elseif ($tels) {	# same messsge for all numbers
			$tels=array_keys($tels);
			$sms_ar[]=array(implode(',',$tels), $html);
		}
		if (!$sms_ar) die('{success:false, msg:"no phones found"}');
		// print_ar($sms_ar,'$sms_ar');
		// print_ar($telPolices,'$telPolices');
		
		include_once 'inc/sms.php';
		$send_sms = send_mass_sms($sms_ar);
		if (!is_array($send_sms)) die("{success:false, msg:'".jsEscape($send_sms)."'}");
		if (is_array($send_sms)) list($send_success, $send_fail, $sms_credits, $errors) = $send_sms;
		$count=count($send_success);
		$fail_count=count($send_fail);
		echo "{success:true, total:$count, fail_total:$fail_count, credits:".(int)$sms_credits.", errors:".array2json($errors)." }";
		
		if ($send_success) {
			$policeIds='';
			foreach($send_success as $t) {
				$c=$telClients[$t];
				$policeIds.=($policeIds?',':'').$c['policeIds']; //$telPolices[$t];
				
				$notes = $hasVars ? $sms_ar_by_tel[$t] : $html;
				runsql("INSERT DELAYED INTO crm_contacts SET client_id=$c[client_id], client_type='client', user_id=$SVARS[user_id], type='sms', them='SMS אל $t', notes='".sql_escape($notes)."'");
			}
			if ($policeIds) runsql("UPDATE crm_policies SET renew=31, user_id=$SVARS[user_id] WHERE police_id IN($policeIds)");
		}
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function letterReplaceVars($c, $html) {
	$html=str_replace('{clientName}', $c['client_name'], $html);
	$html=str_replace('{clientAddress}', $c['client_addresses'], $html);
	$html=str_replace('{clientCity}', $c['city0'], $html);
	$html=str_replace('{clientAdr}', $c['adr0'], $html);
	$html=str_replace('{clientZip}', $c['zip0'], $html);
	$html=str_replace('{insType}', $c['insType'], $html);
	$html=str_replace('{policeNum}', $c['police_num'], $html);
	$html=str_replace('{carNum}', $c['car_num'], $html);
	$html=str_replace('{policeEnd}', str2time($c['end'],'d/m/Y'), $html);
	$html=str_replace('{policeStart}', str2time($c['start'],'d/m/Y'), $html);
	$html=str_replace('{dateEnd}', str2time($c['end'],'d/m/Y'), $html);
	$html=str_replace('{dateStart}', str2time($c['start'],'d/m/Y'), $html);
	$html=eregi_replace('{today}', date('d/m/Y'), $html);
 return $html;
}
//————————————————————————————————————————————————————————————————————————————————————
function printStickers($rows) {
	if ($_GET['debug']) {echo count($rows); exit;}

	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename=address_stickers');
	header('Connection: close');
	ob_end_clean();		// Make sure the headers have been sent to the client.
	
	require_once('inc/phpexcel/Classes/PHPExcel/Shared/PDF/tcpdf.php');
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 

	$l['a_meta_charset'] = 'utf-8';
	$l['a_meta_dir'] = 'rtl';
	$l['a_meta_language'] = 'he';
	//$l['w_page'] = 'page';
	$pdf->setLanguageArray($l); 
	$pdf->SetAutoPageBreak(false, 0);
	$pdf->SetFont('arial', '', 12);
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	$pdf->SetCreator('B.A.F.I. - Best Advice for Insurance');
	
	# group by client
	foreach($rows as $k=>$r) $rowsByClient[$r['client_id']]=&$rows[$k];	unset($rows);
	
	$pageWidth=210; $pageHeight=297;
	
	if($_REQUEST['format']) list($colsOnPage,$rowsOnPage) = explode(",", $_REQUEST['format']); else {$colsOnPage=3; $rowsOnPage=7;}
	
	$rightMargin = 5;
	
	if ($rowsOnPage==8 AND $colsOnPage==4) {$cellHeight=35; $cellWidth=52.5; $topMargin=15;}
	elseif ($rowsOnPage==8 AND $colsOnPage==3) {$cellHeight=35; $cellWidth=70; $topMargin=10;}
	elseif ($rowsOnPage==12 AND $colsOnPage==3) {$cellHeight=24; $cellWidth=63; $topMargin=5;$rightMargin=13;}
	elseif ($rowsOnPage==11 AND $colsOnPage==3) {$cellHeight=25.5; $cellWidth=70; $topMargin=10;}
	else {
		$topMargin = 20;
		$cellHeight=(int)(($pageHeight-$topMargin-10)/$rowsOnPage);
		$cellWidth=(int)($pageWidth/$colsOnPage);
	}
	
	$pdf->SetMargins(0, $topMargin, $rightMargin);//void   SetMargins   (float $left, float $top, [float $right  = -1]) 
	
	$cCount=$rCount=0;
	foreach($rowsByClient as $c) {
		$html.='<td style="height:'.$cellHeight.'mm; width:'.$cellWidth.'mm;" valign="middle"><div style="padding:10px;">לכבוד:</div><div><b>'.$c['client_name'].'</b></div>'//.'<img src="inc/tcpdf/images/logo_example.png" alt="test alt attribute" width="30" height="30" border="0" />'
			.($c['adr0'] ? '<div style="padding:10px;">'.$c['adr0'].',</div>':'').'<div style="padding:10px;">'.$c['city0'].(($c['city0'] AND $c['zip0'])?', ':'')."$c[zip0]</div></td>";
		$cCount++;
		if ($cCount==$colsOnPage) {$html.='</tr>'.($rCount==$rowsOnPage?'':'<tr>'); $cCount=0; $rCount++;}
		if ($rCount==$rowsOnPage) {
			$html=iconv("utf-8", "UTF-8", '<table dir="rtl"><tr>'.$html.'</table>');
			$pdf->AddPage(); $pdf->writeHTML($html); 
			//echo $html;
			unset($html,$rCount);
		}
	}
	
	if ($html) {
		$html=iconv("utf-8", "UTF-8", '<table dir="rtl"><tr>'.$html.($cCount?'</tr>':'').'</table>');
		$pdf->AddPage(); $pdf->writeHTML($html); 
		//echo $html;
		unset($html,$rCount);
	}
	$pdf->Output('Lables.pdf', 'I');//Close and output PDF document
}
//————————————————————————————————————————————————————————————————————————————————————
function sort2d ($array, $index, $order='ASC', $natsort=FALSE, $case_sensitive=FALSE){
	$order=strtoupper(trim($order));
	if(is_array($array) && count($array)>0) {
		foreach(array_keys($array) as $key)	$temp[$key]=$array[$key][$index];
		if(!$natsort) ($order=='ASC')? asort($temp) : arsort($temp);
		else {
			($case_sensitive)? natsort($temp) : natcasesort($temp);
			if ($order!='ASC') $temp=array_reverse($temp,TRUE);
		}
		foreach(array_keys($temp) as $key)	(is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
		return $sorted;
	}
return $array;
}
//————————————————————————————————————————————————————————————————————————————————————
function set_title($g,$v,$r,$n) {
 global $prms_groups, $crm_agents, $insTypes;

	$grouping = array('agent_id'=>'סוכן ביטוח','firm'=>'חברת ביטוח','renew'=>'סטאטוס','status'=>'סטאטוס','police_cat'=>'סוג ביטוח','claim_cat'=>'סוג תביעה','user_id'=>'מטפל','type'=>'סוג פעולה');
	$renew = array(0=>'לא טופל',11=>'לא חודש עקב מחיר',12=>'לא חודש עקב מיגון',13=>'לא חודש עקב שרות לקוי',14=>'לא חודש עקב סיבה אחרת',20=>'חודש',30=>'בטיפול');
	$status = array (1=>'פתוח',2=>'סגור',3=>'הודעה בלבד');
	$contact_types = array ('phone'=>'שיחה','mail'=>'דוא"ל','post'=>'דואר יוצא','sms'=>'SMS','task'=>'משימה');
	if ($g=='renew') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>$renew[$v]</b></u></em></div>";
	elseif ($g=='firm') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>$v</b></u></em></div>";
	elseif ($g=='police_cat' OR $g=='claim_cat') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>".($insTypes[$v]?$insTypes[$v]:'אחר')."</b></u></em></div>";
	elseif ($g=='agent_id') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><u><em>{$grouping[$g]}: <b>".($crm_agents[$v]?$crm_agents[$v]:'ללא סוכן')."</b></u></div>";
	elseif ($g=='user_id') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>".htmlspecialchars($r['username'])."</b></u></div>";
	elseif ($g=='status') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>".$status[$v]."</b></u></div>";
	elseif ($g=='type') return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>".$contact_types[$v]."</b></u></div>";

	return "<div style='".($n==3?"padding-right:30px; ":'')."font-size:14px; text-align:".($n!=1?"right":"center")."'><em><u>{$grouping[$g]}: <b>$v</b></u></div>";
}
//————————————————————————————————————————————————————————————————————————————————————
function printStyle() {
return "<style>
body, a, p, div, span, th, td {font-size:10px; font-family: Tahoma, Arial, Helvetica }
.t_list, .t_title {border-collapse:collapse}
.t_list th {border-top:2px solid silver; border-bottom:2px solid silver; background-color:#eaeaea; font-size:11px; text-align:right; padding:2px 3px}
.t_list td {border-top:1px solid silver; font-size:10px; padding-right:3px; }

.t_title td {border: 0 !important;}
a, a:visited { text-decoration:none; color:black }
a:hover {text-decoration:none; color:black}
a:active {color:black}
.tborder td, .tborder th {border-right:1px solid silver; border-bottom:1px solid silver;}
.btop td {border-top:1px solid silver}
.tfoot td {border-top:3px double silver; font-weight:bold}
.total td {font-size:12px !important;}
</style>";
}
?>