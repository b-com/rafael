<?php
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF']) OR !$SVARS) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

$NO_SKIN=1;

// include_once 'modules/common/crm_inc.php';
if ($_REQUEST['ids']) {
	foreach(split(',',$_REQUEST['ids']) as $id) if ((int)$id) {
		echo printDoc($id, $_GET['type'], 0, $_GET['pattern_id'])."<div style='page-break-before:always'></div>";
	}
} elseif ((int)$_REQUEST['id']) echo printDoc((int)$_REQUEST['id'], $_GET['type'], 0, $_GET['pattern_id']);

// else die("{success:false, msg:'מסמך לא קיים!!!'}");

// include_once "../inc/wkhtmltopdf.php";		
// wkhtmltopdf(printDoc($id, $_GET['type'],0, $_GET['pattern_id']), 'embed');	
//————————————————————————————————————————————————————————————————————————————————————
function printDoc($id, $docType, $original_only=0, $pattern_id=0, $print=true) {
global $SVARS, $CFG;
$vat_rate =sql2array("SELECT t1.vat_rate as vat_rate FROM VAT_Rates t1 , VAT_Rates t2, acc_docs
				     where t1.from_date<(select acc_docs.date as date from acc_docs order by id desc limit 1) 
					 and (select acc_docs.date as date from acc_docs order by id desc limit 1)<t2.from_date 
				     and t1.id = t2.id-1 limit 1",0,'vat_rate',1) ;
	$doc_types = array (
		'payment'=>'תשלום',
		'cash_payment'=>'תשלום במזומן',
		'order'=>'הזמנה',
		'offer'=>'הצעת מחיר',
		'refund_bankclient'=>'החזרת שיקים מבנק ללקוח',
		'refund_bankcash'=>'החזרת שיקים מבנק לקופה',
		'refund_cashclient'=>'החזרת שיקים מקופה ללקוח',
		'deposit'=>'הפקדה לחשבון בנק',
		'expense'=>'קליטת הוצאות',
		'invoice_credit'=>'חשבונית מס',
		'invoice_debt'=>'חשבונית מס זיכוי',
		'invoice_accept'=>'חשבונית מס קבלה',
		'accept'=>'קבלה',
		'accept_tmp'=>'קבלה זמנית'
	);

	$inv_types = array (
		'part'=>'חלפים',
		'material'=>'חומרים',
		'service'=>'עבודות',
		'fserv'=>'עבודות חוץ',
		'discount'=>'הנחות'
	);

	$sql="SELECT acc_orders.*, acc_docs.*, "
		."clients.name, clients.cnumber, "
		."clients.tel_type0, clients.tel0, clients.tel_type1, clients.tel1, clients.tel_type2, clients.tel2, clients.tel_type3, clients.tel3, clients.tel_type4, clients.tel4, "
		."adr_type0, clients.city0, adr0, zip0, adr_type1, clients.city1, adr1, zip1, adr_type2, clients.city2, adr2, zip2 "
		."FROM acc_docs "
		."LEFT JOIN acc_orders ON (acc_docs.parent_doc_id=acc_orders.id) "
		."INNER JOIN clients ON acc_docs.client_id=clients.id "
		."WHERE acc_docs.id=$id";
		
	// if ($docType=='invoice_credit' OR $docType=='invoice_accept') $tax_clean = sql2array("SELECT tax_clean FROM companies WHERE id=$group_id",0,'tax_clean',1);
		
	if ($docType=='deposit') {
		$sql="SELECT acc_accept_rows.*, acc_deposit_rows.sum, acc_deposit_rows.type AS ptype, TRIM(CONCAT(clients.first_name,' ',clients.last_name)) AS client_name "
			."FROM acc_deposit_rows "
			."LEFT JOIN acc_accept_rows ON (acc_deposit_rows.cheque_id=acc_accept_rows.id) "
			."LEFT JOIN acc_docs ON (acc_accept_rows.accept_id=acc_docs.id) "
			."LEFT JOIN clients ON (acc_docs.client_id=clients.client_id) "
			."WHERE deposit_id=$id ORDER BY acc_deposit_rows.type, acc_deposit_rows.id";
		$deposit_rows = sql2array($sql);
		
		$sql="SELECT acc_docs.*, bank_name, branch_code, branch_name, acc_bank_accounts.tel, acc_bank_accounts.fax "
			."FROM acc_docs "
			."INNER JOIN acc_bank_accounts ON acc_docs.account_id=acc_bank_accounts.id "
			."LEFT JOIN acc_banks ON (acc_bank_accounts.bank=acc_banks.bank_code AND acc_bank_accounts.branch=acc_banks.branch_code) "
			."WHERE acc_docs.id=$id";
	}elseif ($docType=='refund' OR $docType=='refund_bankclient' OR $docType=='refund_bankcash' OR $docType=='refund_cashclient') {
		$accept_rows = sql2array("SELECT acc_accept_rows.*, acc_deposit_rows.sum, acc_deposit_rows.type AS ptype FROM acc_deposit_rows LEFT JOIN acc_accept_rows ON (acc_deposit_rows.cheque_id=acc_accept_rows.id) WHERE deposit_id=$id ORDER BY type, acc_deposit_rows.id");
	}elseif ($docType=='expense') {
		$expense_rows = sql2array("SELECT acc_expense_rows.*, acc_expense_ext.sup_doc_date, acc_expense_ext.sup_doc_num FROM acc_expense_rows, acc_expense_ext WHERE acc_expense_rows.doc_id=$id AND acc_expense_ext.doc_id=$id ORDER BY type, id");
	}elseif ($docType=='order') {
		$sql="SELECT acc_orders.*, 'order' AS type, "
			."clients.*, acc_orders.notes AS notes "
			// ."clients.first_name, clients.last_name, clients.cnumber, clients.birthday, "
			// ."clients.tel_type0, clients.tel0, clients.tel_type1, clients.tel1, clients.tel_type2, clients.tel2, clients.tel_type3, clients.tel3, clients.tel_type4, clients.tel4, clients.email, "
			// ."adr_type0, clients.city0, adr0, zip0, adr_type1, clients.city1, adr1, zip1, adr_type2, clients.city2, adr2, zip2 "
			."FROM acc_orders "
			."INNER JOIN clients ON acc_orders.client_id=clients.client_id "
			."WHERE acc_orders.id=$id";
			$invoice_rows = sql2array("SELECT * FROM acc_order_rows WHERE card_id=$id ORDER BY id");
	}elseif ($docType=='payment') {
		$accept_rows = sql2array("SELECT acc_accept_rows.*, "
			."IF (acc_accept_rows.ptype='שיק',acc_bank_accounts.number,'') AS account, "
			."IF ((acc_accept_rows.ptype='שיק' OR acc_accept_rows.ptype='ה.קבע/ה.בנקאית'),CONCAT(acc_banks.bank_code,' - ',acc_banks.bank_name),acc_payment_methods.type) AS bank FROM acc_accept_rows "
			."LEFT JOIN acc_bank_accounts ON (acc_bank_accounts.id=acc_accept_rows.account) "
			."LEFT JOIN acc_payment_methods ON (acc_payment_methods.id=acc_accept_rows.account) "
			."LEFT JOIN acc_banks ON (acc_bank_accounts.bank=acc_banks.bank_code AND acc_bank_accounts.branch=acc_banks.branch_code) "
			."WHERE accept_id=$id ORDER BY id");
	}elseif ($docType=='invoice_accept') {
		$accept_rows = sql2array("SELECT *, SUM(sum) AS sum, sum AS first_sum, COUNT(*) AS payments_count FROM acc_accept_rows WHERE invoice_id=$id ORDER BY id");
		// echo "<div dir=ltr align=left><pre>".print_r($accept_rows,1)."</pre></div>";
		$invoice_rows = sql2array("SELECT * ,CASE type WHEN 'part' THEN 1 WHEN 'material' THEN 2 WHEN 'service' THEN 3 WHEN 'fserv' THEN 4 WHEN 'discount' THEN 5 ELSE type END AS s FROM acc_invoice_rows WHERE invoice_id=$id ORDER BY s, id");
		if (!$invoice_rows) $invoice_rows[]=array('name'=>'עבור תשלום');
	}elseif ($docType=='accept') {
		$accept_rows = sql2array("SELECT acc_accept_rows.*, "
			."IF (acc_accept_rows.ptype='שיק',acc_bank_accounts.number,'') AS account, "
			."IF ((acc_accept_rows.ptype='שיק' OR acc_accept_rows.ptype='ה.קבע/ה.בנקאית'),CONCAT(acc_banks.bank_code,' - ',acc_banks.bank_name),'') AS bank FROM acc_accept_rows "
			."LEFT JOIN acc_bank_accounts ON (acc_bank_accounts.id=acc_accept_rows.account) "
			."LEFT JOIN acc_banks ON (acc_bank_accounts.bank=acc_banks.bank_code AND acc_bank_accounts.branch=acc_banks.branch_code) "
			."WHERE accept_id=$id ORDER BY id");
	} else {
		$invoice_rows = sql2array("SELECT * ,CASE type WHEN 'part' THEN 1 WHEN 'material' THEN 2 WHEN 'service' THEN 3 WHEN 'fserv' THEN 4 WHEN 'discount' THEN 5 ELSE type END AS s FROM acc_invoice_rows WHERE invoice_id=$id ORDER BY s, id");
	}

	$doc=sql2array($sql,'','',1);
	if (!$doc) die("Document error!");

	if ($docType=='order' AND ($doc['status']==4 OR $doc['status']==5)) $doc['type']='offer';
	
	// runsql("UPDATE acc_docs SET printed=1 WHERE id=$id");
	$firm=sql2array("SELECT name, cnumber,adr0,city0,zip0,tel0,tel1,fax  FROM clients WHERE id=$SVARS[cid]",0,0,1,'Rafael');
	$address=htmlspecialchars($firm['adr0']).($firm['adr0'].$firm['city0']?', ':'').htmlspecialchars($firm['city0']).($firm['city0'].$firm['zip0']?', ':'').htmlspecialchars($firm['zip0']);
	$phones=($firm['tel0'].$firm['tel1']?'<b>טל</b>: ':'').htmlspecialchars($firm['tel0']).($firm['tel0'].$firm['tel1']?' ':'').htmlspecialchars($firm['tel1']).($firm['fax']?'<b> פקס</b>: ':'').htmlspecialchars($firm['fax']);

	$user = htmlspecialchars($SVARS['user']['name']?$SVARS['user']['name']:$SVARS['user']['username']);
/*
	# Set user_logo
	$user_logo = (($docType=='order' OR !$print) ? glob("../www/files/users_logos/$group_id.*") : glob("files/users_logos/$group_id.*"));
	if ($user_logo[0]) {
		$user_logo = $user_logo[0];
		list($w, $h) = getimagesize($user_logo);
		$img_w=160; $img_h=80;
		$ratio_orig = $w/$h;
		if ($img_w/$img_h > $ratio_orig) $img_w = round( $img_h*$ratio_orig );
		else $img_h = round( $img_w/$ratio_orig );
		$user_logo="<span id=user_logo><img src='$user_logo?".filemtime($user_logo)."' width=$img_w height=$img_h style='-ms-interpolation-mode:bicubic; float:left;'></span>";
	}else $user_logo='<span id=user_logo></span>';
	$units = array(1=>'יחידה',2=>'שעה',3=>'ליטר',4=>'קילוגרם',5=>'מטר');
*/	
	header("Content-Type: text/html;charset=utf-8");
	
	$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html dir="rtl"><head><title>'.$doc_types[$doc['type']].' מספר &nbsp;'.$doc['num'].'</title>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />';

	
	// $html = "<html><head><title>".$doc_types[$doc['type']]." מספר &nbsp;".$doc['num']."</title>";
	$html.= "<link rel=stylesheet href=skin/style.css>
	<style>
	body {font-family:Tahoma; font-size:9pt;}
	.tbl1 { border-collapse:collapse; border:1px solid gray; width:100%;}
	.tbl1 td, .tbl1 th { padding:2px 5px; border:1px solid gray; font-size:8pt;}
	h2 {font-family:Tahoma; font-size:14pt; margin:0}
	.tbl1 th {text-align:right; background-color:#F0F0F0; }
	.tbl1 .bold td {font-weight:bold;}
	@media print{ .userinput{border-bottom:0} }
	</style>
	</head>
	<body dir=rtl onLoad='if (confirm(\"?האם ברצונך להדפים את המסמך\")) window.print()'>
	<table  width=100% style='height:100%'><tr><td valign=top>
	<div align=right style='margin-bottom:5px; font-size:9pt;'>".$user_logo."<h2>".htmlspecialchars($firm['name'])."</h2>".trim(($firm['cnumber']? "ח.פ.: ".$firm['cnumber']:''))."<br>$address<br>$phones</div>
	<div style='clear:both; border-bottom:1px solid gray;'>
	<table width=100% cellpadding=0 cellspacing=0><tr><td valign=top style='padding:0; text-align:right; font-size:9pt;'>";

	if ($docType=='deposit') {
		$html.= "<br><b>בנק: ".htmlspecialchars($doc['bank_name'])."</b><br><b>סניף</b>: ".htmlspecialchars("$doc[branch_code] - $doc[branch_name]")."<br>".($doc['tel']?"<b>טלפון</b>: $doc[tel]  ":'').($doc['fax']?"<b>פקס</b>: $doc[fax]  ":'');
	}else{
		$html.= "<br><b>לכבוד: ".trim("$doc[name]")."<br>".($doc['c_type']==1 ? 'ת.ז.':'ע.מ./ח.פ.').":</b> ".htmlspecialchars($doc['cnumber'])/*."<br><b>טלפונים</b>: ".client_phones($doc)."<br><b>כתובת</b>: ".client_addresses($doc)*/;
	}
	
	$html.= "</td><td valign=bottom style='text-align:left; font-size:10px;'>תאריך הדפסה: ".date('d/m/Y H:i')."

	</td></tr></table>

	</div><br>
	<table width=100%><tr>
		<td colspan=2 align=center nowrap width=40%><h2>".$doc_types[$doc['type']]." מספר &nbsp;".$doc['num']."</h2>".($tax_clean ? '<br><b>לחברה פטור מלא מניקוי מס במקור</b>':'')."</td>
	</tr><tr>
		<td style='font-size:9pt;'><b>תאריך: ".str2time($doc['date'],'d/m/Y')."</b></td>
		<td align=left nowrap><b style='font-size:18px'>".((!$doc['printed'] OR $original_only) ? 'מקור':'העתק משוחזר')."</b></td>
	</tr></table>
	<div style='padding:15px 5px; font-family:Tahoma; font-size:10pt; border-top:1px solid gray'>
	<br></div>";

	if ($doc['type']=='cash_payment') {
		$docHTML="<table width=100% class=tbl1><tr><th width=10>#</th><th>סוג תשלום</th><th width=100>סכום</th></tr>"
			."<tr><td width=10>1</td><td>מזומן</td><td width=100>".price($doc['sum'],2)."</td></tr>"
			."<tr><td width=10>&nbsp;</td><td><b>סה\"כ</b></td><td width=100><b>".price($doc['sum'],2)."</b></td></tr>"
			."</table>";
	}

	if ($invoice_rows) {
		$docHTML.= "<table width=100% class=tbl1><tr><th width=10>#</th><th>פרטים</th><th width=100>סכום</th></tr>";
			foreach($invoice_rows as $r) {
				$n++;
				$docHTML.="<tr><td>$n</td><td colspan=2>".htmlspecialchars($r['name'])."</td></tr>";
			}
		// $docHTML.='<tr class=bold><td colspan=2 style="text-align:center; border-bottom: 1px solid black">&nbsp;</td></tr>';
		$docHTML.='<tr><td colspan=2 align=left><b>סה"כ לפני מע"מ</b>:</td><td><b>'.($doc['type']=='debt'?'(':'').price($accept_rows[0]['sum']-round($accept_rows[0]['sum']/(100+$vat_rate)*$vat_rate,2),2).($doc['type']=='debt'?')':'').'</b></td></tr>';
		$docHTML.='<tr><td colspan=2 align=left><b>מע"מ '.$vat_rate.'%</b>:</td><td><b>'.($doc['type']=='debt'?'(':'').price($accept_rows[0]['sum']/(100+$vat_rate)*$vat_rate,2).($doc['type']=='debt'?')':'').'</b></td></tr>';
		$docHTML.='<tr><td colspan=2 align=left><b>סה"כ לתשלום</b>:</td><td><b>'.($doc['type']=='debt'?'(':'').price($accept_rows[0]['sum'],2).($doc['type']=='debt'?')':'').'</b></td></tr>';
		$docHTML.="</table>";
	}

	if ($accept_rows) {
		$total=0;
		foreach($accept_rows as $r) {
			$n++;$total+=$r['sum']; $sum=price($r['sum'],2); $ptype=$r['ptype'];
			if ($r['ptype']=='ה.קבע/ה.בנקאית') $r['account']='';
			if ($r['ptype']=='אשראי') {
				$r['account']=$r['payments_count'];
				$rowsCreditCardHTML.= "<tr><td>$n</td><td>".str2time($r['date'],'d/m/Y')."</td><td>".htmlspecialchars($r['ptype'])."</td><td>".htmlspecialchars($r['bank'])."</td><td>".htmlspecialchars($r['number'])."</td><td>".htmlspecialchars($r['account'])."</td><td>".price($r['first_sum'])."</td><td>$sum</td></tr>";
			} else
				$rowsHTML.= "<tr><td>$n</td><td>".str2time($r['date'],'d/m/Y')."</td><td>".htmlspecialchars($r['ptype'])."</td><td>".htmlspecialchars($r['bank'])."</td><td>".htmlspecialchars($r['account'])."</td><td>".htmlspecialchars($r['number'])."</td><td>$sum</td></tr>";
		}
		
		if ($rowsCreditCardHTML) $docHTML.= "<br><br><br><br><table width=100% class=tbl1><tr><th width=10>#</th><th nowrap>תאריך תשלום</th><th width=100>סוג תשלום</th><th width=150>חברת אשראי</th><th nowrap>מס' כרטיס אשראי</th><th width=100>מס' תשלומים</th><th width=100>תשלום ראשון</th><th width=100>סכום</th></tr>".$rowsCreditCardHTML;
		if ($rowsHTML) $docHTML.= "<br><br><br><br><table width=100% class=tbl1><tr><th width=10>#</th><th nowrap>תאריך פרעון</th><th width=100>סוג תשלום</th><th width=150>בנק</th><th width=120>חשבון</th><th nowrap>מס' שיק</th><th width=100>סכום</th></tr>".$rowsHTML;
		
		$docHTML.= '</table><table width=100% class=tbl1>';
		if ($doc['tax_clear']) 	{
			$docHTML.= '<tr><td colspan=6 align=left><b>סה"כ המחאות</b>:</td><td width=100><b>'.($doc['type']=='debt'?'(':'').price($total,2).($doc['type']=='debt'?')':'').'</b></td></tr>';
			$docHTML.= '<tr><td colspan=6 align=left><b>ניקוי מס במקור ('.(int)$doc['tax_clear'].'%)</b>:</td><td width=100><b>'.($doc['type']=='debt'?'(':'').price($total/(100-$doc['tax_clear'])*$doc['tax_clear'],2).($doc['type']=='debt'?')':'').'</b></td></tr>';
			$total=$total/(100-$doc['tax_clear'])*100;
		}
		$docHTML.= '<tr><td colspan=6 align=left><b>סה"כ</b>:</td><td width=100><b>'.($doc['type']=='debt'?'(':'').price($total,2).($doc['type']=='debt'?')':'').'</b></td></tr>';
		$docHTML.= '</table>';
	}

	if ($deposit_rows) {
		$total=0;
		foreach($deposit_rows as $r) {
			$n++;$total+=$r['sum']; $sum=price($r['sum'],2); $ptype=$r['ptype'];
			$rowsHTML.= "<tr><td>$n</td><td>".str2time($r['date'],'d/m/Y')."</td><td>".htmlspecialchars($r['client_name'])."</td><td>".htmlspecialchars($r['ptype'])."</td><td>".htmlspecialchars($r['bank'])."</td><td>".htmlspecialchars(($ptype=='אשראי'? "$r[dept]/" :'').$r['account'])."</td><td>".htmlspecialchars($r['number'])."</td><td>$sum</td></tr>";
		}
		
		if ($ptype=='אשראי') 
			$docHTML.= "<br><br><br><br><table width=100% class=tbl1><tr><th width=10>#</th><th nowrap>תאריך קבלה</th><th nowrap>שם לקוח</th><th width=100>סוג תשלום</th><th width=150>חברת אשראי</th><th width=120>תשלום</th><th nowrap>מס' כרטיס אשראי</th><th width=100>סכום</th></tr>".$rowsHTML;
		else
			$docHTML.= "<br><br><br><br><table width=100% class=tbl1><tr><th width=10>#</th><th nowrap>תאריך פרעון</th><th nowrap>שם לקוח</th><th width=100>סוג תשלום</th><th width=150>בנק</th><th width=120>חשבון</th><th nowrap>מס' שיק</th><th width=100>סכום</th></tr>".$rowsHTML;
		
		$docHTML.= '<tr><td colspan=7 align=left><b>סה"כ</b>:</td><td><b>'.($doc['type']=='debt'?'(':'').price($total,2).($doc['type']=='debt'?')':'').'</b></td></tr></table>';
	}

	if ($expense_rows) {
		$total=0;
		$docHTML.= "<table width=100% class=tbl1><tr><th width=10>#</th><th>שם פריט</th><th width=150 nowrap>אסמכתא</th><th width=170>קבוצת הוצאות</th><th width=100>סה''כ</th></tr>";
		foreach($expense_rows as $r) {
			$n++;$total+=$r['sum']; $sum=price($r['sum'],2);
			$docHTML.= "<tr><td>$n</td><td>".htmlspecialchars($r['name'])."</td><td>".htmlspecialchars("$r[sup_doc_num] (".str2time($r['sup_doc_date'],'d/m/Y').")")."</td><td>".htmlspecialchars($r['type'])."</td><td>$sum</td></tr>";
		}
		
		
		$docHTML.= '<tr><td colspan=3 align=left><b>סה"כ</b>:</td><td><b>'.($doc['type']=='debt'?'(':'').price($total,2).($doc['type']=='debt'?')':'').'</b></td></tr></table>';
	}
	
	if ($docHTML) $html.=$docHTML;
	
	if ($doc['notes']) $html.= "<br><font style='font-size:8pt'><u>הערות</u>:&nbsp; ".$doc['notes'].'</font>'; 

	$html.= "</td></tr>";

	
	if($pattern_id){
		$pattern=sql2array("SELECT text FROM crm_letters WHERE id=$pattern_id",'','',1);
		$pattern_text=letterReplaceVars($doc, $pattern['text']);

		$html.= "<tr><td style='height:100%;' valign=top>
			<table width=100%>
				<tr><td>".$pattern_text."</td></tr>
			</table><br>
		</td></tr>";
	}
	

	if($docType!='order'){
		$html.= "<tr><td>
		<table cellpadding=0 cellspacing=0 style='width:100%; font-family:Tahoma; font-size:8pt;'><tr>
			<td align=rigth nowrap>שם מפיק המסמך: <u>&nbsp;$user&nbsp;</u> חתימה: ____________</td>
			<td align=left nowrap>שם המקבל: _____________ &nbsp; &nbsp; תאריך: __________ &nbsp; &nbsp; חתימה: ____________</td>
		</tr></table>
		</td></tr>";
	
		$html.="<tr><td><br><br>
		<div style='font-size:10px'>מסמך ממוחשב במשלוח בדוא\"ל</span></div>
		<div style='font-size:7px'>הופק ע''י - <span dir=ltr style='font-size:7px'>© 2011-".date('Y')." Rafael - CRM </span></div></td></tr>";
	}
	
	$html.="</table>";
		
	if (!$doc['printed'] AND $docType!='order' AND !$original_only) {
		$html.= "<div style='page-break-before:always'></div>
		<table width=100% style='height:100%'><tr><td>
		<div align=right style='margin-bottom:5px;'>".$user_logo."<h2>".htmlspecialchars($agency[$group_id]['agency'])."</h2>".trim(($agency[$group_id]['c_id']? "ח.פ.: ".$agency[$group_id]['c_id']:'').($agency[$group_id]['c_authorized']? ($PRS['garge_pan'][1]==1 ? ' מוסך':' עוסק')." מורשה: ".$agency[$group_id]['c_authorized']:''))."<br>$address<br>$phones</div>
		<div style='clear:both; border-bottom:1px solid gray; text-align:left; font-size:10px;'>
		<table width=100% cellpadding=0 cellspacing=0><tr><td valign=top style='padding:0; text-align:right; font-size:9pt;'>";

		if ($docType=='deposit') {
			$html.= "<b>בנק: ".htmlspecialchars($doc['bank_name'])."</b><br><b>סניף</b>: ".htmlspecialchars("$doc[branch_code] - $doc[branch_name]")."<br>".($doc['tel']?"<b>טלפון</b>: $doc[tel]  ":'').($doc['fax']?"<b>פקס</b>: $doc[fax]  ":'');
		}else{
			$html.= "<b>לכבוד: ".trim("$doc[name]")."<br>".($doc['c_type']==1 ? 'ת.ז.':'ע.מ./ח.פ.').":</b> ".htmlspecialchars($doc['cnumber'])/*."<br><b>טלפונים</b>: ".client_phones($doc)."<br><b>כתובת</b>: ".client_addresses($doc)*/;
		}
		
		$html.= "</td><td valign=bottom style='text-align:left; font-size:10px;'>תאריך הדפסה: ".date('d/m/Y H:i')."
		</td></tr></table>
		</div><br>
		<table width=100%><tr>
			<td colspan=2 align=center nowrap width=40%><h2>".$doc_types[$doc['type']]." מספר &nbsp;".$doc['num']."</h2></td>
		</tr><tr>
			<td><b>תאריך: ".str2time($doc['date'],'d/m/Y')."</b></td>
			<td align=left nowrap><b style='font-size:18px'>העתק</b></td>
		</tr></table>
		<div style='padding:15px 5px; font-family:Tahoma; font-size:10pt; border-top:1px solid gray'>";

		if ($PRS['garge_pan'][1]==1 AND ($docType=='order' OR $doc['parent_doc_id'])) {//לניהול מוסך
		$html.= "<table width=100%><tr>
			<td width=16% valign=top nowrap><b>סוג עבודה</b>:<br/>".htmlspecialchars($doc['wtype'])."</td>
			<td width=28% valign=top><b>פרטי הרכב</b>:<br/>דגם: ".htmlspecialchars($doc['car_name'])."<br/>שנת יצור: ".htmlspecialchars($doc['car_year'])."<br/>מספר רשוי: ".htmlspecialchars($doc['car_num'])."<br/>תאריך כניסה: ".str2time($doc['start'],'d/m/Y')."</td>
			<td width=28% valign=top><b>פרטי ביטוח</b>:<br/>חברת ביטוח: ".htmlspecialchars($doc['ins_firm'])."<br/>סוכן: ".htmlspecialchars($doc['ins_agent'])."<br/>מס' טלפון: ".htmlspecialchars($doc['ins_agent_tel'])."</td>
			<td width=28% valign=top><b>פרטי תאונה</b>:<br/>תאונה מיום: ".str2time($doc['accident_date'],'d/m/Y')."<br/>שמאי: ".htmlspecialchars($doc['appraiser'].($doc['appraiser_sum']?', שכ"ט: '.$doc['appraiser_sum']:'')).($doc['rep_type']?'<br> '.htmlspecialchars($doc['rep_type']):'')."</td>
		</tr></table>";
		}

		$html.= "<br></div>";

		if ($docHTML) $html.= $docHTML;

		/**/
		$html.= "</td></tr><tr><td style='height:100%'><div></div></td></tr><tr><td>
		<table cellpadding=0 cellspacing=0 style='width:100%; font-family:Tahoma; font-size:8pt;'><tr>
			<td align=rigth nowrap>שם מפיק המסמך: <u>&nbsp;$user&nbsp;</u> חתימה: ____________</td>
			<td align=left nowrap>שם המקבל: _____________ &nbsp; &nbsp; תאריך: __________ &nbsp; &nbsp; חתימה: ____________</td>
		</tr></table>
		<br><br>
		<div style='font-size:10px'>מסמך ממוחשב במשלוח בדוא\"ל</span></div>
		<div style='font-size:7px'>הופק ע''י - <span dir=ltr style='font-size:7px'>© 2009-".date('Y')." CRM247</span></div>
		</td></tr></table>";
		
	}
	$html.= "</body></html>";
	
	
	if($print AND ($docType=='order')){
		// if($doc['type']!='offer') $footer_params='--footer-center "שם מפיק המסמך: '.$user.'   חתימה: ____________               שם המקבל: ___________________ תאריך: __________חתימה: ___________"	--footer-font-size 6 --footer-font-name "Tahoma" --footer-spacing 2';
		// else $footer_params='';
		
		if($doc['type']!='offer') $footer_params='--footer-html "modules/accounting/print_doc_footer.html" --footer-spacing 2';
		else $footer_params='';

		
		include_once "inc/wkhtmltopdf.php";		
		wkhtmltopdf($html, 'embed','file.doc',$footer_params);
	}

	return $html;
}
//————————————————————————————————————————————————————————————————————————————————————
function letterReplaceVars($r, $html) {
	$order_statuses=array(1=>'הזמנה בעבודה',2=>'הזמנה בוצעה',3=>'הזמנה מבוטלת',4=>'הצעה בעבודה',5=>'הצעה מבוטלת');
	# client info
	$html=str_replace('{שם לקוח}', 		$r['first_name'].' '.$r['last_name'], $html);	
	$html=str_replace('{לקוח}', 		$r['first_name'].' '.$r['last_name'], $html);	
	$html=str_replace('{ת.ז.}', 		$r['cnumber'], $html);
	$html=str_replace('{ת.לידה}', 		str2time($r['birthday'], 'd/m/Y'), $html);
	$html=str_replace('{כתובת}', 		$r['adr0'], $html);
	$html=str_replace('{יישוב}', 		$r['city0'], $html);
	$html=str_replace('{מיקוד}', 		$r['zip0'], $html);
	//$html=str_replace('{כל הכתובות}', 	client_addresses($r), $html);
	$html=str_replace('{טלפון}', 		$r['tel0'], $html);
	//$html=str_replace('{כל הטלפונים}', 	client_phones($r), $html);
	$html=str_replace('{דוא"ל}', 		$r['email'], $html);
	$html=str_replace('{סוכן}', 		$r['agent_name'], $html);
	$html=str_replace('{מקום עבודה}', 	$r['work'], $html);
	$html=str_replace('{תפקיד}', 		$r['job'], $html);
	$html=str_replace('{אתר אינטרנט}', 	$r['url'], $html);
	$html=str_replace('{ICQ}', 			$r['icq'], $html);
	$html=str_replace('{Messenger}', 	$r['msn'], $html);
	$html=str_replace('{Skype}', 		$r['skype'], $html);
	$html=str_replace('{Facebook}', 	$r['facebook'], $html);
	$html=str_replace('{דרכון}', 		$r['darkon'], $html);
	$html=eregi_replace('{היום}', 		date('d/m/Y'), $html);

	# order info
	$html=str_replace('{תאריך}', 		str2time($r['date'], 'd/m/Y'), $html);	
	$html=str_replace('{סטטוס}', 		$order_statuses[$r['status']], $html);	
	$html=str_replace('{מספר הזמנה}', 	$r['num'], $html);
	$html=str_replace('{כותרת}', 		$r['name'], $html);
	$html=str_replace('{מתאריך}', 		str2time($r['start'], 'd/m/Y'), $html);
	$html=str_replace('{עד תאריך}', 	str2time($r['end'], 'd/m/Y'), $html);
	$html=str_replace("{מס' תשלומים}", 	$r['payments'], $html);
	$html=str_replace('{תשלומים מתאריך}',str2time($r['pay_start'], 'd/m/Y'), $html);

	//old patterns
	$html=str_replace('{clientName}', 	$r['client_name'], $html);
	$html=str_replace('{clientAddress}',$r['adr0'], $html);
	$html=str_replace('{clientCity}', 	$r['city0'], $html);
	$html=str_replace('{clientAdr}', 	$r['adr0'], $html);
	$html=str_replace('{clientZip}', 	$r['zip0'], $html);
	$html=eregi_replace('{today}', 		date('d/m/Y'), $html);
	
	return $html;
}
?>