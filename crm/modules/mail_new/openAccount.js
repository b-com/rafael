ImgUploadWin = function(parentForm) {
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',fileUpload:true,labelWidth:55,defaultType:'textfield',
		items: [
			{xtype:'fileuploadfield', name:'file', emptyText:'בחר קובץ...', fieldLabel:'שם קובץ', anchor:'95%', allowBlank:false,
				regex:/^[^\/\\\?:*<>"|]+$/, invalidText:'שם קובץ אינו יכול להכיל את התווים הבאים:<br><b>\\ / * ? " &lt; &gt; |</b>',
				listeners: { scope:this,
					fileselected:function(fld,v){
						if ((/\.(pdf|jpg|jpeg|gif|png|bmp)$/i).test(v)) {
							v = v.replace(/\//g, '\\');
							v = v.replace(/^([^\\]+\\)*/g, '');	// remove path
							var regs = /^(.+)\.(.{2,4})$/.exec(v);
							if (regs && regs[2]) { fld.el.dom.value=regs[1]; this.fileExt=regs[2]; }
							else fld.el.dom.value=v;
							fld.validate();
							fld.el.dom.setAttribute('name','file_name');
							fld.el.dom.removeAttribute('readOnly');
							fld.el.dom.style.color='black';
						}
						else Ext.MessageBox.show({width:300, msg:'הקובץ צריך להיות תמונה עם הסיומות jpg, jpeg, gif, bmp או png', buttons:Ext.MessageBox.OK});
					}
				}
			 }
		]
	});

	ImgUploadWin.superclass.constructor.call(this, {
		title:'הוספת תמונה', iconCls:'upload', width:430, modal:true, resizable:false, layout:'fit',	plain:true,	bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{text:'הוסף', handler:function(){this.submitForm();}, scope:this},{text:'סגור', handler:function(){this.close();}, scope:this}]
	});
		
	this.submitForm = function() {
		if(!this.form.form.isValid()) return;
		var fileName = this.form.form.findField('file_name').getRawValue().trim();
		if (this.fileExt) fileName=fileName+'.'+this.fileExt;
		
		this.form.form.submit({
			url: '?m=mail_new/mail_f&f=image_to_signature',
			params:{fileName:fileName},
			waitTitle: 'אנא המתן...', waitMsg: 'טוען את הקובץ...',
			scope:this,
			success: function(r,o){	
				parentForm.find('name','htmlCode')[0].insertAtCursor('<img src='+o.result.file+' align=absmiddle>');
				this.close();
			},
			failure: function(r,o){this.close();ajRqErrors(o.result);}
		});
	};
};
Ext.extend(ImgUploadWin, Ext.Window);

// Add new account
OpenAccountWin = function(action,mail) {
	
	this.action = action;
	this.tree = mail.menuTree;
	this.accountsStore = new Ext.data.JsonStore({
		url:'?m=mail_new/mail_f&f=get_accounts',
		totalProperty:'total',
		root:'data',
		fields:['id','name','email','type','host','port','security','smtp_host','smtp_port','smtp_sec','smtp_auth','listSubscribedOnly','username','replyTo','primary'],
		listeners:{
			load: function(){
				this.accountsStore.insert(0,new Ext.data.Record({id:0, email:'חשבון חדש'}));
				if (this.accountsStore.getCount()==1) this.form.find('hiddenName','type')[0].setValue('');

			}, scope:this
		}
	});
	
	this.form = new Ext.form.FormPanel({
		title:'חשבונות דוא"ל', baseCls:'x-plain', autoHeight:true, buttonAlign:'center', labelWidth:82, bodyStyle:'padding:5px',
		items:[
			{fieldLabel:'בחר חשבון', hiddenName:'account', xtype:'combo',tabIndex:1,width:200, store:this.accountsStore, valueField:'id', displayField:'email',
			listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true,allowBlank:false, emptyText:'- בחר -',
				listeners:{
					select: function(combo, record, index){
						this.form.find('name','password')[0].setValue();
						if (record.data.id) {
							this.form.form.loadRecord(record);
							this.form.find('hiddenName','security')[0].setValue(record.data.security);
							this.form.find('hiddenName','smtp_sec')[0].setValue(record.data.smtp_sec);
							this.form.find('name','listSubscribedOnly')[0].setValue(record.data.listSubscribedOnly);
						//	this.form.find('name','smtp_auth')[0].setValue(record.data.smtp_auth);
							this.form.find('name','primary')[0].setValue(record.data.primary);
							this.form.buttons[0].setText('עדכן');
							this.form.buttons[1].show(); 
						} else {
							this.form.buttons[0].setText('הוסף');
							this.form.buttons[1].hide();
							this.form.getForm().reset();
						}
					},scope:this
				}
			},
			{xtype:'fieldset',title:'פרטי משתמש', cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:
				[    
					{layout:'table', layoutConfig: {columns:3}, baseCls: 'x-plain', items:[
						{xtype:'label', text:'שם חשבון :'},{name:'name', xtype:'textfield', tabIndex:1, width:200, allowBlank:false}
					   ,{name:'primary', xtype:'checkbox',boxLabel:'עיקרי', tabIndex:2}
					   ,{xtype:'label', text:'דוא"ל :'},{colspan:2,name:'email', xtype:'textfield', tabIndex:3, width:200, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/, allowBlank:false}
					   ,{xtype:'label', text:'דוא"ל לתשובה:'},{name:'replyTo', xtype:'textfield', tabIndex:4, width:200, regex: /^(([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4})?$/, allowBlank:false}
					   ,{colspan:5,name:'chkRep', xtype:'checkbox',boxLabel:'תשתמש בדוא"ל החשבון', tabIndex:5,
							listeners:{
								check:function(f){
									if(f.checked){
										var eml = this.form.find('name','email')[0].getValue();
										if(!eml) this.form.find('name','chkRep')[0].setValue(false);
										else
											this.form.find('name','replyTo')[0].setValue(eml);
									}else this.form.find('name','replyTo')[0].setValue('');
								},scope:this
							}
						}
					   ,{name:'id', xtype:'hidden'}
					]}
				]
			 }
			  ,{xtype:'fieldset',title:'פרטי שרת', cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:
				[ 
					{layout:'table', layoutConfig: {columns:5}, baseCls:'x-plain', items:[
						 {xtype:'label', text:'סוג חשבון :'},{hiddenName:'type', colspan:4,xtype:'combo', store:[[1,'imap'],[2,'pop3']],tabIndex:3, disabled:true, value:1, width:50 ,mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true,allowBlank:false}
						,{xtype:'label', text:'שרת דואר נכנס :'},{name:'host',  xtype:'textfield', tabIndex:4, width:180}
						,{xtype:'label', text:'פורט :'},{name:'port', xtype:'numberfield', tabIndex:5, width:40}
						// ,{name :'security',  xtype:'checkbox',boxLabel:'SSL', tabIndex:6}
						,{hiddenName:'security', xtype:'combo', store:[['','None'],['ssl','SSL'],['tls','TLS']],tabIndex:6, value:'', width:50, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
						,{xtype:'label', text:'שרת דואר יוצא (SMTP) :'},{name:'smtp_host',  xtype:'textfield', tabIndex:7, width:180}
						,{xtype:'label', text:'פורט :'},{name:'smtp_port',  xtype:'numberfield', tabIndex:8, width:40}
						// ,{name :'smtp_sec',  xtype:'checkbox',boxLabel:'SSL', tabIndex:9}
						,{hiddenName:'smtp_sec', xtype:'combo', store:[['','None'],['ssl','SSL'],['tls','TLS']],tabIndex:6, value:'', width:50, mode:'local', listClass:'rtl', editable:false, triggerAction:'all', forceSelection:true}
						,{name :'listSubscribedOnly', checked:true, colspan:5,  xtype:'checkbox',boxLabel:'הצג רק תיקיות מנוי', tabIndex:10}
						//,{name :'smtp_auth', colspan:5,  xtype:'checkbox',boxLabel:'שרת הדואר היוצא שלי דורש אימות', tabIndex:10}
					]}
				]
			 }
			  ,{xtype:'fieldset',title:'פרטי כניסה', cls:'rtl', style:'padding:2px; margin-top:3px; margin-bottom:5px;', autoHeight:true, items:
				[ 
					{layout:'table', layoutConfig: {columns:2}, baseCls: 'x-plain', items:[
						{xtype:'label', text:'שם משתמש :'},{name:'username', xtype:'textfield', tabIndex:10, width:200, allowBlank:false}
					   ,{xtype:'label', text:'סיסמה :'},{name:'password',inputType:'password', xtype:'textfield', tabIndex:11, width:200, allowBlank:true}
					]}
				]
			 }											
			],
		buttons:[
			 {text:'הוסף', iconCls:'edit', handler:function(){this.onSubmit();}, scope:this}
			,{text:'מחק', xtype:'button', iconCls:'delete', hidden:true, handler:
				function(){
					Ext.MessageBox.show({width:300, title:'מחק חשבון?', icon:Ext.MessageBox.ERROR, msg:'האם ברצונך למחוק חשון מהרשימה?', buttons:Ext.MessageBox.YESNO,
						fn:function(o){
							if(o == 'yes'){
								this.el.mask('...טוען');
								var acc_id = this.form.find('name','id')[0].getValue();
								Ext.Ajax.request({
									url:'?m=mail_new/mail_f&f=delete_account',
									params:{id:acc_id},
									success: function(r,o){
										var res = Ext.decode(r.responseText);
										if(res.success){
											var child = this.tree.getRootNode().findChild('root_id',acc_id);
											if(child) child.destroy();
											this.close();
										}else
											Ext.Msg.alert('שגיאה', 'שגיאה. נסה/י שוב.'); 
										this.el.unmask();
									},
									failure: function(r){
										this.el.unmask();
										Ext.Msg.alert('Error', 'Request failed.'); 	
									},scope: this
								});
							}
					},scope:this});
				}, scope:this}
			,{text:'סגור', handler:function(){this.close();}, scope:this}
			]
	});
	
	this.signature_form = new Ext.FormPanel({title:'חתימת אלקטרונית', labelAlign:'top', layout:'fit',baseCls:'x-plain', bodyStyle:'padding:5px', buttonAlign:'center',
		tbar:[
		      {text:'הוסף תמונה', name:'insert_img_btn', iconCls:'insert-image', handler: function(){
				var win = new ImgUploadWin(this.signature_form);
				win.show();
		      },scope:this
	      }
		],
		items: [
			{xtype:'htmleditor', name:'htmlCode', cls:'rtl', 
				enableFont:false, enableLinks:false, enableSourceEdit:false,
				insertAtCursor : function(text){
					if(!this.activated) return;
					if(Ext.isIE){
						this.win.focus();
						var r = this.doc.selection.createRange();
						if(r){r.collapse(true); r.pasteHTML(text); this.syncValue(); this.win.focus();}
					} else
						if(Ext.isGecko || Ext.isOpera || Ext.isWebKit){this.win.focus(); this.execCmd('InsertHTML', text); this.deferFocus();
					}
				},
				setValue : function(v){Ext.form.HtmlEditor.superclass.setValue.call(this, Ext.util.Format.htmlDecode(v)); this.pushValue();},
				listeners: { initialize: function(){this.getEditorBody().style.direction='rtl';}}
			},{xtype:'hidden', id:'ids', name:'ids'}
		],
		buttons: [
			 {text:'שמור תבנית', scope:this, handler: function(){
				if(!this.signature_form.form.isValid()) return; 
			 	this.signature_form.getForm().submit({
			 		url: '?m=common/common_func&f=signature&a=save',
			 		waitMsg: 'שומר...',
			 		success:function(){
			 			Ext.Msg.alert('הודעה', 'החתימה נשמרה בהצלחה!'); 
			 			this.close();
			 		}
			 	});}
			 }
			,{text:'נקה', scope:this, handler: function(){this.signature_form.find('name','htmlCode')[0].setValue('');}
			}
			,{text:'סגור', handler:function(){this.close();}, scope:this}
		],
		listeners:{
			activate: function(){
				Ext.Ajax.request({
					url: '?m=common/common_func&f=signature&a=load',
					success: function(r,o){r=Ext.decode(r.responseText);if (!ajRqErrors(r)) {this.signature_form.form.setValues({htmlCode:r.data.signature});}}
					// ,failure:function(){win.getEl().unmask()}
					,scope: this
				});
			},
			scope:this
		}
	});
	
	this.layout_form = new Ext.form.FormPanel({
		title:'תצוגה', baseCls:'x-plain',autoHeight:true, labelWidth:8, buttonAlign:'center',
		items:{border:false, layout:"column", baseCls:'x-plain', style:'margin-top:40px; margin-bottom:100px', items:[
			 {width:180, border:false, baseCls:'x-plain', layout:'form', style:'margin-top:30px;', items:[
				{xtype:'radio', name:'rep_type', boxLabel:'תצוגה אופקית', checked:(MailPreview.layout=='Bottom'), style:'margin:0 7px 1px 0;', labelStyle:'padding-bottom:7px;', handler:function(e,checked){if (checked) this.setLayuot('Bottom');}, scope:this},
				{xtype:'radio', name:'rep_type', boxLabel:'תצוגה אנכית',  checked:(MailPreview.layout=='Left'), style:'margin:0 7px 1px 0;', labelStyle:'padding-bottom:7px;', handler:function(e,checked){if (checked) this.setLayuot('Left');}, scope:this},
				{xtype:'radio', name:'rep_type', boxLabel:'תצוגה מוסתרת', checked:(MailPreview.layout=='Hide'), style:'margin:0 7px 1px 0;', labelStyle:'padding-bottom:7px;', handler:function(e,checked){if (checked) this.setLayuot('Hide');}, scope:this}
			]}
			,{columnWidth:0.8, border:false, baseCls:'x-plain', layout:'form', items:[
				{html:'<img src="skin/icons/bmail/window_split_'+MailPreview.layout+'.png">', name:'image', border:false, baseCls:'x-plain'}
			]}
		]},
		buttons:[
			{text:'סגור', handler:function(){this.close();}, scope:this}
		]
	});	
	
    this.options_form = new Ext.form.FormPanel({
        title:'הגדרות אחרות', baseCls:'x-plain',autoHeight:true, labelWidth:60, buttonAlign:'center',
		items:{border:false, layout:"form", baseCls:'x-plain', style:'margin-top:40px;margin-right:30px;margin-bottom:183px;', items:[
            {fieldLabel:'סוג פונט',hiddenName:'fontname',xtype:'combo',
             tabIndex:1,width:200,valueField:'id', 
             displayField:'fontname',listClass:'rtl',editable:false, 
             triggerAction:'all',forceSelection:true,allowBlank:false,mode:'local', 
             value:MailComposeWindowCfg.fontname ? MailComposeWindowCfg.fontname : 0,
            store: new Ext.data.ArrayStore({
                fields: ['id','fontname'],
                data: [[0,'Arial'],[1,'Courier New'],[2,'Tahoma'],[3,'Times New Roman'],[4,'Verdana']]
             }), 
             listeners:{}}
            ,{fieldLabel:'גודל',hiddenName:'fontsize',xtype:'combo',
             tabIndex:2,width:200,valueField:'id', 
             displayField:'fontsize',listClass:'rtl',editable:false, 
             triggerAction:'all',forceSelection:true,allowBlank:false,mode:'local', 
             value:MailComposeWindowCfg.fontsize ? MailComposeWindowCfg.fontsize : 1,
            store: new Ext.data.ArrayStore({
                fields: ['id','fontsize'],
                data: [[1,'1'],[2,'2'],[3,'3'],[4,'4'],[5,'5'],[6,'6'],[7,'7'],[8,'8'],[9,'9'],[10,'10']]
             }), 
             listeners:{}
            }
        ]},
        buttons: [
            {text:'שמור',handler:function(){
               var fn = this.options_form.items.items[0].items.items[0].value;
               var fs = this.options_form.items.items[0].items.items[1].value;
               MailComposeWindowCfg.fontname = fn;
               MailComposeWindowCfg.fontsize = fs;
            
            var cfg_arr = [];
            for (var key in MailComposeWindowCfg) {
                if (MailComposeWindowCfg.hasOwnProperty(key)) {
                    cfg_arr.push(key+':"'+MailComposeWindowCfg[key]+'"'); 
                }
            }
			setObjView({a:'mail_cfg', module:'mail', object:'MailComposeWindow', cfg:'{'+cfg_arr.join(',')+'}'});
            }, scope:this},
	{text:'סגור', handler:function(){this.close();}, scope:this}
        ]
    });
    
	this.setLayuot = function(layout) {
		var preview = mail.mailPreview;
		var right = Ext.getCmp('right-preview');
		var bot = Ext.getCmp('bottom-preview');
		switch(layout){
			case 'Bottom':	right.hide(); bot.add(preview); bot.show();	bot.ownerCt.doLayout();	break;
			case 'Left':	bot.hide();	right.add(preview);	right.show(); right.ownerCt.doLayout();	break;
			case 'Hide':	preview.ownerCt.hide();	preview.ownerCt.ownerCt.doLayout();	break;
		}
		if (MailPreview.layout!=layout) {
			MailPreview.layout = layout;
			this.layout_form.find('name','image')[0].body.update('<img src="skin/icons/bmail/window_split_'+layout+'.png">');
			setObjView({a:'mail_cfg', module:'mail', object:'MailPreview', cfg:'{width:'+MailPreview.width+', height:'+MailPreview.height+', layout:"'+MailPreview.layout+'"}'});
		}
	};
	
	this.tabs = new Ext.TabPanel({
		autoScroll:true, border:false, baseCls:'x-plain', plain:true, activeTab:0,region:'center',
		items:[this.form,this.signature_form,this.layout_form,this.options_form]
	});

	OpenAccountWin.superclass.constructor.call(this, {
		title:'הגדרות דוא"ל',iconCls:'gear',width:500,modal:true,
		resizable:false,layout:'fit',bodyStyle:'padding:5px;',
		items:this.tabs
	});
	
	this.onSubmit = function() {
		if(this.form.form.isValid()){
			this.el.mask('...טוען');
			this.form.form.submit({
				url:'?m=mail_new/mail_f&f=add_account',
				scope:this,
				success: function(r,o){
					Ext.MessageBox.show({width:200, title:(this.action == 'edit' ? 'עדכון חשבון':'הוספת חשבון'),
						msg:(this.action == 'edit' ? 'חשבון עודכן בהצלחה.' : ' חשבון הוסף בהצלחה! נא לרענן תיקיות.'),
						icon:Ext.MessageBox.INFO
					});
					//Ext.Msg.alert((this.action == 'edit') ? 'עדכון חשבון':'הוספת חשבון','<img src="skin/icons/1/icon-info.gif" align=left><br>'+((this.action == 'edit') ? 'חשבון עודכן בהצלחה.' : ' חשבון הוסף בהצלחה! נא לרענן תיקיות.')+'</img>');
					this.el.unmask();
					this.accountsStore.reload();
				},
				failure: function(r,o){
					this.el.unmask();
					this.syncShadow();
					Ext.MessageBox.show({width:400, title:'שגיאה', msg:o.result.msg, icon:Ext.MessageBox.ERROR,buttons:Ext.MessageBox.OK, scope:this});
				
					// ajRqErrors(o.result);
				}
			});
		}
	};
	
	if (this.action == 'new') {
		this.accountsStore.insert(0,new Ext.data.Record({id:0, email:'חשבון חדש'}));
		this.form.find('hiddenName','type')[0].setValue('');
	}
	this.accountsStore.load();
};
Ext.extend(OpenAccountWin, Ext.Window);