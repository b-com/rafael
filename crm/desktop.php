<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?

//<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">?>

<html>
<head>
<title>Rafael - Crm</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
<meta http-equiv="EXPIRES" content="-1">

<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all-rtl.css" />

<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/desktop.css?<?=filemtime('skin/extjs/resources/css/desktop.css');?>" />
<link rel="stylesheet" type="text/css" href="skin/themes/xtheme-vistablue/css/xtheme-vistablue.css?<?=filemtime('skin/themes/xtheme-vistablue/css/xtheme-vistablue.css');?>" />

<!--
<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/xtheme-blue.css" />
-->
<link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all-rtl.css?<?=filemtime('skin/extjs/resources/css/ext-all-rtl.css');?>" />
<link rel="stylesheet" type="text/css" href="skin/style_ext.css?<?=filemtime('skin/style_ext.css');?>" />


<!-- EXT JS LIBRARY -->
<!-- <link rel="stylesheet" type="text/css" href="skin/extjs/resources/css/ext-all-notheme.css" /> -->
<script type="text/javascript" src="skin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="skin/extjs/ext-all-debug.js"></script>

<script type="text/javascript" src="skin/extjs/ext-lang-he.js"></script>
<script type="text/javascript" src="skin/extjs/ext-rtl.js?<?=filemtime('skin/extjs/ext-rtl.js');?>"></script>

<script type="text/javascript" src="skin/extjs/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript" src="skin/extjs/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript" src="skin/extjs/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript" src="skin/extjs/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript" src="skin/extjs/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript" src="skin/extjs/ux/treegrid/TreeGrid.js"></script>

<script type="text/javascript" src="skin/extjs/ux/Ext.ux.Loader.js?<?=filemtime('skin/extjs/ux/Ext.ux.Loader.js');?>"></script>
<script type="text/javascript" src="skin/extjs/ux/Ext.ux.all.js?<?=filemtime('skin/extjs/ux/Ext.ux.all.js');?>"></script>

<!-- CORE -->
<!-- In a production environment these would be minified into one file -->
<script type="text/javascript" src="client/App.js"></script>
<script type="text/javascript" src="client/Desktop.js"></script>
<script type="text/javascript" src="client/Module.js"></script>
<script type="text/javascript" src="client/Notification.js"></script>
<script type="text/javascript" src="client/Shortcut.js"></script>
<script type="text/javascript" src="client/StartMenu.js"></script>
<script type="text/javascript" src="client/TaskBar.js"></script>

<script type="text/javascript">
// Modules
<? echo modules_list(); ?>

// Desktop
var Crm = new Ext.app.App({
	init : function(){
		Ext.BLANK_IMAGE_URL = 'skin/extjs/resources/images/default/s.gif';
		Ext.QuickTips.init();
	},
	//modules: Ms,

	// The members privileges.
	privileges: {"demo-accordion":[],"demo-bogus":[],"demo-grid":[],"demo-layout":[],"demo-tab":[],"qo-preferences":["viewThemes","viewWallpapers"],"qo-profile":["loadProfile","saveProfile","savePwd"]},

	// The desktop config object.
	desktopConfig: {
		// language:'he',
		// appearance: {"fontColor":"333333","taskbarTransparency":"100","theme":{"id":1,"name":"Blue","file":"resources\/css\/xtheme-blue.css"}},
		background: {"color":"69baf9"/*,"wallpaperPosition":"center","wallpaper":{"id":11,"name":"qWikiOffice","file":"skin/images/crm247wallpaper1280x1024.jpg"}*/},
		launchers: {
			"autorun":[],
			"quickstart":[],
			// "shortcut":["qo-preferences","demo-accordion","demo-grid","demo-layout","demo-bogus","demo-tab"]
			"shortcut":["companies","tasks","fax","accounting","config","files"]//"groups","lawyers","toto1","users","worktime"
		},
		taskbarConfig: {
			buttonScale:'large',
			cls:'ltr',
			position:'bottom',
			quickstartConfig: {width:120},
			startButtonConfig: {iconCls:'start32', text:''},
			startMenuConfig: {iconCls:'icon-user-48', title:'<?=jsEscape($SVARS['user']['name'])?>', width:320}
		}
	}
	
	/**
	* An array of the module definitions.
	* The definitions are used until the module is loaded on demand.
	*/
	// modules: [
	// {"id":"demo-accordion",
	// "type":"demo/accordion",
	// "className":"Crm.AccordionWindow",
	// "launcher":
		// {"iconCls":"acc-icon",
		// "shortcutIconCls":"demo-acc-shortcut",
		// "text":"Accordion Window",
		// "tooltip":"<b>Accordion Window<\/b><br \/>A window with an accordion layout"
		// }
		// ,"launcherPaths":{"startmenu":"\/"}
	// }
	// ,{"id":"demo-bogus","type":"demo/bogus","className":"Crm.BogusWindow","launcher":{"iconCls":"bogus-icon","shortcutIconCls":"demo-bogus-shortcut","text":"Bogus Window","tooltip":"<b>Bogus Window<\/b><br \/>A bogus window"},
		// "launcherPaths":{"startmenu":"/Bogus Menu/Bogus Sub Menu"}
	// }
	// ,{"id":"demo-grid","type":"demo/grid","className":"Crm.GridWindow","launcher":{"iconCls":"grid-icon","shortcutIconCls":"demo-grid-shortcut","text":"Grid Window","tooltip":"<b>Grid Window<\/b><br \/>A grid window"},"launcherPaths":{"startmenu":"\/"}}
	// ,{"id":"demo-layout","type":"demo/layout","className":"Crm.LayoutWindow","launcher":{"iconCls":"layout-icon","shortcutIconCls":"demo-layout-shortcut","text":"Layout Window","tooltip":"<b>Layout Window<\/b><br \/>A layout window"},"launcherPaths":{"startmenu":"\/"}}
	// ,{"id":"demo-tab","type":"demo/tab","className":"Crm.TabWindow","launcher":{"iconCls":"tab-icon","shortcutIconCls":"demo-tab-shortcut","text":"Tab Window","tooltip":"<b>Tab Window<\/b><br \/>A tab window"},"launcherPaths":{"startmenu":"\/"}}
	
	// ,{
		// "id":"qo-preferences",
		// "type":"system/preferences",
		// "className":"Crm.QoPreferences",
		// "launcher":{"iconCls":"qo-pref-icon","shortcutIconCls":"qo-pref-shortcut-icon","text":"QO Preferences","tooltip":"<b>QO Preferences<\/b><br \/>Allows you to modify your desktop"},
		// "launcherPaths":{"contextmenu":"\/","startmenutool":"\/"}
	// }
	
	// ,{"id":"qo-profile","type":"user/profile","className":"Crm.QoProfile","launcher":{"iconCls":"qo-profile-icon","shortcutIconCls":"qo-profile-shortcut-icon","text":"Profile","tooltip":"<b>Profile<\/b><br \/>Allows user profile administration"},"launcherPaths":{"contextmenu":"\/","startmenutool":"\/"}}
	// ],
});

</script>

</head>
<body scroll="no"></body>
</html>
<?
include_once "../inc/cfg_vars.php";
//******************************************************************************************************
function modules_list() {
 global $SVARS;
	$ln='he';
	$sql = "SELECT modules FROM companies WHERE id=$SVARS[cid] AND active=1";

	$company_modules=sql2array($sql, 0, 'modules', 1);
	
	if (!ereg('^[0-9]+(,[0-9]+)*$',$company_modules)) die('{success:false, msg:"no active modules"}');
	$sql = "SELECT * FROM modules WHERE id IN($company_modules) OR parent_id IN($company_modules) AND active=1 ORDER BY id, parent_id";
	$modules=sql2array($sql, 0, 0, 0);
	
	if ($modules)  foreach($modules as $m) {
		$Ms[$m['name']]=array(
			'launcherPaths'=>array('startmenu'=>$m["startmenu_$ln"].'/'),
			'launcher'=>array(
				'iconCls'=>$m['iconCls'],
				'shortcutIconCls'=>"$m[iconCls]-shortcut",
				'text'=>$m["title_$ln"],
				'tooltip'=>$m["tooltip_$ln"]
			)
		);
	}
	
	return "var Ms=".array2json($Ms).';';
}

?>
