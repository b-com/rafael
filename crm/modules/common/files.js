var policies_store=[];
var filesFolders=['כללי'];

SearchFilterField = Ext.extend(Ext.form.TwinTriggerField, {
	initComponent: function(){
		SearchFilterField.superclass.initComponent.call(this);
		this.on('keyup', this.keySearch, this);
		this.keyUpTask = new Ext.util.DelayedTask(this.onTrigger2Click, this);
	},
	emptyText:'חיפוש',
	enableKeyEvents:true,
	validationEvent:false,
	validateOnBlur:false,
	trigger1Class:'x-form-clear-trigger',
	trigger2Class:'x-form-search-trigger',
	hideTrigger1:true,
	width:180,

	keySearch: function(f,e){
		if (e.keyCode<48 && e.keyCode!=8) return;	// not word char or backspace
		else if (e.keyCode==13) { this.keyUpTask.cancel(); this.onTrigger2Click(); return; } // enter
		this.keyUpTask.delay(10);
	},
	
	onTrigger1Click: function(){
		this.store.filterBy(function(){return true;});
		this.triggers[0].hide();
	},

	onTrigger2Click: function(){
		var v=this.getRawValue().trim();
		if (v.length<1) { this.onTrigger1Click(); return; }
		//if (v.length<2) return;
		this.store.filterBy(function(record){
			if (record.data.name.indexOf(v)>-1) return true;
			else if (typeof(record.data.notes)!='undefined' && record.data.notes.indexOf && record.data.notes.indexOf(v)>-1) return true;
			else if (record.data.linkText && record.data.linkText.indexOf(v)>-1) return true;
			return false;
		});
		this.triggers[0].show();
	}
});


PoliciesCombo = function(d, config) {
	PoliciesCombo.superclass.constructor.call(this, {
		xtype:'iconcombo',
		itemCls:'rtl',
		valueField: 'id',
		displayField: 'name',
		iconClsField: 'icon',
		triggerAction: 'all',
		mode: 'local',
		forceSelection:true,
		store: new Ext.data.SimpleStore({fields: ['id', 'name', 'icon', 'group'], data: d})
	});
	if (config) Ext.apply(this, config);
};
Ext.extend(PoliciesCombo, Ext.ux.IconCombo);

UploadWin = function(grid) {
	this.grid=grid;
	this.fileExt='';

	if(this.grid.client_type=='group') var combo_hidden=true;

	//add custom folders
	var folders=this.grid.store.collect('folder');
	for(var i=0; i<folders.length; i++){ if (!filesFolders.inArray(folders[i])) filesFolders.push(folders[i]); }
	
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',
		fileUpload:true,
		labelWidth:55,
		height:400,
		defaultType:'textfield',
		items: [
			 {xtype:'combo', hiddenName:'folder', fieldLabel:'תיקייה', store:filesFolders, value:'כללי', anchor:'95%', listClass:'rtl', triggerAction:'all', allowBlank:false }
			// ,new PoliciesCombo(policies_store, {hidden:combo_hidden, fieldLabel:'שייך ל', hiddenName:'link', valueField:'police_id', displayField:'name', value:'', anchor:'95%'})
			// ,{xtype:'combo', hiddenName:'link', fieldLabel:'שייך ל', store:policies_store, value:'', width:150, listClass:'rtl', triggerAction:'all', forceSelection:true }
			,{xtype:'fileuploadfield', name:'file', id:'file', emptyText:'בחר קובץ...', fieldLabel:'שם קובץ', anchor:'95%', allowBlank:false,
				regex:/^[^\/\\\?:*<>"|]+$/, invalidText:'שם קובץ אינו יכול להכיל את התווים הבאים:<br><b>\\ / * ? " &lt; &gt; |</b>',
				listeners: { scope:this,
					fileselected:function(fld,v){ 
						v = v.replace(/\//g, '\\');
						v = v.replace(/^([^\\]+\\)*/g, '');	// remove path
						var regs = /^(.+)\.(.{2,4})$/.exec(v);
						if (regs && regs[2]) { fld.el.dom.value=regs[1]; this.fileExt=regs[2]; }
						else fld.el.dom.value=v;
						fld.validate();
						fld.el.dom.setAttribute('name','file_name');
						fld.el.dom.removeAttribute('readOnly');
						fld.el.dom.style.color='black';
					}
				}
			 }
			,{xtype:'textfield', name:'notes', fieldLabel:'הערות', anchor:'95%'}
		]
	});

	UploadWin.superclass.constructor.call(this, {
		title: 'העלאת מסמך',
		iconCls:'upload',
		width:430,
		height:400,
		modal:true,
		resizable:false,
		layout:'fit',
		plain:true,
		bodyStyle:'padding:5px;',
		buttonAlign:'center',
		items: this.form,
		buttons: [{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function(overwrite) {
		// alert(1);
		if(!this.form.form.isValid()) return;
		
		var folder = this.form.form.findField('folder').getRawValue().trim();
		this.form.form.findField('folder').setValue(folder);
		
		var fileName = this.form.form.findField('file_name').getRawValue().trim();
		if (this.fileExt) fileName=fileName+'.'+this.fileExt;
		
		// if (this.form.form.findField('link').getRawValue().trim()=='') this.form.form.findField('link').setValue(''); // clear link value
		
		// check if file already exists (in folder)
		this.overwriteRow=0;
		if (overwrite) this.overwriteRow = this.grid.fileExists(folder+'/'+fileName);
		else if (!overwrite && this.grid.fileExists(folder+'/'+fileName)) {
			var fileExistWin = new Ext.Window({
				title:'קובץ כבר קיים', width:300, plain:true, modal:true, border:false, closable:true, buttonAlign:'center', bodyStyle:{direction:'rtl',padding:'10px'},
				html:'קובץ בשם <b>'+fileName+'</b> קבר קיים בתקייה <b>'+folder+'</b>',
				buttons:[
					 {text:'החלף', handler:function(){fileExistWin.close(); this.submitForm(true);}, scope:this}
					,{text:'ביטול', handler:function(){fileExistWin.close();}}
				]				
			});
			fileExistWin.show();
			return;
		}
		
		this.form.form.submit({
			url: '?m=common/files&f=upload_files&client_id='+this.grid.client_id+(this.overwriteRow ? '&overwriteId='+this.overwriteRow.data.id :''),
			params:{client_type: this.grid.client_type},
			waitTitle: 'אנא המתן...',
			waitMsg: 'טוען את הקובץ...',
			scope:this,
			success: function(form, o){
				if (this.overwriteRow) this.grid.store.remove(this.overwriteRow);
				var rec = new this.grid.store.recordType({newRecord:true, folder:folder, name:fileName, id:o.result.id, user:o.result.user, size:o.result.size, date:Date.parseDate(o.result.date,'d/m/y H:i'), /*link:form.findField('link').getValue(), linkText:form.findField('link').getRawValue(),*/ notes:form.findField('notes').getRawValue()});
				rec.commit();
				this.grid.store.addSorted(rec);
				this.close();
				Ext.fly(this.grid.view.getRow(this.grid.store.indexOf(rec))).highlight("#b6ff6c", {attr:'background-color', easing:'easeIn', duration:3 });
			},
			failure: function(r,o){
				this.close();
				ajRqErrors(o.result);
			}
		});
	}	
};
Ext.extend(UploadWin, Ext.Window, {});

EditWin = function(grid) {
	this.grid=grid;
	this.fileExt='';
	this.record=grid.getSelectionModel().getSelections()[0];
	
	if(this.grid.client_type=='group') var combo_hidden=true;

	var regs = /^(.+)\.(.{2,4})$/.exec(this.record.data.name);
	if (regs && regs[2]) { this.file_name=regs[1]; this.fileExt=regs[2]; }
	else this.file_name=this.record.data.name;

	//add custom folders
	var folders=this.grid.store.collect('folder');
	for(var i=0; i<folders.length; i++){ if (!filesFolders.inArray(folders[i])) filesFolders.push(folders[i]); }
	
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',
		labelWidth:55,
		defaultType:'textfield',
		items: [
			 {xtype:'combo', hiddenName:'folder', fieldLabel:'תיקייה', store:filesFolders, value:this.record.data.folder, anchor:'95%', listClass:'rtl', triggerAction:'all', allowBlank:false }
			,   new PoliciesCombo(policies_store, {hidden:combo_hidden, fieldLabel:'שייך ל', hiddenName:'link', valueField:'police_id', displayField:'name', value:this.record.data.link, anchor:'95%'})
			// ,{xtype:'combo', hiddenName:'link', fieldLabel:'שייך ל', store:policies_store, value:this.record.data.link, width:150, listClass:'rtl', triggerAction:'all', forceSelection:true }
			,{xtype:'textfield', name:'file_name', fieldLabel:'שם קובץ', value:this.file_name, anchor:'95%',
				regex:/^[^\/\\\?:*<>"|]+$/, invalidText:'שם קובץ אינו יכול להכיל את התווים הבאים:<br><b>\\ / * ? " &lt; &gt; |</b>'}
			,{xtype:'textfield', name:'notes', fieldLabel:'הערות', value:this.record.data.notes, anchor:'95%'}
			,{xtype:'hidden', name:'id', value:this.record.data.id }
			,{xtype:'hidden', name:'file_ext', value:this.fileExt }
		]
	});

	EditWin.superclass.constructor.call(this, {
		title: 'עריכת מסמך',
		iconCls:'edit_file',
		width:430,
		
		modal:true,
		resizable:false,
		layout:'fit',
		plain:true,
		bodyStyle:'padding:5px;',
		buttonAlign:'center',
		items: this.form,
		buttons: [{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		var folder = this.form.form.findField('folder').getRawValue().trim();
		this.form.form.findField('folder').setValue(folder);
		
		var fileName = this.form.form.findField('file_name').getRawValue().trim();
		if (this.fileExt) fileName=fileName+'.'+this.fileExt;
		
		if (this.form.form.findField('link').getRawValue().trim()=='') this.form.form.findField('link').setValue(''); // clear link value
		
		// check if file already exists (in folder)
		var fileExists=false;
		this.grid.store.each(function(r){
			if (folder==r.data.folder && fileName==r.data.name && this.record.data.id!=r.data.id) { fileExists=r; return; }
		}, this);
		if (fileExists) {
			Ext.Msg.show({title:'קובץ כבר קיים', msg:'קובץ בשם <b>'+fileName+'</b> קבר קיים בתקייה <b>'+folder+'</b><br>בחר שם אחר.', minWidth:400, modal:true, icon:Ext.Msg.INFO, buttons:Ext.Msg.OK});
			return;
		}
		
		this.form.form.submit({
			url: '?m=common/files&f=update_file&client_id='+this.grid.client_id,
			params:{client_type: this.grid.client_type},
			waitTitle: 'אנא המתן...',
			waitMsg: 'טוען...',
			scope:this,
			success: function(form, o){
				if (folder==this.record.data.folder) { // same folder
					this.record.beginEdit();
					this.record.set('name', fileName);
					this.record.set('notes', this.form.form.findField('notes').getValue());
					this.record.set('link', this.form.form.findField('link').getValue());
					this.record.set('linkText', this.form.form.findField('link').getRawValue());
					this.record.endEdit();
					this.record.commit();
					this.grid.getSelectionModel().deselectRow(this.grid.store.indexOf(this.record));
				}else { // different folder
					rCopy=this.record.copy();
					this.grid.store.remove(this.record);
					rCopy.set('folder', folder);
					this.grid.store.addSorted(rCopy);
					this.record=rCopy;
				}
				this.close();
				Ext.fly(this.grid.view.getRow(this.grid.store.indexOf(this.record))).highlight("#b6ff6c", {attr:'background-color', easing:'easeIn', duration:2 });
			},
			failure: function(r,o){
				this.close();
				ajRqErrors(o.result);
			}
		});
	}	
};
Ext.extend(EditWin, Ext.Window, {});

EditFolderWin = function(folderName, grid) {
	var folders=grid.store.collect('folder');
	for(var i=0; i<folders.length; i++){ if (!filesFolders.inArray(folders[i])) filesFolders.push(folders[i]); }
	this.form = new Ext.form.FormPanel({
		baseCls:'x-plain',
		labelWidth:55,
		defaultType:'textfield',
		items:[
			{xtype:'textfield', name:'newName', fieldLabel:'תיקייה', value:folderName, anchor:'95%'}
			,{xtype:'hidden', name:'oldName', value:folderName}
		]
	});
	EditFolderWin.superclass.constructor.call(this, {
		title:'שינוי שם תקייה', iconCls:'edit_file', width:430, modal:true, resizable:false, plain:true, layout:'fit', bodyStyle:'padding:5px;', buttonAlign:'center', 
		items:this.form,
		buttons:[{text:'שמור', handler:function(){this.submitForm();}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		var newName = this.form.form.findField('newName').getValue().trim();
		this.form.form.findField('newName').setValue(newName);
		if (newName==folderName) { this.close(); return; } // no change
		for(var i=0; i<filesFolders.length; i++) if(newName==filesFolders[i]){ Ext.Msg.alert('שגיאה', 'תקייה בשם <b>'+newName+'</b> כבר קיימת<br>אנא בחר שם אחר.'); return; }
		this.form.form.submit({
			url: '?m=common/files&f=update_folder_name&client_id='+grid.client_id,
			waitTitle:'אנא המתן...', waitMsg:'טוען...', scope:this,
			success:function(){ this.close(); grid.store.reload(); },
			failure:function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}
};
Ext.extend(EditFolderWin, Ext.Window, {});

MoveToClientWin = function(grid) {
	this.clientType=grid.client_type;
	
	this.clientsStore = new Ext.data.JsonStore({url:'/?m=crm/fax_f&f=select_client&clientType='+this.clientType, totalProperty:'total', root:'data', fields:['id','name']});
	this.clientsCombo = new Ext.form.ComboBox({
		fieldLabel:(this.clientType=='client' ? 'לקוח' : 'מעסיק'), 
		hiddenName:'toClient', store:this.clientsStore, valueField:'id', displayField:'name', minChars:2, forceSelection:true,
		selectOnFocus:true, width:280, loadingText:'טוען...', emptyText:'חיפוש...', listClass:'rtl', //triggerAction:'all', allowBlank:false,
		listeners:{
			select: function(combo, record, index){
				this.policiesStore.removeAll();
				this.policiesCombo.clearValue();
				if (this.clientsCombo.getValue()) {
					this.policiesCombo.emptyText='טוען...';
					this.policiesCombo.clearValue();
					this.loadClientData(this.clientsCombo.getValue());
					Ext.getCmp('movefile_btn').enable();
				}else Ext.getCmp('movefile_btn').disable();
			},scope:this
		}
	});
	
	this.loadClientData=function(client_id){
		Ext.Ajax.request({scope:this
			,url:'/?m=crm/fax_f&f=select_police&client_id='+client_id+'&clientType='+this.clientType
			,success: function(r,o){
				r=Ext.decode(r.responseText);
				if (r.policies.length){ this.policiesStore.loadData(r.policies); this.policiesCombo.enable(); this.policiesCombo.emptyText='בחר פוליסה/תביעה...'; }
				else { this.policiesCombo.disable(); this.policiesCombo.emptyText='אין פוליסות/תביעות'; }
				this.policiesCombo.clearValue();
				
				if (r.folders.length){
					var folders=[];
					Ext.apply(folders, filesFolders);
					for(var i=0; i<r.folders.length; i++) if (!folders.inArray(r.folders[i])) folders.push(r.folders[i]);
					this.folderCombo.store.loadData(folders);
					this.folderCombo.setValue('כללי');
				}
			},failure:function(r){}
		});
	};
	
	this.folderCombo = new Ext.form.ComboBox({fieldLabel:'תיקייה', hiddenName:'file_cat', store:filesFolders, value:'כללי', triggerAction:'all', width:280, mode:'local', listClass:'rtl', allowBlank:false});
	
	this.policiesStore = new Ext.data.JsonStore({fields:['id','name', 'icon', 'group']});
	this.policiesCombo = new Ext.ux.IconCombo({
		fieldLabel:'פוליסה/תביעה', id:'policies_combo', valueField:'id', displayField:'name', hiddenName:'link', iconClsField:'icon', 
		width:280, itemCls:'rtl', mode:'local', triggerAction:'all', forceSelection:true, disabled:true, selectOnFocus:true, loadingText:'...טוען', listClass:'rtl',
		store:this.policiesStore
	});
	
	var records = grid.getSelectionModel().getSelections();
	var ids=[];
	Ext.each(records, function(r){ids.push(r.data.id);});

	this.form = new Ext.form.FormPanel({
		baseCls: 'x-plain',
		labelWidth:80,
		defaultType: 'textfield',
		items: [
			this.clientsCombo, this.folderCombo, this.policiesCombo
			,{xtype:'hidden', name:'ids', value:ids.join(',')}
		]
	});

	MoveToClientWin.superclass.constructor.call(this, {
		title: 'העברת קובץ ללקוח אחר', iconCls:(this.clientType=='client'?'user-link':'business_link'),
		width:400, modal:true, resizable:false, plain:true, bodyStyle:'padding:5px;', buttonAlign:'center',
		items: this.form,
		buttons: [{id:'movefile_btn', text:'<b>העבר</b>', disabled:true, handler:function(){this.submitForm()}, scope:this},{text:'ביטול', handler:function(){this.close();}, scope:this}]
	});

	this.submitForm = function() {
		if(!this.form.form.isValid()) return;
		this.form.form.submit({scope:this, waitTitle:'אנא המתן...', waitMsg:'טוען...', 
			url:'?m=common/files&f=change_client&client_id='+grid.client_id+'&clientType='+this.clientType,
			success:function(){ this.close(); grid.store.reload(); },
			failure:function(r,o){ this.close(); ajRqErrors(o.result); }
		});
	}
};
Ext.extend(MoveToClientWin, Ext.Window, {});

FilesGrid = function(cfg) {//client_id,client_type,group_email
	this.client_id=cfg.client_id;
	this.client_type=cfg.client_type;
	
	this.store = new Ext.data.GroupingStore({
		proxy: new Ext.data.HttpProxy({
			url: '?m=common/files&f=files_tree&client_id='+this.client_id+'&client_type='+this.client_type,
			method:'GET'}),
		reader: new Ext.data.JsonReader({root:'data', totalProperty:'total'}, ['id','folder','name','link','linkText',{name:'date',type:'date', dateFormat:'d/m/y H:i'},'size','user_id','notes']),
		remoteSort:true,
		sortInfo:{field:'date', direction:'DESC'},
		groupField:'folder'
	});

	//this.searchField = new Ext.app.SearchField({store:this.store, width:100});
	this.searchFilter = new SearchFilterField({store:this.store, width:100});

	this.store.on('beforeload', function(){
		this.store.removeAll();
		//this.store.baseParams.q=this.searchField.getRawValue().trim();;
	}, this);

	this.store.on('load', function(){	// check for errors
		var callback=function() { this.store.load(); };
		if (this.store.getCount()<50) this.view.expandAllGroups();
		ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
	}, this);

	// this.store.load();

	this.fileExists = function(fullName){
		var exists=false;
		this.store.each(function(r){
			if (fullName==r.data.folder+'/'+r.data.name) { exists=r; return; }
		});
		return exists;
	}

	this.delFilePromt = function(){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		if (!this.getSelectionModel().getCount()) return;
		var records = this.getSelectionModel().getSelections();
		var names=[];
		Ext.each(records, function(r){names.push(r.data.folder+'/<b>'+r.data.name+'</b>');});
		Ext.MessageBox.show({
			width:400, title:'למחוק קבצים?', msg:'האם ברצונך למחוק את קבצים:<br>'+names.join(',<br>')+' ?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') this.delFile(records); }
		});
	};

	this.delFile = function(records){
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});

		this.getEl().mask('...טוען');
		Ext.Ajax.request({
			url: '?m=common/files&f=del_file&client_id='+this.client_id+'&ids='+ids.join(','),
			success: function(r,o){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					Ext.each(records, function(d){
						row=this.store.indexOf(d);
						this.selModel.selectRow(row);
						this.view.getRow(row).style.border='1px solid red';
						Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
						// topMsg.msg('פקס נמחק','פקס <b>'+d.data.from+'</b> <span dir=ltr>'+d.data.date+'</span>' +' נמחק בהצלחה');
					},this);
				}
				this.getEl().unmask();
			},
			failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	};

	this.ShowUploadWin = function(){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		var uploadWin=new UploadWin(this);
		uploadWin.show();
	};
	
	this.ShowEditWin = function(){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		var editWin=new EditWin(this);
		editWin.show();
	};	
	
	this.showEditFolderWin = function(folder){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		var editFolderWin=new EditFolderWin(folder, this);
		editFolderWin.show();
	};

	this.emailWindow = function(){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		var sel=this.getSelectionModel().getSelections();
		//var d=this.getSelectionModel().getSelections()[0].data;
		if (!sel[0].data) return;
		var win=new ContactWindow({grid:this, type:'mail', attach:sel}, null);//, clientEmail
		win.show();
	}
	

	this.sendFaxWin = function(){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		if (!faxActive) { Ext.Msg.alert('שרות פקס', 'לקבלת שרות פקס התקשרו: 074-7144333'); return; }
		var sel=this.getSelectionModel().getSelections();
		if (!sel[0].data) return;
		// Dump(sel[0].data);
		var win=new SendFax({grid:this, file:sel[0].data});
		win.show();
	};
	
	
	this.tbar = [
		{ iconCls:'x-tbar-loading', tooltip:'רענן', scope:this, handler:function(){this.store.reload();} }
		,this.searchFilter
		, '-' , { text:'העלאת מסמך', iconCls:'upload', scope:this, handler:this.ShowUploadWin }
		, '-' , { text:'סריקת מסמך', iconCls:'scan', scope:this, disabled:(!Ext.isIE && !Ext.isGecko), handler:function(){
			if (this.previewWin.isVisible()) this.previewWin.hide();
			var win=window.open('http://'+location.host+'/crm/?m=common/scan&client_id='+this.client_id+'&client_type='+this.client_type, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
			win.focus();
		} }
		, '-' ,{ text:'ערוך', id:'edit_file', iconCls:'edit_file', handler:this.ShowEditWin, disabled:true, scope:this }
		, '-' ,{ text:'הורד למחשב', id:'dl_file', iconCls:'download', handler:function(){var d=this.getSelectionModel().getSelections()[0].data; location.href='http://'+location.host+'/crmdownload/'+d.id+'/'+encodeURIComponent(d.name);}, disabled:true, scope:this }
		, '-' ,{ text:'שלח בדוא"ל', id:'send_mail', iconCls:'mail_add', handler:this.emailWindow, disabled:true, scope:this }
		, '-' ,{ text:'שלח בפקס', id:'send_fax', iconCls:'fax-send',    handler:this.sendFaxWin,  disabled:true, scope:this }
		, '-' ,{ text:'מחק', id:'del_file', iconCls:'delete', handler:this.delFilePromt, disabled:true, scope:this }
	];

	if(this.client_type=='group') var column_hidden=true;

	FilesGrid.superclass.constructor.call(this, {
		id:'FilesGrid'+this.client_id,
		monitorResize:true,	width:'100%', border:false,	minColumnWidth:18,	enableColLock:false, loadMask:true,	stripeRows:true, enableHdMenu:false,
		title:'מסמכים',	autoExpandColumn:'notes',
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		view: new Ext.grid.GroupingView({
			groupTextTpl: '<span dir=rtl style="height:16px;line-height:16px; padding-right:18px; background:url(skin/icons/folder.gif) no-repeat top right; zoom:1; display:block;"><b>{group}</b></span>'
			,startCollapsed:true 
			,enableRowBody: true
		}),
		columns:[{id:'folder', dataIndex:'folder', hidden:true }
				,{dataIndex:'id', width:62, renderer:function(v,m,r){return '<span class="b-action download" ext:qtip="הורד למחשב"></span>&nbsp;<span class="b-action new_win" ext:qtip="פתח בחלון חדש"></span>'+((/\.(pdf|jpg|jpeg|gif|png)$/).test(r.data.name)?'&nbsp;<span class="b-action preview" ext:qtip="הצג"></span>':'');} }
				,{header:'נוסף ב', id:'date', width:85, dataIndex:'date', sortable:true, css:'direction:ltr;', align:'right', renderer:Ext.util.Format.dateRenderer('d/m/y H:i') }
				,{header:'משתמש', id:'user', width:120, dataIndex:'user_id', sortable:true, renderer:userRenderer}
				,{header:'גודל', id:'size', width:55, dataIndex:'size', sortable:true, css:'direction:ltr;padding-left:5px;' }
				,{header:'הערות', id:'notes', dataIndex:'notes', sortable:false, renderer: function(v,meta) {meta.attr='ext:qtip="'+Ext.util.Format.htmlEncode(v)+'"'; return v; } }
				,{header:'שייך ל', id:'linkText', width:90, dataIndex:'linkText', sortable:true, hidden:column_hidden }
				,{header:'שם הקובץ', id:'name', width:230, dataIndex:'name', sortable:true, scope:this, 
					renderer:function(name){
						var regs=/^(.+)(\.[^\.]{2,4})$/.exec(name);
						return '<img unselectable=on src="/crm/skin/0.gif" class="file_icon '+this.file_cls(name)+'"/> '+ (regs ? '<span dir=rtl>'+regs[1]+'</span><font color=silver>'+regs[2]+'</font>' : name);
					}.createDelegate(this)
				}
				,{dataIndex:'', width:30 }
		]
	});

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('del_file').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('dl_file').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('edit_file').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('send_mail').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('send_fax').setDisabled(sm.getCount()!=1);
	}, this);
	
	this.preview = function(d) {
		var previewWin = Ext.getCmp('previewWin');
		if (!previewWin) previewWin = new Ext.Window({
			id:'previewWin', html:'<iframe id=prevIframe src="about:blank" style="width:100%; height:100%;" frameBorder=0></iframe>',
			x:5, y:50, width:(Ext.getBody().getWidth()-270), height:(Ext.getBody().getHeight()-55), plain:true, border:false, layout:'fit', closable:false,
			listeners: { hide:function(){Ext.get('prevIframe').dom.src='about:blank';} }
		});
		if ((/\.(pdf|jpg|jpeg|gif|png)$/i).test(d.name)) {
			this.prev_d=d;
			previewWin.setTitle('<div class="x-tool x-tool-close" style="float:right" onClick="Ext.getCmp(\'previewWin\').close();"></div> <span class="span_icon '+this.file_cls(d.name)+'">'+d.name+'</span>');
			previewWin.show();
			var iframe = Ext.get('prevIframe');
			if (iframe.dom.src!='about:blank') iframe.dom.src='about:blank';
			iframe.dom.src='http://'+location.host+'/crmview/'+d.id+'/'+encodeURIComponent(d.name);
		}
	}
	
	this.on('rowdblclick', function(grid, row, e){
		var d=this.getSelectionModel().getSelected().data;
		if ((/\.(pdf|jpg|jpeg|gif|png)$/i).test(d.name)) this.preview(d);
	});
	
	this.append=function(id){
		var win=window.open('http://'+location.host+'/crm/?m=common/scan&client_id='+this.client_id+'&client_type='+this.client_type+'&append='+id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
		win.focus();
	}
	
	this.joinPdfs=function(sel){
		var ids=[];
		Ext.each(sel, function(r){ids.push(r.data.id);});

		Ext.Ajax.request({
			url: '?m=common/files&f=join_pdfs&client_id='+this.client_id+'&client_type='+this.client_type+'&ids='+ids.join(','),
			success: function(r){
				d=Ext.decode(r.responseText);
				if (!ajRqErrors(d)) {
					var rec = new this.store.recordType({newRecord:true, folder:d.folder, name:d.name, id:d.id, size:d.size, date:d.date, link:'', linkText:'', notes:''});
					rec.commit();
					this.store.addSorted(rec);
					Ext.fly(this.view.getRow(this.store.indexOf(rec))).highlight("#b6ff6c", {attr:'background-color', easing:'easeIn', duration:3 });
				}
			},
			failure: function(r){ Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	}
	
	
	this.moveToFolder = function(b){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		if (!this.getSelectionModel().getCount()) return;
		
		var records = this.getSelectionModel().getSelections();
		var ids=[];
		Ext.each(records, function(r){ids.push(r.data.id);});
		Ext.Ajax.request({
			url: '?m=common/files&f=change_folder&newFolder='+encodeURIComponent(b.text)+'&client_id='+this.client_id+'&client_type='+this.client_type+'&ids='+ids.join(','),
			success: function(r){ if (!ajRqErrors(Ext.decode(r.responseText))) this.store.reload(); },
			failure: function(r){ Ext.Msg.alert('Error', 'Request failed.'); },
			scope: this
		});
	}	
	
	this.moveToClientWin = function(){
		if (this.previewWin.isVisible()) this.previewWin.hide();
		if (!this.getSelectionModel().getCount()) return;

		var win=new MoveToClientWin(this);
		win.show();
	};

	
	// this.moveToClient = function(){
		// if (this.previewWin.isVisible()) this.previewWin.hide();
		// if (!this.getSelectionModel().getCount()) return;
		
		// var records = this.getSelectionModel().getSelections();
		// var ids=[];
		// Ext.each(records, function(r){ids.push(r.data.id);});
		// Ext.Ajax.request({
			// url: '?m=crm/crm_files_f&f=change_client&newClien='+encodeURIComponent(b.text)+'&client_id='+this.client_id+'&client_type='+this.client_type+'&ids='+ids.join(','),
			// success: function(r){ if (!ajRqErrors(Ext.decode(r.responseText))) this.store.reload(); },
			// failure: function(r){ Ext.Msg.alert('Error', 'Request failed.'); },
			// scope: this
		// });
	// };
	
	this.on('rowcontextmenu', function(grid, row, e){
		var d=this.store.getAt(row).data;
		var sm=this.selModel;

		if (sm.getCount()>1) {
			this.menu = new Ext.menu.Menu({
				items: [{text:'שלח בדוא"ל', iconCls:'mail_add', scope:this, handler:this.emailWindow}]
			});
			// check if all selected files are PDF and add button
			var allPdf=true;
			var sel=sm.getSelections();
			Ext.each(sel, function(r){ if (!(/\.pdf$/).test(r.data.name)) allPdf=false; });
			if (allPdf) this.menu.add({text:'חבר לקובץ אחד', iconCls:'page_add', handler:function(){this.joinPdfs(sel);}, scope:this });

		}else {
			this.selModel.selectRow(row);
			
			this.menu = new Ext.menu.Menu({
				items: [
					 {text:'ערוך', iconCls:'edit_file', scope:this, handler:function(){ this.ShowEditWin(); } }
					,{text:'פתח בחלון חדש', iconCls:'new_win', scope:this, handler:function(){ var win=top.window.open('http://'+location.host+'/crmview/'+d.id+'/'+encodeURIComponent(d.name), 'file'+d.id, 'width=900,height=1000,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');  win.focus(); } }
					,{text:'הורד למחשב', iconCls:'download', scope:this, handler:function(){location.href='http://'+location.host+'/crmdownload/'+d.id+'/'+encodeURIComponent(d.name);} }
					,{text:'שלח בדוא"ל', iconCls:'mail_add', scope:this, handler:this.emailWindow }
				]
			});
			if ((/\.pdf$/).test(d.name)) this.menu.add({text:'שלח בפקס', iconCls:'fax-send', handler:this.sendFaxWin, scope:this });
			
			if ((/\.pdf$/).test(d.name)) this.menu.add({text:'הוסף עמודים', iconCls:'page_add', handler:function(){this.append(d.id);}, scope:this });
			this.menu.add({text:'מחק', iconCls:'delete', scope:this, handler:function(){this.delFilePromt()} });
		}
		
		this.menu.addSeparator();
		
		var foldersMenu=[];
		var folders=this.store.collect('folder');
		for(var i=0; i<folders.length; i++) foldersMenu.push({text:folders[i], iconCls:'folder', handler:this.moveToFolder, scope:this});
		foldersMenu.push({
			xtype:'panel', layout:'table', baseCls:'x-plain', style:'direction:rtl',
			items: [{xtype:'label', text:'חדש:'},{xtype:'textfield', id:'newFolderName', width:90}
				,{xtype:'button', text:'OK', handler:function(){this.menu.hide(); this.moveToFolder({text:Ext.getCmp('newFolderName').getValue()});}, scope:this}
			]
		});
		this.menu.add({text:'&nbsp; &nbsp; &nbsp; העבר לתקייה', iconCls:'folder', scope:this, menu:{items:foldersMenu}});
		
		if(this.client_type=='client') this.menu.add({text:'העבר ללקוח אחר', iconCls:'user-link', scope:this, handler:this.moveToClientWin});

		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	// folder context menu
	this.on('render', function(grid, row, e){
		this.view.mainBody.on('contextmenu', function(e){
			e.preventDefault();
			var hd = e.getTarget('.x-grid-group-hd', this.view.mainBody);
			if (hd){
				var folder = hd.parentNode.id.match(/folder-(.+)/)[1];
				this.groupHeaderMenu = new Ext.menu.Menu({
					items:[{text:'שנה שם תקייה', iconCls:'folder', scope:this, handler:function(){this.showEditFolderWin(folder)}}]
				});
				this.groupHeaderMenu.showAt(e.getXY());
			}
		}, this);
	});
	
	this.file_cls = function(file) {
		var regs = /^(.+)\.(.{3,4})$/.exec(file);
		if (!regs || !regs[2]) return 'file_other';
		
		var ext = regs[2].toLowerCase();
		if (ext=='doc' || ext=='docx' || ext=='wri') return 'file_doc';
		else if (ext=='xls' || ext=='xlsx' || ext=='csv') return 'file_xls';
		else if (ext=='pdf') return 'file_pdf';
		else if (ext=='ppt' || ext=='pps') return 'file_ppt';
		else if (ext=='zip' || ext=='arj') return 'file_zip';
		else if (ext=='rar') return 'file_rar';
		else if (ext=='jpg' || ext=='jpeg') return 'file_jpg';
		else if (ext=='gif') return 'file_gif';
		else if (ext=='png') return 'file_png';
		else if (ext=='tif' || ext=='tiff') return 'file_tif';
		else if (ext=='exe') return 'file_exe';
		else if (ext=='msg') return 'file_msg';
		else return 'file_other';
	}		

	this.resizePanel=function() {
		Ext.getDom(this.getEl()).style.overflow='hidden';
		Ext.getDom(this.getEl()).style.width = '100px';
		Ext.getDom(this.getEl()).style.height= '100px';
		this.syncSize();
		var size=Ext.get(this.getEl().dom.parentNode).getSize(true);
		Ext.getDom(this.getEl()).style.width = size.width+'px';
		Ext.getDom(this.getEl()).style.height = size.height+'px';
		this.syncSize();
	}
};
Ext.extend(FilesGrid, Ext.grid.GridPanel);