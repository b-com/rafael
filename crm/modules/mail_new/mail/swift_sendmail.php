﻿<?
/**********************  IMPORTANT ********************
*          Send mail work only with UTF-8 charset       *
*******************************************************/
function swift_send_mail($to, $subject, $message_body, $cfg=NULL) {
//	global $group_id, $SVARS;
	set_time_limit(1800);
	ini_set('memory_limit', '64M');
	
	// Load Swift library
	require_once dirname(__FILE__).'/SwiftMailer/lib/swift_required.php';
	require_once dirname(__FILE__).'/buildMessage.php';
	
	// Create the message
	$message = Swift_Message::newInstance();
	
	// This method takes an integer value between 1 and 5:
	// 1 - Highest
	// 2 - High
	// 3 - Normal
	// 4 - Low
	// 5 - Lowest
	if($cfg['importance']) $message->setPriority($cfg['importance']);
	
	//It is possible to request a read-receipt to be sent to an address when the email is opened
	//Read receipts won’t work for the majority of recipients since many mail clients auto-disable them.
	//Those clients that will send a read receipt will make the user aware that one has been requested.
	//$message->setReadReceiptTo('your@address.tld');
	
	// Give the message a subject
	$message->setSubject($subject);
	
	// Temporary solution for templates
	if ($cfg['template'] AND is_dir(dirname(__FILE__)."/$cfg[template]")) {
		$template_dir=realpath(dirname(__FILE__)."/$cfg[template]");

		$message_body=str_replace('{TEXT}', $message_body, file_get_contents("$template_dir/index.htm"));
		$message_body=str_replace('mail_top.png',$message->embed(Swift_Image::fromPath($template_dir.'/mail_top.png')), $message_body);
		$message_body=str_replace('mail_bottom.png', $message->embed(Swift_Image::fromPath($template_dir.'/mail_bottom.png')), $message_body);
	}
	
	// Set the From address with an associative array('john@doe.com' => 'John Doe')
	// If you set multiple From: addresses then you absolutely must set a Sender:
	// address to indicate who physically sent the message.
	$message->setFrom($cfg['from']);
	// $cfg['sender'] = $cfg['sender'] ? $cfg['sender'] : array_slice($cfg['from'],0,1,true);
	// $message->setSender($cfg['sender']);
	
	// Set Recipients
	# split $to address and add them to array
	$to_ar = explode(',', $to);
	$to = array();
	if ($to_ar) { foreach ($to_ar as $row) { $row = trim($row); if ($row) $to[$row]= ''; } }
	if (!is_array($to)) { $GLOBALS['send_mail_result']['message'] = "No to address";  return; }
	$message->setTo($to);

	//The Return-Path: address specifies where bounce notifications should be sent
	$actualAddress = array_keys($cfg['from']);
	$message->setReturnPath($actualAddress[0]);
	
	// Set the Reply To address
	if ($cfg['replyto']) $message->setReplyTo($cfg['replyto']);
	
	// Attach inline parts
	$MessageBuilder = new BuildMessage($message, $message_body);
	
	$message_body = $MessageBuilder->getMessage();
	
	// Set message body
	$message->setBody($message_body, 'text/html');

	// Add alternative parts
	$message_body = html2text($message_body,true);
	$message->addPart($message_body, 'text/plain');
	
	// Add addresses of recipients who will be copied in on the message
	if ($cfg['cc']){
		$cfg['cc'] = explode(',', $cfg['cc']);
		$message->setCc($cfg['cc']);
	}
	
	// Add addresses of recipients who the message will be blind-copied to. 
	// Other recipients will not be aware of these copies.
	if ($cfg['bcc']){
		$cfg['bcc'] = explode(',', $cfg['bcc']);
		$message->setBcc($cfg['bcc']);
	}
	
	// Attaching Existing Files
	$invalidFilenameCharacters = array("\\","/",":","?","*","\"","<",">","|","_");
	if ($cfg['file']){
		$file_name = $cfg['file'][1] ? $cfg['file'][1] : base_name($cfg['file'][0]);
		$file_name = str_replace($invalidFilenameCharacters,' ', $file_name);
		$attachment = Swift_Attachment::newInstance(file_get_contents($cfg['file'][0]), $file_name);
		// Attach it to the message
		$message->attach($attachment);
	}
	
	if ($cfg['files']) {	# $files[]=array('file path', 'file name');
		foreach($cfg['files'] as $file) {
			$file_name = $file[1] ? $file[1] : base_name($file[0]);
			$file_name = str_replace($invalidFilenameCharacters,' ', $file_name);
			$attachment = Swift_Attachment::newInstance(file_get_contents($file[0]), $file_name);
			// Attach it to the message
			$message->attach($attachment);
		}
	}
	
	if ($cfg['host'] AND $cfg['user'] AND $cfg['pass']) {
		if (!$cfg['port']) $cfg['port']=25; # default smtp port
		if (!$cfg['security']) $cfg['security'] = ''; // not secure connection
		
		// Create the Transport
		$transport = Swift_SmtpTransport::newInstance()
		  ->setUsername($cfg['user'])
		  ->setPassword($cfg['pass'])
		  ->setHost($cfg['host'])
		  ->setPort($cfg['port'])
		  ->setEncryption($cfg['security'])
		  ;
	}else $transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');// connect to MTA server
	
	// Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($transport);
	
	try{
		$result = $mailer->send($message, $failures);
	}catch(Swift_TransportException $e){
		$GLOBALS['send_mail_result']['message'] = 'Transport says:</br>'.$e->getMessage();
	}catch(Swift_IoException $e){
		$GLOBALS['send_mail_result']['message'] = 'IO says:</br>'.$e->getMessage();
	}catch(Swift_SwiftException $e){
		$GLOBALS['send_mail_result']['message'] = 'Send application says:</br>'.$e->getMessage();
	}catch(Exception $e){
		$GLOBALS['send_mail_result']['message'] = $e->getMessage();
	}
	
	if (!$result) {//return failed recipients
		$GLOBALS['send_mail_result']['failures'] = $failures;
		return false;
	}else {
		$MIMEMSG = $message->toString();
		return $MIMEMSG;
	}
}

?>