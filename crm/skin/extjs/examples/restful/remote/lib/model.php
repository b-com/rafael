tral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="2931356-112_neutral_LDR">
			<applicable disposition="staged">
				<updateComponent elevate="revision">
					<assemblyIdentity name="NetFx-ASPNET_FILTER_DLL" version="6.1.7601.22617" processorArchitecture="x86" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
			</applicable>
			<component>
				<assemblyIdentity name="e400454689408d5f73af4621722cc6fb" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="Trigger_14">
			<applicable disposition="staged">
				<updateComponent elevate="revision">
					<assemblyIdentity name="NetFx-ASPNET_FILTER_DLL" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
			</applicable>
		</update>
		<update name="2931356-113_neutral_LDR">
			<applicable disposition="staged">
				<updateComponent elevate="serviceLevel">
					<assemblyIdentity name="NetFx-ASPNET_FILTER_DLL" version="6.1.7601.22617" processorArchitecture="x86" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
				<detectUpdate>
					<update name="Trigger_14"/>
				</detectUpdate>
			</applicable>
			<component>
				<assemblyIdentity name="e400454689408d5f73af4621722cc6fb" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="2931356-114_neutral_GDR">
			<applicable disposition="staged">
				<updateComponent elevate="serviceLevel">
	