<?php

function wkhtmltopdf($html, $mode='embed', $file='doc.pdf',$params='--orientation Portrait --page-size A4',$add_pdf=''){
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set("display_errors", "On");

	$rnd=mt_rand();
	
	$tmp_html="../../temp/$rnd.html";
	$tmp_pdf="../../temp/$rnd.pdf";
	file_put_contents($tmp_html, $html);
	
	 exec('wkhtmltopdf '.$params.' '.$tmp_html.' '.$tmp_pdf, $execout);	// For Server!!!!
 	exec('wkhtmltopdf --orientation Portrait --page-size A4 '.($params?$params.' ':'').$tmp_html.' '.$tmp_pdf, $execout);
	exec('W:\utils\wkhtmltopdf\bin\wkhtmltopdf.exe --orientation Portrait --page-size A4 '.($params?$params.' ':'').$tmp_html.' '.$tmp_pdf, $execout);
	exec('wkhtmltopdf --page-size A4 --disable-smart-shrinking --orientation Portrait -B 0 -L 0 -R 0 -T 0 '.$tmp_html.' '.$tmp_pdf, $execout);

	
	unlink($tmp_html);
	if (!is_file($tmp_pdf)) return false;
	if($mode=='save') {
		rename(realpath($tmp_pdf), $file);
		return true;
	}
	
	$pdf_content=file_get_contents($tmp_pdf);
	unlink($tmp_pdf);
	
	$GLOBALS['NO_HEADERS']=1;
	
	if($mode=='embed'){
		header('Content-Type: application/pdf');
		header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Length: '.strlen($pdf_content));
		header('Content-Disposition: inline; filename="'.basename($file).'";');
		echo $pdf_content;
	
	}elseif($mode=='download'){
		header('Content-Description: File Transfer');
		header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		// force download dialog
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream', false);
		header('Content-Type: application/download', false);
		header('Content-Type: application/pdf', false);
		// use the Content-Disposition header to supply a recommended filename
		header('Content-Disposition: attachment; filename="'.basename($file).'";');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.strlen($pdf_content));
		echo $pdf_content;
	
	}else {
		return $pdf_content;
	}
 return true;
}
?>