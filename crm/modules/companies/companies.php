﻿<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
 
 	$ln='he';
	
	$modules=sql2array("SELECT * FROM modules WHERE active=1 AND parent_id=0 ORDER BY id",'','',0,'rafael');
	
	if ($modules)  foreach($modules as $m) {
		$Ms[]=array('defaults'=>"{baseCls:'x-plain', cls:'rtl'}",
			'items'=>array(
				array(
					'html'=>"<div class='".($m['shortcutIconCls']? $m['shortcutIconCls']: 'demo-grid-shortcut')."' style='width:100%; height:48px; background-position:center top; background-repeat:no-repeat;' onClick='Ext.getCmp(\"mod$m[id]\").setValue(!Ext.getCmp(\"mod$m[id]\").getValue());'></div>",
					'style'=>'text-align:center;','baseCls'=>'x-plain','cls'=>'rtl'
				),
				array('xtype'=>'checkbox', 'checked'=>false, 'boxLabel'=>$m["title_$ln"],	'name'=>"mod[$m[id]]")
			)
		);
	}
	
	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end FROM users ORDER BY fullName",'id');
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;
	}

	#Load contacts patterns
	$patterns_ar=sql2array("SELECT id, type, name FROM patterns ORDER BY name",'id');
	if ($patterns_ar) foreach($patterns_ar as $id=>$pattern) {
		if ($pattern['type']=='sms') $smsPatterns.=($smsPatterns ? ',':'')."[$id,'".jsEscape($pattern['name'])."']";
	}
	
	// $client_groups_ar=sql2array("SELECT * FROM client_groups",'id','name');
	
	// if ($client_groups_ar) foreach($client_groups_ar as $id=>$client_group) $client_groups.=($client_groups ? ',':'')."[$id,'".jsEscape($client_group)."']";

	?>
	
	var Modules=<?=array2json($Ms)?>;
	
	var smsActive=<?=($SVARS['user']['sms_active'] ? 'true' : 'false')?>;
	var faxActive=<?=($SVARS['user']['fax_active'] ? 'true' : 'false')?>;
	var fax_numbers=[];

	var users=[<?=$users;?>];
	var agents=[<?=$agents;?>];
	var users_obj=<?=array2json($users_ar);?>;
	var	sms_originator='';
	var smsPatterns=[<?=$smsPatterns;?>];
	// var client_groups=[<?=$client_groups;?>];
	
	Ext.ux.Loader.load([
			'skin/extjs/ux/superboxselect/superboxselect.css?<?=filemtime('skin/extjs/ux/superboxselect/superboxselect.css');?>',
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			'skin/extjs/ux/superboxselect/SuperBoxSelect.js?<?=filemtime('skin/extjs/ux/superboxselect/SuperBoxSelect.js');?>',
			'modules/common/contacts.js?<?=filemtime('modules/common/contacts.js');?>',
			'modules/common/files.js?<?=filemtime('modules/common/files.js');?>',
			'modules/users/users.js?<?=filemtime('modules/users/users.js');?>',
			'modules/companies/companies.js?<?=filemtime('modules/companies/companies.js');?>'
		], function(){Ms.companies.run();}
	);
	<?
}
//————————————————————————————————————————————————————————————————————————————————————
function f_list() {
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;

	if ($_POST['q']) {
		$make_where_search=make_where_search('login,name,cperson0,cnumber,tel0,tel1,fax,email,city0,city1,notes', $_POST['q']);
		if ($make_where_search) $where.=" AND ($make_where_search)";
	}

	$sql="SELECT * FROM clients WHERE active=".(int)$_POST['active'] 
		." ORDER BY $_POST[sort]".($_POST['dir']=='DESC'?' DESC':' ASC')
		." LIMIT $_POST[start],$_POST[limit]";
	$rows=sql2array($sql);
	
	if($rows) foreach($rows as $k=>$r) {
		if ($r['date_end']) $rows[$k]['date_end']=str2time($r['date_end'],'d/m/Y'); else $rows[$k]['date_end']='';
		$rows[$k]['modules'] = explode(',',$r['modules']);
	}
	echo '{total:"'.count($rows).'", data:'.array2json($rows).'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_faxes_list() {
	$sql="SELECT fax_number FROM fax_numbers WHERE company_id=".(int)$_REQUEST['id'];
	$rows=sql2array($sql);
	echo '{fax_numbers:'.($rows ? array2json($rows) : '[]').'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_load_data() {
 global $SVARS;
	$sql="SELECT * FROM companies WHERE id=$SVARS[cid]";
	$data=sql2array($sql,0,0,1);
	
	if($data){
		if($data['current_vat_period']!='0000-00-00'){
			$data['current_vat_month']=str2time($data['current_vat_period'],'m');
			$data['current_vat_year']=str2time($data['current_vat_period'],'Y');
		}
		else{
			$data['current_vat_month']=date('m');
			$data['current_vat_year']=date('Y');
		}
		if(!$data['vat_period']) $data['vat_period']=1;
	}
	
	echo '{data:'.($data ? array2json($data) : '[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_edit() {
 global $SVARS;
 
	$id=(int)$_REQUEST['id'];
	
	if(isset($_POST['current_vat_year']) AND isset($_POST['current_vat_month'])) $current_vat_period=quote($_POST['current_vat_year']).'-'.quote($_POST['current_vat_month']).'-01'; 
	
	$sql="clients SET name=".quote($_POST['name'])
		.", company_id=".(int)$SVARS['cid']
		.", cnumber=".quote($_POST['cnumber'])
		.", tel0=".quote($_POST['tel0'])
		.", tel1=".quote($_POST['tel1'])
		.", fax=".quote($_POST['fax'])
		.", email=".quote($_POST['email'])
		.", url=".quote($_POST['url'])
		.", vat_period=".quote($_POST['vat_period'])
		.($current_vat_period?", current_vat_period=".quote($current_vat_period):'')
		.", income_tax=".quote($_POST['income_tax'])
		.", notes=".quote($_POST['notes'])
		.($_POST['login'] ? ", login=".quote($_POST['login']):'')
		.(isset($_POST["active"])	? ($_POST["active"]=='true' ? ", active=1":", active=0"):'')
		.(isset($_POST["date_end"])	? ", date_end='".str2time($_POST['date_end'],'Y-m-d')."'":'')
		;
	
	if ($_POST['mod']) $sql.=", modules=".quote(implode(',',array_keys($_POST['mod']))); elseif (!$id) $sql.=", modules='10,49'";
	
	$n=0;
	for ($i=0; $i<3; $i++) if ($_POST["city$i"] OR $_POST["zip$i"] OR $_POST["adr$i"]) {$sql.=", adr_type$n=".(int)$_POST["adr_type$i"].", city$n=".quote($_POST["city$i"]).", zip$n=".quote($_POST["zip$i"]).", adr$n=".quote($_POST["adr$i"]); $n++;}
	for ($i=$n; $i<3; $i++) $sql.=", adr_type$i='', city$i='', zip$i='', adr$i=''";

	$n=0;
	for ($i=0; $i<5; $i++) if ($_POST["cperson$i"] OR $_POST["cp_tel$i"] OR $_POST["cp_email$i"]) {$sql.=", cperson$n=".quote($_POST["cperson$i"]).", cp_tel$n=".quote($_POST["cp_tel$i"]).", cp_email$n=".quote($_POST["cp_email$i"]); $n++;}
	for ($i=$n; $i<5; $i++) $sql.=", cperson$i='', cp_tel$i='', cp_email$i=''";

	$sql = ($id ? "UPDATE $sql WHERE id=$id" : "INSERT INTO $sql");
	
	if (runsql($sql)) {
		// if (!$id) {#Create new client
			// $id = mysql_insert_id();
			// #Create New Data Base with tables from main data base
			// runsql("CREATE DATABASE crm_$id CHARACTER SET utf8 COLLATE utf8_general_ci");
			// $tablesList = sql2array("SHOW TABLES FROM crm_1",0,'Tables_in_crm_1');
			// foreach($tablesList as $t) if ($t!='modules' AND $t!='fax_numbers') runsql("CREATE TABLE crm_$id.$t LIKE crm_1.$t");
			// #Create admin
			// runsql("INSERT INTO groups SET name='מנהלי מערכת'","crm_$id");
			// runsql("INSERT INTO users SET username='Admin', fname='Admin', lname='Admin', password='".md5('Admin')."', group_id=1, active=1","crm_$id");
		}
		
		if ($_POST['fax_numbers']) {
			runsql("DELETE FROM fax_numbers WHERE company_id=$id");
			$fax_numbers=explode(',',$_POST['fax_numbers']);
			// echo "/*<div dir=ltr align=left><pre>".print_r($fax_numbers,1)."</pre></div>*/";
			if ($fax_numbers) foreach($fax_numbers as $k=>$number) runsql("INSERT INTO fax_numbers SET company_id=$id, fax_number=".quote($number));
		}
		
		$name = jsEscape(trim($_POST['name']));
		echo "{id:$id, name:'$name', success:true}"; 
	}else echo '{success:false}';
}
?>