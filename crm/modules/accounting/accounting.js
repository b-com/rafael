SendInvoicesWindow = function() {
	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=send_invoices_list', totalProperty:'total', root:'data',
		fields: ['id','client_id','client_name','fax','email',{name:'date',type:'date', dateFormat:'Y-m-d'},'num','sum','print','send_doc_type','send_doc_to'],
		sortInfo: {field:'client_name', direction:'ASC'},
		timeout:1000, remoteSort:true
	});
	
	// this.sendTypeCombo = new Ext.form.ComboBox({store:['דוא"ל','פקס','לא לשלוח'], width:75, listWidth:75, listClass:'rtl', triggerAction:'all', forceSelection:true});
	this.sendTypeCombo = new Ext.form.ComboBox({store:['דוא"ל','לא לשלוח'], width:75, listWidth:75, listClass:'rtl', triggerAction:'all', forceSelection:true});
    
	var checkColumn = new Ext.grid.CheckColumn({header:"הדפס", dataIndex:'print', width:40,refreshSummary:false});

	this.colModel = new Ext.grid.ColumnModel([
		{header:'תאריך הפקה', dataIndex:'date', width:85, sortable:true, css:'direction:ltr;', align:'center', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
		,{header:"מס' חשבונית", width:90, dataIndex:'num'}
		,{header:'שם לקוח',width:20, dataIndex:'client_name', id:'client_name'}
		,{header:'סה"כ', width:90, dataIndex:'sum', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
		,{header:'לשלוח', width:75, dataIndex:'send_doc_type', editor:this.sendTypeCombo, renderer:function(v){return '<div style="background-image: url(/crm/skin/extjs/resources/images/default/form/trigger-rtl.gif);  background-position: -86px -2px; background-repeat: no-repeat;">'+v+'</div>'}}
		,{header:'פרטי משלוח', width:120, dataIndex:'send_doc_to', editor:new textField}
		,checkColumn
	]);
	
	this.grid = new Ext.grid.EditorGridPanel({
		stripeRows:true, layout:'fit', region:'center',	autoExpandColumn:'client_name', border:false, enableHdMenu:false, clicksToEdit:1,
		plugins: [checkColumn],
		store: this.store,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		colModel: this.colModel
		// ,bbar: new Ext.PagingToolbar({store:this.store, pageSize:50, displayInfo:true})
	});
	
	this.grid.on('afteredit', function(e){
		if (e.field == 'send_doc_type') {
			if (e.record.get('send_doc_type')=='פקס') e.record.set('send_doc_to',e.record.get('fax'));
			else if (e.record.get('send_doc_type')=='דוא"ל') e.record.set('send_doc_to',e.record.get('email'));
			else e.record.set('send_doc_to','');
		} 
	},this);
	
	this.grid.on('rowdblclick', function(grid,rInd,e){
		//console.log("");
		
		var sent_to_mail = grid.store.data.items[rInd].data.send_doc_to;
		//console.log(sent_to_mail);
		var data = grid.getSelectionModel().getSelected().data;
		//console.log(data);
		var type_id=1;
		var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type=invoice_accept&id='+data.id+'&sent_to_mail='+sent_to_mail+'&type_id='+type_id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	});
	
	this.grid.on('rowcontextmenu', function(grid, row, e){
		// var r=this.store.getAt(row);
		// this.menu = new Ext.menu.Menu({
			// items: [
				// {text:'פתח לקוח בחלון חדש', iconCls:'user_open', scope:this, handler: function(){openClientNewWin(r.data);} }
			// ]
		// });
		e.stopEvent();
		// this.menu.showAt(e.getXY());
	},this);
	
	this.send = function(){
	//alert(1);
		var data=[], error='', to_print=[];
		this.grid.stopEditing();
		this.store.each(function(r){
			if (r.data.send_doc_type=='פקס' || r.data.send_doc_type=='דוא"ל')  {
				if(!r.data.send_doc_to) {error+='חובה להזין נמען בשורה '+(r.store.indexOf(r)+1); return false;}
			} 
			
				if (r.data.print) to_print.push(r.data.id);
			
			data.push({client_id:r.data.client_id, id:r.data.id, num:r.data.num, send_doc_type:r.data.send_doc_type, send_doc_to:r.data.send_doc_to, print:r.data.print});
		}, this);
		//console.log(data);
		if (error!='') {Ext.Msg.alert('אזהרה!', error); return;} 

		if (data) { 
			this.getEl().mask('הפעולה מתבצעת. ניתן להמשיך לעבוד בחלונות אחרים.');
			Ext.Ajax.request({
				url: "?m=common/accounting&f=send_invoices",
				timeout:180000,
				params:{data:Ext.encode(data)},
				success: function(r,o){
					//Dump(r.responseText); exit;
					r=Ext.decode(r.responseText);
					this.store.reload();
					this.getEl().unmask();
				},
				failure: function(r){}, scope:this
			});
		}
		
		if (to_print.length>=1) {
			var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type=invoice_accept&ids='+to_print.join(','), 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
			win.focus();
		}
	}
	
	SendInvoicesWindow.superclass.constructor.call(this, {
		border:false, region:'center', layout:'border', items:this.grid
		// , buttons:[
			// {xtype:'button', cls:'btnRight', text:'שלח', iconCls:'other_police', handler:this.send, scope:this, width: 90}
		// ]
	})
	
	this.store.load();
};
Ext.extend(SendInvoicesWindow, Ext.Panel);

PeriodWindow = function(cfg) {
	var rep_periods1=[[1,'ינואר'],[2,'פברואר'],[3,'מרץ'],[4,'אפריל'],[5,'מאי'],[6,'יוני'],[7,'יולי'],[8,'אוגוסט'],[9,'ספטמבר'],[10,'אוקטובר'],[11,'נובמבר'],[12,'דצמבר']];
	var rep_periods2=[[1,'ינואר-פברואר'],[3,'מרץ-אפריל'],[5,'מאי-יוני'],[7,'יולי-אוגוסט'],[9,'ספטמבר-אוקטובר'],[11,'נובמבר-דצמבר']];

	this.form = new Ext.form.FormPanel({
		items:{layout:'table',style:'margin:3px;',layoutConfig: {columns:8}, baseCls:'x-plain',cls:'rtl',defaults:{baseCls:'x-plain', cls:'rtl'},items:[			
			{xtype:'label', text:'דיווח כל:', style:'display:block; padding-right:7px;'}
			,{xtype:'combo',store:[[1,'חודש'],[2,'חודשיים']],allowBlank:false,value:1,hiddenName:'vat_period',width:80,listClass:'rtl',triggerAction:'all',editable:false,forceSelection:true
				,listeners:{select:function(){
					if(this.form.find('hiddenName','vat_period')[0].getValue()==1) {
						this.form.find('hiddenName','current_vat_month')[0].store.loadData(rep_periods1);
						this.form.find('hiddenName','current_vat_month')[0].setValue(this.form.find('hiddenName','current_vat_month')[0].getValue());

					}
					else if(this.form.find('hiddenName','vat_period')[0].getValue()==2) {
						this.form.find('hiddenName','current_vat_month')[0].store.loadData(rep_periods2);
						this.form.find('hiddenName','current_vat_month')[0].setValue((this.form.find('hiddenName','current_vat_month')[0].getValue()%2==0?this.form.find('hiddenName','current_vat_month')[0].getValue()-1:this.form.find('hiddenName','current_vat_month')[0].getValue()));
					}
				},scope:this}}
			,{xtype:'label', text:'תקופת מע"מ נוכחית:'}		
			,{xtype:'combo', store:rep_periods1,value:today.format('n'),hiddenName:'current_vat_month', width:120,mode:'local',listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false,editable:false}
			,{xtype:'combo', store:years, value:today.format('Y'),hiddenName:'current_vat_year',  width:50, mode:'local',listClass:'rtl', triggerAction:'all', forceSelection:true, allowBlank:false,editable:false}

			,{xtype:'label', text:'מקדמות מס:',style:'display:block; padding-right:14px;'}
			,{xtype:'numberfield', name:'income_tax', width:20}							
			,{xtype:'label', text:' %'}
			,{xtype:'label', html:'<br>נא לקבוע תקופת דיווח נוכחית למע"מ.<br>בהמשך תקבע ע"י נעילת תקופת דיווח. ',colspan:8,style:'color:red;font-color:red;font-weight:bold'}
		]}
	});

	PeriodWindow.superclass.constructor.call(this, {
		title:'תקופת דיווח',
		width:580, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:[this.form],
		buttons:[
			{text:'שמור', handler:function(){
				if(!this.form.form.isValid()) return;
				this.submitForm();
				}, scope:this},
			{text:'בטל', handler:function(){this.close();}, scope:this}
		]
	});
	
	this.submitForm = function() {
		this.form.form.submit({
			url: "?m=accounting/accounting&f=init_vat_period",
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			success: function(f,a){
				if (cfg.callBack) cfg.callBack();
				this.close(); 
			},
			failure: function(r,o){ajRqErrors(o.result);},//this.close(); 
			scope:this
		});
	};
	this.on('afterrender', function(){this.form.find('hiddenName','current_vat_month')[0].focus(true,800);}, this);
};
Ext.extend(PeriodWindow, Ext.Window, {});

AccountingPan = function() {
	this.load = function(per) {
			Ext.Ajax.request({
			url: "?m=accounting/accounting&f=load_data",
			params:{period:per},
			success: function(r) {
				json = Ext.decode(r.responseText);
				
				if(json.data.current_vat_period=='0000-00-00') this.periodWindow();else {current_vat_period=new Date(json.data.current_vat_period);}
				Ext.getCmp('AccountingPan').tpl.overwrite(Ext.getCmp('AccountingPan').body, json.data);
				Ext.getCmp('CashPan').tpl.overwrite(Ext.getCmp('CashPan').body, json.data);
			},
			scope: this
		});
	};
	
	this.periodWindow =  function() {	
		this.getEl().mask();
		var win=new PeriodWindow({
			callBack:function(){this.load();this.getEl().unmask();}.createDelegate(this)})
		win.show();
	};
	
	AccountingPan.superclass.constructor.call(this, {region:'center',autoScroll:false,
	items:[
			{xtype:'panel',	id:'AccountingPan', border:false, title:'דוח רווח והפסד',
			 tpl: new Ext.Template(
				'<table class="t_accounting" cellspacing=0 border=0>', 
				'<tr>',
					'<td nowrap style="width:120px; cursor:hand; cursor:pointer; padding:3px;" onmouseover="this.className=\'td_on_image\';" onmouseout="this.className=\'td_off_image\';" onClick="Ms.accounting.invoiceWindow();" qtip="קבלה חדשה"><img src="modules/accounting/pict/cache.gif" align=absmiddle></br>אישור תשלום</td>',
					// '<td valign=top style="width:20%; padding:3px;">תקופה נוכחית<br><span style="font-weight:normal;font-size:11px;">'+current_vat_period.format('d/m/Y')+'-'+current_vat_period.add(Date.MONTH, vat_period).add(Date.DAY, -1).format('d/m/Y')+'<br><img src="modules/accounting/pict/lock16.png" onClick="Ms.accounting.closeVATPeriod();" style="cursor:hand; cursor:pointer; margin-top:3px;"  qtip="נעילת תקופת דיווח">'+'</span></td>',
					'<td valign=top style="width:20%; padding:3px;">תקופה נוכחית<br><span style="font-weight:normal;font-size:11px;">{current_vat_period_str}<br><img src="modules/accounting/pict/lock16.png" onClick="Ms.accounting.closeVATPeriod();" style="cursor:hand; cursor:pointer; margin-top:3px;"  qtip="נעילת תקופת דיווח">'+'</span></td>',
					// '<td valign=top style="width:20%; padding:3px;">תקופה קודמת<br><span style="font-weight:normal;font-size:11px;">'+current_vat_period.add(Date.MONTH, -vat_period).format('d/m/Y')+'-'+current_vat_period.add(Date.DAY, -1).format('d/m/Y')+'</span></td>',
					'<td valign=top style="width:20%; padding:3px;">תקופה קודמת<br><span style="font-weight:normal;font-size:11px;">{prev_vat_period_str}</span></td>',
					'<td valign=top style="width:20%; padding:3px;">מתחילת שנה</td>',
					'<td valign=top style="width:20%; padding:3px;">אחוז</td>',
					'<td valign=top style="width:20%; padding:3px;">ממוצע חודשי</td></tr>',
				'<tr style="color:green; font-size:14px;">',
					'<td onmouseover="this.className=\'td_on_image\';" onmouseout="this.className=\'td_off_image\';" onClick="Ms.accounting.sendInvoicesWindow();" qtip="חשבוניות ממתינות למשלוח" style="cursor:hand; cursor:pointer;padding:3px;"><img src="modules/accounting/pict/money+.gif" align=absmiddle></br>הכנסות (חשבוניות למשלוח:{new_invoices})</td>',
					'<td onmouseover="this.className=\'td_on\';" onmouseout="this.className=\'td_off\';" onClick="Ms.accounting.invoiceRepWindow(\'curr\');" qtip="דוח תקופה נוכחית" style="cursor:hand; cursor:pointer;" dir=ltr>+ {incomeLast:ilMoney}</td>',
					'<td onmouseover="this.className=\'td_on\';" onmouseout="this.className=\'td_off\';" onClick="Ms.accounting.invoiceRepWindow(\'prev\');" qtip="דוח תקופה קודמת" style="cursor:hand; cursor:pointer;" dir=ltr>+ {incomePrev:ilMoney}</td>',
					'<td onmouseover="this.className=\'td_on\';" onmouseout="this.className=\'td_off\';" onClick="Ms.accounting.invoiceRepWindow(\'year\');" qtip="דוח מתחילת שנה" style="cursor:hand; cursor:pointer;" dir=ltr>+ {incomeYear:ilMoney}</td>',
					'<td>&nbsp;100%</td>',
					'<td dir=ltr>+ {incomeAverage:ilMoney}</td></tr>',
				'<tr style="color:#930060; font-size:14px; direction:ltr;">',
					'<td onmouseover="this.className=\'td_on_image\';" onmouseout="this.className=\'td_off_image\';" onClick="Ms.accounting.expenseWindow();" qtip="קליטת הוצאה חדשה" style="cursor:hand; cursor:pointer;padding:3px;"><img src="modules/accounting/pict/money-.gif" align=absmiddle></br>הוצאות</td>',
					'<td onmouseover="this.className=\'td_on\';" onmouseout="this.className=\'td_off\';" onClick="Ms.accounting.expensesRepWindow(\'curr\');" qtip="דוח תקופה נוכחית" style="cursor:hand; cursor:pointer;">- {expenseCurr:ilMoney}</td>',
					'<td onmouseover="this.className=\'td_on\';" onmouseout="this.className=\'td_off\';" onClick="Ms.accounting.expensesRepWindow(\'prev\');" qtip="דוח תקופה קודמת" style="cursor:hand; cursor:pointer;">- {expensePrev:ilMoney}</td>',
					'<td onmouseover="this.className=\'td_on\';" onmouseout="this.className=\'td_off\';" onClick="Ms.accounting.expensesRepWindow(\'year\');" qtip="דוח מתחילת שנה" style="cursor:hand; cursor:pointer;">- {expenseYear:ilMoney}</td>',
					'<td>- {expensePercent}</td>',
					'<td>- {expenseAver:ilMoney}</td>',
				'</tr>',
				'<tr style="font-size:14px; direction:ltr;">',
					'<td style="cursor:hand; cursor:pointer;padding:3px;" onmouseover="this.className=\'td_on_image\';" onmouseout="this.className=\'td_off_image\';" qtip="הצגת דוח רווח והפסד תקופתי" onClick ="Ms.accounting.profitLossWindow(\'curr\')";  style="padding:3px; color:#ffab00;"><img src="modules/accounting/pict/profit.gif" align=absmiddle></br>רווח/הפסד</br>(דו"ח)</td>',
					'<td style="color:#{profitLastColor};"> {profitLast}</td>',
					'<td style="color:#{profitPrevColor};"> {profitPrev}</td>',
					'<td style="color:#{profitYearColor};"> {profitYear}</td>',
					'<td style="color:#{profitPercentColor};"> {profitPercent}</td>',
					'<td style="color:#{profitAverageColor};"> {profitAverage}</td>',
				'</tr>',
				
				'</table>'
			)},
			{xtype:'panel', 	id:'CashPan', border:false, title:'קופה',
			 tpl: new Ext.Template(
				'<table class="t_accounting" cellspacing=0 border=0>',
				'<tr>',
					'<td nowrap onClick="Ms.accounting.cashWindow();" qtip="כספים שנתקבלו ושיתקבלו בעתיד" rowspan=2 style="cursor:hand; cursor:pointer; width:120px; padding:3px;"><img src="modules/accounting/pict/wallet64.png"><br>תזרים</td>',
					'<td valign=top style="width:25%; padding:3px; border-bottom:0px">תקופה נוכחית</td>',
					'<td valign=top style="width:25%; padding:3px; border-bottom:0px">תקופה הבאה</td>',
					'<td valign=top style="width:25%; padding:3px; border-bottom:0px">תקופות עתידיות</td>',
					'<td valign=top style="width:25%; padding:3px; border-bottom:0px">סה"כ</td>',
				'</tr>',
				'<tr>',
					'<td>&nbsp;{currentCash:ilMoney}</td>',
					'<td>&nbsp;{nextCash:ilMoney}</td>',
					'<td>&nbsp;{futureCash:ilMoney}</td>',
					'<td>&nbsp;{totalCash:ilMoney}</td>',
				'</tr>',
						// '<tr><td onClick="Ms.accounting.chequesCashWindow();" align=center style="cursor:hand; cursor:pointer; padding:2px; font-weight:bold; width:40px; border-bottom: 1px solid gray;"><img src="skin/pict/cheque-32x32.png"><br>תנועות קופה</td><td style="padding:2px; font-weight:bold; border-bottom: 1px solid gray; width:130px; text-align:center;">&nbsp;{currentCheques:ilMoney}</td><td style="padding:2px; font-weight:bold; border-bottom: 1px solid gray; width:130px; text-align:center;">&nbsp;{nextCheques:ilMoney}</td><td style="padding:2px; font-weight:bold; border-bottom: 1px solid gray; width:130px; text-align:center;">&nbsp;{futureCheques:ilMoney}</td><td style="padding:2px; font-weight:bold; border-bottom: 1px solid gray; width:130px; text-align:center;">&nbsp;{totalCheques:ilMoney}</td><td style="border-bottom: 1px solid gray;">&nbsp;</td></tr>',
						// '<tr><td onClick="Ms.accounting.creditsCashWindow();" align=center style="cursor:hand; cursor:pointer; padding:2px; font-weight:bold; width:40px;"><img src="skin/pict/credit-card-32x32.png"><br>כ.אשראי דחויים</td><td style="padding:2px; font-weight:bold; width:130px; text-align:center;">&nbsp;{currentCredits:ilMoney}</td><td style="padding:2px; font-weight:bold; width:130px; text-align:center;">&nbsp;{nextCredits:ilMoney}</td><td style="padding:2px; font-weight:bold; width:130px; text-align:center;">&nbsp;{futureCredits:ilMoney}</td><td style="padding:2px; font-weight:bold;">&nbsp;{totalCredits:ilMoney}</td><td></td></tr>',
				'</table>'
			)}
			,{xtype:'panel',border:false, title:'פעולות', 
				html:['<table class="t_accounting" cellspacing=0 border=0><tr>',
					'<td onClick="Ms.accounting.vatWindow();" style="cursor:hand; cursor:pointer; padding:3px;"><img src="modules/accounting/pict/report64.png" align=absmiddle qtip="דוחות ותשלום מעמ מס הכנסה וביטוח לאומי. דוח ניכוי מס במקור"></br>דוחות תקופתיים</td>',
					'<td onClick="Ms.accounting.prevPeriodsWindow();" style="cursor:hand; cursor:pointer; padding:3px;"><img src="modules/accounting/pict/report64.png" align=absmiddle></br>קליטת תקופות קודמות</td>',
					'<td onClick="Ms.accounting.realWindow();" style="cursor:hand; cursor:pointer; padding:3px;"><img src="modules/accounting/pict/property64.png" align=absmiddle qtip="רשימת נכסים"></br>רשימת נכסים</td>',
					'</tr></table>'].join('')
			}
		]});
	
	this.load();
};
Ext.extend(AccountingPan, Ext.Panel);


Ms.accounting.run = function(cfg){
	if (vat_period==0) {
		Ext.Msg.alert('שגיאה!','חובה להגדיר תקופת דיווח בהגדרות פרטי העסק!');
		return;
	}

	this.invoiceWindow = function(){	
		var win=new InvoiceWindow({type:'invoice_accept', callBack:function(){accountingPan.load();}, d:{}}); 
		win.show();
	};
	
	this.expenseWindow = function(){	
		var win=new ExpenseWindow({callBack:function(){accountingPan.load();}, d:{}}); 
		win.show();	
};
this.prevPeriodsWindow = function(){
	var year = current_vat_period.getFullYear();
		var month= current_vat_period.getMonth();
	var win = Crm.desktop.getWindow('prevPeriodsWindow-win');
		if(!win){
			
			win = Crm.desktop.createWindow({height:300,width:300,id:'prevPeriodsWindow-win',  title:'קליטת תקופות קודמות',resizable:false, items:new prevPeriods_1({rep_period:vat_period, month:month, year:year })}, m);
		}
		win.show();
}

	
	this.printVAT = function(){
		window.open('?m=accounting/reports_f&f=vat&'+type+'=1&rep_period='+rep_period+'&month='+month+'&year='+year+'&sort='+SortState.field+'&dir='+SortState.direction,'','toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
	}
	
	this.closeVATPeriod = function(){
		if (new Date()<current_vat_period.add(Date.MONTH, vat_period)) {
			Ext.Msg.alert('שגיאה!','אין לסגור דוח! תקופת הדוח טרם הסתיימה!');
			return;
		}
		Ext.MessageBox.show({
			width:360, title:'אזהרה!', msg:'האם ברצונך לסגור תקופת מע"מ '+(current_vat_period.format('F'))+(vat_period==2?' - '+current_vat_period.add(Date.MONTH, 1).format('F'):'')+' '+(current_vat_period.format('Y'))+'?<br>אין קליטת הוצאות לתקופה סגורה', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) {
				if(btn=='yes') 	{Ext.Ajax.request({
					url: "?m=accounting/accounting&f=close_vat_period",
					success: function(r) {
						current_vat_period=current_vat_period.add(Date.MONTH, vat_period);
						accountingPan.load();
					},
					scope: this
				});
				}
			}
		});
	}
	
	this.depositWindow = function(){	
		var win=new DepositWindow({callBack:function(){accountingPan.load();}, d:{}}); 
		win.show();
	};
	
	this.invoiceRepWindow = function(period){
		var year = current_vat_period.getFullYear();
		var month= current_vat_period.getMonth();
		
		if (period=='curr') {
			from = (new Date(new Date(year, month,1))).format('d/m/Y');
			to   = (new Date(new Date(year, month+vat_period,0))).format('d/m/Y');
		} else if (period=='prev') {
			from = (new Date(new Date(year, month-vat_period,1))).format('d/m/Y');
			to   = (new Date(new Date(year, month,0))).format('d/m/Y');
		} else if (period=='year') {
			from = (new Date(new Date(year, 0,1))).format('d/m/Y');
			to   = (new Date(new Date(year, month+vat_period,0))).format('d/m/Y');
		} 
		
		var win = Crm.desktop.getWindow('invoiceRepWindow-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'invoiceRepWindow-win',  title:'רשימת חשבוניות', width:900, height:600, layout:'border',	items:new InvoicesRep(from,to)}, m);
		}
		win.show();
	};	
	
	this.expensesRepWindow = function(period){
		var year = current_vat_period.getFullYear();
		var month= current_vat_period.getMonth();
		
		if (period=='curr') {
			from = (new Date(new Date(year, month,1))).format('d/m/Y');
			to   = (new Date(new Date(year, month+vat_period,0))).format('d/m/Y');
		} else if (period=='prev') {
			from = (new Date(new Date(year, month-vat_period,1))).format('d/m/Y');
			to   = (new Date(new Date(year, month,0))).format('d/m/Y');
		} else if (period=='year') {
			from = (new Date(new Date(year, 0,1))).format('d/m/Y');
			to   = (new Date(new Date(year, month+vat_period,0))).format('d/m/Y');
		} 
		
		var win = Crm.desktop.getWindow('expensesRepWindow-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'expensesRepWindow-win',  title:'רשימת הוצאות', width:900, height:600, layout:'border',	items:new ExpensesRep(from,to)}, m);
		}
		win.show();
	};	
	
	
	
	this.vatWindow = function(period){
		var year = current_vat_period.getFullYear();
		var month= current_vat_period.getMonth();
		
		var win = Crm.desktop.getWindow('vatWindow-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'vatWindow-win',  title:'מע"מ', width:950, height:600, layout:'border', items:new VatRep({rep_period:vat_period, month:month, year:year })}, m);
		}
		win.show();
	};
	
	

   this.profitLossWindow = function (period){
		var year = current_vat_period.getFullYear();
		var month= current_vat_period.getMonth();
		
		from = (new Date(new Date(year, month,1))).format('d/m/Y');
		to   = (new Date(new Date(year, month+vat_period,0))).format('d/m/Y');
		
		var win = Crm.desktop.getWindow('profitLossWindowREP-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'profitLossWindowREP-win',  title:'דו"ח רווח והפסד תקופתי', width:950, height:600, layout:'border', items:new ProfitLoss_rep({rep_period:vat_period, month:month, year:year})}, m);
		}
		win.show();
	};
	
	
	
	this.realWindow = function(){
		var win = Crm.desktop.getWindow('realWindow-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'realWindow-win',  title:'רשימת נכסים', width:660, height:400, layout:'border', items:new RealRep({})}, m);
		}
		win.show();
	};
	
	this.cashWindow = function(period){
		var win = Crm.desktop.getWindow('cashWindow-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'cashWindow-win',  title:'תזרים', width:1180, height:700, layout:'border', items:new CashRep()}, m);
			// win = Crm.desktop.createWindow({id:'cashWindow-win',  title:'רשימת שיקים דחויים', width:900, height:600, layout:'border', items:new ChequesCashRep()}, m);
		}
		win.show();
	};
	
	this.creditsCashWindow = function(period){
		var win = Crm.desktop.getWindow('creditsCashWindow-win');
		if(!win){
			win = Crm.desktop.createWindow({id:'creditsCashWindow-win',  title:'רשימת תשלומי אשראי', width:900, height:600, layout:'border', items:new CreditsCashRep()}, m);
		}
		win.show();
	};
	
	this.sendInvoicesWindow = function(period){
		var win = Crm.desktop.getWindow('sendInvoicesWindow-win');
		
		if(!win){
			var sendInvoicesWindow = new SendInvoicesWindow();
			win = Crm.desktop.createWindow({id:'sendInvoicesWindow-win',  title:'חשבוניות ממתינות למשלוח', width:700, height:400, layout:'border',	
				items:sendInvoicesWindow
				,buttons:[
					{xtype:'button', cls:'btnRight', text:'שלח', iconCls:'other_police', handler:sendInvoicesWindow.send, scope:sendInvoicesWindow, width: 90}
				]
			}, m);
		}
		win.show();
	};
	
	var m=Ms.accounting;
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var accountingPan = new AccountingPan();
		win = Crm.desktop.createWindow({width:850, height:660, resizeable: false,  layout:'fit',	items:accountingPan}, m);
	}
	win.show();
};