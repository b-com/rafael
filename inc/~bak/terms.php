<div class=tl><div class=tr style='padding:10px; height:35px; width:100%;'></div></div>
<div class=p style='padding:10px; width:100%; border-left: solid 1px #CACDE0; border-right: solid 1px #CACDE0;'>
<font SIZE="3"><b>
<p ALIGN="CENTER">���� ����� ������</p>
</b></font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
<p ALIGN="RIGHT">&nbsp;</p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">( ����: <b>&quot;������&quot;</b></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
) </font><font SIZE="2">����� ����� ������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">������ ������ ��������� ������� ��</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">������ �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">�� ������ ��� ������ �������� ������ :</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">������</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">&quot;�����&quot;</font></b><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
� </font><font SIZE="2">���, ����� ����� �����, ��� ���� ������ ������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">�/�� ����� ����� ������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">&quot;���� ������&quot;</b></font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">� �����, ������, �������, ������, ������, �������, ������, 
������ (����� ���� �������� ������� </font><font FACE="Arial" SIZE="2">SMS</font><font SIZE="2"> 
), �������� ( ����� ������� ��������� �� ����� �������</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
) </font><font SIZE="2">, ��� �� ���, ����� �/�� ���� ����� �/�� ������ ����� 
����� ������ ������.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">&quot;���� �����&quot;</b> � �� ���� ����� �� ��� ������ �/�� �� �����, 
����� ����� �������.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">&quot;���� �����&quot;</b> � ������� ���� ������ �� ������� �������� 
(�����), ���&quot;�-1981</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">����</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">��-���&nbsp; ������� ����� ��&quot;� </font>
<font FACE="Times New Roman (Hebrew)" SIZE="2">( </font><font SIZE="2">����: <b>
&quot;�����&quot;</b></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </font>
<font SIZE="2">) ���� ������ ��������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">�� ������ ��� ����� ����� ����� ��������� �� ������ 
���������� ���� ����� ����� ������ ����� �����, ��� �����, ���� ���.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">������</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font></b><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ������ ������� �������� �� ������ ������ ���� ��� ( </font>
<font FACE="Arial" SIZE="2">as is</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font FACE="Arial" SIZE="2">(</font><font SIZE="2"> ������� �� ��� �� 
�����, ����� �� ���� ���� ������ ������ �/�� �������� �� �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ���� �� ������ ������ ������ ����� �/�� ������� �/�� 
������ �/�� �� ������. ����� �� ���� ������� ���� �� ��� ��� ���� ������ ������ 
�/�� ������� �/�� ������ �/�� �� ������ �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">����� ���� ������ ������ ������ ���� ����� ( ����: </font><b>
<font FACE="Times New Roman (Hebrew)" SIZE="2">&quot;</font><font SIZE="2">����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">&quot;</font></b><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
), </font><font SIZE="2">��� �� �� ����� �� ����� �� ����� ���, ��� �� ��� 
������ ���� �� ���� ������ ������ ������� ����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">�/�� ��� ��� ���� ���� �� ���� ������� ����� ���� ������� 
������</font><font FACE="Arial" SIZE="2"> </font><font SIZE="2">�� ����� ����� 
������ ���</font><font FACE="Times New Roman (Hebrew)" SIZE="2" COLOR="#2e2e2e"> 
.</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </p>
</font><font SIZE="2">
<p ALIGN="RIGHT">�����, ���� ������ �� ��� ������ ����� ���� ����� ���� ����� 
���� �� ������.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ������ ���� ���� ������ ���� ������ �/�� ���� ������� �/�� 
���� ������ ���, ���� ����� ������ ���� ���� �/�� ��� �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">,</font><font SIZE="2"> 
���� ���� �� ����� ������ �� ��� ����� �����.</font><font FACE="Arial" SIZE="2"><br>
</font><font SIZE="2">����� �� ���� ��� ������ ���� ������ �/�� ������ �� ������ 
������� �������� �� ���� ������.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">������ �����</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font></b><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ����� ��� ����� �� ����� ����� ����� ���� ����� �� 
������ ������ ��� ������ ������ ������</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
(</font><font FACE="Arial" SIZE="2">username </font>
<font FACE="Times New Roman (Hebrew)" SIZE="2">+</font><font FACE="Arial" SIZE="2"> 
password</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </font>
<font FACE="Arial" SIZE="2">(</font><font SIZE="2"> ��� �� ��� ������ ���� ����� 
������� �� ���� ������ ������ �� ������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ����� ��� ����� �� ���� ������ ������ ���� �� ��� �/�� 
���� /��� ���� ���� ����� ����� ������ ��� ���� �/�� ����� �/�� ����� ����� 
����� ����� ���� ����� �/�� �� ����� ���� ��� ������ ������ ����� ����.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ����� ��� ����� �� �� ����� �� ��� ����� ������� �� ���� 
������� ����� �� ����� �/�� �� ����� ������ ���� ����� ������� ����� ����, ���� 
������ �� ����� ���� �� ��� ������ �� �/�� ��� ���� ������ ����. </p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><u><font SIZE="2">
<p ALIGN="RIGHT">����� �������</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font></u><font FACE="Times New Roman (Hebrew)" SIZE="2">:</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">����� ����� ������ ����� ������ ������ �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">��������</font><font SIZE="2" COLOR="#323232"> ��� ����� 
����� �� ����� ������ �� ������ ��� ���</font><font FACE="Times New Roman (Hebrew)" SIZE="2" COLOR="#323232">.
</font><font SIZE="2" COLOR="#323232">����� ���</font><font FACE="Times New Roman (Hebrew)" SIZE="2" COLOR="#323232">,
</font><font SIZE="2">���� ����� �� ����� ������ ������� �� �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.
</font><font SIZE="2">�� ��, ����� �� ��� ������ ���� ������ ������ ����� ����� 
������ �/�� ���� ����� ����� ����� ����� ����� �� ����� ����� ������� �����.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ����� ��� ����� �� �� ���� ����� ������� ������ ���� 
������ ������.</p>
<p ALIGN="RIGHT">������ �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">��� ������ ���� ����� ����� ����� �������, ����� ����� 
����� ������ ���������, ����� ���� ������ ���� 
, ��� ����� ������� ��� ���� �������, ����&quot;� - 1981.</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</p>
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT"><font style='color:red;'><b>���� ����� ����� ����, ����� ������� ����� ������� ���� �� �� ���� ������ ����� ��� ���� �/�� ���� ���� ���� ����� ��� ������ ����� ����� ��� ��� ���� ��� �� ��� ���� ���� ����� ����� ������� ����� �/�� ������ �������� ����� �/�� ����.</b></font></p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">������ ����� ��� ������ ��� ����� ����� �� ����� ������ �� ��� 
������ ����� ���</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</font><font SIZE="2"><br>
��� ������ ���� ������� ����� ����� ����� ����� �� ���, ����� ������ ����� �� �� 
������</font><font FACE="Times New Roman (Hebrew)" SIZE="2"> </font>
<font SIZE="2">���� �� ����� ����� ����� ����� ���� ���� ������ �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">������ ������ �����</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
</font></b><font FACE="Times New Roman">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">���� ��� �� ��� ����� ���� �� ������ ������� ���� ������� 
��������� ������ �������, ������ �� ��� �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.</p>
</font><font SIZE="2">
<p ALIGN="RIGHT">����� ������ �� ������ �/�� �� ����� �� ���� ���� ����� �/�� 
����� �/�� ������ �/�� ����� �/�� ������ ����� �/�� ����� ����� ������ �/�� 
������ ����� ��� ���� �� ���� �� ��� ���� ������ ������ ��, ����� �� ������, ��� 
������ ���� ��� �����, ��� ���� ������ �� ����� ���� �����.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font>
<font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">�����</u></font><font FACE="Times New Roman (Hebrew)" SIZE="2"> 
:</p>
<p ALIGN="RIGHT"></p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">������ ���� �� ����� �/�� �� ����� ���� ���, �/�� ���� �/�� 
���� ���� �/�� ����� �� ����� ������� ���, ����� ������ ���� ���&quot;� ���� ���,</font><font SIZE="2" COLOR="#323232"> 
��� ���� ���� ����� ��� �� ��� ������ �/�� �� ��� �� �����. </font>
<font FACE="Times New Roman (Hebrew)" SIZE="2"></p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><b><font SIZE="2"><u>
<p ALIGN="RIGHT">����� </u></font>
<font FACE="Times New Roman (Hebrew)" SIZE="2">:</p>
</font></b><font SIZE="2">
<p ALIGN="RIGHT">���� �� ����� ����� ��� ����� ����� ����, �� ���� ������ �� 
�����.</p>
<p ALIGN="RIGHT">������� �/�� ������� ����� ����� ��� ������ ��� ������� ���� 
���� ����� ��� ������ ������ �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">.
</font><font FACE="Arial" SIZE="2"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">�� ���� �� �����</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">���� ����� �����. ������ ����� �� ��� ������ ������</font><font FACE="Arial" SIZE="2">
</font><font SIZE="2">������� ������ �� ������ �� ���� ����� ������ ������� �� 
��� �����</font><font FACE="Arial" SIZE="2"> </font><font SIZE="2">����� ���� 
����.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">����� ������ �� �� ���� ����� ������, ����� ��� ������,</font><font FACE="Times New Roman (Hebrew)" SIZE="2">
</font><font SIZE="2">���� ���� ����� ������ ������. ����� ����� ������ ������ 
�/�� ���� �������� ���� ��� �� ������ ����� ��� ����� ���� ������ ������, ������� 
����� �� ����.</p>
</font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2"><b>
<p ALIGN="RIGHT">����� ����� ����� �� ����� ����� �� ���� ������ ������ ��� ���, 
��� ��� ��� ����� ���� ��� ������ ����� ���� ������ �������� ���� ������ �����.</p>
</b></font><font FACE="Arial" SIZE="2">
<p ALIGN="RIGHT"></p>
</font><font SIZE="2">
<p ALIGN="RIGHT">���� ������ ������ ��� �� ������ ��� ������ �� ���� ������ ������, 
���� �� ������� ������ ����� ��� ���.</p>
</font>
</div><div style='background-image:url(skin/portal/bg1.png); background-repeat:no-repeat; zoom:1; background-position:right bottom; height:8px; padding-right:10px;'>
<div style='background-image:url(skin/portal/bg1.png); background-repeat:no-repeat; zoom:1; background-position:left bottom; height:8px;'></div></div>