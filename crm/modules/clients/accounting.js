ClientAccount= function(d) {
	this.client_id=d.client_id;

	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=client_account&client_id='+d.client_id+'&client_type='+d.client_type,
		totalProperty: 'total',
		delCount: 'deleted',
		root: 'data',
		fields: ['id',{name:'date', type:'date', dateFormat:'d/m/Y'},'type','num','debt','credit','balance','user_id','notes'],
		sortInfo: {field:'date', direction:'ASC'}
	});
	
	this.store.on('load', function(store, options){
		var Rec = this.store.recordType;
		var p = new Rec(this.store.reader.jsonData.summary);
		this.store.insert(this.store.getCount(), p);
	}, this);
	
	this.invoiceWindow = function(t,d){
		if (d.json && d.json.invoice) {Ext.Msg.alert('הזהרה!', 'חשבונית כבר הופקה!'); return;}
		// var win=new InvoiceWindow(this,t,(d.data ? {doc_type:d.data.type, doc_id:d.data.id, notes:"חשבונית מס "+(d.data.type=='invoice_accept'?'קבלב':'')+" מספר "+d.data.num, client_id:this.client_id}:{})); win.show();
		var cfg = (d.data ? {doc_type:d.data.type, doc_id:d.data.id, notes:"חשבונית מס "+(d.data.type=='invoice_accept'?'קבלב':'')+" מספר "+d.data.num, client_id:this.client_id}:{});
		cfg.type = t;
		cfg.callBack = function(){this.store.reload();}
		var win=new InvoiceWindow(cfg); 
		win.show();
	}

	
	this.printDoc = function(d){
		var win=window.open('http://'+location.host+'/crm/?m=common/print_doc&type='+d.type+'&id='+d.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	};
	
	ClientAccount.superclass.constructor.call(this, {
	    title:'ניהול חשבון', store:this.store, stripeRows:true, autoExpandColumn:'notes', hidden:d.hidden, cls:'prod-rep', 
	    columns: [
			{header:'משתמש', width:110, dataIndex:'user_id', renderer:userRenderer}
			,{header:'הערות',id:'notes', dataIndex:'notes'}
			,{header:'יתרה', dataIndex:'balance', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'זכות', dataIndex:'debt', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'חובה', dataIndex:'credit', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'אסמכתא', dataIndex:'num'}
			,{header:'סוג פעולה', dataIndex:'type', renderer:function(v){return doc_types[v]}}
			,{header:'תאריך', dataIndex:'date', width:95, css:'text-align:right; direction:ltr;', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
	    ]
		,tbar: [ 
			{text:'מסמך חדש', iconCls:'police_add', 
				menu:{defaults:{scope:this},
				items: [
					{text:'חשבונית מס', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_credit',{});}},
					{text:'חשבונית זיכוי', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_debt',{});}},
					{text:'קבלה', iconCls:'police_add', handler:function(){this.invoiceWindow('accept',{});}},
					{text:'<font color=red>קבלה זמנית</font>', iconCls:'police_add', handler:function(){this.invoiceWindow('accept_tmp',{});}},
					{text:'חשבונית מס - קבלה', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_accept',{});}}
					], scope:this}
			},{text:'הדפס', iconCls:'print', id:'prind_doc', disabled:true, scope:this, handler:function(){this.printDoc(this.getSelectionModel().getSelected().data);}}
		]
		,bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})

		,initComponent: function() {ClientAccount.superclass.initComponent.call(this);}
	});
	
	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('prind_doc').setDisabled(sm.getCount()!=1);
	}, this);
	
	this.on('rowcontextmenu', function(grid, row, e){
		var r=this.store.getAt(row);
		if (r.data.type!='invoice_credit' && r.data.type!='invoice_accept') {e.stopEvent(); return;}
		this.menu = new Ext.menu.Menu({
			items: {text:'הפק חשבונית זיכוי', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_debt',r);}, scope:this}
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);

};
Ext.extend(ClientAccount, Ext.grid.GridPanel);

ClientOrders= function(d) {
	this.client_id=d.client_id;

	this.store = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=client_orders&client_id='+d.client_id,
		totalProperty: 'total',	delCount: 'deleted', root: 'data',
		fields: ['id',{name:'date', type:'date', dateFormat:'d/m/Y'},'name','num','status','sum','user_id','notes'],
		sortInfo: {field:'id', direction:'ASC'}
	});
	
	this.orderWindow	= function(d){var win=new OrderWindow(this,d); win.show();}
	
	this.invoiceWindow = function(t,d){
		if (d.json && d.json.invoice) {Ext.Msg.alert('הזהרה!', 'חשבונית כבר הופקה!'); return;}
		var win=new InvoiceWindow(this,t,{doc_type:'order',doc_id:d.data.id, notes:"הזמנה מס' "+d.data.num, client_id:this.client_id});	win.show();
	}

	this.printDoc = function(d){
		var win=window.open('http://'+location.host+'/crm/?m=accounting/print_doc&type=order&id='+d.id, 'scan', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
	};
	
	ClientOrders.superclass.constructor.call(this, {
	    title:'הזמנות', store:this.store, stripeRows:true, autoExpandColumn:'notes',
	    columns: [
			{header:'משתמש', width:110, dataIndex:'user_id', renderer:userRenderer}
			,{header:'הערות',id:'notes', dataIndex:'notes'}
			,{header:'סכום', dataIndex:'sum', css:'direction:ltr; text-align:right;', renderer:Ext.util.Format.ilMoney}
			,{header:'סטאטוס', dataIndex:'status', width:70, renderer:function(v){return (v!=2 ? 'טרם בוצעה':'בוצעה')}}
			,{header:"כותרת", dataIndex:'name', width:180}
			,{header:"מס' הזמנה", dataIndex:'num'}
			,{header:'תאריך', dataIndex:'date', width:95, css:'text-align:right; direction:ltr;', renderer:Ext.util.Format.dateRenderer('d/m/Y') }
	    ]
		,tbar:[ 
			{text:'הזמנה חדשה', iconCls:'police_add', handler:function(){this.orderWindow({});}, scope:this}
			,'-',{text:'עדכן הזמנה', id:'order_edit', iconCls:'edit', handler:function(){this.orderWindow(this.getSelectionModel().getSelected());}, disabled:true, scope:this }
			,'-',{text:'הדפס', iconCls:'print', id:'order_print', disabled:true, scope:this, handler:function(){this.printDoc(this.getSelectionModel().getSelected().data);}}
			,{text:'הפק חשבונית', iconCls:'police_add', disabled:true, id:'invoice_add',
				menu:{defaults:{scope:this},
				items: [
					{text:'חשבונית מס', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_credit',this.getSelectionModel().getSelected());}},
					{text:'חשבונית מס - קבלה', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_accept',this.getSelectionModel().getSelected());}}
					], scope:this}
			}
		]
		,bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})
		,initComponent: function() {ClientOrders.superclass.initComponent.call(this);}
	});
	
	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('order_edit').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('order_print').setDisabled(sm.getCount()!=1);
		this.getTopToolbar().items.get('invoice_add').setDisabled(sm.getCount()!=1);
	}, this);
	
	this.on('rowcontextmenu', function(grid, row, e){
		var r=this.store.getAt(row);
		this.menu = new Ext.menu.Menu({
			items: [
				{text:'הפק חשבונית מס', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_credit',r);}, scope:this},
				{text:'הפק חשבונית מס - קבלה', iconCls:'police_add', handler:function(){this.invoiceWindow('invoice_accept',r);}, scope:this}
			]
		});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	},this);

};
Ext.extend(ClientOrders, Ext.grid.GridPanel);