var acc_types = [['','כל החשבונות'],['deleted','חשבונות מבוטלים']];

AccountWindow = function(grid, d) {
	if (d) this.product_id=d.product_id;
	this.grid=grid;
	
	this.branchs = new Ext.data.JsonStore({
		url:'?m=common/accounting&f=branchs_list',
		totalProperty: 'total',
		root: 'data',
		fields: ['branch_code','branch_name','tel','fax'],
		sortInfo: {field:'branch_name', direction:'ASC'},
		remoteSort: true
	});

	this.branchs.on('beforeload', function(store, options){
			this.branchs.removeAll();
			var setParams = {bank: Ext.getCmp('bankCombo').getValue()};
			Ext.apply(store.baseParams, setParams);
			Ext.apply(options.params, setParams);
		}, this);

	this.form = new Ext.form.FormPanel({
		id:'accForm', labelWidth:58, autoHeight:true, baseCls: 'x-plain',
		items:[{
			layout:'table', layoutConfig:{columns:7, labelWidth:80}, baseCls:'x-plain', cls:'rtl', defaults: {layout:'form', baseCls: 'x-plain', labelStyle:'padding-right:15px'},
			items:[
				{xtype:'label', text:'בנק:'},{xtype:'combo', store:banks, hiddenName:'bank', id:'bankCombo', emptyText:'- בחר -', width:140, colspan:2, triggerAction:'all', forceSelection:true, allowBlank:false, value:(d ? d.bank:''), 
					listeners:{select:function(){this.branchs.reload(); Ext.getCmp('branchCombo').setValue(); Ext.getCmp('branchCombo').enable();}, scope:this}
				},
				{xtype:'label', text:'סניף:'},{
					xtype:'combo', allowBlank:false,
					store:this.branchs, 
					triggerAction:'all',
					emptyText:'- בחר -', hiddenName:'branch', id:'branchCombo', width:260, colspan:3, disabled:!(d && d.bank), 
					valueField:'branch_code',
					displayField:'branch_name'
					, listeners:{select:function(e,r,i){Ext.getCmp('branch_tel').setValue(r.data.tel);Ext.getCmp('branch_fax').setValue(r.data.fax);}, scope:this}

				},
				{xtype:'label', text:'סוג חשבון:', style:'display:block; width:58px'},{xtype:'combo', store:[[1,'עו"ש'],[2,'מט"ח']], hiddenName:'type', width:140, colspan:2, listClass:'rtl', triggerAction:'all', forceSelection:true, value:(d ? d.type:''), allowBlank:false},
				{xtype:'label', text:'מספר:'},{xtype:'textfield', name:'number', width:260, colspan:3, allowBlank:false, value:(d ? d.number:'')},
				{xtype:'label', text:'איש קשר:'},{xtype:'textfield', name:'contact_person', width:140, colspan:2, value:(d ? d.contact_person:'')},
				{xtype:'label', text:'טלפון:'},{xtype:'textfield', name:'tel', id:'branch_tel', width:100, value:(d ? d.tel:'')},
				{xtype:'label', text:'פקס:'},{xtype:'textfield', name:'fax', id:'branch_fax', width:'100%', value:(d ? d.fax:'')},
				{xtype:'label', text:"מס' מפתח חשבשבת:", colspan:2},{xtype:'textfield', name:'acc_card', width:'100%', value:(d ? d.acc_card:'')}
			]
		},
		{labelText:'הערות', fieldLabel:'הערות', height:36, name:'notes', xtype:'textarea', cls:'rtl',  width:'100%', value:(d ? d.notes:'')},
		{xtype:'hidden', name:'id', value:(d ? d.id:'')}
		
		]
	});
	
	if (d && d.bank) {this.branchs.reload({params: { bank: d.bank }}); Ext.getCmp('branchCombo').setValue(d.branch); Ext.getCmp('branchCombo').setRawValue(d.branch_name);}
	
	AccountWindow.superclass.constructor.call(this, {
		title:(d ? 'עידכון חשבון':'הוספת חשבון חדש'),
		layout:'fit', modal:true,
		width:530, closeAction:'close', buttonAlign:'center', bodyStyle:'padding:5px;', plain:true,
		items:this.form,
		buttons:[{text:((d && d.id) ? 'עדכן':'הוסף'), handler:function(){this.submitForm();}, scope:this},{text:'סגור', handler:function(){this.close();}, scope:this}]
	});
	
	this.submitForm = function(overwrite) {
		if(!this.form.form.isValid()) return;
		
		this.form.form.submit({
			url: "?m=config/config&f=add_account",
			waitTitle:'אנא המתן...',
			waitMsg:'טוען...',
			scope:this,
			success: function(f,a){this.close(); grid.store.reload();},
			failure: function(r,o){ this.close(); ajRqErrors(o.result);}
		});
	}
};
Ext.extend(AccountWindow, Ext.Window, {});

AccountsGrid = function() {
	this.store = new Ext.data.JsonStore({
		url:'?m=config/config&f=accounts_list',
		totalProperty: 'total',
		root: 'data',
		fields: ['id','bank','branch','bank_name','branch_name','number','type','acc_card','contact_person','tel','fax','balance','notes'],
		sortInfo: {field:'name', direction:'ASC'},
		remoteSort: true
	});

	this.accountWindow = function(d){var win=new AccountWindow(this,d);	win.show();	win.center();}

	this.searchField = new GridSearchField({store:this.store, width:100});
	
	this.hlStr = hlStr;
	
	// this.contactRenderer = function (str, p, r) {return str+(r.data.tel ? ' טל:'+r.data.tel:'')+(r.data.fax ? ' פקס:'+r.data.fax:'');}
	this.contactRenderer = function(val,p,r){p.attr='ext:qtip="'+Ext.util.Format.htmlEncode((r.data.tel ? 'טל:  '+r.data.tel:'')+((r.data.tel && r.data.fax) ? '<br>':'')+(r.data.fax ? 'פקס: '+r.data.fax:''))+'</div>" ext:qtitle="'+Ext.util.Format.htmlEncode('שם: '+val)+'"'; return Ext.util.Format.stripTags(val); }

	this.typeRenderer = function (str) {var acc_types = {1:'עו"ש',2:'מט"ח'}; return acc_types[str];}

	AccountsGrid.superclass.constructor.call(this, {
		cls:'rtl', enableColLock:false, loadMask:true,	stripeRows:true, enableHdMenu:false, autoExpandColumn:'notes',
		store:this.store,
		columns: [
			{dataIndex:'bank_name', header:'בנק',		width:110, sortable:true, renderer:this.hlStr.createDelegate(this)},
			{dataIndex:'branch_name', header:'סניף',	width:100, sortable:true, renderer:this.hlStr.createDelegate(this)},
			{dataIndex:'number', header:'מספר חשבון',	width:80, sortable:true, renderer:this.hlStr.createDelegate(this)},
			{dataIndex:'type', header:"סוג חשבון", width:60, renderer:this.typeRenderer},
			{dataIndex:'contact_person', header:'איש קשר', width:100, renderer:this.contactRenderer}, 
			{dataIndex:'balance', header:'יתרה', width:80, renderer:Ext.util.Format.ilMoney}, 
			{dataIndex:'notes', id:'notes', header:'הערות', renderer:this.hlStr.createDelegate(this)} 
		],
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		tbar: [
			'-' , 'חיפוש:', this.searchField
			,'סוג חשבון:', new Ext.form.ComboBox({store:acc_types, id:'account_type', hiddenName:'type', value:'', width:100, listClass:'rtl', triggerAction:'all', forceSelection:true, 
				listeners:{
					select:function(f){
						groupID=Ext.getCmp('account_type').getValue();
						if (groupID=='deleted') {
							this.getTopToolbar().items.get('del_account').hide();
							this.getTopToolbar().items.get('rest_account').show();
						} else {
							this.getTopToolbar().items.get('del_account').show();
							this.getTopToolbar().items.get('rest_account').hide();
						}
						this.store.load();
						}, 
					scope:this
				}
			})
			, '-' ,{ text:'חשבון חדש', iconCls:'add', handler:function(){this.accountWindow()}, scope:this }
			, '-' ,{ text:'עדכן', id:'edit_account', iconCls:'edit', handler:function(){this.accountWindow(this.getSelectionModel().getSelected().data)}, scope:this }
			, '-' ,{ text:'מחק',  id:'del_account', iconCls:'delete', handler:function(){this.delAccount();}, disabled:true, scope:this }
				  ,{ text:'שחזר', id:'rest_account', iconCls:'restore', handler:function(){this.restoreAccount();}, disabled:true, hidden:true, scope:this }
		],
		bbar: new Ext.PagingToolbar({store:this.store,pageSize:50})
    });
	
	this.store.on('beforeload', function(store, options){
			this.store.removeAll();
			var setParams = {
				q: this.searchField.getRawValue().trim(),
				type: Ext.getCmp('account_type').getValue()
			}
			Ext.apply(store.baseParams, setParams);
			Ext.apply(options.params, setParams);
		}, this);

	this.store.on('load', function(){	// check for errors
			var callback=function() { this.store.load(); };
			ajRqErrors(this.store.reader.jsonData, callback.createDelegate(this, []) );
		}, this);
	this.store.load({ params:{start:0, limit:50} });

	this.getSelectionModel().on('selectionchange', function(sm, row, r) {
		this.getTopToolbar().items.get('edit_account').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('del_account').setDisabled(!sm.getCount());
		this.getTopToolbar().items.get('rest_account').setDisabled(!sm.getCount());
	}, this);
	
	this.on('rowcontextmenu', function(grid, row, e){
		this.sRow=row;
		this.srData=this.store.getAt(row).data;
		
		this.delRow=row;
			this.menu = new Ext.menu.Menu({
				items: [
					{text:'עדכן חשבון', iconCls:'edit', scope:this, handler:function(){this.accountWindow(this.srData);} },
					((Ext.getCmp('account_type').getValue()=='deleted') ? {text:'שחזר חשבון', iconCls:'restore', scope:this, handler:this.restoreAccount} : {text:'מחק חשבון', iconCls:'delete', scope:this, handler:this.delAccount} )
				]
			});
		e.stopEvent();
		this.menu.showAt(e.getXY());
	});

	this.delAccount = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		var records = this.getSelectionModel().getSelections();
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'למחוק חשבו'+(records.length==1?'ן':'ות')+'?', msg:'האם ברצונך למחוק את החשבו'+(records.length==1?'ן':'ות המסומנות')+'?', buttons:Ext.MessageBox.YESNO, scope:this,

			fn: function(btn) {if(btn=='yes') {
					var ids=[];
					Ext.each(records, function(r){ids.push(r.data.id);});
					this.getEl().mask('...טוען');
					Ext.Ajax.request({
						url: '?m=config/config&f=del_account&ids='+ids.join(','),
						success: function(r,o){
							d=Ext.decode(r.responseText);
							if (!ajRqErrors(d)) {
								Ext.each(records, function(d){
									row=this.store.indexOf(d);
									this.selModel.selectRow(row);
									this.view.getRow(row).style.border='1px solid red';
									Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
								},this);
							}
							this.getEl().unmask();
						},
						failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
						scope: this
					});
				}
			}
		});
	}
	
	this.restoreAccount = function(){
		if (this.sRow!=null) this.selModel.selectRow(this.sRow); // content menu
		if (!this.getSelectionModel().getCount()) return;
		var records = this.getSelectionModel().getSelections();
		Ext.MessageBox.show({
			width:300, icon:Ext.MessageBox.QUESTION, title:'לשחזר חשבו'+(records.length==1?'ן':'ות')+'?', msg:'האם ברצונך לשחזר את החשבו'+(records.length==1?'ן':'ות המסומנות')+'?', buttons:Ext.MessageBox.YESNO, scope:this,
			fn: function(btn) { if(btn=='yes') {
				var ids=[];
				Ext.each(records, function(r){ids.push(r.data.id);});
				this.getEl().mask('...טוען');
				Ext.Ajax.request({
					url: '?m=config/config&f=rest_account&ids='+ids.join(','),
					success: function(r,o){
						d=Ext.decode(r.responseText);
						if (!ajRqErrors(d)) {
							Ext.each(records, function(d){
								row=this.store.indexOf(d);
								this.selModel.selectRow(row);
								this.view.getRow(row).style.border='1px solid green';
								Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
							},this);
						}
						this.getEl().unmask();
					},
					failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed.'); },
					scope: this
				});
			}}
		});
	}
};

Ext.extend(AccountsGrid, Ext.grid.GridPanel);

Ms['config/accounts'].run = function(cfg){
	var m=Ms['config/accounts'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var  accountsGrid = new AccountsGrid();
		win = Crm.desktop.createWindow({iconCls:'account', width:600, height:300, items:accountsGrid}, m);
	}
	win.show();
};