prevPeriodsType2 = function(cfg) {

	this.GroupCombo = function(prnt){
		return new Ext.form.ComboBox({
			store: new Ext.data.Store({
				proxy: new Ext.data.HttpProxy({	
					
					url:'?m=config/config&f=expenses_group_list'
				}),
				reader:new Ext.data.JsonReader({
					root: 'data', 
					totalProperty:'total'
				},['id','code','name'])
			}),
			onTriggerClick:function(el){
				var win;
				if (Ext.getCmp('type').getRawValue()=='הוצאות'){
					var expensesgroupsGrid = new ExpensesgroupsGrid({},{
						height:363, 
						layout:'fit', 
						rec:prnt.grid.getSelectionModel().getSelected()
						});
					expensesgroupsGrid.on('beforeedit', function(e){
						return (e.record.data.id ? false : true)
					});
					expensesgroupsGrid.on('celldblclick', function(g,r,c){
						var r = g.store.getAt(r).data;
						this.rec.set('group_id',r.id);
						this.rec.set('group',r.name);
						this.rec.set('VAT',parseFloat(r.VAT));
						win.close();
					});
					win = new Ext.Window ({
						title:'קבוצות הוצאות', 
						width:300, 
						height:400, 
						items:expensesgroupsGrid
					});
					win.show();
				}else {
					var incomegroupsGrid = new IncomesgroupsGrid({},{
						height:363, 
						layout:'fit', 
						rec:prnt.grid.getSelectionModel().getSelected()
						});
					incomegroupsGrid.on('beforeedit', function(e){
						return (e.record.data.id ? false : true)
					});
					incomegroupsGrid.on('rowdblclick', function(g,r,c){
						var r = g.store.getAt(r).data;
						this.rec.set('group_id',r.id);
						this.rec.set('group',r.name);
						this.rec.set('VAT',parseFloat(r.VAT));
						win.close();
					});
					win = new Ext.Window ({
						title:'קבוצת הכנסות', 
						width:300, 
						height:400, 
						items:incomegroupsGrid
					});
					win.show();
				}
				
			},
			listeners: {
				select: function(e,r){
					if (Ext.getCmp('type').getRawValue()=='הוצאות'){
						expenseGroup=r.json;
					}else{
						incomeGroup=r.json;
					}
				},
				render: function(){
					if (Ext.getCmp('type').getRawValue()=='הוצאות'){
						expenseGroup={}; 
						this.gridEditor.el.useDisplay=true;
					}
					
					
				},
				show: 	function(el){
					if (Ext.getCmp('type').getRawValue()=='הוצאות'){
						expenseGroup={};
						el.getEl().dom.parentNode.style.width='120px'; 
						el.getEl().dom.style.height='18px';
					}else{
						incomeGroup={};						
						el.getEl().dom.parentNode.style.width='120px';
						el.getEl().dom.style.height='18px';
					}
					
				}
			},
			width:170, 
			typeAhead:true, 
			loadingText:'...טוען', 
			editable:true,// hiddenName:'part_id', valueField:'name',
			//width:170, typeAhead:true, loadingText:'...טוען', editable:true, hiddenName:'expense_group', valueField:'id',
			displayField:'name', 
			minChars:2, 
			pageSize:8, 
			hideMode:'display', 
			triggerClass:'x-form-search-trigger',
			tpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding-right:5px; text-align:right">{name} <b>({acc_card})</b></div></tpl>'),
			itemSelector: 'div.search-item',
			tabTip:'AAAAAA'
			
			
		});
	}

	var Record = Ext.data.Record.create(['date_field','payType','group_name','group_name','sum','pay_status']);

	var IncExp = [[0,'הכנסות'],[1,'הוצאות'],[2,'ציוד ונכסים']]
	var vatProp = [[0,'הוצאה'],[1,'ציוד ונכסים']]

	this.prevPeriodsStore = new Ext.data.JsonStore({
		fields: ['date','ptype','group','sum','payStatus'],
		remoteSort: true
	});
	this.RealGridStore = new Ext.data.JsonStore({
		fields: ['date','ptype','group','sum','payStatus'],
		remoteSort: true
	});

	this.vatPropCombo = new Ext.form.ComboBox({
		store:vatProp,
		id:'vatPropCombo'
	})
	this.incExpCombo = new Ext.form.ComboBox({
		store:IncExp, 
		id:'IncExpCombo'
		,
		listeners: {
			render: function(){
				this.gridEditor.el.useDisplay=true;
			},
			show:	function(el){
				el.getEl().dom.parentNode.style.width='85px';
				el.getEl().dom.style.height='18px';
			},
			select: function(c,r,i){
				alert (i)
				if (i==0)
					Ext.getCmp('vatPropCombo').disable()
				else if (i==1)
					Ext.getCmp('vatPropCombo').enable()
			}
		}
	})

	this.type = new Ext.form.ComboBox({
		store: [[0,'הכנסות'],[1,'הוצאות'],[2,'ציוד ונכסים']], 
		hiddenName:'type', 
		valueField:'id', 
		displayField:'val', 
		width:70, 
		triggerAction:'all', 
		forceSelection:true, 
		allowBlank:true,
		blankText:'שדה זה הכרחי',
		value:0,
		id:'type'
		,
		listeners: {
			render: function(){
				this.gridEditor.el.useDisplay=true;
			},
			show:	function(el){
				el.getEl().dom.parentNode.style.width='85px';
				el.getEl().dom.style.height='18px';
			}
		}

	});
	
	this.paymentCombo = new Ext.form.ComboBox({
		store:[[0,'מזומן'],[1,'אשראי'],[2,'בנק'],[3,'לא שולם']], 
		hiddenName:'payment', 
		valueField:'id', 
		displayField:'val', 
		width:70, 
		triggerAction:'all', 
		forceSelection:true, 
		allowBlank:true,
		blankText:'שדה זה הכרחי',
		value:0
		,
		listeners: {
			render: function(){
				this.gridEditor.el.useDisplay=true;
			},
			show:	function(el){
				el.getEl().dom.parentNode.style.width='85px';
				el.getEl().dom.style.height='18px';
			}
		}

	});
	
	this.groupNamesCombo = new Ext.form.ComboBox({
		store:[], 
		hiddenName:'payment', 
		valueField:'id', 
		displayField:'val', 
		width:70, 
		triggerAction:'all', 
		forceSelection:true, 
		allowBlank:true,
		blankText:'שדה זה הכרחי',
		value:0
		,
		listeners: {
			render: function(){
				this.gridEditor.el.useDisplay=true;
			},
			show:	function(el){
				el.getEl().dom.parentNode.style.width='130px';
				el.getEl().dom.style.height='18px';
			}
		}

	});
	
	
	
	
	this.form = new Ext.form.FormPanel({
		autoHeight:true, 
		baseCls:'x-plain',// style:'padding:0 6px',
		items:{
			layout:'table', 
			layoutConfig: {
				columns:4
			}, 
			baseCls: 'x-plain', 
			cls:'rtl', 
			items: [

			

			this.grid = new Ext.grid.EditorGridPanel({ 
				store:this.prevPeriodsStore,
				height:160,
				width:'100%',
				cls:'rtl',
				id:'prevPeriodsGrid',
				autoScroll:true,
				enableColumnResize:false,
				//plugins: acceptGridSummary,
				selModel: new Ext.grid.RowSelectionModel({
					singleSelect:true
				}),
				autoExpandColumn:'Sum',
				clicksToEdit:1,
				border: false,
				columns: [
				{
					header:'תאריך',
					id:'date_field', 
					dataIndex:'date', 
					width:85, 
					editor:{
						xtype:'datefield',
						value:new Date()
						}, 
					renderer:Ext.util.Format.dateRenderer('d/m/Y'), 
					css:'direction:ltr;text-align:right;'
				},

				{
					header:'סוג',
					id:'payType', 
					dataIndex:'ptype',  
					width:85, 
					editor:this.type,
					renderer:function(v){
						var obj={
							0:'הכנסות',
							1:'הוצאות'
						};						
						return obj[v];
					}
				}
			,{
				header:'קבוצה',
				id:'group_name', 
				dataIndex:'group',  
				width:130, 
				editor:this.GroupCombo(this),
				scope:this
			},{
				header:'פרטים',
				editor:new textField,
				width:85,
				dataIndex:'details', 
				hidden:false
				
			}
			
			,{
				header:'סכום',
				id:'Sum', 
				dataIndex:'sum', 
				width:70, 
				editor:new numberField, 
				summaryType:'sum', 
				renderer:Ext.util.Format.ilMoney
				}
			,{
				header:'תשלום',
				id:'pay_status', 
				dataIndex:'payStatus',  
				width:85, 
				editor:this.paymentCombo,
				renderer:function(v){
					var obj={
						0:'מזומן',
						1:'אשראי',
						2:'בנק',
						3:'לא שולם'
					};					
					return obj[v];
				}
			},{
				dataIndex:'group_id', 
				hidden:true
			}
			,{
				dataIndex:'VAT', 
				hidden:true
			}
			
			
			
			]
		
		})
		
	]
	
	}
});

this.gridAfterEdit = function(e){
	//alert (this.prevPeriodsStore.getCount());
	if ((this.prevPeriodsStore.getCount()-e.row)==1) this.prevPeriodsStore.insert(this.prevPeriodsStore.getCount(), new Record()); 
	this.grid.plugins.refreshSummary();
}


	
this.grid.on('afteredit',this.gridAfterEdit,this);


	


this.grid.on('click', function(){
	//alert (this.prevPeriodsStore.getCount());
	if (this.prevPeriodsStore.getCount()==0) {
		this.prevPeriodsStore.insert(this.prevPeriodsStore.getCount(),new Record());
		this.grid.startEditing(0,0);
	}
},this);



prevPeriodsType2.superclass.constructor.call(this, 
{
	title:'קליטת תקופות קודמות',
	width:570, 
	height:410, 
	resizable:false,
	bodyStyle: 'background:transparent;', 
	border:true,
	items:[this.grid,this.form],
	buttons:[
	{
		text:'שמור', 
		disabled:(cfg.id), 
		handler:function(){
			this.submitForm();
		}, 
		scope:this
	},

	{
		text:'הדפס', 
		disabled:(!cfg.id), 
		handler:function(){
			var win=window.open('http://'+location.host+'/crm?m=common/print_doc&type='+this.type+'&id='+this.id, 'print', 'width=1000,height=750,scrollbars=1,resizable=1,menubar=0,toolbar=0,location=no,status=0,titlebar=0');
			win.focus();
		}, 
		scope:this
	},

	{
		text:'בטל', 
		handler:function(){
			this.close();
		}, 
		scope:this
	}
	]
})
		
		
this.submitForm = function() {
		
	Ext.MessageBox.show({
		width:280, 
		title:'אזהרה!', 
		msg:'לאחר שמירה לא ניתן לשנות את המסמך! האם להמשיך?', 
		buttons:Ext.MessageBox.YESNO, 
		icon: Ext.MessageBox.QUESTION, 
		scope:this,
		fn: function(btn) {
			if(btn!='yes') return;
		}
	});
		
	var data=[], idata=[], isum=0, sum=0, vat_sum=0, error='';
	this.grid.stopEditing();
	
	
	
	this.prevPeriodsStore.each(function(r){
		//alert (r.data.sum);
		if (r.data.sum) {
			//alert (r.data)
			data.push(r.data);
			sum=sum+parseFloat(r.data.sum);
				
		} else {
			this.prevPeriodsStore.remove(r)
			}
	}, this);

	this.prevPeriodsStore.each(function(r){
		if (r.data.sum_tax<r.data.sum) {
			error+='סכום '+r.data.name+' שגוי!';
			return false;
		}
		idata.push(r.data);
		isum=isum+(r.data.sum_tax==null ? 0: parseFloat(r.data.sum_tax));
		vat_sum=vat_sum+(r.data.sum_tax==null ? 0: parseFloat(r.data.sum_tax))-(r.data.sum==null ? 0: parseFloat(r.data.sum))
		});
		
	if (error!='') {
		Ext.Msg.alert('הזהרה!', error);
		return;
	} 
		
	//if (cfg.isum && cfg.isum!=isum) {Ext.Msg.alert('הזהרה!', 'סכום החשבונית לא שווה לסכות תקבול ('+cfg.isum+' ₪) !'); return;} 
	// if (this.type=='invoice_accept' && isum!=asum) {Ext.Msg.alert('שגיאה!','סכום חשבונית לא שווה לסכום קבלה!'); return;}
	this.form.form.submit({
		url: "?m=common/accounting&f=prevPeriods",
		params:{
			formType:2
			,
			rowsdata:Ext.encode(data)
			},
		waitTitle:'אנא המתן...',
		waitMsg:'טוען...',
		success: function(f,a){
			Ext.MessageBox.show({
				width:280, 
				title:'אישור!', 
				msg:'פעולה בוצעה בהצלחה', 
				buttons:Ext.MessageBox.OK, 
				icon: Ext.MessageBox.QUESTION, 
				scope:this
			
			});
			// this.grid.tbar.hide();
			// if (this.type=='invoice_credit' || this.type=='invoice_debt' || this.type=='invoice_accept') this.grid.tbar.dom.style.display='none';
			if (this.type=='accept' || this.type=='accept_tmp' || this.type=='invoice_accept') this.prevPeriodsStore.tbar.dom.style.display='none';
			// this.syncSize();
			//if (cfg.callBack) cfg.callBack();
			this.id = Ext.decode(a.response.responseText).id;
			//this.form.find('name','id')[0].setValue(this.id);
			this.id = Ext.decode(a.response.responseText).id;
			//this.form.find('name','id')[0].setValue(this.id);
			this.buttons[0].disable();
			this.buttons[1].enable();
			this.buttons[2].setText('סגור');
			this.syncShadow();
		},//this.close(); 
		failure: function(r,o){
			ajRqErrors(o.result);
		},//this.close(); 
		scope:this
	});
};

};
Ext.extend(prevPeriodsType2, Ext.Window);