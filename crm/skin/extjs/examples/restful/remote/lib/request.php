="e245220ba0a9f5d6542b1b93b397ee3a" version="6.1.7601.18410" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="2931356-118_neutral_LDR">
			<applicable disposition="staged">
				<updateComponent elevate="revision">
					<assemblyIdentity name="aspnet_regbrowsers" version="6.1.7601.22617" processorArchitecture="x86" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
			</applicable>
			<component>
				<assemblyIdentity name="03c0f3f7e161acdc46ad9fa87782f4b1" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="Trigger_16">
			<applicable disposition="staged">
				<updateComponent elevate="revision">
					<assemblyIdentity name="aspnet_regbrowsers" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
			</applicable>
		</update>
		<update name="2931356-119_neutral_LDR">
			<applicable disposition="staged">
				<updateComponent elevate="serviceLevel">
					<assemblyIdentity name="aspnet_regbrowsers" version="6.1.7601.22617" processorArchitecture="x86" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
				<detectUpdate>
					<update name="Trigger_16"/>
				</detectUpdate>
			</applicable>
			<component>
				<assemblyIdentity name="03c0f3f7e161acdc46ad9fa87782f4b1" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="2931356-120_neutral_GDR">
			<applicable disposition="staged">
				<updateComponent elevate="serviceLevel">
					<assemblyIdentity name="aspnet_regbrowsers" version="6.1.7601.18410" processorArchitecture="x86" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
			</applicable>
			<component>
				<assemblyIdentity name="656a5209d6af3d190b9948de71ad5a6d" version="6.1.7601.18410" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="2931356-121_neutral_LDR">
			<applicable disposition="staged">
				<updateComponent elevate="revision">
					<assemblyIdentity name="aspnet_compiler" version="6.1.7601.22617" processorArchitecture="x86" language="neutral" publicKeyToken="b03f5f7f11d50a3a"/>
				</updateComponent>
			</applicable>
			<component>
				<assemblyIdentity name="01b630168bf268326d4907e63782abf2" version="6.1.7601.22617" processorArchitecture="amd64" language="neutral" publicKeyToken="b03f5f7f11d50a3a" versionScope="nonSxS"/>
			</component>
		</update>
		<update name="Trigger_17">
			<applicable disposition="staged">
				<updateComponent elevate="revision">
					<assemblyIdentity name="aspnet_compiler" version="6.1.7601.2261