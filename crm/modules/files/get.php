<?
if (stripos($_SERVER['PHP_SELF'], basename(__FILE__))) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

$NO_SKIN=1;
$NO_HEADERS=1;
if ($SVARS['user_type']!=3 AND $SVARS['parent_id']) $group_id=$SVARS['parent_id']; else $group_id=$SVARS['user_id'];

//Log file download for improt production
if ($_GET['get_log']) {
	$file='../tmp_files/'.$_GET['file'];
	if (!file_exists($file)) die('Error: file not exist!');
	// $_GET['download']=1;
	send_file($file, $_GET['file_name']);
	
}elseif($_GET['get_mail']){
	$client_id = intval($_GET['clnt']);
	$contact_id = intval($_GET['cct']);
	if(!$client_id OR !$contact_id) die ('Error: Data not valid!');
	
	$fileName = $_GET['file'];
	$file = "../mail_data/$group_id/".substr($client_id, -2)."/".$client_id."/".$contact_id."/".$fileName;
	
	if (!is_file($file)) {
		$fileName = iconv('UTF-8','Windows-1255',$_GET['file']);
		$file = "../mail_data/$group_id/".substr($client_id, -2)."/".$client_id."/".$contact_id."/".$fileName;
	}
	
	if (!is_file($file)) {
		send_headers();
		die("Error: file <b>$file</b> not exist!");
	}
	
	send_file($file,$fileName);
	
} elseif($_GET['get_attachment']){
	$fileName = iconv('UTF-8','Windows-1255',$_GET['file']);
	if($_GET['win']=='sendFax') $file = "../temp/fax_compose/$SVARS[user_id]/$_GET[timestart]/".$fileName;
	else if($_GET['fileSource']) $file=$_GET['fileSource'];
	else $file = "../temp/mail_compose/$SVARS[user_id]/$_GET[timestart]/".$fileName;
	
	if (!is_file($file)) {
		send_headers();
		die("Error: file <b>$fileName</b> not exist!");
	}
	send_file($file,$fileName);
	
} elseif($_GET['get_wav']){
	$client_id = intval($_GET['clnt']);
	$contact_id = intval($_GET['cct']);
	$fileName = $_GET['file'];
	if(!$client_id OR !$contact_id) die ('Error: Data not valid!');
	$file = "../phone_calls/$group_id/".substr($client_id, -2)."/".$client_id."/".$contact_id."/".$fileName;
	if (!is_file($file)) {
		send_headers();
		die("Error: file <b>$file</b> not exist!");
	}

	send_file($file);
	
}elseif((int)$_GET['digital_signature']){
	$fileName = $_GET['file'];
	$file = "../signature_files/users_digital/$group_id/".substr($SVARS['user_id'], -2)."/$SVARS[user_id]/".$fileName;
	
	if (!is_file($file)) {
		send_headers();
		die("Error: file <b>$file</b> not exist!");
	}

	send_file($file);
}else {
	if (!$group_id AND !$cSVARS['client_id']) die ('Error: no group_id');
	
	$_GET['file_id']=(int)$_GET['file_id'];
	if (!$_GET['file_id']) die('error');
	$sql="SELECT * FROM $CFG[prefix]crm_files WHERE id=$_GET[file_id]".($cSVARS['client_id']?" AND client_id=$cSVARS[client_id]":'');

	$f=sql2array($sql, 0,0,1);
	
	$file="../crm-files/$f[group_id]/".substr($f['client_id'], -2)."/$f[client_id]/$f[id]".($f['file_ext']?".$f[file_ext]":'');
	
	if (!is_file($file)) {
		send_headers();
		die("Error: file <b>$file</b> not exist!");
	}

	send_file($file);
}


//if (!function_exists('mime_content_type')){
function mime_by_ext($filename){
	$idx = strtolower(end( explode( '.',$filename )) );
	$mimet = array('ai' =>'application/postscript','aif' =>'audio/x-aiff','aifc' =>'audio/x-aiff','aiff' =>'audio/x-aiff','asc' =>'text/plain','atom' =>'application/atom+xml','avi' =>'video/x-msvideo','bcpio' =>'application/x-bcpio','bmp' =>'image/bmp','cdf' =>'application/x-netcdf','cgm' =>'image/cgm','cpio' =>'application/x-cpio','cpt' =>'application/mac-compactpro','crl' =>'application/x-pkcs7-crl','crt' =>'application/x-x509-ca-cert','csh' =>'application/x-csh','css' =>'text/css','dcr' =>'application/x-director','dir' =>'application/x-director','djv' =>'image/vnd.djvu','djvu' =>'image/vnd.djvu','doc' =>'application/msword','docx' =>'application/msword','dtd' =>'application/xml-dtd','dvi' =>'application/x-dvi','dxr' =>'application/x-director','eps' =>'application/postscript','etx' =>'text/x-setext','ez' =>'application/andrew-inset','gif' =>'image/gif','gram' =>'application/srgs','grxml' =>'application/srgs+xml','gtar' =>'application/x-gtar','hdf' =>'application/x-hdf','hqx' =>'application/mac-binhex40','html' =>'text/html','html' =>'text/html','ice' =>'x-conference/x-cooltalk','ico' =>'image/x-icon','ics' =>'text/calendar','ief' =>'image/ief','ifb' =>'text/calendar','iges' =>'model/iges','igs' =>'model/iges','jpe' =>'image/jpeg','jpeg' =>'image/jpeg','jpg' =>'image/jpeg','js' =>'application/x-javascript','kar' =>'audio/midi','latex' =>'application/x-latex','m3u' =>'audio/x-mpegurl','man' =>'application/x-troff-man','mathml' =>'application/mathml+xml','me' =>'application/x-troff-me','mesh' =>'model/mesh','mid' =>'audio/midi','midi' =>'audio/midi','mif' =>'application/vnd.mif','mov' =>'video/quicktime','movie' =>'video/x-sgi-movie','mp2' =>'audio/mpeg','mp3' =>'audio/mpeg','mpe' =>'video/mpeg','mpeg' =>'video/mpeg','mpg' =>'video/mpeg','mpga' =>'audio/mpeg','ms' =>'application/x-troff-ms','msh' =>'model/mesh','mxu m4u' =>'video/vnd.mpegurl','nc' =>'application/x-netcdf','oda' =>'application/oda','ogg' =>'application/ogg','pbm' =>'image/x-portable-bitmap','pdb' =>'chemical/x-pdb','pdf' =>'application/pdf','pgm' =>'image/x-portable-graymap','pgn' =>'application/x-chess-pgn','php' =>'application/x-httpd-php','php4' =>'application/x-httpd-php','php3' =>'application/x-httpd-php','phtml' =>'application/x-httpd-php','phps' =>'application/x-httpd-php-source','png' =>'image/png','pnm' =>'image/x-portable-anymap','ppm' =>'image/x-portable-pixmap','ppt' =>'application/vnd.ms-powerpoint','ps' =>'application/postscript','qt' =>'video/quicktime','ra' =>'audio/x-pn-realaudio','ram' =>'audio/x-pn-realaudio','ras' =>'image/x-cmu-raster','rdf' =>'application/rdf+xml','rgb' =>'image/x-rgb','rm' =>'application/vnd.rn-realmedia','roff' =>'application/x-troff','rtf' =>'text/rtf','rtx' =>'text/richtext','sgm' =>'text/sgml','sgml' =>'text/sgml','sh' =>'application/x-sh','shar' =>'application/x-shar','shtml' =>'text/html','silo' =>'model/mesh','sit' =>'application/x-stuffit','skd' =>'application/x-koan','skm' =>'application/x-koan','skp' =>'application/x-koan','skt' =>'application/x-koan','smi' =>'application/smil','smil' =>'application/smil','snd' =>'audio/basic','spl' =>'application/x-futuresplash','src' =>'application/x-wais-source','sv4cpio' =>'application/x-sv4cpio','sv4crc' =>'application/x-sv4crc','svg' =>'image/svg+xml','swf' =>'application/x-shockwave-flash','t' =>'application/x-troff','tar' =>'application/x-tar','tcl' =>'application/x-tcl','tex' =>'application/x-tex','texi' =>'application/x-texinfo','texinfo' =>'application/x-texinfo','tgz' =>'application/x-tar','tif' =>'image/tiff','tiff' =>'image/tiff','tr' =>'application/x-troff','tsv' =>'text/tab-separated-values','txt' =>'text/plain','ustar' =>'application/x-ustar','vcd' =>'application/x-cdlink','vrml' =>'model/vrml','vxml' =>'application/voicexml+xml','wav' =>'audio/x-wav','wbmp' =>'image/vnd.wap.wbmp','wbxml' =>'application/vnd.wap.wbxml','wml' =>'text/vnd.wap.wml','wmlc' =>'application/vnd.wap.wmlc','wmlc' =>'application/vnd.wap.wmlc','wmls' =>'text/vnd.wap.wmlscript','wmlsc' =>'application/vnd.wap.wmlscriptc','wmlsc' =>'application/vnd.wap.wmlscriptc','wrl' =>'model/vrml','xbm' =>'image/x-xbitmap','xht' =>'application/xhtml+xml','xhtml' =>'application/xhtml+xml','xls' =>'application/vnd.ms-excel','xml xsl' =>'application/xml','xpm' =>'image/x-xpixmap','xslt' =>'application/xslt+xml','xul' =>'application/vnd.mozilla.xul+xml','xwd' =>'image/x-xwindowdump','xyz' =>'chemical/x-xyz','zip' =>'application/zip');
	if (isset( $mimet[$idx] )){return $mimet[$idx];}
	else{return 'application/octet-stream';}
}
//}

/**
*Check is Local server
*@return true when server is local and false in another case
*/
function isLocalServer() {
	return preg_match('/\.loc$/i', $_SERVER['HTTP_HOST']);
}

/**
*Send file to client
*@params (string, strung) $fileToTransfer path file to transfer, $destFileName - destination file name
*/
function send_file($fileToTransfer, $destFileName='') {
	if (isLocalServer()) a_send_file($fileToTransfer, $destFileName); # for local tests
	else x_send_file($fileToTransfer, $destFileName);	# Lighttpd X-Sendfile
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
function x_send_file($fileToTransfer, $destFileName='') {
	if ($_GET['download']) { 
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment;'.($destFileName?' filename="'.$destFileName.'"':'') );
	}else header('Content-Type: '.mime_by_ext($fileToTransfer));
	header("X-LIGHTTPD-send-file: ".realpath($fileToTransfer));
}

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧
# $speedLimit in Kb, 0 = no limit
function a_send_file($fileToTransfer, $destFileName='', $speedLimit=0) {
	if (connection_status()!=0) {return;}

	if (!$fileSize=@filesize($fileToTransfer)) {echo "Error: file size is 0"; return;}

	$startByte= 0;
	$lastByte = $fileSize-1;

	// Check if the user is trying to resume or download in chunks (ala GetRight)
	if (isset($_SERVER['HTTP_RANGE'])) {
		// $_SERVER['HTTP_RANGE'] is something like: bytes=10-
		if (preg_match("/^bytes=([0-9]+)-([0-9]*)$/", $_SERVER['HTTP_RANGE'], $matches)) {
			$startByte = $matches[1];
			if ($matches[2]) $lastByte = $matches[2];
			
			if (($startByte < 0) || ($startByte >= $fileSize)) { 	// Ack! Invalid range specified for the file.
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				header('Accept-Ranges: bytes');
				header('Content-Range: bytes */'. $fileSize);
				return;
			}
		}
	}

	// Send the required headers
	if (($startByte != 0) || ($lastByte != ($fileSize-1))) {
		header('HTTP/1.1 206 Partial Content');
		header('Content-Range: bytes '. $startByte .'-'. $lastByte .'/'. $fileSize);
	}
	
	if ($_GET['download']) { 
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment;'.($destFileName?' filename="'.$destFileName.'"':'') );
	}else header('Content-Type: '.mime_by_ext($fileToTransfer));
	header('Accept-Ranges: bytes');
	header('Content-Length: '.(($lastByte+1) - $startByte));
	header('Connection: close');
	ob_end_clean();		// Make sure the headers have been sent to the client.

	if (strcmp($_SERVER['REQUEST_METHOD'], "HEAD") == 0) return;

	# We open the output to get the file handle instead of using print/echo - this way we can check the return code of fwrite() to see how many bytes were written to the output.
	if (!$outFile=fopen("php://output", "wb")) return "Unable to open output stream!";
	# Open File that is being downloaded
	if (!$inFile=fopen($fileToTransfer, "rb")) return "Unable to open file to be downloaded!";

	# Set Number of bytes to transfer in each chunk
	$transferChunkSize = $speedLimit ? $speedLimit*1024 : 8192;

	# If resuming, seek to the requested position in the file.
	if ($startByte != 0) fseek($inFile, $startByte);

	$bytesWritten = 0;
	while (!feof($inFile) && ($bytesWritten <= $lastByte) && (connection_status() == 0)) {
		set_time_limit(30);
		$thisChunkSize = $transferChunkSize;
		// $lastByte+1 since it is always 1 less than the transfer size as it is 0-based
		if (($bytesWritten + $thisChunkSize) > ($lastByte+1)) $thisChunkSize = ($lastByte+1) - $bytesWritten;

		//$data = fread($inFile, $thisChunkSize);

		// Write out the data, and flush the output streams to make sure that it isn't being cached locally
		$res = fwrite($outFile, fread($inFile, $thisChunkSize), $thisChunkSize);
		fflush($outFile);
		flush();

		if ($res !== FALSE) $bytesWritten += $res;
		if ($speedLimit != 0) sleep(1);
	}

	fclose($inFile);
	fclose($outFile);
}
?>