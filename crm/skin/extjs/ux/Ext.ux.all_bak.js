
function $(sId) { if (sId) return document.getElementById(sId); }

Array.prototype.inArray=function(valeur) {
	for (var i in this) if (this[i]===valeur) return true;
 return false;
}

// function trim(str) { if (str) return str.replace(/^(\s+)?(\S*)(\s+)?$/, '$2'); else return str; }
// if(!String.prototype.trim) String.prototype.trim = function() { return this.replace(/^\s*/,'').replace(/\s*$/, ''); }


//————————————————————————————————————————————————————————————————————
function debugWin(text) {
	if (!window.top.debugWindow || window.top.debugWindow.closed) {
		window.top.debugWindow = window.open("", "Debug", "width=600,height=600,scrollbars=yes,status=yes,resizable=yes");	//left=0,top=0,
		window.top.debugWindow.opener = self;
		window.top.debugWindow.document.open();
		window.top.debugWindow.document.write("<html><head><title>Debug Window</title><style>body,pre{font-size:12px; font-family:Courier New; margin:0}</style></head><body>\n");
		window.top.debugWindow.document.write(text+"<hr>\n");
		window.top.debugWindow.focus();
	}else {
		window.top.debugWindow.document.write(text+"<hr>\n");
		window.top.debugWindow.focus();
	}
}
function Dump(d,ml,l) {
	if (l == null) l = 1;
	if (!ml) ml=2;
	var s = '';
	if (d==null) s = '<font color=blue>null</font>';
	else if (l>ml && (typeof(d)=="object")) s = '<font color=#6f6fff>'+typeof(d)+'...</font>';
	else if (d.constructor==Array) { // isArray
		var els=[];
		for (var i=0; i<d.length; i++) els.push(Dump(d[i], ml, 2));
		s += '['+ els.join(', ') +']';
	
	}else if (typeof(d)=='object') {
		if (d.nodeName) s += "<font color=#008080>HTML "+d.nodeName+" Element:</font> ";
		s += "{<blockquote style='margin:0 0 0 25px;'>";
		err=false;
		try { for(var k in d){} } catch (err){}
		if (!err) {
			for (var k in d) {
				s += '<pre><font color='+(/^[0-9]+$/.test(k)?'blue':'red')+'>'+ k +'</font>:';				
				if (k=='innerHTML' || k=='innerText' || k=='outerHTML' || k=='outerText' || k=='textContent') {
					var html=d[k];
					html=html.replace(/<(\/?[a-zA-Z]*)([^>]*)>/g, '<font color=blue>&lt;$1<font color=red>$2</font>&gt;</font>');
					html=html.replace(/("[^"]*")/g, '<font color=#fe01fe>$1</font>');
					html=html.replace(/('[^']*')/g, '<font color=#fe01fe>$1</font>');
					s += '</pre><div style="max-height:200px; overflow:auto; border:1px dashed silver;">'+html+'</div><pre>';
				}else if (k!='top' && k!='window' && k!='document' && k!='frames' && k!='self') {
					try { s += Dump(d[k], ml, l+1); } catch(e){ s+='*skipped* ('+e.message+')<br>';}
				}else s += '*skipped*<br>';
				s += '</pre>';
			}
		}
		s += '</blockquote>}'
	
	}else {
		if (typeof(d)=='string') s+="<font color=#fe01fe>'"+ d +"'</font>";
		else if (typeof(d)=='number' || typeof(d)=='boolean') s+="<font color=blue>"+ d +"</font>";
		else if (typeof(d)=='function') //s+="<font color=#6f6fff>function...</font>";
		/**/{
			var fs=d+' ';
			fs=fs.replace(/([~`!@#$%^&|*()_+\[\]{}<>\\\.;:,?=-]+)/g, '<font color=black>$1</font>');
			fs=fs.replace(/(^|\W)(break|case|catch|continue|debugger|default|delete|do|else|false|finally|for|function|if|in|instanceof|new|null|return|switch|this|throw|true|try|typeof|var|void|while|with)(\W|$)/g, '$1<font color=blue>$2</font>$3');
			fs=fs.replace(/(^|[^\w])([0-9]+)([^\w]|$)/g, '$1<font color=#3737ff>$2</font>$3');
			fs=fs.replace(/("[^"]*")/g, '<font color=#fe01fe>$1</font>');
			fs=fs.replace(/('[^']*')/g, '<font color=#fe01fe>$1</font>');
			s+='<pre><font color=red>'+ fs +'</font></pre>';
		}
		else s+= d +'<font color=gray>('+typeof(d)+")</font>";
	}

	if (l==1) debugWin(s);
	else return s;
}



// check ajax request errors and show message or login window
ajRqErrors = function(d, callback){
	if (d.relogin) {
		ShowLoginWin(d.msg, callback);
		return true;
	}else if (d.success==false) {
		Ext.MessageBox.show({title:'Error', msg:(d.msg?d.msg:'Error'), icon:Ext.MessageBox.ERROR, buttons:Ext.MessageBox.OK});
		return true;
	}
}


hlStr = function(str){
	var terms = this.searchField.terms;
	if (!str || !terms) return str;
	return str.toString().replace(new RegExp('('+terms.join(' ?|')+')', 'ig'), '<font color=#b30000 style="background-color:#ffffd9">$1</font>');
};


Ext.util.Format.ilMoney = function(v){
	if (v==='' || v==null) return "";
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '.00';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {whole = whole.replace(r, '$1' + ',' + '$2');}
    v = whole + sub;
    if(v.charAt(0) == '-'){return '₪ -' + v.substr(1);}
    return "₪ " +  v;
};

Ext.util.Format.date = function (v, format) {
    if (!v || v=='0000-00-00') return "";
    if (!(v instanceof Date)) v = new Date(Date.parse(v));
    return v.dateFormat(format || "d/m/Y");
};



GridSearchField = Ext.extend(Ext.form.TwinTriggerField, {
	initComponent: function(){
		if(!this.store.baseParams) this.store.baseParams = {};
		GridSearchField.superclass.initComponent.call(this);
		this.on('keyup', this.keySearch, this);
		this.keyUpTask = new Ext.util.DelayedTask(this.onTrigger2Click, this);
	},

	paramName:'q', enableKeyEvents:true, validationEvent:false, validateOnBlur:false, hideTrigger1:true, width:150, hasSearch:false,
	trigger1Class:'x-form-clear-trigger', trigger2Class:'x-form-search-trigger', terms:false,

	keySearch: function(f,e){
		if (e.keyCode<48 && e.keyCode!=8 && e.keyCode!=46) return;	// not word char or backspace/delete
		else if (e.keyCode==13) { this.keyUpTask.cancel(); this.onTrigger2Click(); return; } // enter
		this.keyUpTask.delay(700);
	},
	
	onTrigger1Click: function(){
		if(this.hasSearch){
			this.store.baseParams[this.paramName] = '';
			this.store.removeAll();
			this.el.dom.value = '';
			this.triggers[0].hide();
			this.hasSearch = false;
			this.terms=false;
			this.store.reload();
			this.focus();
		}
	},

	onTrigger2Click: function(){
		var v=this.getRawValue().trim();
		if(v.length<1) { this.onTrigger1Click(); return; }
		if(v.length<2) return;
		this.terms=v.replace(/[\.\+\?,;:\\()/]+/g, '\\$1').split(' ');
		this.store.removeAll();
		this.store.baseParams[this.paramName] = v;
		var o = {start:0};
		this.store.reload({params:o});
		this.hasSearch = true;
		this.triggers[0].show();
		this.focus();
	}
});

/*

// Grid RowExpander
Ext.grid.RowExpander = function(config){
	Ext.apply(this, config);
	this.addEvents({
		beforeexpand: true,
		expand: true,
		beforecollapse:true,
		collapse: true
	});
	Ext.grid.RowExpander.superclass.constructor.call(this);
	if(this.tpl){
		if(typeof this.tpl == 'string') this.tpl = new Ext.Template(this.tpl);
		this.tpl.compile();
	}
	this.state = {};
	this.bodyContent = {};
};

Ext.extend(Ext.grid.RowExpander, Ext.util.Observable, {
	header: "",
	width: 20,
	sortable: false,
	fixed:true,
	menuDisabled:true,
	dataIndex: '',
	id: 'expander',
	lazyRender: true,
	enableCaching: true,

	getRowClass: function(record, rowIndex, p, ds){
		p.cols = p.cols-1;
		var content = this.bodyContent[record.id];
		if(!content && !this.lazyRender) content=this.getBodyContent(record, rowIndex);
		if(content) p.body = content;
		return this.state[record.id] ? 'x-grid3-row-expanded' : 'x-grid3-row-collapsed';
	},

	init: function(grid){
		this.grid = grid;
		var view = grid.getView();
		view.getRowClass = this.getRowClass.createDelegate(this);
		view.enableRowBody = true;
		grid.on('render', function(){view.mainBody.on('mousedown', this.onMouseDown, this);}, this);
	},

	getBodyContent: function(record, index){
		if(!this.enableCaching) return this.tpl.apply(record.data);
		var content = this.bodyContent[record.id];
		if(!content){
			content = this.tpl.apply(record.data);
			this.bodyContent[record.id] = content;
		}
		return content;
	},

	onMouseDown: function(e, t){
		if(t.className == 'x-grid3-row-expander'){
			e.stopEvent();
			var row = e.getTarget('.x-grid3-row');
			this.toggleRow(row);
		}
	},

	renderer: function(v, p, record){
		p.cellAttr = 'rowspan="2"';
		return '<div class="x-grid3-row-expander">&#160;</div>';
	},

	beforeExpand: function(record, body, rowIndex){
		if(this.fireEvent('beforeexpand', this, record, body, rowIndex) !== false){
			if(this.tpl && this.lazyRender) body.innerHTML=this.getBodyContent(record, rowIndex);
			return true;
		}else return false;
	},

	toggleRow: function(row){
		if(typeof row == 'number') row=this.grid.view.getRow(row);
		this[Ext.fly(row).hasClass('x-grid3-row-collapsed') ? 'expandRow' : 'collapseRow'](row);
	},

	expandRow: function(row){
		if(typeof row == 'number') row=this.grid.view.getRow(row);
		var record = this.grid.store.getAt(row.rowIndex);
		var body = Ext.DomQuery.selectNode('tr:nth(2) div.x-grid3-row-body', row);
		if(this.beforeExpand(record, body, row.rowIndex)){
			this.state[record.id] = true;
			Ext.fly(row).replaceClass('x-grid3-row-collapsed', 'x-grid3-row-expanded');
			this.fireEvent('expand', this, record, body, row.rowIndex);
		}
	},

	collapseRow: function(row){
		if(typeof row == 'number') row=this.grid.view.getRow(row);
		var record = this.grid.store.getAt(row.rowIndex);
		var body = Ext.fly(row).child('tr:nth(1) div.x-grid3-row-body', true);
		if(this.fireEvent('beforecollapse', this, record, body, row.rowIndex) !== false){
			this.state[record.id] = false;
			Ext.fly(row).replaceClass('x-grid3-row-expanded', 'x-grid3-row-collapsed');
			this.fireEvent('collapse', this, record, body, row.rowIndex);
		}
	}
});

*/

// Add the additional 'advanced' VTypes
Ext.apply(Ext.form.VTypes, {
	daterange : function(val, field) {
		var date = field.parseDate(val);

		if(!date){
			return;
		}
		if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
			var start = Ext.getCmp(field.startDateField);
			start.setMaxValue(date);
			start.validate();
			this.dateRangeMax = date;
		} 
		else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
			var end = Ext.getCmp(field.endDateField);
			end.setMinValue(date);
			end.validate();
			this.dateRangeMin = date;
		}
		// Always return true since we're only using this vtype to set the
		// min/max allowed values (these are tested for after the vtype test)
		return true;
	},

	password : function(val, field) {
		if (field.initialPassField) {
			var pwd = Ext.getCmp(field.initialPassField);
			return (val == pwd.getValue());
		}
		return true;
	},

	passwordText : 'Passwords do not match'
});

/*

// Upload Field
Ext.ns('Ext.ux.form');
Ext.ux.form.FileUploadField = Ext.extend(Ext.form.TextField,  {
	buttonText: '&nbsp; ...עיין &nbsp;',
	buttonOnly: false,
	buttonOffset: 3,
	readOnly: true,
	autoSize: Ext.emptyFn,
	initComponent: function(){
		Ext.ux.form.FileUploadField.superclass.initComponent.call(this);
		this.addEvents('fileselected');
	},
	onRender : function(ct, position){
		Ext.ux.form.FileUploadField.superclass.onRender.call(this, ct, position);
		this.wrap = this.el.wrap({cls:'x-form-field-wrap x-form-file-wrap'});
		this.el.addClass('x-form-file-text');
		this.el.dom.removeAttribute('name');
		this.fileInput = this.wrap.createChild({
			id:this.getFileInputId(),
			name:this.name||this.getId(),
			cls:'x-form-file', tag:'input', type:'file', size:1
		});
		var btnCfg = Ext.applyIf(this.buttonCfg || {}, {text:this.buttonText});
		this.button = new Ext.Button(Ext.apply(btnCfg, {
			renderTo: this.wrap,
			cls: 'x-form-file-btn' + (btnCfg.iconCls ? ' x-btn-icon' : '')
		}));
		if(this.buttonOnly){
			this.el.hide();
			this.wrap.setWidth(this.button.getEl().getWidth());
		}
		this.fileInput.on('change', function(){
			var v = this.fileInput.dom.value;
			this.setValue(v);
			this.fireEvent('fileselected', this, v);
		}, this);
	},
	getFileInputId: function(){return this.id + '-file';},
	onResize: function(w, h){
		Ext.ux.form.FileUploadField.superclass.onResize.call(this, w, h);
		this.wrap.setWidth(w);
		if(!this.buttonOnly){
			var w = this.wrap.getWidth() - this.button.getEl().getWidth() - this.buttonOffset;
			this.el.setWidth(w);
		}
	},
	onDestroy: function(){
		Ext.ux.form.FileUploadField.superclass.onDestroy.call(this);
		Ext.destroy(this.fileInput, this.button, this.wrap);
	},
	preFocus : Ext.emptyFn,
	getResizeEl : function(){return this.wrap;},
	getPositionEl : function(){return this.wrap;},
	alignErrorIcon : function(){this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);}
});
Ext.reg('fileuploadfield', Ext.ux.form.FileUploadField);
Ext.form.FileUploadField = Ext.ux.form.FileUploadField;





Ext.tree.ColumnTree = Ext.extend(Ext.tree.TreePanel, {
	lines:false,
	borderWidth: Ext.isBorderBox ? 0 : 2, // the combined left/right border for each cell
	cls:'x-column-tree',
	
	onRender : function(){
		Ext.tree.ColumnTree.superclass.onRender.apply(this, arguments);
		this.headers = this.body.createChild({cls:'x-tree-headers'},this.innerCt.dom);

		var cols = this.columns, c;
		var totalWidth = 0;

		for(var i = 0, len = cols.length; i < len; i++){
			 c = cols[i];
			 totalWidth += c.width;
			 this.headers.createChild({
				 cls:'x-tree-hd ' + (c.cls?c.cls+'-hd':''),
				 cn: {cls:'x-tree-hd-text', html: c.header},
				 style: (c.width ? 'width:'+(c.width-this.borderWidth)+'px;' : 'border-left:0')
			 });
		}
		this.headers.createChild({cls:'x-clear'});
		// prevent floats from wrapping when clipped
		//this.headers.setWidth(totalWidth);
		//this.innerCt.setWidth(totalWidth);
	}
});

Ext.tree.ColumnNodeUI = Ext.extend(Ext.tree.TreeNodeUI, {
	focus: Ext.emptyFn, // prevent odd scrolling behavior

	renderElements: function(n, a, targetNode, bulkRender){
		this.indentMarkup = n.parentNode ? n.parentNode.ui.getChildIndent() : '';

		var t = n.getOwnerTree();
		var cols = t.columns;
		var bw = t.borderWidth;
		var c = cols[0];

		var buf = [
			 '<li class="x-tree-node"><div ext:tree-node-id="',n.id,'" class="x-tree-node-el x-tree-node-leaf ', a.cls,'">',
				'<div class="x-tree-col" style="width:',c.width-bw,'px;">',
					'<span class="x-tree-node-indent">',this.indentMarkup,"</span>",
					'<img src="', this.emptyIcon, '" class="x-tree-ec-icon x-tree-elbow">',
					'<img src="', a.icon || this.emptyIcon, '" class="x-tree-node-icon',(a.icon ? " x-tree-node-inline-icon" : ""),(a.iconCls ? " "+a.iconCls : ""),'" unselectable="on">',
					'<a hidefocus="on" class="x-tree-node-anchor" href="',a.href ? a.href : "#",'" tabIndex="1" ',
					a.hrefTarget ? ' target="'+a.hrefTarget+'"' : "", '>',
					'<span unselectable="on">', n.text || (c.renderer ? c.renderer(a[c.dataIndex], n, a) : a[c.dataIndex]),"</span></a>",
				'</div>'];
		 for(var i = 1, len = cols.length; i < len; i++){
			 c = cols[i];
			 var text = (c.renderer ? c.renderer(a[c.dataIndex], n, a) : a[c.dataIndex]);
			 
			 buf.push('<div class="x-tree-col ',(c.cls?c.cls:''),'" style="width:',c.width-bw,'px;">',
						'<div class="x-tree-col-text">',(text ? text : '&nbsp;'),"</div>",
					  '</div>');
		 }
		 buf.push('<div class="x-clear"></div></div><ul class="x-tree-node-ct" style="display:none;"></ul></li>');

		if(bulkRender !== true && n.nextSibling && n.nextSibling.ui.getEl()){this.wrap = Ext.DomHelper.insertHtml("beforeBegin", n.nextSibling.ui.getEl(), buf.join(""));
		}else{this.wrap = Ext.DomHelper.insertHtml("beforeEnd", targetNode, buf.join(""));}

		this.elNode = this.wrap.childNodes[0];
		this.ctNode = this.wrap.childNodes[1];
		var cs = this.elNode.firstChild.childNodes;
		this.indentNode = cs[0];
		this.ecNode = cs[1];
		this.iconNode = cs[2];
		this.anchor = cs[3];
		this.textNode = cs[3].firstChild;
	}
});



Ext.ux.IconCombo = Ext.extend(Ext.form.ComboBox, {
    initComponent:function() {
        Ext.apply(this, {
            tpl:'<tpl for=".">'
           		+'<tpl if="this.group != values.group"><tpl exec="this.group = values.group"></tpl><h1 dir=rtl>{group}</h1></tpl>'
                + '<div class="x-combo-list-item ux-icon-combo-item '
                + '{' + this.iconClsField + '}">'
                + '{' + this.displayField + '}</div></tpl>'
        });
        Ext.ux.IconCombo.superclass.initComponent.call(this);
    }, // end of function initComponent
 
    onRender:function(ct, position) {
        Ext.ux.IconCombo.superclass.onRender.call(this, ct, position);
        this.wrap.applyStyles({position:'relative'});
        this.el.addClass('ux-icon-combo-input');
        this.icon = Ext.DomHelper.append(this.el.up('div.x-form-field-wrap'), {
            tag: 'div', style:'position:absolute'
        });
    }, // end of function onRender
 
    setIconCls:function() {
        var rec = this.store.query(this.valueField, this.getValue()).itemAt(0);
        if(rec) {
            this.icon.className = 'ux-icon-combo-icon ' + rec.get(this.iconClsField);
        }
    }, // end of function setIconCls
 
    setValue: function(value) {
        Ext.ux.IconCombo.superclass.setValue.call(this, value);
        this.setIconCls();
    } // end of function setValue
});
Ext.reg('iconcombo', Ext.ux.IconCombo);


Ext.ux.HtmlCombo = Ext.extend(Ext.form.ComboBox, {
	mode:'local',
	editable:false,
	listClass:'x-htmlcombo-list',
	triggerAction:'all',
	defaultAutoCreate: {tag:"div", type:'text', 'class':'x-form-htmlcombo'},
	getRawValue:function () {
		var v = Ext.value(this.value, "");
		if (v===this.emptyText) v="";
		return v;
	},
	setValue: function(v){
		var text = v;
		if(this.valueField){
			var r = this.findRecord(this.valueField, v);
			if(r) text = r.data[this.displayField];
			else if(Ext.isDefined(this.valueNotFoundText)) text = this.valueNotFoundText;
		}
		this.lastSelectionText = text;
		this.el.dom.innerHTML = text;
		if(this.hiddenField) this.hiddenField.value = v;
		this.value = v;
		return this;
	}
});
Ext.reg('htmlcombo', Ext.ux.HtmlCombo);

Ext.grid.GridSummary = function(config){Ext.apply(this, config);};

Ext.extend(Ext.grid.GridSummary, Ext.util.Observable, {
    init : function(grid){
        this.grid = grid;
        this.cm = grid.getColumnModel();
        this.view = grid.getView();
        var v = this.view;
        v.layout = this.layout.createDelegate(this);
        v.afterMethod('refresh', this.refreshSummary, this);
        v.afterMethod('refreshRow', this.refreshSummary, this);
        v.afterMethod('onColumnWidthUpdated', this.doWidth, this);
        v.afterMethod('onAllColumnWidthsUpdated', this.doAllWidths, this);
        v.afterMethod('onColumnHiddenUpdated', this.doHidden, this);
        v.afterMethod('onUpdate', this.refreshSummary, this);
        v.afterMethod('onRemove', this.refreshSummary, this);

        if(!this.rowTpl){
            this.rowTpl = new Ext.Template(
                '<div class="x-grid3-summary-row" style="{tstyle}">',
                '<table class="x-grid3-summary-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
                    '<tbody><tr>{cells}</tr></tbody>',
                '</table></div>'
            );
            this.rowTpl.disableFormats = true;
        }
        this.rowTpl.compile();

        if(!this.cellTpl){
            this.cellTpl = new Ext.Template(
                '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}">',
                '<div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on">{value}</div>',
                "</td>"
            );
            this.cellTpl.disableFormats = true;
        }
        this.cellTpl.compile();
    },
	Calculations: {
		'none' : function(v, record, field){return '';},
		'sum'  : function(v, record, field){return parseFloat(v) + (typeof record.data[field]=='undefined' ? 0 :parseFloat(record.data[field]));},
		'count': function(v, record, field, data){return data[field+'count'] ? ++data[field+'count'] : (data[field+'count'] = 1);},
		'max'  : function(v, record, field, data){
			var v = record.data[field];
			var max = data[field+'max'] === undefined ? (data[field+'max'] = v) : data[field+'max'];
			return v > max ? (data[field+'max'] = v) : max;
		},

		'min' : function(v, record, field, data){
			var v = record.data[field];
			var min = data[field+'min'] === undefined ? (data[field+'min'] = v) : data[field+'min'];
			return v < min ? (data[field+'min'] = v) : min;
		},

		'average' : function(v, record, field, data){
			var c = data[field+'count'] ? ++data[field+'count'] : (data[field+'count'] = 1);
			var t = (data[field+'total'] = ((data[field+'total']||0) + (record.data[field]||0)));
			return t === 0 ? 0 : t / c;
		}
	},
    renderSummary : function(o, cs){
        cs = cs || this.view.getColumnData();
        var cfg = this.cm.config;

        var buf = [], c, p = {}, cf, last = cs.length-1;
        for(var i = 0, len = cs.length; i < len; i++){
            c = cs[i];
            cf = cfg[i];
            p.id = c.id;
            p.style = c.style;
            p.css = i == 0 ? 'x-grid3-cell-first ' : (i == last ? 'x-grid3-cell-last ' : '');
            if(cf.summaryType || cf.summaryRenderer){
                p.value = (cf.summaryRenderer || c.renderer)(o.data[c.name], p, o);
            }else{ p.value = ''; }
            if(p.value == undefined || p.value === "") p.value = "";
            buf[buf.length] = this.cellTpl.apply(p);
        }
        return this.rowTpl.apply({tstyle: 'width:'+this.view.getTotalWidth()+';', cells: buf.join('')});
    },

    calculate : function(rs, cs){
        var data = {}, r, c, cfg = this.cm.config, cf;
        for(var j = 0, jlen = rs.length; j < jlen; j++){
            r = rs[j];
            for(var i = 0, len = cs.length; i < len; i++){
                c = cs[i];
                cf = cfg[i];
                if(cf && cf.summaryType) data[c.name] = this.Calculations[cf.summaryType](data[c.name] || 0, r, c.name, data);
            }
        }
        return data;
    },

    layout : function(){
		if (!this.view.summary) this.view.summary = Ext.DomHelper.insertAfter(this.view.mainBody.dom.parentNode, {tag:'div'}, true);
        if(!this.view.mainBody) return;
        var g = this.grid;
        var c = g.getGridEl(), cm = this.cm, expandCol = g.autoExpandColumn, gv = this.view;
        var csize = c.getSize(true);
        var vw = csize.width;
        if(!vw || !csize.height) return;

        if(g.autoHeight){this.view.scroller.dom.style.overflow = 'visible';
        }else{
            var smHeight = this.view.summary.getHeight();
            var hdHeight = this.view.mainHd.getHeight();
            this.view.el.setSize(csize.width, csize.height + smHeight);
            var vh = csize.height - (hdHeight) - (smHeight);
            this.view.scroller.setSize(vw, vh);
            this.view.innerHd.style.width = (vw)+'px';
        }
        if(this.view.forceFit){
            if(this.view.lastViewWidth != vw){
                this.view.fitColumns(false, false);
                this.view.lastViewWidth = vw;
            }
        }else {this.view.autoExpand();}
        this.view.onLayout(vw, vh);
    },

    doWidth : function(col, w, tw){
        var gs = this.view.getRows(), s;
        for(var i = 0, len = gs.length; i < len; i++){
            s = gs[i].childNodes[2];
            s.style.width = tw;
            s.firstChild.style.width = tw;
            s.firstChild.rows[0].childNodes[col].style.width = w;
        }
    },

    doAllWidths : function(ws, tw){
        var gs = this.view.getRows(), s, cells, wlen = ws.length;
        for(var i = 0, len = gs.length; i < len; i++){
            s = gs[i].childNodes[2];
            s.style.width = tw;
            s.firstChild.style.width = tw;
            cells = s.firstChild.rows[0].childNodes;
            for(var j = 0; j < wlen; j++){cells[j].style.width = ws[j];}
        }
    },

    doHidden : function(col, hidden, tw){
        var gs = this.view.getRows(), s, display = hidden ? 'none' : '';
        for(var i = 0, len = gs.length; i < len; i++){
            s = gs[i].childNodes[2];
            s.style.width = tw;
            s.firstChild.style.width = tw;
            s.firstChild.rows[0].childNodes[col].style.display = display;
        }
    },

    // Note: requires that all (or the first) record in the 
    // group share the same group value. Returns false if the group
    // could not be found.
    refreshSummary : function(){
        var g = this.grid, cm = g.colModel, ds = g.store, stripe = g.stripeRows;
        var colCount = cm.getColumnCount();
        if(ds.getCount() < 1){return "";}
        var cs = this.view.getColumnData();
        var startRow = startRow || 0;
        var endRow = typeof endRow == "undefined"? ds.getCount()-1 : endRow;
        var rs = ds.getRange();
		var buf = [];
		var data = this.calculate(rs, cs);
        buf.push('</div>', this.renderSummary({data: data}, cs), '</div>');
        this.view.summary.update(buf.join(''));
        this.view.layout();
    },

    getSummaryNode : function(){return this.view.summary },

    showSummaryMsg : function(groupValue, msg){
        var gid = this.view.getGroupId(groupValue);
        var node = this.getSummaryNode(gid);
        if(node){node.innerHTML = '<div class="x-grid3-summary-msg">' + msg + '</div>';}
    }
});




Ext.ux.form.DateTime = Ext.extend(Ext.form.Field, {
	 dateValidator:null
	,defaultAutoCreate:{tag:'input', type:'hidden'}
	,dtSeparator:' '			// Date - Time separator. Used to split date and time (defaults to ' ' (space))
	,hiddenFormat:'Y-m-d H:i'
	,otherToNow:true			// Set other field to now() if not explicly filled in (defaults to true)
	//,emptyToNow:				// Set field value to now on attempt to set empty value. If it is true then setValue() sets value of field to current date and time (defaults to false)
	,timePosition:'right' 		// valid values:'below', 'right'
	,timeValidator:null
	,timeWidth:55
	,width:140
	,dateFormat:'d/m/Y'
	// ,dateConfig:{altFormats:'d/m/y|d.m.Y|d.m.y|Y-m-d|dmY'}
	,timeFormat:'H:i'
	,timeConfig:{altFormats:'H:i:s|H i|G:i|G i|Hi'}

	,initComponent:function() {
		// call parent initComponent
		Ext.ux.form.DateTime.superclass.initComponent.call(this);

		// create DateField
		var dateConfig = Ext.apply({}, {
			 id:this.id + '-date'
			,format:this.dateFormat || Ext.form.DateField.prototype.format
			,width:this.timeWidth
			,selectOnFocus:this.selectOnFocus
			,validator:this.dateValidator
			,listeners:{
				 blur:{scope:this, fn:this.onBlur}
				,focus:{scope:this, fn:this.onFocus}
			}
		}, this.dateConfig);
		this.df = new Ext.form.DateField(dateConfig);
		this.df.ownerCt = this;
		delete(this.dateFormat);

		// create TimeField
		var timeConfig = Ext.apply({}, {
			 id:this.id + '-time'
			,format:this.timeFormat || Ext.form.TimeField.prototype.format
			,width:this.timeWidth
			,selectOnFocus:this.selectOnFocus
			,validator:this.timeValidator
			,listeners:{
				 blur:{scope:this, fn:this.onBlur}
				,focus:{scope:this, fn:this.onFocus}
			}
		}, this.timeConfig);
		this.tf = new Ext.form.TimeField(timeConfig);
		this.tf.ownerCt = this;
		delete(this.timeFormat);

		// relay events
		this.relayEvents(this.df, ['focus', 'specialkey', 'invalid', 'valid']);
		this.relayEvents(this.tf, ['focus', 'specialkey', 'invalid', 'valid']);

		this.on('specialkey', this.onSpecialKey, this);

	}
	//Renders underlying DateField and TimeField and provides a workaround for side error icon bug
	,onRender:function(ct, position) {
		if(this.isRendered) return;	// don't run more than once

		// render underlying hidden field
		Ext.ux.form.DateTime.superclass.onRender.call(this, ct, position);

		// render DateField and TimeField, create bounding table
		var t;
		if('below' === this.timePosition || 'bellow' === this.timePosition) {
			t = Ext.DomHelper.append(ct, {tag:'table',style:'border-collapse:collapse',children:[
				 {tag:'tr',children:[{tag:'td', style:'padding-bottom:1px', cls:'ux-datetime-date'}]}
				,{tag:'tr',children:[{tag:'td', cls:'ux-datetime-time'}]}
			]}, true);
		}
		else {
			t = Ext.DomHelper.append(ct, {tag:'table',style:'direction:ltr; border-collapse:collapse',children:[
				{tag:'tr',children:[
					{tag:'td',style:'padding-right:1px', cls:'ux-datetime-date'},{tag:'td', cls:'ux-datetime-time'}
				]}
			]}, true);
		}

		this.tableEl = t;
		this.wrap = t.wrap({cls:'x-form-field-wrap'});
		// this.wrap = t.wrap();
		this.wrap.on("mousedown", this.onMouseDown, this, {delay:10});

		// render DateField & TimeField
		this.df.render(t.child('td.ux-datetime-date'));
		this.tf.render(t.child('td.ux-datetime-time'));

		// workaround for IE trigger misalignment bug, see http://extjs.com/forum/showthread.php?p=341075#post341075
		// if(Ext.isIE && Ext.isStrict) { t.select('input').applyStyles({top:0}); }

		this.df.el.swallowEvent(['keydown', 'keypress']);
		this.tf.el.swallowEvent(['keydown', 'keypress']);

		// create icon for side invalid errorIcon
		if('side' === this.msgTarget) {
			var elp = this.el.findParent('.x-form-element', 10, true);
			if(elp) this.errorIcon = elp.createChild({cls:'x-form-invalid-icon'});

			var o = {
				 errorIcon:this.errorIcon
				,msgTarget:'side'
				,alignErrorIcon:this.alignErrorIcon.createDelegate(this)
			};
			Ext.apply(this.df, o);
			Ext.apply(this.tf, o);
			// this.df.errorIcon = this.errorIcon;
			// this.tf.errorIcon = this.errorIcon;
		}
		// setup name for submit
		this.el.dom.name = this.hiddenName || this.name || this.id;
		// prevent helper fields from being submitted
		this.df.el.dom.removeAttribute("name");
		this.tf.el.dom.removeAttribute("name");
		// we're rendered flag
		this.isRendered = true;
		// update hidden field
		this.updateHidden();
	}
	
	,adjustSize:Ext.BoxComponent.prototype.adjustSize
	
	,alignErrorIcon:function() {
		this.errorIcon.alignTo(this.tableEl, 'tl-tr', [2, 0]);
	}
	
	,initDateValue:function() {
		this.dateValue = this.otherToNow ? new Date() : new Date(1970, 0, 1, 0, 0, 0);
	}
	
	,clearInvalid:function(){
		this.df.clearInvalid();
		this.tf.clearInvalid();
	}
	
	,markInvalid:function(msg){
		this.df.markInvalid(msg);
		this.tf.markInvalid(msg);
	}
	
	,beforeDestroy:function() {
		if(this.isRendered) {
			// this.removeAllListeners();
			this.wrap.removeAllListeners();
			this.wrap.remove();
			this.tableEl.remove();
			this.df.destroy();
			this.tf.destroy();
		}
	}
	
	,disable:function() {
		if(this.isRendered) {
			this.df.disabled = this.disabled;
			this.df.onDisable();
			this.tf.onDisable();
		}
		this.disabled = true;
		this.df.disabled = true;
		this.tf.disabled = true;
		// this.getEl().up('.x-form-item', 10).addClass('x-item-disabled');
		this.fireEvent("disable", this);
		return this;
	}
	
	,enable:function() {
		if(this.rendered){
			this.df.onEnable();
			this.tf.onEnable();
		}
		this.disabled = false;
		this.df.disabled = false;
		this.tf.disabled = false;
		// this.getEl().up('.x-form-item', 10).removeClass('x-item-disabled');
		this.fireEvent("enable", this);
		return this;
	}
	
	,focus:function(){ this.df.focus(); }
	
	,getPositionEl:function(){ return this.wrap; }
	
	,getResizeEl:function(){ return this.wrap; }
	
	,getValue:function(){ return this.dateValue ? new Date(this.dateValue) : ''; }
	
	,isValid:function(){ return this.df.isValid() && this.tf.isValid(); }
	
	,isVisible:function(){ return this.df.rendered && this.df.getActionEl().isVisible(); }
	
	,onBlur:function(f) {	// called by both DateField and TimeField blur events
		if(this.wrapClick) {	// revert focus to previous field if clicked in between
			f.focus();
			this.wrapClick = false;
		}
		// update underlying value
		if(f === this.df) this.updateDate();
		else this.updateTime();
		this.updateHidden();
		this.validate();
		// fire events later
		(function() {
			if(!this.df.hasFocus && !this.tf.hasFocus) {
				var v = this.getValue();
				if(String(v)!==String(this.startValue)) this.fireEvent("change", this, v, this.startValue);
				this.hasFocus = false;
				this.fireEvent('blur', this);
			}
		}).defer(100, this);
	}
	
	,onFocus:function() {
		if(!this.hasFocus){
			this.hasFocus = true;
			this.startValue = this.getValue();
			this.fireEvent("focus", this);
		}
	}
	
	,onMouseDown:function(e) {	// Just to prevent blur event when clicked in the middle of fields
		if(!this.disabled) { this.wrapClick = 'td'===e.target.nodeName.toLowerCase(); }
	}

	,onSpecialKey:function(t, e) {	// Handles Tab and Shift-Tab events
		var key = e.getKey();
		if(key === e.TAB) {
			if(t === this.df && !e.shiftKey) {
				e.stopEvent();
				this.tf.focus();
			}
			if(t === this.tf && e.shiftKey) {
				e.stopEvent();
				this.df.focus();
			}
			this.updateValue();
		}
		// otherwise it misbehaves in editor grid
		if(key === e.ENTER) this.updateValue();
	}
	
	,reset:function() {
		this.df.setValue(this.originalValue);
		this.tf.setValue(this.originalValue);
	}
	
	,setDate:function(date){ this.df.setValue(date); }
	
	,setTime:function(date){ this.tf.setValue(date); }
	
	,setSize:function(w, h) {	// Sets correct sizes of underlying DateField and TimeField With workarounds for IE bugs
		if(!w) return;
		if('below' === this.timePosition) {
			this.df.setSize(w, h);
			this.tf.setSize(w, h);
			if(Ext.isIE) {
				this.df.el.up('td').setWidth(w);
				this.tf.el.up('td').setWidth(w);
			}
		}else {
			this.df.setSize(w - this.timeWidth - 4, h);
			this.tf.setSize(this.timeWidth, h);

			if(Ext.isIE) {
				this.df.el.up('td').setWidth(w - this.timeWidth - 4);
				this.tf.el.up('td').setWidth(this.timeWidth);
			}
		}
	}
	
	,setValue:function(val) {
		if(!val && true === this.emptyToNow) {
			this.setValue(new Date());
			return;
		}
		else if(!val) {
			this.setDate('');
			this.setTime('');
			this.updateValue();
			return;
		}
		if ('number' === typeof val) {
		  val = new Date(val);
		}
		else if('string' === typeof val && this.hiddenFormat) {
			val = Date.parseDate(val, this.hiddenFormat);
		}
		val = val ? val : new Date(1970, 0 ,1, 0, 0, 0);
		var da;
		if(val instanceof Date) {
			this.setDate(val);
			this.setTime(val);
			this.dateValue = new Date(Ext.isIE ? val.getTime() : val);
		}
		else {
			da = val.split(this.dtSeparator);
			this.setDate(da[0]);
			if(da[1]) {
				if(da[2]) {
					// add am/pm part back to time
					da[1] += da[2];
				}
				this.setTime(da[1]);
			}
		}
		this.updateValue();
	}
	
	,setVisible: function(visible){
		if(visible) {
			this.df.show();
			this.tf.show();
		}else{
			this.df.hide();
			this.tf.hide();
		}
		return this;
	}
	
	,show:function(){ return this.setVisible(true); }
	
	,hide:function(){ return this.setVisible(false); }
	
	,updateDate:function() {
		var d = this.df.getValue();
		if(d) {
			if(!(this.dateValue instanceof Date)) {
				this.initDateValue();
				if(!this.tf.getValue()) this.setTime(this.dateValue);
			}
			this.dateValue.setMonth(0); // because of leap years
			this.dateValue.setFullYear(d.getFullYear());
			this.dateValue.setMonth(d.getMonth(), d.getDate());
			// this.dateValue.setDate(d.getDate());
		}
		else {
			this.dateValue = '';
			this.setTime('');
		}
	}
	
	,updateTime:function() {
		var t = this.tf.getValue();
		if(t && !(t instanceof Date)) {
			t = Date.parseDate(t, this.tf.format);
		}
		if(t && !this.df.getValue()) {
			this.initDateValue();
			this.setDate(this.dateValue);
		}
		if(this.dateValue instanceof Date) {
			if(t) {
				this.dateValue.setHours(t.getHours());
				this.dateValue.setMinutes(t.getMinutes());
				this.dateValue.setSeconds(t.getSeconds());
			}
			else {
				this.dateValue.setHours(0);
				this.dateValue.setMinutes(0);
				this.dateValue.setSeconds(0);
			}
		}
	}
	
	,updateHidden:function() {
		if(this.isRendered) {
			var value = this.dateValue instanceof Date ? this.dateValue.format(this.hiddenFormat) : '';
			this.el.dom.value = value;
		}
	}

	,updateValue:function() {
		this.updateDate();
		this.updateTime();
		this.updateHidden();
		return;
	}
	
	,validate:function() {
		return this.df.validate() && this.tf.validate();
	}
	
	,renderer: function(field) {
		var format = field.editor.dateFormat || Ext.ux.form.DateTime.prototype.dateFormat;
		format += ' ' + (field.editor.timeFormat || Ext.ux.form.DateTime.prototype.timeFormat);
		var renderer = function(val) {
			var retval = Ext.util.Format.date(val, format);
			return retval;
		};
		return renderer;
	}
});
Ext.reg('xdatetime', Ext.ux.form.DateTime);	// register xtype

*/