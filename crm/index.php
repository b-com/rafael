<?
if (function_exists('sys_getloadavg')) {
	$loadavg = sys_getloadavg();
	if (
		($loadavg[0]>3 AND ($_REQUEST['m']=='tasks/alerts_f' OR $_REQUEST['m']=='cars/search'))
		OR $loadavg[0]>7
	) { header('HTTP/1.1 503 Too busy, try again later'); exit; }
}

$startTime=array_sum(explode(' ',microtime()));
set_time_limit(60);
ini_set('memory_limit', '50M');
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", "On");
$CFG['show_errors']=1;


include_once "../inc/common.php";
include_once "../inc/cfg.php";

set_error_handler("php_error_handler", E_ALL);

// include_once "inc/skin.php";
if ($ln) include_once "../inc/lang/$ln.php";

remove_magic_quotes();

$m = $_REQUEST['m'];
$f = $_REQUEST['f'];
$a = $_REQUEST['a'];
//$p = $_REQUEST['p'];

check_login();

if (!$m AND !isset($_GET['mainmenu'])) $m='home';
elseif (eregi("^admin/", $m) AND !$SVARS['is_admin']) $m='';

# set display_errors on/off
if ($SVARS['is_admin'] OR eregi('\.loc$', $_SERVER['HTTP_HOST'])) {
	$CFG['show_errors']=1;
	ini_set("display_errors", "On");
}else {
	ini_set("display_errors", "Off");
}

if (!$_REQUEST['NOHD']) send_headers();

# enable gzip compression
if (!$_REQUEST['NOGZ'] AND !$_REQUEST['NOHD'] AND !headers_sent() AND extension_loaded('zlib') AND function_exists('ob_gzhandler')) ob_start('ob_gzhandler');

if ($SVARS['user'] OR ($m=='users/users' AND $f=='login')) {
	if ($m AND is_file("modules/$m.php")) include_once "modules/$m.php";	// Run Module
	elseif ($m AND is_dir("modules/$m") && file_exists("modules/$m/$m.php")) include_once("modules/$m/$m.php");
	else include "desktop.php";	// Desktop
}else {
	header("Location: /");
	exit;
}
// include_once "../inc/cfg_vars.php";

?>