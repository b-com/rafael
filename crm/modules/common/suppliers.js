
IndexBar_SuppliersGrid = Ext.extend(Ext.grid.EditorGridPanel, {

	init_toolbar: function() {
		
		this.store = new Ext.data.JsonStore({
			url:'?m=common/suppliers&f=suppliers_shortList',
			params:{deleted:'0'},
			totalProperty:'total',	
			root:'data',
			fields: this.fields,
			remoteSort: true
		});
	
	//-------delete function--------------------
		this.delItem = function(){
		
			if (this.sRow!==null) this.selModel.selectRow(this.sRow); // content menu
			if (!this.getSelectionModel().getCount()) return;
			var records = this.getSelectionModel().getSelections();
			

			Ext.MessageBox.show({
				width:250, 
				title:'מחיקת תוכן', 
				msg:'האם ברצונך למחוק את השור'+(records.length===1?'ה':'ות המסומנות')+'?', 
				buttons:Ext.MessageBox.YESNO, scope:this,
				fn: function(btn) {
					 
					if(btn==='yes') {

						var ids=[];
						var passport;
						Ext.each(records, function(r){
							ids.push(r.data.id);
							passport=r.data.passport;
						});
						this.getEl().mask('...טוען');
						
						Ext.Ajax.request({
							url:'?m=common/suppliers&f=del_suppliers',
							params:{passport:passport, ids:ids},
							success: function(r,o){
								d=Ext.decode(r.responseText);
								if (!ajRqErrors(d)) {
									Ext.each(records, function(d){
										row=this.store.indexOf(d);
										this.selModel.selectRow(row);
										this.view.getRow(row).style.border='1px solid red';
										Ext.fly(this.view.getRow(row)).fadeOut({easing:'backIn', duration:1, remove:false, callback:function(){this.store.remove(this.selModel.getSelected());}, scope:this });
									},this);

								}else
									Ext.Msg.confirm('לקוח נמחק בהצלחה!');
								
								this.getEl().unmask();
								
								
							},
							failure: function(r){ this.getEl().unmask(); Ext.Msg.alert('Error', 'Request failed'); },
							scope: this
						});
						if (this.php_class=='ACC_class') {
										this.cardsGrid.store.reload()
										this.cardsGrid.class_id_store.reload()
									}
					}
				}
			});
		};
		
		this.editItem = function(){
						if (this.sRow!==null) this.selModel.selectRow(this.sRow); // content menu
						if (!this.getSelectionModel().getCount()) return;
						// var records = this.getSelectionModel().getSelections();
					
						var record = this.selModel.getSelected();
						this.plugins.startEditing(record);
		}
		
	//-------end delete function--------------------
	
	//-------print function-------------------------
		this.printList = function() {
			this.printExportList('print_all')
		};
		
		this.exportExcel = function() {
			this.printExportList('export_excel')
		};	
		
		this.printExportList = function(method){
		url='/?m=config/cards_class&class='+this.php_class+'&method='+method;
		window.open(url, '', 'toolbar=0,resizable=1,location=0,status=0,scrollbars=1');
		};	
	//-------end print function--------------------
	
	//-------add new button function--------------------

	this.onAdd = function (btn, ev) {
        var u = new this.store.recordType({});
        this.plugins.stopEditing();
        this.store.insert(0, u);
        this.plugins.startEditing(0);
    };
	//------- end add new button function--------------------
	
	this.plugins = new Ext.ux.grid.RowEditor({
		errorSummary:false,
		cls:'rtl',
		parent:this,
		listeners:{

					canceledit:function(o, a){
						try{
							var store = this.parent.getStore();
							var rec = store.getAt(0);
							if(!rec.data.id){
								store.remove(rec);
								this.parent.getView().refresh();
								} 
							}
						catch(e){}
					},
				 	beforeedit:function(o, rowIndex){
						var g = this.parent, view = g.getView(),
							row = view.getRow(rowIndex),
							record = g.store.getAt(rowIndex);
						
						if (record.data.group_id==0){
							g.getColumnModel().getCellEditor(0,rowIndex ).field.disable();
							if (this.parent.php_class=='ACC_card')
								g.getColumnModel().getCellEditor(2,rowIndex ).field.disable();

						}else {
							g.getColumnModel().getCellEditor(0,rowIndex ).field.enable();
							if (this.parent.php_class=='ACC_card')
								g.getColumnModel().getCellEditor(2,rowIndex ).field.enable();
						}
					}, 
					afteredit:function(o, d, rec, i){
					
						if (rec.data.id) {var method='update';} else {var method='add';}
						Ext.Ajax.request({
							url: '?m=common/suppliers&f=add_supplier',
							params:rec.data,
							success: function(r,o){
							r = Ext.decode(r.responseText);
							if(ajRqErrors(r)){ 
									//Error need alert and rollback changes
									Ext.Msg.alert('קרתה תקלה, נא נסה שנית');
									if (rec.data.id) {
										this.parent.getStore().reload();
									}
									else {	
										this.parent.getStore().removeAt(i);
										this.parent.getView().refresh();
									}
								} 
								else{
									if (rec.data.id) {
									rec.commit();
									//	this.parent.getStore().reload(); 
									//	this.parent.getView().refresh();
									}
									else{ //it is new
										this.parent.getStore().reload(); 
										this.parent.getView().refresh();
										
									}
									if (this.parent.php_class=='ACC_class') {
										this.parent.cardsGrid.store.reload()
										this.parent.cardsGrid.class_id_store.reload()
									}
								}
							},
							failure: function(r){Ext.Msg.alert('Error', 'Request failed.'); },
							scope: this
						});
					}
		},scope:this
		
	}),
	this.hlStr = hlStr;
	this.searchField = new GridSearchField({store:this.store, width:120});
	this.clientWindow = function(d){ var win=new ClientWindow(this, d); win.show(); win.center();}
	this.selModel = new Ext.grid.RowSelectionModel({singleSelect:true,
					listeners:{
						rowselect: function(sm, rowIndex, rec){
							this.getTopToolbar().find('iconCls','delete')[0].setDisabled(rec.data.group_id==0);
							this.getTopToolbar().find('iconCls','edit')[0].setDisabled(rec.data.group_id==0);
							// this.getTopToolbar().find('iconCls','user_edit')[0].setDisabled(rec.data.group_id==0);
						},
						scope:this
					}
				}),
				
	this.tbar = [
				 { text:'חיפוש', scope:this }, this.searchField,
				 { text:'ספק חדש', iconCls:'add', handler:this.onAdd, scope:this }
				,{ text:'מחיקת ספק',  iconCls:'delete', handler:this.delItem, disabled:true, scope:this}
				,{ text:'עריכת ספק',  iconCls:'edit', handler:this.editItem, disabled:true, scope:this}
				// ,{ text:'תצוגה מורחבת',  name:'complete_view', iconCls:'print',handler:this.printList, scope:this }
				// ,{ text:'עדכן', iconCls:'user_edit', disabled:true,
				// 	handler:function(){
				// 		var selectedRow = this.selModel.getSelected();
				// 		console.log(selectedRow);
				// 		// var rowId = this.selModel.getSelected().store.indexOf(this.selModel.getSelection()[0]);
				// 		// console.log(rowID);
				// 		// console.log(this.selModel.getSelected());
				// 		// Dump(this.plugins);
				// 		// var id = this.selModel.getSelected().json;
   	// 					// this.clientWindow(id);
   						
 			// 		},
				// scope:this 
				// }
	];
	 
	this.bbar = new Ext.PagingToolbar({store:this.store,pageSize:50, displayInfo:true});
	}
});


SuppliersGrid= function(d,cfg) {
	this.fields = ['id','client','passport','email','adr0','city0','tel0']; // ,'birthday'];
	// this.php_class = 'ACC_card';

	this.init_toolbar();
		
	this.store.load();
	
SuppliersGrid.superclass.constructor.call(this, {
		// title:'כרטיסיות',
		// php_class: 'ACC_card',
		combo:cfg.combo,
		store:this.store, 
		autoScroll:true,
		width:600,
		height:400, 
		layout:'fit',
		cls:'ltr',
		// region:'west',
		frame:true, 
		border:false, 
		autoExpandColumn:'tel0',
		selModel:this.selModel,
		colModel: new Ext.grid.ColumnModel({
						defaults: {sortable:false, menuDisabled:true},
							columns: [
									// {header:'תאריך לידה', width:90, dataIndex:'birthday',id:'birthday', editor: new Ext.form.TextField({allowBlank:false})},
									{header:'טלפון', width:90, dataIndex:'tel0', id:'tel0', editor: new Ext.form.TextField({})},
									{header:'עיר', width:90, dataIndex:'city0', editor: new Ext.form.TextField({})},
									{header:'כתובת', width:90, dataIndex:'adr0', editor: new Ext.form.TextField({})},
									{header:'דוא"ל', width:120, dataIndex:'email', editor: new Ext.form.TextField({
									validator: function(newEmailAddress)
												{
													var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
													if(!reg.test(newEmailAddress) ) {
													return 'כתובת המייל אינה תקינה';
													} else {
															return true;
													}
												}
									
									})},
									{header:'ת.זהות/ח"פ', width:90, dataIndex:'passport', editor: new Ext.form.TextField({
									validator: function(newPassport)
												{
													
													// var reg = /^[\d]{9}$/;
													// if(reg.test(newPassport)){
														// return true;
													// } else {
															// return 'יש להזין 9 ספרות';
													// }
													
												}
									
									})},
									{header:'שם ספק', width:90, dataIndex:'client', renderer:this.hlStr.createDelegate(this), editor: new Ext.form.TextField({allowBlank:false})}
									// ,{header:'קוד קבוצה', width:75, dataIndex:'class_id', editor: new Ext.form.ComboBox({store:this.class_id_store, displayField:'class_id',allowBlank:false, valueField:'userclass_row_id', autoScroll:'true', mode:'remote', listClass:'rtl', triggerAction:'all', forceSelection:true }), align: 'center'}
							]
						})
	});
			
	//for calling outside this
	this.on('activate',function(){
		this.store.load();  
		
	});
	this.on('rowcontextmenu',function(thisGrid, rowIndex, evtObj){

	evtObj.stopEvent(); //Hide default browser context menu

	thisGrid.getSelectionModel().selectRow(rowIndex);
		if(!thisGrid.rowCtxMenu){ //create static instance of Ext menu
			thisGrid.rowCtxMenu = new Ext.menu.Menu({
				items : {
					text : 'ערוך לקוח',
					handler : function(){
						var record = thisGrid.selModel.getSelected();
						thisGrid.plugins.startEditing(record);
					}
				}
			})
		}
		thisGrid.rowCtxMenu.showAt(evtObj.getXY());
	});
};
Ext.extend(SuppliersGrid, IndexBar_SuppliersGrid);

if (Ms['clients']) 
	Ms['clients'].run = function(cfg){
	var m=Ms['clients'];
	var l=m.launcher;
	var win = Crm.desktop.getWindow(m.id+'-win');
	if(!win){
		var  suppliersGrid = new SuppliersGrid();
		win = Crm.desktop.createWindow({width:600, height:700
			,items:suppliersGrid
		}, m);
	}
		
	win.on('show', function() {
		if (suppliersGrid.store.getCount()>0) suppliersGrid.startEditing(suppliersGrid.store.getCount()-1,0);
	});	
	win.show();
};