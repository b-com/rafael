<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
 	
	// $departs = sql2array("SELECT * FROM departments WHERE isDeleted = 0",'id');
	// $companies = sql2array("SELECT id,name FROM insur_companies WHERE isDeleted = 0",1);
	// $roadCompany = sql2array("SELECT id,name FROM road_serv_companies WHERE isDeleted = 0",1);
	
	if($departs)foreach($departs as $id=>$depart){$Ds.=($Ds ? ',':'')."[$id,'".jsEscape($depart['name'])."','".$depart['permissions']."']";}
	
	#Load & generate Users&Agents lists
	$users_ar=sql2array("SELECT id,  TRIM(CONCAT(fname,' ',lname)) as fullName,  active FROM users ORDER BY fullName",'id');
	
	if ($users_ar) foreach($users_ar as $id=>$user) {
		$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		if ($user['status']=='salesagent' && $user['active']){
			if($SVARS['user']['isAdmin']){
				$salesagent.=($salesagent ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			}
			if($user['department'] == 7 OR $user['department'] == 2)// מכירות שטח,הנהלה ראשית - מפקח 
				$survi.=($survi ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		//if ($user['status']=='salesagent') $salesAgents_ar[]=$user;
	}
	
	if($SVARS['user']['status'] == 'salesagent'){
		$salesagent = "[".$SVARS['user']['id'].",'".jsEscape($SVARS['user']['name'])."']";
		if($SVARS['user']['department'] == 7)
			$survi = "[".$SVARS['user']['id'].",'".jsEscape($SVARS['user']['name'])."']";
	}

	?>
	
	var users=[<?=$users;?>];
	var Departments=[<?=$Ds;?>];
	var companies=<?=array2json($companies)?>;
	var roadsCompanies=<?=array2json($roadCompany)?>;
	var supervisor = [<?=$survi;?>];
	var attendant = [<?=$salesagent;?>];
	var users_obj=<?=array2json($users_ar);?>;
	
	Ext.ux.Loader.load([
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			
			'modules/common/contacts.js?<?=filemtime('modules/common/contacts.js');?>',
			
			'modules/common/files.js?<?=filemtime('modules/common/files.js');?>',
			
			'modules/salesagents/salesagents.js?<?=filemtime('modules/salesagents/salesagents.js');?>'
		], function(){Ms.salesagents.run();}
	);
	
<?}

//————————————————————————————————————————————————————————————————————————————————————
function f_list() {
	
	$where ="WHERE active=".(int)$_POST['active']." AND status='salesagent'";
	
	if ($_POST['q']) {
		$make_where_search=make_where_search('fname,lname,city0,adr0,tel0,tel1,fax,email,notes', $_POST['q']);
		if ($make_where_search) $where.=" AND ($make_where_search)";
	}
	
	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if ($_POST['sort']=='tz')  $order_by='passport';
	elseif ($_POST['sort']=='adr') $order_by='city0';
	elseif ($_POST['sort']=='tel') $order_by='tel0';
	elseif ($_POST['sort']=='fax') $order_by='fax';
	else $order_by='fname, lname';
//$info['email_txt'].=($info['email_txt']?'<br>':'').'עיקרי'.': <a href="mailto:'.$info['email'].'">'.htmlspecialchars($info["email"]).'</a>';
	$sql="SELECT SQL_CALC_FOUND_ROWS u.id,u.fname,u.lname,u.isAdmin,DATE_FORMAT(u.date_added,'%d/%m/%Y') AS date_added,u.tel0,u.tel1,u.fax"
		.",email,dep.name AS department"
		." FROM users AS u LEFT JOIN departments AS dep ON dep.id=u.department $where" 
		." ORDER BY $order_by".($_POST['dir']=='DESC'?' DESC':'')
		." LIMIT $_POST[start],$_POST[limit]";
	;
	
	$rows=sql2array($sql);
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	if ($rowsCount){
	/*	foreach($rows as $k=>$r) {
			if ($r['work_start']) $rows[$k]['work_start']=str2time($r['work_start'],'d/m/Y'); else $rows[$k]['work_start']='';
			//$rows[$k]['modules'] = explode(',',$r['modules']);
		}*/
		echo '{total:"'.$rowsCount.'", data:'.array2json($rows).'}';
	}
	else
		die('{success:false, msg:"רשומות לא נמצאו!"}');
}
//————————————————————————————————————————————————————————————————————————————————————
/*
* check if the fax number a user entered is one of the nati's
*/
function f_faxnumber_check() {
	$fax_in =  $_POST['fax_in'];

	if (sql2array("SELECT * FROM fax_numbers WHERE fax_number IN ($fax_in)"))
		echo '{success:true, msg:"מספר פקס נכנס תקין!"}';
	else 	echo '{success:false, msg:"מספר פקס לא שייך לחברה!"}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_edit() {
 	global $SVARS;
 
	$id=(int)$_POST['id'];

	$sql="users SET"//adminPermissions
		."  fname=   ".quote($_POST['fname'])
		.", lname=   ".quote($_POST['lname'])
		.", username=".quote($_POST['username'])
		.", passport=".quote($_POST['passport'])
		.", tel0=    ".quote($_POST['tel0'])
		.", tel1=    ".quote($_POST['tel1'])
		.", fax=     ".quote($_POST['fax'])
		.", fax_in=  ".quote($_POST['fax_in'])
		.", email=   ".quote($_POST['email'])
		.", notes=   ".quote($_POST['notes'])
		.", status=  'salesagent"
		."',department=".(int)$_POST['department']
		.", sex=    ".(int)$_POST['sex']
		.(isset($_POST['active'])	? ", active=1" : ", active=0")
		.($SVARS['user']['isAdmin'] ? (isset($_POST['isAdmin']) ? ", isAdmin=1" : ", isAdmin=0") : '')
		.(isset($_POST['birthday'])	? ", birthday='".str2time($_POST['birthday'],'Y-m-d')."'":'');
		
	if($_POST['password'])
		$sql.=", password=".quote(md5($_POST['password']));

	if($_POST['mod']){
		$dispatcher_access ='';
		foreach($_POST['mod'] as $k=>$r){
			$modules[$k]= $k;
			if ($k == 48) {
				// פתיחת תת-מחלקות מוקד בפתיחת מודול מוקד
				//TODO: get call center sub departmets from DB
				$dispatcher_access = '3,4,5,10';
			}
			$elementData = json_decode($r);
			for($i = 0; $i < sizeof($elementData); $i++){
				$modElemetsSql .= ($modElemetsSql ? "," : "").("($k,$elementData[$i], {user_id})");
			}
		}
		$callModuleUpdate = true;
		$sql.= ", modules=".quote(implode(',',$modules)).", dispatcher_access='$dispatcher_access' ";
	}
	
	$n=0;
	for ($i=0; $i<3; $i++) if ($_POST["city$i"] OR $_POST["zip$i"] OR $_POST["adr$i"]) {$sql.=", adr_type$n=".(int)$_POST["adr_type$i"].", city$n=".quote($_POST["city$i"]).", zip$n=".quote($_POST["zip$i"]).", adr$n=".quote($_POST["adr$i"]); $n++;}
	for ($i=$n; $i<3; $i++) $sql.=", adr_type$i='', city$i='', zip$i='', adr$i=''";

	$sql = ($id ? "UPDATE $sql WHERE id=$id" : "INSERT INTO ".$sql.=", date_added =".quote(date('Y-m-d h:m:s')));

	if (runsql($sql)) {
		if(!$id) $id = mysql_insert_id();
		else{
			// Delete all previous elements permissions on modules
			if($callModuleUpdate) runsql("DELETE FROM user_access_permissions WHERE user_id = $id");
		}
		//$name = jsEscape(trim($_POST['lname']));
		if($modElemetsSql){
			$modElemetsSql = str_replace('{user_id}', $id, $modElemetsSql);
			runsql("INSERT INTO user_access_permissions (mod_id, element_id, user_id) VALUES ".$modElemetsSql);
		}
		echo '{success:true}'; 
	}else echo '{success:false}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_salesagent_info() {
	
	$id=(int)$_REQUEST['id'];
	
	if (!$id) die('{success:false, msg:"מס ID לא תקין!"}');
	$info=sql2array("SELECT users.*, departments.name AS depart FROM users INNER JOIN departments ON departments.id = users.department"
		." WHERE users.id=".$id." LIMIT 1",'','',1);
	
	if ($info["email"]){
		$info['email_txt'].=($info['email_txt']?'<br>':'').'עיקרי'.': <a href=# onClick="callEmailCompose(\''.$info["email"].'\')">'.htmlspecialchars($info["email"]).'</a>';
	//	$info['email_txt'].=($info['email_txt']?'<br>':'').'עיקרי'.': <a href=# onClick="sendEmail('.$id.",'$info[email]'".')">'.htmlspecialchars($info["email"]).'</a>';
	}
	
	$addrTypes = array(1=>'בית',2=>'חברה/עסק',3=>'עבודה',4=>'למשלוח דואר',0=>'אחר');
	
	for ($i=0; $i<2; $i++){
		if ($info["tel$i"]){
			$info['phone_txt'].=($info['phone_txt']?' , ':'').htmlspecialchars($info["tel$i"]);
		}
	}
	
	$info['adr_txt'] =($info['adr_txt']?' ; ':'').$addrTypes[$info["adr_type0"]].': '.htmlspecialchars($info["adr0"]).','.htmlspecialchars($info["city0"]);
	
	if($info){
		//$info['modules'] = explode(',',$info['modules']);
		$info['modules'] = array_fill_keys(explode(',',$info['modules']), array());
		if(!empty($info['modules'])){
			$elementsArray = sql2array("SELECT mod_id, element_id FROM user_access_permissions WHERE user_id = $id");
			if(is_array($elementsArray))
				foreach ($elementsArray as $k=>$v){
					if(!is_array($info['modules'][$v['mod_id']])) $info['modules'][$v['mod_id']] = array();
					$info['modules'][$v['mod_id']][] = $v['element_id'];
				}
		}
		if ($info['birthday']) $info['birthday']=str2time($info['birthday'],'d/m/Y'); else $info['birthday']='';
		unset($info['password']);
		echo '{success:true, data:'.array2json($info).'}';
	}
	else
		die('{success:false, msg:"תקלה!"}');
}

?>
