<?
if (eregi(basename(__FILE__), $_SERVER['PHP_SELF'])) { header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }

if (function_exists("f_{$f}")) call_user_func("f_{$f}"); else f_init();

//————————————————————————————————————————————————————————————————————————————————————
function f_init() {
 global $CFG, $SVARS;
 
 	$ln='he';
	
	$modules=sql2array("SELECT * FROM modules WHERE active=1 AND parent_id=0 ORDER BY id",'','',0,'rafael');
	
	if ($modules)  foreach($modules as $m) {
		$Ms[]=array('defaults'=>"{baseCls:'x-plain', cls:'rtl'}",
			'items'=>array(
				array(
					'html'=>"<div class='".($m['shortcutIconCls']? $m['shortcutIconCls']: 'demo-grid-shortcut')."' style='width:100%; height:48px; background-position:center top; background-repeat:no-repeat;' onClick='Ext.getCmp(\"mod$m[id]\").setValue(!Ext.getCmp(\"mod$m[id]\").getValue());'></div>",
					'style'=>'text-align:center;','baseCls'=>'x-plain','cls'=>'rtl'
				),
				array('xtype'=>'checkbox', 'checked'=>false, 'boxLabel'=>$m["title_$ln"],	'name'=>"mod[$m[id]]")
			)
		);
	}
	echo "var Modules=".array2json($Ms).';';
	
	
	$users_ar=sql2array("SELECT id, is_agent, TRIM(CONCAT(fname,' ',lname)) as fullName, work_end FROM users ORDER BY fullName",'id');
	
	if ($users_ar) foreach($users_ar as $id=>$user) {
		if ($user['work_end']=='0000-00-00' OR $user['work_end']>date('Y-m-d')) {
			$users.=($users ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
			if ($user['is_agent']==1) $agents.=($agents ? ',':'')."[$id,'".jsEscape($user['fullName'])."']";
		}
		if ($user['is_agent']==1) $agents_ar[]=$user;

	}
	?>
	
	var smsActive=<?=(($SVARS['sms_active'] OR $SVARS['sms_credits']<0) ? 'true' : 'false')?>;
	var user={user_id:<?=$SVARS['user']['id']?>}; 
	var users=[<?=$users;?>];
	var agents=[<?=$agents;?>];
	var users_obj=<?=array2json($users_ar);?>;
	
	Ext.ux.Loader.load([
			'skin/aj.js?<?=filemtime('skin/aj.js');?>',
			'modules/common/common.js?<?=filemtime('modules/common/common.js');?>',
			'skin/extjs/ux/superboxselect/superboxselect.css?<?=filemtime('skin/extjs/ux/superboxselect/superboxselect.css');?>',
			'skin/extjs/ux/superboxselect/SuperBoxSelect.js?<?=filemtime('skin/extjs/ux/superboxselect/SuperBoxSelect.js');?>',
			'modules/common/contacts.js?<?=filemtime('modules/common/contacts.js');?>',
			'modules/tasks/tasks.js?<?=filemtime('modules/tasks/tasks.js');?>'
		], function(){Ms.tasks.run();}
	);
	<?
}

//————————————————————————————————————————————————————————————————————————————————————
function f_tasks_list() {
 global $SVARS;
 	$_POST['limit'] = (int)$_POST['limit'] ? (int)$_POST['limit'] : 50;
	$_POST['start'] = (int)$_POST['start'] ? (int)$_POST['start'] : 0 ;
	
	if ($_POST['q']) $make_where_search=make_where_search('name,tasks.notes,fname,lname', $_POST['q']);
	
	if ($_REQUEST['sort']) $_REQUEST['sort'] = sql_escape($_REQUEST['sort']) . ($_REQUEST['dir']=='DESC' ? ' DESC' : '');
	else { $_REQUEST['sort']='end'; $_REQUEST['dir']='DESC'; }

	$sql=	"SELECT SQL_CALC_FOUND_ROWS tasks.*, TRIM(CONCAT(clients.lname,' ',clients.fname)) AS client_name,clients.fname, clients.lname "
			."FROM tasks "
			."LEFT JOIN clients ON clients.id=tasks.client_id "
			."WHERE (tasks.owner_id={$SVARS['user']['id']} OR tasks.user_id={$SVARS['user']['id']} OR tasks.user_id=0) "
			.($_REQUEST['status']==-1 ? "AND tasks.deleted=1 " : "AND tasks.deleted=0 ")
			.($_REQUEST['status']>0 ? "AND tasks.status=$_REQUEST[status] " : "")
			// .($agent_id ? "AND crm_claims.agent_id=$agent_id " : '')
			.($make_where_search ? "AND $make_where_search " : '')
			.($_REQUEST['sort'] ? "ORDER BY $_REQUEST[sort]" : '')
		  	.(($_GET['print'] OR $_GET['export'])?'':" LIMIT $_POST[start],$_POST[limit]")
			;
	 // echo $sql;		
	$rows=sql2array($sql);
	
	$rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);
	
	if ($rows) foreach($rows as $k=>$r) {
		$rows[$k]['start']=($r['start']=='0000-00-00 00:00:00'?'':str2time($r['start'],'d/m/Y H:i'));
		$rows[$k]['end']=($r['end']=='0000-00-00 00:00:00'?'':str2time($r['end'],'d/m/Y H:i'));
		$rows[$k]['remind']=($r['remind']=='0000-00-00 00:00:00'?'':str2time($r['remind'],'d/m/Y H:i'));
	}
	
	echo "{success:true, total:$rowsCount, data:".($rows ? array2json($rows):'[]').'}';
}
//————————————————————————————————————————————————————————————————————————————————————
function f_add_task() {
 global $SVARS, $prms_groups;
	$taskStatus = array(1=>'טרם בוצע',2=>'בוצע',3=>'הודעה בלבד');

	$id=(int)$_POST['id'];
	$_REQUEST['user_id']=(int)$_REQUEST['user_id'];
	if ($id) $sql = "UPDATE tasks SET user_id=".(int)$_REQUEST['user_id'].", name=".quote($_REQUEST['name']).", status=".(int)$_REQUEST['status'].", importance=".(int)$_REQUEST['importance'].", sms='".sql_escape($_REQUEST['sms'])."', end='".str2time($_REQUEST['end'],'Y-m-d H:i:s')."', remind='".str2time($_REQUEST['remind'],'Y-m-d H:i:s')."', notes='$notes' WHERE id=$id";
	else	 $sql = "INSERT INTO tasks SET owner_id=".(int)$SVARS['user']['id'].", user_id=".(int)$_REQUEST['user_id'].", client_id=".(int)$_REQUEST['client_id'].", client_type='".sql_escape($_REQUEST['client_type'])."', link='".sql_escape($_REQUEST['link'])."', name=".quote($_REQUEST['name']).", status=".(int)$_REQUEST['status'].", importance=".(int)$_REQUEST['importance'].", sms='".sql_escape($_REQUEST['sms'])."', start=NOW(), end='".str2time($_REQUEST['end'],'Y-m-d H:i:s')."', remind='".str2time($_REQUEST['remind'],'Y-m-d H:i:s')."', notes='$notes'";
	// echo($sql);
	runsql($sql); 
	$taskID = ($id ? $id : mysql_insert_id());
	if ($taskID) {
		if ($id) {
			$notes='';
			if ($_REQUEST['_status']) {
				$notes.=($notes?'; ':'')."סטטוס ".$taskStatus[$_REQUEST['_status']];
				
				$message =  "$SVARS[fullname] שינה סטאטוס משימה: '$name' ל".$taskStatus[$_REQUEST['_status']];
				
				if ($_REQUEST['sms'] AND $SVARS['sms_active'] AND $tel=ereg_replace('[^0-9]+','',$_REQUEST['sms']) AND ereg('^(0|972)(5[024][0-9]{7})$', $tel, $regs)) {
					$phone='972'.$regs[2];
					include_once 'inc/sms.php';
					$send_sms = send_sms($phone, $message, $SVARS['sms_originator']);
				}
			}
			if ($_REQUEST['_name']) $notes.=($notes?'; ':'')."שם המשימה ".$_REQUEST['_name'];
			if ($_REQUEST['_importance']) $notes.=($notes?'; ':'')."עדיפות ".($_REQUEST['_importance']==2 ? 'דחופה':($_REQUEST['_importance']==2 ?'חשובה':'רגילה'));
			if ($_REQUEST['_end']) $notes.=($notes?'; ':'')."יעד סיום ".$_REQUEST['_end'];
			if ($_REQUEST['_remind']) $notes.=($notes?'; ':'')."תזכורת לביצוע ".$_REQUEST['_remind'];
			if ($_REQUEST['_notes']) $notes.=($notes?'; ':'')."תאור ".$_REQUEST['_notes'];
			if (isset($_REQUEST['_sms'])) $notes.=($notes?'; ':'')."שליחת התראות ".(($_REQUEST['_sms']  AND $tel=ereg_replace('[^0-9]+','',$_REQUEST['_sms']) AND ereg('^(0|972)(5[024][0-9]{7})$', $tel, $regs))?'הופעלה ל-'.$tel:'בוטלה');
			
			if ($notes) $notes="עודכן ".$notes;
			
			if ($_REQUEST['_user_id']) $notes.=($notes?'; ':'')."המשימה הועברה ל-".$_REQUEST['user_name'];

		}else $notes = "המשימה הוקמה ".($SVARS['user_id']!=$_REQUEST['user_id']?' לביצוע ע"י '.sql_escape($_REQUEST['user_name']):'');
		$sql = "INSERT INTO task_actions SET task_id=$taskID, user_id={$SVARS['user']['id']}, date=NOW(), notes=".quote($notes);
		if (runsql($sql)) echo "{success:true, id:$taskID}"; 
		else {runsql("DELETE FROM tasks WHERE id=$taskID"); echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";}
	}else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_del_task() {
	$id = (int)$_REQUEST['id'] ;
	if (!$id) die('{success:false, msg:"TaskID error"}');
	$sql = "UPDATE tasks SET deleted=1 WHERE id=$id";
	if (runsql($sql) AND mysql_affected_rows()) echo "{success:true}"; else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_rest_task() {
	$id = (int)$_REQUEST['id'];
	if (!$id) die('{success:false, msg:"TaskID error"}');
	$sql = "UPDATE tasks SET deleted=0 WHERE id=$id";
	if (runsql($sql) AND mysql_affected_rows()) echo "{success:true}"; else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_actions_list() {
 	$task_id = (int)$_REQUEST['task_id'] ;
	if (!$task_id) die('{success:false, msg:"TaskID error"}');
	$sql="SELECT task_actions.*, DATE_FORMAT(task_actions.date,'%d/%m/%Y %H:%i') AS date, TRIM(CONCAT(clients.name, ' ', clients.lname, ' ', clients.fname)) AS name FROM task_actions LEFT JOIN clients ON clients.id=task_actions.client_id WHERE task_id=$task_id ORDER BY id DESC";
	$rows=sql2array($sql);
	echo "{success:true, data:".($rows ? array2json($rows):'[]').'}';
}

//————————————————————————————————————————————————————————————————————————————————————
function f_add_action() {
 global $SVARS;
	$taskID = (int)$_REQUEST['task_id'];
	if (!$taskID) die('{success:false, msg:"TaskID error"}');

	$sql = "INSERT INTO task_actions SET task_id=$taskID, user_id={$SVARS['user']['id']}, client_id='".(int)$_POST['client_id']."', date=NOW(), notes=".quote($_REQUEST['notes']);

	if (runsql($sql)) echo "{success:true}"; else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
}

//————————————————————————————————————————————————————————————————————————————————————
function f_change_user() {
 global $SVARS;
	$taskID = (int)$_REQUEST['task_id'];
	$userID = (int)$_REQUEST['user_id'];
	if (!$taskID OR !$userID) die('{success:false, msg:"TaskID or UserID error"}');
	$notes="המשימה הועברה ל-".$_REQUEST['user_name'];
	
	if (runsql("UPDATE tasks SET user_id=$userID WHERE id=$taskID")) {
		$sql = "INSERT INTO task_actions SET task_id=$taskID, user_id={$SVARS['user']['id']}, date=NOW(), notes=".quote($notes);
		if (runsql($sql)) echo "{success:true}"; else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
	}  else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";

}
//————————————————————————————————————————————————————————————————————————————————————
function f_change_status() {
 global $SVARS;
 	$taskStatus = array(1=>'טרם בוצע',2=>'בוצע',3=>'הודעה בלבד');
	$taskID = (int)$_REQUEST['task_id'];
	if (!$taskID) die('{success:false, msg:"TaskID or UserID error"}');
	$notes = "$SVARS[fullname] שינה סטאטוס משימה ל".$taskStatus[$_REQUEST['status']];
	if (runsql("UPDATE tasks SET status=$_REQUEST[status] WHERE id=$taskID")) {
		$sql = "INSERT INTO task_actions SET task_id=$taskID, user_id={$SVARS['user']['id']}, date=NOW(), notes='$notes'";
		if (runsql($sql)) echo "{success:true}"; else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";
	}  else echo "{success:false, msg:'תקלת מערכת. אנא נסו שנית.'}";

}
?>