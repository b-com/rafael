<?

//$V=substr(phpversion(), 0, 1);
//include_once "php$V/SMTP$V.php";
//include_once "php5/SMTP5.php";

//覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧�
function send_mail($to, $subject, $message, $from='', $headers='', $attachment='') {
  global $CFG;
  if (!$from) {
		if ($CFG['mail']['from']) $from=$CFG['mail']['from'];
		else {	# set default from adress to 'contact@thisdomain'
			eregi("(www\.)?([a-z0-9\.-]+)", $_SERVER['HTTP_HOST'], $host_ar);	# get current domain name without www
			$from="contact@$host_ar[2]";
		}
	}

	# split $from to name and address
	if (eregi("^([^<]+)<([^>]+)>", $from, $regs)) $from=array('from'=>trim($regs[2]), 'name'=>trim($regs[1]));
	else $from=array('from'=>$from, 'name'=>'');
	
	# split $to address
	if (!$to_ar=split('[,;]', $to)) $to_ar[0]=$to;
	foreach ($to_ar as $key=>$row) {
		if (ereg("^([^<]+)<([^>]+)>", $row, $regs)) {
			$to_ar[$key]=array('to'=>trim($regs[2]), 'name'=>trim($regs[1]));
		}else $to_ar[$key]=array('to'=>trim($row), 'name'=>'');
	}
	
	$charset="windows-1255";	# set default charset
	if (!$CFG['smtp_delivery']) $CFG['smtp_delivery']='local';

	set_time_limit(120);
	
	define('DISPLAY_XPM4_ERRORS', true); // display errors

	$V=substr(phpversion(), 0, 1);	# check PHP version
	require_once "php$V/MAIL$V.php";
	if ($V==5) $m = new MAIL5;
	else $m = new MAIL4;

	$m->Name('localhost');
	$m->From($from['from'], $from['name'], $charset);
	$m->Subject($subject, $charset);
	if ($message) $m->Html($message, $charset);
	if (is_array($headers))	foreach ($headers as $header) $m->addheader($header['name'], $header['value'], $charset);

	if ($attachment AND file_exists($attachment['path']))	$m->attach(file_get_contents($attachment['path']), 'ATTACH_FILE', $attachment['name']);
	
	foreach ($to_ar as $key=>$row) $m->AddTo($row['to'], $row['name'], $charset);

	if ($CFG['mail']['delivery']=='relay' AND $CFG['mail']['host'] AND $CFG['mail']['user'] AND $CFG['mail']['pass']) {	// connect to MTA server
		if (!$CFG['mail']['port']) $CFG['mail']['port']=25; # default smtp port
		$c = $m->Connect($CFG['mail']['host'], $CFG['mail']['port'], $CFG['mail']['user'], $CFG['mail']['pass']);
		if (!$c) { $GLOBALS['send_mail_result']=$m->Result; return false; }
		$sent = $m->Send($c);
		$m->Disconnect(); 
		$GLOBALS['send_mail_result']=$m->Result;
	
	}elseif ($CFG['mail']['delivery']=='client') {
		$sent = $m->Send('client');
		$GLOBALS['send_mail_result']=$m->Result;
	
	}else { # local
		$sent = $m->Send('local');
		$GLOBALS['send_mail_result']=$m->Result;
	}
 return $sent;
}

?>