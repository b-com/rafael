<?
if (stripos($_SERVER['PHP_SELF'], basename(__FILE__)) OR !$SVARS){ header("HTTP/1.0 404 Not Found"); die("<HTML><HEAD><TITLE>404 Not Found</TITLE></HEAD><BODY><H1>Not Found</H1>The requested URL $_SERVER[REQUEST_URI] was not found on this server.</BODY></HTML>"); }
$NO_SKIN=1;

if ($SVARS['user_type']==10 OR $SVARS['user_type']==3) $is_adm=1;


if (!$_REQUEST['client_id']) die('{success:false, msg:"bad client_id"}');

if (function_exists("f_{$f}")) call_user_func("f_{$f}");
else die('{success:false, msg:"function name error"}');
//————————————————————————————————————————————————————————————————————————————————————
function f_files_tree() {
 global $files, $folders, $links;
	
	$_REQUEST['limit'] = (int)$_REQUEST['limit'] ? (int)$_REQUEST['limit'] : 500;
	$_REQUEST['start'] = (int)$_REQUEST['start'] ? (int)$_REQUEST['start'] : 0 ;

	$client_type=$_REQUEST['client_type'];
	if(!$client_type) $client_type='client';
	
	if ($_GET['sort']=='date')  $order_by='date_added';
	elseif ($_GET['sort']=='user') $order_by='user';
	elseif ($_GET['sort']=='size') $order_by='file_size';
	elseif ($_GET['sort']=='notes') $order_by='notes';
	// elseif ($_GET['sort']=='linkText') $order_by='link';
	else $order_by='file_name';
	
	if($_REQUEST['link']){
		$link=(int)$_REQUEST['link'];
		$link_type=sql_escape("file-$_REQUEST[link_type]");
		$rows=sql2array("SELECT links.* FROM links WHERE id2=$_REQUEST[link] AND type LIKE '$link_type'");
		
		if ($rows) {
			foreach ($rows as $key=>$row){
				$ids.=($ids?',':'').$row['id1'];
			}
		}
		if($ids) $where=" WHERE files.group_id=$group_id AND files.id IN ($ids) "; else $where=" WHERE false ";

	} else $where=" WHERE files.client_id=$_REQUEST[client_id] AND client_type='$client_type' ";

	$sql="SELECT SQL_CALC_FOUND_ROWS files.*, users.fname, users.lname, users.username ".($client_type=='contact'?',user_phones.email AS contact_email ':'')
		.($client_type=='employee'?",employees.passport AS employee_passport, TRIM(CONCAT(employees.fname,' ',employees.lname)) AS employee_name ":'')
		.($client_type=='client'?",crm_clients.passport AS client_passport, TRIM(CONCAT(crm_clients.first_name,' ',crm_clients.first_name)) AS client_name ":'')
		."FROM files " 
		."LEFT JOIN $CFG[prefix]users ON $CFG[prefix]users.id=$CFG[prefix]files.user_id "
		.($client_type=='contact'?"LEFT JOIN $CFG[prefix]user_phones ON $CFG[prefix]user_phones.id=$CFG[prefix]files.client_id ":'')
		.($client_type=='client'?"LEFT JOIN crm_clients ON crm_clients.client_id=files.client_id ":'')
		.($client_type=='employee'?"LEFT JOIN employees ON employees.id=files.client_id ":'')
		.$where
		." ORDER BY file_cat, $order_by".($_REQUEST['dir']=='DESC'?' DESC ':' ')
		." LIMIT $_REQUEST[start],$_REQUEST[limit]";
		
	$files=sql2array($sql);
	$rowsCount=count($files);
	if ($rowsCount==$_REQUEST['limit']) $rowsCount=sql2array("SELECT FOUND_ROWS() AS count", false, 'count', true);

	if (!$files) die("{total:0, data:[]}");
	
	if ($files) foreach ($files as $k=>$r) {
		if (trim($r['file_cat'])==='') $r['file_cat']='ככלי';
		
		$r['user']=("$r[fname]$r[lname]" ? "$r[fname] $r[lname]" : $r['username']);
		$C[$k]['id']	= $r['id'];
		$C[$k]['folder']= $r['file_cat'];
		$C[$k]['name']	= $r['file_name'];
		// $C[$k]['link']	= $r['link'];
		$C[$k]['linkText'] = ($r['link'] ? 'שיוך בוצע':0);
		$C[$k]['date']	= str2time($r['date_added'], 'd/m/y H:i');
		$C[$k]['size']	= ( $r['file_size'] ? (($r['file_size']>1024000) ? number_format($r['file_size']/1024) : round($r['file_size']/1024,2) ).' kB' : '' );
		$C[$k]['user']	= $r['user'];
		$C[$k]['notes']	= $r['notes'];
		$C[$k]['contact_email']	= $r['contact_email'];
		if($r['client_type']=='employee'){
			$C[$k]['client_name']	= $r['employee_name'];
			$C[$k]['client_passport']	=$r['employee_passport'];
		}
		else if($r['client_type']=='client'){
			$C[$k]['client_name']	= $r['client_name'];
			$C[$k]['client_passport']	=$r['client_passport'];
		}
	}
	
	echo "/*<div dir=ltr align=left><pre>".print_r($files,1)."</pre></div>*/";

	echo "{total:'$rowsCount',data:".array2json($C)."}";
}
//————————————————————————————————————————————————————————————————————————————————————
function files_json_list($folder) {
 global $files, $folders, $links;
	if (!is_array($files) OR !is_array($folders)) return;
	
	foreach ($folders[$folder] as $file_id) {
		$file=$files[$file_id];
		//print_ar($file,'$file');
		$file['user']="$file[fname] $file[lname] <span dir=ltr>($file[username])</span>";
		
		$file['linkText']='';
		
		$json="{uiProvider:'col',"
			."id:'"			.jsEscape($file['id'])."',"
			//."file_cat:'"	.jsEscape($file['file_cat'])."',"
			."name:'"		.jsEscape($file['file_name'])."',"
			// ."link:'"		.jsEscape($file['link'])."',"
			// ."linkText:'"	.jsEscape($links[$file['link']])."',"
			//."ext:'"		.jsEscape($file['file_ext'])."',"
			."size:'"		.jsEscape( $file['file_size'] ? (($file['file_size']>1024000) ? number_format($file['file_size']/1024) : round($file['file_size']/1024,2) ).' kB' : '' )."',"
			."date:'"		.jsEscape( str2time($file['date_added'],'d/m/y H:i') )."',"
			."user:'"		.jsEscape($file['user'])."',"
			."notes:'"		.jsEscape($file['notes'])."',";	
		//if ($folders[$file_id]) $json.="cls:'folder',children:[".files_json_list($file_id)."]";
		//else 
		$json.="leaf:true";
		
		$json.="}";
		$json_ar[]=$json;
	}
	
 return implode(',',$json_ar);
}
//————————————————————————————————————————————————————————————————————————————————————
function f_upload_dialog() {
?><html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1255"><link rel=stylesheet href="skin/style.css">
<script language='javascript1.3' src='modules/crm/crm_file_jup.js'></script>
</head><body dir=rtl><center>
<script>
showJavaUploader(<?=$_GET['client_id']?>);
//parent.window.showJavaUploader();
//document.writeln(parent.window.javaUploader)
//alert(parent.window.javaUploader);
</script>
</body></html><?
}
//————————————————————————————————————————————————————————————————————————————————————
function f_upload_files() {
 global $CFG, $SVARS, $m, $group_id;
	$client_id=(int)$_GET['client_id'];
	echo"/*$client_id*/";
	$client_type=$_REQUEST['client_type'];
	if(!$client_type) $client_type='client';
	$overwriteId=(int)$_GET['overwriteId'];
	$id=(int)$_GET['id'];
	
	if ($_FILES['RemoteFile']) {	# scan
		$_FILES['file']=&$_FILES['RemoteFile'];
		$_POST['linked_data']  = $_GET['linked_data'];
	}
	
	if ($client_type=='mail') {//Upload to temporary folder for mail
	
		if (!(int)$_REQUEST['timestart']) die('{success:false, msg:"!בעיה בשמירת קובץ"}');
		
		eregi("^(.+)\.([a-z0-9_#!@-]{1,5})$", $_FILES['file']['name'], $regs);	# find file extention
		$file_ext=strtolower($regs[2]);
		$size=(int)$_FILES['file']['size'];
		
		$file_name = $_POST['file_name'] ? $_POST['file_name'].".$file_ext" : $_FILES['file']['name'];

		if (!is_dir("../temp/mail_compose/{$SVARS['user']['id']}/$_REQUEST[timestart]")) mkdir("../temp/mail_compose/{$SVARS['user']['id']}/$_REQUEST[timestart]", 0777, true);

		if (move_uploaded_file($_FILES['file']['tmp_name'], "../temp/mail_compose/$SVARS[user_id]/$_REQUEST[timestart]/$file_name")) {
			if (!$_GET['file_name']) die("{success:true}");
			exit;
		}else die('{success:false, msg:"!בעיה בשמירת קובץ"}');
	}
	
	if (!$client_id) die('{success:false, msg:"No client id"}');

	$_POST['notes'] = trim($_POST['notes']);
	$_POST['folder'] = trim($_POST['folder']);
	$_POST['file_name']= trim($_POST['file_name']);
	
	$linked_data = json_decode($_POST['linked_data']);

	if (!$_POST['folder']) $_POST['folder']='כללי';
	
	if (!$_FILES['file'] OR !$_FILES['file']['size'] OR $_FILES['file']['error']) die('{success:false, msg:"File upload error"}');
	
	if (($_GET['mode']=='append' OR $_GET['mode']=='merge') AND $id) {
		$f=sql2array("SELECT * FROM files WHERE id=$id", 0,0,1);
		if (!$f) die('{success:false, msg:"Error: file not found!"}');
		elseif ($f['file_ext']!='pdf') die('{success:false, msg:"can append only to PDF file"}');
		
		$targetFile = realpath("../crm-files/$f[group_id]/".substr($f['client_id'], -2)."/$f[client_id]/$f[id]".($f['file_ext']?".$f[file_ext]":'') );
		if (!is_file($targetFile)) die('{success:false, msg:"Error: file not found!.."}');
		
		if ($_GET['mode']=='append') {	# save full file
			if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) {
				runsql("UPDATE files SET file_size=".(int)$_FILES['file']['size'].", file_cat='".sql_escape($_POST['folder'])."', "
					."link='".sql_escape($_POST['link'])."', notes='".sql_escape($_POST['notes'])."' WHERE id=$id LIMIT 1");
				exit;
			}else die('Error');
		
		}elseif ($_GET['mode']=='merge') {	# merge files
			if ($_SERVER['HTTP_HOST']=='bafi.loc') exec("d:/WWW/bafi/tools/pdftk/pdftk.exe \"$targetFile\" \"{$_FILES['file']['tmp_name']}\" cat output \"{$targetFile}.new.pdf\"");
			else $Error = exec("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile={$targetFile}.new.pdf $targetFile {$_FILES['file']['tmp_name']}");

			if ($Error) die(iconv('windows-1255','utf-8','בעיה בחיבור קבצים.'));
			
			if (!is_file("{$targetFile}.new.pdf")) {
				error("Cannot merge files, CMD: pdftk.exe \"$targetFile\" \"{$_FILES['file']['tmp_name']}\" cat output \"{$targetFile}.new.pdf\"\r\n	OUT:".print_r($out,1));
				die('Error');
			}
			
			$newFileSize=filesize("{$targetFile}.new.pdf");
			if ($newFileSize>0) {
				unlink($targetFile);
				rename("{$targetFile}.new.pdf", $targetFile);
				runsql("UPDATE files SET file_size=$newFileSize, file_cat='".sql_escape($_POST['folder'])."', "
					."link='".sql_escape($_POST['link'])."', notes='".sql_escape($_POST['notes'])."' WHERE id=$id LIMIT 1");
				exit;
			}else die('Error');
		}	
	}else {	# Upload
		if ($overwriteId) f_del_file($overwriteId);
		
		if (!is_dir("../../crm-files/$SVARS[cid]/".substr($client_id, -2)."/$client_id")) 
			mkdir("../../crm-files/$SVARS[cid]/".substr($client_id, -2)."/$client_id", 0777, true);

		preg_match("/\A(.+)\.([a-z0-9_#!@-]{1,5})$/i", $_FILES['file']['name'], $regs);	# find file extention
		$file_ext=strtolower($regs[2]);
		$file_name = $_POST['file_name'] ? $_POST['file_name'].".$file_ext" : "file.$file_ext";
		$size=(int)$_FILES['file']['size'];
		
		$sql="INSERT INTO files SET client_id=$client_id, client_type='$client_type', group_id=$SVARS[cid], user_id={$SVARS['user']['id']}, "
			."file_name='".sql_escape($file_name)."', file_ext='$file_ext', file_size=$size, file_cat='".sql_escape($_POST['folder'])
			."', notes='".sql_escape($_POST['notes'])."', date_added='".date('Y-m-d H:i:s')."'";
		runsql($sql);
		//echo"/*$sql*/";
		$last_id=mysql_insert_id();
		
		# link
		$id1=$last_id;
		if($linked_data) foreach($linked_data as $row) {
			$id2=$row->id;
			$type=$row->type;				
			if($type AND $id2) $values.=($values?',':'')."('file-$type','$id1','$id2')";
		}
		
		if($id1) runsql("DELETE FROM links WHERE id1=$id1 AND type IN ('file-order','file-contact')");

		if($values){
			$linked_sql="INSERT INTO links (type,id1,id2) VALUES $values";
			runsql($linked_sql);
		}

		
		$uploadfile = "../../crm-files/$SVARS[cid]/".substr($client_id, -2)."/$client_id/$last_id".($file_ext?".$file_ext":'');
		if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
			if ($_FILES['RemoteFile']) exit;
			$sizeKb=( $size>1024000 ? number_format($size/1024) : round($size/1024,2) ).' kB';
			$user=jsEscape("$SVARS[fullname] ($SVARS[username])");
			die("{success:true, id:$last_id, size:'$sizeKb', 'user':'$user', date:'".date('d/m/y H:i')."' }");
		}else {
			runsql("DELETE FROM files WHERE id=$last_id");
			die('{success:false, msg:"!בעיה בשמירת קובץ"}');
		}
	}
}

//————————————————————————————————————————————————————————————————————————————————————
function f_join_pdfs() {
 global $CFG, $SVARS, $m, $group_id;
	$client_id=(int)$_GET['client_id'];
	$client_type=$_REQUEST['client_type'];
	if(!$client_type) $client_type='client';
	
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');

	$files=sql2array("SELECT * FROM $CFG[prefix]files WHERE id IN($_GET[ids]) ORDER BY file_name");
	if (count($files)<2) die('{success:false, msg:"Files not found!"}');
	foreach($files as $f) {
		eregi("^(.+)\.(.{3,4})$", $f['file_name'], $regs);
		if ($f['file_ext']!='pdf' OR strtolower($regs[2])!='pdf') die('{success:false, msg:"Only PDF files supported!"}');
		$file_path="../crm-files/$f[group_id]/".substr($f['client_id'], -2)."/$f[client_id]/$f[id]".($f['file_ext']?".$f[file_ext]":'');
		if (is_file($file_path)) {
			$file_list[] = $file_path; //'"'.realpath($file_path).'"';
			$new_file_name.= ($new_file_name?' + ':''). $regs[1];
			$new_size+=$f['file_size'];
		}
	}
	if (count($file_list)<2) die('{success:false, msg:"Files not found !"}');
	
	$new_file_name="חיבור $new_file_name.pdf";
	
	$sql="INSERT INTO files SET client_id=$client_id, client_type='$client_type', group_id=$group_id, user_id=$SVARS[user_id], "
		."file_name='".sql_escape($new_file_name)."', file_ext='pdf', file_size=$new_size, file_cat='".sql_escape($files[0]['file_cat'])."', "
		."link='', notes='', date_added='".date('Y-m-d H:i:s')."'";
	runsql($sql);
	$last_id=mysql_insert_id();

	$new_file = "../crm-files/$group_id/".substr($client_id, -2)."/$client_id/$last_id.pdf";	
	$pdftk = $_SERVER['HTTP_HOST']=='bafi.loc' ? 'd:/WWW/bafi/tools/pdftk/pdftk.exe' : 'pdftk';
	// exec("$pdftk ".implode(' ',$file_list)." cat output \"$new_file\"");
	exec("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=$new_file ".implode(' ',$file_list));

	if (!is_file($new_file)) {
		runsql("DELETE FROM files WHERE id=$last_id");
		die('{success:false, msg:"Error mergin files."}');
	}else {
		$sizeKb=( $new_size>1024000 ? number_format($new_size/1024) : round($new_size/1024,2) ).' kB';
		die("{success:true, id:$last_id, size:'$sizeKb', folder:'".jsEscape($files[0]['file_cat'])."', name:'".jsEscape($new_file_name)."', date:'".date('d/m/y H:i')."' }");
	}
}
//————————————————————————————————————————————————————————————————————————————————————
function f_del_file($id=null) {
 global $CFG, $SVARS, $m, $group_id;
 
	if ((int)$id) $_GET['ids']=(int)$id;
	elseif (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	
	$files=sql2array("SELECT * FROM files WHERE id IN($_GET[ids])"); 
	
	if (!$files) { if ($id) return; else die('{success:false, msg:"תקלת מערכת. קובץ לא קיים!"}'); }
	
	foreach($files as $file) {
		$filename="../crm-files/$file[group_id]/".substr($file['client_id'], -2)."/$file[client_id]/$file[id]".($file['file_ext']?".$file[file_ext]":'');
		if (is_file($filename)) unlink($filename);
	}
	
	if (runsql("DELETE FROM files WHERE id IN($_GET[ids])")) { if ($id) return true;  else die('{success:true}'); }
	else die('{success:false, msg:"תקלת מערכת. אנא נסו שנית מאוחר יותר."}');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_update_file() {
 global $CFG, $SVARS, $m, $group_id;
	$_POST['id']=(int)$_POST['id'];
	if (!$_POST['id']) die('{success:false, msg:"wrong file id"}');
	$linked_data = json_decode($_POST['linked_data']);


	$file_name = trim(iconv('UTF-8', 'windows-1255', $_POST['file_name']));
	if ($file_ext) $file_name.=".$file_ext";
	
	$sql="UPDATE files SET "
	."file_name='".sql_escape($file_name)
	."',file_cat='".sql_escape($_POST['folder'])
	."',notes='".sql_escape($_POST['notes'])
	."' WHERE id=$_POST[id]";
	runsql($sql);
	
	
	$id1=$_POST['id'];
	
	if($linked_data) foreach($linked_data as $row) {
		$id2=$row->id;
		$type=$row->type;
		if($type AND $id2) $values.=($values?',':'')."('file-$type',$id1,$id2)";
	}
	
	runsql("DELETE FROM links WHERE id1=$id1 AND type IN ('file-order','file-contact')");

	if($values){
		$linked_sql="INSERT INTO links (type,id1,id2) VALUES $values";
		runsql($linked_sql);
	}
	
	if (runsql($sql)) die("{success:true}");
	else die('{success:false, msg:"error updating file info"}');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_update_folder_name() {
 global $CFG, $SVARS, $m, $group_id;
	$client_id=(int)$_GET['client_id'];
	$oldName = trim( iconv('UTF-8', 'windows-1255', $_POST['oldName']) );
	$newName = trim( iconv('UTF-8', 'windows-1255', $_POST['newName']) );
	if (!$client_id OR !$oldName OR !$newName) die('{success:false, msg:"missing client_id or folder name"}');

	// $folder_name2num=array('כללי'=>1, 'אלמנטרי'=>2, 'חיים'=>3, 'שוק ההון'=>4, 'תביעות'=>5);
	$sql="UPDATE files SET file_cat=".quote($newName)." WHERE client_id=$client_id AND file_cat=".quote($oldName);
	if (runsql($sql)) die("{success:true}");
	else die('{success:false, msg:"error updating folder name"}');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_change_folder() {
 global $CFG, $SVARS, $m, $group_id;
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_GET['ids'])) die('{success:false, msg:"wrong ids"}');
	$newFolder = trim( iconv('UTF-8', 'windows-1255', $_GET['newFolder']) );
	if (!$newFolder) die('{success:false, msg:"missing folder name"}');
	
	$sql="UPDATE files SET file_cat=".quote($newFolder)." WHERE client_id=$_GET[client_id] AND id IN($_GET[ids])";
	echo "/* $sql */\r\n";
	if (runsql($sql)) die("{success:true}");
	else die('{success:false, msg:"error"}');
}
//————————————————————————————————————————————————————————————————————————————————————
function f_get_folders() {
 global $CFG, $SVARS, $m, $group_id;
	# load folders list
	$folders=sql2array("SELECT DISTINCT file_cat FROM files WHERE client_id=$_REQUEST[client_id] ORDER BY file_cat", 'file_cat', '1');
	
	if ($folders) { # convert numbers to names
		$file_cats=array('כללי','הזמנות','מכירות');
		foreach($folders as $k=>$v) if (is_numeric($k) AND $k>0 AND $k<5) { unset($folders[$k]); $folders[$file_cats[$k]]=null; }
		$folders=array_keys($folders);
	}else $folders=array();
	echo '{success:true, folders:'.array2json($folders).'}';

}

//————————————————————————————————————————————————————————————————————————————————————
function f_change_client() {
 global $CFG, $SVARS, $m, $group_id;
	$client_id=(int)$_GET['client_id'];
	// $toClient=(int)$_POST['toClient'];
	$toClient=(int)$_REQUEST['toClient'];
	if (!$toClient) die('{success:false, msg:"wrong toClient"}');
	if (!ereg('^[0-9]+(,[0-9]+)*$',$_REQUEST['ids'])) die('{success:false, msg:"wrong ids"}');
	$file_cat = trim( iconv('UTF-8', 'windows-1255', $_POST['file_cat']) );
	if (!$file_cat) die('{success:false, msg:"missing folder name"}');
	
	if($_REQUEST['clientType']=='group'){
	
		$to_group_id=sql2array("SELECT group_id FROM crm_groups WHERE team_id=$toClient LIMIT 1",0,'group_id',1);
		$toClient=-1 * $toClient;
		
	}else $to_group_id=sql2array("SELECT group_id FROM crm_clients WHERE client_id=$toClient LIMIT 1",0,'group_id',1);
	
	if (!$to_group_id) die('{success:false, msg:"bad toClient"}');
	if ($to_group_id!=$group_id) {	# check permission
		$permission = sql2array("SELECT pview FROM crm_permissions WHERE user_id=$SVARS[user_id] AND group_id=$group_id LIMIT 1", 0,'pview',1);
		if (!$permission) die('{success:false, msg:"no permission"}');
	}

	$files=sql2array("SELECT id,group_id,client_id,file_ext FROM files WHERE id IN($_REQUEST[ids])");
	# check source files
	foreach($files as $f) if (!is_file("../crm-files/$f[group_id]/".substr($f['client_id'], -2)."/$f[client_id]/$f[id]".($f['file_ext']?".$f[file_ext]":''))) die('{success:false, msg:"Error: one or more files are missing'.($SVARS['is_admin']?". id:$f[id]":'').'"}');
	
	# move files
	foreach($files as $f) {
		$src_file = "../crm-files/$f[group_id]/".substr($f['client_id'], -2)."/$f[client_id]/$f[id]".($f['file_ext']?".$f[file_ext]":'');
		$target_dir="../crm-files/$to_group_id/".substr($toClient, -2)."/$toClient";
		$target_file="$target_dir/$f[id]".($f['file_ext']?".$f[file_ext]":'');
		if (!is_dir($target_dir)) mkdir($target_dir, 0777, true);
		
			if (rename($src_file, $target_file)) $movedIds[]=$f['id'];
			else $errorIds[]=$f['id'];
		// }
	}
	if (!$movedIds) die('{success:false, msg:"error copying/renaming files"}');
	
		$sql="UPDATE files SET client_id=$toClient, file_cat=".quote($file_cat).", link=".quote($_POST['link'])." WHERE client_id=$_GET[client_id] AND id IN(".implode(',',$movedIds).")";
	
	if (runsql($sql)) die("{success:true}");
	else die('{success:false, msg:"error"}');
}

//————————————————————————————————————————————————————————————————————————————————————
function f_files_shortList(){
 global $group_id;
	$client_id=(int)$_REQUEST['client_id'];
	
 	if (!$client_id ) {echo '{success:false}'; return;}
	$sql = "SELECT files.* "
		."FROM files "
		."WHERE files.client_id='$client_id' AND files.group_id='$group_id'";
	$filesArray=sql2array($sql);
	if ($filesArray) {
		foreach ($filesArray as $key=>$row) 
			$files_ar["$row[id]"] = array(
				'id'=>$row['id'],
				'file_name'=>$row['file_name'],
				'file_size'=>$row['file_size'],
				'folder'=>$row['file_cat'],
				'icon'=>'file_'.$row['file_ext']
			);
	}
	if ($files_ar) sort($files_ar);
	
	echo '{success:true, total:'.count($files_ar).', data:'.($files_ar ? array2json($files_ar): '[]' ).'}';
}
?>