<?
/**
 * cfg_vars.php - included in inc/skin.php
 * This file contains user and group configuration. 
 * Replaces config definition in module initializing files like clients.php, reports.php, claims.php etc.  
 */

/*if ($SVARS['agent_id']==2998){
	$logo = glob("skin/logos/rafael_logo.PNG");
}
else{
	$logo = glob("skin/logos/bcom_logo.png");
}

if ($logo[0]) {
	$logo = $logo[0];
	list($w, $h) = getimagesize($logo);
	
	$img_w=100; $img_h=88;
	$ratio_orig = $w/$h;
	if ($img_w/$img_h > $ratio_orig) $img_w = round( $img_h*$ratio_orig );
	else $img_h = round( $img_w/$ratio_orig );
	$logo="<span id=user_logo><img src=\"$logo?".filemtime($logo)."\" width=$img_w height=$img_h style=\"-ms-interpolation-mode:bicubic; float:left;padding-right:5px;\"></span>";
}
else{
	$logo='<span id=user_logo></span>';
}*/
// print_ar($SVARS['user']);
$docNames	= sql2array("SELECT code, name FROM acc_doc_types",'code','name');
// print_ar($docNames);
?>

<script>
CFG={};
CFG.host='http://'+location.host;
CFG.user={
	user_id:'<?=$SVARS['user']['id'];?>'
	,username:'<?=jsEscape($SVARS['user']['username'])?>'
	,name:'<?=jsEscape($SVARS['user']['name'])?>'
};

CFG.doc_types = <?=array2json($docNames)?>;

</script>
